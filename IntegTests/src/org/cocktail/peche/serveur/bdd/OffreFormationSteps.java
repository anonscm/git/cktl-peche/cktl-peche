package org.cocktail.peche.serveur.bdd;

import org.cocktail.peche.serveur.bdd.WebDriverFactory.WebDriverType;
import org.cocktail.peche.serveur.bdd.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;

public class OffreFormationSteps {

	private WebDriver getWebDriver(){
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();	
	}

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);		
	}
	
	@Alors("^il clique sur l'onglet Enseignements$")
	public void il_clique_sur_l_onglet_Enseignements() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body//li[@id='enseignements_tabId']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^il clique sur le bouton de Offre Formation$")
	public void il_clique_sur_le_bouton_de_Offre_Formation() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body//nav/div/div[1]//div[1]/a[@id='menuItem_offreFormation']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}
	
}
