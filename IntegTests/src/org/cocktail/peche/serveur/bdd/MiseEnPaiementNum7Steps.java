package org.cocktail.peche.serveur.bdd;

import static org.junit.Assert.assertTrue;

import org.cocktail.peche.serveur.bdd.WebDriverFactory.WebDriverType;
import org.cocktail.peche.serveur.bdd.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.fr.Alors;

public class MiseEnPaiementNum7Steps {

	private WebDriver getWebDriver(){
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();	
	}

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);		
	}
	
	@Alors("^elle clique sur l'onglet de Mise en paiement$")
	public void elle_clique_sur_l_onglet_de_Mise_en_paiement() throws Throwable {
		String path="//html/body/header/nav/div/nav/ul/li[@id='MiseEnPaiement_tabId']";
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^elle clique sur le bouton de Paiement$")
	public void elle_clique_sur_le_bouton_de_Paiement() throws Throwable {
		String path="//html/body/header/nav/div/nav/div/div[1]/div/div[1]/a[@id='menuItem_paiement']";
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^elle clique sur le paiement \"([^\"]*)\"$")
	public void elle_clique_sur_le_paiement(String numero) throws Throwable {
		getWebDriver().findElement(By.linkText(numero)).click();
	}
	
	@Alors("^elle trouve un champs texte avec le \"([^\"]*)\" et le texte \"([^\"]*)\"$")
	public void elle_trouve_un_champs_texte_avec_le_et_le_texte(String libelle, String texte) throws Throwable {
	    
		assertTrue(libelle + " non trouvé dans la page", getWebDriver().getPageSource().contains(libelle));
		
		String xpath = "//html/body//form[2]//div[1]//div[3]//input";

		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		assertTrue(texte + " non trouvé dans la page", getWebDriver().getPageSource().contains(texte));
	}
	
}
