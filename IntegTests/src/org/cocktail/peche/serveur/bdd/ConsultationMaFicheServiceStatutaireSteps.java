package org.cocktail.peche.serveur.bdd;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.cocktail.peche.serveur.bdd.WebDriverFactory.WebDriverType;
import org.cocktail.peche.serveur.bdd.util.Constants;
import org.cocktail.peche.serveur.bdd.util.TestIntegrationUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;

public class ConsultationMaFicheServiceStatutaireSteps {

	private WebDriver getWebDriver(){
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();	
	}

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);		
	}
	
	@Lorsqu("^il se connecte$")
	public void il_se_connecte() throws Throwable {
		getWebDriver().findElement(By.name("mot_de_passe")).sendKeys("Cocktail");
		getWebDriver().findElement(By.name("validerLogin")).click();
	}

	@Alors("^il se retrouve sur la page d'accueil de Peche$")
	public void il_se_retrouve_sur_la_page_d_accueil_de_Peche() throws Throwable {
		// Express the Regexp above with the code you wish you had
		assertTrue(getWebDriver().getPageSource().contains("Le module PECHE est le module de gestion des charges et services d'enseignement"));
	}

	@Alors("^il clique sur le bouton de Ma Fiche de Service$")
	public void il_clique_sur_le_bouton_de_Ma_Fiche_de_Service() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("menuItem_ficheServiceStat")));
		List<WebElement> findElements = getWebDriver().findElements(By.id("menuItem_ficheServiceStat"));
		TestIntegrationUtil.getInstance().clickOnFirstVisibleElement(findElements);
		
	}	

	@Alors("^il vérifie qu'il est bien sur la page \"([^\"]*)\"$")
	public void il_vérifie_qu_il_est_bien_sur_la_page(String titrePage) throws Throwable {
		Assert.assertTrue(getWebDriver().getPageSource().contains(titrePage));
	}

	@Alors("^il voit un \"([^\"]*)\" de \"([^\"]*)\"$")
	public void il_voit_un_de(String libelle, String valeur) throws Throwable {
		Assert.assertTrue(getWebDriver().getPageSource().contains(libelle));
		Assert.assertTrue(getWebDriver().getPageSource().contains(valeur));
	}
	
}
