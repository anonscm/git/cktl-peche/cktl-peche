package org.cocktail.peche.serveur.bdd;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;


/**
 */
public class WebDriverFactory {
	private static WebDriverFactory webDriverFactory;
	private WebDriverType webDriverType;
	private WebDriver sharedWebDriver;

	public enum WebDriverType {
		FIREFOX, HTML_UNIT
	}

	public static WebDriverFactory getWebDriverFactory() {
		if (webDriverFactory == null) {
			webDriverFactory = new WebDriverFactory();
		}
		return webDriverFactory;
	}
	
	public void setWebDriverType(WebDriverType webDriverType) {
	  this.webDriverType = webDriverType;
  }
	
	public WebDriverType getWebDriverType() {
	  return webDriverType;
  }

	public WebDriver getSharedWebDriver() {
		if (sharedWebDriver == null) {
		
				switch (webDriverType) {
				case FIREFOX:
					sharedWebDriver = new FirefoxDriver();
					sharedWebDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					break;
				case HTML_UNIT:
					sharedWebDriver = new HtmlUnitDriver();
					break;
				default:
					sharedWebDriver = new FirefoxDriver();
					break;
				}
		}
	
		return sharedWebDriver;
	}


}
