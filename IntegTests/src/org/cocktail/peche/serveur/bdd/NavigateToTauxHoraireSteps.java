package org.cocktail.peche.serveur.bdd;

import static org.junit.Assert.assertTrue;

import org.cocktail.peche.serveur.bdd.WebDriverFactory.WebDriverType;
import org.cocktail.peche.serveur.bdd.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Alors;


public class NavigateToTauxHoraireSteps {
	
	private WebDriver getWebDriver(){
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();	
	}

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);		
	}
	
	@Alors("^elle clique sur l'onglet de Parametrage$")
	public void elle_clique_sur_l_onglet_de_Parametrage() throws Throwable {
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body/header/nav/div/nav/ul/li[@id='parametrage_tabId']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^elle clique sur le bouton de Taux horaires$")
	public void elle_clique_sur_le_bouton_de_Taux_horaires() throws Throwable {
		Thread.sleep(4000);
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body/header/nav/div/nav/div/div[2]/div/div[1]/a[@id='menuItem_tauxhoraire']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}
	
	@Alors("^elle vérifie qu'elle est bien sur la page \"([^\"]*)\"$")
	public void elle_vérifie_qu_elle_est_bien_sur_la_page(String titrePage) throws Throwable {
		assertTrue(getWebDriver().getPageSource().contains(titrePage));
	}

	@Alors("^elle voit un \"([^\"]*)\" de \"([^\"]*)\"$")
	public void elle_voit_un_de(String libelle, String valeur) throws Throwable {
		assertTrue(getWebDriver().getPageSource().contains(libelle));
		assertTrue(getWebDriver().getPageSource().contains(valeur));
	}
}
