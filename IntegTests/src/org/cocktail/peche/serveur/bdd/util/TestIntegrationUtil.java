package org.cocktail.peche.serveur.bdd.util;

import java.util.Collection;

import org.openqa.selenium.WebElement;

/**
 * Classe utilitaire sur les tests d'integration.
 * @author anthony
 *
 */
public final class TestIntegrationUtil {

	private static TestIntegrationUtil INSTANCE;
	
	private TestIntegrationUtil() {
		
	}
	
	/**
	 * @return l'instance du singleton.
	 */
	public static TestIntegrationUtil getInstance(){
		if(INSTANCE == null){
			INSTANCE = new TestIntegrationUtil();
		}
		return INSTANCE;
	}
	
	/**
	 * Clique sur le premier element visible de la liste.
	 * @param elements
	 */
	public void clickOnFirstVisibleElement(Collection<WebElement> elements){
		for(WebElement element : elements){
			if(element.isDisplayed()){
				element.click();
				return;
			}
		}
	}
	
}
