package org.cocktail.peche.serveur.bdd.util;

/**
 * 
 * Classe de constantes pour les TI.
 * 
 * @author anthony
 *
 */
public final class Constants {

	private Constants() {
	}
	
	public static final int TIMEOUT_TIME = 10;
	
}
