package org.cocktail.peche.serveur.bdd;

import static org.junit.Assert.assertTrue;

import org.cocktail.peche.serveur.bdd.WebDriverFactory.WebDriverType;
import org.cocktail.peche.serveur.bdd.util.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.java.fr.Alors;

public class SuiviValidationsSteps {

	private WebDriver getWebDriver(){
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();	
	}

	@cucumber.api.java.Before
	public void setUp() {
		WebDriverFactory.getWebDriverFactory().setWebDriverType(WebDriverType.FIREFOX);		
	}
	
	@Alors("^il clique sur l'onglet Suivi$")
	public void il_clique_sur_l_onglet_Suivi() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body//li[@id='suivi_tabId']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}

	@Alors("^il clique sur le bouton de Suivi des validations$")
	public void il_clique_sur_le_bouton_de_Suivi_des_validations() throws Throwable {
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		String path="//html/body//nav/div/div[1]//div[1]/a[@id='menuItem_suiviValidations']";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(path)));
		WebElement w=getWebDriver().findElement(By.xpath(path));
		w.click();
	}
	
	@Alors("^il trouve une liste déroulante avec le \"([^\"]*)\" et le texte \"([^\"]*)\"$")
	public void il_trouve_une_liste_déroulante_avec_le_et_le_texte(String libelle, String texte) throws Throwable {
		assertTrue(libelle + " non trouvé dans la page", getWebDriver().getPageSource().contains(libelle));
		
		String xpath = "//html/body/section//form/div[1]/div/div[2]/div[1]//select";
		
		WebDriverWait wait = new WebDriverWait(getWebDriver(), Constants.TIMEOUT_TIME);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		assertTrue(texte + " non trouvé dans la page", getWebDriver().getPageSource().contains(texte));
	}
}
