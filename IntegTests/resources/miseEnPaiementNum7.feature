# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à la page Liste des Taux Horaires de l'application Peche
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur l'onglet Mise en apiement et je choisis le paiement N°7
  
  
  
  Scénario: Accès à la liste des taux horaires pour un utilisateur habilité
    Soit alexia habilitée à utiliser Peche
    Lorsqu'elle se connecte
    Alors elle se retrouve sur la page d'accueil de Peche
    Et elle clique sur l'onglet de Mise en paiement
    Et elle clique sur le bouton de Paiement
    Et elle clique sur le paiement "00007"
 	Et elle vérifie qu'elle est bien sur la page "Détail d'un paiement"
 	Alors elle voit un "Date du paiement" de "18/12/2014"
 	Alors elle trouve un champs texte avec le "Libellé" et le texte "Paiement n° 7 du 18/12/2014"