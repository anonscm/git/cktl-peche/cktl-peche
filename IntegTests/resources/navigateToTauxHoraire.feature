# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à la page Liste des Taux Horaires de l'application Peche
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur l'onglet Paramétrage et je choisis le menu Taux horaire
  
  
  
  Scénario: Accès à la liste des taux horaires pour un utilisateur habilité
    Soit alexia habilitée à utiliser Peche
    Lorsqu'elle se connecte
    Alors elle se retrouve sur la page d'accueil de Peche
    Et elle clique sur l'onglet de Parametrage
    Et elle clique sur le bouton de Taux horaires
 	Et elle vérifie qu'elle est bien sur la page "Liste des taux horaires"
 	Alors elle voit un "Taux brut" de "40,91"