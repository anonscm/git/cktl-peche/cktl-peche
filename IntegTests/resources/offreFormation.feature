# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à la page Offre de Formation de l'application Peche
  En tant qu'utilisateur de Peche
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur la page de consultation de l'offre de formation
  
  
  
  Scénario: Accès à la liste l'offre de formation pour un utilisateur habilité
    Soit ogrange habilitée à utiliser Peche
    Lorsqu'il se connecte
    Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur l'onglet Enseignements
    Et il clique sur le bouton de Offre Formation
    Alors il vérifie qu'il est bien sur la page "Offre de formation"
 	