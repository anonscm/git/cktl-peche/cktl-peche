# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à la page Suivi des Validations de l'application Peche
  En tant qu'utilisateur de Peche
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur la page de consultation de l'offre de formation
  
  
  
  Scénario: Accès à au suivi des validations pour un utilisateur habilité
    Soit ogrange habilitée à utiliser Peche
    Lorsqu'il se connecte
    Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur l'onglet Suivi
    Et il clique sur le bouton de Suivi des validations
 	Alors il vérifie qu'il est bien sur la page "Suivi des validations"
 	Alors il trouve une liste déroulante avec le "Année universitaire" et le texte "2014/2015"
 	