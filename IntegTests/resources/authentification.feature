# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à l'application Peche
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe

  Scénario: Authentification d'un utilisateur habilité
    Soit alexia habilitée à utiliser Peche
    Lorsqu'elle se connecte
    Alors elle se retrouve sur la page d'accueil de Peche