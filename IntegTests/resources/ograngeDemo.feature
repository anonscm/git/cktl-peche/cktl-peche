# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à des pages de l'application Peche
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur l'onglet Paramétrage et je choisis le menu Taux horaire
  
  
  
  Scénario: Test 1 - Accès à la liste la fiche de service pour un utilisateur habilité
    Soit ogrange habilitée à utiliser Peche
    Lorsqu'il se connecte
    Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur le bouton de Ma Fiche de Service
 	Et il vérifie qu'il est bien sur la page "Fiche individuelle de service"
 	Alors il voit un "Service dû" de "192,00"
 	
 
  Scénario: Test 2 - Accès à la liste l'offre de formation pour un utilisateur habilité
   ## Soit ogrange habilitée à utiliser Peche
   ## Lorsqu'il se connecte
   ## Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur l'onglet Enseignements
    Et il clique sur le bouton de Offre Formation
 	Alors il vérifie qu'il est bien sur la page "Offre de formation"
 	
 Scénario: Test 3 - Accès à au suivi des validations pour un utilisateur habilité
    #Soit ogrange habilitée à utiliser Peche
    #Lorsqu'il se connecte
    #Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur l'onglet Suivi
    Et il clique sur le bouton de Suivi des validations
 	Alors il vérifie qu'il est bien sur la page "Suivi des validations"
 	Alors il trouve une liste déroulante avec le "Année universitaire" et le texte "2014/2015"