# language: fr
Fonctionnalité: Authentification hors CAS
  Afin d'accéder à la page Liste des Taux Horaires de l'application Peche
  En tant qu'utilisateur du PGI Cocktail
  Je dois m'identifier à l'aide de mon identifiant et de mon mot de passe
  Puis je vais sur ma fiche de Service en tant que statutaire
  
  
  
  Scénario: Accès à la liste la fiche de service pour un utilisateur habilité
    Soit ogrange habilitée à utiliser Peche
    Lorsqu'il se connecte
    Alors il se retrouve sur la page d'accueil de Peche
    Et il clique sur le bouton de Ma Fiche de Service
 	Et il vérifie qu'il est bien sur la page "Fiche individuelle de service"
 	Alors il voit un "Service dû" de "192,00"