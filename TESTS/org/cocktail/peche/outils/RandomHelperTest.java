package org.cocktail.peche.outils;

import org.junit.Assert;
import org.junit.Test;


public class RandomHelperTest {

	private static final String PREFIX = "prefix";

	@Test
	public void getRandomStringShouldNotGiveSameResults() throws Exception {
		String firstRandomString = RandomHelper.getInstance().getRandomString();
		String secondRandomString = RandomHelper.getInstance().getRandomString();
		Assert.assertNotEquals(firstRandomString, secondRandomString);
	}

	@Test
	public void getRandomStringWithPrefixShouldNotGiveSameResults() throws Exception {
		String randomStringWithPrefix = RandomHelper.getInstance().getRandomStringWithPrefix(PREFIX);
		Assert.assertTrue(randomStringWithPrefix.contains(PREFIX));
	}

	
}
