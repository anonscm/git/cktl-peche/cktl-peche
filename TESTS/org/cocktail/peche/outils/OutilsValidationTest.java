package org.cocktail.peche.outils;

import junit.framework.Assert;

import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

public class OutilsValidationTest {
	
	@Mock
	Session session = Mockito.mock(Session.class);

	private static final int VINGT_QUATRE_HEURES = 1000 * 60 * 60 * 24;
	
	private NSTimestamp aujourdhui;
	private NSTimestamp demain;
	
	@Before
	public void setUp() {
		long heure = System.currentTimeMillis();
		this.aujourdhui = new NSTimestamp(heure);
		this.demain = new NSTimestamp(heure + VINGT_QUATRE_HEURES);
	}
	
	@After
	public void tearDown() {
		
	}
	

	@Test
	public void testVerifierOrdreDatesOk() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(this.aujourdhui, this.demain, session);
		}
		catch (ValidationException e) {
			catched = true;
		}
		
		Assert.assertFalse("Une exception n'aurait pas du être levée", catched);
		// On vérifie qu'aucune méthode n'a été appelée sur la session.
		Mockito.verifyNoMoreInteractions(session);
	}

	@Test
	public void testVerifierOrdreDatesKo() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(this.demain, this.aujourdhui, session);
		}
		catch (ValidationException e) {
			catched = true;
		}
		
		Assert.assertTrue("Une exception aurait du être levée", catched);
	}

	@Test
	public void testVerifierOrdreDatesMemeJour() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(this.aujourdhui, this.aujourdhui, session);
		}
		catch (ValidationException e) {
			catched = true;
		}
		
		Assert.assertFalse("Une exception n'aurait pas du être levée", catched);
		// On vérifie que la seule et unique méthode appelée sur la session est la suivante :
		Mockito.verify(session, Mockito.only()).addSimpleMessage(TypeMessage.ATTENTION, OutilsValidation.LA_DATE_DE_FIN_EST_LA_MEME_QUE_LA_DATE_DE_DEBUT);
	}

	@Test
	public void testVerifierOrdreDatesDateDebutNulle() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(null, this.aujourdhui, session);
		}
		catch (ValidationException e) {
			catched = true;
		}

		Assert.assertTrue("Une exception aurait du être levée", catched);
	}

	@Test
	public void testVerifierOrdreDatesDateFinNulle() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(this.aujourdhui, null, session);
		}
		catch (ValidationException e) {
			catched = true;
		}

		Assert.assertTrue("Une exception aurait du être levée", catched);
	}

	@Test
	public void testVerifierOrdreDatesDatesNulles() {
		boolean catched = false;
		try {
			OutilsValidation.verifierOrdreDates(null, null, session);
		}
		catch (ValidationException e) {
			catched = true;
		}

		Assert.assertTrue("Une exception aurait du être levée", catched);
	}
}
