package org.cocktail.peche.metier.hce;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class MethodeDeCalculParProratisationSurPresentielTest extends AbstractTest {
	
	/**
	 * Service dû : 192 heures :: 100 heures TP, 56 heures TD, 56 heures CM => 44,86 HETD Comp, 212 présentiel, 236,86 HETD
	 */
	@Test
	public void testHcomp() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "4", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
		double serviceDu = 192.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "AP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(56.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 2, "AP2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(56.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "AP3", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(100.0d);
				serviceDetail.setService(service);
			}		
		}
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(44.86d, hcompPrevues, 0.03d);
		assertEquals(236.855d, serviceHETDPrevu, 0.0d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 56.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 100.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 56.0f)
				)
		);

		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(236.855d, serviceHETDSouhaite, 0.00d);
		assertEquals(44.86d, hcompSouhaites, 0.01d);
	}

	/**
	 * Service dû : 192 heures :: 60 heures TP, 100 heures TD, 40 heures CM => 200 présentiel, 27.20 HETD Comp, 219,20 HETD
	 */
	@Test
	public void testRouenMail() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "4", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
		double serviceDu = 192.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "AP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(40.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 2, "AP2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(100.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "AP3", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(60.0d);
				serviceDetail.setService(service);
			}		
		}
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(27.20d, hcompPrevues, 0.03d);
		assertEquals(219.20d, serviceHETDPrevu, 0.0d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 40.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 60.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 100.0f)
				)
		);
		
		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(219.20d, serviceHETDSouhaite, 0.00d);
		assertEquals(27.20d, hcompSouhaites, 0.01d);
	}

	/**
	 * Service dû : 192 heures :: 50 heures TP, 50 heures TD, 100 heures CM => 200 présentiel, 57.33 HETD Comp, 249,33 HETD
	 */
	@Test
	public void testRouenCahierDesCharge() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "4", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
		double serviceDu = 192.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "AP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(100.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 2, "AP2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(50.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "AP3", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(50.0d);
				serviceDetail.setService(service);
			}		
		}
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(57.33d, hcompPrevues, 0.03d);
		assertEquals(249.333d, serviceHETDPrevu, 0.0d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 100.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 50.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 50.0f)
				)
		);
		
		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(249.333d, serviceHETDSouhaite, 0.00d);
		assertEquals(57.33d, hcompSouhaites, 0.01d);
	}

	@Test
	public final void testServiceHETDBasique() {
		double serviceDu = 192.0d;
		
		EOService service = eoService(
				eoServiceDetail(
						eoAP(1, "AP1", typeCM, 56.0f)
				),
				eoServiceDetail(
						eoAP(2, "AP2", typeTP, 100.0f)
				),
				eoServiceDetail(
						eoAP(2, "AP3", typeTD, 56.0f)
				)
		);
		
		EOCorps corps = EOCorps.creerInstance(ec);

		eoParamPopHetd(corps, typeCM, 3, 2, 3, 2);
		eoParamPopHetd(corps, typeTD, 1, 1, 1, 1);
		eoParamPopHetd(corps, typeTP, 1, 1, 2, 3);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "4", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(236.855d, serviceHETDPrevu, 0.0d);
		assertEquals(0.0d, serviceHETDRealise, 0.0d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 56.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 100.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 56.0f)
				)
		);
		
		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		assertEquals(236.855d, serviceHETDSouhaite, 0.00d);
	}
}
