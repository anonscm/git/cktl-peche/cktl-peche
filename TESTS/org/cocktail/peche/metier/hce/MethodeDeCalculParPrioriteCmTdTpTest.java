package org.cocktail.peche.metier.hce;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class MethodeDeCalculParPrioriteCmTdTpTest extends AbstractTest {
	
	/**
	 * Service dû : 192 heures :: 56 heures CM, 56 heures TD, 100 heures TP => 32 HETD Comp, 
	 */
	@Test
	public void testLaRochelleExemple1() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOActuelEnseignant actuelEnseignant = new EOActuelEnseignant();
//		actuelEnseignant.setC
//		actuelEnseignant.to
		
		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
//		personne.toi
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "2", null, null);

		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		
		double serviceDu = 192.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "APCM1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(56.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 2, "APTD1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(56.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "APTP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(100.0d);
				serviceDetail.setService(service);
			}
		}
		
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(32.0d, hcompPrevues, 0.00d);
		assertEquals(224.0d, serviceHETDPrevu, 0.00d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 56.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 100.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP3", typeTD, 56.0f)
				)
		);

		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(224.0d, serviceHETDSouhaite, 0.00d);
		assertEquals(32.0d, hcompSouhaites, 0.00d);
	}
}
