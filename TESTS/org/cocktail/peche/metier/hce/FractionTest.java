package org.cocktail.peche.metier.hce;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class FractionTest {

	@Test
	public void testUnite() {
		Fraction f = new Fraction(1, 1);
		assertEquals(1.0d, f.times(1.0d), 0.0d);
		assertEquals(12.334d, f.times(12.334d), 0.0d);
		double d = Math.random();
		assertEquals(d, f.times(d), 0.0d);
	}
	
	@Test
	public void testInverse() {
		Fraction f = new Fraction(3, 2);
		f = f.inverse();
		assertEquals(2, f.getNumerateur());
		assertEquals(3, f.getDenominateur());
	}
	
	@Test
	public void testInverseNumerateurNull() {
		Fraction f = new Fraction(0, 3);
		boolean caught = false;
		try {
			f.inverse();
		}
		catch (ArithmeticException ae) {
			caught = true;
		}
		assertTrue(caught);
	}
	
	@Test
	public void testDenominateurNull() {
		boolean caught = false;
		try {
			new Fraction(2, 0);
		}
		catch (ArithmeticException ae) {
			caught = true;
		}
		assertTrue(caught);
	}
	
	@Test
	public void testMultiplication() {
		Fraction f = new Fraction(3, 2);
		double d = f.times(3);
		assertEquals(4.5d, d, 0.0d);
		d = Math.random();
		assertEquals(d * 3.0d / 2.0d, f.times(d), 0.0d);
		
		f = new Fraction(2, 3);
		d = f.times(3);
		assertEquals(2.0d, d, 0.0d);
		d = Math.random();
		assertEquals(d * 2.0d / 3.0d, f.times(d), 0.0d);
	}
	
	@Test
	public void testMultiplicationFractions() {
		Fraction f1 = new Fraction(3, 2);
		Fraction f2 = new Fraction(4, 5);
		
		Fraction f = f1.times(f2);
		
		assertEquals(6, f.getNumerateur());
		assertEquals(5, f.getDenominateur());
		
		f2 = new Fraction(2, 3);
		f = f1.times(f2);
		
		assertEquals(1, f.getNumerateur());
		assertEquals(1, f.getDenominateur());
	}
}
