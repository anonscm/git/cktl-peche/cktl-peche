package org.cocktail.peche.metier.hce;

import java.util.List;
import java.util.Locale;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.Keywords;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.i18n.LocalizedKeywords;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.CandidateSteps;
import org.jbehave.core.steps.InstanceStepsFactory;

public abstract class MoteurCalculHCEStory extends JUnitStory {

	@Override
	public Configuration configuration() {
		Keywords keywords = new LocalizedKeywords(new Locale("fr"));
		return new MostUsefulConfiguration()
			.useKeywords(keywords)
			.useStoryParser(new RegexStoryParser(keywords))
			.useStoryLoader(new LoadFromClasspath(this.getClass()))
			.useStoryReporterBuilder(
					new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.TXT));
	}

	@Override
	public List<CandidateSteps> candidateSteps() {
		return new InstanceStepsFactory(configuration(), new MoteurCalculHCESteps()).createCandidateSteps();
	}
}
