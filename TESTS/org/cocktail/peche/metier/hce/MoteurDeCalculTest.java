/**
 *
 */
package org.cocktail.peche.metier.hce;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Equipe PECHE
 *
 */
public class MoteurDeCalculTest extends AbstractTest {
	
	@Test
	public final void testHeuresCompBasique() {
		
		EOService service = eoService(
				eoServiceDetail(
						eoAP(1, "AP1", typeCM, 100.0f)
				),
				eoServiceDetail(
						eoAP(2, "AP2", typeTP, 50.0f)
				),
				eoServiceDetail(
						eoAP(2, "AP3", typeTD, 50.0f)
				)
		);
		

		IMethodeDeCalcul methode = mock(IMethodeDeCalcul.class);
		when(methode.hcompPrevu()).thenReturn(12.34d);
		
		EOCorps corps = mock(EOCorps.class);
		
		EOActuelEnseignant actuelEnseignant = mock(EOActuelEnseignant.class);
		
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methode);
		
		double hcomp = moteur.getHeuresComplementairesPrevues(192, service);
		
		assertEquals(hcomp, 12.34d, 0.0d);
		
		InOrder orderedExec = inOrder(methode);
		orderedExec.verify(methode, times(1)).init(192.0d);
		orderedExec.verify(methode, times(3)).traiterDetailService(any(EOServiceDetail.class), any(Fraction.class), any(Fraction.class));
		orderedExec.verify(methode, times(1)).hcompPrevu();
	}
	
	@Test
	public void testerDatesOK() {
		
		EOCorps corps = EOCorps.creerInstance(ec);
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantPast);
		param.setDateFin(NSTimestamp.DistantFuture);
		param.setNumerateurService(3);
		param.setDenominateurService(2);
		param.setNumerateurHorsService(1);
		param.setDenominateurHorsService(4);
		
		IMethodeDeCalcul methodeDeCalcul = mock(IMethodeDeCalcul.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methodeDeCalcul);
		
		EOService service = mock(EOService.class);
		
		NSArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>();
		
		EOServiceDetail serviceDetail = mock(EOServiceDetail.class);
		EOAP ap = mock(EOAP.class);
		when(ap.typeAP()).thenReturn(typeCM);
		when(serviceDetail.composantAP()).thenReturn(ap);
		listeServiceDetail.add(serviceDetail);
		
		when(service.listeServiceDetails()).thenReturn(listeServiceDetail);
		moteur.getHeuresComplementairesPrevues(192, service);
		
		InOrder orderedExec = inOrder(methodeDeCalcul);
		
		ArgumentCaptor<Fraction> enService = ArgumentCaptor.forClass(Fraction.class);
		ArgumentCaptor<Fraction> horsService = ArgumentCaptor.forClass(Fraction.class);
		orderedExec.verify(methodeDeCalcul, times(1)).traiterDetailService(any(EOServiceDetail.class), enService.capture(), horsService.capture());
		assertEquals(3, enService.getValue().getNumerateur());
		assertEquals(2, enService.getValue().getDenominateur());
//		assertEquals(1, horsService.getValue().getNumerateur());
//		assertEquals(4, horsService.getValue().getDenominateur());
	}

	@Test
	public void testerDateFinNull() {
		
		EOCorps corps = EOCorps.creerInstance(ec);
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantPast);
		param.setDateFin(null);
		param.setNumerateurService(1);
		param.setDenominateurService(2);
		param.setNumerateurHorsService(3);
		param.setDenominateurHorsService(4);
		
		IMethodeDeCalcul methodeDeCalcul = mock(IMethodeDeCalcul.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methodeDeCalcul);
		
		EOService service = mock(EOService.class);
		
		NSArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>();
		
		EOServiceDetail serviceDetail = mock(EOServiceDetail.class);
		EOAP ap = mock(EOAP.class);
		when(ap.typeAP()).thenReturn(typeCM);
		when(serviceDetail.composantAP()).thenReturn(ap);
		listeServiceDetail.add(serviceDetail);
		
		when(service.listeServiceDetails()).thenReturn(listeServiceDetail);
		moteur.getHeuresComplementairesPrevues(192, service);
		
		InOrder orderedExec = inOrder(methodeDeCalcul);
		
		ArgumentCaptor<Fraction> enService = ArgumentCaptor.forClass(Fraction.class);
		ArgumentCaptor<Fraction> horsService = ArgumentCaptor.forClass(Fraction.class);
		orderedExec.verify(methodeDeCalcul, times(1)).traiterDetailService(any(EOServiceDetail.class), enService.capture(), horsService.capture());
//		assertEquals(1, enService.getValue().getNumerateur());
		assertEquals(2, enService.getValue().getDenominateur());
		assertEquals(3, horsService.getValue().getNumerateur());
//		assertEquals(4, horsService.getValue().getDenominateur());
	}

	@Test
	public void testerDateDebutKO() {
		
		EOCorps corps = EOCorps.creerInstance(ec);
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantFuture);
		param.setDateFin(NSTimestamp.DistantFuture);
		param.setNumerateurService(3);
		param.setDenominateurService(2);
		param.setNumerateurHorsService(1);
		param.setDenominateurHorsService(4);
		
		IMethodeDeCalcul methodeDeCalcul = mock(IMethodeDeCalcul.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methodeDeCalcul);
		
		EOService service = mock(EOService.class);
		
		NSArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>();
		
		EOServiceDetail serviceDetail = mock(EOServiceDetail.class);
		EOAP ap = mock(EOAP.class);
		when(ap.typeAP()).thenReturn(typeCM);
		when(serviceDetail.composantAP()).thenReturn(ap);
		listeServiceDetail.add(serviceDetail);
		
		when(service.listeServiceDetails()).thenReturn(listeServiceDetail);
		moteur.getHeuresComplementairesPrevues(192, service);
		
		InOrder orderedExec = inOrder(methodeDeCalcul);
		
		ArgumentCaptor<Fraction> enService = ArgumentCaptor.forClass(Fraction.class);
		ArgumentCaptor<Fraction> horsService = ArgumentCaptor.forClass(Fraction.class);
		orderedExec.verify(methodeDeCalcul, times(1)).traiterDetailService(any(EOServiceDetail.class), enService.capture(), horsService.capture());
		assertEquals(3, enService.getValue().getNumerateur());
		assertEquals(2, enService.getValue().getDenominateur());
		assertEquals(3, horsService.getValue().getNumerateur());
		assertEquals(2, horsService.getValue().getDenominateur());
	}

	@Test
	public void testerDateFinKO() {
		
		EOCorps corps = EOCorps.creerInstance(ec);
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantPast);
		param.setDateFin(NSTimestamp.DistantPast);
		param.setNumerateurService(3);
		param.setDenominateurService(2);
		param.setNumerateurHorsService(1);
		param.setDenominateurHorsService(4);
		
		IMethodeDeCalcul methodeDeCalcul = mock(IMethodeDeCalcul.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methodeDeCalcul);
		
		EOService service = mock(EOService.class);
		
		NSArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>();
		
		EOServiceDetail serviceDetail = mock(EOServiceDetail.class);
		EOAP ap = mock(EOAP.class);
		when(ap.typeAP()).thenReturn(typeCM);
		when(serviceDetail.composantAP()).thenReturn(ap);
		listeServiceDetail.add(serviceDetail);
		
		when(service.listeServiceDetails()).thenReturn(listeServiceDetail);
		moteur.getHeuresComplementairesPrevues(192, service);
		
		InOrder orderedExec = inOrder(methodeDeCalcul);
		
		ArgumentCaptor<Fraction> enService = ArgumentCaptor.forClass(Fraction.class);
		ArgumentCaptor<Fraction> horsService = ArgumentCaptor.forClass(Fraction.class);
		orderedExec.verify(methodeDeCalcul, times(1)).traiterDetailService(any(EOServiceDetail.class), enService.capture(), horsService.capture());
		assertEquals(3, enService.getValue().getNumerateur());
		assertEquals(2, enService.getValue().getDenominateur());
		assertEquals(3, horsService.getValue().getNumerateur());
		assertEquals(2, horsService.getValue().getDenominateur());
	}


	public void testerMultiplesDate() {
		
		EOCorps corps = EOCorps.creerInstance(ec);
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantPast);
		param.setDateFin(NSTimestamp.DistantPast);
		param.setNumerateurService(1);
		param.setDenominateurService(2);
		param.setNumerateurHorsService(3);
		param.setDenominateurHorsService(4);
		
		param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantPast);
		param.setDateFin(NSTimestamp.DistantFuture);
		param.setNumerateurService(5);
		param.setDenominateurService(6);
		param.setNumerateurHorsService(7);
		param.setDenominateurHorsService(8);
		
		param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeCM);
		param.setDateDebut(NSTimestamp.DistantFuture);
		param.setDateFin(NSTimestamp.DistantFuture);
		param.setNumerateurService(9);
		param.setDenominateurService(10);
		param.setNumerateurHorsService(11);
		param.setDenominateurHorsService(12);
		
		IMethodeDeCalcul methodeDeCalcul = mock(IMethodeDeCalcul.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
//		moteur.setMethodeDeCalcul(methodeDeCalcul);
		
		EOService service = mock(EOService.class);
		
		NSArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>();
		
		EOServiceDetail serviceDetail = mock(EOServiceDetail.class);
		EOAP ap = mock(EOAP.class);
		when(ap.typeAP()).thenReturn(typeCM);
		when(serviceDetail.composantAP()).thenReturn(ap);
		listeServiceDetail.add(serviceDetail);
		
		when(service.listeServiceDetails()).thenReturn(listeServiceDetail);
		moteur.getHeuresComplementairesPrevues(192, service);
		
		InOrder orderedExec = inOrder(methodeDeCalcul);
		
		ArgumentCaptor<Fraction> enService = ArgumentCaptor.forClass(Fraction.class);
		ArgumentCaptor<Fraction> horsService = ArgumentCaptor.forClass(Fraction.class);
		orderedExec.verify(methodeDeCalcul, times(1)).traiterDetailService(any(EOServiceDetail.class), enService.capture(), horsService.capture());
		assertEquals(5, enService.getValue().getNumerateur());
		assertEquals(6, enService.getValue().getDenominateur());
		assertEquals(7, horsService.getValue().getNumerateur());
		assertEquals(8, horsService.getValue().getDenominateur());
	}
	
	@Test
	public final void testHeuresCompBasiqueVoeux() {
		
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 100.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 50.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP3", typeTD, 50.0f)
				)
		);
		

		IMethodeDeCalcul methode = mock(IMethodeDeCalcul.class);
		when(methode.serviceHETDPrevu()).thenReturn(233.33d);
		
		EOCorps corps = mock(EOCorps.class);
		
//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		moteur.remplacerMethode(methode);
		
//		double serviceSouhaite = moteur.getServiceHETDSouhaite(corps, 192, voeux);
		
//		assertEquals(serviceSouhaite, 233.33d, 0.0d);
		
		InOrder orderedExec = inOrder(methode);
//		orderedExec.verify(methode, times(1)).init(192.0d);
//		orderedExec.verify(methode, times(3)).traiterDetailService(any(EOFicheVoeuxDetail.class), any(Fraction.class), any(Fraction.class));
//		orderedExec.verify(methode, times(1)).serviceHETDPrevu();
	}

	@Test
	public void testerConvertirEnHetdEnService() {
		EOCorps corps = mock(EOCorps.class);
		
		EOAP apCm = mock(EOAP.class);
		when(apCm.typeAP()).thenReturn(typeCM);
		
		EOAP apTd = mock(EOAP.class);
		when(apTd.typeAP()).thenReturn(typeTD);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
//		BigDecimal heureHetd = moteur.convertirEnHetdEnService(corps, apCm, BigDecimal.valueOf(15.3).setScale(2));
//		assertEquals(22.95, heureHetd.doubleValue(), 0.0);

//		heureHetd = moteur.convertirEnHetdEnService(corps, apTd, BigDecimal.valueOf(15.3).setScale(2));
//		assertEquals(15.3, heureHetd.doubleValue(), 0.0);
	}
}
