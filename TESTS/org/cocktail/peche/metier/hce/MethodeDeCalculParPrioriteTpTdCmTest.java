package org.cocktail.peche.metier.hce;

import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class MethodeDeCalculParPrioriteTpTdCmTest extends AbstractTest {
	
	/**
	 * Enseignant du second degré.
	 * <p>
	 * Service dû : 384 heures :: 300 heures TP, 50 heures TD, 40 heures CM => 26 HETD Comp
	 */
	@Test
	public void testToulouseExemple1() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "3", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
		double serviceDu = 384.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "APTP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(250.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 2, "APTP2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(50.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "APTD1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(12.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 4, "APTD2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(38.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 5, "APCM1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(33.0d);
				serviceDetail.setService(service);
			}		
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 6, "APCM2", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(7.0d);
				serviceDetail.setService(service);
			}		
		}
		
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(26.0d, hcompPrevues, 0.00d);
		assertEquals(410.0d, serviceHETDPrevu, 0.00d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 33.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 250.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 38.0f)
				),
				eoVoeuxDetail(
						eoAP(4, "AP4", typeCM, 7.0f)
				),
				eoVoeuxDetail(
						eoAP(5, "AP5", typeTP, 50.0f)
				),
				eoVoeuxDetail(
						eoAP(6, "AP6", typeTD, 12.0f)
				)
		);

		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(410.0d, serviceHETDSouhaite, 0.00d);
		assertEquals(26.0d, hcompSouhaites, 0.02d);
	}

	/**
	 * Enseignant du second degré.
	 * <p>
	 * Service dû : 384 heures :: 400 heures TP, 10 heures TD, 10 heures CM => 35.66 HETD Comp, 419.66 HETD
	 */
	@Test
	public void testToulouseExemple2() {
		EOCorps corps = EOCorps.creerInstance(ec);

		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");
		
		EOParamPopHetd param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 2, 2, 3, 3, personne, personne, typeCM);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 1, 1, 1, 1, personne, personne, typeTD);
		param.setCorps(corps);
		param = EOParamPopHetd.createEOParamPopHetd(ec, new NSTimestamp(), NSTimestamp.DistantPast, NSTimestamp.DistantPast, 3, 1, 2, 1, personne, personne, typeTP);
		param.setCorps(corps);
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre.createEOPecheParametre(ec, "", now, now, "hcomp.methode.defaut", "3", null, null);

//		MoteurDeCalcul moteur = new MoteurDeCalcul(ec, corps);
		MoteurDeCalcul moteur= new MoteurDeCalcul(ec, corps, 2013, true);
		
		double serviceDu = 384.0d;
		EOService service = EOService.creerEtInitialiser(ec);
		{
			EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 1, "APTP1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTP);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(400.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 3, "APTD1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeTD);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(10.0d);
				serviceDetail.setService(service);
			}
			serviceDetail = EOServiceDetail.creerEtInitialiser(ec);
			{
				EOAP ap = EOAP.createSco_AP(ec, 5, "APCM1", EOComposantInfoVersion.create(ec, 2013, null));
				ap.setTypeAP(typeCM);
				serviceDetail.setComposantAP(ap);
				serviceDetail.setHeuresPrevues(10.0d);
				serviceDetail.setService(service);
			}		
		}
		
		double hcompPrevues = moteur.getHeuresComplementairesPrevues(serviceDu, service);
		double serviceHETDPrevu = moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		double hcompRealisees = moteur.getHeuresComplementairesRealisees(serviceDu, service);
		double serviceHETDRealise = moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		
		assertEquals(35.66d, hcompPrevues, 0.02d);
		assertEquals(419.667d, serviceHETDPrevu, 0.02d);
		assertEquals(0.0d, hcompRealisees, 0.00d);
		assertEquals(0.0d, serviceHETDRealise, 0.00d);
		
		/* Calcul du service souhaité en HETD */
		EOFicheVoeux voeux = eoFicheVoeux(
				eoVoeuxDetail(
						eoAP(1, "AP1", typeCM, 10.0f)
				),
				eoVoeuxDetail(
						eoAP(2, "AP2", typeTP, 400.0f)
				),
				eoVoeuxDetail(
						eoAP(3, "AP3", typeTD, 10.0f)
				)
		);

		double serviceHETDSouhaite = moteur.getServiceHETDSouhaite(serviceDu, voeux);
		double hcompSouhaites = serviceHETDSouhaite - serviceDu;
		
		assertEquals(419.667d, serviceHETDSouhaite, 0.00d);
		assertEquals(35.67d, hcompSouhaites, 0.02d);
	}
}
