Calcul des heures complémentaires d'enseignement à l'Université de La Rochelle

Scenario: Calcul des heures complémentaire d'un enseignant chercheur à La Rochelle

Etant donné que je suis un enseignant chercheur
Quand je fais 56 heures de CM
Quand je fais 56 heures de TD
Quand je fais 100 heures de TP
Alors j'ai 41.3 heures complémentaires
