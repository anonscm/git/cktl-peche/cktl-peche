package org.cocktail.peche.metier.hce;

import junit.framework.Assert;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class MoteurCalculSteps {
	
	@Given("je suis un $1")
	public void setTypeEnseignant(String typeEnseignant) {
		System.out.println("typeEnseignant: " + typeEnseignant);
	}
	
	@When("je fais $1 heures de $2")
	public void addHours(int nb, String code) {
		System.out.println("nb: " + nb);
		System.out.println("code: " + code);
	}

	@Then("j'ai $1 heures complémentaires")
	public void check(double nbHce) {
		System.out.println("nbHce: " + nbHce);
		Assert.assertTrue(true);
	}
}
