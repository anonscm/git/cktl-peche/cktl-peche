package org.cocktail.peche.metier.hce;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;

public class MoteurCalculHCESteps {
	
	@Given("je fais $1 heures de $2")
	public void addHours(int nb, String code) {
		System.out.println("nb: " + nb);
		System.out.println("code: " + code);
	}

	@Then("j'ai $1 heures complémentaires")
	public void check(double nbHce) {
		System.out.println("nbHce: " + nbHce);
	}
	
}
