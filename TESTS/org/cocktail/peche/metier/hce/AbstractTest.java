package org.cocktail.peche.metier.hce;

import static org.mockito.Mockito.spy;

import java.math.BigDecimal;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mockito;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

public abstract class AbstractTest {

	protected EOTypeAP typeCM;
	protected EOTypeAP typeTP;
	protected EOTypeAP typeTD;

	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils");

	@Before
	public void before() {
		ec.setRetainsRegisteredObjects(true);
		
		typeCM = Mockito.mock(EOTypeAP.class);
		Mockito.when(typeCM.code()).thenReturn(EOTypeAP.TYPECOURS_CODE);
		typeTP = Mockito.mock(EOTypeAP.class);
		Mockito.when(typeTP.code()).thenReturn(EOTypeAP.TYPETP_CODE);
		typeTD = Mockito.mock(EOTypeAP.class);
		Mockito.when(typeTD.code()).thenReturn(EOTypeAP.TYPETD_CODE);
		
		EOPersonne personne = EOPersonne.createEOPersonne(ec, 42, "John Mock", 1, "Good guy");

		NSTimestamp now = new NSTimestamp();
		EOMethodesCalculHComp.createEOMethodesCalculHComp(ec, MethodeDeCalculParProratisation.class.getName(), now, now, 1, 42, 42, "Proratisation", personne, personne);
		EOMethodesCalculHComp.createEOMethodesCalculHComp(ec, MethodeDeCalculParPrioriteCmTdTp.class.getName(), now, now, 2, 42, 42, "Priorité CM/TD/TP", personne, personne);
		EOMethodesCalculHComp.createEOMethodesCalculHComp(ec, MethodeDeCalculParPrioriteTpTdCm.class.getName(), now, now, 3, 42, 42, "Priorité TP/TD/CM", personne, personne);
		EOMethodesCalculHComp.createEOMethodesCalculHComp(ec, MethodeDeCalculParProratisationSurPresentiel.class.getName(), now, now, 4, 42, 42, "Proratisation sur présentiel", personne, personne);
		
		/* ************************************************/
		/*  Circuit de validation pour la fiche de voeux  */
		/* ************************************************/
		/* Application */
		EOGdApplication applicationTestee = new EOGdApplication();
		applicationTestee.setAppStrId("PECHE");
		applicationTestee.setAppLc("Peche");
		ec.insertSavedObject(applicationTestee);
		
		/* Circuit de validation */
		EOCircuitValidation circuitValidation = new EOCircuitValidation();
		circuitValidation.setToGdApplication(applicationTestee);
		circuitValidation.setCodeCircuitValidation("PECHE_VOEUX");
		circuitValidation.setTemoinUtilisable("O");
		ec.insertSavedObject(circuitValidation);
		
		/* Etape initiale */
		EOEtape etapeInitiale = new EOEtape();
		etapeInitiale.setCodeEtape("INITIALE");
		etapeInitiale.setTemoinEtapeInitiale("O");
		ec.insertSavedObject(etapeInitiale);
		etapeInitiale.setToCircuitValidationRelationship(circuitValidation);
	}

	protected EOService eoService(EOServiceDetail... details) {
		EOService mockService = EOService.creerEtInitialiser(ec);
		mockService = spy(mockService);

		for (EOServiceDetail detail : details)
			mockService.addToListeServiceDetails(detail);
		
		return mockService;
	}
	
	protected EOServiceDetail eoServiceDetail(EOAP ap) {
		EOServiceDetail mockServiceDetail = EOServiceDetail.creerEtInitialiser(ec);
		mockServiceDetail.setComposantAP(ap);
		mockServiceDetail.setHeuresPrevues(ap.chargeEnseignementTheorique().valeurHeures().doubleValue());
		
		return mockServiceDetail;
	}
	
	protected EOAP eoAP(Integer id, String libelle, EOTypeAP typeAP, float heures) {
		EOAP mockAP = EOAP.createSco_AP(ec, id, libelle, EOComposantInfoVersion.create(ec, 2013, null));
		mockAP.setTypeAP(typeAP);
		
		EOTypeChargeEnseignement mockTypeChargeEnseignement = EOTypeChargeEnseignement.createSco_TypeChargeEnseignement(ec, "THEORIQUE", "Théorique");
		EOChargeEnseignement mockChargeEnseignement = EOChargeEnseignement.createSco_ChargeEnseignement(ec, new NSTimestamp(), 42, mockTypeChargeEnseignement);
		
		mockChargeEnseignement.setToAPRelationship(mockAP);
		mockChargeEnseignement.setValeurHeures(new BigDecimal(heures));
		
		return mockAP;
	}
	
	protected EOParamPopHetd eoParamPopHetd(EOCorps corps, EOTypeAP typeAP, int numService, int denomService, int numHorsService, int denomHorsService) {
		return eoParamPopHetd(corps, typeAP, numService, denomService, numHorsService, denomHorsService, NSTimestamp.DistantPast, NSTimestamp.DistantFuture);
	}
	
	protected EOParamPopHetd eoParamPopHetd(EOCorps corps, EOTypeAP typeAP, int numService, int denomService, int numHorsService, int denomHorsService, NSTimestamp dateDebut, NSTimestamp dateFin) {
		
		EOParamPopHetd param = EOParamPopHetd.creerEtInitialiser(ec);
		param.setCorps(corps);
		param.setTypeAP(typeAP);
		param.setDateDebut(dateDebut);
		param.setDateFin(dateFin);
		param.setNumerateurService(numService);
		param.setDenominateurService(denomService);
		param.setNumerateurHorsService(numHorsService);
		param.setDenominateurHorsService(denomHorsService);

		return param;
	}
	
	protected EOFicheVoeux eoFicheVoeux(EOFicheVoeuxDetail... details) {
		EOFicheVoeux mockService = EOFicheVoeux.creerEtInitialiser(ec, new Integer(1));
		mockService = spy(mockService);

		for (EOFicheVoeuxDetail detail : details)
			mockService.addToListeFicheVoeuxDetails(detail);
		
		return mockService;
	}
	
	protected EOFicheVoeuxDetail eoVoeuxDetail(EOAP ap) {
		EOFicheVoeuxDetail mockServiceDetail = EOFicheVoeuxDetail.creerEtInitialiser(ec);
		mockServiceDetail.setComposantAP(ap);
		mockServiceDetail.setHeuresSouhaitees(ap.chargeEnseignementTheorique().valeurHeures().doubleValue());
		
		return mockServiceDetail;
	}

}
