package org.cocktail.peche.metier.miseenpaiement;

import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EODefinitionFluxPaiement;
import org.cocktail.peche.entity.EODetailFluxPaiement;
import org.cocktail.peche.entity.EOMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.entity.EOTauxHoraire;
import org.cocktail.peche.entity._EOService;
import org.cocktail.peche.services.PeriodeServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

public class MiseEnPaiementTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche");
	
	private EOParametresFluxPaiement parametresFluxPaiement;
	private EOService serviceVacataire;
	private EORepartService repartServiceVacataire;
	private EOService serviceStatutaire;
	private EORepartService repartServiceStatutaire;
	private EOService serviceStatutairePaiementEnAttente;
	private EORepartService repartServiceStatutairePaiementEnAttente;
	private EOService serviceStatutairePaiementNonPaye;
	private EORepartService repartServiceStatutairePaiementNonPaye;
	private EOService serviceStatutairePaye;
	private EORepartService repartServiceStatutairePaye;
	private EOServiceDetail serviceDetailStatutaire1;
	private EOServiceDetail serviceDetailStatutaire2;
	private EOServiceDetail serviceDetailStatutaire3;
	private EOServiceDetail serviceDetailAPayer3;
	private EOServiceDetail serviceDetailPasAPayer;
	private EOServiceDetail serviceDetailAPayer0;
	private EOServiceDetail serviceDetailAPayer2;
	private EOServiceDetail serviceDetailEnAttenteAPayer5;
	private EOServiceDetail serviceDetailPaiementAPayer6;
	private EOServiceDetail serviceDetailPaiementPaye7;
	private EOServiceDetail serviceDetailPaiementPaye8;
	private EOPaiement paiementNonPaye;
	private EOPaiement paiementPaye;
	private EOPaiement paiementStatutaireNonPaye;
	private EOPaiement paiementStatutairePaye;
	private EOPersonne personne;
	private IPecheParametres pecheParametre;
	private PeriodeServiceImpl periodeService;
	
	private EODetailFluxPaiement detailFp;
	private EODetailFluxPaiement detailFp1;
	private EODetailFluxPaiement detailFp2;
	private EODetailFluxPaiement detailFp3;
	private EODetailFluxPaiement detailFp4;
	private EODetailFluxPaiement detailFp5;
	private EODetailFluxPaiement detailFp6;
	private EODetailFluxPaiement detailFp7;
	private EODetailFluxPaiement detailFp8;
	private EODetailFluxPaiement detailFp9;
	private EODetailFluxPaiement detailFp10;
	private EODetailFluxPaiement detailFp11;
	private EODetailFluxPaiement detailFp12;
	private EODetailFluxPaiement detailFp13;
	private EODetailFluxPaiement detailFp14;
	private EODetailFluxPaiement detailFp15;
	private EODetailFluxPaiement detailFp16;
	
	
	private EODefinitionFluxPaiement definitionFp;
	private EODefinitionFluxPaiement definitionFp1;
	private EODefinitionFluxPaiement definitionFp2;
	private EODefinitionFluxPaiement definitionFp3;
	private EODefinitionFluxPaiement definitionFp4;
	private EODefinitionFluxPaiement definitionFp5;
	private EODefinitionFluxPaiement definitionFp6;
	private EODefinitionFluxPaiement definitionFp7;
	private EODefinitionFluxPaiement definitionFp8;
	private EODefinitionFluxPaiement definitionFp9;
	private EODefinitionFluxPaiement definitionFp10;
	private EODefinitionFluxPaiement definitionFp11;
	private EODefinitionFluxPaiement definitionFp12;
	private EODefinitionFluxPaiement definitionFp13;
	private EODefinitionFluxPaiement definitionFp14;
	private EODefinitionFluxPaiement definitionFp15;
	private EODefinitionFluxPaiement definitionFp16;
	

	@Before
	public void setUp() {
		
		// Paramètres flux de paiement
		parametresFluxPaiement = Mockito.mock(EOParametresFluxPaiement.class);
		Mockito.when(parametresFluxPaiement.templateNomFlux()).thenReturn("paiement_%1$d_%2$04d%3$02d");
		Mockito.when(parametresFluxPaiement.administration()).thenReturn("022");
		Mockito.when(parametresFluxPaiement.departement()).thenReturn("031");
		
		detailFp = Mockito.mock(EODetailFluxPaiement.class);
		detailFp1 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp2 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp3 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp4 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp5 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp6 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp7 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp8 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp9 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp10 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp11 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp12 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp13 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp14 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp15 = Mockito.mock(EODetailFluxPaiement.class);
		detailFp16 = Mockito.mock(EODetailFluxPaiement.class);

		definitionFp = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp1 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp2 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp3 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp4 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp5 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp6 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp7 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp8 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp9 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp10 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp11 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp12 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp13 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp14 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp15 = Mockito.mock(EODefinitionFluxPaiement.class);
		definitionFp16 = Mockito.mock(EODefinitionFluxPaiement.class);
		
		Mockito.when(definitionFp.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_CODE_CHAINE_PAYE.getCodeChamp());
		Mockito.when(definitionFp1.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_CORR.getCodeChamp());
		Mockito.when(definitionFp2.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_MIN.getCodeChamp());
		Mockito.when(definitionFp3.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_ORIG.getCodeChamp());
		Mockito.when(definitionFp4.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_IR_SOLIDARITE.getCodeChamp());
		Mockito.when(definitionFp5.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_SENS.getCodeChamp());
		Mockito.when(definitionFp6.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_MTT.getCodeChamp());
		Mockito.when(definitionFp7.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_TPL_LIB_COMP.getCodeChamp());
		Mockito.when(definitionFp10.codeChamp()).thenReturn(FluxPaiementChampSpecifique.WINPAIE_TPL_NOM_PRENOM.getCodeChamp());
		Mockito.when(definitionFp11.codeChamp()).thenReturn(FluxPaiementChampSpecifique.WINPAIE_OBJET.getCodeChamp());
		Mockito.when(definitionFp14.codeChamp()).thenReturn(FluxPaiementChampSpecifique.WINPAIE_IR_SOLIDARITE.getCodeChamp());
		Mockito.when(definitionFp15.codeChamp()).thenReturn(FluxPaiementChampSpecifique.WINPAIE_IR_SANS_SOLIDARITE.getCodeChamp());
		Mockito.when(definitionFp16.codeChamp()).thenReturn(FluxPaiementChampSpecifique.GIRAFE_NUMERO_REMISE.getCodeChamp());
		
		Mockito.when(detailFp.toDefinitionFluxPaiement()).thenReturn(definitionFp);
		Mockito.when(detailFp.valeurChamp()).thenReturn("PP");
		Mockito.when(detailFp1.toDefinitionFluxPaiement()).thenReturn(definitionFp1);
		Mockito.when(detailFp1.valeurChamp()).thenReturn("DNI");
		Mockito.when(detailFp2.toDefinitionFluxPaiement()).thenReturn(definitionFp2);
		Mockito.when(detailFp2.valeurChamp()).thenReturn("933");
		Mockito.when(detailFp3.toDefinitionFluxPaiement()).thenReturn(definitionFp3);
		Mockito.when(detailFp3.valeurChamp()).thenReturn("1");
		Mockito.when(detailFp4.toDefinitionFluxPaiement()).thenReturn(definitionFp4);
		Mockito.when(detailFp4.valeurChamp()).thenReturn("0331");
		Mockito.when(detailFp5.toDefinitionFluxPaiement()).thenReturn(definitionFp5);
		Mockito.when(detailFp5.valeurChamp()).thenReturn("0");
		Mockito.when(detailFp6.toDefinitionFluxPaiement()).thenReturn(definitionFp6);
		Mockito.when(detailFp6.valeurChamp()).thenReturn("A");
		Mockito.when(detailFp7.toDefinitionFluxPaiement()).thenReturn(definitionFp7);
		Mockito.when(detailFp7.valeurChamp()).thenReturn("%1$1.2f heures à %2$1.2f");
		Mockito.when(detailFp8.toDefinitionFluxPaiement()).thenReturn(definitionFp8);
		Mockito.when(detailFp8.valeurChamp()).thenReturn("CAD");
		Mockito.when(detailFp9.toDefinitionFluxPaiement()).thenReturn(definitionFp9);
		Mockito.when(detailFp9.valeurChamp()).thenReturn("PP01793");
		Mockito.when(detailFp10.toDefinitionFluxPaiement()).thenReturn(definitionFp10);
		Mockito.when(detailFp10.valeurChamp()).thenReturn("%1$s, %2$s");
		Mockito.when(detailFp11.toDefinitionFluxPaiement()).thenReturn(definitionFp11);
		Mockito.when(detailFp11.valeurChamp()).thenReturn(String.format("%-22s", "CRS COMPL"));
		Mockito.when(detailFp12.toDefinitionFluxPaiement()).thenReturn(definitionFp12);
		Mockito.when(detailFp12.valeurChamp()).thenReturn("F10");
		Mockito.when(detailFp13.toDefinitionFluxPaiement()).thenReturn(definitionFp13);
		Mockito.when(detailFp13.valeurChamp()).thenReturn(String.format("%14s", "01"));
		Mockito.when(detailFp14.toDefinitionFluxPaiement()).thenReturn(definitionFp14);
		Mockito.when(detailFp14.valeurChamp()).thenReturn("0204");
		Mockito.when(detailFp15.toDefinitionFluxPaiement()).thenReturn(definitionFp15);
		Mockito.when(detailFp15.valeurChamp()).thenReturn("0331");
		Mockito.when(detailFp16.toDefinitionFluxPaiement()).thenReturn(definitionFp16);
		Mockito.when(detailFp16.valeurChamp()).thenReturn("0001");
		
		when(parametresFluxPaiement.toListeDetailFluxPaiement()).thenAnswer(
			    getAnswerForElements(detailFp, detailFp1, detailFp2, detailFp3, detailFp4, detailFp5, detailFp6
			    		, detailFp7, detailFp8, detailFp9, detailFp10, detailFp11, detailFp12, detailFp13, detailFp14, detailFp15, detailFp16));
		
		//Année universitaire
		pecheParametre = Mockito.mock(PecheParametres.class);
		Mockito.when(pecheParametre.getAnneeUniversitaireEnCours()).thenReturn(2013);
		
		periodeService = (PeriodeServiceImpl)Mockito.mock(PeriodeServiceImpl.class);
		periodeService.setPecheParametres(pecheParametre);
		
		EOPeriode periodeSemestre2 = EOPeriode.createEOPeriode(ec, "Semestre 2", 8, 1, 2, 14, 12, EOPeriode.TYPE_SEMESTRE);
		periodeSemestre2 = Mockito.mock(EOPeriode.class);
		Mockito.when(periodeSemestre2.editingContext()).thenReturn(ec);
		
		EOPeriode periodeSemestre1 = EOPeriode.createEOPeriode(ec, "Semestre 1", 12, 9, 1, 5, 4, EOPeriode.TYPE_SEMESTRE);
		periodeSemestre1 = Mockito.mock(EOPeriode.class);
		Mockito.when(periodeSemestre1.editingContext()).thenReturn(ec);
		
		Mockito.when(periodeService.rechercherSecondSemestre(ec)).thenReturn(periodeSemestre2);
		Mockito.when(periodeService.rechercherPremierSemestre(ec)).thenReturn(periodeSemestre1);
		
		
		NSTimestamp now = new NSTimestamp();
		personne = EOPersonne.createEOPersonne(ec, null, "Nom prénom", 1, "PER");

		//Enseignant vacataire
		EOIndividu individu = EOIndividu.createEOIndividu(ec, now, now, "N", 1, "de Villedon de Neide", 1, "Jean-Christophe", null);
		individu.setIndNoInsee("1920231012211");
		individu.setIndCleInsee(12);

		Calendar dateEnseignant = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		dateEnseignant.clear();
		dateEnseignant.set(2013, Calendar.JANUARY, 1);
		NSTimestamp debutEnseignant= new NSTimestamp(dateEnseignant.getTime());
			
		EOActuelEnseignant enseignantVacataire = EOActuelEnseignant.createEOActuelEnseignant(ec, debutEnseignant,1, "N", "N", individu);
		EOPersonnel personnel = EOPersonnel.creerPersonnel(ec, individu);
		personnel.setNpc("90");
		
		Calendar date = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		date.clear();
		
		date.set(2013, Calendar.SEPTEMBER, 10);
		NSTimestamp debutVacation= new NSTimestamp(date.getTime());
		date.set(2014, Calendar.AUGUST, 28);
		NSTimestamp finVacation = new NSTimestamp(date.getTime());
		
		//Enseignant statutaire 
		EOIndividu individuStatutaire = EOIndividu.createEOIndividu(ec, now, now, "N", 1, "Disney", 1, "Walt", null);
		individuStatutaire.setIndNoInsee("1920231012211");
		individuStatutaire.setIndCleInsee(12);
		EOActuelEnseignant enseignantStatutaire = EOActuelEnseignant.createEOActuelEnseignant(ec, debutEnseignant, 1, "O", "O", individuStatutaire);
		EOPersonnel personnelStatutaire = EOPersonnel.creerPersonnel(ec, individuStatutaire);
		personnelStatutaire.setNpc("90");
		
		//Enseignant statutaire avec paiement non effectué
		individuStatutaire = EOIndividu.createEOIndividu(ec, now, now, "N", 2, "King", 2, "Stephen", null);
		individuStatutaire.setIndNoInsee("1920231012211");
		individuStatutaire.setIndCleInsee(12);
		EOActuelEnseignant enseignantStatutaireNonPaye = EOActuelEnseignant.createEOActuelEnseignant(ec, debutEnseignant, 2, "O", "N", individuStatutaire);
		personnelStatutaire = EOPersonnel.creerPersonnel(ec, individuStatutaire);
		personnelStatutaire.setNpc("90");
		
		//Enseignant statutaire avec paiement effectué
		individuStatutaire = EOIndividu.createEOIndividu(ec, now, now, "N", 3, "Salengro", 3, "Christophe", null);
		individuStatutaire.setIndNoInsee("1920231012211");
		individuStatutaire.setIndCleInsee(12);
		EOActuelEnseignant enseignantStatutairePaye = EOActuelEnseignant.createEOActuelEnseignant(ec, debutEnseignant, 3, "O", "O", individuStatutaire);
		personnelStatutaire = EOPersonnel.creerPersonnel(ec, individuStatutaire);
		personnelStatutaire.setNpc("90");
		
		EOStructure structure = Mockito.mock(EOStructure.class);
		Mockito.when(structure.cStructure()).thenReturn("ETB");

		// AP et type d'AP
		EOTypeAP typeApCm = Mockito.mock(EOTypeAP.class);
		Mockito.when(typeApCm.code()).thenReturn(EOTypeAP.TYPECOURS_CODE);
		EOAP apCm = Mockito.mock(EOAP.class);
		Mockito.when(apCm.typeAP()).thenReturn(typeApCm);
		Mockito.when(apCm.parents()).thenReturn(new ArrayList<IComposant>());
		Mockito.when(apCm.getStructure()).thenReturn(structure);
		EOTypeAP typeApTd = Mockito.mock(EOTypeAP.class);
		Mockito.when(typeApTd.code()).thenReturn(EOTypeAP.TYPETD_CODE);
		EOAP apTd = Mockito.mock(EOAP.class);
		Mockito.when(apTd.typeAP()).thenReturn(typeApTd);
		Mockito.when(apTd.parents()).thenReturn(new ArrayList<IComposant>());
		
		// Périodes
		EOPeriode periodeSeptembre = EOPeriode.createEOPeriode(ec, "Septembre", 9, 9, 1, 1, 1, EOPeriode.TYPE_MOIS);
		EOPeriode periodeNovembre = EOPeriode.createEOPeriode(ec, "Novembre", 10, 10, 1, 3, 3, EOPeriode.TYPE_MOIS);
		
		// Circuits
		EOCircuitValidation circuitValidationVacataire = Mockito.mock(EOCircuitValidation.class);
		Mockito.when(circuitValidationVacataire.codeCircuitValidation()).thenReturn(CircuitPeche.PECHE_VACATAIRE.getCodeCircuit());
		EOEtape etapeVacataire = Mockito.mock(EOEtape.class);
		Mockito.when(etapeVacataire.toCircuitValidation()).thenReturn(circuitValidationVacataire);
		Mockito.when(etapeVacataire.codeEtape()).thenReturn(EtapePeche.VALIDEE.getCodeEtape());
		EODemande demandeVacataire = Mockito.mock(EODemande.class);
		Mockito.when(demandeVacataire.toEtape()).thenReturn(etapeVacataire);
		
		EOCircuitValidation circuitValidationStatutaire = Mockito.mock(EOCircuitValidation.class);
		Mockito.when(circuitValidationStatutaire.codeCircuitValidation()).thenReturn(CircuitPeche.PECHE_STATUTAIRE.getCodeCircuit());
		EOEtape etapeStatutaire = Mockito.mock(EOEtape.class);
		Mockito.when(etapeStatutaire.toCircuitValidation()).thenReturn(circuitValidationStatutaire);
		Mockito.when(etapeStatutaire.codeEtape()).thenReturn(EtapePeche.VALIDEE.getCodeEtape());
		EODemande demandeStatutaire = Mockito.mock(EODemande.class);
		Mockito.when(demandeStatutaire.toEtape()).thenReturn(etapeStatutaire);
		
		// Service d'un vacataire
		serviceVacataire = EOService.creerEtInitialiser(ec);
		serviceVacataire.setAnnee(2013);
		serviceVacataire.setDateCreation(now);
		serviceVacataire.setDateModification(now);		
		serviceVacataire.setTemoinEnseignantGenerique("N");
		serviceVacataire.setTemoinValide("O");
		serviceVacataire.setPersonneCreationRelationship(personne);
		  
		serviceVacataire.setEnseignantRelationship(enseignantVacataire);
		
		repartServiceVacataire = EORepartService.creerNouvelRepartService(ec, serviceVacataire, null, personne);
		repartServiceVacataire.setToDemandeRelationship(demandeVacataire);
				
		//serviceVacataire = Mockito.mock(EOService.class);
		
		// Détail avec à payer
		serviceDetailAPayer3 = EOServiceDetail.createEOServiceDetail(ec, now, now, 10.0d, 4.0d, personne, serviceVacataire);
		serviceDetailAPayer3.setHeuresAPayer(BigDecimal.valueOf(3d));
		serviceDetailAPayer3.setComposantAPRelationship(apTd);
		serviceDetailAPayer3.setToPeriodeRelationship(periodeSeptembre);
		serviceDetailAPayer3.setToRepartServiceRelationship(repartServiceVacataire);
		
		//serviceDetailAPayer3 = Mockito.mock(EOServiceDetail.class);

		// Détail sans a payer
		serviceDetailPasAPayer = EOServiceDetail.createEOServiceDetail(ec, now, now, 10.0d, 4.0d, personne, serviceVacataire);
		serviceDetailPasAPayer.setToPeriodeRelationship(periodeNovembre);
		serviceDetailPasAPayer.setToRepartServiceRelationship(repartServiceVacataire);
		//serviceDetailPasAPayer = Mockito.mock(EOServiceDetail.class);

		// Détail avec à payer de zéro (explicite)
		serviceDetailAPayer0 = EOServiceDetail.createEOServiceDetail(ec, now, now, 10.0d, 4.0d, personne, serviceVacataire);
		serviceDetailAPayer0.setHeuresAPayer(BigDecimal.valueOf(0d));
		serviceDetailAPayer0.setComposantAPRelationship(apTd);
		serviceDetailAPayer0.setToPeriodeRelationship(periodeNovembre);
		serviceDetailAPayer0.setToRepartServiceRelationship(repartServiceVacataire);
		//serviceDetailAPayer0 = Mockito.mock(EOServiceDetail.class);
		
		// Détail avec à payer
		serviceDetailAPayer2 = EOServiceDetail.createEOServiceDetail(ec, now, now, 1.0d, 3.0d, personne, serviceVacataire);
		serviceDetailAPayer2.setHeuresAPayer(BigDecimal.valueOf(2d));
		serviceDetailAPayer2.setComposantAPRelationship(apCm);
		serviceDetailAPayer2.setToPeriodeRelationship(periodeNovembre);
		serviceDetailAPayer2.setToRepartServiceRelationship(repartServiceVacataire);
		//serviceDetailAPayer2 = Mockito.mock(EOServiceDetail.class);

		// Détail avec mise en paiement en attente
		serviceDetailEnAttenteAPayer5 = EOServiceDetail.createEOServiceDetail(ec, now, now, 1.0d, 3.0d, personne, serviceVacataire);
		serviceDetailEnAttenteAPayer5.setHeuresAPayer(BigDecimal.valueOf(5d));
		serviceDetailEnAttenteAPayer5.setComposantAPRelationship(apTd);
		serviceDetailEnAttenteAPayer5.setToPeriodeRelationship(periodeNovembre);
		serviceDetailEnAttenteAPayer5.setToRepartServiceRelationship(repartServiceVacataire);
		//serviceDetailEnAttenteAPayer5 = Mockito.mock(EOServiceDetail.class);
		
		EOMiseEnPaiement miseEnPaiement = EOMiseEnPaiement.creerEtInitialiser(ec, serviceVacataire, BigDecimal.valueOf(5.0d), BigDecimal.valueOf(5.0d), BigDecimal.valueOf(40.91d),  BigDecimal.valueOf(42.96d),personne);
		miseEnPaiement.setToServiceDetailRelationship(serviceDetailEnAttenteAPayer5);
		
		// Détail dans un paiement (non payé)
		serviceDetailPaiementAPayer6 = EOServiceDetail.createEOServiceDetail(ec, now, now, 1.0d, 3.0d, personne, serviceVacataire);
		serviceDetailPaiementAPayer6.setHeuresAPayer(BigDecimal.valueOf(6d));
		serviceDetailPaiementAPayer6.setComposantAPRelationship(apCm);
		serviceDetailPaiementAPayer6.setToPeriodeRelationship(periodeNovembre);
		serviceDetailPaiementAPayer6.setToRepartServiceRelationship(repartServiceVacataire);
		
		//serviceDetailPaiementAPayer6 = Mockito.mock(EOServiceDetail.class);
		
		miseEnPaiement = EOMiseEnPaiement.creerEtInitialiser(ec, serviceVacataire, BigDecimal.valueOf(6.0d), BigDecimal.valueOf(9.0d), BigDecimal.valueOf(40.91d), BigDecimal.valueOf(42.96d),personne);
		miseEnPaiement.setToServiceDetailRelationship(serviceDetailPaiementAPayer6);
		
		paiementNonPaye = EOPaiement.creerEtInitialiser(ec, 1, 8, 2013, personne);
		paiementNonPaye.setDatePaiement(new NSTimestamp(1376539200000l));  //Date du 15/08/2013
		miseEnPaiement.setToPaiementRelationship(paiementNonPaye);
		//paiementNonPaye = Mockito.mock(EOPaiement.class);
		

		// Détail dans un paiement (payé)
		// ******************************
		serviceDetailPaiementPaye7 = EOServiceDetail.createEOServiceDetail(ec, now, now, 1.0d, 3.0d, personne, serviceVacataire);
		serviceDetailPaiementPaye7.setHeuresAPayer(null);
		serviceDetailPaiementPaye7.setHeuresPayees(BigDecimal.valueOf(7d));
		serviceDetailPaiementPaye7.setComposantAPRelationship(apCm);
		serviceDetailPaiementPaye7.setToPeriodeRelationship(periodeNovembre);
		serviceDetailPaiementPaye7.setToRepartServiceRelationship(repartServiceVacataire);
		
		//serviceDetailPaiementPaye7 = Mockito.mock(EOServiceDetail.class);
		
		miseEnPaiement = EOMiseEnPaiement.creerEtInitialiser(ec, serviceVacataire, BigDecimal.valueOf(7.0d), BigDecimal.valueOf(10.5d), BigDecimal.valueOf(40.91d),BigDecimal.valueOf(42.96d), personne);
		miseEnPaiement.setHeuresPayees(BigDecimal.valueOf(7.0d));
		miseEnPaiement.setHeuresPayeesHetd(BigDecimal.valueOf(10.5d));
		miseEnPaiement.setToServiceDetailRelationship(serviceDetailPaiementPaye7);
		
		paiementPaye = EOPaiement.creerEtInitialiser(ec, 2, 8, 2013, personne);
		paiementPaye.setPaye(Constante.OUI);
		paiementPaye.setDatePaiement(new NSTimestamp(1376539200000l));  //Date du 15/08/2013
		miseEnPaiement.setToPaiementRelationship(paiementPaye);
		//paiementPaye = Mockito.mock(EOPaiement.class);

		serviceDetailPaiementPaye8 = EOServiceDetail.createEOServiceDetail(ec, now, now, 2.0d, 4.0d, personne, serviceVacataire);
		serviceDetailPaiementPaye8.setHeuresAPayer(null);
		serviceDetailPaiementPaye8.setHeuresPayees(BigDecimal.valueOf(8d));
		serviceDetailPaiementPaye8.setComposantAPRelationship(apCm);
		serviceDetailPaiementPaye8.setToPeriodeRelationship(periodeSeptembre);
		serviceDetailPaiementPaye8.setToRepartServiceRelationship(repartServiceVacataire);
		
		//serviceDetailPaiementPaye8 = Mockito.mock(EOServiceDetail.class);
		
		miseEnPaiement = EOMiseEnPaiement.creerEtInitialiser(ec, serviceVacataire, BigDecimal.valueOf(8.0d), BigDecimal.valueOf(12.0d), BigDecimal.valueOf(41.97d),BigDecimal.valueOf(42.96d), personne);
		miseEnPaiement.setHeuresPayees(BigDecimal.valueOf(8.0d));
		miseEnPaiement.setHeuresPayeesHetd(BigDecimal.valueOf(12.0d));
		miseEnPaiement.setToServiceDetailRelationship(serviceDetailPaiementPaye8);
		miseEnPaiement.setToPaiementRelationship(paiementPaye);

		// Service d'un statutaire
		serviceStatutaire = EOService.creerEtInitialiser(ec);
		serviceStatutaire.setAnnee(2013);
		serviceStatutaire.setDateCreation(now);
		serviceStatutaire.setDateModification(now);		
		serviceStatutaire.setTemoinEnseignantGenerique("N");
		serviceStatutaire.setTemoinValide("O");
		serviceStatutaire.setPersonneCreationRelationship(personne);
		  
		
		serviceStatutaire.setHeuresAPayer(BigDecimal.valueOf(12.54d));
		serviceStatutaire.setEnseignantRelationship(enseignantStatutaire);
				
		repartServiceStatutaire = EORepartService.creerNouvelRepartService(ec, serviceStatutaire, null, personne);
		repartServiceStatutaire.setToDemandeRelationship(demandeStatutaire);
		
		// Détail du service
		serviceDetailStatutaire1 = EOServiceDetail.createEOServiceDetail(ec, now, now, 10.0d, 10.0d, personne, serviceStatutaire);
		serviceDetailStatutaire1.setComposantAPRelationship(apCm);
		
		serviceDetailStatutaire2 = EOServiceDetail.createEOServiceDetail(ec, now, now, 4.0d, 4.0d, personne, serviceStatutaire);
		serviceDetailStatutaire2.setComposantAPRelationship(apTd);
		
		// Service d'un statutaire en attente de paiement
		serviceStatutairePaiementEnAttente = EOService.creerEtInitialiser(ec);
		serviceStatutairePaiementEnAttente.setAnnee(2013);
		serviceStatutairePaiementEnAttente.setDateCreation(now);
		serviceStatutairePaiementEnAttente.setDateModification(now);		
		serviceStatutairePaiementEnAttente.setTemoinEnseignantGenerique("N");
		serviceStatutairePaiementEnAttente.setTemoinValide("O");
		serviceStatutairePaiementEnAttente.setPersonneCreationRelationship(personne);
		
		serviceStatutairePaiementEnAttente.setHeuresAPayer(BigDecimal.valueOf(15.0d));
		serviceStatutairePaiementEnAttente.setEnseignantRelationship(enseignantStatutairePaye);
		
		repartServiceStatutairePaiementEnAttente = EORepartService.creerNouvelRepartService(ec, serviceStatutairePaiementEnAttente, null, personne);
		repartServiceStatutairePaiementEnAttente.setToDemandeRelationship(demandeStatutaire);
		
		EOMiseEnPaiement miseEnPaiementStatutaire = EOMiseEnPaiement.creerEtInitialiser(ec, serviceStatutairePaiementEnAttente, 
						BigDecimal.valueOf(15.0d), BigDecimal.valueOf(15.0d), BigDecimal.valueOf(40.91d), BigDecimal.valueOf(42.96d), personne);
				
		
		// Service d'un statutaire avec paiement non fait
		serviceStatutairePaiementNonPaye = EOService.creerEtInitialiser(ec);
		serviceStatutairePaiementNonPaye.setAnnee(2013);
		serviceStatutairePaiementNonPaye.setDateCreation(now);
		serviceStatutairePaiementNonPaye.setDateModification(now);		
		serviceStatutairePaiementNonPaye.setTemoinEnseignantGenerique("N");
		serviceStatutairePaiementNonPaye.setTemoinValide("O");
		serviceStatutairePaiementNonPaye.setPersonneCreationRelationship(personne);
		
		serviceStatutairePaiementNonPaye.setHeuresAPayer(BigDecimal.valueOf(15.0d));
		serviceStatutairePaiementNonPaye.setEnseignantRelationship(enseignantStatutaireNonPaye);
		
		repartServiceStatutairePaiementNonPaye = EORepartService.creerNouvelRepartService(ec, serviceStatutairePaiementNonPaye, null, personne);
		repartServiceStatutairePaiementNonPaye.setToDemandeRelationship(demandeStatutaire);
		
		
		// Détail du service
		serviceDetailStatutaire3 = EOServiceDetail.createEOServiceDetail(ec, now, now, 10.0d, 10.0d, personne, serviceStatutairePaiementNonPaye);
		serviceDetailStatutaire3.setComposantAPRelationship(apCm);
		
		miseEnPaiementStatutaire = EOMiseEnPaiement.creerEtInitialiser(ec, serviceStatutairePaiementNonPaye, 
				BigDecimal.valueOf(10.0d), BigDecimal.valueOf(10.0d), BigDecimal.valueOf(40.91d), BigDecimal.valueOf(42.96d), personne);
		
		paiementStatutaireNonPaye = EOPaiement.creerEtInitialiser(ec, 1, 8, 2013, personne);
		paiementStatutaireNonPaye.setDatePaiement(new NSTimestamp(1376539200000l));  //Date du 15/08/2013
		miseEnPaiementStatutaire.setToPaiementRelationship(paiementStatutaireNonPaye);
		
		
		// Service d'un statutaire avec paiement effectué
		serviceStatutairePaye = EOService.creerEtInitialiser(ec);
		serviceStatutairePaye.setAnnee(2013);
		serviceStatutairePaye.setDateCreation(now);
		serviceStatutairePaye.setDateModification(now);		
		serviceStatutairePaye.setTemoinEnseignantGenerique("N");
		serviceStatutairePaye.setTemoinValide("O");
		serviceStatutairePaye.setPersonneCreationRelationship(personne);
		
		
		serviceStatutairePaye.setHeuresAPayer(BigDecimal.valueOf(15.0d));
		serviceStatutairePaye.setEnseignantRelationship(enseignantStatutairePaye);
		
		repartServiceStatutairePaye = EORepartService.creerNouvelRepartService(ec, serviceStatutairePaye, null, personne);
		repartServiceStatutairePaye.setToDemandeRelationship(demandeStatutaire);
		
		
		miseEnPaiementStatutaire = EOMiseEnPaiement.creerEtInitialiser(ec, serviceStatutairePaye, 
				BigDecimal.valueOf(15.0d), BigDecimal.valueOf(15.0d), BigDecimal.valueOf(40.91d), BigDecimal.valueOf(42.96d), personne);
		miseEnPaiementStatutaire.setHeuresPayees(BigDecimal.valueOf(15.0d));
		miseEnPaiementStatutaire.setHeuresPayeesHetd(BigDecimal.valueOf(15d));
		
		paiementStatutairePaye = EOPaiement.creerEtInitialiser(ec, 1, 8, 2013, personne);
		paiementStatutairePaye.setDatePaiement(new NSTimestamp(1376539200000l));  //Date du 15/08/2013
		paiementStatutairePaye.setPaye(Constante.OUI);
		miseEnPaiementStatutaire.setToPaiementRelationship(paiementStatutairePaye);
		
		
		// Taux horaire
		// ************
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.clear();
		
		calendar.set(2013, Calendar.APRIL, 1);
		NSTimestamp dateDebut = new NSTimestamp(calendar.getTime());
		calendar.set(2013, Calendar.SEPTEMBER, 30);
		NSTimestamp dateFin = new NSTimestamp(calendar.getTime());
		
		EOTauxHoraire tauxHoraire = EOTauxHoraire.creerEtInitialiser(ec, dateDebut, BigDecimal.valueOf(40.91), BigDecimal.valueOf(42.96), BigDecimal.valueOf(58.09), personne);
		tauxHoraire.setDateFinApplication(dateFin);
		
		calendar.set(2013, Calendar.OCTOBER, 1);
		dateDebut = new NSTimestamp(calendar.getTime());
		tauxHoraire = EOTauxHoraire.creerEtInitialiser(ec, dateDebut, BigDecimal.valueOf(41.97), BigDecimal.valueOf(43.56), BigDecimal.valueOf(58.87), personne);
	}

	@Test
	public void testMettreEnPaiementVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		BigDecimal totalPaye = BigDecimal.valueOf(0);
		BigDecimal totalAPayer = BigDecimal.valueOf(0);
		
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = serviceVacataire.toListeMisesEnPaiement();
		Assert.assertEquals(4, listeMisesEnPaiement.size());		

		miseEnPaiement.setPecheParametres(pecheParametre);
		miseEnPaiement.setPeriodeService(periodeService);
//		miseEnPaiement.mettreEnPaiement(serviceVacataire, personne);
//		
//		listeMisesEnPaiement = serviceVacataire.toListeMisesEnPaiement();
//		Assert.assertEquals(7, listeMisesEnPaiement.size());
//		
//		for (EOMiseEnPaiement eoMiseEnPaiement : listeMisesEnPaiement) {
//			BigDecimal paye = (eoMiseEnPaiement.heuresPayees() == null ? BigDecimal.valueOf(0) : eoMiseEnPaiement.heuresPayees());
//			totalPaye = totalPaye.add(paye);
//			totalAPayer = totalAPayer.add(eoMiseEnPaiement.heuresAPayer().subtract(paye));
//		}
//		
//		Assert.assertEquals(15d, totalPaye.doubleValue(), 0);
//		Assert.assertEquals(16d, totalAPayer.doubleValue(), 0);
//		
//		miseEnPaiement.annulerMiseEnPaiementEnAttente(serviceVacataire);
//		
//		Assert.assertEquals(3, listeMisesEnPaiement.size());	//serviceDetail 6, 7 et 8	
	}
	
	@Test
	public void testMettreEnPaiementStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = serviceStatutaire.toListeMisesEnPaiement();
		Assert.assertEquals(0, listeMisesEnPaiement.size());	

		miseEnPaiement.setPeriodeService(periodeService);
		miseEnPaiement.setPecheParametres(pecheParametre);
		
		miseEnPaiement.mettreEnPaiement(serviceStatutaire, personne);
		
		Assert.assertEquals(1, listeMisesEnPaiement.size());

		BigDecimal totalPaye = BigDecimal.valueOf(0);
		BigDecimal totalAPayer = BigDecimal.valueOf(0);
		for (EOMiseEnPaiement eoMiseEnPaiement : listeMisesEnPaiement) {
			BigDecimal paye = (eoMiseEnPaiement.heuresPayees() == null ? BigDecimal.valueOf(0) : eoMiseEnPaiement.heuresPayees());
			totalPaye = totalPaye.add(paye);
			totalAPayer = totalAPayer.add(eoMiseEnPaiement.heuresAPayer().subtract(paye));
		}
		
		Assert.assertEquals(0.0d, totalPaye.doubleValue(), 0);
		Assert.assertEquals(12.54d, totalAPayer.doubleValue(), 0);
	}
	
	@Test
	public void testAnnulerMiseEnPaiementEnAttenteVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = serviceVacataire.toListeMisesEnPaiement();
		Assert.assertEquals(4, listeMisesEnPaiement.size());		

		miseEnPaiement.annulerMiseEnPaiementEnAttente(serviceVacataire);
		
		listeMisesEnPaiement = serviceVacataire.toListeMisesEnPaiement();
		Assert.assertEquals(3, listeMisesEnPaiement.size());		
	}
	
	@Test
	public void testAnnulerMiseEnPaiementEnAttenteStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = serviceStatutaire.toListeMisesEnPaiement();
		
		miseEnPaiement.setPeriodeService(periodeService);
		miseEnPaiement.setPecheParametres(pecheParametre);
		
		miseEnPaiement.mettreEnPaiement(serviceStatutaire, personne);
		Assert.assertEquals(1, listeMisesEnPaiement.size());		

		miseEnPaiement.annulerMiseEnPaiementEnAttente(serviceStatutaire);
		
		listeMisesEnPaiement = serviceStatutaire.toListeMisesEnPaiement();
		Assert.assertEquals(0, listeMisesEnPaiement.size());		
	}

	@Test
	public void testCalculerAPayerEnCoursVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		BigDecimal aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceVacataire, null);
		Assert.assertEquals(0.0, aPayerEnCours.doubleValue(), 0.0d);		
		
		//vacataire => mise en paiement à partir des service détails 
		aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceVacataire, serviceDetailAPayer3);
		Assert.assertEquals(0.0, aPayerEnCours.doubleValue(), 0.0d);		

		aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceVacataire, serviceDetailEnAttenteAPayer5);
		Assert.assertEquals(5.0, aPayerEnCours.doubleValue(), 0.0d);		
	
		aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceVacataire, serviceDetailPaiementAPayer6);
		Assert.assertEquals(6.0, aPayerEnCours.doubleValue(), 0.0d);		

		aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceVacataire, serviceDetailPaiementPaye7);
		Assert.assertEquals(0.0, aPayerEnCours.doubleValue(), 0.0d);		
	}
	
	@Test
	public void testCalculerAPayerEnCoursStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		
		//statutaire => mise en paiement à partir de la fiche de service
		BigDecimal aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceStatutairePaiementNonPaye, null);
		Assert.assertEquals(10.0, aPayerEnCours.doubleValue(), 0.0d);		
		
		aPayerEnCours = miseEnPaiement.calculerAPayerEnCours(serviceStatutairePaiementNonPaye, serviceDetailStatutaire3);
		Assert.assertEquals(0.0, aPayerEnCours.doubleValue(), 0.0d);
	}

	@Test
	public void testCalculerResteAPayerVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		BigDecimal resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, null);
		Assert.assertNull("Les heures à payer du service d'un vacataire sont nulles", resteAPayer);
		
		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailAPayer3);
		Assert.assertEquals(3.0, resteAPayer.doubleValue(), 0.0d);

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailAPayer0);
		Assert.assertEquals(0.0, resteAPayer.doubleValue(), 0.0d);

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailPasAPayer);
		Assert.assertNull(resteAPayer);		

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailEnAttenteAPayer5);
		Assert.assertNull(resteAPayer);		
	
		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailPaiementAPayer6);
		Assert.assertNull(resteAPayer);		

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailPaiementPaye7);
		Assert.assertNull(resteAPayer);		
	}
	
	@Test
	public void testCalculerResteAPayerStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		BigDecimal resteAPayer = miseEnPaiement.calculerResteAPayer(serviceStatutairePaiementNonPaye, null);
		Assert.assertEquals(5.0, resteAPayer.doubleValue(), 0.0d);

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceStatutairePaiementNonPaye, serviceDetailStatutaire3);
		Assert.assertNull(resteAPayer);		
	}

	@Test
	public void testAnnulerMiseEnPaiementPaiementVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		boolean exceptionPrevue = false;
		try {
			miseEnPaiement.annulerMiseEnPaiementduPaiement(paiementPaye, serviceVacataire);
		} catch (IllegalArgumentException e) {
			exceptionPrevue = true;
		}
		Assert.assertTrue(exceptionPrevue);
		
		BigDecimal resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailPaiementAPayer6);
		Assert.assertNull(resteAPayer);		
		
		miseEnPaiement.annulerMiseEnPaiementduPaiement(paiementNonPaye, serviceVacataire);

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceVacataire, serviceDetailPaiementAPayer6);
		Assert.assertEquals(6.0, resteAPayer.doubleValue(), 0.0d);		
	}
	
	@Test
	public void testAnnulerMiseEnPaiementPaiementStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		boolean exceptionPrevue = false;
		try {
			miseEnPaiement.annulerMiseEnPaiementduPaiement(paiementStatutairePaye, serviceStatutairePaye);
		} catch (IllegalArgumentException e) {
			exceptionPrevue = true;
		}
		Assert.assertTrue(exceptionPrevue);
		
		BigDecimal resteAPayer = miseEnPaiement.calculerResteAPayer(serviceStatutairePaiementNonPaye, null);
		Assert.assertEquals(5.0, resteAPayer.doubleValue(), 0.0d);		
		
		miseEnPaiement.annulerMiseEnPaiementduPaiement(paiementStatutaireNonPaye, serviceStatutairePaiementNonPaye);

		resteAPayer = miseEnPaiement.calculerResteAPayer(serviceStatutairePaiementNonPaye, null);
		Assert.assertEquals(15.0, resteAPayer.doubleValue(), 0.0d);		
	}

	@Test
	public void testGenererPaiement() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		EOPaiement paiement = miseEnPaiement.genererPaiement(ec, 3, 9, 2013, personne);
		Assert.assertEquals(3, paiement.numeroPaiement().intValue());
		Assert.assertEquals(9, paiement.moisPaiement().intValue());
		Assert.assertEquals(2013, paiement.anneePaiement().intValue());
		Assert.assertEquals(Constante.NON, paiement.paye());
		Assert.assertEquals(2, paiement.toListeMisesEnPaiement().size());
		
		//serviceDetailEnAttenteAPayer5 + serviceStatutairePaiementEnAttente
		double totalNbHeuresEnPaiement = paiement.toListeMisesEnPaiement().get(0).heuresAPayer().doubleValue() 
				+ paiement.toListeMisesEnPaiement().get(1).heuresAPayer().doubleValue();
		
		Assert.assertEquals(20.0, totalNbHeuresEnPaiement, 0.0);
	}

	@Test
	public void testInvaliderPaiement() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		EOPaiement paiement = miseEnPaiement.genererPaiement(ec, 3, 9, 2013, personne);
		Assert.assertEquals(3, paiement.numeroPaiement().intValue());
		Assert.assertEquals(9, paiement.moisPaiement().intValue());
		Assert.assertEquals(2013, paiement.anneePaiement().intValue());
		Assert.assertEquals(Constante.NON, paiement.paye());
		Assert.assertEquals(2, paiement.toListeMisesEnPaiement().size());
		
		//serviceDetailEnAttenteAPayer5 + serviceStatutairePaiementEnAttente
		double totalNbHeuresEnPaiement = paiement.toListeMisesEnPaiement().get(0).heuresAPayer().doubleValue() 
				+ paiement.toListeMisesEnPaiement().get(1).heuresAPayer().doubleValue();
		Assert.assertEquals(20.0, totalNbHeuresEnPaiement, 0.0);
		
		miseEnPaiement.invaliderPaiement(paiement, personne);
		Assert.assertEquals(0, paiement.toListeMisesEnPaiement().size());
	}

	@Test
	public void testInvaliderPaiementPayeVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		boolean exceptionPrevue = false;
		try {
			miseEnPaiement.invaliderPaiement(paiementPaye, personne);
		} catch (IllegalArgumentException e) {
			exceptionPrevue = true;
		}
		Assert.assertTrue(exceptionPrevue);
	}
	
	@Test
	public void testInvaliderPaiementPayeStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		boolean exceptionPrevue = false;
		try {
			miseEnPaiement.invaliderPaiement(paiementStatutairePaye, personne);
		} catch (IllegalArgumentException e) {
			exceptionPrevue = true;
		}
		Assert.assertTrue(exceptionPrevue);
	}

	@Test
	public void testValiderPaiement() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		boolean exceptionPrevue = false;
		try {
			miseEnPaiement.validerPaiement(paiementPaye, personne);
		} catch (IllegalArgumentException e) {
			exceptionPrevue = true;
		}
		Assert.assertTrue(exceptionPrevue);

		//Vacataire
		Assert.assertEquals(6.0, serviceDetailPaiementAPayer6.heuresAPayer().doubleValue(), 0.0); 
		Assert.assertNull(serviceDetailPaiementAPayer6.heuresPayees()); 
		miseEnPaiement.validerPaiement(paiementNonPaye, personne);
		Assert.assertNull(serviceDetailPaiementAPayer6.heuresAPayer()); 
		Assert.assertEquals(6.0, serviceDetailPaiementAPayer6.heuresPayees().doubleValue(), 0.0); 
		
		//Statutaire
		Assert.assertEquals(15.0, serviceStatutairePaiementNonPaye.heuresAPayer().doubleValue(), 0.0); 
		Assert.assertNull(serviceStatutairePaiementNonPaye.heuresPayees()); 
		miseEnPaiement.validerPaiement(paiementStatutaireNonPaye, personne);
		Assert.assertEquals(5.0, serviceStatutairePaiementNonPaye.heuresAPayer().doubleValue(), 0.0); 
		Assert.assertEquals(10.0, serviceStatutairePaiementNonPaye.heuresPayees().doubleValue(), 0.0); 
	}
	
	@Test
	public void testGenererFluxPaiementSurPaiementPayeVacataire() throws Exception {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		Assert.assertEquals(0, paiementPaye.toListeFluxMiseEnPaiement().size());
		
		// Format Girafe
		FluxPaiement fluxPaiement;
		fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementGirafe.class, personne);
		
		Assert.assertEquals(2, paiementPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_2_201308.ds2", fluxPaiement.getNom());
		Assert.assertEquals(241, fluxPaiement.getFlux().length());
		Assert.assertEquals("201308PPDNI0001     2093319202310122111290103310A    0042956        10,50 heures à 40,91               022031 022031    \n201308PPDNI0001     2093319202310122111290103310A    0050364        12,00 heures à 41,97               022031 022031    ", fluxPaiement.getFlux());
		
		// Format Winpaie
	/*	fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementWinpaie.class, personne);
		
		Assert.assertEquals(2, paiementPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_2_201308.xls", fluxPaiement.getNom());
		Assert.assertEquals(409, fluxPaiement.getFlux().length());
		Assert.assertEquals("CAD201308PP017930220311920231012211129020002040A00000042956        CRS COMPL             F10                                                                    DE VILLEDON DE NEIDE, JEAN-CHR            01\nCAD201308PP017930220311920231012211129020002040A00000050364        CRS COMPL             F10                                                                    DE VILLEDON DE NEIDE, JEAN-CHR            01", fluxPaiement.getFlux());*/
	}
	
	@Test
	public void testGenererFluxPaiementSurPaiementPayeStatutaire() throws Exception {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		Assert.assertEquals(0, paiementStatutairePaye.toListeFluxMiseEnPaiement().size());
		
		// Format Girafe
		FluxPaiement fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementStatutairePaye, "N", true, parametresFluxPaiement, FormatFluxPaiementGirafe.class, personne);
		
		Assert.assertEquals(1, paiementStatutairePaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.ds2", fluxPaiement.getNom());
		Assert.assertEquals(120, fluxPaiement.getFlux().length());
		Assert.assertEquals("201308PPDNI0001     2093319202310122111290103310A    0061365        15,00 heures à 40,91               022031 022031    ", fluxPaiement.getFlux());
		
		// Format Winpaie
		/*fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementStatutairePaye, "N", true, parametresFluxPaiement, FormatFluxPaiementWinpaie.class, personne);
		
		Assert.assertEquals(1, paiementStatutairePaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.xls", fluxPaiement.getNom());
		Assert.assertEquals(204, fluxPaiement.getFlux().length());
		Assert.assertEquals("CAD201308PP017930220311920231012211129020002040A00000061365        CRS COMPL             F10                                                                    SALENGRO, CHRISTOPHE                      01", fluxPaiement.getFlux());*/
	}

	@Test
	public void testGenererFluxPaiementSurPaiementNonPayeVacataire() throws Exception {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		Assert.assertEquals(Constante.NON, paiementNonPaye.paye());
		Assert.assertEquals(0, paiementNonPaye.toListeFluxMiseEnPaiement().size());
		
		// Format Girafe
		FluxPaiement fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementNonPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementGirafe.class, personne);
		
		Assert.assertEquals(Constante.OUI, paiementNonPaye.paye());
		Assert.assertEquals(1, paiementNonPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.ds2", fluxPaiement.getNom());
		Assert.assertEquals(120, fluxPaiement.getFlux().length());
		Assert.assertEquals("201308PPDNI0001     2093319202310122111290103310A    0036819        9,00 heures à 40,91                022031 022031    ", fluxPaiement.getFlux());
		
		// Format Winpaie
	/*	fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementNonPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementWinpaie.class, personne);
		
		Assert.assertEquals(1, paiementNonPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.xls", fluxPaiement.getNom());
		Assert.assertEquals(204, fluxPaiement.getFlux().length());
		Assert.assertEquals("CAD201308PP017930220311920231012211129020002040A00000036819        CRS COMPL             F10                                                                    DE VILLEDON DE NEIDE, JEAN-CHR            01", fluxPaiement.getFlux());*/	
	}
	
	@Test
	public void testGenererFluxPaiementSurPaiementNonPayeStatutaire() throws Exception {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();

		Assert.assertEquals(Constante.NON, paiementStatutaireNonPaye.paye());
		Assert.assertEquals(0, paiementStatutaireNonPaye.toListeFluxMiseEnPaiement().size());
		
		// Format Girafe
		FluxPaiement fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementStatutaireNonPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementGirafe.class, personne);
		
		Assert.assertEquals(Constante.OUI, paiementStatutaireNonPaye.paye());
		Assert.assertEquals(1, paiementStatutaireNonPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.ds2", fluxPaiement.getNom());
		Assert.assertEquals(120, fluxPaiement.getFlux().length());
		Assert.assertEquals("201308PPDNI0001     2093319202310122111290103310A    0040910        10,00 heures à 40,91               022031 022031    ", fluxPaiement.getFlux());
		
		// Format Winpaie
		/*fluxPaiement = miseEnPaiement.genererFluxPaiement(paiementStatutaireNonPaye, "N", true, parametresFluxPaiement, FormatFluxPaiementWinpaie.class, personne);
		
		Assert.assertEquals(1, paiementStatutaireNonPaye.toListeFluxMiseEnPaiement().size());
		Assert.assertEquals("paiement_1_201308.xls", fluxPaiement.getNom());
		Assert.assertEquals(204, fluxPaiement.getFlux().length());
		Assert.assertEquals("CAD201308PP017930220311920231012211129020002040A00000040910        CRS COMPL             F10                                                                    KING, STEPHEN                             01", fluxPaiement.getFlux());*/		
	}

	@Test
	public void testRechercherEnseignantsAPayerVacataire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		miseEnPaiement = Mockito.spy(miseEnPaiement);
		Mockito.doReturn(Integer.valueOf(1)).when(miseEnPaiement).genererNumeroPaiement(Mockito.any(EOPaiement.class));
		
		CriteresRecherchePaiement criteres = new CriteresRecherchePaiement();
		criteres.setAnneeUniversitaire(2013);
		EOPeriode periode = Mockito.mock(EOPeriode.class);
		Mockito.when(periode.ordrePeriode()).thenReturn(3);
		criteres.setPeriode(periode);
		criteres.setEnseignantsStatutaire(false);
		criteres.setEnseignantsVacataire(true);
		miseEnPaiement.setPecheParametres(pecheParametre);
		miseEnPaiement.setPeriodeService(periodeService);
//		NSArray<MiseEnPaiementEnseignantDataBean> listeEnseignants = miseEnPaiement.rechercherHeuresAPayer(ec, criteres, personne);
//		
//		Assert.assertEquals(2, listeEnseignants.size());
//		Assert.assertEquals(BigDecimal.valueOf(5.0).setScale(2), listeEnseignants.get(0).getTotalNombreHeuresAPayer().add(listeEnseignants.get(1).getTotalNombreHeuresAPayer()));
//		
//		listeEnseignants.get(1).setBonPourPaiement(false);
//		EOPaiement paiement = miseEnPaiement.genererPaiement(ec, 2013, periode, listeEnseignants, personne);
//		Assert.assertEquals(2, paiement.toListeMisesEnPaiement().size());
	}
	
	@Test
	public void testRechercherEnseignantsAPayerStatutaire() {
		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		miseEnPaiement = Mockito.spy(miseEnPaiement);
		Mockito.doReturn(Integer.valueOf(1)).when(miseEnPaiement).genererNumeroPaiement(Mockito.any(EOPaiement.class));
		
		CriteresRecherchePaiement criteres = new CriteresRecherchePaiement();
		criteres.setAnneeUniversitaire(2013);
		EOPeriode periode = Mockito.mock(EOPeriode.class);
		Mockito.when(periode.editingContext()).thenReturn(ec);
		Mockito.when(periode.ordrePeriode()).thenReturn(14);
		criteres.setPeriode(periode);
		criteres.setEnseignantsStatutaire(true);
		criteres.setEnseignantsVacataire(false);
		
		miseEnPaiement.setPecheParametres(pecheParametre);
		miseEnPaiement.setPeriodeService(periodeService);
		
		NSArray<MiseEnPaiementEnseignantDataBean> listeEnseignants = miseEnPaiement.rechercherHeuresAPayer(ec, criteres, personne);
		
		Assert.assertEquals(3, listeEnseignants.size());
		Assert.assertEquals(BigDecimal.valueOf(12.54), listeEnseignants.get(0).getTotalNombreHeuresAPayer());
		Assert.assertEquals(BigDecimal.valueOf(5.0), listeEnseignants.get(1).getTotalNombreHeuresAPayer());
		Assert.assertEquals(BigDecimal.valueOf(15.0), listeEnseignants.get(2).getTotalNombreHeuresAPayer());
		
		//Si c'est faux, je supprime la mise en paiement
		listeEnseignants.get(0).setBonPourPaiement(false);
		EOPaiement paiement = miseEnPaiement.genererPaiement(ec, 2013, periode, listeEnseignants, personne);
		Assert.assertEquals(2, paiement.toListeMisesEnPaiement().size());
		
		listeEnseignants.get(0).setBonPourPaiement(true);
		 paiement = miseEnPaiement.genererPaiement(ec, 2013, periode, listeEnseignants, personne);
		Assert.assertEquals(3, paiement.toListeMisesEnPaiement().size());	
	}
	
	// Private Helpers
	private Answer<NSArray<EODetailFluxPaiement>> getAnswerForElements(EODetailFluxPaiement... elements) {
		final NSArray<EODetailFluxPaiement> listeElements = new NSArray<EODetailFluxPaiement>(Arrays.asList(elements));
		Answer<NSArray<EODetailFluxPaiement>> answer = new Answer<NSArray<EODetailFluxPaiement>>() {

			public NSArray<EODetailFluxPaiement> answer(InvocationOnMock invocation) throws Throwable {
				return listeElements;
			}
		};
		return answer;
	}
	
	
	
}
