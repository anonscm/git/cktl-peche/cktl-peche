/**
 *
 */
package org.cocktail.peche.entity;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

/**
 * @author Chama LAATIK
 *
 */
public class EOFicheVoeuxDetailTest {

	private EOFicheVoeuxDetail ficheVoeuxDetail;
	private EOAP composantAP;
	private String etablissementExterne;

	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche");

	@Before
	public void setUp() {
		this.ficheVoeuxDetail = EOFicheVoeuxDetail.creerEtInitialiser(ec);
		this.composantAP = mock(EOAP.class);
		this.etablissementExterne = "etablissement connu";
	}


	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#creerEtInitialiser(com.webobjects.eocontrol.EOEditingContext)}.
	 */
	@Test
	public final void testCreerEtInitialiser() {
		assertNotNull(ficheVoeuxDetail);

		assertNull("Les heures souhaitées devraient être à null", ficheVoeuxDetail.heuresSouhaitees());
		assertNull("L'établissement externe devrait être à null", ficheVoeuxDetail.etablissementExterne());
		assertNull("L'enseignement externe devrait être à null", ficheVoeuxDetail.enseignementExterne());
		assertNull("Le commentaire devrait être à null", ficheVoeuxDetail.commentaire());
		assertNull("La date du commentaire devrait être à null", ficheVoeuxDetail.dateCommentaire());
		assertNull("La date de création devrait être à null", ficheVoeuxDetail.dateCreation());
		assertNull("La date de modification devrait être à null", ficheVoeuxDetail.dateModification());

		// Les relations
		assertNull("Le composant AP devrait être à null", ficheVoeuxDetail.composantAP());
		assertNull("Le service devrait être à null", ficheVoeuxDetail.ficheVoeux());
		assertNull("La personne responsable de la création devrait être à null", ficheVoeuxDetail.personneCreation());
		assertNull("La personne responsable de la modification devrait être à null",ficheVoeuxDetail.personneModification());
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#setEtablissementExterne(java.lang.String)}.
	 */
	@Test
	public final void testSetEtablissementExterne() {
		ficheVoeuxDetail.setEtablissementExterne(etablissementExterne);

		assertNotNull("L'établissement externe doit être non null", ficheVoeuxDetail.etablissementExterne());
		assertNull("L'AP doit être null", ficheVoeuxDetail.composantAP());
		assertEquals(etablissementExterne, ficheVoeuxDetail.etablissementExterne());
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#setComposantAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP)}.
	 */
	@Test
	public final void testSetComposantAP() {
		ficheVoeuxDetail.setComposantAP(composantAP);

		assertNotNull("L'AP doit être non null", ficheVoeuxDetail.composantAP());
		assertNull("L'établissement externe doit être null", ficheVoeuxDetail.etablissementExterne());
		assertNull("L'enseignement externe doit être null", ficheVoeuxDetail.enseignementExterne());
		assertEquals(composantAP, ficheVoeuxDetail.composantAP());
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#verifierEntiteNonNull(EOAP, String)}.
	 */
	@Test
	public final void testVerifierEntiteNonNull_KO() {
		Boolean essai = ficheVoeuxDetail.verifierEntiteNonNull(null, null);
		assertFalse("Au moins une des 2 entités doit être non nulles", essai);
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#verifierEntiteNonNull(EOAP, String)}.
	 */
	@Test
	public final void testVerifierEntiteNonNull_APNull() {
		Boolean essai = ficheVoeuxDetail.verifierEntiteNonNull(null, etablissementExterne);
		assertTrue("Si AP null et établissement externe non null alors renvoie vrai", essai);
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOFicheVoeuxDetail#verifierEntiteNonNull(EOAP, String)}.
	 */
	@Test
	public final void testVerifierEntiteNonNull_APNonNull() {
		Boolean essai = ficheVoeuxDetail.verifierEntiteNonNull(composantAP, null);
		assertTrue("Si AP est non null et établissement externe null alors renvoie vrai", essai);
	}

}
