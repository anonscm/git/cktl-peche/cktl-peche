/**
 * 
 */
package org.cocktail.peche.entity;



/**
 * @author Chama LAATIK
 *
 */
public class EOServiceTest1 {
	
	/*private EOService service;
	private EOReh reh;
	private EOElementMaquette elementMaquette;
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils", "FwkCktlPeche");
	
	@Before
	public void setUp() {
		 Circuit de validation 
		EOCircuitValidation circuitValidation = new EOCircuitValidation();
		circuitValidation.setCode("PECHE_PREVISIONNEL");
		ec.insertSavedObject(circuitValidation);
		
		 Etape initiale 
		EOEtape etapeInitiale = new EOEtape();
		etapeInitiale.setCode("INITIALE");
		etapeInitiale.setTemoinEtapeInitiale("O");
		ec.insertSavedObject(etapeInitiale);
		etapeInitiale.setCircuitRelationship(circuitValidation);
		
		this.service = EOService.creerEtInitialiser(ec, 0);
		this.reh = EOReh.creerEtInitialiser(ec);
		this.elementMaquette = EOElementMaquette.creerEtInitialiser(ec);
	}

	*//**
	 * Test method for {@link org.cocktail.peche.entity.EOService#creerEtInitialiser(com.webobjects.eocontrol.EOEditingContext)}.
	 *//*
	@Test
	public void testCreerEtInitialiser() {
		*//** Ajouter message d'erreur **//*
		// Les attributs
		assertNotNull(service);
		assertNull(service.heuresPrevues());
		assertNull(service.heuresRealisees());
		assertNull(service.temoinPaye());
		assertNull(service.temoinValide());
		assertNull(service.commentaire());
		assertNull(service.dateCommentaire());
		assertNull(service.annee());
		
		// Les relations
		assertNull(service.enseignant());
		assertNull(service.elementMaquette());
		assertNull(service.reh());
	}

	*//**
	 * Test method for {@link org.cocktail.peche.entity.EOService#dupliquer(org.cocktail.peche.entity.EOService, com.webobjects.eocontrol.EOEditingContext)}.
	 *//*
	@Test
	public void testDupliquer() {
		//TODO : implémenter
	}

	*//**
	 * Test method for {@link org.cocktail.peche.entity.EOService#setReh(org.cocktail.peche.entity.EOReh)}.
	 *//*
	@Test
	public void testSetReh() {
		service.setReh(reh);
		assertNotNull(service.reh());
		assertNull(service.elementMaquette());
		assertEquals(reh, service.reh());
	}

	*//**
	 * Test method for {@link org.cocktail.peche.entity.EOService#setElementMaquette(org.cocktail.peche.entity.EOElementMaquette)}.
	 *//*
	@Test
	public void testSetElementMaquetteEOElementMaquette() {
		service.setElementMaquette(elementMaquette);
		assertNotNull(service.elementMaquette());
		assertNull(service.reh());
		assertEquals(elementMaquette, service.elementMaquette());
	}

	*//**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOElementMaquette)}.
	 *//*
	@Test
	public void testVerifierEntiteNonNull_KO() {
		Boolean essai = service.verifierEntiteNonNull(null, null);
		Assert.assertFalse(essai);
	}

	*//**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOElementMaquette)}.
	 *//*
	@Test
	public void testVerifierEntiteNonNull_RehNull() {
		Boolean essai = service.verifierEntiteNonNull(null, elementMaquette);
		Assert.assertTrue(essai);
	}
	
	*//**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOElementMaquette)}.
	 *//*
	@Test
	public void testVerifierEntiteNonNull_ElementMaquetteNull() {
		Boolean essai = service.verifierEntiteNonNull(reh, null);
		Assert.assertTrue(essai);
	}*/
}
