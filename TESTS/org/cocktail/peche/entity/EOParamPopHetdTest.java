package org.cocktail.peche.entity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

public class EOParamPopHetdTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils");
	
	@Before
	public void setUp() {
		
	}
	
	@After
	public void tearDown() {
		
	}

	@Test
	public void testCreerEtInitialiser() {
		
		assertNotNull(ec.insertedObjects());
		assertEquals(0, ec.insertedObjects().size());
		
		EOParamPopHetd paramPopHetd = EOParamPopHetd.creerEtInitialiser(ec);
		
		assertNotNull(ec.insertedObjects());
		assertEquals(1, ec.insertedObjects().size());
		assertEquals(paramPopHetd, ec.insertedObjects().get(0));

		// Les attributs
		assertNull("La date de création devrait être à null", paramPopHetd.dateCreation());
		assertNull("La date de début devrait être à null", paramPopHetd.dateDebut());
		assertNull("La date de fin devrait être à null", paramPopHetd.dateFin());
		assertNull("La date de modification devrait être à null", paramPopHetd.dateModification());
		assertNull("Le dénominateur hors service devrait être à null", paramPopHetd.denominateurHorsService());
		assertNull("Le dénominateur du service devrait être à null", paramPopHetd.denominateurService());
		assertNull("Le numérateur hors service devrait être à null", paramPopHetd.numerateurHorsService());
		assertNull("Le numérateur du service devrait être à null", paramPopHetd.numerateurService());
		
		// Les relations
		assertNull("Le corps d'appartenance de l'enseignant devrait être à null", paramPopHetd.corps());
		assertNull("La personne création devrait être à null", paramPopHetd.personneCreation());
		assertNull("La personne modification devrait être à null", paramPopHetd.personneModification());
		assertNull("Le type de contrat de travail devrait être à null", paramPopHetd.typeContratTravail());
		assertNull("Le type horraire associé devrait être à null", paramPopHetd.typeAP());
		
	}

	@Test
	public void testDupliquer() {
		EOParamPopHetd source = EOParamPopHetd.creerEtInitialiser(ec);
		NSTimestamp maintenant = new NSTimestamp(System.currentTimeMillis());
		NSTimestamp hier = maintenant.timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);
		NSTimestamp demain = maintenant.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		
		
		
		source.setDateDebut(hier);
		source.setDateFin(demain);
		source.setDenominateurHorsService(2);
		source.setDenominateurService(3);
		source.setNumerateurHorsService(4);
		source.setNumerateurService(5);
		
		EOCorps corps = EOCorps.creerInstance(ec);
		corps.setLlCorps("Corps pour tests unitaires");
		corps.setLcCorps("CTU");
		source.setCorps(corps);
		
		// TODO : à compléter !
		
		EOParamPopHetd copie = EOParamPopHetd.dupliquer(source, ec);
		
		assertEquals(source.dateDebut(), copie.dateDebut());
		assertEquals(source.dateFin(), copie.dateFin());
		assertEquals(source.denominateurHorsService(), copie.denominateurHorsService());
		assertEquals(source.denominateurService(), copie.denominateurService());
		assertEquals(source.numerateurHorsService(), copie.numerateurHorsService());
		assertEquals(source.numerateurService(), copie.numerateurService());
		assertEquals(source.corps(), copie.corps());
	}
	
	@Test
	public void testSetCorps() {
		EOParamPopHetd paramPopHetd = EOParamPopHetd.creerEtInitialiser(ec);
		EOCorps corps = EOCorps.creerInstance(ec);
		EOTypeContratTravail typeContratTravail = EOTypeContratTravail.creerInstance(ec);
		
		paramPopHetd.setCorps(corps);
		
		assertNotNull(paramPopHetd.corps());
		assertNull(paramPopHetd.typeContratTravail());
		assertEquals(corps, paramPopHetd.corps());
		
		paramPopHetd.setTypeContratTravail(typeContratTravail);
		assertNull(paramPopHetd.corps());
		assertNotNull(paramPopHetd.typeContratTravail());
		assertEquals(typeContratTravail, paramPopHetd.typeContratTravail());
	}
	
	@Test
	public void testLibelleCourtCorpsOuContrat() {
		EOParamPopHetd paramPopHetd = EOParamPopHetd.creerEtInitialiser(ec);
		
		String corpsPourTU = "Corps pour TU";
		EOCorps corps = EOCorps.creerInstance(ec);
		corps.setLcCorps(corpsPourTU);
		
		String typeContratTravailPourTU = "Type contrat travail pour TU";
		EOTypeContratTravail typeContratTravail = EOTypeContratTravail.creerInstance(ec);
		typeContratTravail.setLcTypeContratTrav(typeContratTravailPourTU);
		
		paramPopHetd.setCorps(corps);
		assertEquals(corpsPourTU, paramPopHetd.libelleCourtCorpsOuContrat());
		
		paramPopHetd.setTypeContratTravail(typeContratTravail);
		assertEquals(typeContratTravailPourTU, paramPopHetd.libelleCourtCorpsOuContrat());
	}
}
