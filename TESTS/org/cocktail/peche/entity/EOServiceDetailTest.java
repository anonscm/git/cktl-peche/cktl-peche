package org.cocktail.peche.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

/**
 * @author Chama LAATIK
 *
 */
public class EOServiceDetailTest {

	private EOServiceDetail serviceDetail;
	private EOReh reh;
	private EOAP composantAP;
	private EOStructure structure;

	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils", "FwkCktlPeche");

	@Before
	public void setUp() {
		this.serviceDetail = EOServiceDetail.creerEtInitialiser(ec);

		this.reh = mock(EOReh.class);
		this.composantAP = mock(EOAP.class);
		this.structure = mock(EOStructure.class);
		
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOReh#creerEtInitialiser(com.webobjects.eocontrol.EOEditingContext)}.
	 */
	@Test
	public void testCreerEtInitialiser() {
		// Les attributs
		assertNotNull(serviceDetail);
		
		assertNull("Le commentaire devrait être à null", serviceDetail.commentaire());
		assertNull("La date du commentaire devrait être à null", serviceDetail.dateCommentaire());
		assertNull("La date de création devrait être à null", serviceDetail.dateCreation());
		assertNull("La date de modification devrait être à null", serviceDetail.dateModification());
		
		assertEquals(new Double(0), serviceDetail.heuresPrevues());
		assertEquals(new Double(0), serviceDetail.heuresRealisees());
		assertEquals(null, serviceDetail.heuresAPayer());		
		assertNull("Le témoin de la répartition croisée devrait être à null", serviceDetail.etat());

		// Les relations
		assertNull("Le composant AP devrait être à null", serviceDetail.composantAP());
		assertNull("La personne responsable de la création devrait être à null", serviceDetail.personneCreation());
		assertNull("La personne responsable de la modification devrait être à null",serviceDetail.personneModification());
		assertNull("Le REH devrait être à null", serviceDetail.reh());
		assertNull("Le service devrait être à null", serviceDetail.service());
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOService#setReh(org.cocktail.peche.entity.EOReh)}.
	 */
	@Test
	public void testSetReh() {
		serviceDetail.setReh(reh);
		assertNotNull(serviceDetail.reh());
		assertNull(serviceDetail.composantAP());
		assertEquals(reh, serviceDetail.reh());
	}

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOService#setElementMaquette(org.cocktail.peche.entity.EOElementMaquette)}.
	 */
	@Test
	public void testSetComposantAP() {
		serviceDetail.setComposantAP(composantAP);
		assertNotNull(serviceDetail.composantAP());
		assertNull(serviceDetail.reh());
		assertEquals(composantAP, serviceDetail.composantAP());
	}

	/**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOAP)}.
	*/
	@Test
	public void testVerifierEntiteNonNull_KO() {
		Boolean essai = serviceDetail.verifierEntiteNonNull(null, null);
		assertFalse(essai);
	}

	/**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOAP)}.
	 */
	@Test
	public void testVerifierEntiteNonNull_APNull() {
		Boolean essai = serviceDetail.verifierEntiteNonNull(reh, null);
		assertTrue(essai);
	}

	/**
	 * Test method for {@link EOService#verifierEntiteNonNull(EOReh, EOAP)}.
	 */
	@Test
	public void testVerifierEntiteNonNull_RehNull() {
		Boolean essai = serviceDetail.verifierEntiteNonNull(null, composantAP);
		assertTrue(essai);
	}
	
	/**
	 * Test method for {@link EOService#testSetStructure(EOStructure)}.
	 */
	@Test
	public void testSetStructure() {
		serviceDetail.setToStructureRelationship(structure);
		assertNull(serviceDetail.composantAP());
		assertEquals(structure, serviceDetail.toStructure());
	}
	
	
}
