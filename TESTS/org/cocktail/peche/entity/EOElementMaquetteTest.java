package org.cocktail.peche.entity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

/**
 * @author Chama LAATIK
 *
 */
public class EOElementMaquetteTest {
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils");

	/**
	 * Test method for {@link org.cocktail.peche.entity.EOElementMaquette#creerEtInitialiser(com.webobjects.eocontrol.EOEditingContext)}.
	 */
	@Test
	public void testCreerEtInitialiser() {/*
		assertNotNull(ec.insertedObjects());
		assertEquals(0, ec.insertedObjects().size());
		
		EOElementMaquette elementMaquette = EOElementMaquette.creerEtInitialiser(ec);
		
		assertNotNull(ec.insertedObjects());
		assertEquals(1, ec.insertedObjects().size());
		assertEquals(elementMaquette, ec.insertedObjects().get(0));
		*/
		//Ajouter les notNull sur tous les éléments si ajout/modification des élements
	}

	@Test
	public void testHeuresPrevues() {/*
		EOElementMaquette elementMaquette = EOElementMaquette.creerEtInitialiser(ec);
		
		EOServiceDetail service1 = EOServiceDetail.creerEtInitialiser(ec);
		service1.setElementMaquette(elementMaquette);
		service1.setHeuresPrevues(Double.valueOf(10.0));
		
		EOServiceDetail service2 = EOServiceDetail.creerEtInitialiser(ec);
		service2.setElementMaquette(elementMaquette);
		service2.setHeuresPrevues(Double.valueOf(15.0));
				
		assertEquals(elementMaquette.heuresPrevues(), Double.valueOf(25.0));*/
	}
}
