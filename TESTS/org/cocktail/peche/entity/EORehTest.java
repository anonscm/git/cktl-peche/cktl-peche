/**
 * 
 */
package org.cocktail.peche.entity;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSArray;
import com.wounit.rules.MockEditingContext;

/**
 * @author Chama LAATIK
 *
 */
public class EORehTest {
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche", "FwkCktlPersonne",  "FwkCktlDroitsUtils");
	/**
	 * Test method for {@link org.cocktail.peche.entity.EOReh#creerEtInitialiser(com.webobjects.eocontrol.EOEditingContext)}.
	 */
	@Test
	public void testCreerEtInitialiser() {
		// On vérifie que l'EditingContext est créé et vide
		assertNotNull(ec.insertedObjects());
		assertEquals(0, ec.insertedObjects().size());
		
		EOReh reh = EOReh.creerEtInitialiser(ec);
		
		// On vérifie que l'objet reh a bien été ajouté à l'EditingContext
		assertNotNull(ec.insertedObjects());
		assertEquals(1, ec.insertedObjects().size());
		assertEquals(reh, ec.insertedObjects().get(0));

		// Les attributs
		assertNotNull("L'objet reh devrait être non null", reh);
		assertNull("Le libellé court devrait être à null", reh.libelleCourt());
		assertNull("Le libellé long devrait être à null", reh.libelleLong());
		assertNull("La description devrait être à null", reh.description());
		assertNull("Le commentaire devrait être à null", reh.commentaire());
		assertNull("La date de début devrait être à null", reh.dateDebut());
		assertNull("La date de fin devrait être à null", reh.dateFin());
		assertNull("La date de création devrait être à null", reh.dateCreation());
		assertNull("La date de modification devrait être à null", reh.dateModification());
		
		// Les relations
		assertNull("La personne responsable de la création devrait être à null", reh.personneCreation());
		assertNull("La personne responsable de la modification devrait être à null",reh.personneModification());
		assertEquals(new NSArray<EOService>(), reh.listeServiceDetails());
	}

}
