package org.cocktail.peche.entity;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.peche.outils.RandomHelper;

public class IComposantTestHelper {

	private static IComposantTestHelper INSTANCE;

	private IComposantTestHelper() {
	}

	/**
	 * @return l'instance du singleton.
	 */
	public static IComposantTestHelper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new IComposantTestHelper();
		}
		return INSTANCE;
	}
	
	public IComposant getComposantWithCodeLibelle(){
		IComposant composant = mock(IComposant.class);
		when(composant.code()).thenReturn(RandomHelper.getInstance().getRandomString());
		when(composant.libelle()).thenReturn(RandomHelper.getInstance().getRandomString());
		return composant;
	}
	
	public ITypeComposant getTypeComposant(String nom) {
		ITypeComposant type = mock(ITypeComposant.class);
		when(type.nom()).thenReturn(nom);
		return type;
	}
	
}
