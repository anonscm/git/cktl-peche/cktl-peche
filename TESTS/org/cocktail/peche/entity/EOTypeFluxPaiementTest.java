package org.cocktail.peche.entity;

import org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe;
import org.cocktail.peche.metier.miseenpaiement.IFormatFluxPaiement;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class EOTypeFluxPaiementTest {

	private EOTypeFluxPaiement typeFluxPaiement;
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("Peche");

	@Before
	public void setUp() {
		
		typeFluxPaiement = new EOTypeFluxPaiement();
		typeFluxPaiement.setClasseTypeFlux("org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe");
	}
	
	@Test
	public void testGetClasseTypeFlux() {
		
		Class<? extends IFormatFluxPaiement> typeFluxPaiementClass = typeFluxPaiement.getClasseTypeFlux();
		
		Assert.assertEquals("org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe", typeFluxPaiementClass.getCanonicalName());
		Assert.assertEquals(FormatFluxPaiementGirafe.class, typeFluxPaiementClass);
		
	}
}
