package org.cocktail.peche.components.commun;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Classe de test de CktlPecheBaseComponentCtrl
 * @author Chama LAATIK
 *
 */
public class CktlPecheBaseComponentCtrlTest {

	private CktlPecheBaseComponentCtrl controleur;

	@Before
	public void setUp() {
		controleur = new CktlPecheBaseComponentCtrl();
	}

	@Test
	public final void testGetTemoinRepartCroiseeEtatInconnu() {
		String etatTemoin = "";

		assertEquals("si l'état est inconnu alors doit renvoyer une chaine vide, ",etatTemoin, controleur.getEtatRepartition("etatInconnu"));
	}

	@Test
	public final void testGetTemoinRepartCroiseeEtatAttente() {
		String etatTemoin = Constante.ETAT_ATTENTE;

		assertEquals("si l'état est 'E' alors doit renvoyer 'En attente'", etatTemoin, controleur.getEtatRepartition(Constante.ATTENTE));
	}

	@Test
	public final void testGetTemoinRepartCroiseeEtatAccepte() {
		String etatTemoin = Constante.ETAT_ACCEPTE;

		assertEquals("si l'état est 'A' alors doit renvoyer 'Accepté'", etatTemoin, controleur.getEtatRepartition(Constante.ACCEPTE));
	}

	@Test
	public final void testGetTemoinRepartCroiseeEtatRefuse() {
		String etatTemoin = Constante.ETAT_REFUSE;

		assertEquals("si l'état est 'R' alors doit renvoyer 'Refusé'", etatTemoin, controleur.getEtatRepartition(Constante.REFUSE));
	}
	
}
