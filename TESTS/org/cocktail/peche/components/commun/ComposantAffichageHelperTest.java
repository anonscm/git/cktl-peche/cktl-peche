package org.cocktail.peche.components.commun;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeAP;
import org.cocktail.peche.entity.IComposantTestHelper;
import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Lists;



public class ComposantAffichageHelperTest {

	private static final String LIBELLE = "libelle";
	private static final String CODE = "code";

	@Test
	public void affichageCodeShouldHandleNullParams() throws Exception {
		String code = ComposantAffichageHelper.getInstance().affichageCode(null);		
		Assert.assertEquals(ComposantAffichageHelper.LIBELLE_CODE_NULL, code);
	}

	@Test
	public void affichageCodeShouldHandleEmptyParams() throws Exception {
		String code = ComposantAffichageHelper.getInstance().affichageCode(mock(IComposant.class));		
		Assert.assertEquals(ComposantAffichageHelper.LIBELLE_CODE_NULL, code);
	}

	@Test
	public void affichageCodeShouldReturnCode() throws Exception {
		IComposant composant = mock(IComposant.class);
		when(composant.code()).thenReturn(CODE);
		String code = ComposantAffichageHelper.getInstance().affichageCode(composant);		
		Assert.assertEquals(CODE, code);
	}

	@Test
	public void affichageCodeShouldReturnLibelleWhenComposantPeche() throws Exception {
		IComposant composant = mock(IComposant.class);
		when(composant.libelle()).thenReturn(LIBELLE);
		when(composant.tagApplication()).thenReturn(Constante.APP_PECHE);
		String code = ComposantAffichageHelper.getInstance().affichageCode(composant);		
		Assert.assertEquals(LIBELLE, code);
	}

	
	@Test
	public void affichageEnseignementShouldHandleNullParameters() throws Exception {
		ComposantAffichageHelper.getInstance().affichageEnseignementParent(null);
	}

	@Test
	public void affichageEnseignementShouldHandleEmptyParameters() throws Exception {
		ComposantAffichageHelper.getInstance().affichageEnseignementParent(mock(IComposant.class));
	}

	@Test
	public void affichageEnseignementShouldReturnLibelle() throws Exception {
		IComposant uEFlottante = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		IComposant parent = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		when(parent.tagApplication()).thenReturn(Constante.APP_PECHE);
		when(uEFlottante.parents()).thenReturn(Lists.newArrayList(parent));
		String affichageEnseignement = ComposantAffichageHelper.getInstance().affichageEnseignementParent(uEFlottante);
		Assert.assertEquals(affichageEnseignement, parent.libelle());
	}

	@Test
	public void affichageEnseignementShouldReturnCodeLibelle() throws Exception {
		IComposant uEFlottante = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		IComposant parent = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		when(uEFlottante.parents()).thenReturn(Lists.newArrayList(parent));
		String affichageEnseignement = ComposantAffichageHelper.getInstance().affichageEnseignementParent(uEFlottante);
		Assert.assertTrue(affichageEnseignement.contains(parent.libelle()));
		Assert.assertTrue(affichageEnseignement.contains(parent.code()));
	}

	@Test
	public void affichageTypeShouldHandleNullParam() throws Exception {
		ComposantAffichageHelper.getInstance().affichageType(null);
	}

	@Test
	public void affichageTypeShouldHandleEmptyParam() throws Exception {
		ComposantAffichageHelper.getInstance().affichageType(mock(IAP.class));
	}

	@Test
	public void affichageTypeShouldHandleSimpleParam() throws Exception {
		IAP iap = mock(IAP.class);
		ITypeAP type = mock(ITypeAP.class);
		when(type.code()).thenReturn(CODE);
		when(iap.typeAP()).thenReturn(type);
		String result = ComposantAffichageHelper.getInstance().affichageType(iap);
		Assert.assertEquals(CODE, result);
	}

}
