package org.cocktail.peche.components.controlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.peche.entity.IComposantTestHelper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


/**
 * Classe de test pour {@link OffreFormationCtrl}.
 * @author Equipe GRH
 *
 */
public class OffreFormationCtrlTest {

	private static final String LIBELLE_STRUCTURE = "LibelleStructure";
	private OffreFormationCtrl controlleur;
	private IComposant composant;

	@Before
	public void init(){
		controlleur = new OffreFormationCtrl(){
			@Override
			protected String formaterLibelleListeStructuresComposant(IComposant composant) {
				return LIBELLE_STRUCTURE;
			}
		};
		composant = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
	}
	
	@Test
	public void formaterLibelleNoeudShouldHandleNullParams() throws Exception {
		controlleur.formaterLibelleNoeud(null);
	}

	@Test
	public void formaterLibelleNoeudShouldHandleEmptyParams() throws Exception {
		controlleur.formaterLibelleNoeud(mock(IComposant.class));
	}

	@Test
	public void formaterLibelleNoeudShouldReturnLibelleWhenNoType() throws Exception {
		String result = controlleur.formaterLibelleNoeud(composant);
		Assert.assertEquals(composant.libelle(), result);
	}

	@Test
	public void formaterLibelleNoeudShouldReturnCodeLibelleWhenUE() throws Exception {
		ITypeComposant typeComposant = IComposantTestHelper.getInstance().getTypeComposant(EOTypeComposant.TYPEUE_NOM);
		when(composant.typeComposant()).thenReturn(typeComposant);
		String result = controlleur.formaterLibelleNoeud(composant);
		Assert.assertTrue(result.contains(composant.code()));
		Assert.assertTrue(result.contains(composant.libelle()));
	}
	
	@Test
	public void formaterLibelleNoeudShouldReturnCodeLibelleWhenUEMutualisee() throws Exception {
		ITypeComposant typeComposant = IComposantTestHelper.getInstance().getTypeComposant(EOTypeComposant.TYPEUE_NOM);
		when(composant.typeComposant()).thenReturn(typeComposant);
		when(composant.isMutualise()).thenReturn(true);
		String result = controlleur.formaterLibelleNoeud(composant);
		Assert.assertTrue("libelle " + result, result.contains(OffreFormationCtrl.LIBELLE_MUTUALISE));
	}

	@Test
	public void formaterLibelleNoeudShouldReturnOnlyLibelleWhenDiplome() throws Exception {
		ITypeComposant typeComposant = IComposantTestHelper.getInstance().getTypeComposant(EOTypeComposant.TYPEDIPLOME_NOM);
		when(composant.typeComposant()).thenReturn(typeComposant);
		String result = controlleur.formaterLibelleNoeud(composant);
		Assert.assertFalse(result.contains(composant.code()));
		Assert.assertTrue(result.contains(composant.libelle()));
	}
	
	@Test
	public void formaterLibelleNoeudShouldReturnOnlyLibelleAndStructureWhenDiplomeWithUeFLottante() throws Exception {
		ITypeComposant typeComposant = IComposantTestHelper.getInstance().getTypeComposant(EOTypeComposant.TYPEDIPLOME_NOM);
		when(composant.typeComposant()).thenReturn(typeComposant);
		when(composant.tagApplication()).thenReturn("");
		String result = controlleur.formaterLibelleNoeud(composant);
		Assert.assertFalse(result.contains(composant.code()));
		Assert.assertTrue(result.contains(composant.libelle()));
		Assert.assertTrue(result.contains(LIBELLE_STRUCTURE));
	}
	
	
}
