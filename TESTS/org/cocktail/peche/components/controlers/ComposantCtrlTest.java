package org.cocktail.peche.components.controlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.IComposantTestHelper;
import org.junit.Assert;
import org.junit.Test;

import com.google.common.collect.Lists;


/**
 * Classe de test de la classe {@link ComposantCtrl}.
 * @author anthony
 *
 */
public class ComposantCtrlTest {

	@Test
	public void getParentAPEOAPShouldHandleNullParams() throws Exception {
		IComposant composantAvecParentNull = mock(IComposant.class);
		IComposant parentAP = ComposantCtrlSingleton.getInstance().getParentAP(composantAvecParentNull);
		Assert.assertNull(parentAP);
	}

	
	@Test
	public void getParentAPEOAPShouldHandleEmptyParams() throws Exception {
		IComposant composantAvecParentsVide = mock(IComposant.class);
		when(composantAvecParentsVide.parents()).thenReturn(new ArrayList<IComposant>());
		IComposant parentAP = ComposantCtrlSingleton.getInstance().getParentAP(composantAvecParentsVide);
		Assert.assertNull(parentAP);
	}

	@Test
	public void getParentAPEOAPShouldReturnFirstParent() throws Exception {
		IComposant parent1 = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		IComposant parent2 = IComposantTestHelper.getInstance().getComposantWithCodeLibelle();
		IComposant composantAvecParents = mock(IComposant.class);;
		ArrayList<IComposant> parents = new ArrayList<IComposant>();
		parents.add(parent1);
		parents.add(parent2);
		when(composantAvecParents.parents()).thenReturn(parents);
		IComposant parentAP = ComposantCtrlSingleton.getInstance().getParentAP(composantAvecParents);
		Assert.assertNotNull(parentAP);
		Assert.assertEquals(parentAP.libelle(), parent1.libelle());
	}
	
	
	

	@Test
	public void isParentUeFlottanteShouldHandleNullParameters() throws Exception {
		ComposantCtrlSingleton.getInstance().isParentUeFlottante(null);
	}

	@Test
	public void isParentUeFlottanteShouldHandleEmptyParameters() throws Exception {
		IAP ap = mock(IAP.class);
		ComposantCtrlSingleton.getInstance().isParentUeFlottante(ap);
	}

	@Test
	public void isParentUeFlottanteShouldReturnTrueWhenTheParentIsUEFlottante() throws Exception {
		IAP ap = mock(IAP.class);
		IComposant parentWithTagPeche = mock(IComposant.class);
		when(parentWithTagPeche.tagApplication()).thenReturn(Constante.APP_PECHE);
		when(ap.parents()).thenReturn(Lists.newArrayList(parentWithTagPeche));
		ComposantCtrl instance = new ComposantCtrl();
		Assert.assertTrue(instance.isParentUeFlottante(ap));
	}

	
}
