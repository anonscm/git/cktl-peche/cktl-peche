package org.cocktail.peche.components;

import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.components.commun.PecheComponent;

import com.webobjects.appserver.WOContext;

/**
 * Composant d'entrée dans l'application.
 * @author Yannick Mauray
 *
 */
public class Accueil extends PecheComponent {

	private static final long serialVersionUID = -6886737030010138904L;
	
	private Boolean isOpenFenetreException = Boolean.FALSE;

	/**
	 * Constructeur.
	 * @param context le context WebObjects.
	 */
	public Accueil(WOContext context) {
        super(context);
        Object annee = getPecheSession().objectForKey("NouvelleAnneeUniv");
        if ((annee != null) && (getPecheSession().getPecheParametres() != null)) {        			
        	getPecheSession().getPecheParametres().setAnnee(Integer.valueOf((String) annee));
        	if (getPecheSession().getApplicationUser() != null) {
        		PecheApplicationUser appUser = new PecheApplicationUser(session().defaultEditingContext(), 
        				Integer.valueOf(getPecheSession().getApplicationUser().getPersId().intValue()),
        				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        		if (appUser != null) {
        			getPecheSession().setApplicationUser(appUser);        			
        		}
        	}
        	getPecheSession().removeObjectForKey("NouvelleAnneeUniv");
        }
        //getPecheSession().autorisationDebugList();
        //getPecheSession().getApplicationUser().debugListStructures();
	}

	/**
	 * @return the isOpenFenetreException
	 */
	public Boolean isOpenFenetreException() {
		return isOpenFenetreException;
	}


	/**
	 * @param isOpenFenetreException the isOpenFenetreException to set
	 */
	public void setIsOpenFenetreException(Boolean isOpenFenetreException) {
		this.isOpenFenetreException = isOpenFenetreException;
	}
	
	public String getSrcImage() {
		return "images/Peche.png";
	}
}