package org.cocktail.peche.components.miseenpaiement;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;
import org.cocktail.peche.metier.miseenpaiement.FluxPaiement;
import org.cocktail.peche.metier.miseenpaiement.FluxXmlBordereauPaiement;
import org.cocktail.peche.metier.miseenpaiement.FluxXmlJustificatifPaiementStatutaires;
import org.cocktail.peche.metier.miseenpaiement.FluxXmlJustificatifPaiementVacataires;
import org.cocktail.peche.metier.miseenpaiement.GrouperMiseEnPaiementPourPageDetailPaiement;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiement;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiementDataBean;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiementEnseignantDataBean;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;

/**
 * Classe représentant le détail d'une mise en paiement
 */
public class DetailMEP extends CktlPecheTableComponent<EOPaiement> {

	private static final long serialVersionUID = 6135431369338361341L;

	private EOPaiement editedObject;
	private GenerateurPDFCtrl generateurPDFCtrl;

	private WODisplayGroup displayGroupMEP;
	private MiseEnPaiementDataBean currentItemMEP;

	public static final String MOIS_EN_COURS = "mois en cours";
	public static final String MOIS_SUIVANT = "mois suivant";

	private String moisPaiement;

	private String cotisationSolidarite;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public DetailMEP(WOContext context) {
		super(context);
	}

	/**
	 * Annule la mise en paiement
	 * @return page de la liste des paiements
	 */
	public WOActionResults annulerPaiement() {
		MiseEnPaiement.getInstance().invaliderPaiement(editedObject, personneConnecte());

		try {
			edc().saveChanges();
			return pageWithName(ListeMEP.class.getName());
		} catch (ValidationException e) {
			edc().revert();
			throw e;
		}
	}

	/**
	 * @return la liste des mois de paiement
	 */

	public NSDictionary<String, String> listeMois() {
		NSDictionary<String, String> liste = new NSMutableDictionary<String, String>();
		liste.takeValueForKey(" Mois en cours ", MOIS_EN_COURS);
		liste.takeValueForKey(" Mois suivant ", MOIS_SUIVANT);		
		return liste;
	}


	/**
	 * Génération du bordereau
	 * @return null
	 */
	public WOActionResults genererBordereau() {
		//génération du xml
		FluxXmlBordereauPaiement flux = new FluxXmlBordereauPaiement();

		if (editedObject.dateBordereau() == null) {
			editedObject.setDateBordereau(new NSTimestamp());
		}

		String fluxXml = flux.getFlux(editedObject);

		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

		Calendar date = Calendar.getInstance();
		date.setTime(editedObject.datePaiement());

		generateurPDFCtrl.setReportFilename("bordereau_" + editedObject.numeroPaiement().toString() + "_" + date.get(Calendar.YEAR) + "" + (date.get(Calendar.MONTH) + 1) + ".pdf");
		if (editedObject.isPaiementForStatutaires(getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			generateurPDFCtrl.genererBordereauPaiment("Reports/bordereauStatutaire/bordereau_stat.jasper", true);
		} else {
			generateurPDFCtrl.genererBordereauPaiment("Reports/bordereauVacataire/bordereau_vacat.jasper", false);
		}

		edc().saveChanges();

		AjaxUpdateContainer.updateContainerWithID(detailPaiementUpdateContainerId(), context());
		return null;
	}

	/**
	 * Génération du Justificatif
	 * @return null
	 */
	public WOActionResults genererJustificatif() {

		if (editedObject.isPaiementForStatutaires(getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			//génération du xml
			FluxXmlJustificatifPaiementStatutaires flux = new FluxXmlJustificatifPaiementStatutaires(edc());

			String fluxXml = flux.getFlux(editedObject);

			//génération du pdf
			generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

			Calendar date = Calendar.getInstance();
			date.setTime(editedObject.datePaiement());

			generateurPDFCtrl.setReportFilename("justificatif_" + editedObject.numeroPaiement().toString() + "_" + date.get(Calendar.YEAR) + "" + (date.get(Calendar.MONTH) + 1) + ".pdf");
			generateurPDFCtrl.genererJustificatifPaimentStatutaires();		

		} else {
			//génération du xml
			FluxXmlJustificatifPaiementVacataires flux = new FluxXmlJustificatifPaiementVacataires(edc());
			String fluxXml = flux.getFlux(editedObject);

			//génération du pdf
			generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

			Calendar date = Calendar.getInstance();
			date.setTime(editedObject.datePaiement());

			generateurPDFCtrl.setReportFilename("justificatif_" + editedObject.numeroPaiement().toString() + "_" + date.get(Calendar.YEAR) + "" + (date.get(Calendar.MONTH) + 1) + ".pdf");
			generateurPDFCtrl.genererJustificatifPaimentVacataires();		
		}

		//edc().saveChanges();

		//AjaxUpdateContainer.updateContainerWithID(detailPaiementUpdateContainerId(), context());
		return null;
	}

	/**
	 * Méthode pour générer un arrêté
	 */
	public void genererArrete() {

	}

	/**
	 * @return génération du flux de paiement
	 */
	public WOActionResults genererFluxPaiement() {

		if (validerGenerationFluxPaiement()) {			
			try {
				MiseEnPaiement.getInstance().genererFluxPaiement(editedObject, isMoisSuivantPourTG(), estCotisationSolidarite(),
						EOParametresFluxPaiement.getParametresPourDatePaiement(edc(), editedObject.datePaiement()), personneConnecte());
				edc().saveChanges();
			} catch (Exception e) {
				e.printStackTrace();
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, e.getMessage());
				edc().invalidateAllObjects();
				edc().revert();			
			}			
		}

		return null;
	}

	/**
	 * 
	 * @return
	 */
	private boolean validerGenerationFluxPaiement() {
		NSTimestamp datePaiement = editedObject.datePaiement();
		EOParametresFluxPaiement parametres = EOParametresFluxPaiement.getParametresPourDatePaiement(edc(), datePaiement);
		if (parametres == null) {
			getPecheSession().addSimpleErrorMessage(
					"Erreur",
					Constante.PARAM_FLUX_PAIEMENT_INEXISTANT);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @return génération du lien pour récupérer le fichier "flux de paiement" à télécharger
	 */
	public WOActionResults genererLienFluxPaiement() {
		if (isPaiementEffectue()) {
			try {
				FluxPaiement fluxPaiementGenere = MiseEnPaiement.getInstance().genererFluxPaiement(editedObject, isMoisSuivantPourTG(), estCotisationSolidarite(),
						EOParametresFluxPaiement.getParametresPourDatePaiement(edc(), editedObject.datePaiement()), personneConnecte());

				edc().revert();		

				if (!fluxPaiementGenere.isFormatExcel()) {
					CktlDataResponse reponse = new CktlDataResponse();
					reponse.setHeader("text/plain", "content-type");
					reponse.setHeader(String.valueOf(fluxPaiementGenere.getFlux().length()), "Content-Length");
					reponse.setFileName(fluxPaiementGenere.getNom());
					reponse.setContent(fluxPaiementGenere.getFlux());
					return reponse;
				} else {

					CktlDataResponse reponse = new CktlDataResponse();
					reponse.setHeader("maxage=1", "Cache-Control");
					reponse.setHeader("public", "Pragma");
					reponse.setFileName(fluxPaiementGenere.getNom());
					HSSFWorkbook wb = fluxPaiementGenere.getFluxFormatExcel();
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					wb.write(os);
					byte[] bytes = os.toByteArray();
					reponse.setContent(bytes, "application/xls");					
					return reponse;
				}


			} catch (Exception e) {
				e.printStackTrace();
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, e.getMessage());				
			}		
		}
		return null;
	}

	/**
	 * enregistrer le libellé de paiement
	 */
	public void enregistrerLibellePaiement() {
		try {
			this.editedObject.majDonnesAuditModification(personneConnecte());
			edc().saveChanges();
			getPecheSession().addSimpleInfoMessage(message(Messages.DETAIL_MEP_MESSAGE_LIBELLE_SAUVEGARDE_TITRE), 
					message(Messages.DETAIL_MEP_MESSAGE_LIBELLE_SAUVEGARDE_DETAIL));
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}

	/**
	 * @return displayGroup
	 */
	public WODisplayGroup getDisplayGroupMEP() {
		//On fabrique un nouveau displayGroup
		displayGroupMEP = new ERXDisplayGroup<MiseEnPaiementEnseignantDataBean>();
		EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(
				MiseEnPaiementEnseignantDataBean.class), edc());
		//On lui passe la liste du groupage de mise en paiement
		dataSource.setArray(getListeGroupageMEP());

		displayGroupMEP.setDataSource(dataSource);
		displayGroupMEP.setDelegate(this);

		displayGroupMEP.setSelectsFirstObjectAfterFetch(true);
		displayGroupMEP.fetch();		

		return displayGroupMEP;
	}

	/**
	 * @return liste du groupage des mises en paiement pour la liste des enseignants
	 */
	public NSArray<MiseEnPaiementDataBean> getListeGroupageMEP() {
		return new NSArray<MiseEnPaiementDataBean>(MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(
				this.editedObject, new GrouperMiseEnPaiementPourPageDetailPaiement()));
	}

	/**
	 * @return l'année universitaire
	 */
	public String getAnneeUniversitaire() {
		return this.editedObject.anneePaiement() + "/" + (this.editedObject.anneePaiement() + 1);
	}

	/**
	 * Est-ce que le paiement n'est pas un paiement via Papaye?
	 * @return True/False
	 */
	public boolean isNonPaiementPapaye() {
		NSTimestamp datePaiement = editedObject.datePaiement();
		EOParametresFluxPaiement parametres = EOParametresFluxPaiement.getParametresPourDatePaiement(edc(), datePaiement);
		if (parametres.isFluxPapaye()) {
			return false;
		}
		return true;
	}


	/**
	 * Est-ce que le paiement a déjà été effectué?
	 * @return True/False
	 */
	public boolean isPaiementEffectue() {
		return this.editedObject.paye().equals(Constante.OUI);
	}

	/**
	 * Est-ce que le bordereau a été généré ?
	 * @return True/False
	 */
	public boolean isBordereauGenere() {
		return (this.editedObject.dateBordereau() != null);
	}

	public boolean isBordereauNonGenere() {
		return (this.editedObject.dateBordereau() == null);
	}

	
	/**
	 * @return Message de confirmation avant l'annulation du paiement
	 */
	public String confirmerAnnulerPaiement() {
		return "return confirm('" + message(Messages.DETAIL_MEP_CONFIRM_ANNULATION_PAIEMENT) + "')";
	}

	/**
	 * @return Message de confirmation avant la génération du flux de paiement
	 */
	public String confirmerGenerationFluxPaiement() {
		if (!isPaiementEffectue()) {
			return "return confirm('" + message(Messages.DETAIL_MEP_CONFIRM_GENERATION_FLUX) + "')";
		}

		return null;
	}

	/**
	 * @return renvoie vers la page précédente
	 * 				=> force à réactualiser la page parent
	 */
	public WOActionResults pagePrecedente() {
		WOComponent pageRetour = (WOComponent) getPecheSession().objectForKey(Session.BOUTONRETOUR);

		if (pageRetour instanceof ListeMEP) {
			((ListeMEP) pageRetour).displayGroup().fetch();
		}

		return pageRetour;	
	}

	public EOPaiement getEditedObject() {
		return editedObject;
	}

	public void setEditedObject(EOPaiement editedObject) {
		this.editedObject = editedObject;
	}

	public GenerateurPDFCtrl getGenerateurPDFCtrl() {
		return generateurPDFCtrl;
	}

	public MiseEnPaiementDataBean getCurrentItemMEP() {
		return currentItemMEP;
	}

	public void setCurrentItemMEP(MiseEnPaiementDataBean currentItemMEP) {
		this.currentItemMEP = currentItemMEP;
	}

	/**
	 * @return the numeroACinqChiffres
	 */
	public NSNumberFormatter numeroACinqChiffres() {
		return OutilsValidation.getNumeroACinqChiffresFormatter();
	}

	/**
	 * @return the detailPaiementUpdateContainerId
	 */
	public String detailPaiementUpdateContainerId() {
		return getComponentId() + "_detailPaiementUpdateContainerId";		
	}

	public String getMoisPaiement() {		
		if (moisPaiement == null)  {
			moisPaiement = MOIS_EN_COURS;
		}
		return moisPaiement;
	}


	public void setMoisPaiement(String moisPaiement) {
		this.moisPaiement = moisPaiement;
	}

	public String isMoisSuivantPourTG() {
		if (moisPaiement != null)  {
			if (MOIS_SUIVANT.equals(moisPaiement)) {
				return Constante.OUI;
			}
		}
		return Constante.NON;
	}

	/**
	 * @return the moisPaiementEnCours
	 */
	public String moisPaiementEnCours() {
		return MOIS_EN_COURS;
	}

	/**
	 * @return the moisPaiementSuivant
	 */
	public String moisPaiementSuivant() {
		return MOIS_SUIVANT;
	}

	/**
	 * 
	 * @return
	 */
	public String getCotisationSolidariteOui() {
		return Constante.OUI;
	}

	/**
	 * 
	 * @return
	 */
	public String getCotisationSolidariteNon() {
		return Constante.NON;
	}

	/**
	 * Cotisation à la solidarité
	 * Checkbox par défaut à Oui
	 * @return
	 */
	public String cotisationSolidarite(){

		if (cotisationSolidarite == null)  {
			cotisationSolidarite = getCotisationSolidariteOui();
		}
		return cotisationSolidarite;

	}

	/**
	 * 
	 * @param cotisationSolidarite
	 */
	public void setCotisationSolidarite(String cotisationSolidarite) {
		this.cotisationSolidarite = cotisationSolidarite;
	}

	/**
	 * 
	 * @return
	 */
	private boolean estCotisationSolidarite(){
		if (getCotisationSolidariteOui().equals(cotisationSolidarite()))  {
			return true;
		}
		else {
			return false;
		}
	}

	public String getSelectedItemId() {
		return PecheMenuItem.MISE_EN_PAIEMENT.getId();
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}

	/**
	 * @return the libelleImprimerBordereau
	 */
	public String libelleImprimerBordereau() {
		if (isBordereauGenere()) {
			return "Imprimer le bordereau";
		} else {
			return "Générer le bordereau";
		}
	}

	

}