package org.cocktail.peche.components.miseenpaiement;

import java.math.BigDecimal;

import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.entity.EOMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Liste des paiements associés à une répartition (serviceDetail)
 * 
 * @author Chama LAATIK
 */
public class ListePaiementParRepartition extends CktlPecheTableComponent<EOPaiement> {
	
	private static final long serialVersionUID = -4676006310278766718L;
	private EOServiceDetail serviceDetail;
	private String parentAPSelected;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public ListePaiementParRepartition(WOContext context) {
        super(context);
        
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOPaiement.ENTITY_NAME);
		this.getDisplayGroup().setDataSource(dataSource);
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	serviceDetail = (EOServiceDetail) valueForBinding("serviceDetail");
    	parentAPSelected = (String) valueForBinding("parentAPSelected");
    	EOQualifier qualifier = getQualifierPaiementPourServiceDetail();
		
		((ERXDatabaseDataSource) this.getDisplayGroup().dataSource()).setAuxiliaryQualifier(qualifier);
		this.getDisplayGroup().setSortOrderings(EOPaiement.NUMERO_PAIEMENT.descs());
		this.getDisplayGroup().fetch();
		
    	super.appendToResponse(response, context);
    }
    
    /**
     * On fabrique un qualifier pour récupérer la liste des paiements qui concerne le serviceDetail passé 
     * en paramètre à la classe
     * 
     * @return qualifier
     */
    private EOQualifier getQualifierPaiementPourServiceDetail() {
		return EOPaiement.TO_LISTE_MISES_EN_PAIEMENT.dot(EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY).eq(getServiceDetail());
	}
    
    /**
     * @return les heures payées en HETD 
     */
    public BigDecimal heuresPayeesHETDPaiement() {	
		if (recupererMEPDuServiceDetail() != null) {
			return recupererMEPDuServiceDetail().heuresPayeesHetd();
		}
		
		return null;
	}
    
    /**
     * @return les heures à payer en HETD 
     */
    public BigDecimal heuresAPayerHETDPaiement() {	
		if (recupererMEPDuServiceDetail() != null) {
			return recupererMEPDuServiceDetail().heuresAPayerHetd();
		}
		return null;
	}
    
    /**
     * @return la période du service détail payé ou à payer
     */
    public String periodePaiement() {	
		if (getServiceDetail() != null && getServiceDetail().toPeriode() != null) {
			return getServiceDetail().toPeriode().libelle();
		}
		return null;
	}

	/**
	 * @return La mise en paiement correspondant au serviceDeail
	 */
	private EOMiseEnPaiement recupererMEPDuServiceDetail() {
		EOQualifier qualifier = ERXQ.equals(EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY, getServiceDetail());
		
		if (getCurrentItem() != null) {
			NSArray<EOMiseEnPaiement> listeMEP = getCurrentItem().toListeMisesEnPaiement(qualifier);
			
			for (EOMiseEnPaiement unMEP : listeMEP) {
				if (unMEP.toServiceDetail() == getServiceDetail()) {
					return unMEP;
				}
			}
		}
		return null;
	}
    
	public boolean isPaiementOkOuEnCours() {
		return (this.getDisplayGroup().allObjects().size() > 0);
	}
	
	public boolean isHeuresPayeesRenseigne() {
		return ((recupererMEPDuServiceDetail() != null) && (recupererMEPDuServiceDetail().heuresPayeesHetd() != null));
	}
    @Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	public EOServiceDetail getServiceDetail() {
		return serviceDetail;
	}

	public void setServiceDetail(EOServiceDetail serviceDetail) {
		this.serviceDetail = serviceDetail;
	}

	public String getParentAPSelected() {
		return parentAPSelected;
	}

	public void setParentAPSelected(String parentAPSelected) {
		this.parentAPSelected = parentAPSelected;
	}
}