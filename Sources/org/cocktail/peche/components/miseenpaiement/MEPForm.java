package org.cocktail.peche.components.miseenpaiement;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.metier.miseenpaiement.CriteresRecherchePaiement;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiement;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiementEnseignantDataBean;
import org.cocktail.peche.outils.CorpsHelper;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Classe représentant le choix des enseignants pour la création d'une mise en paiement
 */
public class MEPForm extends PecheCreationForm<EOPaiement>  {
	
   
	private static final long serialVersionUID = 14482930951011394L;
	public static final String STATUT_VACATAIRE = "vacataire";
	public static final String STATUT_STATUTAIRE = "statutaire";
	
	private CorpsHelper corpsHelperVacataire;
	private CorpsHelper corpsHelperNonVacataire;
	
	private NSArray<EOPeriode> periodes;
	private EOPeriode currentPeriode;
	private EOPeriode selectedPeriode;

	private EOStructure currentComposanteScolarite;
	private EOStructure composanteScolariteSelectionnee;
	
	private String currentAnnee;
	private String selectedAnnee;
	
	private String statut;
	private String currentStatut;

	private MiseEnPaiementEnseignantDataBean currentItem;
	private WODisplayGroup displayGroup;
	
	private CriteresRecherchePaiement criteres;

	private NSMutableArray<MiseEnPaiementEnseignantDataBean> liste = new NSMutableArray<MiseEnPaiementEnseignantDataBean>();

	private boolean checkTout;
	
	public String getCurrentStatut() {
		return currentStatut;
	}

	public void setCurrentStatut(String currentStatut) {
		this.currentStatut = currentStatut;
	}

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public MEPForm(WOContext context) {
		super(context);
		
		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire())  {
			corpsHelperVacataire = new CorpsHelper(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString(), false, true);
			// initialisation de la liste des contrats vacataires
			setCurrentStatut(STATUT_VACATAIRE);		
		}

		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires())  {
			// initialisation de la liste des contrats non vacataires
			corpsHelperNonVacataire = new CorpsHelper(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString(), true, false);
		}
		
		// initialisation de la liste des périodes
		periodes = EOPeriode.fetchEOPeriodes(edc(), ERXQ.equals(EOPeriode.TYPE_KEY, EOPecheParametre.getChoixTypePeriode(edc()) + getPecheSession().getPecheParametres().getCodeFormatAnnee()), 
				EOPeriode.ORDRE_AFFICHAGE.ascs());
		
		setCheckTout(true);
		
		setAnneeSelectionnee(new Integer(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()).toString());
		}


	@Override
	protected String getFormTitreCreationKey() {
		return Messages.PAIEMENT_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return null;
	}

	@Override
	protected WOActionResults pageGestion() {
		DetailMEP page = (DetailMEP) pageWithName(DetailMEP.class.getName());
		page.setEditedObject(editedObject());
		return page;
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
	}

	@Override
	protected EOPaiement creerNouvelObjet() {
		return null;		
	}
	

	/**
	 * @return the nouveauPaiementContainerId
	 */
	public String nouveauPaiementContainerId() {
			return getComponentId() + "_nouveauPaiementContainerId";		
	}
	
	/**
	 * @return la liste des types de status
	 */

	public NSDictionary<String, String> typesStatuts() {
		NSDictionary<String, String> typesStatuts = new NSMutableDictionary<String, String>();
		
		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire())  {
			typesStatuts.takeValueForKey(" Vacataire ", STATUT_VACATAIRE);	
		}

		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires())  {
			typesStatuts.takeValueForKey(" Statutaire ", STATUT_STATUTAIRE);	
		}
		
		return typesStatuts;
	}
	
	/**
	 * 
	 * @return List<IStructure>
	 */
	public List<IStructure> getListeStructures() {
		List<IStructure> liste = null;
		if (isStatutVacataire()) {
			liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire();
			new StructureTri().trierParLibelleCourt(liste);
			return liste;			
		} else {
			liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
			new StructureTri().trierParLibelleCourt(liste);
			return liste;
		}
	}

	/**
	 * 
	 * @return Vrai si le statut est vacataire
	 */
	public boolean isStatutVacataire() {
		return STATUT_VACATAIRE.equals(this.statut);
	}

	/**
	 * @return the typeStatutDisplayString
	 */
	public String typeStatutDisplayString() {
		return (String) typesStatuts().valueForKey(currentStatut);
	}

	public EOStructure getCurrentComposanteScolarite() {
		return currentComposanteScolarite;
	}

	public void setCurrentComposanteScolarite(EOStructure currentComposanteScolarite) {
		this.currentComposanteScolarite = currentComposanteScolarite;
	}
		
	public EOStructure getComposanteScolariteSelectionnee() {
		return composanteScolariteSelectionnee;
	}

	public void setComposanteScolariteSelectionnee(EOStructure composanteScolariteSelectionnee) {
		this.composanteScolariteSelectionnee = composanteScolariteSelectionnee;
	}
		
	/**
	 * @return the typeContratContainerId
	 */
	public String typeContratContainerId() {
		return getComponentId() + "_nouveauPaiementTypeContratContainerId";	
	}

	/**
	 * retour sur la page précedente.
	 * 
	 * @return La page appelante
	 */
	public WOActionResults pagePrecedente() {		
		WOComponent pageRetour = (WOComponent) getPecheSession().objectForKey(Session.BOUTONRETOUR);
		edc().revert(); 
		if (pageRetour != null) {
			return pageRetour;
		}
		return null;
	}
	
	/**
	 * Recherche les mises en paiement
	 * @return null
	 */
	
	public WOActionResults rechercherMEP() {
		
		if (selectedAnnee == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "le choix de l'année universitaire est obligatoire");
			return null;
		}
		
		if (selectedPeriode == null && statut.equals(STATUT_VACATAIRE)) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "le choix de la période (\"Jusqu'à\") est obligatoire");
			return null;
		}
	
		edc().revert(); //car réutilisation du bouton recherché possible		
		
		criteres = new CriteresRecherchePaiement();
		
		if (selectedAnnee != null) {
			criteres.setAnneeUniversitaire(Integer.valueOf(selectedAnnee));	
		}
		
		criteres.setPeriode(selectedPeriode);
		criteres.setEnseignantsStatutaire(statut.equals(STATUT_STATUTAIRE));
		criteres.setEnseignantsVacataire(statut.equals(STATUT_VACATAIRE));
		if (isStatutVacataire()) {
			criteres.setTypeContrat(corpsHelperVacataire.getSelectionContratTravail());
			criteres.setCorps(corpsHelperVacataire.getSelectionCorps());
		} else {
			criteres.setTypeContrat(corpsHelperNonVacataire.getSelectionContratTravail());
			criteres.setCorps(corpsHelperNonVacataire.getSelectionCorps());
		}
		
		if (composanteScolariteSelectionnee != null) {
			List<IStructure> listeStruct = new ArrayList<IStructure>();
			listeStruct.add((IStructure) composanteScolariteSelectionnee);
			criteres.setListeStructure(listeStruct);
		} else {
			criteres.setListeStructure(getListeStructures());
		}
		
		liste = MiseEnPaiement.getInstance().rechercherHeuresAPayer(edc(), criteres, getPecheSession().getApplicationUser().getPersonne());
				
		return null;
	}

	public NSArray<EOPeriode> getPeriodes() {
		return periodes;
	}

	public void setPeriodes(NSArray<EOPeriode> periodes) {
		this.periodes = periodes;
	}

	public EOPeriode getCurrentPeriode() {
		return currentPeriode;
	}

	public void setCurrentPeriode(EOPeriode currentPeriode) {
		this.currentPeriode = currentPeriode;
	}

	public EOPeriode getSelectedPeriode() {
		return selectedPeriode;
	}

	public void setSelectedPeriode(EOPeriode selectedPeriode) {
		this.selectedPeriode = selectedPeriode;
	}
	
	/**
	 * @return the statut
	 */
	public String statut() {
		if (statut == null) {
			statut = STATUT_VACATAIRE;				
		}
		return statut;
	}

	/**
	 * @param statut the statut to set
	 */
	public void setStatut(String statut) {
		this.statut = statut;
	}
		
	/**
	 * @return the currentAnnee
	 */
	public String currentAnnee() {
		return currentAnnee;
	}

	/**
	 * @param currentAnnee the currentAnnee to set
	 */
	public void setCurrentAnnee(String currentAnnee) {
		this.currentAnnee = currentAnnee;
	}

	/**
	 * @return the anneeSelectionnee
	 */
	public String anneeSelectionnee() {
		return selectedAnnee;
	}

	/**
	 * @param anneeSelectionnee the anneeSelectionnee to set
	 */
	public void setAnneeSelectionnee(String anneeSelectionnee) {
		this.selectedAnnee = anneeSelectionnee;
	}

	/**
	 * @return the anneesDisplayString
	 */
	public String anneesDisplayString() {
		return (String) listeAnneesUniversitaires().valueForKey(currentAnnee());
	}

	
	/**
	 * @return l'identifiant du conteneur des boutons.
	 */
	public String buttonsContainerId() {
		return getComponentId() + "_buttonsContainerId";
	}

	/**
	 * @return the currentItem
	 */
	public MiseEnPaiementEnseignantDataBean currentItem() {
		return currentItem;
	}

	/**
	 * @param currentItem the currentItem to set
	 */
	public void setCurrentItem(MiseEnPaiementEnseignantDataBean currentItem) {
		this.currentItem = currentItem;
	}
	
	/**
	 * @param displayGroup the displayGroup to set
	 */
	public void setDisplayGroup(WODisplayGroup displayGroup) {
		this.displayGroup = displayGroup;
	}

	/**
	 * @return displayGroup the displayGroup
	 */
	
	public WODisplayGroup displayGroup() {
		displayGroup = new ERXDisplayGroup<MiseEnPaiementEnseignantDataBean>();
		EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(MiseEnPaiementEnseignantDataBean.class), edc());
		dataSource.setArray(liste);
		
		displayGroup.setDataSource(dataSource);
		displayGroup.setDelegate(this);
		
		displayGroup.setSelectsFirstObjectAfterFetch(true);
		displayGroup.fetch();
        return displayGroup;
    }

	@Override
	public WOActionResults enregistrerEtRetour() {
		
		if (nePasAutoriserPaiement()) {
			return null;
		}
		
		if (selectedAnnee != null) {
			criteres.setAnneeUniversitaire(Integer.valueOf(selectedAnnee));	
		}
		criteres.setPeriode(selectedPeriode);
		criteres.setEnseignantsStatutaire(statut.equals(STATUT_STATUTAIRE));
		criteres.setEnseignantsVacataire(statut.equals(STATUT_VACATAIRE));
		
		if (isStatutVacataire()) {
			criteres.setTypeContrat(corpsHelperVacataire.getSelectionContratTravail());
			criteres.setCorps(corpsHelperVacataire.getSelectionCorps());
		} else {
			criteres.setTypeContrat(corpsHelperNonVacataire.getSelectionContratTravail());
			criteres.setCorps(corpsHelperNonVacataire.getSelectionCorps());
		}

		MiseEnPaiement miseEnPaiement = MiseEnPaiement.getInstance();
		
		EOPaiement paiement = miseEnPaiement.genererPaiement(edc(), criteres.getAnneeUniversitaire(), criteres.getPeriode(), liste, getPecheSession().getApplicationUser().getPersonne());
		
		setEditedObject(paiement);
						
		return super.enregistrerEtRetour();
		
	}

	/**
	 * @return l'identifiant du conteneur des données de la table.
	 */
	public String tableViewId() {
		return getComponentId() + "_tableViewId";
	}

	/**
	 * @return the nePasAutoriserPaiement
	 */
	public Boolean nePasAutoriserPaiement() {		
		for (MiseEnPaiementEnseignantDataBean miseEnPaiementEnseignantDataBean : liste) {
			if (miseEnPaiementEnseignantDataBean.isBonPourPaiement()) {
				return false;
			}
		}
		return true; 
	}

	/**
	 * @return the checkTout
	 */
	public boolean isCheckTout() {
		return checkTout;
	}

	/**
	 * @param ckTout the checkTout to set
	 */
	public void setCheckTout(boolean ckTout) {
		this.checkTout = ckTout;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.MISE_EN_PAIEMENT.getId();
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}

	public String displayStructures() {
		if (getCurrentComposanteScolarite() != null) {
			return getCurrentComposanteScolarite().lcStructure() + " (" + getCurrentComposanteScolarite().toStructurePere().lcStructure() + ")"; 
		}
		return null;
	}

	public CorpsHelper getCorpsHelperVacataire() {
		return corpsHelperVacataire;
	}

	public void setCorpsHelperVacataire(CorpsHelper corpsHelperVacataire) {
		this.corpsHelperVacataire = corpsHelperVacataire;
	}

	public CorpsHelper getCorpsHelperNonVacataire() {
		return corpsHelperNonVacataire;
	}

	public void setCorpsHelperNonVacataire(CorpsHelper corpsHelperNonVacataire) {
		this.corpsHelperNonVacataire = corpsHelperNonVacataire;
	}

}


