package org.cocktail.peche.components.miseenpaiement;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSNumberFormatter;

import er.extensions.eof.ERXDatabaseDataSource;

	
/**
 * Classe représentant la liste des mises en paiement
 */
public class ListeMEP extends PecheGestionComponent<EOPaiement, MEPForm> {
	
	
	private static final long serialVersionUID = -2033086783987989629L;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public ListeMEP(WOContext context) {
        super(context);  

        EOQualifier qualifier = EOPaiement.ANNEE_PAIEMENT.eq(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOPaiement.ENTITY_NAME);
        EOFetchSpecification f = new EOFetchSpecification(EOPaiement.ENTITY_NAME, qualifier, EOPaiement.NUMERO_PAIEMENT.ascs());
        dataSource.setFetchSpecification(f);
        displayGroup().setDataSource(dataSource);
		displayGroup().fetch();
    }


	@Override
	protected String getEntityName() {
		return EOPaiement.ENTITY_NAME;
		}


	@Override
	protected Class<?> getFormClass() {
		return MEPForm.class;
	}


	@Override
	protected String getConfirmSupressMessageKey() {
		return null;
	}


	/**
	 * @return the etatpaiement
	 */
	public String etatpaiement() {
		EOPaiement eoPaiement = this.getCurrentItem();
		if (eoPaiement != null) {
			if (eoPaiement.paye().equals("O")) {
				return "Oui";
				} else {
				return "Non";
				} 
			}
		return "";		
	}

	
	
    /**
     * Retourne le detail d'une mise en paiement
     * @return la page.
     */
    public WOActionResults getLinkToDetailPaiment() {
		if (getCurrentItem() != null) {
			DetailMEP page = (DetailMEP) pageWithName(DetailMEP.class.getName());
			page.setEditedObject(getCurrentItem());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} 
		
		return null;
    }
	
	
	
	/**
	 * @return le detail d'une mise en paiement
	 */
	public WOActionResults detailAction() {
		if (getSelectedItem() != null) {
			DetailMEP page = (DetailMEP) pageWithName(DetailMEP.class.getName());
			page.setEditedObject(getSelectedItem());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} 
		
		return null;
	}

	/**
	 * @return le formulaire de création d'une mise en paiement
	 */
	public WOActionResults ajouterAction() {
		MEPForm page = (MEPForm) pageWithName(MEPForm.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		this.session().setObjectForKey(this, Session.BOUTONRETOUR);
		return page;
	}
	

	/**
	 * @return the etatdetail
	 */
	public boolean etatdetail() {
		EOPaiement paiement = this.getSelectedItem();
		if (paiement == null) {
			paiement = getCurrentItem();
		}
		if (paiement != null)  {
			if (paiement.isPaiementForStatutaires(getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) 
					&& getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires()) {
				return false;
			}
			if (!paiement.isPaiementForStatutaires(getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) 
					&& getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire()) {
				return false;
			}
		}
		return true;
	}


	/**
	 * @return the numeroACinqChiffres
	 */
	public NSNumberFormatter numeroACinqChiffres() {
		return OutilsValidation.getNumeroACinqChiffresFormatter();
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.MISE_EN_PAIEMENT.getId();
	}
	
	
}