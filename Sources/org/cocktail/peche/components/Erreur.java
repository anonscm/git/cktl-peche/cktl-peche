package org.cocktail.peche.components;

import org.cocktail.peche.components.commun.PecheComponent;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSDictionary;


public class Erreur extends PecheComponent {
	
	private NSDictionary exceptionInfos;
	
    public Erreur(WOContext context) {
        super(context);
    }

	public WOComponent retourAccueil() {
		Accueil accueil = (Accueil)getPecheSession().getSavedPageWithName(Accueil.class.getName());
		getPecheSession().resetDefaultEditingContext();
		accueil.setOnloadJS(null);
		return accueil;
	}

	/**
	 * @return the exceptionInfos
	 */
	public NSDictionary exceptionInfos() {
		exceptionInfos = (NSDictionary)getPecheSession().exceptionInfos();
		return exceptionInfos;
	}

	/**
	 * @param exceptionInfos the exceptionInfos to set
	 */
	public void setExceptionInfos(NSDictionary exceptionInfos) {
		this.exceptionInfos = exceptionInfos;
	}
}