package org.cocktail.peche.components.controlers;

import java.util.Collection;
import java.util.TreeSet;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;

/**
 * Contrôleur pour les type d'AP.
 */
public class TypeApCtrl {
	/**
	 * Trier la liste des codes AP (CM, TD, TP puis les autres par ordre alphabétique).
	 * <p>
	 * Remarque : Cette méthode supprime également les doublons.
	 * 
	 * @param listeCodesAp La liste des codes AP à trier (c'est cette liste qui sera triée)
	 */
	public void trierListeCodesAp(Collection<String> listeCodesAp) {
		Collection<String> listeTemporaire = new TreeSet<String>(listeCodesAp);

		boolean cmPresent = true;
		boolean tdPresent = true;
		boolean tpPresent = true;

		if (!listeTemporaire.isEmpty()) {
			cmPresent = listeTemporaire.contains(EOTypeAP.TYPECOURS_CODE);
			tdPresent = listeTemporaire.contains(EOTypeAP.TYPETD_CODE);
			tpPresent = listeTemporaire.contains(EOTypeAP.TYPETP_CODE);
		}

		listeTemporaire.remove(EOTypeAP.TYPECOURS_CODE);
		listeTemporaire.remove(EOTypeAP.TYPETD_CODE);
		listeTemporaire.remove(EOTypeAP.TYPETP_CODE);
		listeCodesAp.clear();
		if (cmPresent) {
			listeCodesAp.add(EOTypeAP.TYPECOURS_CODE);
		}
		if (tdPresent) {
			listeCodesAp.add(EOTypeAP.TYPETD_CODE);
		}
		if (tpPresent) {
			listeCodesAp.add(EOTypeAP.TYPETP_CODE);
		}
		listeCodesAp.addAll(listeTemporaire);
	}
}
