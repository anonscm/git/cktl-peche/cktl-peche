package org.cocktail.peche.components.controlers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeChargeEnseignement;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EORepartUefDetail;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.RandomHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Controleur pour le composant UEFlottante
 * @author Chama LAATIK
 */
public class UECtrl extends BasicPecheCtrl {



	/**
	 * Constructeur sans editiingContext.
	 */
	public UECtrl() {
		super(null);
	}
	
	/**
	 * Constructeur.
	 * @param editingContext le contexte d'accès à la base de données.
	 * @param annee m'annee sur laquelle on travaille.
	 */
	public UECtrl(EOEditingContext editingContext, int annee) {
		super(editingContext, annee, false);
	}
	

	/**
	 * Renvoie l'établissement externe où va se dérouler l'enseignement
	 * @param composantUE : l'enseignement
	 * @return etablissement externe
	 */
	public String getEtablissementExterne(IComposant composantUE) {
		String etablissement = null;
		EORepartUefEtablissement repartition = getRepartition(composantUE);

		if (repartition != null) {
			etablissement = repartition.etablissementExterne();
		}

		return etablissement;
	}

	/**
	 * Renvoie les lienComposer du composant passé en paramètre
	 * @param composant : composant parent
	 * @return liste de lienComposer des enfants de type AP
	 */
	public NSArray<EOLienComposer> getLienComposerEnfantsTypeAP(EOComposant composant) {
		return composant.liensComposerAvecLesComposantsEnfants(EOTypeComposant.typeAP(edc()));
	}

	/**
	 * Renvoie la répartition des UE flottantes/Etablissement
	 * @param composantUE : UE pour laquelle on cherche une répartition
	 * @return répartition
	 */
	public EORepartUefEtablissement getRepartition(IComposant composantUE) {
		return EORepartUefEtablissement.rechercherEtablissementPourUE(edc(), composantUE);
	}

	
	/**
	 * Supprime la répartition entre l'UE passé en paramètre et de l'établissement
	 * @param composantUE : UE à supprimer
	 */
	public void supprimerRepartitionEtablissement(EOUE composantUE) {
		EORepartUefEtablissement repartition = getRepartition(composantUE);

		if (repartition != null) {
			edc().deleteObject(repartition);
			edc().saveChanges();
		}
	}
	
	/**
	 * Supprime l'association entre l'ue et le détail de l'ue.
	 * @param ue
	 */
	public void supprimerDetail(EOUE ue){
		EORepartUefDetail detail = EORepartUefDetail.rechercherDetailForUE(edc(), ue);
		if(detail != null){
			edc().deleteObject(detail);
		}
	}
	
	/**
	 * Crée les liensComposer et CreditableLien entre l'AP et son parent l'UE
	 * @param ap : AP => enfant
	 * @param ue : UE => parent
	 * @throws Exception
	 */
	public void creerlienAP(EOAP ap, EOUE ue) throws Exception {
		try {
			// Mise à jour de l'AP
			ap.setCode(RandomHelper.getInstance().getRandomStringWithPrefix(Constante.CODE_AP_PREFIX));
			ap.setLibelle(ap.typeAP().libelle());
			ap.setModeRelationship(EOModeAP.modePresentiel(edc()));

			// creation du lien composer
			EOLienComposer lienComposer = EOComposantFactory.createLienComposer(ue, ap);
			ue.addToLiensParentsRelationship(lienComposer);
			ap.addToLiensChildsRelationship(lienComposer);

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Initialise l'AP
	 * @param typeAP typeAP
	 * @param persId Personne connecté
	 * @paraù annee année concernée
	 * @return AP
	 */
	public EOAP initialiserAP(EOTypeAP typeAP, Integer persId, Integer annee) {
		EOAP composantAP = null;
		try {
			composantAP = EOComposantFactory.createComposant(EOAP.class, persId, edc(), annee, null);
			composantAP.setTypeAP(typeAP);
			composantAP.setTagApplication(Constante.APP_PECHE);
			
			EOComposantFactory.createChargeEnseignementSurAP(composantAP, EOTypeChargeEnseignement.typePrevisionnelle(edc()), persId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return composantAP;
	}

	/**
	 * Renvoie les charges d'enseignement ==> nombre d'heures * nombre de groupe
	 * @param ap : AP
	 * @param prorataActif : pour les AP mutualisés, est-ce qu'on prend en compte le nombre de parent dans le calcul ==> oui/non
	 * @return nombre d'heures
	 */
	public float getHeuresMaquette(EOAP ap, boolean isMutualise) {
		float heuresMaquette = 0.0f;

		
		if (ap != null) {
			Integer nbGroupeLienComposer = null;
			BigDecimal nbHeuresLienComposer = null;
			int prorataNbHeures = 1;					
			
			
			if (isMutualise) { 
				// AP mutualisé : définit au prorata le nombre d'heures de l'AP en fonction du nombre des parents
				// dans le calcul des charges d'enseignement
				List<IComposant> se = ap.parents();
				for (IComposant c : se) {
					if (c.parents().size()>1) {
						prorataNbHeures = c.parents().size();
					}
				}
			}
			
			// On récupère les lienComposer de l'AP
			List<ILienComposer> listeLiensComposer = getListeLienComposer(ap);
			
			if (listeLiensComposer != null) {
				// Est-ce qu'on prend en compte le nombre de parents des AP mutualisés?

			
				//TODO : ajout controle sur valeur de nbGroupe/heures ds les différents lienComposer
				//Pr les AP mutualisés, le nombre de groupe doit etre le meme pour ts les liensComposer
				for (ILienComposer lienComposer : listeLiensComposer) {
					IChargeEnseignement chargeEnseignement = ((EOLienComposer) lienComposer).chargeEnseignementPrevisionnelle();
					if (chargeEnseignement != null) {
						nbGroupeLienComposer = chargeEnseignement.nbGroupes();
						nbHeuresLienComposer = chargeEnseignement.valeurHeures();

						
				// TODO : ajout controle sur valeur de nbGroupe/heures ds les différents lienComposer
//				// Pr les AP mutualisés, le nombre de groupe doit etre le meme pour ts les liensComposer
//				for (ILienComposer unlienComposer : listeLiensComposer) {
//					ILienComposer lienComposer = unlienComposer;
//					if ((lienComposer.nbGroupes() != null) || (lienComposer.valeurHeures() != null)) {
//						nbGroupeLienComposer = lienComposer.nbGroupes();
//						nbHeuresLienComposer = lienComposer.valeurHeures();
//						break;
					}
				}
			}

			Integer nbGroupe = getNbGroupe(nbGroupeLienComposer, ap, 0);
			BigDecimal nbHeures = getNbHeures(nbHeuresLienComposer, ap, BigDecimal.ZERO);
			
			// TODO Remplacer par BigDecimal
			heuresMaquette = (nbHeures.floatValue() / prorataNbHeures) * nbGroupe;
			
//			Float nbHeures = getNbHeures(nbHeuresLienComposer, ap, 0.0f);

			// TODO :remplacer par BigDecimal
//			heuresMaquette = (nbHeures / prorataNbHeures) * nbGroupe;
		}

		return heuresMaquette;
	}

	/**
	 * Renvoie le nombre de groupe à prendre en compte pour le calcul des charges d'enseignement ==> En priorité le nombre de groupe surchargé, puis le nombre de
	 * groupe de l'AP sinon on renvoie une valeur par défaut
	 * @param nbGroupeLienComposer : Nombre de groupe récupéré du lienComposer
	 * @param ap : AP
	 * @param nbGroupeDefaut : nombre de groupe par défaut si null
	 * @return nombre de groupe
	 */
	public Integer getNbGroupe(Integer nbGroupeLienComposer, EOAP ap, Integer nbGroupeDefaut) {
		//On prend en priorité le nombre de groupe surchargé
		Integer nbGroupe = nbGroupeLienComposer;
		
		// Si null, on prend le groupe depuis l'AP
		if (nbGroupe == null) {
			IChargeEnseignement chargeEnseignement = getChargeEnseignement(ap);
			
			if (chargeEnseignement != null) {
				nbGroupe = chargeEnseignement.nbGroupes();
			}
		}
		
		//Si le nombre de groupe est non renseigné, on renvoie un nombre de groupe par défaut
		if (nbGroupe == null) {
			nbGroupe = nbGroupeDefaut;
		}
		
		return nbGroupe;
	}

	/**
	 * Renvoie le nombre d'heures à prendre en compte pour le calcul des charges d'enseignement ==> En priorité les heures surchargées, puis les heures de l'AP
	 * sinon on renvoie une valeur par défaut
	 * @param nbHeuresLienComposer : Nombre d'heures récupéré du lienComposer
	 * @param ap : AP
	 * @param nbHeuresDefaut : nombre d'heures par défaut si null
	 * @return nombre d'heures
	 */
	public BigDecimal getNbHeures(BigDecimal nbHeuresLienComposer, EOAP ap, BigDecimal nbHeuresDefaut) {
		// On prend en priorité le nombre d'heures surchargé
		BigDecimal nbHeures = nbHeuresLienComposer;
	
		// Si null, on prend les heures AP
		if (nbHeures == null) {
			IChargeEnseignement chargeEnseignement = getChargeEnseignement(ap);
			
			if (chargeEnseignement != null) {
				nbHeures = chargeEnseignement.valeurHeures();
			}
		}
	
		// Si les heures sont non renseignées, on renvoie un nombre d'heures par défaut
		if (nbHeures == null) {
			nbHeures = nbHeuresDefaut;
		}
	
		return nbHeures;
	}

	/**
	 * Renvoie le nombre d'heures prévu lors de la répartiton d'un AP
	 * @param ap : AP
	 * @param modeEdition : modeEdition
	 * @param heuresEditedObject : nombre d'heures à ajouter
	 * @param prorataParentActif : pour les AP mutualisés, est-ce qu'on prend en compte le nombre de parent dans le calcul ==> oui/non
	 * @return nombre d'heures total
	 */
	public float getTotalHeuresPrevues(EOAP ap, ModeEdition modeEdition, Double heuresEditedObject, boolean prorataParentActif) {
		float heuresPrevues = getTotalHeuresPrevues(ap, prorataParentActif);

		// Traitement spécial lors de la création pour les prendre en compte
		if (modeEdition == ModeEdition.CREATION && heuresEditedObject != null) {
			heuresPrevues += heuresEditedObject;
		}

		return heuresPrevues;
	}

	/**
	 * Renvoie le nombre d'heures réalisées lors de la répartiton d'un AP
	 * @param ap : AP
	 * @param modeEdition : modeEdition
	 * @param heuresEditedObject : nombre d'heures à ajouter
	 * @param prorataParentActif : pour les AP mutualisés, est-ce qu'on prend en compte le nombre de parent dans le calcul ==> oui/non
	 * @return nombre d'heures total
	 */
	public float getTotalHeuresRealisees(EOAP ap, ModeEdition modeEdition, Double heuresEditedObject, boolean prorataParentActif) {
		float heuresRealisees = getTotalHeuresRealisees(ap, prorataParentActif);

		// Traitement spécial lors de la création pour les prendre en compte
		if (modeEdition == ModeEdition.CREATION && heuresEditedObject != null) {
			heuresRealisees += heuresEditedObject;
		}

		return heuresRealisees;
	}
	
	/**
	 * Renvoie le nombre d'heures total prévu de la répartition ==> gère bien les heures en mode Modification car met à jour directement le @sum
	 * @param ap : AP
	 * @param prorataActif : pour les AP mutualisés, est-ce qu'on prend en compte le nombre de parent dans le calcul ==> oui/non
	 * @return nombre d'heures
	 */
	public float getTotalHeuresPrevues(EOAP ap, boolean prorataActif) {
		return getTotalHeures(ap, prorataActif, "@sum.heuresPrevues");
	}

	
	/**
	 * Renvoie le nombre d'heures total réalisé de la répartition ==> gère bien les heures en mode Modification car met à jour directement le @sum
	 * @param ap : AP
	 * @param prorataActif : pour les AP mutualisés, est-ce qu'on prend en compte le nombre de parent dans le calcul ==> oui/non
	 * @return nombre d'heures
	 */
	public float getTotalHeuresRealisees(EOAP ap, boolean prorataActif) {
		return getTotalHeures(ap, prorataActif, "@sum.heuresRealisees");
	}
	
	
	private float getTotalHeures(EOAP ap, boolean prorataActif, String key) {
		int nbParentAPMutualise = 1;

		// Est-ce qu'on prend en compte le nombre de parents des AP mutualisés?
		if ((getListeLienComposer(ap) != null) && prorataActif) {
			// AP mutualisé : définit au prorata le nombre d'heures de l'AP en fonction du nombre des parents
			// dans le calcul des charges d'enseignement
			nbParentAPMutualise = getListeLienComposer(ap).size();
		}

		EOQualifier qualifier = EOServiceDetail.COMPOSANT_AP.dot(EOAP.ID_KEY).eq(ap.id());
		EOQualifier qualAnnee = EOServiceDetail.SERVICE.dot(EOService.ANNEE_KEY).eq(getAnneeUniversitaire());
		EOQualifier qualFinal = ERXQ.and(qualifier, qualAnnee);
		NSArray<EOServiceDetail> details = EOServiceDetail.fetchEOServiceDetails(edc(), qualFinal, null);
		BigDecimal heuresPrevues = (BigDecimal) details.valueForKey(key);
		return (heuresPrevues.floatValue() / nbParentAPMutualise);
	}
	
	
	
	/**
	 * Renvoie le reste des heures à répartir : heures maquette - heures prévues ==> heures maquette prend en compte le nombre de groupe
	 * @param ap : AP
	 * @return reste à répartir
	 */
	public float getNbHeuresResteARepartir(EOAP ap) {
		return getHeuresMaquette(ap, false) - getTotalHeuresPrevues(ap, false);
	}
	
	/**
	 * Renvoie le reste des heures à réaliser : heures maquette - heures réalisées ==> heures maquette prend en compte le nombre de groupe
	 * @param ap : AP
	 * @return reste à réaliser
	 */
	public float getNbHeuresResteARealiser(EOAP ap) {
		return getHeuresMaquette(ap, false) - getTotalHeuresRealisees(ap, false);
	}
	

	/**
	 * On récupère les lienComposer de l'AP
	 * @param ap : AP
	 * @return liensComposer de l'AP
	 */
	private List<ILienComposer> getListeLienComposer(EOAP ap) {
		List<ILienComposer> liensParents = new ArrayList<ILienComposer>();
		
		for (ILien lien :  ap.liensChildsWithoutDuplicate()) {
			if (lien instanceof EOLienComposer) {
				liensParents.add((EOLienComposer) lien);
			}
		}
	  
	  return liensParents;
	}
	
	/**
	 * Retourne la charge d'enseignement pour ce lien.
	 * S'il n'y a pas de charge d'enseignement pour ce lien, on recherche pour le composant enfant.
	 * 
	 * @param lien un lien
	 * @param typeChargeEnseignement un type de charge d'enseignement (voir constantes dans {@link EOTypeChargeEnseignement})
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    private IChargeEnseignement getChargeEnseignement(EOLien lien, String typeChargeEnseignement) {
    	IChargeEnseignement chargeEnseignement = lien.chargeEnseignement(typeChargeEnseignement);
    	
    	// Si pas trouvé au niveau du lien, on recherche au niveau du composant
    	if (chargeEnseignement == null) {
    		chargeEnseignement = ((EOAP) lien.child()).chargeEnseignement(typeChargeEnseignement);
    	}
    	
    	return chargeEnseignement;
    }
    
	/**
	 * Retourne la charge d'enseignement prévisionnelle (saisie surcharge Peche) pour ce lien.
	 * S'il n'y a pas de charge d'enseignement pour ce lien, on recherche pour le composant enfant.
	 * 
	 * @param lien un lien
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement getChargeEnseignementPrevisionnelle(EOLien lien) {
    	return getChargeEnseignement(lien, ITypeChargeEnseignement.CODE_TYPE_PREVISIONNELLE);
    }
    
	/**
	 * Retourne la charge d'enseignement théorique (saisie Girofle) pour ce lien.
	 * S'il n'y a pas de charge d'enseignement pour ce lien, on recherche pour le composant enfant.
	 * 
	 * @param lien un lien
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement getChargeEnseignementTheorique(EOLien lien) {
    	return getChargeEnseignement(lien, ITypeChargeEnseignement.CODE_TYPE_THEORIQUE);
    }
    
	/**
	 * Retourne la charge d'enseignement pour ce lien.
	 * S'il n'y a pas de charge d'enseignement pour ce lien, on recherche pour le composant enfant.
	 * Recherche en premier le prévisionel (saisie surcherge Peche) puis le théorique (saisie Girofle).
	 * 
	 * @param lien un lien
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement getChargeEnseignement(EOLien lien) {
    	IChargeEnseignement chargeEnseignement = getChargeEnseignementPrevisionnelle(lien);
    	
    	if (chargeEnseignement == null) {
    		chargeEnseignement = getChargeEnseignementTheorique(lien);
    	}
    	
    	return chargeEnseignement;
    }
    
	/**
	 * Retourne la charge d'enseignement pour cette AP.
	 * Recherche d'abord la charge d'enseignement prévisionnelle (saisie Peche).
	 * S'il n'y en a pas recherche ensuite la charge d'enseignement théorique (saisie Girofle).
	 * 
	 * @param ap une AP
	 * @return la charge d'enseignement (peut retourner <code>null</code> s'il n'y en a pas de définie)
	 */
    public IChargeEnseignement getChargeEnseignement(EOAP ap) {
    	IChargeEnseignement chargeEnseignement = ap.chargeEnseignementPrevisionnelle();
    	
    	if (chargeEnseignement == null) {
    		chargeEnseignement = ap.chargeEnseignementTheorique();
    	}
    	
    	return chargeEnseignement;
    }   
    
    
	/**
	 * Renvoie le nombre d'heures total souhaitées par AP
	 * dont la demande est à valider par le répartiteur
	 * @param ap : AP
	 * @param annee : annee
	 * @return nombre d'heures
	 */
	public float getTotalHeuresSouhaitees(EOAP ap, Integer annee) {
		
		NSArray<EOFicheVoeuxDetail> details = EOFicheVoeuxDetail.fetchEOFicheVoeuxDetails(edc(), getQualifierPourFicheVoeuxAArbitrer(ap, annee), null);
		BigDecimal heuresSouhaitees = (BigDecimal) details.valueForKey("@sum.heuresSouhaitees");
		return heuresSouhaitees.floatValue();
	}
    
	/**
	 * 
	 * @param ap ap
	 * @param annee année universitaire en cours
	 * @return qualifier 
	 */
	public EOQualifier getQualifierPourFicheVoeuxAArbitrer(EOAP ap, Integer annee) {

		ERXKeyValueQualifier qualifierAP = EOFicheVoeuxDetail.COMPOSANT_AP.dot(EOAP.ID_KEY).eq(ap.id());
		ERXKeyValueQualifier qualifierAnnee = EOFicheVoeuxDetail.FICHE_VOEUX.dot(EOFicheVoeux.ANNEE).eq(annee);
		ERXKeyValueQualifier qualifierEtatDemande = EOFicheVoeuxDetail.FICHE_VOEUX.dot(EOFicheVoeux.TO_DEMANDE).dot(EODemande.TO_ETAPE).dot(EOEtape.CODE_ETAPE).eq(EtapePeche.VALID_REPARTITEUR.getCodeEtape());
		return ERXQ.and(qualifierAP, qualifierAnnee, qualifierEtatDemande);
	}
    
	/**
	 * Renvoie le nombre d'heures qui dépasse concernant les fiches de voeux
	 * (NbHeuresReparties + NbHeuresSouhaitees) - heuresMaquette
	 * @param ap ap
	 * @param annee année universitaire en cours
	 * @return nombre d'heures en dépassement
	 */
	public float getTotalHeuresEnDepassement(EOAP ap, Integer annee) {
		return (getTotalHeuresPrevues(ap, false) + getTotalHeuresSouhaitees(ap, annee)) - getHeuresMaquette(ap, false);
	}
	
	/**
	 * 
	 * @param listeUeDansEtablissement liste UE à dupliquer
	 * @param anneeCible année cible
	 * @param personneConnecte personne connectée
	 * @return nombre de duplications réalisées
	 */
	public Integer dupliquerUEFlottantesDansEtablissement(NSArray<EOUE> listeUeDansEtablissement, Integer anneeCible, EOPersonne personneConnecte) {
		
		Integer nombreDuplications = 0;
		
		for (EOUE ue : listeUeDansEtablissement) {
			
			nombreDuplications = dupliquerUEFlottante(ue, anneeCible, nombreDuplications, personneConnecte, null);
			
		}
		
		edc().saveChanges();
		
		return nombreDuplications;
	}
	
	/**
	 * Duplique une UE Flottante
	 * @param ue
	 * @param anneeCible
	 * @param nombreDuplications
	 * @param personneConnecte
	 * @param repartUefADupliquer
	 * @return
	 */
	private int dupliquerUEFlottante(EOUE ue, Integer anneeCible, Integer nombreDuplications, EOPersonne personneConnecte, EORepartUefEtablissement repartUefADupliquer) {

		if (ue.getComposantSuivant() == null) {
			
			IComposant ueDuplique = ue.dupliquer(anneeCible, null);
			
			List<IComposant> listeComposantsAP = ue.childs();
			
			for (IComposant iComposant : listeComposantsAP) {
				
				iComposant.dupliquer(anneeCible, null);
				
			}
			
			if (repartUefADupliquer != null) {
				// cas d'une UEF hors établissement, on duplique le repart UEF
				repartUefADupliquer.dupliquer(edc(), ueDuplique, personneConnecte);
				
			}		
			
			nombreDuplications++;
						
		}
		return nombreDuplications;		
	}
	
	
	/**
	 * 
	 * @param listeRepartUefEtablissement listeRepartUef à dupliquer
	 * @param anneeCible  année cible
	 * @param personneConnecte personne connecte
	 * @return nombre de duplications
	 */
	public Integer dupliquerUEFlottantesHorsEtablissement(NSArray<EORepartUefEtablissement> listeRepartUefEtablissement, Integer anneeCible, EOPersonne personneConnecte) {
		
		Integer nombreDuplications = 0;
		
		for (EORepartUefEtablissement repartUefEtablissement : listeRepartUefEtablissement) {
			
			nombreDuplications = dupliquerUEFlottante(repartUefEtablissement.composantUE(), anneeCible, nombreDuplications, personneConnecte, repartUefEtablissement);
				
		}		
		edc().saveChanges();
		
		return nombreDuplications;
	}
	

}
