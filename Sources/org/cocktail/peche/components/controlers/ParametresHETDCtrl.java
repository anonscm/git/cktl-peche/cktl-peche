package org.cocktail.peche.components.controlers;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.entity.EOPecheParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Controlleur pour les composants de paramétrage du calcul des heurs complémentaires.
 * @author Equipe PECHE.
 *
 */
public class ParametresHETDCtrl extends BasicPecheCtrl {

	private EOMethodesCalculHComp methodeParDefaut;
	private EOPersonne personneConnectee;

	/**
	 * Constructeur.
	 * @param editingContext le contexte d'édition de WebObject.
	 * @param personneConnectee la personne connectée.
	 */
	public ParametresHETDCtrl(EOEditingContext editingContext,EOPersonne personneConnectee,Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext,annee,isFormatAnneeExerciceAnneeCivile);
		this.personneConnectee = personneConnectee;
		EOPecheParametre paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(edc(), EOPecheParametre.KEY_KEY, "hcomp.methode.defaut");
		if (paramMethodeParDefaut != null) {
			int idMethode = Integer.parseInt(paramMethodeParDefaut.value());
			this.methodeParDefaut = EOMethodesCalculHComp.fetchEOMethodesCalculHComp(edc(), EOMethodesCalculHComp.ID_KEY, idMethode);
		} else {
			this.methodeParDefaut = null;
		}
	}
	
	public EOMethodesCalculHComp getMethodeParDefaut() {
		return this.methodeParDefaut;
	}
	
	/**
	 * @param methodeParDefaut méthode par défaut.
	 */
	public void setMethodeParDefaut(EOMethodesCalculHComp methodeParDefaut) {
		this.methodeParDefaut = methodeParDefaut;
		
		NSTimestamp now = new NSTimestamp();
		EOPecheParametre paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(edc(), EOPecheParametre.KEY_KEY, "hcomp.methode.defaut");
		if (paramMethodeParDefaut != null) {
			paramMethodeParDefaut.setValue(String.valueOf(methodeParDefaut.id()));
			paramMethodeParDefaut.setDateModification(now);
			paramMethodeParDefaut.setPersonneModification(this.personneConnectee);
		} else {
			paramMethodeParDefaut = EOPecheParametre.createEOPecheParametre(edc(), "Méthode de calcul des HCOMP à appliquer par défaut", now, now, "hcomp.methode.defaut", String.valueOf(methodeParDefaut.id()), personneConnectee, personneConnectee);
		}
	}

	public NSArray<EOMethodesCalculHComp> getMethodes() {
		return EOMethodesCalculHComp.fetchAllEOMethodesCalculHComps(edc());
	}
}
