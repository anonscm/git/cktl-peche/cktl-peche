package org.cocktail.peche.components.controlers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.peche.Application;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl.ReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.appserver.WOActionResults;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;


/**
 * Controleur permettant de génerer les tableaux de bord.
 *
 * @author Chama LAATIK
 */
public class GenerateurXLSCtrl {
	
	private CktlAbstractReporter reporter;
	private IJrxmlReportListener listener = new ReporterAjaxProgress(100);

	private String reportFilename;
	private String jasperFilename = "Reports/suiviValidations/suiviValidations.jasper";
	private String xml;
	
	/**
	 * Constructeur.
	 * @param xml : le XML à générer
	 */
	public GenerateurXLSCtrl(String xml) {
		this.xml = xml;
	}

	/**
	 * Genere le tableau de bord
	 * @return null
	 */
	public WOActionResults genererTableauDeBord() {
		reporter = reporterTableauBord(xml, listener);		
		return null;
	}
	
	private CktlAbstractReporter reporterTableauBord(String xml, IJrxmlReportListener listener) {
		//récupération du chemin du fichier jasper de l'application
		String pathForJasper = pathForJasper(getJasperFilename());
		
		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			File f = new File(Application.pathJasperLocal() + "suiviValidations/suiviValidations.jasper");
			if (f.exists()) {
				pathForJasper = f.getAbsolutePath();
			}
		}
		
		String recordPath = "/suivi_validations";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			
			jr.printWithThread("Impression du suivi des validations", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_XLS, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}
	
	/**
	 * @param reportPath : le jasper
	 * @return le chemin du jasper
	 */
	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}

	public String getJasperFilename() {
		return jasperFilename;
	}

	public void setJasperFilename(String jasperFilename) {
		this.jasperFilename = jasperFilename;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}
	
	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public IJrxmlReportListener getListener() {
		return listener;
	}
}
