package org.cocktail.peche.components.controlers;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.EODecharge;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponentCtrl;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.enseignants.MinorationsServiceDataBean;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.entity.EOVueAbsences;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;
import org.cocktail.peche.outils.LogoEdition;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXExistsQualifier;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.qualifiers.ERXKeyValueQualifier;
import er.extensions.qualifiers.ERXOrQualifier;

/**
 * Contrôleur pour le composant EnseignantsStatutaires.
 *
 * @author Yannick Mauray
 * @author Chama LAATIK
 *
 */
public class EnseignantsStatutairesCtrl extends BasicPecheCtrl implements IEnseignantsCtrl {

	/** Logger. */
	private static Logger log = Logger.getLogger(EnseignantsStatutairesCtrl.class);

	private String typePopulation;

	private DemandeCtrl controleurDemande;
	private HceCtrl hceCtrl;


	/**
	 * Constructeur.
	 *
	 * @param editingContext : le contexte d'accès à la base de données.
	 * @param anneeUniversitaire annee en cours
	 * @param isFormatAnneeExerciceAnneeCivile type d'année
	 */
	public EnseignantsStatutairesCtrl(EOEditingContext editingContext, Integer anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext, anneeUniversitaire, isFormatAnneeExerciceAnneeCivile);
		controleurDemande = new DemandeCtrl(editingContext);
		hceCtrl = new HceCtrl(editingContext);
	}


	/**
	 * @param service une fiche de service
	 * @return Le nombre d'heures complémentaires prévisionnelles.
	 */
	public double getHcompPrev(EOService service) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), service.enseignant(), getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getHeuresComplementairesPrevues(getServiceDu(service.enseignant(), service.annee()), service);
		}

		return 0;
	}

	/**
	 * @param service une fiche de service
	 * @return Le nombre d'heures complémentaires realisées.
	 */
	public double getHcompRealise(EOService service) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), service.enseignant(), getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getHeuresComplementairesRealisees(getServiceDu(service.enseignant(), service.annee()), service);
		}

		return 0;
	}

	/**
	 * @param enseignant L'enseignant
	 * @param annee : année universitaire concernée
	 * @return le libellé de la première minoration qui n'autorise pas les heures complémentaires
	 */
	public String getLibelleMinorationInterdisantHComp(EOActuelEnseignant enseignant, Integer annee) {
		String minorationInterdisantHcomp = null;
		NSArray<MinorationsServiceDataBean> listeMinorationsService = rechercherReductionsService(enseignant, annee);

		for (MinorationsServiceDataBean minorationsService : listeMinorationsService) {
			if (Constante.NON.equals(minorationsService.getTemoinHcomp())) {
				minorationInterdisantHcomp = minorationsService.getLibelle();
				break;
			}
		}

		return minorationInterdisantHcomp;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le service statutaire à un enseignant
	 */
	public double getServiceStatutaire(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null && service.heuresService() != null) {
			return service.getHeuresService().doubleValue();
		}
		return enseignant.getNbHeuresServiceStatutaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le service attribué à un enseignant
	 */
	public double getServiceAttribue(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null) {		
			return service.getHeuresServiceAttribue().doubleValue();			
		}
		return 0;
	}


	/**
	 * @param enseignant Un enseignant
	 * @return Le Reh attribué à un enseignant
	 */
	public double getRehAttribue(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), service.enseignant(), getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getServiceHETDPrevu(getTotalHeuresPrevues(service.listeServiceDetailsReh()), service.listeServiceDetailsReh());
		}
		return 0;
	}


	/**
	 * @param enseignant Un enseignant
	 * @return Le service réalisé par l'enseignant
	 */
	public double getServiceRealise(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null) {		
			return service.getHeuresServiceRealise().doubleValue();			
		}
		return 0;
	}


	/**
	 * @param enseignant Un enseignant
	 * @return Le REH réalisé par l'enseignant
	 */
	public double getRehRealise(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), service.enseignant(), getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getServiceHETDRealise(getServiceDu(service.enseignant(), service.annee()), service.listeServiceDetailsReh());
		}
		return 0;
	}


	/**
	 * @param enseignant Un enseignant
	 * @return Le service souhaité par l'enseignant dans sa fiche de voeux
	 */
	public double getVoeuxSouhaite(EOActuelEnseignant enseignant) {
		EOFicheVoeux voeux = enseignant.getFicheVoeux(getAnneeUniversitaire());
		if (voeux != null) {
			return voeux.getHeuresServiceSouhaite().doubleValue();			
		}
		return 0;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le libellé court du corps de l'enseignant
	 */
	public String getLibelleCourtCorps(EOActuelEnseignant enseignant) {
		if (enseignant.getCorps(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()) == null) {
			return null;
		}

		return enseignant.getCorps(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()).lcCorps();
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le libellé court du corps ou typeContrat de l'enseignant
	 */
	public String getLibelleCourtCorpsOuContrat(EOActuelEnseignant enseignant) {
		return enseignant.getLibelleCourtCorpsouContrat(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
	}
	/**
	 * @param enseignant Un enseignant
	 * @return Le libellé long du corps ou typeContrat de l'enseignant
	 */
	public String getLibelleLongCorpsOuContrat(EOActuelEnseignant enseignant) {
		return enseignant.getLibelleLongCorpsouContrat(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
	}
	
	/**
	 * @param enseignant Un enseignant
	 * @return Le libellé long du type de population de l'enseignant
	 */
	public String getLibelleLongTypePopulation(EOActuelEnseignant enseignant) {
		return enseignant.getLibelleLongTypePopulation(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le libellé long du corps de l'enseignant
	 */
	public String getLibelleLongCorps(EOActuelEnseignant enseignant) {
		if (enseignant.getCorps(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()) == null) {
			return null;
		}

		return enseignant.getCorps(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()).llCorps();
	}


	/**
	 * Renvoie la liste des enseignants présents dans les structures
	 * @param listeStructures liste des structures à filtrer
	 * @param annee annee en cours
	 * @return la liste des enseignants
	 */
	public EOQualifier getListeEnseignants(List<IStructure> listeStructures, Integer annee) {
		return getListeEnseignants(listeStructures, annee, false, true, null);
	}


	/**
	 * Renvoie la liste des enseignants présents dans les structures
	 * @param listeStructures liste des structures à conserver
	 * @param annee annee en cours
	 * @param avecFicheServiceObligatoire  est-ce que l'on filtre avec la fiche de service ?
	 * @param filtrerAvecStructures doit-on filtrer les résultats avec la listeStructures passée en paramètre ?
	 * @param listeCodeEtape les listes d'étapes à conserver
	 * @return la liste des enseignants
	 */
	public EOQualifier getListeEnseignants(List<IStructure> listeStructures, Integer annee, boolean avecFicheServiceObligatoire, boolean filtrerAvecStructures,
			NSArray<EtapePeche> listeCodeEtape) {
		// ------------------------------------------------------------
		// On récupère la liste des enseignants affectés aux structures
		// ------------------------------------------------------------

		NSArray<EOQualifier> qualifierPourStructures = new NSMutableArray<EOQualifier>();
		

		if (filtrerAvecStructures  && !CollectionUtils.isEmpty(listeStructures)) {
			for (IStructure structure : listeStructures) {
				qualifierPourStructures.add(
						ERXQ.and(
								ERXQ.equals(EOActuelEnseignant.TO_AFFECTATION.dot(EOAffectation.C_STRUCTURE_KEY).key(), structure.cStructure()),
								ERXQ.equals(EOActuelEnseignant.TO_AFFECTATION.dot(EOAffectation.TEM_PRINCIPALE_KEY).key(), Constante.OUI))		
						);
			}
		}

		// AFFECTATION
		ERXKeyValueQualifier dDebAffectationQual = ERXQ.lessThanOrEqualTo(
				EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_DEB_AFFECTATION_KEY).key(),
						OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinAffectationValideQual = ERXQ.greaterThanOrEqualTo(
				EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_FIN_AFFECTATION_KEY).key(),
						OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinAffectationIsNullQual = ERXQ
				.isNull(EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_FIN_AFFECTATION_KEY).key());

		ERXOrQualifier dFinAffectationQual = ERXQ.or(dFinAffectationValideQual, dFinAffectationIsNullQual);

		ERXKeyValueQualifier temValideQual = ERXQ.equals(
				EOActuelEnseignant.TO_AFFECTATION.dot(EOAffectation.TEM_VALIDE_KEY).key(), Constante.OUI);

		// Date de début et de fin du contrat ou de l'élement de carrière
		ERXKeyValueQualifier dDebElementOuContratQual = 
				ERXQ.lessThanOrEqualTo(EOActuelEnseignant.DATE_DEBUT_KEY,
						OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinElementOuContratQual = 
				ERXQ.greaterThanOrEqualTo(EOActuelEnseignant.DATE_FIN_KEY,
						OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinElementOuContratIsNullQual = ERXQ.isNull(EOActuelEnseignant.DATE_FIN_KEY);

		ERXOrQualifier dateFinElementOuContratQual = ERXQ.or(dFinElementOuContratQual, dFinElementOuContratIsNullQual);

		//Témoin statutaire à Oui
		ERXKeyValueQualifier temStatutaireQual = ERXQ.equals(EOActuelEnseignant.TEM_STATUTAIRE.key(), Constante.OUI);

		NSArray<EOQualifier> qualifierAutres = new NSMutableArray<EOQualifier>();

		qualifierAutres.add(dDebAffectationQual);
		qualifierAutres.add(dFinAffectationQual);
		qualifierAutres.add(dDebElementOuContratQual);
		qualifierAutres.add(dateFinElementOuContratQual);
		qualifierAutres.add(temValideQual);
		qualifierAutres.add(temStatutaireQual);

		EOQualifier ficheServiceExistanteQual = new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), annee), EOActuelEnseignant.LISTE_SERVICES.key());

		String codeCircuit = determinerCircuitSouhaite();
		if (!StringUtils.isEmpty(codeCircuit)) {
			
			NSArray<EOQualifier> qualifierPourEtapes = new NSMutableArray<EOQualifier>();

			
			if (!NSArrayCtrl.isEmpty(listeCodeEtape)) {
				for (EtapePeche etapePeche : listeCodeEtape) {
					qualifierPourEtapes.add(
							ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(
									EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.CODE_ETAPE))).key(), etapePeche.getCodeEtape()));
				}
			}
			
			
			String destinationAttributeName = GRHUtilities.relationshipName(EOActuelEnseignant.class.getSimpleName(), EOActuelEnseignant.LISTE_SERVICES_KEY);

			EOQualifier codeCircuitSurAnneeQual = new ERXQualifierInSubquery(ERXQ.and(ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(
					EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.TO_CIRCUIT_VALIDATION.dot(
							EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY)))).key(), codeCircuit), 
							ERXQ.equals(EOService.ANNEE.key(), annee), ERXQ.or(qualifierPourEtapes)), EOService.ENTITY_NAME, EOActuelEnseignant.NO_DOSSIER_PERS_KEY, destinationAttributeName);


			if (codeCircuit.equals(CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit())) {

				if (!avecFicheServiceObligatoire) {

					EOQualifier listeServiceNullQual = 
							ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), annee), EOActuelEnseignant.LISTE_SERVICES.key()));

					qualifierAutres.add(ERXQ.or(codeCircuitSurAnneeQual, listeServiceNullQual));

				} else {

					qualifierAutres.add(codeCircuitSurAnneeQual);

				}

			} else {
				// Circuit PECHE_STATUTAIRE_DEFINITIF
				qualifierAutres.add(codeCircuitSurAnneeQual);
			}

		} else {
			if (avecFicheServiceObligatoire) {
				qualifierAutres.add(ficheServiceExistanteQual);
			}
		}

		EOQualifier qualifier;

		if (filtrerAvecStructures) {
			qualifier = ERXQ.and(ERXQ.or(qualifierPourStructures),
					ERXQ.and(qualifierAutres));
		} else {
			qualifier = ERXQ.and(qualifierAutres);
		}

		return qualifier;
	}

	
	
	
	
	/**
	 * En fonction du type de fiche 
	 * @return CircuitPeche.PECHE_STATUTAIRE / CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL
	 */
	private String determinerCircuitSouhaite() {
		String codeCircuit = null;

		if (getTypePopulation() != null) {
			if (Constante.DEFINITIF.equals(getTypePopulation())) {
				codeCircuit = CircuitPeche.PECHE_STATUTAIRE.getCodeCircuit();
			} else if (Constante.PREVISIONNEL.equals(getTypePopulation())) {
				codeCircuit = CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit();
			}
		}
		return codeCircuit;
	}


	/**
	 * renvoie la fiche de voeux d'un enseignant
	 * @param enseignant : enseignant
	 * @return fiche de voeux
	 */
	public EOFicheVoeux getFicheVoeux(EOActuelEnseignant enseignant) {
		return enseignant.getFicheVoeux(getAnneeUniversitaire());
	}

	/**
	 * Renvoie la liste des enseignants statutaires de toutes les composantes filtré par le nom
	 * @param nom : nom de l'enseignant (facultatif)
	 * @param listeStructures Une liste de structures (facultatif)
	 * @param annee annee en cours
	 * @return liste enseignants statutaires filtrés
	 */
	public EOQualifier getListeEnseignantsStatutairesParNomEtStructure(String nom, List<IStructure> listeStructures, Integer annee) {
		EOQualifier qualifier;

		if (listeStructures != null) {
			qualifier = getListeEnseignants(listeStructures, annee);
		} else {
			qualifier = getListeEnseignants(listeStructures, annee, false, false, null);
		}

		if (!StringCtrl.isEmpty(nom)) {
			qualifier = ERXQ.and(qualifier, EOActuelEnseignant.TO_INDIVIDU.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(nom));
		}

		return qualifier;
	}

	/**
	 * valide la fiche et la fait avancer à l'étape suivante
	 * @param repartService concerné
	 * @param personne connectée
	 * @return vrai si la fiche change de circuit 
	 */
	public boolean validerFiche(EORepartService repartService, Integer personne) {
		boolean isChangementCircuit = false;
		
		EODemande demande = repartService.toDemande();
		demande.faireAvancerSurChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), personne);

		if (demande.toEtape().isFinale() && isCircuitPrevisionnel(demande)) {
			demande.changerCircuit(Constante.APP_PECHE, CircuitPeche.PECHE_STATUTAIRE.getCodeCircuit(), personne);
			isChangementCircuit = true;
		}
		for (EOServiceDetail sd : repartService.toListeServiceDetail()) {
			sd.setEtat(null);
		}
		return isChangementCircuit;
	}

	/**
	 * Renvoie vrai si le circuit de la demande est le circuit prévisionnel
	 * @param demande : la fiche de service
	 * @return vrai/faux
	 */
	public boolean isCircuitPrevisionnel(EODemande demande) {
		return demande.toEtape().toCircuitValidation().codeCircuitValidation().equals(CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit());
	}

	/**
	 * Renvoie le service dû de l'enseignant
	 * 
	 * @param enseignant : enseignant
	 * @param annee Année universitaire
	 * @return le service dû
	 */
	public double getServiceDu(EOActuelEnseignant enseignant, Integer annee) {
		EOService service = enseignant.getService(annee);
		if ((service != null) && (service.heuresServiceDu() != null)) {
			return service.getHeuresServiceDu().doubleValue();			
		} else {
			return enseignant.getNbHeuresServiceStatutaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()) 
					- hceCtrl.getTotalNbHeuresReduction(enseignant, annee, isFormatAnneeExerciceAnneeCivile());
		}
	}

	/**
	 * Renvoie le statut de l'enseignant statutaire
	 * @param enseignant : enseignant
	 * @return statut Titulaire/Non titulaire
	 */
	public String getStatut(EOActuelEnseignant enseignant) {	
		if (enseignant.isTitulaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile())) {
			return Constante.TITULAIRE;
		} else {
			return Constante.NON_TITULAIRE;
		}
	}

	// * *************************************************** *
	// * Décharges/Absences (Modalité/Minoration/Position)   *
	// * *************************************************** *
	/**
	 * @param enseignant L'enseignant
	 * @param annee Année universitaire
	 * @return Les minorations de services d'un enseignant
	 */
	public NSArray<MinorationsServiceDataBean> rechercherReductionsService(EOActuelEnseignant enseignant, Integer annee) {
		NSArray<MinorationsServiceDataBean> listeMinorationsService = new NSMutableArray<MinorationsServiceDataBean>();

		if (listeMinorationsService.size() == 0) {
			//On récupère toutes les décharges de l'enseignant
			for (EODecharge decharge : getListeDecharges(enseignant.toIndividu())) {
				listeMinorationsService.add(new MinorationsServiceDataBean(decharge));
			}

			//On récupère toutes les absences de l'enseignant
			for (EOVueAbsences absence : hceCtrl.getListeAbsences(enseignant, annee, isFormatAnneeExerciceAnneeCivile())) {
				listeMinorationsService.add(new MinorationsServiceDataBean(absence, enseignant.getService(getAnneeUniversitaire())));
			}
		}

		return listeMinorationsService;
	}

	/**
	 * Renvoie la liste des décharges courantes de l'enseignant
	 * @param individu : enseignant
	 * @return décharges
	 */
	public NSArray<EODecharge> getListeDecharges(EOIndividu individu) {
		return EODecharge.dechargeAnneeUniversitaire(edc(), individu, getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * Renvoie le total du nombre d'heures souhaitées par enseignant
	 * @param enseignant : enseignant
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresSouhaitees(EOActuelEnseignant enseignant) {
		EOFicheVoeux ficheVoeux = getFicheVoeux(enseignant);

		if (ficheVoeux != null) {

			NSArray<EOFicheVoeuxDetail> fichesVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails();

			double nbHeuresSouhaitees = 0.0;

			for (EOFicheVoeuxDetail ficheVoeuxDetail : fichesVoeuxDetail) {
				nbHeuresSouhaitees += ficheVoeuxDetail.heuresSouhaitees();
			}
			return nbHeuresSouhaitees;
		}

		return 0;
	}

	/**
	 * Renvoie les AP souhaitées par un enseignant
	 * @param enseignant : enseignant
	 * @return liste d'EOAP
	 */
	public NSArray<EOAP> getAPSouhaites(EOActuelEnseignant enseignant) {
		EOFicheVoeux ficheVoeux = getFicheVoeux(enseignant);

		NSArray<EOAP> listeAP = new NSMutableArray<EOAP>();

		if (ficheVoeux != null) {
			NSArray<EOFicheVoeuxDetail> fichesVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails();

			for (EOFicheVoeuxDetail ficheVoeuxDetail : fichesVoeuxDetail) {
				if (!listeAP.contains(ficheVoeuxDetail.composantAP())) {
					listeAP.add(ficheVoeuxDetail.composantAP());
				}
			}
		}

		return listeAP;
	}

	/**
	 * Renvoie le nombre d'heures souhaitées par enseignant pour un AP donné
	 * @param enseignant : enseignant
	 * @param ap : atome pédagogique
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresSouhaiteesParAP(EOActuelEnseignant enseignant, EOAP ap) {
		EOFicheVoeux ficheVoeux = getFicheVoeux(enseignant);
		double nbHeuresSouhaitees = 0.0;

		if (ficheVoeux != null) {
			NSArray<EOFicheVoeuxDetail> fichesVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails();

			for (EOFicheVoeuxDetail ficheVoeuxDetail : fichesVoeuxDetail) {
				if (ficheVoeuxDetail.composantAP() == ap) {
					nbHeuresSouhaitees += ficheVoeuxDetail.heuresSouhaitees();
				}
			}
		}

		return nbHeuresSouhaitees;
	}
	
	/**
	 * Renvoie le nombre d'heures réalisées par enseignant pour un AP donné
	 * @param enseignant : enseignant
	 * @param ap : atome pédagogique
	 * @return nombre d'heures réalisées
	 */
	public double getNbHeuresRealiseesParAP(EOActuelEnseignant enseignant, EOAP ap) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		double nbHeuresRealisees = 0.0;

		if (service != null) {
			NSArray<EOServiceDetail> serviceDetails = service.listeServiceDetails();

			for (EOServiceDetail serviceDetail : serviceDetails) {
				if (ap.equals(serviceDetail.composantAP())) {
					nbHeuresRealisees += serviceDetail.heuresRealisees();
				}
			}
		}

		return nbHeuresRealisees;
	}

	/**
	 * @param service : service de l'enseignant
	 * @return total des heures à payer + heures à reporter + heures gratuites + heures payees
	 */
	private BigDecimal getTotalHeuresComplementaires(EOService service) {
		return OutilsValidation.retournerBigDecimalNonNull(service.getHeuresAPayer().add(service.getHeuresAReporterAttribuees()).
				add(service.getHeuresGratuites()).add(service.getHeuresPayees()));
	}

	/**
	 * Est-ce que le total des heures à payer, reportées, gratuites et payées est égal aux heures complémentaires réalisées ?
	 * @param service : service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean getTotalHcompOK(EOService service) {
		boolean totalHcompOK = false;
		double hcompRealise = getHcompRealise(service);

		if (!isCircuitPrevisionnel(getDemande(service))) {
			if (getTotalHeuresComplementaires(service).doubleValue() == hcompRealise) {
				totalHcompOK = true;
			}
		}

		return totalHcompOK;
	}


	/**
	 * @param enseignant enseignant en cours
	 * @param ficheVoeux ficheVoeux à basculer
	 * @param pecheSession session
	 * @param listeAPsEnDepassement liste des APs sur lequel un arbitrage doit être fait
	 */
	public void basculerFicheVoeuxEnFicheService(EOActuelEnseignant enseignant, EOFicheVoeux ficheVoeux, Session pecheSession, NSArray<EOAP> listeAPsEnDepassement) {

		EOService service = enseignant.getService(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
		if (service == null) {

			EOService serviceACreer = EOService.creerEtInitialiser(edc());
			EORepartService repartService = EORepartService.creerNouvelRepartService(edc(), serviceACreer, null, pecheSession
					.getApplicationUser().getPersonne(), CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());

			NSArray<EOFicheVoeuxDetail> listeFicheVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails();

			for (EOFicheVoeuxDetail ficheVoeuxDetail : listeFicheVoeuxDetail) {

				if (!listeAPsEnDepassement.contains(ficheVoeuxDetail.composantAP())) {
					EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(edc());
					serviceDetail.setComposantAP(ficheVoeuxDetail.composantAP());
					serviceDetail.setHeuresPrevues(ficheVoeuxDetail.heuresSouhaitees());
					StringBuffer commentaires = new StringBuffer();
					if (serviceDetail.commentaire() != null) {
						commentaires.append(serviceDetail.commentaire()).append("\n");
					}
					commentaires.append(DateCtrl.dateToString(DateCtrl.now()));
					commentaires.append(":").append(Constante.COMMENTAIRE_COPIE_AUTOMATIQUE);
					serviceDetail.setCommentaire(commentaires.toString());
					serviceDetail.setService(serviceACreer);
					serviceDetail.majDonnesAuditCreation(pecheSession.getApplicationUser().getPersonne());
					serviceDetail.setToRepartService(repartService);
					ficheVoeuxDetail.setCommentaire(Constante.COMMENTAIRE_COPIE_AUTOMATIQUE);
				} else {
					ficheVoeuxDetail.setHeuresSouhaitees(0.0);
				}
			}

			serviceACreer.setTemoinEnseignantGenerique(Constante.NON);
			serviceACreer.setTemoinValide(Constante.OUI);
			serviceACreer.majDonnesAuditCreation(pecheSession.getApplicationUser().getPersonne());
			serviceACreer.setEnseignant(enseignant);
			serviceACreer.setAnnee(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());

			double serviceStatutaire = HceCtrl.arrondirAuCentieme(enseignant.getNbHeuresServiceStatutaire(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours(), pecheSession.getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(enseignant, pecheSession.getPecheParametres().getAnneeUniversitaireEnCours(), pecheSession.getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			serviceACreer.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
			serviceACreer.setHeuresService(BigDecimal.valueOf(serviceStatutaire));

			pecheSession.addSimpleMessage(TypeMessage.INFORMATION, Constante.FICHE_PREVISIONNEL_CREE);

		} else {
			pecheSession.addSimpleMessage(TypeMessage.ERREUR, Constante.BASCULE_FICHE_VOEUX_NON_REALISEE);
		}

	}

	/**
	 * 
	 * @param enseignantsArray liste enseignants
	 * @param anneeSource  annee de base
	 * @param anneeCible   année cible
	 * @param personneConnecte personne connectée
	 * @return nombre de duplications
	 */
	public int dupliquerEnseignants(NSArray<EOActuelEnseignant> enseignantsArray, Integer anneeSource, Integer anneeCible, EOPersonne personneConnecte) {

		int nbDuplications = 0;

		if (!NSArrayCtrl.isEmpty(enseignantsArray)) {

			EOQualifier qualifier = EOReh.getQualifierPourListeRehVisibles(anneeCible, 
					isFormatAnneeExerciceAnneeCivile());
			NSArray<EOReh> listeRehsAnneeCible = EOReh.fetchEORehs(edc(), qualifier, null);

			if (EOPecheParametre.isFicheVoeuxUtilisation(edc(), anneeCible)) {

				nbDuplications = dupliquerEnseignantsUtilisationFicheVoeux(
						enseignantsArray, anneeSource, anneeCible,
						personneConnecte, nbDuplications);

			} else {

				nbDuplications = dupliquerEnseignantsNonUtilisationFicheVoeux(
						enseignantsArray, anneeSource, anneeCible,
						personneConnecte, nbDuplications, listeRehsAnneeCible);
			}
			edc().saveChanges();
		}
		return nbDuplications;
	}


	private int dupliquerEnseignantsNonUtilisationFicheVoeux(
			NSArray<EOActuelEnseignant> enseignantsArray, Integer anneeSource,
			Integer anneeCible, EOPersonne personneConnecte,
			int nbDuplications, NSArray<EOReh> listeRehsAnneeCible) {
		for (EOActuelEnseignant enseignant : enseignantsArray) {				

			EOService serviceSource = enseignant.getService(anneeSource);
			EOService serviceAnneeCible = null;

			serviceAnneeCible = enseignant.getService(anneeCible);

			if (serviceAnneeCible == null) {
				serviceAnneeCible = EOService.creerEtInitialiser(edc());
				serviceAnneeCible.majDonnesAuditCreation(personneConnecte);
				serviceAnneeCible.setAnnee(anneeCible);
				serviceAnneeCible.setEnseignant(enseignant);
				serviceAnneeCible.setTemoinValide(Constante.OUI);
				serviceAnneeCible.setTemoinEnseignantGenerique(Constante.NON);
			}

			NSArray<EORepartService> listeRepartServiceCible = serviceAnneeCible.toListeRepartService();
			EORepartService repartServiceCible;

			if (listeRepartServiceCible == null) {
				repartServiceCible = EORepartService.creerNouvelRepartService(edc(), serviceAnneeCible, null, personneConnecte, CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), anneeCible);
			} else {
				repartServiceCible = listeRepartServiceCible.get(0);
			}

			//|| (listeRepartServiceCible != null && listeRepartServiceCible.isEmpty()
			NSArray<EOServiceDetail> serviceListeServiceDetails = serviceAnneeCible.listeServiceDetails();
			
			EODemande demande = repartServiceCible.toDemande();
			boolean etapeValide = false;
			if (demande.estSurEtapeInitiale()
					&& CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit().equals(demande.toEtape().toCircuitValidation().codeCircuitValidation())) {
				etapeValide = true;
			}

			if ((serviceListeServiceDetails) == null ||	 (serviceListeServiceDetails != null && serviceListeServiceDetails.isEmpty()) 
					&& (serviceSource != null)
					&& etapeValide) {

				NSArray<EOServiceDetail> serviceDetailArray =  serviceSource.listeServiceDetails();

				for (EOServiceDetail eoServiceDetail : serviceDetailArray) {

					if (eoServiceDetail.composantAP() != null 
							&& eoServiceDetail.composantAP().getComposantSuivant() != null) {
						
						EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(edc());
						EOAP apSuivant = (EOAP) eoServiceDetail.composantAP().getComposantSuivant();
						serviceDetail.setComposantAP(apSuivant);	
						serviceDetail.majDonnesAuditCreation(personneConnecte);
						serviceDetail.setCommentaire(Constante.COMMENTAIRE_COPIE_AUTOMATIQUE_ANNEE_PRECEDENTE);
						serviceDetail.setHeuresPrevues(eoServiceDetail.heuresRealisees());
						serviceDetail.setService(serviceAnneeCible);								
						serviceDetail.setToRepartService(repartServiceCible);
						if (eoServiceDetail.etat()!=null) {
							serviceDetail.setEtat(Constante.ATTENTE);
						}
					} else if (eoServiceDetail.reh() != null) {

						if (listeRehsAnneeCible.containsObject(eoServiceDetail.reh())) {

							EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(edc());
							serviceDetail.setReh(eoServiceDetail.reh());
							serviceDetail.setToStructure(eoServiceDetail.toStructure());
							serviceDetail.majDonnesAuditCreation(personneConnecte);
							serviceDetail.setCommentaire(Constante.COMMENTAIRE_COPIE_AUTOMATIQUE_ANNEE_PRECEDENTE);
							serviceDetail.setHeuresPrevues(eoServiceDetail.heuresRealisees());
							serviceDetail.setService(serviceAnneeCible);									
							serviceDetail.setToRepartService(repartServiceCible);

						}	
					}
				}
				nbDuplications++;
			}
			
			edc().saveChanges();
			serviceAnneeCible.majDonnesServiceDuStatutaire(edc(), personneConnecte, isFormatAnneeExerciceAnneeCivile());
			serviceAnneeCible.majDonnesServiceReparti(edc(), personneConnecte, isFormatAnneeExerciceAnneeCivile());
			
		}
		return nbDuplications;
	}


	private int dupliquerEnseignantsUtilisationFicheVoeux(
			NSArray<EOActuelEnseignant> enseignantsArray, Integer anneeSource,
			Integer anneeCible, EOPersonne personneConnecte, int nbDuplications) {
		for (EOActuelEnseignant enseignant : enseignantsArray) {

			EOService serviceSource = enseignant.getService(anneeSource);	
			EOFicheVoeux ficheVoeuxCible = null;

			NSArray<EOFicheVoeux> listeFichesVoeux = enseignant.listeFichesVoeux(EOFicheVoeux.ANNEE.eq(anneeCible));
			if (listeFichesVoeux.size() > 0) {					
				ficheVoeuxCible = listeFichesVoeux.get(0);
			}

			if (ficheVoeuxCible == null) {					
				ficheVoeuxCible = EOFicheVoeux.creerEtInitialiser(edc(), personneConnecte.persId());
				ficheVoeuxCible.setAnnee(anneeCible);
				ficheVoeuxCible.setPersonneCreation(personneConnecte);
				ficheVoeuxCible.setDateCreation(DateCtrl.now());
				ficheVoeuxCible.setTemoinValide(Constante.OUI);
				ficheVoeuxCible.setEnseignant(enseignant);
			}

			NSArray<EOFicheVoeuxDetail> voeuxDetailCibleArray =  ficheVoeuxCible.listeFicheVoeuxDetails();

			if ((voeuxDetailCibleArray) == null ||	(voeuxDetailCibleArray != null	&& voeuxDetailCibleArray.isEmpty()) 
					&& (serviceSource != null)) {
				NSArray<EOServiceDetail> serviceDetailArray =  serviceSource.listeServiceDetails();
				for (EOServiceDetail eoServiceDetail : serviceDetailArray) {
					if (eoServiceDetail.composantAP() != null && eoServiceDetail.composantAP().getComposantSuivant() != null) {
						EOFicheVoeuxDetail ficheVoeuxDetail = EOFicheVoeuxDetail.creerEtInitialiser(edc());

						EOAP apSuivant = (EOAP) eoServiceDetail.composantAP().getComposantSuivant();
						ficheVoeuxDetail.setComposantAP(apSuivant);	
						ficheVoeuxDetail.majDonneesAuditCreation(DateCtrl.now(), personneConnecte);
						ficheVoeuxDetail.setCommentaire(DateCtrl.dateToString(DateCtrl.now()) + ":" + Constante.COMMENTAIRE_COPIE_AUTOMATIQUE_ANNEE_PRECEDENTE);
						ficheVoeuxDetail.setHeuresSouhaitees(eoServiceDetail.heuresRealisees());
						ficheVoeuxDetail.setFicheVoeux(ficheVoeuxCible);
						
					}
				}
				nbDuplications++;
			}
			
			ficheVoeuxCible.majDonnees(edc(), DateCtrl.now(), personneConnecte, anneeCible, isFormatAnneeExerciceAnneeCivile());
		}
		return nbDuplications;
	}

	/**
	 * Renvoie la liste de tous les enseignants statutaires de l'établissement
	 * sur une année universitaire
	 * @param annee année en cours
	 * @return qualifier
	 */
	public EOQualifier getListeTousLesEnseignantsStatutaires(Integer annee, boolean avecFicheServiceObligatoire, NSArray<EtapePeche> listeCodeEtape) {
		return getListeEnseignants(null, annee, avecFicheServiceObligatoire, false, listeCodeEtape);
	}



	public String getTypePopulation() {
		return typePopulation;
	}


	public void setTypePopulation(String typePopulation) {
		this.typePopulation = typePopulation;
	}

	/**
	 * Retourne le report de l'année antérieure si cette fiche de service a bien été validée
	 * @param editedObject actuelEnseignant
	 * @param anneeUniversitairePrecedente annee
	 * @return report
	 */
	public Double getReportAnneeAnterieure(EOActuelEnseignant editedObject,
			Integer anneeUniversitairePrecedente) {

		EOService servicePrecedent  = editedObject.getService(anneeUniversitairePrecedente);

		BigDecimal reportAnneeAnterieure = BigDecimal.ZERO;

		if (servicePrecedent != null && !servicePrecedent.toListeRepartService().isEmpty() && controleurDemande.isDemandeValidee(getDemande(servicePrecedent))) {
			reportAnneeAnterieure = servicePrecedent.getHeuresAReporterAttribuees();
		} 


		return reportAnneeAnterieure.doubleValue();
	}

	/**
	 * Retourne le flux XML nécessaire à l'édition d'une fiche de service.
	 * 
	 * @param enseignant : l'enseignant
	 * @param annee : l'année universitaire
	 * @param pecheSession : la	 session en cours
	 * @param componentCtrl : le CktlPecheBaseComponentCtrl instancié
	 * @param titre : le titre qui apparaitra dans le flux
	 * @return le flux XML
	 */
	public String getFluxFicheService(EOActuelEnseignant enseignant, Integer annee,Session pecheSession,CktlPecheBaseComponentCtrl componentCtrl, String titre) {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			w.setEscapeSpecChars(true);
			w.startDocument();
			w.startElement("fiche_service");
			{
				EOService service = enseignant.getService(annee);
				EODemande demande = getDemande(service);
				boolean isCircuitPrev = isCircuitPrevisionnel(demande);

				if (isCircuitPrev) {
					writeElement(w, "titre", titre + " " + Constante.PREVISIONNEL);
				}	else {
					writeElement(w, "titre", titre + " " + Constante.DEFINITIF);
				}

				if (isFormatAnneeExerciceAnneeCivile()) {
					writeElement(w, "annee_universitaire", String.valueOf(annee));
				} else {
					writeElement(w, "annee_universitaire", annee + "/" + (annee + 1));
				}				

				if (EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY) != null) {
					writeElement(w, "etablissement", EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY));
				}
				writeElement(w, "date_fiche", DateCtrl.dateToString(DateCtrl.now(), "dd/MM/yyyy"));

				String cLogo = LogoEdition.cheminLogoEtablissement(edc());
				if (cLogo != null) {
					writeElement(w, "logo_etablissement", cLogo);
				}

				if (service.commentaire() != null) {
					w.writeElement("commentaire", service.commentaire());
				}

				w.startElement("profil");
				{
					writeElement(w, "nom", enseignant.toIndividu().nomAffichage().toUpperCase());
					writeElement(w, "prenom", enseignant.toIndividu().prenomAffichage());
					writeElement(w, "corps_contrat", getLibelleLongCorpsOuContrat(enseignant));

					EOStructure composante = enseignant.getComposante(annee, isFormatAnneeExerciceAnneeCivile());
					if (composante != null) {
						writeElement(w, "composante", composante.llStructure());
					}

					writeElement(w, "statut", enseignant.getStatut(annee, isFormatAnneeExerciceAnneeCivile()));

					EOStructure departement = enseignant.getDepartementEnseignement(annee, isFormatAnneeExerciceAnneeCivile());

					if (departement != null) {				
						writeElement(w, "departement", departement.llStructure());
					}
					
					String typePop = enseignant.getLibelleLongTypePopulation(annee, isFormatAnneeExerciceAnneeCivile());
					
					if (typePop.trim().length() > 0) {				
						writeElement(w, "typePopulation", typePop);
					}
					
					w.endElement();
				}

				w.startElement("serviceHETD");
				{
					writeElement(w, "methodeCalcul", enseignant.getMethodeCalcul(annee, isFormatAnneeExerciceAnneeCivile()));
					writeElement(w, "serviceStatutaire", service.getHeuresService());
					if (pecheSession.getPecheAutorisationCache().hasDroitUtilisationReportHeuresComplementaires()) {
						writeElement(w, "reportAnneeAnterieure", -getReportAnneeAnterieure(enseignant, getAnneeUniversitairePrecedente(annee)));
					} else {
						writeElement(w, "reportAnneeAnterieure", "-0,00");
					}
					// ATTENTION, format 2 chiffres 
					writeElement(w, "modaliteService", StringCtrl.checkString(new DecimalFormat("0.00").format(-hceCtrl.getTotalNbHeuresModaliteService(enseignant, annee, isFormatAnneeExerciceAnneeCivile()))));
					writeElement(w, "decharges", StringCtrl.checkString(new DecimalFormat("0.00").format(-hceCtrl.getTotalNbHeuresDecharges(enseignant.toIndividu(), annee, isFormatAnneeExerciceAnneeCivile()))));
					writeElement(w, "minorations", StringCtrl.checkString(new DecimalFormat("0.00").format(-hceCtrl.getTotalNbHeuresMinorations(enseignant, annee, isFormatAnneeExerciceAnneeCivile()))));
					double serviceDu = getServiceDu(enseignant, annee);
					writeElement(w, "serviceDu", serviceDu);
					writeElement(w, "servicePrevu", getServiceAttribue(enseignant));
					writeElement(w, "rehPrevu", getRehAttribue(enseignant));
					writeElement(w, "hCompPrevu", getHcompPrev(service));

					if (!isCircuitPrev) {
						double serviceRealise = hceCtrl.getServiceRealise(enseignant, annee, isFormatAnneeExerciceAnneeCivile(), service, serviceDu);
						writeElement(w, "serviceRealise", serviceRealise);
						writeElement(w, "rehRealise", getRehRealise(enseignant));
						writeElement(w, "hCompRealise", getHcompRealise(service));

						if (pecheSession.getPecheAutorisationCache().hasDroitUtilisationRepartitionHeuresComplementaires())	{
							double deltaRealise = serviceRealise - serviceDu;
							// sous-service
							if (pecheSession.getPecheAutorisationCache().hasDroitUtilisationReportHeuresComplementaires()
									&& (deltaRealise < 0)) {								
								writeElement(w, "sousServiceDeltaRealise", deltaRealise);
								writeElement(w, "sousServiceAReporter", service.getHeuresAReporterAttribuees());
							}
							// heures complementaires
							if (getHcompRealise(service) > 0) { 
								writeElement(w, "APayer", service.getHeuresAPayer());
								if (service.getHeuresPayees().doubleValue() > 0 ) {
									writeElement(w, "Payees", service.getHeuresPayees());
								}
								if (pecheSession.getPecheAutorisationCache().hasDroitUtilisationReportHeuresComplementaires()) {
									writeElement(w, "AReporter", service.getHeuresAReporterAttribuees());
								}
								writeElement(w, "ATitreGracieux", service.getHeuresGratuites());
							}
						}
					}
					w.endElement();
				}
				w.startElement("circuit");
				{
					DemandeCtrl demandeCtrl = new DemandeCtrl(edc());
					writeElement(w, "etat", demandeCtrl.etatEtape(demande));
					if (demandeCtrl.getDateInitialisation(demande) != null) {
						writeElement(w, "dateInit", "Initialisé le : " + DateCtrl.dateToString(demandeCtrl.getDateInitialisation(demande)));
					}

					NSArray<EOCircuitValidation> circuits = demande.rechercherCircuitsEmpruntes(true);
					StringBuffer circuitsLibelle = new StringBuffer();
					for (EOCircuitValidation circuit : circuits) {
						circuitsLibelle.append("\n").append(getLibelleFicheTypeCircuit(circuit)).append("\n");
						NSArray<EOEtape> etapes = getEtapesUtilisees(circuit);						
						for (EOEtape etape : etapes) {
							circuitsLibelle.append(String.format("%-20s", StringCtrl.chaineSansAccents(EtapePeche.getEtape(etape).getLibelleVisa() + ":")));							
							Map<String, EOHistoriqueDemande> mapVisas = demandeCtrl.getMapVisas(demande, circuit);
							EOHistoriqueDemande historiqueDemande = mapVisas.get(etape.codeEtape());
							if (historiqueDemande != null) {
								circuitsLibelle.append(DateCtrl.dateToString(historiqueDemande.dateModification()));	
							}
							circuitsLibelle.append("\n");
						}
					}
					w.writeElement("circuits", circuitsLibelle.toString());
					w.endElement();
				}

				NSArray<MinorationsServiceDataBean> listeMinorationsService = rechercherReductionsService(enseignant, annee);
				if (listeMinorationsService.size() > 0) {
					w.startElement("minorations");
					for (MinorationsServiceDataBean minorationsService : listeMinorationsService) {
						w.startElement("minoration");
						writeElement(w, "type", minorationsService.getType());
						writeElement(w, "libelle", minorationsService.getLibelle());
						writeElement(w, "periode", minorationsService.getPeriode());
						writeElement(w, "nbHeures", getNbHeuresMinoration(enseignant, minorationsService, annee));
						if (Constante.OUI.equalsIgnoreCase(minorationsService.getTemoinHcomp())) {
							writeElement(w, "temoinHcomp", Constante.OUI_LONG);
						} else {
							writeElement(w, "temoinHcomp", Constante.NON_LONG);
						}
						w.endElement();
					}
					w.endElement();
				}
				String path = ERXQ.keyPath(EOServiceDetail.SERVICE_KEY);
				EOQualifier qualifierReh =	ERXQ.and(ERXQ.equals(path, service), ERXQ.isNotNull(EOServiceDetail.REH_KEY));
				NSArray <EOServiceDetail> listeServicedetailReh = service.listeServiceDetails(qualifierReh);
				if (listeServicedetailReh.size() > 0) {
					w.startElement("rehs");
					for (EOServiceDetail serviceDetail : listeServicedetailReh) {
						w.startElement("reh");
						writeElement(w, "libelle", serviceDetail.reh().getLibelleLongCourt());
						writeElement(w, "composante", serviceDetail.toStructure().lcStructure());
						if (serviceDetail.commentaire() != null) {
							writeElement(w, "commentaire", serviceDetail.commentaire());
						}
						writeElement(w, "heuresPrevues", serviceDetail.heuresPrevues());
						if (!isCircuitPrevisionnel(demande)) {
							writeElement(w, "heuresRealisees", serviceDetail.heuresRealisees());
						}
						w.endElement();
					}
					w.endElement();
				}

				ComposantCtrl composantCtrl = new ComposantCtrl(edc(), annee);
				EOQualifier qualifierService =	ERXQ.and(ERXQ.equals(path, service), ERXQ.isNotNull(EOServiceDetail.COMPOSANT_AP_KEY));
				NSArray <EOServiceDetail> listeServicedetail = service.listeServiceDetails(qualifierService);
				HashMap<String, Double> mapHeuresPrevues = new HashMap<String, Double>();
				HashMap<String, Double> mapHeuresRealisees = new HashMap<String, Double>();

				if (listeServicedetail.size() > 0) {
					w.startElement("repartitionService");
					for (EOServiceDetail serviceDetail : listeServicedetail) {
						w.startElement("service");
						writeElement(w, "composante", composantCtrl.getComposanteParentAP(serviceDetail));						
						writeElement(w, "enseignement",  ComposantAffichageHelper.getInstance().affichageEnseignementParent(serviceDetail.composantAP()));
						writeElement(w, "type", serviceDetail.composantAP().typeAP().code());
						
						writeElement(w, "heuresPrevues", serviceDetail.heuresPrevues());
						if (mapHeuresPrevues.containsKey(serviceDetail.composantAP().typeAP().code())) {
							double value = mapHeuresPrevues.get(serviceDetail.composantAP().typeAP().code());
							mapHeuresPrevues.put(serviceDetail.composantAP().typeAP().code(), value + serviceDetail.heuresPrevues());
						} else {
							mapHeuresPrevues.put(serviceDetail.composantAP().typeAP().code(), serviceDetail.heuresPrevues());
						}
						
						if (serviceDetail.commentaire() != null) {
							writeElement(w, "commentaire", serviceDetail.commentaire());
						}
						if (!isCircuitPrevisionnel(demande)) {
							writeElement(w, "heuresRealisees", serviceDetail.heuresRealisees());
							if (mapHeuresRealisees.containsKey(serviceDetail.composantAP().typeAP().code())) {
								double value = mapHeuresRealisees.get(serviceDetail.composantAP().typeAP().code());
								mapHeuresRealisees.put(serviceDetail.composantAP().typeAP().code(), value + serviceDetail.heuresRealisees());
							} else {
								mapHeuresRealisees.put(serviceDetail.composantAP().typeAP().code(), serviceDetail.heuresRealisees());
							}							
						}
						writeElement(w, "etat", componentCtrl.getEtatRepartition(serviceDetail.etat()));
						w.endElement();
					}

					w.endElement();
				}
				List<String> listeAPdansMap = new ArrayList<String>(mapHeuresPrevues.keySet());
				Collections.sort(listeAPdansMap);
				if (listeAPdansMap.size() > 0) {
					w.startElement("totauxAP");
					for (int i = 0; i < listeAPdansMap.size(); i++) {
						w.startElement("AP");
						writeElement(w, "type", listeAPdansMap.get(i));
						writeElement(w, "heuresPrevues", mapHeuresPrevues.get(listeAPdansMap.get(i)));
						if (mapHeuresRealisees.get(listeAPdansMap.get(i)) != null) {
							writeElement(w, "heuresRealisees",  mapHeuresRealisees.get(listeAPdansMap.get(i)));
						}
						w.endElement();
					}
					w.endElement();
				}
				
				
				w.endElement();
			}
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}
		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");	
		return sw.toString();
	}

	/**
	 * Retourne le flux XML nécessaire à l'édition d'une fiche de voeux.
	 * 
	 * @param enseignant : l'enseignant
	 * @param annee : l'année universitaire
	 * @param pecheSession : la	 session en cours
	 * @param componentCtrl : le CktlPecheBaseComponentCtrl instancié
	 * @param titre : le titre qui apparaitra dans le flux
	 * @return le flux XML
	 */
	public String getFluxFicheVoeux(EOActuelEnseignant enseignant, Integer annee,Session pecheSession,CktlPecheBaseComponentCtrl componentCtrl, String titre) {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			w.setEscapeSpecChars(true);
			w.startDocument();
			w.startElement("fiche_voeux");
			{
				// TODO: pour l'instant : non utilisé dans la fiche de voeux
				//Integer anneePrecedente = getAnneeUniversitairePrecedente(annee);
				EOFicheVoeux ficheVoeux = getFicheVoeux(enseignant);
				writeElement(w, "titre", titre);

				if (isFormatAnneeExerciceAnneeCivile()) {
					writeElement(w, "annee_universitaire", String.valueOf(annee));
				} else {
					writeElement(w, "annee_universitaire", annee + "/" + (annee + 1));
				}				

				if (EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY) != null) {
					writeElement(w, "etablissement", EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY));
				}
				writeElement(w, "date_fiche", DateCtrl.dateToString(DateCtrl.now(), "dd/MM/yyyy"));

				String cLogo = LogoEdition.cheminLogoEtablissement(edc());
				if (cLogo != null) {
					writeElement(w, "logo_etablissement", cLogo);
				}

				if (ficheVoeux.commentaire() != null) {
					w.writeElement("commentaire", ficheVoeux.commentaire());
				}

				w.startElement("profil");
				{
					writeElement(w, "nom", enseignant.toIndividu().nomAffichage().toUpperCase());
					writeElement(w, "prenom", enseignant.toIndividu().prenomAffichage());
					writeElement(w, "corps_contrat", getLibelleLongCorpsOuContrat(enseignant));

					EOStructure composante = enseignant.getComposante(annee, isFormatAnneeExerciceAnneeCivile());
					if (composante != null) {
						writeElement(w, "composante", composante.llStructure());
					}

					writeElement(w, "statut", enseignant.getStatut(annee, isFormatAnneeExerciceAnneeCivile()));

					EOStructure departement = enseignant.getDepartementEnseignement(annee, isFormatAnneeExerciceAnneeCivile());

					if (departement != null) {				
						writeElement(w, "departement", departement.llStructure());
					}

					w.endElement();
				}

				w.startElement("serviceHETD");
				{
					EOService service = enseignant.getService(annee);
					writeElement(w, "methodeCalcul", enseignant.getMethodeCalcul(annee, isFormatAnneeExerciceAnneeCivile()));
					writeElement(w, "serviceStatutaire", service.getHeuresService());
					writeElement(w, "reductionService", -hceCtrl.getTotalNbHeuresReduction(enseignant, annee, isFormatAnneeExerciceAnneeCivile()));
					writeElement(w, "serviceDu", getServiceDu(enseignant, annee));
					writeElement(w, "serviceSouhaite", getVoeuxSouhaite(enseignant));

					double delta = getVoeuxSouhaite(enseignant) - getServiceDu(enseignant, annee);
					writeElement(w, "delta", delta);
					w.endElement();
				}
				w.startElement("circuit");
				{

					DemandeCtrl demandeCtrl = new DemandeCtrl(edc());
					EODemande demande = ficheVoeux.toDemande();
					String etatEtape = demandeCtrl.etatEtape(demande);
					if (EtapePeche.VALIDEE.getEtatDemande().equals(etatEtape)) {
						writeElement(w, "etat", Constante.ETAT_BASCULE);
					} else {
						writeElement(w, "etat", demandeCtrl.etatEtape(demande));
					}
					if (demandeCtrl.getDateInitialisation(demande) != null) {
						writeElement(w, "dateInit", "Initialisé le : " + DateCtrl.dateToString(demandeCtrl.getDateInitialisation(demande)));
					}

					NSArray<EOCircuitValidation> circuits = demande.rechercherCircuitsEmpruntes(true);
					StringBuffer circuitsLibelle = new StringBuffer();
					for (EOCircuitValidation circuit : circuits) {
						circuitsLibelle.append("\n");
						NSArray<EOEtape> etapes = getEtapesUtilisees(circuit);
						for (EOEtape etape : etapes) {
							circuitsLibelle.append(EtapePeche.getEtape(etape).getLibelleVisa()).append(" : ");							
							Map<String, EOHistoriqueDemande> mapVisas = demandeCtrl.getMapVisas(demande, circuit);
							EOHistoriqueDemande historiqueDemande = mapVisas.get(etape.codeEtape());
							if (historiqueDemande != null) {
								circuitsLibelle.append(DateCtrl.dateToString(historiqueDemande.dateModification()));	
							}
							circuitsLibelle.append("\n");
						}
					}
					w.writeElement("circuits", circuitsLibelle.toString());
					w.endElement();
				}

				NSArray<MinorationsServiceDataBean> listeMinorationsService = rechercherReductionsService(enseignant, annee);
				if (listeMinorationsService.size() > 0) {
					w.startElement("minorations");
					for (MinorationsServiceDataBean minorationsService : listeMinorationsService) {
						w.startElement("minoration");
						writeElement(w, "type", minorationsService.getType());
						writeElement(w, "libelle", minorationsService.getLibelle());
						writeElement(w, "periode", minorationsService.getPeriode());
						writeElement(w, "nbHeures", getNbHeuresMinoration(enseignant, minorationsService, annee));
						if (Constante.OUI.equalsIgnoreCase(minorationsService.getTemoinHcomp())) {
							writeElement(w, "temoinHcomp", Constante.OUI_LONG);
						} else {
							writeElement(w, "temoinHcomp", Constante.NON_LONG);
						}
						w.endElement();
					}
					w.endElement();
				}

				ComposantCtrl composantCtrl = new ComposantCtrl(edc(), annee);

				String path = ERXQ.keyPath(EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
				EOQualifier qualifier = ERXQ.equals(path, ficheVoeux);
				NSArray<EOFicheVoeuxDetail> listeVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails(qualifier);
				if (listeVoeuxDetail.size() > 0) {
					w.startElement("repartitionService");
					for (EOFicheVoeuxDetail voeuxDetail : listeVoeuxDetail) {
						w.startElement("service");
						if (voeuxDetail.composantAP() != null) {								
							writeElement(w, "composante", composantCtrl.getComposanteParentAP(voeuxDetail.composantAP()));
							writeElement(w, "enseignement", ComposantAffichageHelper.getInstance().affichageEnseignementParent(voeuxDetail.composantAP()));
							writeElement(w, "type", voeuxDetail.composantAP().typeAP().code());
						} else {
							writeElement(w, "composante", voeuxDetail.etablissementExterne());
							writeElement(w, "enseignement", voeuxDetail.enseignementExterne());
							writeElement(w, "type", "");
						}
						writeElement(w, "heuresSouhaitees", voeuxDetail.heuresSouhaitees());
						if (voeuxDetail.commentaire() != null) {
							writeElement(w, "commentaire", voeuxDetail.commentaire());
						}
						w.endElement();
					}
					w.endElement();
				}

				w.endElement();
			}
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}

		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");	
		return sw.toString();
	}

	public String getNbHeuresMinoration(EOActuelEnseignant enseignant, MinorationsServiceDataBean minorationsService, Integer annee) {
		if (minorationsService.getAbsence() != null) {
			//Si le nombre d'heures de la modalité de service n'est pas renseigné => on calcule au prorata 
			if (minorationsService.getAbsence().isModalite() && minorationsService.getNbHeures() == null) {
				return String.format("%.2f", hceCtrl.getNbHeuresModaliteService(enseignant, minorationsService.getAbsence(), 
						annee, isFormatAnneeExerciceAnneeCivile()));
			}
		}

		if (minorationsService.getNbHeures() == null) {
			return Constante.NON_DEFINI;
		}

		return String.format("%.2f", minorationsService.getNbHeures());
	}


}
