package org.cocktail.peche.components.controlers;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EORepartTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.entity.interfaces.HasComposantAP;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Contrôleur pour les composant (SCOL).
 */
public class ComposantCtrl {
	
	
	private UECtrl controleurUE;
	
	/**
	 * 
	 */
	public ComposantCtrl() {
		
	}
	
	/**
	 * 
	 * @param editingContext edc 
	 * @param annee annee en cours
	 */
	public ComposantCtrl(EOEditingContext editingContext, Integer annee) {
		super();
		this.controleurUE = new UECtrl(editingContext, annee);
	}

	/**
	 * Retourne la structure définie pour le composant.
	 * Si la structure n'est pas définie pour ce composant, on recherche la structure sur le(s) parent(s) (en ainsi de suite).
	 * 
	 * @param composant le composant
	 * @return la structure (ou <code>null</code> si pas définie)
	 */
	public EOStructure getStructure(EOComposant composant) {
		if (composant == null) {
			return null;
		}
		
		EOStructure structure = null;
		List<IComposant> listeComposant = new ArrayList<IComposant>(1);
		listeComposant.add(composant);
		List<IComposant> listeComposantParent = new ArrayList<IComposant>();
		
		while (listeComposant.size() > 0) {
			structure = getStructure(listeComposant, listeComposantParent);
			
			if (structure != null) {
				break;
			} else {
				listeComposant.clear();
				listeComposant.addAll(listeComposantParent);
				listeComposantParent.clear();
			}
		}
		
		return structure;
	}

	/**
	 * Recherche la structure pour la liste des composants de la liste.
	 * 
	 * @param listeComposant les composants pour lequels il faut recherche une structure
	 * @param listeComposantParent les composants parent à scanner si aucune structure n'est trouvée.
	 * @return la structure trouvée (ou <code>null</code> si non trouvée)
	 */
	private EOStructure getStructure(List<IComposant> listeComposant, List<IComposant> listeComposantParent) {
		EOStructure structure = null;
		
		for (IComposant composant : listeComposant) {
			structure = (EOStructure) composant.getStructure();
			
			if (structure != null) {
				break;
			}

			for (IComposant composantParent : composant.parents()) {
				listeComposantParent.add(composantParent);
			}
		}
		
		return structure;
	}
	
	
	/**
	 * Renvoie la structure du parent direct de l'AP
	 * @param ap l'atome pédagogique
	 * @return composante du parent
	 */
	public EOStructure getStructureAP(IComposant ap) {
		if (getParentAP(ap) != null) {
			List<IStructure> listeStructuresComposant = (List<IStructure>) getParentAP(ap).structures();

			if (listeStructuresComposant != null && !listeStructuresComposant.isEmpty()) {
				return (EOStructure) listeStructuresComposant.get(0);
			}
		}
		return null;
	}
	
	/**
	 * Renvoie le parent de l'AP
	 * @param ap : AP
	 * @return parent de l'AP
	 */
	public IComposant getParentAP(IComposant ap) {
		if ((ap != null) && (ap.parents() != null) && (!ap.parents().isEmpty())) {
			return ap.parents().get(0);
		}
		return null;
	}
	
	/**
	 * 
	 * @param ap ap
	 * @param listeStructures liste 
	 * @return true si l'AP est bien contenue dans la listeStructures
	 */
	public boolean estAPContenueDansListeStructures(IComposant ap, NSArray<EOStructure> listeStructures) {
		
		if (listeStructures == null || listeStructures.isEmpty()) {
			return false;
		}
		
		EOStructure structureAP = getStructureAP(ap);
		return listeStructures.contains(structureAP);
		
	}
	
	
	/**
	 * Renvoie la composante du parent direct de l'AP
	 * @param serviceDetail en cours
	 * @return composante du parent
	 */
	public String getComposanteParentAP(EOServiceDetail serviceDetail) {
		if (serviceDetail !=  null) {
			return getComposanteParentAP(serviceDetail.composantAP());
		}
		return "";
	}
	
	
	/**
	 * Renvoie la composante du parent direct de l'AP
	 * @return composante du parent
	 */
	public String getComposanteParentAP(IComposant ap) {
		String lieuParentAP = "INCONNU";
		//TODO : voir comment faire si multiple parent
		if (ap != null && getParentAP(ap) != null) {
			List<IStructure> listeStructuresComposant = (List<IStructure>) getParentAP(ap).structures();

			if (!listeStructuresComposant.isEmpty()) {
				lieuParentAP = listeStructuresComposant.get(0).lcStructure();
			} else if (Constante.APP_PECHE.equals(getParentAP(ap).tagApplication())) {
				lieuParentAP = controleurUE.getEtablissementExterne(getParentAP(ap));
			}
		}

		return lieuParentAP;
	}
	
	
	/**
	 * Renvoie le composant parent direct de l'AP
	 * @param serviceDetail service
	 * @return composant
	 */
	public IComposant getParentAP(HasComposantAP serviceDetail) {
		return getParentAP(serviceDetail.composantAP());
	}
	
	
	
	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct
	 *
	 * @param composantAP
	 *            : AP à afficher
	 * @return affichage de l'AP
	 */
	public String affichageAPEtParent(IComposant composantAP) {
		return ComposantAffichageHelper.getInstance().affichageAPEtParent(composantAP);
	}
	
		
	/**
	 * Renvoie le libellé de l'AP
	 * @param ap : AP
	 * @return libellé de l'AP
	 */
	public String getLibelleAP(IComposant ap) {
		if (ap != null) { 
			return ap.libelle();
		}
		return "";
	}
	
	/**
	 * 
	 * @param structure en cours
	 * @return true / false
	 */
	public boolean estDepartement(EOStructure structure) {
		boolean estDepartement = false;
		for (EORepartTypeGroupe repartTypeGroupe : structure.toRepartTypeGroupes())	{
			if (EOTypeGroupe.TGRP_CODE_DE.equals(repartTypeGroupe.tgrpCode())) {
				estDepartement = true;
			}
		}
		return estDepartement;
	}
	
	/**
	 * @param composantAP : AP
	 * @return code et libellé du parent de l'AP
	 */
	public String getParentAPCodeEtLibelle(IComposant composantAP) {
		return ComposantAffichageHelper.getInstance().affichageEnseignementParent(composantAP);
	}
	
	/**
	 * @param composant
	 * @return true si le composant est taggué "PECHE".
	 */
	public boolean isComposantPeche(IComposant composant){
		return Constante.APP_PECHE.equals(composant.tagApplication());
	}
	
	/**
	 * @param ap
	 * @return true si le parent est une UE flottante
	 */
	public boolean isParentUeFlottante(IAP ap){
		if(ap == null){
			return false;
		}
		IComposant parentAP = getParentAP(ap);
		if(parentAP == null){
			return false;
		}
		return isComposantPeche(parentAP);
	}
	
	/**
	 * @param composant le composant à tester
	 * @param types
	 * @return true si le composant est du même type que celui ou un de ceux pasés en paramètre.
	 */
	public boolean isComposantTypeOf(IComposant composant, String... types){
		ITypeComposant typeComposant = composant.typeComposant();
		if(typeComposant == null){
			return false;
		}
		for(String type : types){
			if(type.equals(typeComposant.nom())){
				return true;
			}
		}
		return false;
	}
	
}
