package org.cocktail.peche.components.controlers;


/**
 * Singleton pour la classe {@link ComposantCtrl}.
 * @author anthony
 *
 */
public final class ComposantCtrlSingleton {

	private static ComposantCtrl INSTANCE;
	
	private ComposantCtrlSingleton() {
	}
			
	/**
	 * 
	 * @return l'instance du singleton
	 */
	public static ComposantCtrl getInstance(){
		if(INSTANCE == null){
			INSTANCE = new ComposantCtrl();
		} 
		return INSTANCE;
	}
	
}
