package org.cocktail.peche.components.controlers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.peche.Application;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithXmlDataSource;

import com.webobjects.appserver.WOActionResults;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;


/**
 * Controleur permettant de génerer les bordereaux.
 * @author etienne
 *
 */
public class GenerateurPDFCtrl {

	//	private String reportFilename;
	private CktlAbstractReporter reporter;
	private IJrxmlReportListener listener = new ReporterAjaxProgress(100);

	private String reportFilename = "bordereau.pdf";
	private String jasperFilename;
	private String xml;


	/**
	 * Constructeur.
	 * @param xml : le XML à générer
	 */
	public GenerateurPDFCtrl(String xml) {
		//		listener = new 
		this.xml = xml;
	}

	/**
	 * Genere la fiche de service
	 * @param jaspernameFile : nom dui jasper à prendre en compte
	 * @param isStatutaire : est-ce que la génération concerne un statutaire?
	 * @return null
	 */
	public WOActionResults genererFicheService(String jaspernameFile, boolean isStatutaire) {
		jasperFilename = jaspernameFile;
		reporter = reporterFicheService(xml, listener, isStatutaire);		
		return null;
	}
	
	private CktlAbstractReporter reporterFicheService(String xml, IJrxmlReportListener listener, boolean isStatutaire) {
		//récupération du chemin du fichier jasper de l'application
		String pathForJasper = pathForJasper(getJasperFilename());
		
		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			
			if (isStatutaire) {
				File f = new File(Application.pathJasperLocal() + "/ficheServiceStatutaire/fiche_service_stat.jasper");
				if (f.exists()) {
					pathForJasper = f.getAbsolutePath();
				}
			} else {
				File f = new File(Application.pathJasperLocal() + "/ficheServiceVacataire/fiche_service_vacat.jasper");
				if (f.exists()) {
					pathForJasper = f.getAbsolutePath();
				}
			}
		}

		String recordPath;
		if (isStatutaire) {
			recordPath = "/fiche_service";
		} else {
			recordPath = "/fiche_service/repartitionServices/repartition";
		}
		
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression de la fiche", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}

	/**
	 * Genere la fiche de voeuxe
	 * @param jaspernameFile : nom dui jasper à prendre en compte
	 * @return null
	 */
	public WOActionResults genererFicheVoeux(String jaspernameFile) {
		jasperFilename = jaspernameFile;
		reporter = reporterFicheVoeux(xml, listener);		
		return null;
	}
	
	private CktlAbstractReporter reporterFicheVoeux(String xml, IJrxmlReportListener listener) {
		//récupération du chemin du fichier jasper de l'application
		String pathForJasper = pathForJasper(getJasperFilename());

		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			File f = new File(Application.pathJasperLocal() + "/ficheVoeux/fiche_voeux.jasper");
			if (f.exists()) {
				pathForJasper = f.getAbsolutePath();
			}
		}

		String recordPath = "/fiche_voeux";

		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression de la fiche", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}

	/**
	 * Genere le bordereau de paiement
	 * @param jaspernameFile : nom dui jasper à prendre en compte
	 * @param isStatutaire : est-ce que la génération concerne un statutaire?
	 * @return null
	 */
	public WOActionResults genererBordereauPaiment(String jaspernameFile, boolean isStatutaire) {
		jasperFilename = jaspernameFile;
		reporter = reporterBordereau(xml, listener, isStatutaire);		
		return null;
	}

	private CktlAbstractReporter reporterBordereau(String xml, IJrxmlReportListener listener, boolean isStatutaire) {
		//récupération du chemin du fichier jasper de l'application
		String pathForJasper = pathForJasper(getJasperFilename());
		
		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			
			if (isStatutaire) {
				File f = new File(Application.pathJasperLocal() + "bordereauStatutaire/bordereau_stat.jasper");
				if (f.exists()) {
					pathForJasper = f.getAbsolutePath();
				}
			} else {
				File f = new File(Application.pathJasperLocal() + "bordereauVacataire/bordereau_vacat.jasper");
				if (f.exists()) {
					pathForJasper = f.getAbsolutePath();
				}
			}
		}

		String recordPath = "/bordereau_paiement/structure/type_contrat/enseignant";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression du bordereau", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}

	/**
	 * Génère le justificatif de paiement d'un vacataire
	 * @return null
	 */
	public WOActionResults genererJustificatifPaimentVacataires() {
		reporter = reporterJustificatifVacataires(xml, listener);		
		return null;
	}

	private CktlAbstractReporter reporterJustificatifVacataires(String xml, IJrxmlReportListener listener) {
		String pathForJasper = pathForJasper("Reports/justificatifVacataire/justificatif_vacat.jasper");
		
		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			File f = new File(Application.pathJasperLocal() + "justificatifVacataire/justificatif_vacat.jasper");
			if (f.exists()) {
				pathForJasper = f.getAbsolutePath();
			}
		}

		String recordPath = "/justificatif_paiement/enseignant/structure/enseignement";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression du justificatif", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}

	/**
	 * Génère le justificatif de paiement d'un statutaire
	 * @return null
	 */
	public WOActionResults genererJustificatifPaimentStatutaires() {
		reporter = reporterJustificatifStatutaires(xml, listener);		
		return null;
	}

	private CktlAbstractReporter reporterJustificatifStatutaires(String xml, IJrxmlReportListener listener) {
		//récupération du chemin du fichier jasper de l'application
		String pathForJasper = pathForJasper("Reports/justificatifStatutaire/justificatif_stat.jasper");
		
		// On teste s'il existe un dossier local pour les jasper et si le fichier jasper existe
		if (Application.pathJasperLocal() != null) {
			File f = new File(Application.pathJasperLocal() + "justificatifStatutaire/justificatif_stat.jasper");
			if (f.exists()) {
				pathForJasper = f.getAbsolutePath();
			}
		}
		
		String recordPath = "/justificatif_paiement/enseignant/offre/structure/enseignement";
		JrxmlReporterWithXmlDataSource jr = null;
		try {
			InputStream xmlFileStream = new ByteArrayInputStream(xml.getBytes());
			jr = new JrxmlReporterWithXmlDataSource();
			Map<String, Object> parameters = new HashMap<String, Object>();
			jr.printWithThread("Impression du justificatif", xmlFileStream, recordPath, pathForJasper,
					parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, listener);						

		} catch (Throwable e) {
			e.printStackTrace();
		}
		return jr;
	}

	protected String pathForJasper(String reportPath) {
		ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
		URL url = rsm.pathURLForResourceNamed(reportPath, "app", null);
		return url.getFile();
	}

	public CktlAbstractReporter getReporter() {
		return reporter;
	}

	public IJrxmlReportListener getListener() {
		return listener;
	}

	public String getReportFilename() {
		return reportFilename;
	}

	public void setReportFilename(String reportFilename) {
		this.reportFilename = reportFilename;
	}

	public String getJasperFilename() {
		return jasperFilename;
	}

	public void setJasperFilename(String jasperFilename) {
		this.jasperFilename = jasperFilename;
	}

	public static class ReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

		public ReporterAjaxProgress(int maximum) {
			super(maximum);
		}

	}

}
