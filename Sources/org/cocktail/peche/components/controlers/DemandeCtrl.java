package org.cocktail.peche.components.controlers;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Contrôleur pour la demande
 */
public class DemandeCtrl extends BasicPecheCtrl {
	
	/**
	 * Constructeur
	 * @param editingContext edc
	 */
	public DemandeCtrl(EOEditingContext editingContext) {
		super(editingContext);
	}

	/**
	 * @param demande : la demande
	 * @return le libellé de l'étape de la demande
	 */
	public String etatEtape(EODemande demande) {
		if (demande == null) {
			return "";
		}
		return EtapePeche.getEtape(demande.toEtape()).getEtatDemande();
	}
	
	/**
	 * @param demande : la demande
	 * @return Est-ce que la fiche est validée?
	 */
	public boolean isDemandeValidee(EODemande demande) {
		boolean demandeValidee = false;
		if (demande != null) {
			demandeValidee = EtapePeche.VALIDEE.equals(EtapePeche.getEtape(demande.toEtape()));
		}
		return demandeValidee;
	}
	
	/**
	 * La date d'initialisation du dossier est la date d'entrée dans le circuit.
	 * @param demande : la demande
	 * @return La date d'entrée de la demande sur son premier circuit
	 */
	public NSTimestamp getDateInitialisation(EODemande demande) {
		if (demande == null) {
			return null;
		}

		NSArray<EOHistoriqueDemande> listeHistoriqueDemande = demande.historiqueChronologique(EOHistoriqueDemande.TO_ETAPE_DEPART.isNull());
		NSTimestamp dateInitialisation = null;

		if (listeHistoriqueDemande.size() > 0) {
			dateInitialisation = listeHistoriqueDemande.get(0).dateModification();
		}

		return dateInitialisation;
	}
	
	/**
	 * On récupère les visa des chemins "VALIDER".
	 * @param demande : la demande
	 * @param circuitValidation : le circuit de validation
	 * @return liste de l'historique des visas de la demande
	 */
	public Map<String, EOHistoriqueDemande> getMapVisas(EODemande demande, EOCircuitValidation circuitValidation) {
		Map<String, EOHistoriqueDemande> mapVisas = new HashMap<String, EOHistoriqueDemande>();
		mapVisas.clear();

		if ((demande != null) && (circuitValidation != null)) {
			NSArray<EOHistoriqueDemande> listeHistoriqueDemande = demande.historiqueChronologique(qualifierHistorique(circuitValidation));

			for (EOHistoriqueDemande historiqueDemande : listeHistoriqueDemande) {
				EOEtape etapeDepart = historiqueDemande.toEtapeDepart();

				if (etapeDepart != null) {
					EOChemin cheminValider = historiqueDemande.toEtapeDepart().chemin(TypeCheminPeche.VALIDER.getCodeTypeChemin());
					
					if (cheminValider != null
							&& cheminValider.toEtapeArrivee() == historiqueDemande.toEtapeArrivee()) {

						mapVisas.put(historiqueDemande.toEtapeDepart().getCodeEtape(), historiqueDemande);
					}

				}
			}
		}
		
		return mapVisas;
	}
	
	/**
	 * On récupère les visa des chemins "VALIDER" en prenant en compte l'étape d'arrivée du dernier
	 * historique 
	 * @param demande : la demande
	 * @return liste de l'historique des visas de la demande
	 */
	public Map<String, EOHistoriqueDemande> getMapVisasHistorique(EODemande demande) {
		Map<String, EOHistoriqueDemande> mapVisas = new HashMap<String, EOHistoriqueDemande>();
		mapVisas.clear();

		if (demande != null) {
			NSArray<EOHistoriqueDemande> listeHistorique = demande.toHistoriqueDemandes(null, 
					EOHistoriqueDemande.DATE_MODIFICATION.ascs(), true);
			
			for (EOHistoriqueDemande historiqueDemande : listeHistorique) {
				if (historiqueDemande.toEtapeDepart() != null) {
				
				EOChemin cheminValider = historiqueDemande.toEtapeDepart().chemin(TypeCheminPeche.VALIDER.getCodeTypeChemin());
				
					if (cheminValider != null
							&& cheminValider.toEtapeArrivee() == historiqueDemande.toEtapeArrivee()) {
						mapVisas.put(historiqueDemande.toEtapeArrivee().getCodeEtape(), historiqueDemande);
					}
				}
			}
			
			//On prend en compte le dernier historique
			mapVisas.put(listeHistorique.lastObject().toEtapeArrivee().getCodeEtape(), listeHistorique.lastObject());
		}
		return mapVisas;
	}
	
	/**
	 * @param circuitValidation : le circuit de validation
	 * @return qualifier de l'historique demande
	 */
	public EOQualifier qualifierHistorique(EOCircuitValidation circuitValidation) {
		return EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION)
				.dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(circuitValidation.codeCircuitValidation());
	}
	
}
