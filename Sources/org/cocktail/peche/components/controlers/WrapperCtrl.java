package org.cocktail.peche.components.controlers;

import org.cocktail.peche.components.Accueil;
import org.cocktail.peche.components.commun.Wrapper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.eocontrol.EOEditingContext;

public class WrapperCtrl {

	private Wrapper wrapper;
	private EOEditingContext edc;

	public WrapperCtrl(Wrapper wrapper) {
        this.wrapper = wrapper;
		edc = wrapper.session().defaultEditingContext();
	}
	
	public WOActionResults accueil() {
		this.wrapper.getPecheSession().resetDefaultEditingContext();
		return this.wrapper.pageWithName(Accueil.class.getName());
	}
	
	public WOActionResults quitter() {
    	return this.wrapper.getPecheSession().onQuitter();
	}

	public EOEditingContext getEdc() {
		return edc;
	}
}
