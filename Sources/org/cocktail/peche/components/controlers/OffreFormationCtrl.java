package org.cocktail.peche.components.controlers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepositoryEOF;
import org.cocktail.fwkcktlscolpedagui.serveur.components.ComposantNode;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.enseignements.CriteresRechercheUeEc;
import org.cocktail.peche.components.enseignements.Noeud;
import org.cocktail.peche.components.enseignements.NoeudTable;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.outils.ListHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Contrôleur de l'offre de formation.
 */
public class OffreFormationCtrl {
	
	public static final String LIBELLE_MUTUALISE = " (mutualisé)";

	/** logger. */
	private static Logger log = Logger.getLogger(OffreFormationCtrl.class);
	
	private UECtrl ueCtrl;
	private EOEditingContext ec;
	private int annee;

	private ListHelper listHelper;
	
	/**
	 * Constructeur sans paramètres utilisé principalement pour les tests.
	 */
	public OffreFormationCtrl() {
	}
	
	/**
	 * Constructeur.
	 * @param ec Un editingContext
	 */
	public OffreFormationCtrl(EOEditingContext ec, int annee) {
		this.ec = ec;
		this.annee = annee;
		ueCtrl = new UECtrl(ec, annee);
	}


	/**
	 * @param affichageConcis Est-ce qu'on est en affichage concis ?
	 * @return La liste des types de composants à afficher
	 */
	public List<String> getTypesComposantsAffiches(boolean affichageConcis) {
		List<String> listeTypesComposants = new ArrayList<String>();

		listeTypesComposants.add(EOTypeComposant.TYPEAP_NOM);
		listeTypesComposants.add(EOTypeComposant.TYPEEC_NOM);
		listeTypesComposants.add(EOTypeComposant.TYPEUE_NOM);
		listeTypesComposants.add(EOTypeComposant.TYPEPERIODE_NOM);
		listeTypesComposants.add(EOTypeComposant.TYPEDIPLOME_NOM);

		if (!affichageConcis) {
			listeTypesComposants.add(EOTypeComposant.TYPEREGROUPEMENT_NOM);
			listeTypesComposants.add(EOTypeComposant.TYPEPARCOURS_NOM);
			listeTypesComposants.add(EOTypeComposant.TYPEVERSIONDIPLOME_NOM);
		}

		return listeTypesComposants;
	}

	/**
	 * @param ec Un editingContext
	 * @return La liste des enseignements diplômants
	 */
	public NSArray<EODiplome> rechercherEnseignementsDiplomants(EOEditingContext ec, int annee) {
		ComposantRepositoryEOF composantRepository = new ComposantRepositoryEOF();
		List<IComposant> versionDiplomes = composantRepository.getListeComposantsMaquettage(EOTypeComposant.typeVersionDiplome(ec), annee);
		
		NSMutableArray<EODiplome> result = new NSMutableArray<EODiplome>();

		for (IComposant versionDiplome : versionDiplomes) {
			EODiplome diplome = ((EOVersionDiplome) versionDiplome).getDiplome();
			if (diplome != null 
					&& diplome.statutFormation() != null 
					&& diplome.statutFormation().code().equals(EOStatutFormation.STATUT_EP_CODE)) {
				result.add(diplome);
			}
		}
		return result;
	}

	/**
	 * @param ec Un editingContext
	 * @param codeUeEc Le code UE ou EC à rechercher
	 * @param libelleUeEc Le libelle UE ou EC à rechercher
	 * @return La liste des enseignements diplômants contenant ce code UE ou EC
	 */
	public NSArray<EODiplome> rechercherEnseignementsDiplomants(EOEditingContext ec, String codeUeEc, String libelleUeEc, int annee) {

		EOQualifier qualifier;
		EOQualifier qualifierCode = null;
		EOQualifier qualifierLibelle = null;

		if (codeUeEc != null) {
			qualifierCode = EOComposant.CODE.contains(codeUeEc);
		}

		if (libelleUeEc != null) {
			qualifierLibelle = EOComposant.LIBELLE.contains(libelleUeEc).or(EOComposant.LIBELLE_COURT.contains(libelleUeEc));
		}

		if (qualifierCode != null && qualifierLibelle != null) {
			qualifier = ERXQ.and(qualifierCode, qualifierLibelle);
		} else if (qualifierCode != null) {
			qualifier = qualifierCode;
		} else {
			qualifier = qualifierLibelle;
		}

		NSArray<EOUE> listeUe = EOUE.fetchSco_UEs(ec, ERXQ.and(qualifier, EOComposant.SYS_ARCHIVE.eq((Boolean) null)), null);
		NSArray<EOEC> listeEc = EOEC.fetchSco_ECs(ec, ERXQ.and(qualifier, EOComposant.SYS_ARCHIVE.eq((Boolean) null)), null);

		NSArray<EOComposant> listeUeEc = new NSMutableArray<EOComposant>(listeUe.size() + listeEc.size());
		listeUeEc.addAll(listeUe);
		listeUeEc.addAll(listeEc);

		List<IComposant> listTmp = new ArrayList(listeUeEc);

		// On recherche maintenant tous les diplômes contenants ces composants
		NSMutableArray<EODiplome> listeDiplomes = new NSMutableArray<EODiplome>();
		rechercherDiplomesParents(listTmp, listeDiplomes);

		NSMutableArray<EODiplome> listeDiplomeFinal = new NSMutableArray<EODiplome>();	
		listeDiplomes = (NSMutableArray<EODiplome>) ERXArrayUtilities.arrayWithoutDuplicates(listeDiplomes);
		NSArray<EODiplome> diplomePourAnnee = rechercherEnseignementsDiplomants(ec, annee);
		for (EODiplome diplome : listeDiplomes) {
			if (diplomePourAnnee.contains(diplome) 
					&& diplome.statutFormation().code().equals(EOStatutFormation.STATUT_EP_CODE))
			{
				listeDiplomeFinal.add(diplome);
			}
		}

		return listeDiplomeFinal;
	}

	/**
	 * @param ec Un editingContext
	 * @param codeUeEc Le code UE ou EC à rechercher
	 * @param libelleUeEc Le libelle UE ou EC à rechercher
	 * @param structuresRecherche structures de recherche
	 * @param annee l'année universitaire
	 * @return La liste des UE/EC contenant le code et/ou le libelle et 
	 * associés (via les parents) à une liste de structures  
	 */
	public NSArray<Integer> rechercherIdsUeEcDiplomants(EOEditingContext ec, String codeUeEc, String libelleUeEc, List<IStructure> structuresRecherche, int annee) {

		EOQualifier qualifier;
		EOQualifier qualifierCode = null;
		EOQualifier qualifierLibelle = null;

		if (codeUeEc != null) {
			qualifierCode = EOComposant.CODE.contains(codeUeEc);
		}

		if (libelleUeEc != null) {
			qualifierLibelle = EOComposant.LIBELLE.contains(libelleUeEc).or(EOComposant.LIBELLE_COURT.contains(libelleUeEc));
		}

		if (qualifierCode != null && qualifierLibelle != null) {
			qualifier = ERXQ.and(qualifierCode, qualifierLibelle);
		} else if (qualifierCode != null) {
			qualifier = qualifierCode;
		} else {
			qualifier = qualifierLibelle;
		}

		qualifier =  ERXQ.and(qualifier, EOComposant.SYS_ARCHIVE.eq((Boolean) null));
		qualifier =  ERXQ.and(qualifier, EOComposant.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE_KEY).eq(annee));
		qualifier =  ERXQ.and(qualifier, EOComposant.TAG_APPLICATION.isNull());

		NSArray<EOUE> listeUe = EOUE.fetchSco_UEs(ec, qualifier, null);
		NSArray<EOEC> listeEc = EOEC.fetchSco_ECs(ec, qualifier, null);

		NSArray<Integer> listeIdsUeEc = new NSMutableArray<Integer>(listeUe.size() + listeEc.size());
		NSArray<IComposant> listeUeEc = new NSMutableArray<IComposant>(listeUe.size() + listeEc.size());

		listeUeEc.addAll(listeUe);
		listeUeEc.addAll(listeEc);

		if (structuresRecherche != null && !structuresRecherche.isEmpty()) {

			for (IComposant c : listeUeEc) {
				if (c.isVisibleEnProduction(annee)) {
					List<IStructure> listeStructures = new ArrayList<IStructure>();
					List<IComposant> listeComposants = new ArrayList<IComposant>();
					listeComposants.add(c);
					rechercherStructuresParents(listeComposants, listeStructures);
					boolean trouve = false;
					if (listeStructures == null 
							|| (listeStructures != null && listeStructures.isEmpty())) {
						// on a mis une structureRecherche mais ce composant n'est associé à aucune structure
						// on ne le prend pas
						// on le retrouvera uniquement par la recherche "Tous" c'est à dire structureRecherche=null
						trouve = false;
					} else 	{
						for (IStructure s : listeStructures) {
							if (structuresRecherche.contains(s)) {
								trouve = true;
							}
						}	
					}
					if (trouve) {
						listeIdsUeEc.add(c.id());
					}
				}
			}
		} else {
			// recherche toutes structures 
			// on prend tout
			for (IComposant c : listeUeEc) {
				if (c.isVisibleEnProduction(annee)) {
					listeIdsUeEc.add(c.id());
				}
			}
		}
		return listeIdsUeEc;
	}
	
	/**
	 * Rechercher les diplômes parents contenants au moins un des composants passés en paramètres.
	 * @param listeUeEc liste des composants devant être au moins un enfant des diplômes recherchés
	 * @param listeDiplomes liste des diplômes trouvés
	 */
	public void rechercherDiplomesParents(List<IComposant> listeUeEc, NSMutableArray<EODiplome> listeDiplomes) {
		for (IComposant composant : listeUeEc) {
			if (composant instanceof EODiplome) {
				if (((EODiplome) composant).statutFormation().code().equals(EOStatutFormation.STATUT_EP_CODE)) {
					listeDiplomes.add((EODiplome) composant);
				}
			} else {
				List<IComposant> listeComposantsParent = composant.parents();
				rechercherDiplomesParents(listeComposantsParent, listeDiplomes);
			}
		}
	}

	/**
	 * Rechercher les structures parents .
	 * On s'arrête aux premières structures trouvées
	 * @param listeUeEc liste des composants devant être au moins un enfant de composants associés à des structures
	 * @param listeStructures liste des structures trouvés.
	 *  
	 */
	public void rechercherStructuresParents(List<IComposant> listeUeEc, List<IStructure> listeStructures) {
		for (IComposant composant : listeUeEc) {
			if (composant.structures() != null && !composant.structures().isEmpty()) {
				listeStructures.addAll(composant.structures());				
			} else {
				List<IComposant> listeComposantsParent = composant.parents();
				rechercherStructuresParents(listeComposantsParent, listeStructures);
			}
		}
	}

	
	/**
	 * Ajoute les enseignements diplômants à partir d'un nœud.
	 * @param parent Le nœud parent (racine)
	 * @param diplomes La liste des diplômes à ajouter
	 * @param affichageConcis Est-ce qu'il faut ignorer certains composants dans l'affichage
	 * @param listeCodesAp La liste des codes AP ajouté (liste en sortir)
	 * @param codeUeEcASelectionner Code de l'UE/EC dont le nœud doit être sélectionné
	 * @param libelleUeEcASelectionner Libellé de l'UE/EC dont le nœud doit être sélectionné
	 * @param ec : editingContext
	 * @return Le nœud qui doit être sélectionné s'il y en a un (ou <code>null</code> si non)
	 */
	public CktlTreeTableNode<Noeud> ajouterEnseignementsDiplomantsDansArborescence(CktlTreeTableNode<Noeud> parent, NSArray<EODiplome> diplomes,
			boolean affichageConcis, Collection<String> listeCodesAp, String codeUeEcASelectionner, String libelleUeEcASelectionner, EOEditingContext ec) {
		CktlTreeTableNode<Noeud> noeudSelectionne = null;

		for (EODiplome diplome : diplomes) {
			CktlTreeTableNode<Noeud> noeudSelectionneTmp = ajouterEnfants(parent, diplome, affichageConcis, listeCodesAp, codeUeEcASelectionner,
					libelleUeEcASelectionner, ec);

			if (noeudSelectionne == null) {
				noeudSelectionne = noeudSelectionneTmp;
			}
		}

		return noeudSelectionne;
	}



	public CktlTreeTableNode<Noeud> ajouterEnseignementsDiplomantsDansArborescencePourAnnee(CktlTreeTableNode<Noeud> parent, NSArray<EODiplome> diplomes,
			boolean affichageConcis, Collection<String> listeCodesAP, String codeUeEcASelectionner, String libelleUeEcASelectionner, EOEditingContext ec) {
		CktlTreeTableNode<Noeud> noeudSelectionne = null;
		CktlTreeTableNode<Noeud> noeudSelectionneTmp = null;
		Noeud data = null;

		for (EODiplome diplome : diplomes) {

			ComposantNode rootNode = new ComposantNode(diplome.getVersionDiplome(annee), null, true);
			rootNode.setExpanded(true);
			data = new Noeud(parent.getData());
			data.setCode(diplome.code());
			data.setId(diplome.id());
			data.setLibelle(formaterLibelleNoeud(diplome));
			data.setTypeComposant(diplome.typeComposant());
			noeudSelectionneTmp = new NoeudTable(data, parent, true);

			if (noeudSelectionne == null) {
				noeudSelectionne = noeudSelectionneTmp;
			}

			NSArray<CktlTreeTableNode<IComposant>> enfants = rootNode.getChildren();

			int anneeDebut = 2000;
			int anneeFin = 3000;
			if (diplome.habilitation() != null) {
				anneeDebut = diplome.habilitation().anneeDebut();
				anneeFin = diplome.habilitation().anneeFin();
			}
			listerEnfants(enfants, noeudSelectionneTmp, listeCodesAP, anneeDebut, anneeFin);
		}
		return noeudSelectionne;
	}



	private void listerEnfants(List<CktlTreeTableNode<IComposant>> enfants, CktlTreeTableNode<Noeud> parent, Collection<String> listeCodesAP, int anneeDebut, int anneeFin) {
		
	Collections.sort(enfants, new Comparator<CktlTreeTableNode<IComposant>>() {
			public int compare(CktlTreeTableNode<IComposant>  composant1, CktlTreeTableNode<IComposant>  composant2)
			{
				int ordre1 = 0;
				int ordre2 = 0;
				if (composant1.getData() instanceof EOPeriode && composant2.getData() instanceof EOPeriode) {
					EOPeriode periode1 = (EOPeriode) composant1.getData();
					EOPeriode periode2 = (EOPeriode) composant2.getData();
					if (periode1 != null && periode2 != null) {
						Integer o1 = ((EOPeriode) composant1.getData()).ordre();
						Integer o2 = ((EOPeriode) composant2.getData()).ordre();
						if (o1 != null && o2 != null) {
							ordre1 = o1 - 1;
							ordre2 = o2 - 1;
						}
					}
				}
				
				if (composant1.getData() instanceof EOUE && composant2.getData() instanceof EOEC) {
					return -1; //UE prioritaire sur EC
				}
				
				if (composant1.getData() instanceof EOEC && composant2.getData() instanceof EOUE) {
					return 1; //UE prioritaire sur EC
				}
				
				if (composant1.getData() instanceof EOUE && composant2.getData() instanceof EOUE) {
					return ((EOUE) composant1.getData()).code().compareTo(((EOUE) composant2.getData()).code());
				}

				if (composant1.getData() instanceof EOEC && composant2.getData() instanceof EOEC) {
					return ((EOEC) composant1.getData()).code().compareTo(((EOEC) composant2.getData()).code());
				}
				
				return ordre1 - ordre2;
			}
		});

			
		for (CktlTreeTableNode<IComposant> enfant : enfants) {
			CktlTreeTableNode<Noeud> enfantTreeTable = null;

			boolean isAnnee = true;

			if (enfant.getData() instanceof EOPeriode) {
				isAnnee = ((EOPeriode) enfant.getData()).isAnnee();
			}

			if (enfant.getData() instanceof EOPeriode && isAnnee) {				
				int ordre = ((EOPeriode) enfant.getData()).ordre() - 1;

			// ATTENTION : anneeDebut==anneeFin est pour gérer les IMPORTS SCOL 
			// TODO: A REVOIR
			if (((anneeDebut + ordre) <= annee && !(annee > (anneeFin + ordre))) || (anneeDebut == anneeFin)) {
					enfantTreeTable = ajouterNoeud(parent, enfant.getData());

					List<CktlTreeTableNode<IComposant>> petitsEnfants = enfant.getChildren();
					if (petitsEnfants.size() > 0) {
						listerEnfants(petitsEnfants, enfantTreeTable, listeCodesAP, anneeDebut, anneeFin);
					}
				}
			} else 	if (!(enfant.getData() instanceof EOAE)) {
				enfantTreeTable = ajouterNoeud(parent, enfant.getData());
				
				if (enfant.getData() instanceof EOAP) {
					if (!listeCodesAP.contains(((EOAP) enfant.getData()).typeAP().code())) {
						listeCodesAP.add(((EOAP) enfant.getData()).typeAP().code());
					}
				}

				List<CktlTreeTableNode<IComposant>> petitsEnfants = ((ComposantNode) enfant).getChildren();
				if (petitsEnfants != null) {
					listerEnfants(petitsEnfants, enfantTreeTable, listeCodesAP, anneeDebut, anneeFin);
				}

			}

		}
	}

	private NSArray<Integer> construireStructuresId(List<IStructure> listeStructures) {
		NSArray<Integer> structuresId = new NSMutableArray<Integer>();
		for (IStructure structure : listeStructures) {
			structuresId.add(Integer.valueOf(structure.cStructure()));
		}
		return structuresId;
	}
	
	private void ajouterUENonDiplomanteDansEtablissement(NSArray<Integer> listeIdAP, List<IStructure> listeStructures, EOTypeComposant typeComposantAP) {
		NSArray<EOUE> listeUeDansEtablissement = rechercherUeNonDiplomanteDansEtablissement(ec, listeStructures);		
		for (EOUE composant : listeUeDansEtablissement) {	
			for (IComposant ap : composant.childs(typeComposantAP)) {
				if (ueCtrl.getChargeEnseignement((EOAP) ap) != null 
						&& ueCtrl.getChargeEnseignement((EOAP) ap).valeurMinutes() != null
						&& ueCtrl.getChargeEnseignement((EOAP) ap).valeurMinutes() > 0
						&& ueCtrl.getChargeEnseignement((EOAP) ap).nbGroupes() != null 
						&& ueCtrl.getChargeEnseignement((EOAP) ap).nbGroupes() > 0) {
					listeIdAP.add(ap.id());
				}
			} 
		}
	}
	
	
	private void ajouterUENonDiplomanteHorsEtablissement(NSArray<Integer> listeIdAP, EOTypeComposant typeComposantAP) {
		NSArray<EORepartUefEtablissement> listeUeHorsEtablissement = rechercherUeNonDiplomanteHorsEtablissement(ec);
		for (EORepartUefEtablissement uehe : listeUeHorsEtablissement) {	
			for (IComposant ap : uehe.composantUE().childs(typeComposantAP)) {
				if (ueCtrl.getChargeEnseignement((EOAP) ap) != null 
						&& ueCtrl.getChargeEnseignement((EOAP) ap).valeurMinutes() != null
						&& ueCtrl.getChargeEnseignement((EOAP) ap).valeurMinutes() > 0
						&& ueCtrl.getChargeEnseignement((EOAP) ap).nbGroupes() != null 
						&& ueCtrl.getChargeEnseignement((EOAP) ap).nbGroupes() > 0) {
					listeIdAP.add(ap.id());
				}
			} 
		}
	}
	
	
	public NSArray<Integer> getAPsAnnee(List<IStructure> listeStructures, List<IStructure> listeUEF) {
		NSArray<Integer> listeIdAP = new NSMutableArray<Integer>();

		// Nous recherchons tous les AP des structures de l'utilisateur connecté + UE flottantes hors établissement 
		//  + UE flottantes présentes dans les structures de l'utilisateur connecté

		//On recupere la liste des composants
		if (!CollectionUtils.isEmpty(listeStructures)) {
			ComposantRepositoryEOF composantRepositoryEOF = new ComposantRepositoryEOF();
			EOTypeComposant typeComposantAP = EOTypeComposant.typeAP(ec);
			NSArray<Integer> structuresId = construireStructuresId(listeStructures);

			listeIdAP.addAll(composantRepositoryEOF.getListeIdAPsAvecChargesDansStructuresEtSansStructures(ec, annee, structuresId));
			
			//Ajout des UE flottantes dans et hors établissement
			
			List<IStructure> listeUEFEtStructs = new ArrayList<IStructure>();
			listeUEFEtStructs.addAll(listeUEF);
			listeUEFEtStructs.addAll(listeStructures);
			
			ajouterUENonDiplomanteDansEtablissement(listeIdAP, listeUEFEtStructs, typeComposantAP);
			ajouterUENonDiplomanteHorsEtablissement(listeIdAP, typeComposantAP);
		}
		return listeIdAP;
	}

	/**
	 * Filtre la liste de composant via le code et/ou le libelle
	 * @param criteresRecherche
	 * @param liste
	 * @return
	 */
	private List<IComposant> filtrerParCodeOuLibelle(CriteresRechercheUeEc criteresRecherche, List<IComposant> liste) {
		
		NSMutableArray<EOQualifier> arrayQualifier = new NSMutableArray<EOQualifier>();
		
		if (criteresRecherche.getCodeUeEc() != null) {
			arrayQualifier.add(ERXQ.likeInsensitive(EOComposant.CODE_KEY, "*" + criteresRecherche.getCodeUeEc() + "*"));
		}
		if (criteresRecherche.getLibelleUeEc() != null) {
			arrayQualifier.add(ERXQ.likeInsensitive(EOComposant.LIBELLE_KEY, "*" + criteresRecherche.getLibelleUeEc() + "*"));
		}
		
		
		NSArray<IComposant> listeTmp = EOQualifier.filteredArrayWithQualifier(new NSArray<IComposant>(liste), ERXQ.and(arrayQualifier));
		liste.clear();		
		liste.addAll(listeTmp);
		
		return liste;
	}


	private CktlTreeTableNode<Noeud> ajouterNoeud (CktlTreeTableNode<Noeud> parent, IComposant composant) {

		Noeud data = null;
		data = new Noeud(parent.getData());
		data.setCode(composant.code());
		data.setId(composant.id());
		data.setLibelle(formaterLibelleNoeud(composant));
		data.setTypeComposant(composant.typeComposant());

		if (composant instanceof EOAP) {
			EOAP ap = (EOAP) composant;
			float heuresMaquette = ueCtrl.getHeuresMaquette(ap, true);
			float heuresPrevues = ueCtrl.getTotalHeuresPrevues(ap, true);
			float heuresRealisees = ueCtrl.getTotalHeuresRealisees(ap, true);
			parent.getData().add(ap, heuresMaquette, heuresPrevues, heuresRealisees);
			parent.getData().setParenAP(true);
		} else {
			parent = new NoeudTable(data, parent, true);
		}

		return parent;
	}



	/**
	 * @param ec Un editingContext
	 * @return La liste des UE non diplômantes (flottantes) enseignées dans l'établissement
	 */
	public NSArray<EOUE> rechercherUeNonDiplomanteDansEtablissement(EOEditingContext ec) {
		ERXAndQualifier qualifier = EOUE.TAG_APPLICATION.eq(Constante.APP_PECHE).and(EOUE.STRUCTURES.isNotNull());
		ERXKeyValueQualifier qualifierAnnee = EOUE.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE_KEY).eq(getAnnee());

		ERXSortOrderings sortOrderings = EOUE.STRUCTURES.dot(EOStructure.LL_STRUCTURE).asc().then(EOUE.LIBELLE.asc());

		return EOUE.fetchSco_UEs(ec, ERXQ.and(qualifier, qualifierAnnee), sortOrderings);
	}
	
	/**
	 * @param ec Un editingContext
	 * @param listeStructures : liste des structures concernées par la recherche
	 * @return La liste des UE non diplômantes (flottantes) enseignées dans l'établissement
	 */
	public NSArray<EOUE> rechercherUeNonDiplomanteDansEtablissement(EOEditingContext ec, List<IStructure> listeStructures) {
		
		ERXAndQualifier qualifier = null;
		if (!CollectionUtils.isEmpty(listeStructures)) {
			qualifier = EOUE.TAG_APPLICATION.eq(Constante.APP_PECHE)
											.and(EOUE.STRUCTURES.isNotNull()
													.and(EOUE.STRUCTURES.in(getListHelper().bidouilleListeStructurePourWO(listeStructures))));
		} else {
			qualifier = EOUE.TAG_APPLICATION.eq(Constante.APP_PECHE).and(EOUE.STRUCTURES.isNotNull());
		}
		
		ERXKeyValueQualifier qualifierAnnee = EOUE.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE_KEY).eq(getAnnee());
		

		//TODO: LPR : le then code asc n'a pas l'air de marcher ????
		//ERXSortOrderings sortOrderings = EOUE.STRUCTURES.dot(EOStructure.LL_STRUCTURE).asc().then(EOUE.CODE.asc());
		ERXSortOrderings sortOrderings = EOUE.STRUCTURES.dot(EOStructure.LL_STRUCTURE).ascs();
		
		return EOUE.fetchSco_UEs(ec, ERXQ.and(qualifier, qualifierAnnee), sortOrderings);
	}
	/**
	 * Ajoute les UE non diplômantes (flottantes) enseignées dans l'établissement à partir d'un nœud.
	 * @param parent Le nœud parent (racine)
	 * @param listeUe La liste des UE non diplômantes (flottantes) enseignées dans l'établissement à ajouter
	 * @param listeCodesAp La liste des codes AP ajouté (liste en sortir)
	 * @param ec : editingContext
	 */
	public void ajouterUeNonDiplomanteDansEtablissementDansArborescence(CktlTreeTableNode<Noeud> parent, NSArray<EOUE> listeUe, Collection<String> listeCodesAp,
			EOEditingContext ec) {
		Noeud data = new Noeud(parent.getData());
		data.setLibelle("UE flottantes au sein de l'établissement");
		data.setCode("UE flottantes au sein de l'établissement");
		CktlTreeTableNode<Noeud> racineLocale = new NoeudTable(data, parent, true);

		String composanteEnCours = null;
		CktlTreeTableNode<Noeud> racineComposante = null;
		
		if (listeUe.size() > 0) {
			NSMutableArray<EOUE> listeUetriee = new NSMutableArray<EOUE>(listeUe);
			Collections.sort(listeUetriee, new Comparator<EOUE>() {
				public int compare(EOUE composant1, EOUE composant2) {
					if (composant1 instanceof EOUE && composant2 instanceof EOUE) {
						if (!NSArrayCtrl.isEmpty(composant1.structures()) && !NSArrayCtrl.isEmpty(composant2.structures())) {
							if (composant1.structures().get(0).libelle().compareTo(composant2.structures().get(0).libelle()) == 0) {
								return composant1.code().toUpperCase().compareTo(composant2.code().toUpperCase());						
							} else {
								return composant1.structures().get(0).llStructure().compareTo(composant2.structures().get(0).llStructure());
							}
						} else {
							return composant1.code().toUpperCase().compareTo(composant2.code().toUpperCase());
						}											
					}
					return 0;
				} 
			});

			for (EOUE ue : listeUetriee) {				
				if (!ue.structures().get(0).cStructure().equals(composanteEnCours)) {
					// On crée un niveau par composante
					Noeud dataEtablissement = new Noeud(racineLocale.getData());
					dataEtablissement.setLibelle(ue.structures().get(0).llStructure());
					dataEtablissement.setCode("ETAB_INT_" + ue.structures().get(0).llStructure());
					racineComposante = new NoeudTable(dataEtablissement, racineLocale, true);

					composanteEnCours = ue.structures().get(0).cStructure();
				}

				ajouterEnfants(racineComposante, ue, false, listeCodesAp, null, null, ec);
			}
		}
	}

	/**
	 * @param ec Un editingContext
	 * @return La liste des UE non diplômantes (flottantes) enseignées hors de l'établissement
	 */
	public NSArray<EORepartUefEtablissement> rechercherUeNonDiplomanteHorsEtablissement(EOEditingContext ec) {
		ERXSortOrderings sortOrderings = EORepartUefEtablissement.ETABLISSEMENT_EXTERNE.asc().then(EORepartUefEtablissement.COMPOSANT_UE.dot(EOUE.LIBELLE).asc());
		ERXKeyValueQualifier qualifierAnnee = EORepartUefEtablissement.COMPOSANT_UE.dot(EOUE.COMPOSANT_INFO_VERSION).dot(EOComposantInfoVersion.ANNEE_KEY).eq(getAnnee());

		return EORepartUefEtablissement.fetchEORepartUefEtablissements(ec, qualifierAnnee, sortOrderings);	
	}

	/**
	 * Ajoute les UE non diplômantes (flottantes) enseignées hors de l'établissement à partir d'un nœud.
	 * @param parent Le nœud parent (racine)
	 * @param listeUe La liste des UE non diplômantes (flottantes) enseignées hors de l'établissement à ajouter
	 * @param listeCodesAp La liste des codes AP ajouté (liste en sortir)
	 * @param ec : editingContext
	 */
	public void ajouterUeNonDiplomanteHorsEtablissementDansArborescence(CktlTreeTableNode<Noeud> parent, NSArray<EORepartUefEtablissement> listeUe,
			Collection<String> listeCodesAp, EOEditingContext ec) {
		Noeud data = new Noeud(parent.getData());
		data.setLibelle("UE flottantes hors établissement");
		data.setCode("UE flottantes hors établissement");
		CktlTreeTableNode<Noeud> racineLocale = new NoeudTable(data, parent, true);

		String etablissementEnCours = null;
		CktlTreeTableNode<Noeud> racineEtablissement = null;

		if (listeUe.size() > 0) {
			for (EORepartUefEtablissement ue : listeUe) {
				if (!ue.etablissementExterne().equals(etablissementEnCours)) {
					// On crée un niveau par établissement
					Noeud dataEtablissement = new Noeud(racineLocale.getData());
					dataEtablissement.setLibelle(ue.etablissementExterne());
					dataEtablissement.setCode("ETAB_EXT_" + ue.etablissementExterne());
					racineEtablissement = new NoeudTable(dataEtablissement, racineLocale, true);

					etablissementEnCours = ue.etablissementExterne();
				}

				ajouterEnfants(racineEtablissement, ue.composantUE(), false, listeCodesAp, null, null, ec);
			}
		}
	}

	/**
	 * Ajouter les enfants de ce nœud à l'arborescence.
	 * @param parent Le nœud parent (racine)
	 * @param composant Le composant parent
	 * @param affichageConcis Est-ce qu'il faut ignorer certains composants dans l'affichage
	 * @param listeCodesAp La liste des codes AP ajouté (liste en sortir)
	 * @param codeUeEcASelectionner Code de l'UE/EC dont le nœud doit être sélectionné
	 * @param libelleUeEcASelectionner Libellé de l'UE/EC dont le nœud doit être sélectionné
	 * @return Le nœud qui doit être sélectionné s'il y en a un (ou <code>null</code> si non)
	 */
	private CktlTreeTableNode<Noeud> ajouterEnfants(CktlTreeTableNode<Noeud> parent, IComposant composant, boolean affichageConcis,
			Collection<String> listeCodesAP, String codeUeEcASelectionner, String libelleUeEcASelectionner, EOEditingContext ec) {
		CktlTreeTableNode<Noeud> fils;
		CktlTreeTableNode<Noeud> noeudSelectionne = null;
		Noeud data = null;

		if (!getTypesComposantsAffiches(affichageConcis).contains(composant.typeComposant().nom())) {
			fils = parent;
		} else {
			data = new Noeud(parent.getData());
			data.setCode(composant.code());
			data.setId(composant.id());
			data.setLibelle(formaterLibelleNoeud(composant));
			data.setTypeComposant(composant.typeComposant());
			fils = new NoeudTable(data, parent, true);
		}

		List<IComposant> children = getChildren(composant);		

		EOTypeComposant typeComposantAP = EOTypeComposant.typeAP(ec);
		for (IComposant child : children) {
			if (!child.typeComposant().equals(typeComposantAP)) {
				CktlTreeTableNode<Noeud> noeudSelectionneTmp = ajouterEnfants(fils, child, affichageConcis, listeCodesAP, codeUeEcASelectionner,
						libelleUeEcASelectionner, ec);

				// Le premier nœud trouvé est sélectionné
				if (noeudSelectionne == null) {
					noeudSelectionne = noeudSelectionneTmp;
				}
			} else {
				EOAP ap = (EOAP) child;
				EOTypeAP typeAP = ap.typeAP();
				String codeTypeAP = typeAP.code();
				float heuresMaquette = ueCtrl.getHeuresMaquette(ap, true);
				float heuresPrevues = ueCtrl.getTotalHeuresPrevues(ap, true);
				float heuresRealisees = ueCtrl.getTotalHeuresRealisees(ap, true);
				data.add(ap, heuresMaquette, heuresPrevues, heuresRealisees);
				data.setParenAP(true);
				if (!listeCodesAP.contains(codeTypeAP)) {
					listeCodesAP.add(codeTypeAP);
				}
			}
		}

		if (noeudSelectionne == null && isNoeudSelectionnable(composant, codeUeEcASelectionner, libelleUeEcASelectionner)) {
			noeudSelectionne = fils;
		}

		return noeudSelectionne;
	}



	private List<IComposant> getChildren(IComposant composant) {
		return composant.childs();
	}
	/**
	 * Formate le libellé du nœud.
	 * 
	 * @param composant
	 *            le composant
	 * @return le libellé à afficher
	 */
	protected String formaterLibelleNoeud(IComposant composant) {
		if (composant == null) {
			return "";
		}
		if (ComposantCtrlSingleton.getInstance().isComposantTypeOf(composant, EOTypeComposant.TYPEUE_NOM,
				EOTypeComposant.TYPEEC_NOM)) {
			return formaterLibelleNoeudForUeUc(composant);
		} else if (ComposantCtrlSingleton.getInstance().isComposantTypeOf(composant, EOTypeComposant.TYPEDIPLOME_NOM)){
			return formaterLibelleNoeudForDiplomes(composant);
		} else {
			return composant.libelle();
		}
	}

	/**
	 * Formate le libellé du noeud pour les UC et les UE.
	 * 
	 * @param composant
	 * @return
	 */
	private String formaterLibelleNoeudForUeUc(IComposant composant) {
		StringBuilder result = new StringBuilder();
		ComposantCtrl controlleur = ComposantCtrlSingleton.getInstance();
		boolean isUeFlottante = controlleur.isComposantTypeOf(composant, EOTypeComposant.TYPEUE_NOM)
				&& controlleur.isComposantPeche(composant);
		if (isUeFlottante) {
			result.append(composant.libelle());
		} else {
			result.append(composant.code()).append(" - ").append(composant.libelle());
			if (composant.isMutualise()) {
				result.append(LIBELLE_MUTUALISE);
			}
		}
		completerLibelleAvecStructures(composant, result);
		return result.toString();
	}

	/**
	 * Formate le libellé du noeud pour les UC et les UE.
	 * 
	 * @param composant
	 * @return
	 */
	private String formaterLibelleNoeudForDiplomes(IComposant composant) {
		StringBuilder result = new StringBuilder();
		result.append(composant.libelle());
		completerLibelleAvecStructures(composant, result);
		return result.toString();
	}

	/**
	 * Complete le libellé avec les structures dans le cas ou ce n'est pas une
	 * UE flottante.
	 * 
	 * @param composant
	 * @param result
	 */
	private void completerLibelleAvecStructures(IComposant composant, StringBuilder result) {
		// Si ce n'est pas une UE Flottante, on ajoute les structures
		if (!ComposantCtrlSingleton.getInstance().isComposantPeche(composant)) {
			String libelleStructures = formaterLibelleListeStructuresComposant(composant);
			if (!StringCtrl.isEmpty(libelleStructures)) {
				result.append(" (").append(libelleStructures).append(")");
			}
		}
	}

	/**
	 * Formatter les structure sous la format d'une liste de code séparé par une
	 * virgule.
	 * 
	 * @param composant
	 *            le composant dont le libellé des structures est à formatter
	 * @return le libellé des structures formatté
	 */
	protected String formaterLibelleListeStructuresComposant(IComposant composant) {
		String libelle = "";
		List<? extends IStructure> listeStructures = composant.structures();

		for (IStructure structure : listeStructures) {
			if (!StringCtrl.isEmpty(libelle)) {
				libelle += ", ";
			}
			libelle += structure.lcStructure();
		}

		return libelle;
	}

	/**
	 * Est-ce que ce nœud doit être sélectionné ?
	 * @param composant le composant
	 * @param codeUeEcASelectionner les critères de sélection
	 * @param libelleUeEcASelectionner les critères de sélection
	 * @return <code>true</code> si le nœud répond aux critères de sélection
	 */
	private boolean isNoeudSelectionnable(IComposant composant, String codeUeEcASelectionner, String libelleUeEcASelectionner) {
		boolean conditionSurCodeUeEcSatisfaite = false;
		boolean conditionSurLibelleUeEcSatisfaite = false;

		if (EOTypeComposant.TYPEUE_NOM.equals(composant.typeComposant().nom()) || EOTypeComposant.TYPEEC_NOM.equals(composant.typeComposant().nom())) {

			if (chaineContient(composant.code(), codeUeEcASelectionner)) {
				conditionSurCodeUeEcSatisfaite = true;
			}

			if (chaineContient(composant.libelle(), libelleUeEcASelectionner) || chaineContient(composant.libelleCourt(), libelleUeEcASelectionner)) {
				conditionSurLibelleUeEcSatisfaite = true;
			}
		}

		return conditionSurCodeUeEcSatisfaite && conditionSurLibelleUeEcSatisfaite;
	}

	/**
	 * Est-ce que {@code chaine} contient {@code chaineCherchee}. Gère les <code>null</code> en entrée.
	 * @param chaine La chaine analysée. Si <code>null</code> retourne <code>false</code>
	 * @param chaineCherchee La chaine recherchée. Si <code>null</code> retourne <code>true</code>
	 * @return <code>true</code> si chaine contient chaineCherchee
	 */
	private boolean chaineContient(String chaine, String chaineCherchee) {
		if (chaine == null) {
			return false;
		}

		if (chaineCherchee == null) {
			return true;
		}

		return chaine.toUpperCase().contains(chaineCherchee.toUpperCase());
	}

	/**
	 * Filtrer les diplômes et ne retourne que les diplômes habilités.
	 * @param listeDiplomes liste des diplômes à filtrer
	 * @param listeStructuresHabilitees liste des structures pour lesquelles l'utilisateur est habilité
	 * @return liste des diplômes habilités
	 */
	public NSArray<EODiplome> filtrerDiplomesHabilites(NSArray<EODiplome> listeDiplomes, List<IStructure> listeStructuresHabilitees) {
		NSMutableArray<EODiplome> listeDiplomesHabilites = new NSMutableArray<EODiplome>();

		// Pour chaque diplôme, on regarde si on est habilité à au moins une des ces structures
		for (EODiplome diplome : listeDiplomes) {
			// Si le diplôme n'a pas de structure, on est habilité
			boolean diplomeHabilite = diplome.structures().size() == 0;

			for (IStructure structure : diplome.structures()) {
				if (listeStructuresHabilitees.contains(structure)) {
					diplomeHabilite = true;
					break;
				}
			}

			if (diplomeHabilite) {
				listeDiplomesHabilites.add(diplome);
			}
		}
		return listeDiplomesHabilites;
	}


	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	public ListHelper getListHelper() {
		if (listHelper == null) {
			listHelper = new ListHelper();
		}
		
		return listHelper;
	}

}
