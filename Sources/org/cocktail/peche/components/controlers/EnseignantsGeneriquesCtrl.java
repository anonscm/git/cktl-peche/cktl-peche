package org.cocktail.peche.components.controlers;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.entity.EOEnseignantGenerique;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Contrôleur pour les enseignants génériques.
 */
public class EnseignantsGeneriquesCtrl extends BasicPecheCtrl {

	/**
	 * Constructeur.
	 * @param editingContext : Un editingContext
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : type d'année
	 */
	public EnseignantsGeneriquesCtrl(EOEditingContext editingContext, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext, annee, isFormatAnneeExerciceAnneeCivile);
	}

	/**
	 * Renvoie le service de l'enseignant
	 *
	 * @param service : le service
	 * @return le service dû par l'enseignant générique
	 */
	public double getHeuresService(EOService service) {
		return service.toEnseignantGenerique().heuresPrevues();
	}

	/**
	 * @param service : le service
	 * @return le service attribué à un individu.
	 */
	public double getServiceAttribue(EOService service) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc());
			return moteur.getServiceHETDPrevu(getHeuresService(service), service.listeServiceDetails());
		}
		return 0;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le service attribué à un enseignant
	 */
	public double getServiceAttribue(EOEnseignantGenerique enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		return getServiceAttribue(service);
	}

	/**
	 * @param service : le service
	 * @return le reh attribué à un individu.
	 */
	public double getRehAttribue(EOService service) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc());
			return moteur.getServiceHETDPrevu(getHeuresService(service), service.listeServiceDetailsReh());
		}
		return 0;
	}
	
	/**
	 * @param service : le service
	 * @return le nombre d'heures complémentaires prévisionnelles.
	 */
	public double getHcompPrev(EOService service) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc());
			return moteur.getHeuresComplementairesPrevues(getHeuresService(service), service);
		}
		return 0;
	}
	
	/**
	 * @param enseignantGenerique : enseignant générique
	 * @return le département de l'enseignant générique
	 */
	public IStructure getStructureDepartement(EOEnseignantGenerique enseignantGenerique) {
		if ((enseignantGenerique.toStructure() != null) 
				&& (getDepartementEnseignement(enseignantGenerique.toStructure()) != null)) {
			return getDepartementEnseignement(enseignantGenerique.toStructure());
		}
		
		return null;
	}
	
	/**
	 * @param enseignantGenerique : enseignant générique
	 * @return la composante de l'enseignant générique
	 */
	public IStructure getStructureComposante(EOEnseignantGenerique enseignantGenerique) {
		if ((enseignantGenerique.toStructure() != null) 
				&& (getComposanteTypeCS(enseignantGenerique.toStructure()) != null)) {
			return getComposanteTypeCS(enseignantGenerique.toStructure());
		}
		
		return null;
	}
}
