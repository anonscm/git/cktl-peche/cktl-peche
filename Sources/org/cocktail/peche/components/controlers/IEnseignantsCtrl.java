package org.cocktail.peche.components.controlers;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

public interface IEnseignantsCtrl {

	
	public EOAP getAP();
	
	void setSelectedAP(EOAP unAP);
	
	public EODemande getDemande(EOService service);
	
	/**
	 * @param listeServiceDetail liste service detail
	 * @return le total des heures prévues
	 **/
	Double getTotalHeuresPrevues(List<EOServiceDetail> listeServiceDetail);
	
	/**
	 * @param listeServiceDetail liste service detail
	 * @return le total des heures réalisées
	 */
	Double getTotalHeuresRealisees(List<EOServiceDetail> listeServiceDetail);
	
}
