package org.cocktail.peche.components.controlers;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.entity.EOService;

import com.webobjects.eocontrol.EOEditingContext;

public interface IEnseignantsVacatairesCtrl {

	
	
	/**
	 * 
	 * @param edc editing context
	 * @param service un service
	 * @return true/false
	 */
	boolean estServiceAvecCircuitVisaDepartement(EOEditingContext edc, EOService service);
	
	
	/**
	 * 
	 * @param edc editing context
	 * @param service un service
	 * @param listeComposante liste de composantes
	 */
	void suppressionRepartServiceSansRepartition(EOEditingContext edc, EOService service, List<IStructure> listeComposante);
	
	
}
