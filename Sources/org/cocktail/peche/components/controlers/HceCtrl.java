package org.cocktail.peche.components.controlers;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.EODecharge;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOVueAbsences;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Contrôleur pour le calcul des heures complémentaires
 * principalement pour le module asynchrone de mise à jour des heures.
 * @author Laurent Pringot
 *
 */
public class HceCtrl {

	private EOEditingContext editingContext;
	private static final Logger log = Logger.getLogger(HceCtrl.class);

	public HceCtrl(EOEditingContext edc) {
		editingContext = edc;	
	}
	/**
	 * @param nbHeures Un nombre d'heures à arrondir
	 * @return Le nombre d'heures arrondi au centième le plus proche
	 */
	private static final double CENT = 100.0d;

	public static double arrondirAuCentieme(double nbHeures) {	
		return Math.rint(nbHeures * CENT) / CENT;
	}

	public double getServiceDuStatutaire(EOActuelEnseignant enseignant, Integer annee,boolean isFormatAnneeExerciceAnneeCivile) {
		return enseignant.getNbHeuresServiceStatutaire(annee, isFormatAnneeExerciceAnneeCivile) 
				- getTotalNbHeuresReduction(enseignant, annee,isFormatAnneeExerciceAnneeCivile);
	}

	public Double getTotalNbHeuresReduction(EOActuelEnseignant enseignant, Integer annee,boolean isFormatAnneeExerciceAnneeCivile) {
		return getTotalNbHeuresDecharges(enseignant.toIndividu(), annee, isFormatAnneeExerciceAnneeCivile) + getTotalNbHeuresModaliteService(enseignant, annee, isFormatAnneeExerciceAnneeCivile) 
				+ getTotalNbHeuresMinorations(enseignant, annee, isFormatAnneeExerciceAnneeCivile) + getReportAnneeAnterieure(enseignant, (annee-1));
	}

	/**
	 * retourne le nombre d'heures total de décharges prévues pour un intervenant
	 * @param individu l'individu.
	 * @return le total d'heures des décharges de l'individu.
	 */
	public double getTotalNbHeuresDecharges(EOIndividu individu,Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double somme = 0;
		NSArray<EODecharge> decharges = EODecharge.dechargeAnneeUniversitaire(editingContext, individu, annee, isFormatAnneeExerciceAnneeCivile);

		if (decharges != null) {
			for (int i = 0; i < decharges.size(); i++) {
				somme += decharges.get(i).nbHDecharge().doubleValue();
			}
		}
		return somme;
	}

	/**
	 * Retourne le report de l'année antérieure si cette fiche de service a bien été validée
	 * @param enseignant l'enseignant.
	 * @param anneeUniversitairePrecedente annee
	 * @return report
	 */
	public Double getReportAnneeAnterieure(EOActuelEnseignant enseignant, Integer anneeUniversitairePrecedente) {

		EOService servicePrecedent  = enseignant.getService(anneeUniversitairePrecedente);

		BigDecimal reportAnneeAnterieure = BigDecimal.ZERO;

		if (servicePrecedent != null 
				&& !servicePrecedent.toListeRepartService().isEmpty() 
				&& isDemandeValidee(getDemande(servicePrecedent))) {
			reportAnneeAnterieure = servicePrecedent.getHeuresAReporterAttribuees();
		} 


		return reportAnneeAnterieure.doubleValue();
	}

	/**
	 * retourne le nombre d'heures total d'absence (minorations + positions) prévu pour un intervenant
	 * @param enseignant l'enseignant.
	 * @param annee Année universitaire
	 * @return le total d'heures des absences de l'individu.
	 */
	public double getTotalNbHeuresMinorations(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double total = 0;
		NSArray<EOVueAbsences> listeMinorations = getListeAbsences(enseignant, annee, isFormatAnneeExerciceAnneeCivile);

		for (EOVueAbsences uneMinoration : listeMinorations) {
			if ((uneMinoration != null) && (uneMinoration.isMinoration()) 
					&& (uneMinoration.getNbHeuresAbsence(enseignant.getService(annee)) != null)) {
				total += uneMinoration.getNbHeuresAbsence(enseignant.getService(annee)).doubleValue();
			} 
		}

		return total;
	}

	/**
	 * 
	 * @param service fiche de service
	 * @return demande
	 */
	public EODemande getDemande(EOService service) {
		return service.toListeRepartService().get(0).toDemande();
	}

	/**
	 * Renvoie la liste des absences valides d'un enseignant pour l'année universitaire en cours
	 * @param enseignant : enseignant
	 * @param annee : Année universitaire
	 * @return liste des absences
	 */
	public NSArray<EOVueAbsences> getListeAbsences(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		return EOVueAbsences.rechercheAbsenceCouranteAnneeUniversitairePourEnseignant(editingContext, enseignant.toIndividu(), annee, isFormatAnneeExerciceAnneeCivile);
	}


	/**
	 * @param demande : la demande
	 * @return Est-ce que la fiche est validée?
	 */
	public boolean isDemandeValidee(EODemande demande) {
		boolean demandeValidee = false;
		if (demande != null) {
			demandeValidee = EtapePeche.VALIDEE.equals(EtapePeche.getEtape(demande.toEtape()));
		}
		return demandeValidee;
	}

	/**
	 * retourne le total des modalités de service de l'enseignant.
	 * @param enseignant : l'enseignant
	 * @param annee : Année universitaire
	 * @return le total des modalités 
	 */
	public double getTotalNbHeuresModaliteService(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double total = 0;
		NSArray<EOVueAbsences> listeModalitesServices = EOVueAbsences.rechercherModalitesCourantesAnneeUniversitaire(editingContext, 
				enseignant.toIndividu(), annee, isFormatAnneeExerciceAnneeCivile);

		for (EOVueAbsences uneModalite : listeModalitesServices) {
			if ((uneModalite != null) && (uneModalite.getNbHeuresAbsence(enseignant.getService(annee)) != null)) {
				total += uneModalite.getNbHeuresAbsence(enseignant.getService(annee)).doubleValue();
			} else {
				total += getNbHeuresModaliteService(enseignant, uneModalite, annee, isFormatAnneeExerciceAnneeCivile);
			}
		}

		return total;
	}

	/** 
	 * Renvoie le nombre d'heures qui sera déduit pour la modalité de service de l'agent
	 * @param enseignant : enseignant
	 * @param absence : une absence de type modalité uniquement
	 * @param annee : Année universitaire
	 * @return nombre d'heures
	 */
	public double getNbHeuresModaliteService(EOActuelEnseignant enseignant, EOVueAbsences absence, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		log.debug("La modalité de service : " + absence.idAbsence() + " - " + absence.toTypeAbsence().lcTypeAbsence());
		double total = 0;
		double nbHeuresModalite = 0;

		if (absence.isModalite()) {
			if (absence.isTypeCRCT()) {
				//Si CRCT, on ne prend pas en compte la quotité (car elle est toujours de 100%)
				total = enseignant.getNbHeuresServiceStatutaire(annee, isFormatAnneeExerciceAnneeCivile);
			} else {
				if (absence.isTypeDelegation()) {
					total = (enseignant.getNbHeuresServiceStatutaire(annee, isFormatAnneeExerciceAnneeCivile) * (absence.quotite())) 
							/ Constante.CENT;
				} else
					total = (enseignant.getNbHeuresServiceStatutaire(annee, isFormatAnneeExerciceAnneeCivile) * (Constante.CENT - absence.quotite())) 
					/ Constante.CENT;

			}

			log.debug("Le total de la modalité de service : " + total);

			//Date de début et de fin de l'année universitaire
			OutilsValidation.getLogPourGregorianDate(OutilsValidation.getDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), "Début de l'année universitaire");
			OutilsValidation.getLogPourGregorianDate(OutilsValidation.getFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), "Fin de l'année universitaire");

			//Date de fin de la modalité de service
			GregorianCalendar dateFinModalite = DateCtrl.getGregorianDate(absence.dateFin());
			OutilsValidation.getLogPourGregorianDate(dateFinModalite, "Fin de la modalité");
			dateFinModalite.add(GregorianCalendar.HOUR_OF_DAY, 24);			// On s'assure de comptabiliser le dernier jour
			OutilsValidation.getLogPourGregorianDate(dateFinModalite, "Fin de la modalité effective");

			GregorianCalendar dateDebut = OutilsValidation.getDateSuperieur(OutilsValidation.
					getDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), DateCtrl.getGregorianDate(absence.dateDebut()));
			OutilsValidation.getLogPourGregorianDate(dateDebut, "Date de début du calcul");
			GregorianCalendar dateFin = OutilsValidation.getDateInferieur(OutilsValidation.getFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), dateFinModalite);
			OutilsValidation.getLogPourGregorianDate(dateFin, "Date de fin du calcul");

			int nbJoursAnneeUniversitaire = DateCtrl.nbJoursEntreDates(OutilsValidation.getDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

			log.debug("Le nombre de jours de l'année universitaire : " + nbJoursAnneeUniversitaire);

			int nbJoursModalite = DateCtrl.nbJoursEntreDates(dateDebut, dateFin);
			log.debug("Le nombre de jours de la modalité de service au prorata : " + nbJoursModalite);

			nbHeuresModalite = (nbJoursModalite * total) / nbJoursAnneeUniversitaire;
			log.debug("Le nombre d'heures de la modalité de service au prorata : " + nbHeuresModalite);
		}

		return nbHeuresModalite;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le service attribué à un enseignant
	 */
	public double getServiceAttribue(EOActuelEnseignant enseignant,Integer annee, boolean isFormatAnneeExerciceAnneeCivile,EOService service, double serviceDu) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(editingContext, enseignant, annee, isFormatAnneeExerciceAnneeCivile);
			return moteur.getServiceHETDPrevu(serviceDu, service.listeServiceDetails());
		}
		return 0;
	}

	public double getServiceRealise(EOActuelEnseignant enseignant,Integer annee, boolean isFormatAnneeExerciceAnneeCivile,EOService service, double serviceDu) {
		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(editingContext, enseignant, annee, isFormatAnneeExerciceAnneeCivile);
			return moteur.getServiceHETDRealise(serviceDu, service.listeServiceDetails());
		}
		return 0;
	}

	public double getServiceSouhaite(EOActuelEnseignant enseignant,Integer annee, boolean isFormatAnneeExerciceAnneeCivile,EOFicheVoeux voeux, double serviceDu) {
		if (voeux != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(editingContext, enseignant, annee, isFormatAnneeExerciceAnneeCivile);
			return moteur.getServiceHETDSouhaite(serviceDu, voeux);
		}
		return 0;
	}
	
	/**
	 * renvoie le nombre d'heures total des vacations d'un enseignant
	 * @param enseignant : enseignant
	 * @param annee : Année universitaire
	 * @return nombre d'heures
	 */
	public double getTotalNbHeuresVacation(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double nbHeures = 0;
		for (IVacataires vacation : enseignant.getListeVacationsCourantesPourAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile)) {
			nbHeures += vacation.getNbHeures();
		}
		return nbHeures;
	}


}
