package org.cocktail.peche.components.controlers;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlgrh.common.metier.finder.VacatairesAffectationFinder;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacatairesAffectation;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.Application;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.IndicateurValidationAuto;
import org.cocktail.peche.components.enseignants.EnseignantDataBean;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.OutilsValidation;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 * Controleur de base pour les composants de l'application Peche.
 * @author Yannick Mauray
 * @author Chama Laatik
 *
 */
public class BasicPecheCtrl {

	private EOEditingContext editingContext;
	private Vector<BasicPecheObservateur> observateurs;
	private EOAP ap;
	private EOReh reh;
	private EnseignantDataBean enseignantBean;
	private Integer anneeUniversitaire;
	private boolean isFormatAnneeExerciceAnneeCivile;

	@Inject
	private IPecheParametres pecheParametres;
	
	/**
	 * Constructeur.
	 * @param editingContext Un editingContext
	 */
	public BasicPecheCtrl(EOEditingContext editingContext) {
		this.editingContext = editingContext;
		this.observateurs = new Vector<BasicPecheObservateur>();
		
		if (Application.application() != null) {		
			pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);
			this.anneeUniversitaire = getPecheParametres().getAnneeUniversitaireEnCours();
			this.isFormatAnneeExerciceAnneeCivile = getPecheParametres().isFormatAnneeExerciceAnneeCivile();
		} else	{
			this.anneeUniversitaire = Integer.valueOf(2013);
			this.isFormatAnneeExerciceAnneeCivile = false;
		}
			
	}
	
	
	public BasicPecheCtrl(EOEditingContext editingContext, Integer anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		this.editingContext = editingContext;
		this.observateurs = new Vector<BasicPecheObservateur>();
		//pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);
		this.anneeUniversitaire = anneeUniversitaire;
		this.isFormatAnneeExerciceAnneeCivile = isFormatAnneeExerciceAnneeCivile;	
	}
	
	public Integer getAnneeUniversitaire() {
		return anneeUniversitaire;
	}
	
	public Integer getAnneeUniversitairePrecedente() {
		return getAnneeUniversitairePrecedente(anneeUniversitaire);
	}
	
	/**
	 * @param anneeUniversitaireEnCours annee
	 * @return L'année universitaire précédente
	 */
	public Integer getAnneeUniversitairePrecedente(Integer anneeUniversitaireEnCours) {
		return anneeUniversitaireEnCours - 1;
	}
	
	/**
	 * @param anneeUniversitaireEnCours annee
	 * @return L'année universitaire précédente
	 */
	public Integer getAnneeUniversitaireSuivante(Integer anneeUniversitaireEnCours) {
		return anneeUniversitaireEnCours + 1;
	}

	public boolean isFormatAnneeExerciceAnneeCivile() {
		return isFormatAnneeExerciceAnneeCivile;
	}

	EOEditingContext edc() {
		return this.editingContext;
	}

	/**
	 * Ajoute un observateur
	 * @param observateur : BasicPecheObservateur
	 */
	public void addObservateur(BasicPecheObservateur observateur) {		
		this.observateurs.add(observateur);
	}

	protected void notifierObservateurs() {
		for (BasicPecheObservateur observateur : observateurs) {		
			observateur.notifier();
		}
	}
	
	/**
	 * Retourne la composante de l'individu.
	 * <p>
	 * En premier lieu la première composante scolarité.
	 * S'il n'en a pas, la première composante d'affectation.
	 * <p>
	 * <b>IMPORTANT :</b> Pour les performances, <b>NE PAS</b> utiliser cette méthode pour récupérer les informations
	 * de l'utilisateur connecté. Utiliser les méthodes de {@link PecheApplicationUser}.
	 * <p>
	 * <b>IMPORTANT 2 :</b> Pour les performances, <b>NE PAS</b> utiliser cette méthode pour récupérer les informations
	 * de l'enseignant si vous avez un objet {@link EOActuelEnseignant} sous la main. Utiliser les méthodes de cet objet.
	 * 
	 * @param enseignant : l'enseignant
	 * @return La composante de l'individu.
	 */
	public IStructure getComposante(EOActuelEnseignant enseignant) {
		
		IStructure composante = getComposanteScolarite(enseignant);
		
		if (composante == null) {
			composante = getComposanteDirecte(enseignant);
		}

		return composante;
	}

	/**
	 * Retourne la composante directe de l'individu.
	 * <p>
	 * Ce n'est pas forcément une composante scolarité qui est retournée.
	 * 
	 * @param enseignant : l'enseignant
	 * @return La composante d'affectation directe de l'individu.
	 */
	public IStructure getComposanteDirecte(EOActuelEnseignant enseignant) {
		List<EOAffectation> affectations = getListeAffectations(enseignant.toIndividu());

		if (enseignant.isStatutaire(anneeUniversitaire, isFormatAnneeExerciceAnneeCivile)) {
			if (!CollectionUtils.isEmpty(affectations)) {
				return affectations.get(0).toStructure().toComposante();
			}
		} 

		return null;
	}
	
	/**
	 * @param enseignant : l'enseignant
	 * @return La première composante scolarité a laquelle l'individu est affecté
	 */
	public IStructure getComposanteScolarite(EOActuelEnseignant enseignant) {
		List<IStructure> listeComposantesScolarite = getComposantesScolarite(enseignant);
		IStructure composante = null;

		if (!CollectionUtils.isEmpty(listeComposantesScolarite)) {
			composante = listeComposantesScolarite.get(0);
		}

		return composante;
	}

	/**
	 * @param enseignant : l'enseignant
	 * @return La liste des composantes scolarité de l'enseignant.
	 */
	public List<IStructure> getComposantesScolarite(EOActuelEnseignant enseignant) {
		return getStructuresDeType(enseignant, EOTypeGroupe.TGRP_CODE_CS);
	}

	private List<IStructure> getStructuresDeType(EOActuelEnseignant enseignant, String typeGroupe) {
		List<IStructure> listeStructures = new ArrayList<IStructure>();

		if (enseignant.isStatutaire(anneeUniversitaire, isFormatAnneeExerciceAnneeCivile)) {
			IStructure structureAffectationPrincipale = getStructureAffectationPrincipale(enseignant.toIndividu());
			if (structureAffectationPrincipale != null) {
				if (isStructureDeTypeGroupe(structureAffectationPrincipale, typeGroupe)) {
					listeStructures.add(structureAffectationPrincipale);
				} 
			}
		} else 	{
			List<IVacatairesAffectation> listeAffectationVacataire = getListeAffectationsVacataires(enseignant);
			if (listeAffectationVacataire != null && !listeAffectationVacataire.isEmpty()) {
				for (IVacatairesAffectation vacatairesAffectation : listeAffectationVacataire) {
					if (vacatairesAffectation.getToStructure() != null) {
						if (isStructureDeTypeGroupe(vacatairesAffectation.getToStructure(), typeGroupe)) {
							listeStructures.add(vacatairesAffectation.getToStructure());
						}
					}
				}
			}
		}
		return listeStructures;
	}		

	/**
	 * Retourne le département d'enseignement de l'individu.
	 * @param individu Individu
	 * @return Le département d'enseignement de l'individu
	 */
	public IStructure getDepartementEnseignement(EOIndividu individu) {
		return getDepartementEnseignement(getStructureAffectationPrincipale(individu));
	}
	
	public IStructure getDepartementEnseignement(IStructure uneStructure) {
		if (isStructureDeTypeGroupe(uneStructure, EOTypeGroupe.TGRP_CODE_DE)) {
			return uneStructure;
		}
		
		return null;
	}

	/**
	 * @param individu Individu
	 * @return Le liste des affectations de l'individu
	 */
	private NSArray<EOAffectation> getListeAffectations(EOIndividu individu) {		
		return EOAffectation.rechercherAffectationsPourIndividuEtPeriode(this.edc(), individu, 
				OutilsValidation.getNSTDebutAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()),
				OutilsValidation.getNSTFinAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()));
	}
	
	/**
	 * Retourne la structure correspondant à l'affectation principale de l'individu
	 * @param individu individu connecté
	 * @return structure de l'affectation principale
	 */
	public IStructure getStructureAffectationPrincipale(EOIndividu individu) {
		
		EOAffectation affectationPrincipale = null;
		IStructure structure = null;
		NSArray<EOAffectation> affectations = getListeAffectations(individu);
		
		for (EOAffectation eoAffectation : affectations) {
			if (Constante.OUI.equals(eoAffectation.temPrincipale())) {
				affectationPrincipale = eoAffectation;
				break;
			}
		}
		
		if (affectationPrincipale != null) {
			structure = affectationPrincipale.toStructure();
		}
		
		return structure;
		
	}

	private List<IVacatairesAffectation> getListeAffectationsVacataires(EOActuelEnseignant enseignant) {
		List<IVacataires> vacations = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
		if (vacations.size() > 0) {
			return VacatairesAffectationFinder.sharedInstance().findForVacation(this.edc(), vacations.get(0));
		}

		return null;
	}
	
	/**
	 * @param structure Une structure
	 * @param typeGroupe Le type de groupe
	 * @return <code>true</code> si la structure est de type groupe spécifié
	 */
	public boolean isStructureDeTypeGroupe(IStructure structure, String typeGroupe) {
		if (structure != null) {
			return EOStructureForGroupeSpec.isGroupeOfType((EOStructure) structure, typeGroupe);
		}
		return false;
	}

	/**
	 * Refuser la fiche et la fait reculer à l'étape précédente.
	 *
	 * @param repartService concerné
	 * @param personne connectée
	 */

	public void refuserFiche(EORepartService repartService, Integer personne) {
		repartService.toDemande().faireAvancerSurChemin(TypeCheminPeche.REFUSER.getCodeTypeChemin(), personne);
		
		for (EOServiceDetail sd : repartService.toListeServiceDetail()) {
			sd.setEtat(Constante.VERIF);
		}
		
		edc().saveChanges();
	}

	/**
	 * @param ap : AP selectionné à modifier
	 */
	public void setSelectedAP(EOAP ap) {
		this.ap = ap;
		notifierObservateurs();
	}

	public EOAP getAP() {
		return this.ap;
	}


	/**
	 * @param reh : Reh selectionné 
	 */
	public void setSelectedReh(EOReh reh) {
		this.reh = reh;
		notifierObservateurs();
	}

	public EOReh getReh() {
		return this.reh;
	}
	
	public EnseignantDataBean getEnseignantBean() {
		return enseignantBean;
	}

	/**
	 * @param enseignantBean : enseignantBean selectionné à modifier
	 */
	public void setEnseignantBean(EnseignantDataBean enseignantBean) {
		this.enseignantBean = enseignantBean;
		notifierObservateurs();
	}

	/**
	 * Correspondance entre listeEtat() et le sens réel. Ex : A==> Accepté,...
	 * @param etat : etat de la répartition
	 * @return l'état choisi
	 */
	public String etat(String etat) {
		if (Constante.ACCEPTE.equals(etat)) {
			return Constante.ETAT_ACCEPTE;
		} else if (Constante.REFUSE.equals(etat)) {
			return Constante.ETAT_REFUSE;
		} else {
			return Constante.ETAT_ATTENTE;
		}
	}

	/**
	 * @return la liste des états de répartition croisée disponible
	 */
	public NSArray<String> listeEtat() {
		return new NSArray<String>(Constante.ATTENTE, Constante.ACCEPTE, Constante.REFUSE);
	}
	
	/**
	 * 
	 * @param service fiche de service
	 * @return demande
	 */
	public EODemande getDemande(EOService service) {
		return getRepartService(service).toDemande();
	}
	
	/**
	 * 
	 * @param service fiche de service
	 * @return repartService
	 */
	public EORepartService getRepartService(EOService service) {
		return service.toListeRepartService().get(0);
	}
	
	
	/**
	 * @param repartService repartService en cours
	 * @return demande
	 */
	public EODemande getDemande(EORepartService repartService) {
		if (repartService != null) {
			return repartService.toDemande();
		} else {
			return null;
		}
	}
	
	public IPecheParametres getPecheParametres() {
		return pecheParametres;
	}
	
	/**
	 * Cherche une structure de type CS au niveau n sinon n+1 sinon n+2
	 * @param uneStructure : structure de type CS ou DE
	 * @return une structure
	 */
	public IStructure getComposanteTypeCS(IStructure uneStructure) {
		if (uneStructure != null) {
			if (isStructureDeTypeGroupe(uneStructure, EOTypeGroupe.TGRP_CODE_CS)) {
				return uneStructure;
			}
			if (isStructureDeTypeGroupe(uneStructure, EOTypeGroupe.TGRP_CODE_DE) 
					&& isStructureDeTypeGroupe(uneStructure.toStructurePere(), EOTypeGroupe.TGRP_CODE_CS)) {
				return uneStructure.toStructurePere();
			}
			if (isStructureDeTypeGroupe(uneStructure, EOTypeGroupe.TGRP_CODE_DE)
					&& uneStructure.toStructurePere() != null 
					&& isStructureDeTypeGroupe(uneStructure.toStructurePere().toStructurePere(), EOTypeGroupe.TGRP_CODE_CS)) {
		 		return uneStructure.toStructurePere().toStructurePere();
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param repartService repart service en cours
	 */
	public void recalculerIndicateurValidationAuto(EORepartService repartService) {
		NSArray<EOServiceDetail> listeServiceDetail = repartService.toListeServiceDetail();
		
		boolean isAllValide = true;		
		boolean isRehPresent = false;
		boolean isServiceDetailNonRehPresent = false;
		boolean isUefHorsEtabPresent = false;
		
		for (EOServiceDetail eoServiceDetail : listeServiceDetail) {
			if (eoServiceDetail.composantAP() != null) {
				IComposant parentAP = eoServiceDetail.composantAP().parents().get(0);
				if (EORepartUefEtablissement.rechercherEtablissementPourUE(edc(), parentAP) != null) {
					isUefHorsEtabPresent = true;
				} else {
					if (!Constante.VALIDE.equals(eoServiceDetail.etat())) {
						isAllValide = false;
						break;
					}
				}
				isServiceDetailNonRehPresent = true;
			} else if (eoServiceDetail.reh() != null) {
				isRehPresent = true;
			}
		}
		
		if (!isAllValide || !isServiceDetailNonRehPresent) {
			repartService.setIndValidationAutoUeEc(IndicateurValidationAuto.ETAT_IND_INV.getCodeIndicateur());
		} else {
			if (isAllValide && !isRehPresent && !isUefHorsEtabPresent) {
				repartService.setIndValidationAutoUeEc(IndicateurValidationAuto.ETAT_IND_VALIDE.getCodeIndicateur());
			}
			if (isAllValide && isRehPresent && !isUefHorsEtabPresent) {
				repartService.setIndValidationAutoUeEc(IndicateurValidationAuto.ETAT_IND_REH.getCodeIndicateur());
			}
			if (isAllValide && !isRehPresent && isUefHorsEtabPresent) {
				repartService.setIndValidationAutoUeEc(IndicateurValidationAuto.ETAT_IND_UEF.getCodeIndicateur());
			}
			if (isAllValide && isRehPresent && isUefHorsEtabPresent) {
				repartService.setIndValidationAutoUeEc(IndicateurValidationAuto.ETAT_IND_REF.getCodeIndicateur());
			}
		}
		
		
	}
	
	public String getFilenameFicheService(EOActuelEnseignant enseignant, Integer annee, boolean isFicheVoeux, boolean isCircuitPrev) {

		StringBuffer filename = new StringBuffer();

		if (!isFicheVoeux) {
			if (isCircuitPrev) {
				filename.append("FicheService_Prev_");

			} else {
				filename.append("FicheService_Defin_");
			}
		} else {
			filename.append("FicheVoeux_");
		}

		filename.append(StringCtrl.checkString(enseignant.toIndividu().nomAffichage().toUpperCase()));

		String libelleAnnee = annee + "_" + (annee + 1);
		if (isFormatAnneeExerciceAnneeCivile()) {
			libelleAnnee = String.valueOf(annee);
		}
		filename.append("_").append(libelleAnnee);

		filename.append("_").append(DateCtrl.currentDateString());
		return filename.toString();

	}
	
	public String getLibelleFicheTypeCircuit(EOCircuitValidation currentCircuit) {
		if (CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit().equals(currentCircuit.codeCircuitValidation())
				|| CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(currentCircuit.codeCircuitValidation())) {
			return "Fiche prévisionnelle";
		} else {
			return "Fiche définitive";
		}
	}

	public NSArray<EOEtape> getEtapesUtilisees(EOCircuitValidation currentCircuit) {
		NSMutableArray<EOEtape> etapesUtiliseesTemp = currentCircuit.etapesUtiliseesSuivantOrdreChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), TypeCheminPeche.REFUSER.getCodeTypeChemin()).mutableClone();
		for (int iEtape = 0; iEtape < etapesUtiliseesTemp.size(); iEtape++) {
			EOEtape etape = etapesUtiliseesTemp.get(iEtape);

			if (etape.isFinale()) {
				etapesUtiliseesTemp.remove(etape);
			}
		}		
		return etapesUtiliseesTemp;
	}
	
	void writeElement(CktlXMLWriter w, String name, String value) throws IOException {
		w.writeElement(name, StringCtrl.checkString(value));		
	}
	
	void writeElement(CktlXMLWriter w, String name, double value) throws IOException {
			w.writeElement(name, StringCtrl.checkString(new DecimalFormat("0.00#").format(value)));
	}
	
	void writeElement(CktlXMLWriter w, String name, BigDecimal value) throws IOException {
		if (value != null) {
			w.writeElement(name, StringCtrl.checkString(new DecimalFormat("0.00#").format(value)));
		} else {
			w.writeElement(name, "");
		}
	}
	
	/**
	 * @param listeServiceDetail liste service detail
	 * @return le total des heures prévues
	 */
	public Double getTotalHeuresPrevues(List<EOServiceDetail> listeServiceDetail) {
		Double sousTotalHeuresPrevues = Double.valueOf(0);

		if (listeServiceDetail != null && !listeServiceDetail.isEmpty()) {
			for (EOServiceDetail serviceDetail : listeServiceDetail) {
				sousTotalHeuresPrevues += serviceDetail.heuresPrevues();
			}
		} 

		return sousTotalHeuresPrevues;
	}
	
	/**
	 * @param listeServiceDetail liste service detail
	 * @return le total des heures réalisées
	 */
	public Double getTotalHeuresRealisees(List<EOServiceDetail> listeServiceDetail) {
		Double sousTotalHeuresRealisees = Double.valueOf(0);

		if (listeServiceDetail != null && !listeServiceDetail.isEmpty()) {
			for (EOServiceDetail serviceDetail : listeServiceDetail) {
				sousTotalHeuresRealisees += serviceDetail.heuresRealisees();
			}
		} 

		return sousTotalHeuresRealisees;
	}
	
	/**
	 * 
	 * @param structure structure
	 * @return libellé sous la forme "<libellé court structure> (<libellé court structure pére>)"
	 */
	public String getLibelleAffichage(IStructure structure) {
		return structure.lcStructure() + " (" + structure.toStructurePere().lcStructure() + ")";
	}
	
}
