package org.cocktail.peche.components.controlers;

public interface BasicPecheObservateur {

	void notifier();

}
