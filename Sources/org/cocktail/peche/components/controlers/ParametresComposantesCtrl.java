package org.cocktail.peche.components.controlers;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Controleur du composant ParametresComposantes.
 * 
 * @author Yannick Mauray
 * 
 */
public class ParametresComposantesCtrl extends BasicPecheCtrl {

	private EOStructure etablissement;

	/**
	 * Constructeur.
	 * 
	 * @param editingContext : le contexte d'accès à la base de données.
	 */
	public ParametresComposantesCtrl(EOEditingContext editingContext,Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext, annee, isFormatAnneeExerciceAnneeCivile);
		this.etablissement = EOStructure.rechercherEtablissement(edc());
	}

	/**
	 * @return le nom de l'établissement.
	 */
	public String getNomEtablissement() {
		return etablissement.llStructure();
	}

	/**
	 * @return les structures filles de l'établissement, de type "composante".
	 */
	public NSArray<EOStructure> getComposantes() {
		NSArray<EOStructure> composantes = this.etablissement
				.structuresFils(EOStructure.QUAL_STRUCTURES_TYPE_COMPOSANTE);
		return composantes;
	}

}
