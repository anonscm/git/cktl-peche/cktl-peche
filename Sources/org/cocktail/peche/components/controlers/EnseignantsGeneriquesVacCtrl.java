package org.cocktail.peche.components.controlers;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.PecheAutorisationsCache;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class EnseignantsGeneriquesVacCtrl extends EnseignantsGeneriquesCtrl implements IEnseignantsVacatairesCtrl {

	public EnseignantsGeneriquesVacCtrl(EOEditingContext editingContext,
			Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext, annee, isFormatAnneeExerciceAnneeCivile);
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean estServiceAvecCircuitVisaDepartement(EOEditingContext edc, EOService service) {

		EOCircuitValidation circuitValidation = null;		
		boolean circuitDefinitifAvecVisaDepartement = false;
		boolean circuitUtiliseAvecVisaDepartement = false;	
		
		String codeCircuit = EORepartService.getCodeCircuitVacataire(edc, getAnneeUniversitaire());
		circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(edc, Constante.APP_PECHE, codeCircuit);			
		
			
		if (CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(circuitValidation.codeCircuitValidation())) {
			EOCircuitValidation circuitValidationDefinitif = EOCircuitValidation.rechercherCircuitValidationUtilisable(edc, Constante.APP_PECHE, CircuitPeche.PECHE_VACATAIRE.getCodeCircuit());
			circuitDefinitifAvecVisaDepartement = circuitValidationDefinitif.estEtapeUtilisee(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
		} else {
			circuitUtiliseAvecVisaDepartement = circuitValidation.estEtapeUtilisee(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
		}
		
		return  circuitUtiliseAvecVisaDepartement || circuitDefinitifAvecVisaDepartement;
	}

	/**
	 * {@inheritDoc}
	 */
	public void suppressionRepartServiceSansRepartition(EOEditingContext edc, EOService service, List<IStructure> listeComposante) {
		
		NSArray<EORepartService> listeRepartService = service.toListeRepartService();
		NSArray<EORepartService> listeRepartServiceASupprimer = new NSMutableArray<EORepartService>();

		for (EORepartService repartService : listeRepartService) {
			if (repartService.toListeServiceDetail() == null || repartService.toListeServiceDetail().size() == 0) {
				if ((CollectionUtils.isEmpty(listeComposante) || ((repartService.toStructure() == null) || !listeComposante.contains(repartService.toStructure())))) {
					listeRepartServiceASupprimer.add(repartService);
				}
			}
		}
		
		if (listeRepartServiceASupprimer.size() > 0) {
			for (EORepartService repartService : listeRepartServiceASupprimer) {
				// ajout d'un test pour éviter la suppression du dernier repartService
				if (repartService.toStructure() != null)  {
					repartService.delete();
				} else if (listeRepartService.size() > listeRepartServiceASupprimer.size()) {
					repartService.delete();
				}
			}
		}
		edc().saveChanges();
	}
	
	
	
}
