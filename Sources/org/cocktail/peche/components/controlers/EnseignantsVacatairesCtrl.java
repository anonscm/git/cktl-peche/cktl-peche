package org.cocktail.peche.components.controlers;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.CktlPecheBaseComponentCtrl;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;
import org.cocktail.peche.outils.LogoEdition;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXExistsQualifier;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Contrôleur pour le composant EnseignantsVacataires.
 * @author Chama LAATIK
 *
 */
public class EnseignantsVacatairesCtrl extends BasicPecheCtrl implements IEnseignantsVacatairesCtrl, IEnseignantsCtrl {

	/** Logger. */
	private static Logger log = Logger.getLogger(EnseignantsVacatairesCtrl.class);

	/**
	 * Constructeur.
	 * @param editingContext : editingContext
	 * @param anneeUniversitaire annee en cours
	 * @param isFormatAnneeExerciceAnneeCivile type d'année
	 */
	public EnseignantsVacatairesCtrl(EOEditingContext editingContext, Integer anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		super(editingContext, anneeUniversitaire, isFormatAnneeExerciceAnneeCivile);
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Les heures attribuées à l'enseignant
	 */
	public double getServiceAttribue(EOActuelEnseignant enseignant) {
		if (enseignant.getService(getAnneeUniversitaire()) != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), enseignant, getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getServiceHETDPrevu(getTotalNbHeuresVacation(enseignant), enseignant.getService(getAnneeUniversitaire()).listeServiceDetails());
		}
		return 0;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Les heures REH attribuées à l'enseignant
	 */
	public double getRehAttribue(EOActuelEnseignant enseignant) {
		if (enseignant.getService(getAnneeUniversitaire()) != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), enseignant, getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getServiceHETDPrevu(getTotalNbHeuresVacation(enseignant), enseignant.getService(getAnneeUniversitaire()).listeServiceDetailsReh());
		}
		return 0;
	}

	/**
	 * @param enseignant Un enseignant
	 * @return Le service réalisé par l'enseignant
	 */
	public double getServiceRealise(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		return getServiceRealise(service);
	}
	
	/**
	 * @param service Un service
	 * @return Le service réalisé par l'enseignant
	 */
	public double getServiceRealise(EOService service) {
		if (service != null) {		
			return service.getHeuresServiceRealise().doubleValue();			
		}
		return 0;
	}
	
	
	/**
	 * @param enseignant Un enseignant
	 * @return Le reh réalisé par l'enseignant
	 */
	public double getRehRealise(EOActuelEnseignant enseignant) {
		if (enseignant.getService(getAnneeUniversitaire()) != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), enseignant, getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			return moteur.getServiceHETDRealise(getTotalNbHeuresVacation(enseignant), enseignant.getService(getAnneeUniversitaire()).listeServiceDetailsReh());
		}
		return 0;
	}


	/**
	 * @param enseignant : enseignant
	 * @param choixHeuresAPayer : Si oui => calcule le total des heures à payer par AP. Si non => calcule le total des heures payées par AP
	 * @return le total des heures à payer ou payées (en fonction du paramètre booléen) par AP
	 */
	public BigDecimal getServiceTotalHeuresAPayerHETD(EOActuelEnseignant enseignant, boolean choixHeuresAPayer) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		BigDecimal totalHeuresHETD = BigDecimal.ZERO;

		if (service != null) {
			NSArray<EOServiceDetail> listeServiceDetail = EOServiceDetail.fetchEOServiceDetails(edc(), ERXQ.equals(EOServiceDetail.SERVICE_KEY, service), null);

			for (EOServiceDetail serviceDetail : listeServiceDetail) {
				if (choixHeuresAPayer) {
					if (serviceDetail.heuresAPayer() != null) {
						totalHeuresHETD = totalHeuresHETD.add(getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresAPayer()));
					}
				} else {
					if (serviceDetail.heuresPayees() != null) {
						totalHeuresHETD = totalHeuresHETD.add(getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresPayees()));
					} 
				}
			}
		}

		return totalHeuresHETD;
	}

	/**
	 * @param service : service de l'enseignant
	 * @param ap : AP
	 * @param heuresPresentielles : les heures présentielles à calculer
	 * @return les heures présentielles converties en HETD
	 */
	public BigDecimal getServiceHeuresHETDParAP(EOService service, EOAP ap, BigDecimal heuresPresentielles) {
		BigDecimal heuresHETD = null;

		if (ap == null) {
			// Cas des REH
			return heuresPresentielles;
		}

		if (service != null) {
			MoteurDeCalcul moteur = new MoteurDeCalcul(edc(), service.enseignant(), getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			if (heuresPresentielles != null) {
				heuresHETD = moteur.convertirEnHetdEnService(ap, heuresPresentielles);
			}
		}

		return heuresHETD;
	}

	/**
	 * renvoie le nombre d'heures du contrat d'un enseignant
	 * @param enseignant : enseignant
	 * @return nombre d'heures
	 */
	public double getTotalNbHeuresVacation(EOActuelEnseignant enseignant) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		if (service != null) {
			return getTotalNbHeuresVacation(service);
		}
		return 0;		
	}

	
	/**
	 * renvoie le nombre d'heures du contrat d'un enseignant sur un service
	 * @param service : service
	 * @return nombre d'heures
	 */
	public double getTotalNbHeuresVacation(EOService service) {
		double nbHeures = 0;
		if (service!=null && service.heuresService() != null) {
			return service.getHeuresService().doubleValue();			
		} else {		
			for (IVacataires vacation : service.enseignant().getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile())) {
				nbHeures += vacation.getNbHeures();				
			}
		}
		return nbHeures;
	}
	

	/**
	 * renvoie le libellé long du type associé à la vacation
	 * si la vacation a un corps, c'est le llCorps + le code
	 * si la vacation est lié à un type de contrat de travail , c'est le llTypeContratTrav + le code
	 * 
	 * @param enseignant : enseignant
	 * @return libellé vacation 
	 */
	public String getLibelleTypeAssocieVacation(EOActuelEnseignant enseignant) {
		List<IVacataires> liste = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

		if (liste.size() > 0) {
			// on prends par défaut la 1ere vacation
			
			EOTypeContratTravail typeContratTravail = liste.get(0).getToTypeContratTravail();
			if (typeContratTravail != null) {
				return typeContratTravail.llTypeContratTrav() + " (" + typeContratTravail.cTypeContratTrav() + ")";
			}

			EOCorps corps = liste.get(0).getToCorps();		
			if (corps != null) {
				return corps.llCorps() + " (" + corps.code() + ")";
			} 
			
		}
		return "";
	}

	/**
	 * renvoie le libellé court du type associé à la vacation
	 * si la vacation a un corps, c'est le lcCorps + le code
	 * si la vacation est lié à un type de contrat de travail , c'est le lcTypeContratTrav + le code
	 * 
	 * @param enseignant : enseignant
	 * @return libellé vacation 
	 */
	public String getLibelleCourtTypeAssocieVacation(EOActuelEnseignant enseignant) {
		List<IVacataires> liste = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

		if (liste.size() > 0) {
			// on prends par défaut la 1ere vacation
			EOTypeContratTravail typeContratTravail = liste.get(0).getToTypeContratTravail();
			if (typeContratTravail != null) {
				return typeContratTravail.lcTypeContratTrav() + " (" + typeContratTravail.cTypeContratTrav() + ")";
			}
			
			EOCorps corps = liste.get(0).getToCorps();		
			if (corps != null) {
				return corps.lcCorps() + " (" + corps.code() + ")";
			} 
			
		}
		return "";
	}
	/**
	 * renvoie le libellé du type de vacation
	 * 
	 * @param enseignant : enseignant
	 * @return libellé vacation ou éméritat
	 */
	public String getLibelleTypeVacation(EOActuelEnseignant enseignant) {
		List<IVacataires> liste = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
		if (liste.size() > 0) {
			return liste.get(0).getLibelleTypeVacation();
		}
		return "";
	}
	
	/**
	 * Recherche le contrat vacataire de cet enseignant.
	 * @param enseignant un enseignant
	 * @return le contrat vacataire (ou <code>null</code> si non trouvé)
	 */
	public IVacataires getContratVacataires(EOActuelEnseignant enseignant) {
		if (getListeContratsSurAnneeUniversitaire(enseignant).size() > 0) {
			return getListeContratsSurAnneeUniversitaire(enseignant).get(0);
		}
		return null;
	}

	/**
	 * Retourne toutes les vacations valides d'un enseignant sur l'année universitaire passée en paramètre.
	 * @param enseignant l'enseignant
	 * @return la liste des vacations
	 */
	public List<IVacataires> getListeContratsSurAnneeUniversitaire(EOActuelEnseignant enseignant) {
		if (enseignant != null) {
			return enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
		}

		return null;
	}


	/**
	 * valide la fiche et la fait avancer à l'étape suivante
	 * @param repartService de l'enseignant concernée
	 * @param personne connectée
	 */

	public void validerFiche(EORepartService repartService, Integer personne) {
		repartService.toDemande().faireAvancerSurChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), personne);

		for (EOServiceDetail sd : repartService.toListeServiceDetail()) {
			sd.setEtat(null);
		}

	}


	/**
	 * passe la fiche en définitive, soit toutes ses demandes associés
	 * @param edc editingContext
	 * @param service de l'enseignant concernée
	 * @param personne connectée
	 * @param listeComposante liste de structures pour lesquelles on ne supprime pas repartService
	 */
	public void passerEnFicheDefinitive(EOEditingContext edc, EOService service, Integer personne, List<IStructure> listeComposante) {

		NSArray<EORepartService> listeRepartService = service.toListeRepartService();

		for (EORepartService repartService : listeRepartService) {
			repartService.setTemIncident(null);
			if ((repartService.toListeServiceDetail() == null || repartService.toListeServiceDetail().size() == 0) 
					&& !listeComposante.contains(repartService.toStructure())) {				
				repartService.deleteAvecDemande(edc);				
			} else {
				repartService.toDemande().changerCircuit(Constante.APP_PECHE, CircuitPeche.PECHE_VACATAIRE.getCodeCircuit(), personne);
			}
		}
	}


	/**
	 * Renvoie vrai si le circuit de la demande est le circuit prévisionnel
	 * @param demande : la fiche de service
	 * @return vrai/faux
	 */
	public boolean isCircuitPrevisionnel(EODemande demande) {
		return demande.toEtape().toCircuitValidation().codeCircuitValidation().equals(CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit());
	}

	/**
	 * Renvoie vrai si le circuit de la demande est le circuit défintif
	 * @param demande : la fiche de service
	 * @return vrai/faux
	 */
	public boolean isCircuitDefinitif(EODemande demande) {
		return !isCircuitPrevisionnel(demande);
	}

	/**
	 * @param service : service de l'intervenant
	 * @param ap : AP
	 * @param heuresPayees : les heures payées en présentiel
	 * @param heuresRealisees : les heures réalisées en présentiel
	 * @return le delta des heures en HETD entre les heures payées et les heures réalisées par l'intervenant pour l'AP demandé
	 */
	public BigDecimal getDeltaHeuresPayeEtRealiseHETD(EOService service, EOAP ap, BigDecimal heuresPayees, Double heuresRealisees) {
		return OutilsValidation.retournerBigDecimalNonNull(getServiceHeuresHETDParAP(service, ap, heuresPayees)).subtract(
				OutilsValidation.retournerBigDecimalNonNull(getHeuresPresentielEnHETDParAP(service, ap, heuresRealisees)));
	}

	/**
	 * @param heuresPayees : les heures à payer en présentiel
	 * @param heuresRealisees : les heures réalisées en présentiel
	 * @return le delta des heures en présentiel entre les heures payées et les heures réalisées
	 */
	public BigDecimal getDeltaHeuresPayeEtRealisePresentiel(BigDecimal heuresPayees, Double heuresRealisees) {
		return OutilsValidation.retournerBigDecimalNonNull(heuresPayees).subtract(
				OutilsValidation.retournerBigDecimalNonNull(new BigDecimal(heuresRealisees)));
	}

	/**
	 * @param service : service de l'intervenant
	 * @param ap : AP
	 * @param heuresPresentielles : les heures présentielles (prévues ou réalisées) à calculer en HETD
	 * @return les heures réalisées en HETD pour l'AP demandé
	 */
	public BigDecimal getHeuresPresentielEnHETDParAP(EOService service, EOAP ap, Double heuresPresentielles) {
		BigDecimal hPresent = new BigDecimal(heuresPresentielles);
		//Forcer 3 chiffres après la virgule 
		hPresent = hPresent.setScale(3, BigDecimal.ROUND_HALF_UP);

		return getServiceHeuresHETDParAP(service, ap, hPresent);
	}


	/**
	 * @param service service en cours
	 * @return est-ce que toutes les demandes liées à ce service sont prévisionnelles ?
	 */
	public boolean estToutesDemandesPrevisionnelles(EOService service) {

		if (service == null) {
			return true;
		}

		NSArray<EORepartService> listeRepartService =  service.toListeRepartService();
		if (listeRepartService != null) {
			for (EORepartService repartService : listeRepartService) {
				if (!isCircuitPrevisionnel(repartService.toDemande())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * @param service service en cours
	 * @return est-ce que toutes les demandes liées à ce service sont prévisionnelles ?
	 */
	public boolean estToutesDemandesPrevisionnellesEtatFinal(EOService service) {
		NSArray<EORepartService> listeRepartService =  service.toListeRepartService();
		if (listeRepartService != null) {
			for (EORepartService repartService : listeRepartService) {
				if (!repartService.toDemande().toEtape().isFinale()) {
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * @param service service en cours
	 * @return est-ce que certaines des demandes liées à ce service sont encore prévisionnelles ?
	 */
	public boolean estFicheServiceNonDefinitif(EOService service) {
		NSArray<EORepartService> listeRepartService =  service.toListeRepartService();
		if (listeRepartService != null) {
			for (EORepartService repartService : listeRepartService) {
				if (isCircuitPrevisionnel(repartService.toDemande())) {
					return true;
				}
			}
		}
		return false;
	}




	/**
	 * Est-ce que toutes les demandes, hormis celle passée en paramétre, sont dans le circuit définitif ?
	 * @param service service en cours 
	 * @param demande demande sur laquelle on ne teste pas si elle est sur le circuit prévisionnel
	 * @return true or false
	 */
	public boolean estToutesAutresDemandeAvecRepartitionDefinitives(EOService service, EODemande demande) {

		NSArray<EORepartService> listeRepartService = service.toListeRepartService();
		for (EORepartService repartService : listeRepartService) {
			if (repartService.toListeServiceDetail() != null && repartService.toListeServiceDetail().size() > 0) {
				if (isCircuitPrevisionnel(repartService.toDemande()) && !repartService.toDemande().equals(demande)) {
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * Est-ce que toutes les demandes sont dans le circuit définitif ?
	 * @param service service en cours 
	 * @return true or false
	 */
	public boolean estToutesDemandeAvecRepartitionDefinitives(EOService service) {

		NSArray<EORepartService> listeRepartService = service.toListeRepartService();
		for (EORepartService repartService : listeRepartService) {
			if (repartService.toListeServiceDetail() != null && repartService.toListeServiceDetail().size() > 0) {
				if (isCircuitPrevisionnel(repartService.toDemande())) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Supprime les repartService liée à une demande sans répartition
	 * @param edc editing contexte
	 * @param service service
	 * @param listeComposante composantes sur lesquelles on ne supprime pas de repartService (les composantes du répartiteur connecté par ex.)
	 */
	public void suppressionRepartServiceSansRepartition(EOEditingContext edc, EOService service, List<IStructure> listeComposante) {
		NSArray<EORepartService> listeRepartService = service.toListeRepartService();

		NSArray<EORepartService> listeRepartServiceASupprimer = new NSMutableArray<EORepartService>();

		for (EORepartService repartService : listeRepartService) {
			if (repartService.toListeServiceDetail() == null || repartService.toListeServiceDetail().size() == 0) {
				EODemande demande = repartService.toDemande();
				if (demande != null 
						&& ((isCircuitPrevisionnel(demande) && demande.toEtape().isInitiale())
								|| (isCircuitDefinitif(demande) && demande.toEtape().isInitiale()))
								&& (CollectionUtils.isEmpty(listeComposante)
										|| ((repartService.toStructure() == null) || !listeComposante.contains(repartService.toStructure())))) {
					listeRepartServiceASupprimer.add(repartService);
				}
			}
		}
		
		if (listeRepartServiceASupprimer.size() > 0) {
			for (EORepartService repartService : listeRepartServiceASupprimer) {
				// ajout d'un test pour éviter la suppression du dernier repartService
				if (repartService.toStructure() != null)  {
					repartService.deleteAvecDemande(edc);
				} else if (listeRepartService.size() > listeRepartServiceASupprimer.size()) {
					repartService.deleteAvecDemande(edc);
				}
			}
		}
	}

	/**
	 * 
	 * @param listeServiceDetail liste service detail
	 * @param structure une structure
	 * @return le total des heures à payer de la liste
	 */
	public BigDecimal getTotalHeuresAPayer(NSArray<EOServiceDetail> listeServiceDetail, IStructure structure) {

		BigDecimal sousTotalHeuresAPayer = BigDecimal.valueOf(0);

		if (listeServiceDetail != null && !listeServiceDetail.isEmpty()) {

			for (EOServiceDetail serviceDetail : listeServiceDetail) {

				if (serviceDetail.heuresAPayer() != null) {
					sousTotalHeuresAPayer = sousTotalHeuresAPayer.add(serviceDetail.heuresAPayer());
				}
			}
		} 

		return sousTotalHeuresAPayer;
	}


	/**
	 * 
	 * @param listeServiceDetail liste service detail
	 * @param structure structure à filtrer
	 * @return le total des heures payées de la liste
	 */
	public BigDecimal getTotalHeuresPayees(NSArray<EOServiceDetail> listeServiceDetail, IStructure structure) {

		BigDecimal sousTotalHeuresPayees = BigDecimal.valueOf(0);

		if (listeServiceDetail != null && !listeServiceDetail.isEmpty()) {

			for (EOServiceDetail serviceDetail : listeServiceDetail) {

				if (serviceDetail.heuresPayees() != null) {
					sousTotalHeuresPayees = sousTotalHeuresPayees.add(serviceDetail.heuresPayees());
				}

			}

		} 

		return sousTotalHeuresPayees;
	}

	/**
	 * La demande est-elle sur un circuit vacataire avec un visa departement ?
	 * Si circuit vacataire prévisionnel, on regarde si le définitif a un visa département
	 * @param edc editing context
	 * @param service service
	 * @return tre / false
	 */
	public boolean estServiceAvecCircuitVisaDepartement(EOEditingContext edc, EOService service) {

		EOCircuitValidation circuitValidation = null;
		
		boolean circuitDefinitifAvecVisaDepartement = false;
		boolean circuitUtiliseAvecVisaDepartement = false;
		
		List<EORepartService> listeRepartService = service.toListeRepartService();
		
		if (CollectionUtils.isEmpty(listeRepartService)) {
			String codeCircuit = EORepartService.getCodeCircuitVacataire(edc, service.annee());
			circuitValidation = EOCircuitValidation.rechercherCircuitValidationUtilisable(edc, Constante.APP_PECHE, codeCircuit);			
		} else {
			for (EORepartService repartService : service.toListeRepartService()) {
				circuitValidation = repartService.toDemande().toEtape().toCircuitValidation();
				if (circuitValidation != null) {
					break;
				}
			}
		}
		if (circuitValidation != null) {
			if (CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(circuitValidation.codeCircuitValidation())) {
				EOCircuitValidation circuitValidationDefinitif = EOCircuitValidation.rechercherCircuitValidationUtilisable(edc, Constante.APP_PECHE, CircuitPeche.PECHE_VACATAIRE.getCodeCircuit());
				circuitDefinitifAvecVisaDepartement = circuitValidationDefinitif.estEtapeUtilisee(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
			}
			circuitUtiliseAvecVisaDepartement = circuitValidation.estEtapeUtilisee(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
			
		}
		return  circuitUtiliseAvecVisaDepartement || circuitDefinitifAvecVisaDepartement;
	}


	/**
	 * existe-t-il sur le service un repart service avec telle structure ?
	 * @param service service en cours
	 * @param structureArray structures à tester
	 * @return true / false
	 */
	public boolean estServiceAvecRepartServiceDansListeStructure(EOService service, NSArray<EOStructure> structureArray) {

		if (NSArrayCtrl.isEmpty(structureArray) || service == null) {
			return false;
		}

		NSArray<EORepartService> listeRepartService = service.toListeRepartService();

		if (NSArrayCtrl.isEmpty(listeRepartService)) {
			return false;
		}

		boolean retour = false;

		for (EORepartService eoRepartService : listeRepartService) {

			if (structureArray.containsObject(eoRepartService.toStructure())) {
				retour = true;
				break;
			}

		}

		return retour;

	}


	/**
	 * @param typePopulation fiche prév. ou déf. ?
	 * @param listeStructures liste structures sur lesquels on filtre si la liste de code étapes n'est pas null
	 * @param listeEtape liste de codes étapes
	 * @return liste d'enseignants
	 */
	public EOQualifier getListeEnseignants(String typePopulation, List<IStructure> listeStructures, NSArray<EtapePeche> listeEtape) {

		ERXKeyValueQualifier dDebQual = ERXQ.lessThan(
				EOActuelEnseignant.DATE_DEBUT_KEY,
				OutilsValidation.getNSTFinAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinQual = ERXQ.greaterThanOrEqualTo(
				EOActuelEnseignant.DATE_FIN_KEY,
				OutilsValidation.getNSTDebutAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile()));

		ERXKeyValueQualifier dFinIsNullQual = ERXQ
				.isNull(EOActuelEnseignant.DATE_FIN_KEY);

		ERXKeyValueQualifier temVacataire = ERXQ.equals(EOActuelEnseignant.TEM_STATUTAIRE.key(), Constante.NON);

		String codeCircuit = determinerCircuitSouhaite(typePopulation);

		NSArray<EOQualifier> qualifierAutres = new NSMutableArray<EOQualifier>();

		if (!StringUtils.isEmpty(codeCircuit)) {

			
			EOQualifier qual = null;

			if (!NSArrayCtrl.isEmpty(listeEtape)) {

				NSArray<EOQualifier> qualEtapes = new NSMutableArray<EOQualifier>();

				for (EtapePeche etape : listeEtape) {
					qualEtapes.add(ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(
							EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.CODE_ETAPE))).key(), etape.getCodeEtape()));
				}
				
				NSArray<EOQualifier> qualStruc = new NSMutableArray<EOQualifier>();
				
				if (!listeEtape.contains(EtapePeche.VALID_PRESIDENT) && !listeEtape.contains(EtapePeche.VALID_DRH)) {
				
					for (IStructure structure : listeStructures) {
						qualStruc.add(ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(EORepartService.TO_STRUCTURE.dot(EOStructure.C_STRUCTURE)).key(), structure.cStructure()));
					}

				}
				
				qual = ERXQ.and(ERXQ.or(qualEtapes), ERXQ.or(qualStruc));
			
			}

			String destinationAttributeName = GRHUtilities.relationshipName(EOActuelEnseignant.class.getSimpleName(), EOActuelEnseignant.LISTE_SERVICES_KEY);
			
			EOQualifier codeCircuitSurAnneeQual = new ERXQualifierInSubquery(ERXQ.and(ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(
					EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.TO_CIRCUIT_VALIDATION.dot(
							EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY)))).key(), codeCircuit), 
							ERXQ.equals(EOService.ANNEE.key(), getAnneeUniversitaire()), qual), EOService.ENTITY_NAME, EOActuelEnseignant.NO_DOSSIER_PERS_KEY, destinationAttributeName);

			if (codeCircuit.equals(CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit())) {

				EOQualifier listeServiceNullQual = 
						ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), getAnneeUniversitaire()), EOActuelEnseignant.LISTE_SERVICES.key()));

				if (NSArrayCtrl.isEmpty(listeEtape)) {
					// ce test correspond à "je ne suis pas sur l'écran 'Mes validations'"
					qualifierAutres.add(ERXQ.or(codeCircuitSurAnneeQual, listeServiceNullQual));
				} else {
					qualifierAutres.add(codeCircuitSurAnneeQual);
					
				}
				

			} else if (codeCircuit.equals(CircuitPeche.PECHE_VACATAIRE.getCodeCircuit())) {

				if (EOPecheParametre.isFicheVacatairePrevisionnelUtilisation(edc(), getAnneeUniversitaire())) {
					qualifierAutres.add(codeCircuitSurAnneeQual);
				}

				// si non utilisation des fiches prévisionnelles, alors on n'ajoute pas de filtres supplémentaires

			}
			
			
			

		}
		

		EOQualifier qualifier = ERXQ.and(dDebQual, ERXQ.or(dFinQual, dFinIsNullQual), temVacataire, ERXQ.and(qualifierAutres));

		return qualifier;
	}

	
	/**
	 * En fonction du type de fiche 
	 * @return CircuitPeche.PECHE_VACATAIRE / CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL
	 */
	private String determinerCircuitSouhaite(String typeFiche) {
		String codeCircuit = null;

		if (!StringUtils.isEmpty(typeFiche)) {
			if (Constante.DEFINITIF.equals(typeFiche)) {
				codeCircuit = CircuitPeche.PECHE_VACATAIRE.getCodeCircuit();
			} else if (Constante.PREVISIONNEL.equals(typeFiche)) {
				codeCircuit = CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit();
			}
		}
		return codeCircuit;
	}


	/**
	 * Retourne le flux XML nécessaire à l'édition d'une fiche de service.
	 * 
	 * @param enseignant : l'enseignant
	 * @param annee : l'année universitaire
	 * @param pecheSession : la	 session en cours
	 * @param componentCtrl : le CktlPecheBaseComponentCtrl instancié
	 * @param titre : le titre qui apparaitra dans le flux
	 * @return le flux XML
	 */
	public String getFluxFicheService(EOActuelEnseignant enseignant, Integer annee,Session pecheSession,CktlPecheBaseComponentCtrl componentCtrl, String titre) {

		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			w.setEscapeSpecChars(true);
			w.startDocument();
			w.startElement("fiche_service");
			{
				EOService service = enseignant.getService(annee);
				NSArray<EORepartService> repartServices = service.toListeRepartService();
				EODemande premiereDemande = null;
				boolean isCircuitPrev = true;
				if (repartServices != null &&  !repartServices.isEmpty()) {
					premiereDemande = repartServices.get(0).toDemande();
					isCircuitPrev = isCircuitPrevisionnel(premiereDemande);
				}

				if (isCircuitPrev) {
					writeElement(w, "titre", titre + " " + Constante.PREVISIONNEL);
				}	else {
					writeElement(w, "titre", titre + " " + Constante.DEFINITIF);
				}

				if (isFormatAnneeExerciceAnneeCivile()) {
					writeElement(w, "annee_universitaire", String.valueOf(annee));
				} else {
					writeElement(w, "annee_universitaire", annee + "/" + (annee + 1));
				}				

				if (EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY) != null) {
					writeElement(w, "etablissement", EOGrhumParametres.parametrePourCle(edc(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY));
				}
				writeElement(w, "date_fiche", DateCtrl.dateToString(DateCtrl.now(), "dd/MM/yyyy"));

				String cLogo = LogoEdition.cheminLogoEtablissement(edc());
				if (cLogo != null) {
					writeElement(w, "logo_etablissement", cLogo);
				}

				if (service.commentaire() != null) {
					w.writeElement("commentaire", service.commentaire());
				}

				w.startElement("profil");
				{
					writeElement(w, "nom", enseignant.toIndividu().nomAffichage().toUpperCase());
					writeElement(w, "prenom", enseignant.toIndividu().prenomAffichage());
					writeElement(w, "statut", getLibelleTypeVacation(enseignant));					
					writeElement(w, "typeVacation", getLibelleTypeAssocieVacation(enseignant));					

					List<IVacataires> listeVacations = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(annee,
							isFormatAnneeExerciceAnneeCivile());

					StringBuffer datesContrats = new StringBuffer();
					String dateDebut, dateFin, date;
					SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
					for (IVacataires vacation : listeVacations) {
						dateDebut = formatter.format(vacation.getDateDebut().getTime());
						dateFin = formatter.format(vacation.getDateFin().getTime());
						date = "du " + dateDebut + " au " + dateFin + "\n";
						datesContrats.append(date);
					}
					writeElement(w, "datesContrats", datesContrats.toString());
					w.endElement();
				}

				w.startElement("serviceHETD");
				{
					writeElement(w, "methodeCalcul", enseignant.getMethodeCalcul(annee, isFormatAnneeExerciceAnneeCivile()));
					writeElement(w, "heuresContrat", getTotalNbHeuresVacation(enseignant));					
					writeElement(w, "serviceAttribue", getServiceAttribue(enseignant));
					writeElement(w, "rehPrevu", getRehAttribue(enseignant));
					writeElement(w, "deltaPrevisionnel", getTotalNbHeuresVacation(enseignant) - getServiceAttribue(enseignant));

					if (!isCircuitPrev) {
						double serviceRealise = getServiceRealise(enseignant);
						writeElement(w, "serviceRealise", serviceRealise);
						writeElement(w, "rehRealise", getRehRealise(enseignant));

						double deltaRealise = getTotalNbHeuresVacation(enseignant) - serviceRealise;
						writeElement(w, "deltaRealise", deltaRealise);							
						BigDecimal heuresPayees = getServiceTotalHeuresAPayerHETD(enseignant, false);

						writeElement(w, "heuresPayees", heuresPayees.doubleValue());
						writeElement(w, "deltaRealisePaye", heuresPayees.subtract(new BigDecimal(serviceRealise)).doubleValue());
						writeElement(w, "heuresAPayer", getServiceTotalHeuresAPayerHETD(enseignant, true));
					}
				}
				w.endElement();


				w.startElement("etatcircuits");
				{
					DemandeCtrl demandeCtrl = new DemandeCtrl(edc());
					if (premiereDemande != null) {
						if (demandeCtrl.getDateInitialisation(premiereDemande) != null) {
							writeElement(w, "dateInit", "Initialisé le : " + DateCtrl.dateToString(demandeCtrl.getDateInitialisation(premiereDemande)));
						}		

						NSArray<EOCircuitValidation> listeCircuits;
						StringBuffer circuitsLibelle = new StringBuffer();

						if (!estToutesDemandeAvecRepartitionDefinitives(service)) {
							// dans le cas où toutes les fiches ne sont pas dans le circuit définitif, on n'affiche pas de visa répartiteur pour la fiche prévisionnelle
							NSArray<EOCircuitValidation> listeCircuitsEmpruntes = premiereDemande.rechercherCircuitsEmpruntes(true);
							listeCircuits = new NSMutableArray<EOCircuitValidation>();
							listeCircuits.add(listeCircuitsEmpruntes.get(0));
						} else	{
							listeCircuits = premiereDemande.rechercherCircuitsEmpruntes(true);
						}


						for (EOCircuitValidation circuit : listeCircuits) {
							if (CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(circuit.codeCircuitValidation())) {
								circuitsLibelle.append("\n").append(getLibelleFicheTypeCircuit(circuit)).append("\n");

								NSArray<EOEtape> etapes = getEtapesUtilisees(circuit);						
								for (EOEtape etape : etapes) {									
									circuitsLibelle.append(String.format("%-20s", StringCtrl.chaineSansAccents(EtapePeche.getEtape(etape).getLibelleVisa() + ":")));							
									Map<String, EOHistoriqueDemande> mapVisas = demandeCtrl.getMapVisas(premiereDemande, circuit);
									EOHistoriqueDemande historiqueDemande = mapVisas.get(etape.codeEtape());
									if (historiqueDemande != null) {
										circuitsLibelle.append(DateCtrl.dateToString(historiqueDemande.dateModification()));	
									}
									circuitsLibelle.append("\n");
								}
							}
							if (CircuitPeche.PECHE_VACATAIRE.getCodeCircuit().equals(circuit.codeCircuitValidation())) {
								circuitsLibelle.append("\n").append(getLibelleFicheTypeCircuit(circuit)).append("\n");								
								for (EORepartService repartService : repartServices) {
									circuitsLibelle.append(String.format("%-20s", repartService.toStructure().lcStructure() + ":"));							
									circuitsLibelle.append(demandeCtrl.etatEtape(repartService.toDemande()));
									circuitsLibelle.append("\n");																
								}
							}
						}




						w.writeElement("circuits", circuitsLibelle.toString());		
					}

				}
				w.endElement();

				String path = ERXQ.keyPath(EOServiceDetail.SERVICE_KEY);

				w.startElement("repartitionServices");
				for (EORepartService repartService : repartServices) {

					ComposantCtrl composantCtrl = new ComposantCtrl(edc(), annee);
					EOQualifier qualifierService =	ERXQ.and(ERXQ.equals(path, service), ERXQ.isNotNull(EOServiceDetail.COMPOSANT_AP_KEY));
					NSArray <EOServiceDetail> listeServicedetail = repartService.toListeServiceDetail(qualifierService);

					EOQualifier qualifierReh =	ERXQ.and(ERXQ.equals(path, service), ERXQ.isNotNull(EOServiceDetail.REH_KEY));
					NSArray <EOServiceDetail> listeServicedetailReh = repartService.toListeServiceDetail(qualifierReh);

					w.startElement("repartition");
					writeElement(w, "structure", repartService.toStructure().lcStructure());
					writeElement(w, "commentaire", repartService.commentaire());
					w.startElement("etatcircuit");
					{
						DemandeCtrl demandeCtrl = new DemandeCtrl(edc());
						EODemande demande = repartService.toDemande();
						writeElement(w, "etat", demandeCtrl.etatEtape(demande));
						if (demandeCtrl.getDateInitialisation(demande) != null) {
							writeElement(w, "dateInit", "Initialisé le : " + DateCtrl.dateToString(demandeCtrl.getDateInitialisation(demande)));
						}

						NSArray<EOCircuitValidation> circuits = demande.rechercherCircuitsEmpruntes(true);
						StringBuffer circuitsLibelle = new StringBuffer();
						for (EOCircuitValidation circuit : circuits) {
							circuitsLibelle.append("\n").append(getLibelleFicheTypeCircuit(circuit)).append("\n");
							NSArray<EOEtape> etapes = getEtapesUtilisees(circuit);						
							for (EOEtape etape : etapes) {
								circuitsLibelle.append(String.format("%-20s", StringCtrl.chaineSansAccents(EtapePeche.getEtape(etape).getLibelleVisa() + ":")));							
								Map<String, EOHistoriqueDemande> mapVisas = demandeCtrl.getMapVisas(demande, circuit);
								EOHistoriqueDemande historiqueDemande = mapVisas.get(etape.codeEtape());
								if (historiqueDemande != null) {
									circuitsLibelle.append(DateCtrl.dateToString(historiqueDemande.dateModification()));	
								}
								circuitsLibelle.append("\n");
							}
						}
						w.writeElement("circuits", circuitsLibelle.toString());
						w.endElement();
					}


					if (listeServicedetailReh.size() > 0) {
						w.startElement("rehs");
						for (EOServiceDetail serviceDetail : listeServicedetailReh) {
							w.startElement("reh");
							writeElement(w, "libelle", serviceDetail.reh().getLibelleLongCourt());
							writeElement(w, "composante", serviceDetail.toStructure().lcStructure());
							if (serviceDetail.commentaire() != null) {
								writeElement(w, "commentaire", serviceDetail.commentaire());
							}
							writeElement(w, "heuresPrevues", serviceDetail.heuresPrevues());
							if (!isCircuitPrevisionnel(premiereDemande)) {
								writeElement(w, "heuresRealisees", serviceDetail.heuresRealisees());
								writeElement(w, "heuresAPayer", serviceDetail.heuresAPayer());
								writeElement(w, "heuresPayees", serviceDetail.heuresPayees());
								writeElement(w, "delta", getDeltaHeuresPayeEtRealisePresentiel(serviceDetail.heuresPayees(), serviceDetail.heuresRealisees()));
							}
							w.endElement();
						}
						w.endElement();
					}

					w.startElement("services");
					if (listeServicedetail.size() > 0) {
						for (EOServiceDetail serviceDetail : listeServicedetail) {
							w.startElement("service");
							writeElement(w, "composante", composantCtrl.getComposanteParentAP(serviceDetail));						
							writeElement(w, "enseignement", ComposantAffichageHelper.getInstance().affichageEnseignementParent(serviceDetail.composantAP()));
							writeElement(w, "type", serviceDetail.composantAP().typeAP().code());

							if (serviceDetail.commentaire() != null) {
								writeElement(w, "commentaire", serviceDetail.commentaire());
							}

							BigDecimal heuresPrevuesHETD = getHeuresPresentielEnHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresPrevues());
							writeElement(w, "heuresPrevuesHETD", heuresPrevuesHETD);

							if (!isCircuitPrevisionnel(premiereDemande)) {
								BigDecimal heuresRealiseesHETD = getHeuresPresentielEnHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresRealisees());
								writeElement(w, "heuresRealiseesHETD", heuresRealiseesHETD);
							}
							BigDecimal heuresAPayerHETD = getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresAPayer());
							if (heuresAPayerHETD != null) {
								writeElement(w, "heuresAPayerHETD", heuresAPayerHETD);
							}
							BigDecimal heuresPayeesHETD = getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresPayees());
							if (heuresPayeesHETD != null) {
								writeElement(w, "heuresPayeesHETD", heuresPayeesHETD);
							}

							BigDecimal deltaHeuresPayeEtRealiseHETD = getDeltaHeuresPayeEtRealiseHETD(service, serviceDetail.composantAP(), serviceDetail.heuresPayees(), serviceDetail.heuresRealisees());
							if (deltaHeuresPayeEtRealiseHETD != null) {
								writeElement(w, "deltaHeuresPayeEtRealiseHETD", deltaHeuresPayeEtRealiseHETD);
							}
							if (serviceDetail.etat() != null) {							
								writeElement(w, "etat", componentCtrl.getEtatRepartition(serviceDetail.etat()));
							}
							w.endElement();
						}
					}
					w.endElement();
					w.endElement();

				}
				w.endElement();
				w.startElement("paiements");
				for (EORepartService repartService : repartServices) {
					ComposantCtrl composantCtrl = new ComposantCtrl(edc(), annee);
					EOQualifier qualifierService =	ERXQ.and(ERXQ.equals(path, service), ERXQ.isNotNull(EOServiceDetail.COMPOSANT_AP_KEY));
					NSArray <EOServiceDetail> listeServicedetail = repartService.toListeServiceDetail(qualifierService);
					if (listeServicedetail.size() > 0) {
						for (EOServiceDetail serviceDetail : listeServicedetail) {
							BigDecimal heuresAPayerHETD = getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresAPayer());
							BigDecimal heuresPayeesHETD = getServiceHeuresHETDParAP(service, serviceDetail.composantAP(), serviceDetail.heuresPayees());

							if  ((heuresAPayerHETD != null) || (heuresPayeesHETD != null)) {
								EOQualifier qualifierPaiement = EOPaiement.TO_LISTE_MISES_EN_PAIEMENT.dot(EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY).eq(serviceDetail);
								NSArray<EOPaiement> paiements = EOPaiement.fetchEOPaiements(edc(), qualifierPaiement, EOPaiement.DATE_BORDEREAU.ascs());
								if (paiements.size() > 0) {
									for (EOPaiement paiement : paiements) {
										w.startElement("paiement");
										writeElement(w, "composante", composantCtrl.getComposanteParentAP(serviceDetail));	
										writeElement(w, "enseignement", ComposantAffichageHelper.getInstance().affichageEnseignementParent(serviceDetail.composantAP()));
										writeElement(w, "type", serviceDetail.composantAP().typeAP().code());
										writeElement(w, "heuresAPayerHETD", heuresAPayerHETD);
										writeElement(w, "anneeUniversitaire", paiement.anneeUniversitaire());
										writeElement(w, "libelle", paiement.libelle());		
										writeElement(w, "dateBordereau", DateCtrl.dateToString(paiement.dateBordereau(), "dd/MM/yyyy"));
										writeElement(w, "dateArrete", DateCtrl.dateToString(paiement.dateArrete(), "dd/MM/yyyy"));
										writeElement(w, "dateFlux", DateCtrl.dateToString(paiement.dateFlux(), "dd/MM/yyyy"));

										EOMiseEnPaiement miseEnPaiement = recupererMEPDuServiceDetail(paiement, serviceDetail);
										if (miseEnPaiement != null) {
											if (miseEnPaiement.heuresPayeesHetd() != null) {
												writeElement(w, "heuresPayeesHETD", miseEnPaiement.heuresPayeesHetd());											
											} 
											//TODO: ATTENTION DOUBLON ???
											//else if (miseEnPaiement.heuresAPayerHetd() != null) {
											//	writeElement(w, "heuresAPayerHETD", miseEnPaiement.heuresAPayerHetd());
											//}
										}
										writeElement(w, "periode", serviceDetail.toPeriode().libelle());				
										writeElement(w, "temPaye", paiement.temPayeLibelleLong());
										w.endElement();
									}
								}
							}
						}
					}
				}
				w.endElement();
			}	
			w.endElement();
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}		
		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");	
		return sw.toString();
	}


	private EOMiseEnPaiement recupererMEPDuServiceDetail(EOPaiement paiement, EOServiceDetail serviceDetail) {
		EOQualifier qualifier = ERXQ.equals(EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY, serviceDetail);

		if (paiement != null) {
			NSArray<EOMiseEnPaiement> listeMEP = paiement.toListeMisesEnPaiement(qualifier);

			for (EOMiseEnPaiement unMEP : listeMEP) {
				if (unMEP.toServiceDetail() == serviceDetail) {
					return unMEP;
				}
			}
		}
		return null;
	}
	
	/**
	 * renvoie le libellé du type de vacation 
	 * 
	 * @param enseignant : enseignant
	 * @return libellé vacation
	 */
	
	public String getLibelleTypeVacationPourXML(EOActuelEnseignant enseignant) {
		String libelleTypeContrat = "";
		List<IVacataires> liste = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
		 
		if (liste.size() > 0) {
			return liste.get(0).getLibelleTypeVacation();
		}
		return libelleTypeContrat;
	}
	
	
	/**
	 * Renvoie le nombre d'heures réalisées par enseignant pour un AP donné
	 * @param enseignant : enseignant
	 * @param ap : atome pédagogique
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresRealiseesParAP(EOActuelEnseignant enseignant, EOAP ap) {
		EOService service = enseignant.getService(getAnneeUniversitaire());
		double nbHeuresRealisees = 0.0;

		if (service != null) {
			NSArray<EOServiceDetail> serviceDetails = service.listeServiceDetails();

			for (EOServiceDetail serviceDetail : serviceDetails) {
				if (ap.equals(serviceDetail.composantAP())) {
					nbHeuresRealisees += serviceDetail.heuresRealisees();
				}
			}
		}

		return nbHeuresRealisees;
	}
	
}
