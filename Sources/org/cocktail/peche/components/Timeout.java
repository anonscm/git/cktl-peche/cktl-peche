package org.cocktail.peche.components;

import org.cocktail.peche.components.commun.PecheComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;
import er.extensions.foundation.ERXStringUtilities;


public class Timeout extends PecheComponent {
	public Timeout(WOContext context) {
		super(context);
	}
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "effects.js");
		AjaxUtils.addScriptResourceInHead(context, response, "wonder.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "scripts/window.js");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlAjaxWebExt.framework", "themes/lighting.css");
	}
	public boolean isStateless() {
		return true;
	}

	/**
	 * Retourne la definition des styles par defaut. Les balises de style CSS 
	 * doivent etre donnees dans la configuration de l'application, le parametre
	 * <code>HTML_CSS_STYLES</code>.
	 */
	public String getDefaultStyles() {
		return ERXStringUtilities.toString(application().config().valuesForKey("HTML_CSS_STYLES").objects(), "\n");
	}
	public String applicationURL() {
		return application().getApplicationURL(context());
	}

	public String siteURL() {
		return application().mainWebSiteURL();
	}

	public boolean hasSiteURL() {
		return (application().mainWebSiteURL() != null);
	}
}