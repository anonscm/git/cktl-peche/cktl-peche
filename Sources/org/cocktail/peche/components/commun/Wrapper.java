package org.cocktail.peche.components.commun;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.peche.Application;
import org.cocktail.peche.components.controlers.WrapperCtrl;
import org.cocktail.peche.serveur.VersionMe;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;
import er.extensions.appserver.ERXResponseRewriter;

/**
 * Cette classe gère le menu principal.
 * 
 * @author yannick
 * 
 */
public class Wrapper extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 5281994042852154645L;

	private String onloadJS;

	private WrapperCtrl ctrl;

	public static final String BINDING_HASHEADER = "hasHeader";
	public static final String BINDING_HASFOOTER = "hasFooter";
	private static final String BINDING_SELECTED_TAB_ID = "selectedTabId";
	private static final String BINDING_SELECTED_ITEM_ID = "selectedItemId";
	
	private String titre;

	private String currentAnnee;
	private String selectedAnnee;

	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le context d'exécution.
	 */
	public Wrapper(WOContext context) {
		super(context);		
		ctrl = new WrapperCtrl(this);
		this.titre = message("application.titre");
		setAnneeSelectionnee(new Integer(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()).toString());
	}
	
	/**
	 * @return the currentAnnee
	 */
	public String currentAnnee() {
		return currentAnnee;
	}

	/**
	 * @param currentAnnee the currentAnnee to set
	 */
	public void setCurrentAnnee(String currentAnnee) {
		this.currentAnnee = currentAnnee;
	}

	/**
	 * @return the anneeSelectionnee
	 */
	public String anneeSelectionnee() {
		return selectedAnnee;
	}

	/**
	 * @param anneeSelectionnee the anneeSelectionnee to set
	 */
	public void setAnneeSelectionnee(String anneeSelectionnee) {
		this.selectedAnnee = anneeSelectionnee;
		getPecheSession().setObjectForKey(anneeSelectionnee, "NouvelleAnneeUniv");
	}

	/**
	 * @return the anneesDisplayString
	 */
	public String anneesDisplayString() {
		return (String) listeAnneesUniversitaires().valueForKey(currentAnnee());
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		super.appendToResponse(response, context);

		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app",
				"styles/bootstrap.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app",
				"styles/bootstrap-responsive.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "themes/default.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "themes/alert.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "themes/lighting.css");

		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCss3Common.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCss3MenuCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/cktlAjaxMenuCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCss3Bleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/CktlCss3MenuBleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response,
				"FwkCktlThemes.framework", "css/cktlAjaxMenuBleu.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app",
				"styles/peche.css");

		CktlAjaxUtils
		.addScriptResourceInHead(context, response, "prototype.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response,
				"FwkCktlAjaxWebExt.framework",
				"scripts/jquery/jquery-1.7.1.min.js");
	    CktlAjaxUtils.addScriptResourceInHead(context, response,
				"FwkCktlAjaxWebExt.framework",
				"scripts/cktlToolTip.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "app",
				"scripts/strings.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "app",
				"scripts/formatteurs.js");
		CktlAjaxUtils.addScriptResourceInHead(context, response, "app",
				"scripts/peche.js");
		ERXResponseRewriter.addScriptCodeInHead(response, context(),
				"jQuery.noConflict();");

		getPecheSession().removeObjectForKey("MessageErreur");

	}

	public String getTitre() {
		return this.titre;
	}

	/**
	 * Retourne le copyright de l'application.
	 * 
	 * @return une chaine de caractères.
	 */
	public String copyright() {
		return "(c) " + DateCtrl.nowDay().get(GregorianCalendar.YEAR)
				+ " Cocktail";
	}

	/**
	 * @return La version de Peche
	 */
	public String version() {
		return VersionMe.txtAppliVersion();
	}
	
	/**
	 * @return Les serveurs base de données
	 */
	public String serveurIds() {
		return Application.serverBDId();
	}
	
	public String getOnloadJS() {
		return onloadJS;
	}

	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	/**
	 * @return the hasHeader
	 */
	public boolean hasHeader() {
		return booleanValueForBinding(BINDING_HASHEADER, true);
	}

	/**
	 * @return the hasFooter
	 */
	public boolean hasFooter() {
		return booleanValueForBinding(BINDING_HASFOOTER, true);
	}

	public WrapperCtrl getCtrl() {
		return ctrl;
	}
	
	public String getSelectedTabId() {
		return (String) valueForBinding(BINDING_SELECTED_TAB_ID);
	}

	public String getSelectedItemId() {
		return (String) valueForBinding(BINDING_SELECTED_ITEM_ID);
	}
	
	/**
	 * @return L'année du début de l'année universitaire
	 */
	public String anneeDebut() {
		return getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString();
	}
	
	/**
	 * @return L'année de fin de l'année universitaire
	 */
	public String anneeFin() {
		Integer anneValue = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() + 1;
		return anneValue.toString();
	}
	
	/**
	 * 
	 * @return <code>true</code> si l'année universitaire est sur deux années civiles
	 */
	public boolean afficherAnneeFin() {
		return !getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile();
	}

	/**
	 * @return the choixAnneeUniversitaireId
	 */
	public String choixAnneeUniversitaireId() {
		return getContainerId() + "_choixAnneeUniversitaire";
	}
}
