package org.cocktail.peche.components.commun;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;

/**
 * Composant utilisé pour se connecter à PECHE.
 * @author Equippe PECHE
 */
public class Login extends PecheComponent {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -5908808542941052450L;

	private String login;
	private String password;
	private String messageErreur;

	private CktlLoginResponder loginResponder;

	/**
	 * Constructeur par défaut.
	 * @param context le contexte WebObjects.
	 */
	public Login(WOContext context) {
        super(context);
    }

	/**
	 * Enregistrment de l'utilitaire de connexion.
	 * @param loginResponder l'utilitaire de connexion.
	 */
	public void registerLoginResponder(CktlLoginResponder loginResponder) {
		this.loginResponder = loginResponder;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
	}

	/**
	 * @return l'utilitaire de connexion.
	 */
	public CktlLoginResponder loginResponder() {
		return loginResponder;
	}

	/**
	 * @return the messageErreur
	 */
	public String messageErreur() {
		return messageErreur;
	}

	/**
	 * @param messageErreur the messageErreur to set
	 */
	public void setMessageErreur(String messageErreur) {
		this.messageErreur = messageErreur;
	}

	/**
	 * @return l'indicateur d'affichage des messages d'erreur.
	 */
	public boolean isAfficherMessageErreur() {
		boolean isAfficherMessageErreur = false;
		
		isAfficherMessageErreur = !StringCtrl.isEmpty(messageErreur());
		
		return isAfficherMessageErreur;
	}

	/**
	 * @return the login
	 */
	public String login() {
		return login;
	}

	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * @return the password
	 */
	public String password() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}