package org.cocktail.peche.components.commun;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.peche.components.controlers.ComposantCtrlSingleton;

/**
 * Classe dédiée à l'affichage des composants.
 * @author Equipe GRH
 */
public class ComposantAffichageHelper {

	public static final String LIBELLE_CODE_NULL = "INCONNU";

	private static ComposantAffichageHelper INSTANCE;

	/**
	 * @return l'instance du singleton.
	 */
	public static ComposantAffichageHelper getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ComposantAffichageHelper();
		}
		return INSTANCE;
	}

	/**
	 * @return le libellé d'affichage de l'enseignement en prenant en compte le parent du composant passé en paramètre.
	 */
	public String affichageEnseignementParent(IComposant composant) {
		if (composant == null) {
			return "";
		}
		IComposant parentAP = ComposantCtrlSingleton.getInstance().getParentAP(composant);
		if (parentAP == null) {
			return "";
		}
		return affichageEnseignement(parentAP);
	}

	/**
	 * @return le libellé d'affichage de l'enseignement en prenant en compte le composant passé en paramètre.
	 */
	public String affichageEnseignement(IComposant parentAP) {
		if (ComposantCtrlSingleton.getInstance().isComposantPeche(parentAP)) {
			return parentAP.libelle();
		} else {
			return parentAP.code() + " - " + parentAP.libelle();
		}
	}

	/**
	 * @return le libellé d'affichage du type.
	 */
	public String affichageType(IAP ap) {
		if (ap != null && ap.typeAP() != null) {
			return ap.typeAP().code();
		} else {
			return "";
		}
	}

	/**
	 * 
	 * @param composant
	 * @return le code parent s'il existe.
	 */
	public String affichageCode(IComposant composant) {
		if (composant != null) {
			if (composant != null && ComposantCtrlSingleton.getInstance().isComposantPeche(composant)
					&& composant.libelle() != null) {
				return composant.libelle();
			}
			if (composant.code() != null) {
				return composant.code();
			}
		}
		return LIBELLE_CODE_NULL;
	}

	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct
	 *
	 * @param composantAP : AP à afficher
	 * @return affichage de l'AP
	 */
	public String affichageAPEtParent(IComposant composantAP) {
		IComposant parentAP = ComposantCtrlSingleton.getInstance().getParentAP(composantAP);
		if (parentAP != null && parentAP.libelle() != null && ComposantCtrlSingleton.getInstance().isComposantPeche(parentAP)) {
			return parentAP.libelle() + " : " + ComposantCtrlSingleton.getInstance().getLibelleAP(composantAP);
		} else if (parentAP.code() != null) {
			return parentAP.code() + " : " + ComposantCtrlSingleton.getInstance().getLibelleAP(composantAP);
		}
		return "INCONNU";
	}

}
