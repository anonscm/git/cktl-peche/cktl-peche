package org.cocktail.peche.components.commun;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.cocktail.peche.Messages;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.foundation.NSNumberFormatter;

import er.extensions.eof.ERXGenericRecord;

/**
 * Classe abstraite pour les formulaire de l'application PECHE (Ancienne norme,
 * ne doit plus être utilisé).
 *
 * @author Equipe PECHE.
 *
 * @param <EO>
 *            la classe des objets persistants manipulés par le formulaire.
 */
public abstract class PecheCreationForm<EO extends ERXGenericRecord> extends
		PecheComponent {

	private static final long serialVersionUID = -4667508456789723959L;

	private ModeEdition modeEdition;
	private boolean validationDidFail = false;

	private EO editedObject;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public PecheCreationForm(WOContext context) {
		super(context);
	}

	protected abstract String getFormTitreCreationKey();

	protected abstract String getFormTitreModificationKey();

	protected abstract WOActionResults pageGestion();

	protected abstract void beforeSaveChanges(ModeEdition modeEdition);

	protected abstract EO creerNouvelObjet();

	protected String getFormTitreConsultationKey() {
		return getFormTitreUnknownKey();
	}

	protected String getFormTitreUnknownKey() {
		return Messages.FORMS_MODE_EDITION_UNKNOWN;
	}

	/**
	 * @return le titre du formulaire.
	 */
	public String formTitre() {
		switch (getModeEdition()) {
		case CREATION:
			return message(getFormTitreCreationKey());
		case MODIFICATION:
			return message(getFormTitreModificationKey());
		case CONSULTATION:
			return message(getFormTitreConsultationKey());
		case UNKNOWN:
		default:
			return message(getFormTitreUnknownKey());
		}
	}

	/**
	 * @return l'action à exécuter pour faire une annulation.
	 */
	public WOActionResults annuler() {
		getPecheSession().getUiMessages().clear();
		edc().revert();
		return pageGestion();
	}

	/**
	 * @return l'action à exécuter pour enregistrer une création/mise à jour et
	 *         retourner à l'écran précédent.
	 */
	public WOActionResults enregistrerEtRetour() {
		boolean gestionOK = gererEnregistrer();
		if (!gestionOK) {
			return doNothing();
		}

		return pageGestion();
	}

	/**
	 * @return l'action à exécuter pour enregistrer une création et
	 *         poursuivre la saisie.
	 */
	public WOActionResults enregistrerEtPoursuivre() {
		boolean gestionOK = gererEnregistrer();
		if (gestionOK) {
			ajouterMessageInfoEnregPoursuivre();
			reinitialiserChamps();
		}
		return doNothing();		
	}

	/**
	 * Réinitialise les champs
	 * Redéfinition possible si besoin.
	 */
	protected void reinitialiserChamps() {
		setEditedObject(null);		
	}

	/**
	 * Ajoute le message d'information
	 * Redéfinition possible si besoin.
	 */
	protected void ajouterMessageInfoEnregPoursuivre() {
		getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, message(Messages.ENREGISTRER_ET_POURSUIVRE));
	}
	
	
	
	private boolean gererEnregistrer() {
		if (!statutValidation()) {
			return false;
		}

		enregistrer();

		if (!statutValidation()) {
			return false;
		}
		return true;
	}

	protected void enregistrer() {
		try {
			validationDidFail = false;
			beforeSaveChanges(getModeEdition());
			edc().saveChanges();
		} catch (ValidationException e) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, e.getMessage());
			
			if (e.additionalExceptions() != null) {
				for (ValidationException additionnals : e.additionalExceptions()) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, additionnals.getMessage());
				} 
			}
			
			validationDidFail = true;
			
		} catch (EOGeneralAdaptorException e) {
			edc().invalidateAllObjects();
			throw e;
		}
	}

	@Override
	public void validationFailedWithException(Throwable e, Object obj,
			String keyPath) {
		validationDidFail = true;
		getPecheSession().addSimpleMessage(TypeMessage.ERREUR, e.getMessage());
		super.validationFailedWithException(e, obj, keyPath);
	}
	
	// Properties
	/**
	 * @return true if validation did fail
	 */
	public boolean isValidationDidFail() {
		return validationDidFail;
	}

	/**
	 * @param validationDidFail
	 *            the validationDidFail to set
	 */
	public void setValidationDidFail(boolean validationDidFail) {
		this.validationDidFail = validationDidFail;
	}

	/**
	 * @return the modeEdition
	 */
	public ModeEdition getModeEdition() {
		return this.modeEdition;
	}

	/**
	 * @param modeEdition
	 *            the modeEdition to set
	 */
	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}

	/**
	 * @return le statut de la validation.
	 */
	public boolean statutValidation() {
		boolean statutOK = true;
		if (validationDidFail) {
			validationDidFail = false;
			statutOK = false;
		}

		return statutOK;
	}

	/**
	 * @return the editedParamPopHetd
	 */
	public EO editedObject() {
		if (this.editedObject == null) {
			switch (getModeEdition()) {
			case CREATION:
				this.setEditedObject(creerNouvelObjet());
				edc().insertObject(this.editedObject);
				break;
			default:
				break;
			}
		}
		return this.editedObject;
	}

	/**
	 * @param editedObject
	 *            l'objet persistant à manipuler.
	 */
	@SuppressWarnings("unchecked")
	public void setEditedObject(EO editedObject) {
		if (editedObject == null) {
			this.editedObject = null;
		} else {
			switch (getModeEdition()) {
			case CREATION:
				this.editedObject = editedObject;
				break;
			case MODIFICATION:
				this.editedObject = (EO) editedObject.localInstanceIn(edc());
				break;
			default:
				break;
			}
		}
	}

	/**
	 * @return l'indicateur du mode d'utilisation : true si mode création, false
	 *         si mode d'édition.
	 */
	public Boolean modeCreation() {
		return (this.modeEdition == ModeEdition.CREATION);
	}
	
	/**
	 * Formatter à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp2DecimalesFormatter() {
		return OutilsValidation.getApp2DecimalesFormatter();
	}
	
	/**
	 * Convertit une chaine en {@link BigDecimal}.
	 * Si la chaine est nulle ou vide, retourne <code>null</code>.
	 * @param uneValeur une valeur
	 * @return la valeur en {@link BigDecimal}
	 * @throws NumberFormatException Si la chaine ne peut pas être convertit
	 */
    public BigDecimal string2bigdec(String uneValeur) throws NumberFormatException {
    	if (uneValeur == null || uneValeur.trim().length() == 0) {
    		return null;
    	}
    	
   		return new BigDecimal(uneValeur.trim());
    }
    
	/**
	 * Convertit une chaine en {@link Integer}.
	 * Si la chaine est nulle ou vide, retourne <code>null</code>.
	 * @param uneValeur une valeur
	 * @return la valeur en {@link Integer}
	 * @throws NumberFormatException Si la chaine ne peut pas être convertit
	 */
    public Integer string2int(String uneValeur) throws NumberFormatException {
    	if (uneValeur == null || uneValeur.trim().length() == 0) {
    		return null;
    	}
    	
   		return Integer.valueOf(uneValeur.trim());
    }
    
    /**
	 * Méthode à overridé - non mis en abstract car tous les components héritant de cette classe n'ont pas forcément de selectedItemId
	 * @return selected Item Id
	 */
	public String getSelectedItemId() {
		return null;
	}
	
	/**
	 * 
	 * @return selected menu tab
	 */
	public String getSelectedMenuTab() {
		if (getSelectedItemId() != null) {
			return PecheMenuItem.getPecheMenuTab(getSelectedItemId()).getId();
		} else {
			return StringUtils.EMPTY;
		}
	}
    
    
}
