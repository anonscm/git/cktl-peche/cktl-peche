package org.cocktail.peche.components.commun;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EODataSource;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXKey;

/**
 * Composant générique pour l'application Peche.
 *
 * @author Yannick Mauray
 *
 * @param <EO>
 *            le type générique de l'objet métier.
 * @param <F>
 *            le formulaire générique de saisie pour l'objet métier.
 */
public abstract class PecheGestionComponent<EO extends ERXGenericRecord, F extends PecheCreationForm<EO>>
		extends PecheComponent {

	private static final long serialVersionUID = -2221006152739501526L;

	protected static final int NUMBER_OF_OBJECTS_PER_BATCH = 20;

	private ERXDisplayGroup<EO> displayGroup = null;
	private EODataSource typeDatasource = null;
	private EO selectedItem = null;
	private EO currentItem = null;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'exécution.
	 */
	public PecheGestionComponent(WOContext context) {
		super(context);
	}

	
	public int getNumberOfObjectsPerBatch() {
		int numberObjects = ((CktlWebApplication) WOApplication.application()).config().intForKey("org.cocktail.peche.number.of.objects.per.batch");
		if (numberObjects <= 0) {
			return NUMBER_OF_OBJECTS_PER_BATCH;			
		} else {
			return numberObjects;
		}
	}

	protected abstract String getEntityName();

	protected abstract Class<?> getFormClass();

	protected abstract String getConfirmSupressMessageKey();

	/**
	 * @return the displayGroup
	 */
	public ERXDisplayGroup<EO> displayGroup() {
		if (this.displayGroup == null) {
			this.displayGroup = new ERXDisplayGroup<EO>();
			this.displayGroup.setDataSource(getDatasource());
			this.displayGroup.setDelegate(new DisplayGroupDelegate());
			this.displayGroup
					.setNumberOfObjectsPerBatch(getNumberOfObjectsPerBatch());
			this.displayGroup.setSelectsFirstObjectAfterFetch(false);
			EOQualifier qualifier = getDisplayGroupQualifier();
			if (qualifier != null) {
				this.displayGroup.setQualifier(qualifier);
			}
			this.displayGroup.fetch();
		}
		return this.displayGroup;
	}

	protected EOQualifier getDisplayGroupQualifier() {
		return null;
	}

	/**
	 * @return a datasource for fetching the EC
	 */
	protected EODataSource getDatasource() {
		if (this.typeDatasource == null) {
			this.typeDatasource = new ERXDatabaseDataSource(edc(),
					getEntityName());
		}
		return this.typeDatasource;
	}

	protected void setDataSource(EODataSource dataSource) {
		this.typeDatasource = dataSource;
	}

	/**
	 * Callback appelé par le DisplayGroupDelegate pour mémoriser l'élément
	 * sélectionné dans l'interface.
	 *
	 * @param selectedObject
	 *            l'élément sélectionné dans l'interface.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setSelectedObjectFromDisplayGroup(Object selectedObject) {
		this.selectedItem = (EO) selectedObject;
	}

	protected EO getSelectedItem() {
		return this.selectedItem;
	}

	@SuppressWarnings("unchecked")
	protected F getFormPage(final ModeEdition modeEdition) {
		F page = (F) pageWithName(getFormClass().getName());
		page.setModeEdition(modeEdition);
		return page;
	}

	/**
	 * Message de confirmation à la suppression d'un élément.
	 *
	 * @return message
	 */
	public String onClickBefore() {
		String msgConfirm = message(getConfirmSupressMessageKey());
		return "confirm('" + msgConfirm + "')";
	}

	/**
	 * Action générique d'ajout.
	 *
	 * @return une instance du formulaire en mode création.
	 */
	public WOActionResults ajouterAction() {
		F form = getFormPage(ModeEdition.CREATION);
		return form;
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (selectedItem != null) {
			F form = getFormPage(ModeEdition.MODIFICATION);
			form.setEditedObject(selectedItem);
			return form;
		} else {
			return null;
		}
	}

	/**
	 * Action générique de suppression. Supprime l'objet sélectionné. Cette
	 * action doit être appelée après confirmation par l'utilisateur.
	 *
	 * @return null.
	 */
	public WOActionResults deleteAction() {
		try {
			if (selectedItem != null) {
				edc().deleteObject(selectedItem);
				edc().saveChanges();
				selectedItem = null;
				displayGroup.fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
		}

		return doNothing();
	}

	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String containersId() {
		return getComponentId() + "_containersId";
	}

	/**
	 * @return l'identifiant du conteneur de la table.
	 */
	public String tableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}

	/**
	 * @return l'identifiant du conteneur des boutons.
	 */
	public String buttonsContainerId() {
		return getComponentId() + "_buttonsContainerId";
	}

	/**
	 * @return l'identifiant du conteneur des données de la table.
	 */
	public String tableViewId() {
		return getComponentId() + "_tableViewId";
	}

	public EO getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(EO currentItem) {
		this.currentItem = currentItem;
	}

	protected void filtrerDisplayGroup(ERXKey<Object> key, String token) {
		if (!StringCtrl.isEmpty(token)) {
			displayGroup().setQualifier(key.contains(token));
		} else {
			displayGroup().setQualifier(null);
		}
		displayGroup().updateDisplayedObjects();
	}

	protected void filtrerDisplayGroup(EOQualifier qualifier) {
		displayGroup().setQualifier(qualifier);
		displayGroup().updateDisplayedObjects();
	}
	
	/**
	 * Méthode à overridé - non mis en abstract car tous les components héritant de cette classe n'ont pas forcément de selectedItemId
	 * @return selected Item Id
	 */
	public String getSelectedItemId() {
		return null;
	}
	
	
	public String getSelectedMenuTab() {
		return PecheMenuItem.getPecheMenuTab(getSelectedItemId()).getId();
	}
	
	
}
