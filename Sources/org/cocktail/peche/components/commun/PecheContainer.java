package org.cocktail.peche.components.commun;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Cette classe modélise une partie des pages de l'application Peche qui semble
 * être systématiquement répétée. Si tel est le cas, il faudrait envisager
 * d'intégrer ce composant directement dans le wrapper.
 * 
 * @author Yannick Mauray
 * 
 */
public class PecheContainer extends WOComponent {

	private static final long serialVersionUID = -1486573963949088052L;
	
	private String containerId;

	/**
	 * Constructeur.
	 * @param context le contexte d'exécution.
	 */
	public PecheContainer(WOContext context) {
		super(context);
	}

	public String getContainerId() {
		return this.containerId;
	}
	
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
}