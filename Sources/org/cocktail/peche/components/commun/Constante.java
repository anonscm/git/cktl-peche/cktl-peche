package org.cocktail.peche.components.commun;

/**
 * Interface permettant de gérer des constantes
 *
 * @author Chama LAATIK
 *
 */
public final class Constante {

	private Constante() {

	}

	public static final String ACCEPTE = "A";
	public static final String REFUSE = "R";
	public static final String VALIDE = "V";
	public static final String ATTENTE = "E";
	public static final String VERIF = "C";

	public static final String ETAT_ACCEPTE = "Accepté";
	public static final String ETAT_REFUSE = "Refusé";
	public static final String ETAT_VALIDE = "Validé";
	public static final String ETAT_ATTENTE = "En attente";
	public static final String ETAT_A_VERIFIER = "A vérifier";
	public static final String ETAT_NON_INITIALISE = "Non initialisé";
	public static final String ETAT_EN_VALIDATION = "En validation";
	public static final String ETAT_BASCULE = "Fiche basculée";
	
	//Message d'erreur
	public static final String VOEUX_RENSEIGNER_AU_MOINS_ENSEIGNEMENT_OU_ETAB_EXTERNE = "Il faut renseigner au moins un enseignement";
	public static final String VOEUX_RENSEIGNER_UN_ENSEIGNEMENT_OU_UN_ETAB_EXTERNE = "Il faut renseigner soit un enseignement prédéfini ou bien un établissement externe";
	//A n'utiliser que si on a un paramètre %s à passer au String.format
	public static final String VOEUX_SUPERIEUR_HEURES_MAQUETTE_PARAMETRE = "Le total d'heures souhaitées pour l'enseignement %s va dépasser le "
			+ "total d'heures de la maquette prévu";
	
	public static final String REPARTITION_DEF_SUPERIEUR_HEURES_CONTRAT = "Le service définitif dépasse le nombre d'heures du contrat. "
			+ "Veuillez contacter la DRH pour modifier le contrat.";

	public static final String REPARTITION_DEMANDE_AUTRE_PREVISIONNEL = "Vous ne pouvez pas valider cette fiche tant qu'il existe une fiche d'une autre composante qui est toujours dans le circuit prévisionnel";
	
	public static final String REPARTITION_EXISTANTE = "Il existe une répartition sur cette UE, veuillez la supprimer avant de continuer";
	public static final String UE_ANNEE_SUIVANTE_EXISTANTE = "Cette UE existe sur l'année suivante, veuillez la supprimer avant de continuer";
	public static final String REPARTITION_EXISTANTE_REH = "Il existe une répartition sur cette activité REH, veuillez la supprimer avant de continuer";
	public static final String RENSEIGNER_UN_ENSEIGNEMENT_OU_UN_ETAB_EXTERNE = "Veuillez renseigner soit une composante de l'établissement "
			+ "soit un établissement externe dans lequel se déroulera l'enseignement";
	public static final String LISTE_AP_NON_RENSEIGNE = "Veuillez renseigner au moins un AP de l'enseignement";
	public static final String LISTE_COMP_NON_RENSEIGNE = "Veuillez renseigner au moins une composante";
	public static final String LISTE_REH_NON_RENSEIGNE = "Veuillez renseigner un REH";
	public static final String REPARTITION_SUPERIEUR_HEURES_MAQUETTE = "Le total d'heures de la répartition pour cet enseignement va dépasser le "
			+ "total d'heures de la maquette";
	//A n'utiliser que si on a un paramètre %s à passer au String.format
	public static final String REPARTITION_SUPERIEUR_HEURES_MAQUETTE_PARAMETRE = "Le total d'heures de la répartition pour l'enseignement %s "
			 + "va dépasser le total d'heures de la maquette";

	public static final String REPARTITION_ERREUR_AP_NON_RENSEIGNE = "Veuillez sélectionner un enseignement";
	public static final String REPARTITION_ERREUR_HEURES_A_PAYER_INTERDIT_SI_PERIODE_NULL = "Vous ne pouvez pas saisir d'heures à payer tant que la période des heures réalisées n'est pas renseignée.";

	public static final String REPARTITION_UE_RENSEIGNER_UN_ENSEIGNANT = "Veuillez sélectionner un enseignant";
	public static final String REPARTITION_UE_INTERDIT_SUPPRESSION = "Vous n'avez pas le droit de supprimer cette répartition";
	public static final String REPARTITION_UE_INTERDIT_SUPPRESSION_PAYEES = "Vous n'avez pas le droit de supprimer cette répartition car il y a des heures payées";
	public static final String REPARTITION_UE_INTERDIT_SUPPRESSION_PAIEMENT = "Vous n'avez pas le droit de supprimer cette répartition car elle fait partie d'un paiement";
	public static final String REPARTITION_UE_FICHE_DEJA_VALIDEE = "La fiche de service de cet enseignant a déjà été validée pour la structure %s. Veuillez choisir une autre structure. ";
	
	public static final String PARAM_MEP_TH_DATE_NON_VALIDE = "La date de début d'application doit être supérieure au ";
	
	public static final String PARAM_MEP_PFP_DATE_NON_VALIDE = "La date de début d'application doit être supérieure au ";
	
	public static final String PARAM_FLUX_PAIEMENT_INEXISTANT = "La génération du flux de paiement est impossible car il n'y a pas de type de flux de paiement existant à cette date";
	
	public static final String NON = "N";
	public static final String OUI = "O";
	public static final String NON_LONG = "Non";
	public static final String OUI_LONG = "Oui";
	
	public static final int CENT = 100;
	
	public static final String DECHARGE = "Décharge";
	public static final String MODALITE = "Modalité de service";
	public static final String MINORATION = "Minoration de service";
	
	public static final String PREVISIONNEL = "prévisionnel";
	public static final String DEFINITIF = "définitif";
	
	public static final String TITULAIRE = "Titulaire";
	public static final String NON_TITULAIRE = "Non titulaire";
	public static final String STATUTAIRE = "Statutaire";
	public static final String VACATAIRE = "Vacataire";
	public static final String GENERIQUE = "Générique";
	public static final String GENERIQUE_STAT = "Gén. statut.";
	public static final String GENERIQUE_VACAT = "Gén. vacat.";
	public static final String GENERIQUE_CONTROLE_LONGUEUR_NOM = "Veuillez saisir au moins 3 caractères";
	public static final String ENSEIGNANT_ERREUR_NON_AFFECTE = "La composante ou le département n'est pas renseigné";
	
	public static final String NON_DEFINI = "<Non Défini>";
	
	public static final String CIRCUIT_VACATAIRE_A_DETERMINER = "CVAD";
	
	public static final String APP_PECHE = "PECHE";
	
	public static final String HEURES_A_PAYER_INFERIEUR_HEURES_EN_ATTENTE = "Les heures à payer ne peuvent pas être inférieures aux heures en attente de paiement";
	public static final String HEURES_REALISEES_INFERIEUR_TOTAL_HEURES_PAYEES_ET_HEURES_A_PAYER = "les heures réalisées ne peuvent pas être "
			+ "inférieures au total des heures payées et les heures à payer";
	public static final String HEURES_REALISEES_INFERIEUR_HEURES_EN_PAIEMENT = "Les heures réalisées ne peuvent pas être inférieures à %s car elles sont en paiement";
	public static final String HEURES_INFERIEURES_A_ZERO = "Les heures ne peuvent pas être inférieures à 0";
	
	public static final String ONGLET_COMPOSANTE_TOUS_CODE = "tous";
	public static final String ONGLET_COMPOSANTE_TOUS_LIBELLE = "Tous";
	
	public static final String TABLEAU_BORD_RENSEIGNER_AU_MOINS_UN_ETAT = "Veuillez saisir au moins un état du dossier";
	public static final String LISTE_RESULTAT_VIDE = "La liste des résultats est vide";
	
	public static final String COMMENTAIRE_COPIE_AUTOMATIQUE = "Copié automatiquement";
	public static final String COMMENTAIRE_COPIE_AUTOMATIQUE_ANNEE_PRECEDENTE = "Copie de l'année universitaire précédente";
	
	public static final String BASCULE_FICHE_VOEUX_NON_REALISEE = "Une fiche de service prévisionnel existe déjà pour cet enseignant, elle n'a donc pas été créée";
	
	public static final String FICHE_PREVISIONNEL_CREE = "Une fiche de service prévisionnel a bien été créée";
	
	public static final String UEF_COMPOSANTE_AJOUT_SANS_SELECTION = "Aucune composante de l'arbre n'a été sélectionnée";
	public static final String UEF_COMPOSANTE_AJOUT_DOUBLON = "Cette composante a déjà été ajoutée dans le tableau";
	public static final String UEF_COMPOSANTE_AJOUT_CS = "Vous ne pouvez pas ajouter de composante de scolarité car déjà présente par défaut pour les UE Flottantes";
	public static final String UEF_COMPOSANTE_SUPPRESSION_SANS_SELECTION = "Aucune composante du tableau n'a été sélectionnée";
	
	public static final String ADMINISTRATION = "Administration";
	public static final String DEPARTEMENT = "Département";
	
	public static final String DUPLICATION_FICHE = "%s fiche(s) de service ont été créées sur l'année cible";
	public static final String DUPLICATION_UEF_DANS = "%s UE Flottantes dans l'établissement ont été créées sur l'année cible";
	public static final String DUPLICATION_UEF_HORS = "%s UE Flottantes hors établissement ont été créées sur l'année cible";
	
	public static final String VALIDATION_AUTO_FICHE_VALIDE = "%s fiche(s) de service ont été validées automatiquement";

	public static final String CODE_AP_PREFIX = "codeAP_";
	public static final String CODE_UE_PREFIX = "codeUE_";

	
}
