package org.cocktail.peche.components.commun;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.PecheAutorisationsCache;
import org.cocktail.peche.Session;
import org.cocktail.peche.outils.ListHelper;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Cette classe regroupe les méthodes communes aux composants "Nouvelle norme"
 * et aux autres. A terme, il faut supprimer les composants "Ancienne norme"
 * (ceux qui héritent de "PecheComponent)".
 * 
 * @author Yannick Mauray
 * 
 */
public abstract class CktlPecheCommun extends CktlAjaxWOComponent {

	private static final long serialVersionUID = -1963465384316544814L;

	private ListHelper listHelper;
	
	private String typeFiche;
	private boolean modeConsultation;
	

	/**
	 * Constructeur.
	 * @param context le contexte WebObjects.
	 */
	public CktlPecheCommun(WOContext context) {
		super(context);
	}

	/**
	 * Retourne la session de l'utilisateur connecté.
	 * 
	 * @return La session de l'utilisateur connecté
	 */
	public Session getPecheSession() {
		return (Session) super.session();
	}

	/**
	 * Retourne les autorisations de l'utilisateur connecté.
	 * 
	 * @return Les autorisations de l'utilisateur connecté
	 */
	public PecheAutorisationsCache getAutorisation() {
		return getPecheSession().getPecheAutorisationCache();
	}	
	
	/**
	 * renvoie la personne connectée
	 * @return personne connectée
	 */
	public final EOPersonne personneConnecte() {
		return getPecheSession().getApplicationUser().getPersonne();
	}
	
	/**
	 * renvoie la personne connectée
	 * @param ec : editingContext
	 * @return personne connectée
	 */
	public final EOPersonne personneConnecte(EOEditingContext ec) {
		return EOPersonne.fetchByKeyValue(ec, EOPersonne.PERS_ID_KEY, getPecheSession().getApplicationUser().getPersId());
	}
	
	/**
	 * liste des années universitaires n , n+1 et n-1 en prenant en compte pour l'affichage si c'est 
	 * en année civile
	 * @return la liste des années universitaires
	 */
	
	public NSDictionary<String, String> listeAnneesUniversitaires() {
		NSDictionary<String, String> listeAnnees = new NSMutableDictionary<String, String>();

		int anneeDebut = getPecheSession().getPecheParametres().getAnneeUniversitaire();
		
		if (getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) {
			listeAnnees.takeValueForKey(new Integer(anneeDebut - 1).toString() , new Integer(anneeDebut - 1).toString());
			listeAnnees.takeValueForKey(new Integer(anneeDebut).toString() , new Integer(anneeDebut).toString());
			listeAnnees.takeValueForKey(new Integer(anneeDebut + 1).toString() , new Integer(anneeDebut + 1).toString());
		} else {
			listeAnnees.takeValueForKey(new Integer(anneeDebut - 1).toString() + "/" + new Integer(anneeDebut).toString(), new Integer(anneeDebut - 1).toString());
			listeAnnees.takeValueForKey(new Integer(anneeDebut).toString() + "/" + new Integer(anneeDebut + 1).toString(), new Integer(anneeDebut).toString());
			listeAnnees.takeValueForKey(new Integer(anneeDebut + 1).toString() + "/" + new Integer(anneeDebut + 2).toString(), new Integer(anneeDebut + 1).toString());
		}
		
		return listeAnnees;
	}
	
	public ListHelper getListHelper() {
		if (listHelper == null) {
			listHelper = new ListHelper();
		}
		
		return listHelper;
	}
	
	public String getTypeFiche() {
		return typeFiche;
	}


	public void setTypeFiche(String typeFiche) {
		this.typeFiche = typeFiche;
	}

	public boolean isTypeFichePrevisionnel() {
		return Constante.PREVISIONNEL.equals(getTypeFiche());
	}

	public boolean isModeConsultation() {
		return modeConsultation;
	}


	public void setModeConsultation(boolean modeConsultation) {
		this.modeConsultation = modeConsultation;
	}
	
}