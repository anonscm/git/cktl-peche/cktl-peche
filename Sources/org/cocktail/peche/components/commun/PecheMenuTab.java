package org.cocktail.peche.components.commun;

/**
 * Liste les onglets du menu.
 */
public enum PecheMenuTab {
	/** Menu, onglet "Enseignants Statutaires". */
	ENSEIGNANTS_STATUTAIRES ("enseignantsstatutaires"),
	/** Menu, onglet "Enseignants Vacataires". */
	ENSEIGNANTS_VACATAIRES ("enseignantsvacataires"),
	/** Menu, onglet "Enseignements". */
	ENSEIGNEMENTS ("enseignements"),
	/** Menu, onglet "Paramétrage". */
	PARAMETRAGE ("parametrage"),
	/** Menu, onglet "Paramétrage". */
	MISE_EN_PAIEMENT ("MiseEnPaiement"),
	/** Menu, onglet "Suivi". */
	SUIVI ("suivi");
	
	/** Id de l'onglet du menu. */
	private String id;

	/**
	 * Retourne l'id de l'onglet du menu.
	 *
	 * @return L'id de l'onglet du menu
	 */
	public String getId() {
		return id;
	}

	/**
	 * Constructeur.
	 *
	 * @param idTab Un id d'onglet
	 */
	PecheMenuTab(String idTab) {
		this.id = idTab;
	}
}
