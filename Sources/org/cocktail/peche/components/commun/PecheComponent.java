package org.cocktail.peche.components.commun;

import org.cocktail.peche.Application;
import org.cocktail.peche.Session;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.components.ERXClickToOpenSupport;

/**
 * Cette classe rassemble les fonctionalités communes à tous les composants de
 * l'application Peche.
 *
 * @author Yannick Mauray
 */
public class PecheComponent extends CktlPecheCommun {

	/**
	 *
	 */
	private static final long serialVersionUID = 3121277888633976801L;

	public static final String BINDING_EDITING_CONTEXT = "editingContext";

	private Session pecheSession = (Session) session();
	public Application application = (Application)application();
	
	private EOEditingContext edc = null;
	private String onloadJS;

	public static final boolean IS_CLICK_TO_OPEN_ENABLED = Boolean
			.valueOf(System.getProperty("er.component.clickToOpen", "false"));

	/**
	 * Cet objet permet de mémoriser l'élément sélectionné par le displayGroup.
	 *
	 * @author Yanick Mauray
	 *
	 */
	public class DisplayGroupDelegate {
		/**
		 * Change l'element sélectionné.
		 *
		 * @param group
		 *            : groupe sélectionné
		 */
		public final void displayGroupDidChangeSelectedObjects(
				final WODisplayGroup group) {
			if (group.selectedObject() != null) {
				setSelectedObjectFromDisplayGroup(group.selectedObject());
			}
		}
	}

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'exécution.
	 */
	public PecheComponent(WOContext context) {
		super(context);
	}

	/**
	 * Fonction de rappel permettant de mémorisé l'élement sélectionné dans un
	 * displayGroup.
	 *
	 * @param selectedObject
	 *            l'élément sélectionné.
	 */
	public void setSelectedObjectFromDisplayGroup(Object selectedObject) {
		/* L'implémentation par défaut ne fait rien. */
	}

	/**
	 * @return l'editingContext courant.
	 */
	public EOEditingContext edc() {
		if (this.edc == null) {
			if (hasBinding(BINDING_EDITING_CONTEXT)) {
				this.edc = (EOEditingContext) valueForBinding(BINDING_EDITING_CONTEXT);
			} else {
				// this.edc = ERXEC.newEditingContext();
				this.edc = pecheSession.defaultEditingContext();
			}
		}
		return this.edc;
	}

	/**
	 * Permet d'ajouter le support du "clickToOpen".
	 *
	 * @param woresponse
	 *            la réponse à envoyer au navigateur.
	 * @param wocontext
	 *            le contexte d'exécution.
	 */
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		ERXClickToOpenSupport.preProcessResponse(woresponse, wocontext,
				IS_CLICK_TO_OPEN_ENABLED);
		super.appendToResponse(woresponse, wocontext);
		ERXClickToOpenSupport.postProcessResponse(getClass(), woresponse,
				wocontext, IS_CLICK_TO_OPEN_ENABLED);
	}

	/**
	 * @return the onloadJS
	 */
	public String onloadJS() {
		return onloadJS;
	}

	/**
	 * @param onloadJS
	 *            the onloadJS to set
	 */
	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

	/**
	 *
	 * @param key
	 *            la clé du message à traduire.
	 * @return le message traduit correspondant à la clé.
	 */
	public String message(String key) {
		return getPecheSession().localizer().localizedStringForKey(key);
	}

	/**
	 * @param suffix
	 *            le suffix à ajouer à l'identifiant du compsant.
	 * @return un identifiant unique à partir de l'identifiant du composant.
	 */
	public String getId(String suffix) {
		return getComponentId() + "_" + suffix;
	}
	
	public Application application() {
	    return (Application)super.application();
	}

}
