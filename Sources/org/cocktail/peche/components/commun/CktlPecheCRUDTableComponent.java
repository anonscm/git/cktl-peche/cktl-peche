package org.cocktail.peche.components.commun;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.extensions.eof.ERXGenericRecord;

/**
 * Classe d'abstraction des composants PECHE qui font des opérations de
 * création, mise à jour et suppression.
 * 
 * @author Yannick Mauray
 * 
 * @param <EO> la classe des objets persistants gérés par cette classe.
 * @param <F> la classe du formulaire de création/mise à jour. 
 */
public abstract class CktlPecheCRUDTableComponent<EO extends ERXGenericRecord, F extends PecheCreationForm<EO>>
		extends CktlPecheTableComponent<EO> {

	private static final long serialVersionUID = 673395630848018575L;

	/**
	 * Constructeur par défaut.
	 * @param context le contecte WebObjects.
	 */
	public CktlPecheCRUDTableComponent(WOContext context) {
		super(context);
	}

	protected abstract Class<?> getFormClass();

	protected abstract String getConfirmSupressMessageKey();

	@SuppressWarnings("unchecked")
	protected F getFormPage(final ModeEdition modeEdition) {
		F page = (F) pageWithName(getFormClass().getName());
		page.setModeEdition(modeEdition);
		return page;
	}

	/**
	 * Message de confirmation à la suppression d'un élément.
	 * 
	 * @return message
	 */
	@Override
	public String onClickBefore() {
		String msgConfirm = message(getConfirmSupressMessageKey());
		return "confirm('" + msgConfirm + "')";
	}

	/**
	 * Action générique d'ajout.
	 * 
	 * @return une instance du formulaire en mode création.
	 */
	public WOActionResults ajouterAction() {
		F form = getFormPage(ModeEdition.CREATION);
		return form;
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			F form = getFormPage(ModeEdition.MODIFICATION);
			form.setEditedObject(getSelectedObject());
			return form;
		} else {
			return null;
		}
	}

	/**
	 * Action générique de suppression. Supprime l'objet sélectionné. Cette
	 * action doit être appelée après confirmation par l'utilisateur.
	 * 
	 * @return null.
	 * @throws Exception 
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				edc().deleteObject(getSelectedObject());
				getDisplayGroup().deleteSelection();		//permet de supprimer la ligne (uniquement en affichage) s'il y a des images dedans 
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
		}

		return doNothing();
	}
}
