package org.cocktail.peche.components.commun;



public enum IndicateurValidationAuto {

	
	ETAT_IND_VALIDE ("VAL", "Indicateur indiquant que la demande pourra être validé automatique"),
	ETAT_IND_REH ("REH", "Une répartition REH existe sur cette fiche de service qui ne sera donc pas validée automatiquement"),
	ETAT_IND_UEF ("UEF", "Une répartition sur une UE Flottante hors établissement existe sur cette fiche de service qui ne sera donc pas validée automatiquement"),
	ETAT_IND_REF ("REF", "Une répartition REH et sur une UE Flottante hors établissement existent sur cette fiche de service qui ne sera donc pas validée automatiquement"),
	ETAT_IND_INV ("INV", "Toutes les répartitions ne sont pas validées");
	
	private String codeIndicateur;
	
	private String messageAvertissement;

	private IndicateurValidationAuto(String codeIndicateur,
			String messageValidationNonAuto) {
		this.codeIndicateur = codeIndicateur;
		this.messageAvertissement = messageValidationNonAuto;
	}

	public String getCodeIndicateur() {
		return codeIndicateur;
	}

	public void setCodeIndicateur(String codeIndicateur) {
		this.codeIndicateur = codeIndicateur;
	}

	public String getMessageAvertissement() {
		return messageAvertissement;
	}

	public void setMessageAvertissement(String messageAvertissement) {
		this.messageAvertissement = messageAvertissement;
	}
	
	/**
	 * 
	 * @param codeIndicateur
	 * @return
	 */
	public static IndicateurValidationAuto getIndicateurValidation(String codeIndicateur) {
		IndicateurValidationAuto resultat = null;
		
		for (int i = 0; i < IndicateurValidationAuto.values().length; i++) {
			IndicateurValidationAuto etape = IndicateurValidationAuto.values()[i];
			
			if (etape.codeIndicateur.equals(codeIndicateur)) {
				resultat = etape;
			}
		}
		
		return resultat;
	}
	
	
	
}
