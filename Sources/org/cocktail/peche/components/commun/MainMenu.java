package org.cocktail.peche.components.commun;

import org.cocktail.peche.PecheAutorisationsCache;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.enseignants.CktlPecheEnseignantsGeneriques;
import org.cocktail.peche.components.enseignants.CktlPecheEnseignantsStatutaires;
import org.cocktail.peche.components.enseignants.CktlPecheEnseignantsVacataires;
import org.cocktail.peche.components.enseignants.FicheEnseignantComponent;
import org.cocktail.peche.components.enseignants.ficheVoeux.FicheVoeuxArbitrage;
import org.cocktail.peche.components.enseignants.ficheVoeux.FicheVoeuxEnseignant;
import org.cocktail.peche.components.enseignants.ficheVoeux.ListeFichesVoeux;
import org.cocktail.peche.components.enseignants.repartitionCroisee.ListeEnseignants;
import org.cocktail.peche.components.enseignants.repartitionCroisee.ListeEnseignement;
import org.cocktail.peche.components.enseignants.validation.CktlPecheValidationFicheService;
import org.cocktail.peche.components.enseignements.OffreFormation;
import org.cocktail.peche.components.enseignements.RechercheParUEEC;
import org.cocktail.peche.components.enseignements.UEEC;
import org.cocktail.peche.components.enseignements.UEFlottantes.CktlPecheTableUEFlottante;
import org.cocktail.peche.components.enseignements.validation.RechercheValidationParUE;
import org.cocktail.peche.components.miseenpaiement.ListeMEP;
import org.cocktail.peche.components.parametres.circuitvalidation.ApplicationCircuitsValidation;
import org.cocktail.peche.components.parametres.circuitvalidation.CircuitsValidationPeche;
import org.cocktail.peche.components.parametres.droits.TableDroits;
import org.cocktail.peche.components.parametres.duplication.Duplication;
import org.cocktail.peche.components.parametres.hetd.CktlPecheParamPopHETD;
import org.cocktail.peche.components.parametres.hetd.TypesAPGestion;
import org.cocktail.peche.components.parametres.mep.FluxPaieGestion;
import org.cocktail.peche.components.parametres.mep.TauxHorGestion;
import org.cocktail.peche.components.parametres.reh.RehGestion;
import org.cocktail.peche.components.parametres.ueflottantes.ChoixComposantes;
import org.cocktail.peche.components.tableauxDeBord.RechercheTableauDeBord;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Cette classe modélise le menu de l'application.
 * @author Yannick Mauray
 * @author Pascal MACOUIN
 * @author Chama LAATIK
 *
 */
public class MainMenu extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 946731173035420767L;

	private static final String BINDING_SELECTED_TAB_ID = "selectedTabId";
	private static final String BINDING_SELECTED_ITEM_ID = "selectedItemId";

	private boolean existenceServiceEnseignantDejaRecherche;
	private boolean existenceServiceEnseignant;
	
	private boolean existenceVoeuxEnseignantDejaRecherche;
	private boolean existenceVoeuxEnseignant;
	
	private boolean isEnseignantStatutaireDejaRecherche;
	private boolean isEnseignantStatutaire;
	
	private boolean isEnseignantVacataireDejaRecherche;
	private boolean isEnseignantVacataire;
	
	/**
	 * Constructeur.
	 * @param context le contexte d'exécution.
	 */
	public MainMenu(WOContext context) {
		super(context);
		existenceServiceEnseignantDejaRecherche = false;
		existenceVoeuxEnseignantDejaRecherche = false;
	}

	/**
	 * Récupère l'onglet selectionné
	 * @return nom de l'onglet
	 */
	public String getSelectedTabId() {
		String selectedTabId = valueForStringBinding(BINDING_SELECTED_TAB_ID, "");

		// Si le mot-clé "*first" est indiqué, on recherche le premier élément autorisé
		if ("*first".equals(selectedTabId)) {
			PecheAutorisationsCache autorisation = getAutorisation();

			if (hasDroitShowMenuEnseignantsStatutaires()) {
				selectedTabId = PecheMenuTab.ENSEIGNANTS_STATUTAIRES.getId();
			} else if (hasDroitShowMenuEnseignantsVacataires())  {
				selectedTabId = PecheMenuTab.ENSEIGNANTS_VACATAIRES.getId();
			} else if (autorisation.hasDroitShowMenuEnseignements()) {
				selectedTabId = PecheMenuTab.ENSEIGNEMENTS.getId();
			} else if (autorisation.hasDroitShowMenuParametrage()) {
				selectedTabId = PecheMenuTab.PARAMETRAGE.getId();
			}
		}

		return selectedTabId;
	}

	public String getSelectedItemId() {
		return valueForStringBinding(BINDING_SELECTED_ITEM_ID, "");
	}

	/**
	 * Renvoie si la fiche de service a été initialisé ou pas
	 * @return vrai/faux
	 */
	public boolean getExistenceServiceEnseignant() {
		if (!existenceServiceEnseignantDejaRecherche) {
			EOActuelEnseignant enseignant = getPecheSession().getApplicationUser().getEnseignant();
			EOService service = null;

			if (enseignant != null) {
				service = enseignant.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			}

			if (service == null) {
				existenceServiceEnseignant = true;
			} else {
				existenceServiceEnseignant = false;
			}
			existenceServiceEnseignantDejaRecherche = true;
		}

		return existenceServiceEnseignant;
	}

	/**
	 * Renvoie si la fiche de voeux a été initialisé ou pas
	 * logique inversée pour disabled
	 * @return vrai/faux
	 */
	public boolean getExistenceVoeuxEnseignant() {
		if (!existenceVoeuxEnseignantDejaRecherche) {
			EOActuelEnseignant enseignant = getPecheSession().getApplicationUser().getEnseignant();
			EOFicheVoeux voeux = null;

			if (enseignant != null) {
				voeux = enseignant.getFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			}

			if (voeux == null) {
				existenceVoeuxEnseignant = true;
			} else {
				existenceVoeuxEnseignant = false;
			}
			existenceVoeuxEnseignantDejaRecherche = true;
		}

		return existenceVoeuxEnseignant;
	}
	/**
	 * Renvoie vrai si la personne connectée est un enseignant statutaire
	 * @return vrai/faux
	 */
	public boolean getAffichageFicheVoeux() {
		return isEnseignantStatutaire();
	}

	/**
	 * Renvoie vrai si la personne connectée a le droit de voir le menu Fiches de voeux
	 * 	==> statutaire ou répartiteur uniquement
	 * @return vrai/faux
	 */
	public boolean getAffichageMenuFicheVoeux() {
		return EOPecheParametre.isFicheVoeuxUtilisation(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()) && (getAffichageFicheVoeux() || getAutorisation().hasDroitShowListeFichesVoeux());
	}
	
	/**
	 * Est-ce qu'on autorise l'affichage des tableaux de bord?
	 * 	=> faux uniquement pour le profil enseignant
	 * @return vrai/faux
	 */
	public boolean isAffichageTableauBordAutorise() {
		return (getFonctionRepartiteur() 
				|| getFonctionDirecteur()
				|| getFonctionDrh() 
				|| getFonctionPresident()); 
	}

	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonEnseignantsStatutaires() {
		return (getFonctionEnseignant() && isEnseignantStatutaire()) || getFonctionRepartiteur() || getFonctionDrh() || getFonctionPresident() || getFonctionDirecteur();
	}
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonEnseignantsVacataires() {
		return (getFonctionEnseignant() && isEnseignantVacataire()) || getFonctionRepartiteur() || getFonctionDrh() || getFonctionPresident() || getFonctionDirecteur();
	}
	
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonEnseignantsVacatairesPrevisionnel() {
		return affichageBoutonEnseignantsVacataires() && EOPecheParametre.isFicheVacatairePrevisionnelUtilisation(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonEnseignantsValidation() {
		return getFonctionRepartiteur() || getFonctionDirecteurComposante() || getFonctionDirecteurDepartement();
	}
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonsMesValidations() {
		return getFonctionRepartiteur() || getFonctionDirecteur() || getFonctionDrh() || getFonctionPresident();
	}
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonMaFicheServiceVacataire() {
		return getAutorisation().hasDroitShowMenuGroupEnseignantFicheService() && isEnseignantVacataire();
	}
	
	/**
	 * 
	 * @return vrai/faux
	 */
	public boolean affichageBoutonMaFicheServiceStatutaire() {
		return getAutorisation().hasDroitShowMenuGroupEnseignantFicheService() && isEnseignantStatutaire();
	}
	
	
	
	// * ******************************* *
	// * ACTIONS SUR LES BOUTONS DU MENU *
	// * ******************************* *
	/**
	 * Action générique.
	 * <p>
	 * Permet d'ajouter des comportements globaux lors de l'action sur un point d'entrée de menu.
	 * <p>
	 * Actuellement, on reset l'editingContext par défaut.
	 * 
	 * @param pecheComponentClass Une page Peche (un WOComponent)
	 * @return Vers la page
	 */
	public WOActionResults action(Class<? extends CktlPecheCommun> pecheComponentClass) {
		resetDefault();		
		return pageWithName(pecheComponentClass.getName());
	}
	
	/**
	 * Action générique.
	 * <p>
	 * Permet d'ajouter des comportements globaux lors de l'action sur un point d'entrée de menu.
	 * <p>
	 * Actuellement, on reset l'editingContext par défaut.
 	 */
	private void resetDefault() {
		getPecheSession().resetDefaultEditingContext();
		getPecheSession().removeObjectForKey(Session.BOUTONRETOUR);
	}
	
	
	/**
	 * @return Vers la liste des enseignants statutaires fiches définitives
	 */
	public WOActionResults actionCktlPecheEnseignantsStatutairesDefinitif() {
		CktlPecheEnseignantsStatutaires page = (CktlPecheEnseignantsStatutaires) pageWithName(CktlPecheEnseignantsStatutaires.class.getName());
		page.setTypePopulation(Constante.DEFINITIF);
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la liste des enseignants statutaires fiches prévisionnelles
	 */
	public WOActionResults actionCktlPecheEnseignantsStatutairesPrevisionnel() {
		CktlPecheEnseignantsStatutaires page = (CktlPecheEnseignantsStatutaires) pageWithName(CktlPecheEnseignantsStatutaires.class.getName());
		page.setTypePopulation(Constante.PREVISIONNEL);
		resetDefault();
		return page;
	}

	
	/**
	 * @return Vers la liste des enseignants statutaires fiches définitives
	 */
	public WOActionResults actionCktlPecheMesValidationsStatDefinitif() {
		CktlPecheEnseignantsStatutaires page = (CktlPecheEnseignantsStatutaires) pageWithName(CktlPecheEnseignantsStatutaires.class.getName());
		page.setTypePopulation(Constante.DEFINITIF);
		page.setMesValidations(true);
		resetDefault();
		return page;
	}
	
	/**
	 * 
	 * @return Vers la liste de mes validations sur les enseignants statutaires fiches prévisionnelles
	 */
	public WOActionResults actionCktlPecheMesValidationsStatPrevisionnel() {
		CktlPecheEnseignantsStatutaires page = (CktlPecheEnseignantsStatutaires) pageWithName(CktlPecheEnseignantsStatutaires.class.getName());
		page.setTypePopulation(Constante.PREVISIONNEL);
		page.setMesValidations(true);
		resetDefault();
		return page;
	}
	
	
	/**
	 * @return Vers la liste des enseignants vacataires fiches définitives
	 */
	public WOActionResults actionCktlPecheEnseignantsVacatairesDefinitif() {
		CktlPecheEnseignantsVacataires page = (CktlPecheEnseignantsVacataires) pageWithName(CktlPecheEnseignantsVacataires.class.getName());
		page.setTypePopulation(Constante.DEFINITIF);
		resetDefault();
		return page;
	}

	/**
	 * @return Vers la liste des enseignants vacataires fiches prévisionnelles
	 */
	public WOActionResults actionCktlPecheEnseignantsVacatairesPrevisionnel() {
		CktlPecheEnseignantsVacataires page = (CktlPecheEnseignantsVacataires) pageWithName(CktlPecheEnseignantsVacataires.class.getName());
		page.setTypePopulation(Constante.PREVISIONNEL);
		resetDefault();
		return page;
	}
	
	
	/**
	 * @return Vers la liste de mes validations vacataires fiches définitives
	 */
	public WOActionResults actionCktlPecheMesValidationsVacDefinitif() {
		CktlPecheEnseignantsVacataires page = (CktlPecheEnseignantsVacataires) pageWithName(CktlPecheEnseignantsVacataires.class.getName());
		page.setTypePopulation(Constante.DEFINITIF);
		page.setMesValidations(true);
		resetDefault();
		return page;
	}

	/**
	 * @return Vers la liste de mes validations vacataires fiches prévisionnelles
	 */
	public WOActionResults actionCktlPecheMesValidationsVacPrevisionnel() {
		CktlPecheEnseignantsVacataires page = (CktlPecheEnseignantsVacataires) pageWithName(CktlPecheEnseignantsVacataires.class.getName());
		page.setTypePopulation(Constante.PREVISIONNEL);
		page.setMesValidations(true);
		resetDefault();
		return page;
	}
	
	

	/**
	 * @return Vers la validation automatique des fiches définitives
	 */
	public WOActionResults actionCktlPecheValidationAutoDefinitif() {
		CktlPecheValidationFicheService page = (CktlPecheValidationFicheService) pageWithName(CktlPecheValidationFicheService.class.getName());
		page.setTypeFiche(Constante.DEFINITIF);
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la validation automatique des fiches prévisionnelles
	 */
	public WOActionResults actionCktlPecheValidationAutoPrevisionnel() {
		CktlPecheValidationFicheService page = (CktlPecheValidationFicheService) pageWithName(CktlPecheValidationFicheService.class.getName());
		page.setTypeFiche(Constante.PREVISIONNEL);
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la liste des enseignants génériques (onglet stat)
	 */
	public WOActionResults actionCktlPecheEnseignantsGeneriquesStat() {
		CktlPecheEnseignantsGeneriques page = (CktlPecheEnseignantsGeneriques) pageWithName(CktlPecheEnseignantsGeneriques.class.getName());
		page.setSelectedItemId(PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId());
		page.setTypeStatutaire(true);
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la liste des enseignants génériques (onglet Vac)
	 */
	public WOActionResults actionCktlPecheEnseignantsGeneriquesVac() {
		CktlPecheEnseignantsGeneriques page = (CktlPecheEnseignantsGeneriques) pageWithName(CktlPecheEnseignantsGeneriques.class.getName());
		page.setSelectedItemId(PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId());
		page.setTypeStatutaire(false);
		resetDefault();
		return page;
	}
	
	

	/**
	 * @return Vers la liste des enseignants (répartition croisée)
	 */
	public WOActionResults actionListeEnseignants() {
		return action(ListeEnseignants.class);
	}

	/**
	 * @return Vers la liste des enseignements (répartition croisée)
	 */
	public WOActionResults actionListeEnseignement() {
		return action(ListeEnseignement.class);
	}

	/**
	 * @return Vers la fiche de service de l'enseignant connecté
	 */
	public WOActionResults actionFicheEnseignantComponent() {
		return action(FicheEnseignantComponent.class);
	}
	
	/**
	 * @return Vers la fiche de service de l'enseignant connecté
	 */
	public WOActionResults actionFicheEnseignantComponentStat() {
		FicheEnseignantComponent page = (FicheEnseignantComponent) pageWithName(FicheEnseignantComponent.class.getName());
		page.setSelectedItemId(getMenuItemMaFicheServiceStat());
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la fiche de service de l'enseignant connecté
	 */
	public WOActionResults actionFicheEnseignantComponentVac() {
		FicheEnseignantComponent page = (FicheEnseignantComponent) pageWithName(FicheEnseignantComponent.class.getName());
		page.setSelectedItemId(getMenuItemMaFicheServiceVac());
		resetDefault();
		return page;
	}
	

	/**
	 * @return Vers l'offre de formation
	 */
	
	public WOActionResults actionOffreFormation() {
		OffreFormation page = (OffreFormation) pageWithName(OffreFormation.class.getName());
		page.setSelectedMenuItem(PecheMenuItem.OFFRE_FORMATION);
		page.setTypeFiche(null);
		page.setModeConsultation(true);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionOffreFormationPrev() {
		OffreFormation page = (OffreFormation) pageWithName(OffreFormation.class.getName());
		page.setSelectedMenuItem(PecheMenuItem.OFFRE_FORMATION_PREV);
		page.setTypeFiche(Constante.PREVISIONNEL);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionOffreFormationDef() {
		OffreFormation page = (OffreFormation) pageWithName(OffreFormation.class.getName());
		page.setSelectedMenuItem(PecheMenuItem.OFFRE_FORMATION_DEF);
		page.setTypeFiche(Constante.DEFINITIF);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}

	/**
	 * @return Vers l'écran UE/EC
	 */
	
	public WOActionResults actionUeEc() {
		RechercheParUEEC page = (RechercheParUEEC) pageWithName(RechercheParUEEC.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_EC.getId());
		page.setTypeFiche(null);
		page.setModeConsultation(true);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionUeEcPrev() {
		RechercheParUEEC page = (RechercheParUEEC) pageWithName(RechercheParUEEC.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_EC_PREV.getId());
		page.setTypeFiche(Constante.PREVISIONNEL);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionUeEcDef() {
		RechercheParUEEC page = (RechercheParUEEC) pageWithName(RechercheParUEEC.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_EC_DEF.getId());
		page.setTypeFiche(Constante.DEFINITIF);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	
	/**
	 * @return Vers les UE fottantes
	 */
	public WOActionResults actionCktlPecheTableUEFlottante() {
		CktlPecheTableUEFlottante page = (CktlPecheTableUEFlottante) pageWithName(CktlPecheTableUEFlottante.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_FLOTTANTES.getId());
		page.setTypeFiche(null);
		page.setModeConsultation(true);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionCktlPecheTableUEFlottantePrev() {
		CktlPecheTableUEFlottante page = (CktlPecheTableUEFlottante) pageWithName(CktlPecheTableUEFlottante.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_FLOTTANTES_PREV.getId());
		page.setTypeFiche(Constante.PREVISIONNEL);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionCktlPecheTableUEFlottanteDef() {
		CktlPecheTableUEFlottante page = (CktlPecheTableUEFlottante) pageWithName(CktlPecheTableUEFlottante.class.getName());
		page.setSelectedItemId(PecheMenuItem.UE_FLOTTANTES_DEF.getId());
		page.setTypeFiche(Constante.DEFINITIF);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	

	public WOActionResults actionRechercheValidationParUeEcPrev() {
		RechercheValidationParUE page = (RechercheValidationParUE) pageWithName(RechercheValidationParUE.class.getName());
		page.setSelectedItemId(PecheMenuItem.VALIDATION_PAR_UE_EC_PREV.getId());
		page.setTypeFiche(Constante.PREVISIONNEL);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	public WOActionResults actionRechercheValidationParUeEcDef() {
		RechercheValidationParUE page = (RechercheValidationParUE) pageWithName(RechercheValidationParUE.class.getName());
		page.setSelectedItemId(PecheMenuItem.VALIDATION_PAR_UE_EC_DEF.getId());
		page.setTypeFiche(Constante.DEFINITIF);
		page.setModeConsultation(false);
		resetDefault();
		return page;
	}
	
	/**
	 * @return Vers la définition des circuits de validation
	 */
	public WOActionResults actionDefinitionCircuitsValidation() {
		return action(CircuitsValidationPeche.class);				
	}

	/**
	 * @return Vers la définition des circuits de validation
	 */
	public WOActionResults actionApplicationCircuitsValidation() {
		return action(ApplicationCircuitsValidation.class);				
	}
	
	/**
	 * @return Vers la définition des REH
	 */
	public WOActionResults actionRehGestion() {
		return action(RehGestion.class);
	}


	/**
	 * @return Vers la définition des Taux Horaires
	 */
	public WOActionResults actionTauxHorairesGestion() {
		return action(TauxHorGestion.class);
	}
	
	/**
	 * @return Vers la définition des Taux Horaires
	 */
	public WOActionResults actionFluxPaiementGestion() {
		return action(FluxPaieGestion.class);
	}
	
	/**
	 * @return Vers la définition des Taux Horaires
	 */
	public WOActionResults actionUefChoixComposantes() {
		return action(ChoixComposantes.class);
	}
	
	/**
	 * @return Vers la définition des Taux Horaires
	 */
	public WOActionResults actionDuplicationEnseignantsStatutaires() {
		return action(Duplication.class);
	}

	/**
	 * @return Vers la définition des Taux Horaires
	 */
	public WOActionResults actionGestionDroitsGestion() {
		return action(TableDroits.class);
	}
	

	/**
	 * @return Vers la définition des types d'AP
	 */
	public WOActionResults actionTypesAPGestion() {
		return action(TypesAPGestion.class);
	}

	/**
	 * @return Vers la définition des coefficients et du calcul des HComp
	 */
	public WOActionResults actionCktlPecheParamPopHETD() {
		return action(CktlPecheParamPopHETD.class);
	}

	/**
	 * @return Vers la fiche de voeux de l'enseignant connecté
	 */
	public WOActionResults actionFicheVoeuxEnseignant() {
		EOActuelEnseignant enseignant = getPecheSession().getApplicationUser().getEnseignant();
		FicheVoeuxEnseignant page = (FicheVoeuxEnseignant) pageWithName(FicheVoeuxEnseignant.class.getName());
		page.setEditedObject(enseignant);
		resetDefault();
		return page;
		
	}

	/**
	 * @return Vers l'arbitrage des fiches de voeux
	 */
	public WOActionResults actionFicheVoeuxArbitrage() {
		return action(FicheVoeuxArbitrage.class);
	}
	
	/**
	 * @return Vers la liste des fiches de voeux
	 */
	public WOActionResults actionListeFicheVoeux() {
		return action(ListeFichesVoeux.class);
	}

	/**
	 * @return Vers la liste des mises en paiement
	 */

	public WOActionResults actionListeMEP() {
		return action(ListeMEP.class);
	}
	
	/**
	 * @return Vers la recherche des suivis des validations
	 */
	public WOActionResults actionRechercheSuivisValidations() {
		return action(RechercheTableauDeBord.class);
	}
	
	public String getMenuItemIdStatutairesDefinitif() {
		return PecheMenuItem.ENSEIGNANTS_STATUTAIRES_DEFINITIF.getId();
	}
	
	public String getMenuItemIdStatutairesPrevisionnel() {
		return PecheMenuItem.ENSEIGNANTS_STATUTAIRES_PREVISIONNEL.getId();
	}
	
	public String getMenuItemIdVacatairesDefinitif() {
		return PecheMenuItem.ENSEIGNANTS_VACATAIRES_DEFINITIF.getId();
	}
	
	public String getMenuItemIdVacatairesPrevisionnel() {
		return PecheMenuItem.ENSEIGNANTS_VACATAIRES_PREVISIONNEL.getId();
	}
	
	public String getMenuItemIdDuplicationStatutaires() {
		return PecheMenuItem.PARAM_DUPLICATION_STATUTAIRES.getId();
	}
	
	public String getMenuItemIdValidationParUeEcPrev() {
		return PecheMenuItem.VALIDATION_PAR_UE_EC_PREV.getId();
	}
	
	public String getMenuItemIdValidationParUeEcDef() {
		return PecheMenuItem.VALIDATION_PAR_UE_EC_DEF.getId();
	}

	public String getMenuItemIdValidationAutoDefinitif() {
		return PecheMenuItem.ENSEIGNANTS_VALIDATION_DEFINITIF.getId();
	}

	public String getMenuItemIdValidationAutoPrevisionnel() {
		return PecheMenuItem.ENSEIGNANTS_VALIDATION_PREVISIONNEL.getId();
	}

	public String getMenuItemIdMesValidationsStatPrevisionnel() {
		return PecheMenuItem.MES_VALIDATIONS_STATUTAIRES_PREVISIONNEL.getId();
	}
	
	public String getMenuItemIdMesValidationsStatDefinitif() {
		return PecheMenuItem.MES_VALIDATIONS_STATUTAIRES_DEFINITIF.getId();
	}

	public String getMenuItemIdMesValidationsVacPrevisionnel() {
		return PecheMenuItem.MES_VALIDATIONS_VACATAIRES_PREVISIONNEL.getId();
	}
	
	public String getMenuItemIdMesValidationsVacDefinitif() {
		return PecheMenuItem.MES_VALIDATIONS_VACATAIRES_DEFINITIF.getId();
	}

	public String getMenuItemPaiement() {
		return PecheMenuItem.MISE_EN_PAIEMENT.getId();
	}

	public String getMenuItemDefinitionCircuitsValidation() {
		return PecheMenuItem.PARAM_CIRCUIT_VALIDATION.getId();
	}
	
	public String getMenuItemApplicationCircuitsValidation() {
		return PecheMenuItem.PARAM_APPLI_CIRCUIT_VALIDATION.getId();
	}

	public String getMenuItemActivitesreh() {
		return PecheMenuItem.PARAM_REH.getId();
	}

	public String getMenuItemTauxHoraires() {
		return PecheMenuItem.PARAM_TAUX_HORAIRE.getId();
	}

	public String getMenuItemFluxPaiement() {
		return PecheMenuItem.PARAM_FLUX_PAIEMENT.getId();
	}

	public String getMenuItemUeFlottantesChoixComposantes() {
		return PecheMenuItem.CHOIX_COMPOSANTES.getId();
	}
	
	public String getMenuItemRepartitionCroiseeEnseignements() {
		return PecheMenuItem.MES_COURS.getId();
	}
	
	public String getMenuItemRepartitionCroiseeEnseignants() {
		return PecheMenuItem.MES_ENSEIGNANTS.getId();
	}
	
	public String getMenuItemGeneriqueStat() {
		return PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId();
	}
	
	public String getMenuItemGeneriqueVac() {
		return PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId();
	}
	
	public String getMenuItemFicheVoeux() {
		return PecheMenuItem.FICHE_VOEUX.getId();
	}
	
	public String getMenuItemMaFicheVoeux() {
		return PecheMenuItem.MA_FICHE_VOEUX.getId();
	}
	
	public String getMenuItemFicheVoeuxArbitrage() {
		return PecheMenuItem.FICHE_VOEUX_ARBITRAGE.getId();
	}
	
	public String getMenuItemMaFicheServiceStat() {
		return PecheMenuItem.MA_FICHE_SERVICE_STAT.getId();
	}
	public String getMenuItemMaFicheServiceVac() {
		return PecheMenuItem.MA_FICHE_SERVICE_VAC.getId();
	}
	
	public String getMenuItemOffreFormation() {
		return PecheMenuItem.OFFRE_FORMATION.getId();
	}

	public String getMenuItemOffreFormationPrev() {
		return PecheMenuItem.OFFRE_FORMATION_PREV.getId();
	}
	
	public String getMenuItemOffreFormationDef() {
		return PecheMenuItem.OFFRE_FORMATION_DEF.getId();
	}
	
	public String getMenuItemUeEc() {
		return PecheMenuItem.UE_EC.getId();
	}

	public String getMenuItemUeEcPrev() {
		return PecheMenuItem.UE_EC_PREV.getId();
	}
	
	public String getMenuItemUeEcDef() {
		return PecheMenuItem.UE_EC_DEF.getId();
	}
	
	public String getMenuItemUeFlottantes() {
		return PecheMenuItem.UE_FLOTTANTES.getId();
	}

	public String getMenuItemUeFlottantesPrev() {
		return PecheMenuItem.UE_FLOTTANTES_PREV.getId();
	}
	
	public String getMenuItemUeFlottantesDef() {
		return PecheMenuItem.UE_FLOTTANTES_DEF.getId();
	}
	
	public String getMenuItemParamTypesHoraires() {
		return PecheMenuItem.PARAM_TYPE_AP.getId();
	}
	
	public String getMenuItemParamHetd() {
		return PecheMenuItem.PARAM_HETD.getId();
	}
	
	public String getMenuItemSuiviValidation() {
		return PecheMenuItem.TABLEAU_DE_BORD.getId();
	}
	
	public String getMenuItemGestionDroits() {
		return PecheMenuItem.PARAM_GESTION_DROITS.getId();
	}
	
	public String getMenuPanelTabParam() {
		return PecheMenuTab.PARAMETRAGE.getId();
	}

	public String getMenuPanelTabPaiement() {
		return PecheMenuTab.MISE_EN_PAIEMENT.getId();
	}

	public String getMenuPanelTabEnseignements() {
		return PecheMenuTab.ENSEIGNEMENTS.getId();
	}

	public String getMenuPanelTabEnseignantsStatutaires() {
		return PecheMenuTab.ENSEIGNANTS_STATUTAIRES.getId();
	}
	
	public String getMenuPanelTabEnseignantsVacataires() {
		return PecheMenuTab.ENSEIGNANTS_VACATAIRES.getId();
	}
	
	public String getMenuPanelTabSuivi() {
		return PecheMenuTab.SUIVI.getId();
	}
	
	public boolean notExistenceStructuresEnseignants() {
		return !(getAutorisation().hasListeCibleEnseignantsStatutaires() || getAutorisation().hasListeCibleEnseignantsVacataire());
	}
	
	public boolean notExistenceStructuresEnseignantsStatutaires() {
		return !getAutorisation().hasListeCibleEnseignantsStatutaires();
	}

	/**
	 * Puis-je voir le menu "Enseignants vacataires" ?
	 * @return true/false
	 */
	public boolean hasDroitShowMenuEnseignantsVacataires() {
		return getAutorisation().hasDroitShowMenuEnseignantsVacataires(isEnseignantVacataire());
	}
	
	/**
	 * Puis-je voir le menu "Enseignants statutaires" ?
	 * @return true/false
	 */
	public boolean hasDroitShowMenuEnseignantsStatutaires() {
		return getAutorisation().hasDroitShowMenuEnseignantsStatutaires(isEnseignantStatutaire(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}

	public boolean isEnseignantStatutaire() {
		if (!isEnseignantStatutaireDejaRecherche) {
			isEnseignantStatutaire = getPecheSession().getApplicationUser().isEnseignantStatutaire();
			isEnseignantStatutaireDejaRecherche = true;	
		}
		return isEnseignantStatutaire;
	}

	public void setEnseignantStatutaire(boolean isEnseignantStatutaire) {
		this.isEnseignantStatutaire = isEnseignantStatutaire;
	}

	public boolean isEnseignantVacataire() {
		if (!isEnseignantVacataireDejaRecherche) {
			isEnseignantVacataire = getPecheSession().getApplicationUser().isEnseignantVacataire();
			isEnseignantVacataireDejaRecherche = true;	
		}
		return isEnseignantVacataire;
	}

	public void setEnseignantVacataire(boolean isEnseignantVacataire) {
		this.isEnseignantVacataire = isEnseignantVacataire;
	}
		
}