package org.cocktail.peche.components.commun;

import java.util.HashMap;
import java.util.Map;

/**
 * Controleur de la classe CktlPecheBaseComponent
 *
 * @author Chama LAATIK
 *
 */
public class CktlPecheBaseComponentCtrl {

	public final static Map<String, String> etatTemoin = new HashMap<String, String>();
	static {
		etatTemoin.put(Constante.ACCEPTE, Constante.ETAT_ACCEPTE);
		etatTemoin.put(Constante.VALIDE, Constante.ETAT_VALIDE);
		etatTemoin.put(Constante.REFUSE, Constante.ETAT_REFUSE);
		etatTemoin.put(Constante.ATTENTE, Constante.ETAT_ATTENTE);
	}

	/**
	 * Controleur
	 *
	 * 
	 */
	public CktlPecheBaseComponentCtrl() {
		
	}

	/**
	 * permet de récupérer l'état de la répartition s'il y en a une.
	 *
	 * @param temoin
	 *            : témoin de la répartition croisée
	 * @return le témoin de la répartition
	 */
	public String getEtatRepartition(String temoin) {
		if (!etatTemoin.containsKey(temoin)) {
			return "";
		}

		return etatTemoin.get(temoin);
	}
}
