package org.cocktail.peche.components.commun;

/**
 * Modes d'édition
 * @author Equipe PECHE.
 *
 */
public enum ModeEdition {
	UNKNOWN, CREATION, MODIFICATION, CONSULTATION;
}
