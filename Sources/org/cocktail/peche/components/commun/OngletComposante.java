package org.cocktail.peche.components.commun;

/**
 * 
 * @author juliencallewaert
 *
 */
public class OngletComposante {

	private String code;
	private String libelle;
	
	/**
	 * Constructeur
	 * @param code : code de l'onglet
	 * @param libelle  : libellé de l'onglet
	 */
	public OngletComposante(String code, String libelle) {
		super();
		this.code = code;
		this.libelle = libelle;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return this.libelle;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj.getClass().equals(OngletComposante.class)) {
			return ((OngletComposante) obj).getCode().equals(this.code);		
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
}
