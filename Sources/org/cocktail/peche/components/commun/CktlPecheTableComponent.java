package org.cocktail.peche.components.commun;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.batching.ERXBatchingDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXKey;

/**
 * Classe de base pour les composants "nouvelle mode" de type "table".
 * 
 * @author Yannick Mauray
 * 
 * @param <T>
 */
public abstract class CktlPecheTableComponent<T> extends CktlPecheBaseComponent {

	private static final long serialVersionUID = -7365984375919542377L;

	private static final int NUMBER_OF_OBJECTS_PER_BATCH = 20;

	private ERXDisplayGroup<T> displayGroup;
	private ERXBatchingDisplayGroup<T> displayBatchingGroup;
	private T currentItem;
	private T selectedObject;

	public Session getPecheSession() {
		return (Session) super.session();
	}

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution.
	 */
	public CktlPecheTableComponent(WOContext context) {
		super(context);

		this.displayGroup = new ERXDisplayGroup<T>();
		this.displayGroup.setDelegate(this);
		this.displayGroup
				.setNumberOfObjectsPerBatch(getNumberOfObjectsPerBatch());
		this.displayGroup.setSelectsFirstObjectAfterFetch(false);
		
		this.displayBatchingGroup = new ERXBatchingDisplayGroup<T>();
		this.displayBatchingGroup.setDelegate(this);
		this.displayBatchingGroup
				.setNumberOfObjectsPerBatch(getNumberOfObjectsPerBatch());
		this.displayBatchingGroup.setSelectsFirstObjectAfterFetch(false);
		
		
	}

	/**
	 * Méthode de délégation du "displayGroup".
	 * 
	 * @param group
	 *            le "displayGroup" de la table.
	 */
	@SuppressWarnings("unchecked")
	public final void displayGroupDidChangeSelectedObjects(
			final WODisplayGroup group) {
		if (group.selectedObject() != null) {
			this.setSelectedObject((T) group.selectedObject());
		}
	}

	/**
	 * @return le contexte d'édition par défaut de la session.
	 */
	public EOEditingContext edc() {
		EOEditingContext edc = session().defaultEditingContext();
		return edc;
	}

	public int getNumberOfObjectsPerBatch() {
		int numberObjects = ((CktlWebApplication) WOApplication.application()).config().intForKey("org.cocktail.peche.number.of.objects.per.batch");
		if (numberObjects <= 0) {
			return NUMBER_OF_OBJECTS_PER_BATCH;			
		} else {
			return numberObjects;
		}
	}

	protected void filtrerDisplayGroup(ERXKey<Object> key, String token) {
		if (!StringCtrl.isEmpty(token)) {
			filtrerDisplayGroup(key.contains(token));
		} else {
			filtrerDisplayGroup(null);
		}
	}

	protected void filtrerDisplayGroup(EOQualifier qualifier) {
		getDisplayGroup().setQualifier(qualifier);
		getDisplayGroup().updateDisplayedObjects();
	}

	
	protected void filtrerBatchingDisplayGroup(ERXKey<Object> key, String token) {
		if (!StringCtrl.isEmpty(token)) {
			filtrerBatchingDisplayGroup(key.contains(token));
		} else {
			filtrerBatchingDisplayGroup(null);
		}
	}

	protected void filtrerBatchingDisplayGroup(EOQualifier qualifier) {
		getBatchingDisplayGroup().setQualifier(qualifier);
		getBatchingDisplayGroup().updateDisplayedObjects();
	}
	
	
	public String getTableViewContainerId() {
		return getId("tableViewContainer");
	}

	public String getTableViewId() {
		return getContainerId() + "_tableview";
	}

	public String getButtonsContainerId() {
		return getContainerId() + "_buttons";
	}

	public ERXDisplayGroup<T> getDisplayGroup() {
		return displayGroup;
	}

	public void setDisplayGroup(ERXDisplayGroup<T> displayGroup) {
		this.displayGroup = displayGroup;
	}

	public ERXBatchingDisplayGroup<T> getBatchingDisplayGroup() {
		return displayBatchingGroup;
	}

	public void setBatchingDisplayGroup(ERXBatchingDisplayGroup<T> displayBatchingGroup) {
		this.displayBatchingGroup = displayBatchingGroup;
	}
	
	public T getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(T currentItem) {
		this.currentItem = currentItem;
	}

	public T getSelectedObject() {
		return selectedObject;
	}

	public void setSelectedObject(T selectedObject) {
		this.selectedObject = selectedObject;
	}
	
	/**
	 * permet de récupérer l'état de la répartition s'il y en a une.
	 * 
	 * @param temoin
	 *            : témoin de la répartition 
	 * @return le témoin de la 
	 */
	public String getEtatRepartition(String temoin) {
		return getComponentCtrl().getEtatRepartition(temoin);
	}

	/**
	 * Message de confirmation à la suppression d'un élément.
	 * 
	 * @return message
	 */
	public String onClickBefore() {
		String msgConfirm = message(Messages.SERVICE_ALERTE_CONFIRM_SUPPRESS);
		return "confirm('" + msgConfirm + "')";
	}

	/**
	 * Cette méthode permet de créer un displayGroup à partir du qualifier
	 * 
	 * @param qualifier
	 *            le qualifier à utiliser.
	 * @param entityName
	 *            le nom de l'entité (objet persistant) à lire en base.
	 */
	public void fetchDisplay(EOQualifier qualifier, String entityName) {
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), entityName);
		dataSource.setAuxiliaryQualifier(qualifier);

		ERXDisplayGroup<T> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.fetch();
	}
	
	/**
	 * Cette méthode permet de créer un displayGroup à partir du qualifier en le triant
	 * 
	 * @param qualifier
	 *            le qualifier à utiliser.
	 * @param entityName
	 *            le nom de l'entité (objet persistant) à lire en base.
	 * @param sortOrdering
	 * 			  tri à utiliser (attention tri en mémoire). Voir {@link #fetchDisplay(EOFetchSpecification)} pour un tri par SQL.
	 */
	public void fetchDisplay(EOQualifier qualifier, String entityName, NSArray<EOSortOrdering> sortOrdering) {
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), entityName);
		dataSource.setAuxiliaryQualifier(qualifier);

		ERXDisplayGroup<T> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.setSortOrderings(sortOrdering);
		dg.fetch();
	}

	/**
	 * Cette méthode permet de créer un displayGroup à partir du qualifier en le triant
	 * 
	 * @param qualifier
	 *            le qualifier à utiliser.
	 * @param entityName
	 *            le nom de l'entité (objet persistant) à lire en base.
	 * @param sortOrdering
	 * 			  tri à utiliser (attention tri en mémoire). Voir {@link #fetchDisplay(EOFetchSpecification)} pour un tri par SQL.
	 */
	public void fetchBatchingDisplay(EOQualifier qualifier, String entityName, NSArray<EOSortOrdering> sortOrdering) {
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), entityName);
		dataSource.setAuxiliaryQualifier(qualifier);

		ERXBatchingDisplayGroup<T> dg = this.getBatchingDisplayGroup();
		dg.setDataSource(dataSource);
		dg.setSortOrderings(sortOrdering);
		dg.fetch();
	}
	
	
	/**
	 * Cette méthode permet de créer un displayGroup à partir d'une fetchSpec.
	 * @param fetchSpec La fetch specification
	 */
	public void fetchDisplay(EOFetchSpecification fetchSpec) {
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), fetchSpec.entityName());
		dataSource.setFetchSpecification(fetchSpec);

		ERXDisplayGroup<T> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.fetch();
	}
	
	/**
	 * Vérifie que l'enseignant est affecté à une composante scolarité ou un département d'enseignement
	 * @param enseignant : l'enseignant
	 * @return Vrai/faux
	 */
	public boolean checkEnseignantAffecte(EnseignantsStatutairesCtrl controleur, EOActuelEnseignant enseignant) {
		if (controleur.getComposante(enseignant) == null && controleur.getDepartementEnseignement(enseignant.toIndividu()) == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.ENSEIGNANT_ERREUR_NON_AFFECTE);
			return true;
		}
		
		return false;
	}
}
