package org.cocktail.peche.components.commun;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.PecheAutorisationsCache;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.CktlAjaxUtils;

/**
 * Classe de base pour tous les composants de PECHE "nouvelle norme".
 *
 * @author Yannick Mauray
 * @author Chama Laatik
 *
 */
public abstract class CktlPecheBaseComponent extends CktlPecheCommun {

	private static final long serialVersionUID = -2452271739940387077L;
	private CktlPecheBaseComponentCtrl componentCtrl;
	private OngletComposante ongletComposanteSelectionne;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            les contexte d'exécution.
	 */
	public CktlPecheBaseComponent(WOContext context) {
		super(context);
		this.componentCtrl = new CktlPecheBaseComponentCtrl();
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		String stylesheet = getStyleSheet();
		if (stylesheet != null) {
			CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app", stylesheet);
		}
	}

	protected String getStyleSheet() {
		return null;
	}

    /**
     *
     * @param key la clé du message à traduire.
     * @return le message traduit correspondant à la clé.
     */
    public String message(String key) {
    	return getPecheSession().localizer().localizedStringForKey(key);
    }

    /**
	 * @param suffix le suffix à ajouter à l'identifiant.
	 * @return un identifiant composé de l'identifiant du composant + "_" + le suffix.
	 */
	public String getId(String suffix) {
		return getComponentId() + "_" + suffix;
	}

	public String getContainerId() {
		return getId("container");
	}

	/**
	 * rend actif/inactif le bouton de validation du répartiteur
	 * @param demande : la demande
	 * @return vrai/faux
	 */
	public boolean getBoutonActif(EODemande demande) {		
		if (getAutorisation() != null && getAutorisation().hasDroitShowChemin(demande, TypeCheminPeche.VALIDER)) {
			return getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER);
		}

		return false;
	}

	/**
	 * Rend actif/inactif le bouton de validation pour les autres profils
	 * @param demande : la demande
	 * @return vrai/faux
	 */
	public boolean getBoutonInactifValidation(EODemande demande) {
		if (getAutorisation() != null && getAutorisation().hasDroitShowChemin(demande, TypeCheminPeche.VALIDER)) {
			return !getAutorisation().hasDroitUtilisationChemin(demande, TypeCheminPeche.VALIDER);
		}

		return true;
	}

	/**
	 * Rend actif/inactif le bouton de rejet pour les autres profils
	 * @param demande : la demande
	 * @return vrai/faux
	 */
	public boolean getBoutonInactifRejet(EODemande demande) {
		if (getAutorisation() != null && getAutorisation().hasDroitShowChemin(demande, TypeCheminPeche.REFUSER)) {
			return !getAutorisation().hasDroitUtilisationChemin(demande, TypeCheminPeche.REFUSER);
		}

		return true;
	}

	/**
	 * Boutons du service actifs si la demande est sur l'étape validation du répartiteur
	 * 
	 * @param demande : la demande
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifsRepartitionService(EODemande demande) {
		if (getAutorisation() != null) {
			return demande == null || !(getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER)
					|| isBouclageFicheServicePossible(demande));
		}
		return true;
	}

	/**
	 * renvoie si la personne connectée a la fonction de validation du répartiteur
	 * @return vrai/faux
	 */
	public boolean getFonctionRepartiteur() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParRepartiteur();		
	}
	
	
	/**
	 * renvoie si la personne connectée a la fonction de validation de directeur
	 * @return vrai/faux
	 */
	public boolean getFonctionDirecteur() {
		if (getAutorisation() != null) {
			return getAutorisation().hasDroitUtilisationValidationParDirecteurComposante()
					|| getAutorisation().hasDroitUtilisationValidationParDirecteurCollege()
					|| getAutorisation().hasDroitUtilisationValidationParDirecteurDepartement();
		}
		return false;
	}
	
	/**
	 * renvoie si la personne connectée a la fonction de validation du directeur de composante
	 * @return vrai/faux
	 */
	public boolean getFonctionDirecteurComposante() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParDirecteurComposante();
	}
	
	/**
	 * renvoie si la personne connectée a la fonction de validation du directeur de composante
	 * @return vrai/faux
	 */
	public boolean getFonctionDirecteurDepartement() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParDirecteurDepartement();
	}
	
	/**
	 * renvoie si la personne connectée a la fonction de validation du directeur de composante
	 * @return vrai/faux
	 */
	public boolean getFonctionDirecteurCollege() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParDirecteurCollege();
	}
	
	/**
	 * renvoie si la personne connectée a la fonction de validation du drh
	 * @return vrai/faux
	 */
	public boolean getFonctionDrh() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParDrh();
	}

	/**
	 * renvoie si la personne connectée a la fonction de validation du président
	 * @return vrai/faux
	 */
	public boolean getFonctionPresident() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParPresident();
	}
	
	/**
	 * renvoie si la personne connectée a la fonction de validation de l'enseignant
	 * @return vrai/faux
	 */
	public boolean getFonctionEnseignant() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParEnseignant();
	}

	/**
	 * renvoie true si la personne connectée a tous les droits de validation
	 * @return vrai/faux
	 */
	public boolean getFonctionToutesValidations() {
		return getAutorisation() != null && getAutorisation().getFonctionToutesValidations();
	}
	
	public CktlPecheBaseComponentCtrl getComponentCtrl() {
		return componentCtrl;
	}

	/**
	 * Est-ce que le bouclage de la fiche de service définitif est possible ?
	 * <p>
	 * Pour pouvoir boucler, la fiche doit être à l'étape finale du circuit définitif
	 * et l'utilisateur doit avoir le droit de validation d'un répartiteur.
	 * 
	 * @param demande une demande
	 * @return <code>true</code> si le bouclage est possible
	 */
	public boolean isBouclageFicheServicePossible(EODemande demande) {
		CircuitPeche circuit = CircuitPeche.getCircuit(demande.toEtape().toCircuitValidation());
		EtapePeche etape = EtapePeche.getEtape(demande.toEtape());
		
		if (circuit.getCodeCircuit().equals(CircuitPeche.PECHE_VACATAIRE.getCodeCircuit())) {
			if (circuit.isCircuitDefinitif()
					&& etape.equals(EtapePeche.VALIDEE)
					&& getFonctionRepartiteur()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Faire boucler la demande sur le circuit définitif.
	 * 
	 * @param demande une demande
	 */
	public void faireBouclerFicheServiceSiNecessaire(EODemande demande) {
		if (isBouclageFicheServicePossible(demande)) {
			CircuitPeche circuit = CircuitPeche.getCircuit(demande.toEtape().toCircuitValidation());
			demande.changerCircuit(Constante.APP_PECHE, circuit.getCodeCircuit(), getPecheSession().getApplicationUser().getPersId());
		}
	}
	
	public void setComponentCtrl(CktlPecheBaseComponentCtrl componentCtrl) {
		this.componentCtrl = componentCtrl;
	}


	/**
	 * Renvoie l'onglet de la page en cours sélectionné (ex. sur la page des vacataires qui contient des onglets)
	 * @return onglet en cours
	 */
	public OngletComposante getOngletComposanteSelectionne() {
		if (this.ongletComposanteSelectionne == null) {
			return getPecheSession().getOngletComposanteSelectionne();
		}
		return ongletComposanteSelectionne;
	}

	/**
	 * 
	 * @return false si onglet Tous sélectionné, true sinon
	 */
	public boolean estPasOngletTous() {
		return !Constante.ONGLET_COMPOSANTE_TOUS_CODE.equals(getOngletComposanteSelectionne().getCode());
	}
	
	/**
	 * @param ongletComposanteSelectionne onglet
	 */
	public void setOngletComposanteSelectionne(
			OngletComposante ongletComposanteSelectionne) {
		this.ongletComposanteSelectionne = ongletComposanteSelectionne;
		getPecheSession().setOngletComposanteSelectionne(ongletComposanteSelectionne);
	}
	
	
	/**
	 * Méthode à overridé - non mis en abstract car tous les components héritant de cette classe n'ont pas forcément de selectedItemId
	 * @return selected Item Id
	 */
	public String getSelectedItemId() {
		return null;
	}
	
	/**
	 * 
	 * @return selectedMenuTab
	 */
	public String getSelectedMenuTab() {
		if (getSelectedItemId() != null) {
			return PecheMenuItem.getPecheMenuTab(getSelectedItemId()).getId();
		} else {
			return StringUtils.EMPTY;
		}
	}
	
	public String libelleHistoriqueNmoinsUn() {		
		int annee = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
		if (getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) {
			return "Fiche " + (annee - 1);				
		} else {
			return "Fiche " + (annee - 1) + "/" + annee;
		}
	}

	public String libelleHistoriqueNmoinsDeux() {		
		int annee = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
		if (getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) {
			return "Fiche " + (annee - 2);				
		} else {
			return "Fiche " + (annee - 2) + "/" + (annee - 1);
		}
	}

}
