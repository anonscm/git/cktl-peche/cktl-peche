package org.cocktail.peche.components.commun;


/**
 * Liste les points d'entrées du menu.
 */
public enum PecheMenuItem {
	// Onglet "Enseignants statutaires" "Enseignants vacataires"
	/** Liste des enseignants statutaires. */
	ENSEIGNANTS_STATUTAIRES_DEFINITIF (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "statutairesdef"),
	/** Liste des enseignants vacataires. */
	ENSEIGNANTS_VACATAIRES_DEFINITIF (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "vacatairesdef"),
	/** Liste des enseignants statutaires. */
	ENSEIGNANTS_STATUTAIRES_PREVISIONNEL (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "statutairesprev"),
	/** Liste des enseignants vacataires. */
	ENSEIGNANTS_VACATAIRES_PREVISIONNEL (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "vacatairesprev"),
	/** Validation auto des fiches définitives. */
	ENSEIGNANTS_VALIDATION_DEFINITIF (PecheMenuTab.ENSEIGNEMENTS, "valautodef"),
	/** Validation auto des fiches prévisionnelles. */
	ENSEIGNANTS_VALIDATION_PREVISIONNEL (PecheMenuTab.ENSEIGNEMENTS, "valautoprev"),
	/** Mes validations statutaires fiches définitives. */
	MES_VALIDATIONS_STATUTAIRES_DEFINITIF (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "mesvalstatdef"),
	/** Mes validations statutaires fiches prévisionnelles. */
	MES_VALIDATIONS_STATUTAIRES_PREVISIONNEL (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "mesvalstatprev"),
	/** Mes validations vacataires fiches définitives. */
	MES_VALIDATIONS_VACATAIRES_DEFINITIF (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "mesvalvacdef"),
	/** Mes validations vacataires fiches prévisionnelles. */
	MES_VALIDATIONS_VACATAIRES_PREVISIONNEL (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "mesvalvacprev"),
	/** Liste des enseignants génériques. */
	ENSEIGNANTS_GENERIQUES_STAT (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "generiquestat"),
	ENSEIGNANTS_GENERIQUES_VAC (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "generiquevac"),
	/** Répartition croisée - Liste de mes enseignants. */
	MES_ENSEIGNANTS (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "repartitionCroiseeEnseignants"),
	/** Répartition croisée - Liste de mes cours. */
	MES_COURS (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "repartitionCroiseeEnseignement"),
	/** Fiche de vœux. */
	FICHE_VOEUX (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "listeFicheVoeux"),
	/** Ma fiche de vœux. */
	MA_FICHE_VOEUX (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "ficheVoeux"),
	/** Fiche de vœux. */
	FICHE_VOEUX_ARBITRAGE (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "arbitrageFicheVoeux"),
	/** Ma fiche de service. */
	MA_FICHE_SERVICE_STAT (PecheMenuTab.ENSEIGNANTS_STATUTAIRES, "ficheServiceStat"),
	/** Ma fiche de service. */
	MA_FICHE_SERVICE_VAC (PecheMenuTab.ENSEIGNANTS_VACATAIRES, "ficheServiceVac"),
	// Onglet "Enseignemants"
	/** Offre de formation. */
	OFFRE_FORMATION (PecheMenuTab.ENSEIGNEMENTS, "offreFormation"),
	OFFRE_FORMATION_PREV (PecheMenuTab.ENSEIGNEMENTS, "offreFormationPrev"),
	OFFRE_FORMATION_DEF (PecheMenuTab.ENSEIGNEMENTS, "offreFormationDef"),
	/** UE/EC. */
	UE_EC (PecheMenuTab.ENSEIGNEMENTS, "ueEc"),
	UE_EC_PREV (PecheMenuTab.ENSEIGNEMENTS, "ueEcPrev"),
	UE_EC_DEF (PecheMenuTab.ENSEIGNEMENTS, "ueEcDef"),
	/** UE flottantes. */
	UE_FLOTTANTES (PecheMenuTab.ENSEIGNEMENTS, "ueFlottantes"),
	UE_FLOTTANTES_PREV (PecheMenuTab.ENSEIGNEMENTS, "ueFlottantesPrev"),
	UE_FLOTTANTES_DEF (PecheMenuTab.ENSEIGNEMENTS, "ueFlottantesDef"),
	/** Validation par UE/EC. */
	VALIDATION_PAR_UE_EC_PREV (PecheMenuTab.ENSEIGNEMENTS, "validationParUeEcPrev"),
	VALIDATION_PAR_UE_EC_DEF (PecheMenuTab.ENSEIGNEMENTS, "validationParUeEcDef"),
	// Onglet "Paramétrage"
	/** Liste des composantes de l'établissements. */
	LISTE_COMPOSANTES (PecheMenuTab.PARAMETRAGE, "composantes"),
	/** Définition des circuits de validation. */
	PARAM_CIRCUIT_VALIDATION (PecheMenuTab.PARAMETRAGE, "definitionCircuitsValidation"),
	/** Application des circuits de validation. */
	PARAM_APPLI_CIRCUIT_VALIDATION (PecheMenuTab.PARAMETRAGE, "applicationCircuitsValidation"),
	/** Paramétrage REH. */
	PARAM_REH (PecheMenuTab.PARAMETRAGE, "activitesreh"),
	/** Paramétrage des types d'AP. */
	PARAM_TYPE_AP (PecheMenuTab.PARAMETRAGE, "typeshoraires"),
	/** Paramétage des HETD. */
	PARAM_HETD (PecheMenuTab.PARAMETRAGE, "paramhetd"),
	/** Paramétage des flux de paiement. */
	PARAM_FLUX_PAIEMENT (PecheMenuTab.PARAMETRAGE, "fluxPaiement"),
	/** Paramétage des HETD. */
	PARAM_TAUX_HORAIRE (PecheMenuTab.PARAMETRAGE, "tauxhoraire"),
	/** Liste des composantes de l'établissements. */
	CHOIX_COMPOSANTES (PecheMenuTab.PARAMETRAGE, "ueFlottantesChoixComposantes"),
	/** Duplication des ens. statutaires. */
	PARAM_DUPLICATION_STATUTAIRES (PecheMenuTab.PARAMETRAGE, "paramdupEnsStat"),
	/** Gestion des droits */
	PARAM_GESTION_DROITS (PecheMenuTab.PARAMETRAGE, "paramGestionDroits"),
	// Onglet "Mise en paiement"
	/** Mise en paiement. */
	MISE_EN_PAIEMENT (PecheMenuTab.MISE_EN_PAIEMENT, "paiement"),
	// Onglet "Suivi"
	/** Tableau de bord. */
	TABLEAU_DE_BORD (PecheMenuTab.SUIVI, "suiviValidations");
	
	
	private PecheMenuTab menuTab;
	
	/** Code du circuit. */
	private String id;

	/**
	 * Retourne le code du circuit.
	 *
	 * @return Le code du circuit
	 */
	public PecheMenuTab getMenuTab() {
		return menuTab;
	}

	/**
	 * Retourne le code du circuit.
	 *
	 * @return Le code du circuit
	 */
	public String getId() {
		return id;
	}

	/**
	 * Constructeur.
	 *
	 * @param codeCircuit Un code circuit
	 */
	PecheMenuItem(PecheMenuTab menuTab, String idItem) {
		this.menuTab = menuTab;
		this.id = idItem;
	}
	
	/**
	 * 
	 * @param idMenuItem id menu
	 * @return {@link PecheMenuTab}
	 */
	public static PecheMenuTab getPecheMenuTab(String idMenuItem) {
		PecheMenuTab resultat = null;
		
		for (int i = 0; i < PecheMenuItem.values().length; i++) {
			PecheMenuItem menuItem = PecheMenuItem.values()[i];
			
			if (menuItem.getId().equals(idMenuItem)) {
				resultat = menuItem.getMenuTab();
			}
		}
		
		return resultat;
	}
	

}
