package org.cocktail.peche.components.commun;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpeche.droits.Fonction;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Classe implémentant les méthodes d'affichage de la Popup Infos "?"
 *
 */

public class InfosPopupWindow extends CktlPecheBaseComponent {
    /**
	 * 
	 */
	private static final long serialVersionUID = -6259551431023835053L;

	private String autorisationItem;
	
    private IStructure structureEnseignementItem;
    private IStructure structureEnseignantItem;
    private IStructure composanteScolItem;
    private IStructure departementItem;
    
    public InfosPopupWindow(WOContext context) {
    	super(context);    	
    }

    public String getInfosPopupWindowId() {
        return getComponentId() + "_infosPopupWindowId"; 
      }
    
    public WOActionResults openPopupWindow() {
		CktlAjaxWindow.open(context(), getInfosPopupWindowId());
		
		return doNothing();
	}

	/**
	 * @return the listeAutorisations
	 */
	public ArrayList<String> listeAutorisations() {
		ArrayList<String> liste = new ArrayList<String>();		
			if (getPecheSession() != null) {
				for (Fonction tmp :Fonction.values()) {			
					if (getPecheSession().getPecheAutorisationCache().hasDroitUtilisationOnFonction(tmp)) {
						liste.add("- " + tmp.getDescription());					
					} else if (getPecheSession().getPecheAutorisationCache().hasDroitConnaissanceOnFonction(tmp)) {
						liste.add("- " + tmp.getDescription() + " (uniquement CONNAISSANCE)");					
					}
				}		
			}
		return liste;
	}
	
	public ArrayList<String> listeFonctions() {
		ArrayList<String> liste = new ArrayList<String>();
		if (getFonctionToutesValidations()) {
			liste.add("ToutesValidations");
		}
		if (getFonctionRepartiteur()) {
			liste.add("Répartiteur");
		}
		if (getFonctionEnseignant()) {
			liste.add("Enseignant");
		}
		if (getFonctionDirecteurComposante()) {
			liste.add("Directeur de Composante");
		}
		if (getFonctionDirecteurDepartement()) {
			liste.add("Directeur de Département");
		}
		if (getFonctionDirecteurCollege()) {
			liste.add("Directeur de Collège");
		}
		if (getFonctionDrh()) {
			liste.add("Directeur des Ressources Humaines");
		}
		if (getFonctionPresident()) {
			liste.add("Président");
		}		
		return liste;
	}

	/**
	 * @return the autorisationItem
	 */
	public String autorisationItem() {
		return autorisationItem;
	}

	/**
	 * @param autorisationItem the autorisationItem to set
	 */
	public void setAutorisationItem(String autorisationItem) {
		this.autorisationItem = autorisationItem;
	}

	public List<IStructure> getListeStructureEnseignantsStatutaires() {
		List<IStructure> liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
		new StructureTri().trierParLibelleLongPuisParentAsc(liste);
		return liste;
	}
	
	public List<IStructure> getListeStructureEnseignementStatutaires() {
		List<IStructure> liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementStatutaires();
		new StructureTri().trierParLibelleLongPuisParentAsc(liste);
		return liste;
	}
	
	public List<IStructure> getListeStructureEnseignementVacataires() {
		List<IStructure> liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire();
		new StructureTri().trierParLibelleLongPuisParentAsc(liste);
		return liste;
	}

	public List<IStructure> listeComposantesScolarite() {
		if (getPecheSession() != null) {
			List<IStructure> liste = getPecheSession().getComposantesScolarite();
			new StructureTri().trierParLibelleLongPuisParentAsc(liste);
			return liste;
		}
		return null;
	}

	public List<IStructure> listeDepartements() {
		if (getPecheSession() != null) {
			List<IStructure> liste = getPecheSession().getDepartements();
			new StructureTri().trierParLibelleLongPuisParentAsc(liste);
			return liste;
		}
		return null;
	}

	public String titreFicheIdentite() {
		if (getPecheSession() != null && getPecheSession().getApplicationUser() != null) {
			return "Fiche d'identité " + getPecheSession().getApplicationUser().getNomAndPrenom() 
					+ " (" + getPecheSession().getPecheParametres().getAnneeUniversitaireEnCoursll() + ")";
		}
		return null;
	}

	public IStructure getStructureEnseignementItem() {
		return structureEnseignementItem;
	}

	public IStructure getStructureEnseignantItem() {
		return structureEnseignantItem;
	}

	public IStructure getComposanteScolItem() {
		return composanteScolItem;
	}

	public IStructure getDepartementItem() {
		return departementItem;
	}

	public void setStructureEnseignementItem(IStructure structureEnseignementItem) {
		this.structureEnseignementItem = structureEnseignementItem;
	}

	public void setStructureEnseignantItem(IStructure structureEnseignantItem) {
		this.structureEnseignantItem = structureEnseignantItem;
	}

	public void setComposanteScolItem(IStructure composanteScolItem) {
		this.composanteScolItem = composanteScolItem;
	}

	public void setDepartementItem(IStructure departementItem) {
		this.departementItem = departementItem;
	}

}