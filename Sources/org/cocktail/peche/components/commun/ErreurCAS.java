package org.cocktail.peche.components.commun;

import com.webobjects.appserver.WOContext;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Cette page affiche un message d'erreur si la personne, authentifiée via le CAS, 
 * n'a pas le droit d'accéder à l'application
 * 
 */
public class ErreurCAS extends PecheComponent {

	private static final long serialVersionUID = 3878501368182450752L;

	/**
	 * Constructeur
	 * 
	 * @param context
	 *            the context
	 */
	public ErreurCAS(WOContext context) {
		super(context);
	}
	
	public String getDefaultStyles() {
		return ERXStringUtilities.toString(application().config().valuesForKey("HTML_CSS_STYLES").objects(), "\n");
	}

}