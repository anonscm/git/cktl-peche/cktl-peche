package org.cocktail.peche.components.tableauxDeBord;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.GenerateurXLSCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.metier.tableauxDeBord.CriteresRechercheService;
import org.cocktail.peche.metier.tableauxDeBord.FluxXmlSuiviValidations;
import org.cocktail.peche.metier.tableauxDeBord.RechercheFicheService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

/**	
 * Classe représentant les filtres à utiliser pour les tableaux de bord
 * @author Chama LAATIK
 */
public class RechercheTableauDeBord extends CktlPecheTableComponent<EORepartService> {

	private static final long serialVersionUID = -7342177115760957182L;
	
	private static final String LISTE_STRUCTURE_CONTAINER_ID = "listeStructureContainerId";
	
	private String currentPopulation;
	private String selectedPopulation;
	
	private String currentAnnee;
	private String selectedAnnee;
	
	private EOStructure currentComposante;
	private EOStructure selectedComposante;
	
	private String currentTypeFiche;
	private String selectedTypeFiche;
	
	private EtapePeche currentEtape;
	private List<EtapePeche> selectedListeEtapes;
	
	private List<EORepartService> listeRepartitionService;
	private EORepartService currentItem;

	private ERXDisplayGroup<EORepartService> displayGroup;
	
	private CriteresRechercheService criteresRecherche;
	private GenerateurXLSCtrl generateurXLSCtrl;
	private BasicPecheCtrl basicPecheCtrl;
	

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
    public RechercheTableauDeBord(WOContext context) {
        super(context);
    	basicPecheCtrl = new BasicPecheCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        criteresRecherche = new CriteresRechercheService(getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        listeRepartitionService = new ArrayList<EORepartService>();
        displayGroup = null;
        
        setSelectedAnnee(new Integer(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()).toString());
    	
    }
    
    /**
	 * Recherche des résultats pour le tableau de bord en fonction des critères de recherche
	 * @return null
	 */
	public WOActionResults rechercherListeTableauBord() {
		if (getSelectedListeEtapes().size() == 0) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.TABLEAU_BORD_RENSEIGNER_AU_MOINS_UN_ETAT);
			return null;
		}
		
		edc().revert(); //car réutilisation du bouton recherché possible
		
		if (getSelectedAnnee() != null) {
			getCriteresRecherche().setAnneeUniversitaire(Integer.valueOf(getSelectedAnnee()));	
		}

		List<IStructure> listeStructures = new ArrayList<IStructure>();
		if (criteresRecherche.getStructure() != null) {
			listeStructures.add(criteresRecherche.getStructure());
		} else {
			listeStructures.addAll(listeComposantes());
		}

		getCriteresRecherche().setListeComposantes(listeStructures);
		
		if (getSelectedListeEtapes().size() > 0) {
			getCriteresRecherche().setListeEtapesCircuitValidation(getSelectedListeEtapes());
		}
		
		getCriteresRecherche().setPopulation(getSelectedPopulation());
		getCriteresRecherche().setTypeFiche(getSelectedTypeFiche());
		
		//On recherche la liste des répartitions en la limitant aux 50 premiers
		listeRepartitionService = RechercheFicheService.getInstance().rechercherListeFichesServices(this.edc(), getCriteresRecherche(), true);
		
		//Message d'information si la liste est vide
		if (listeRepartitionService.isEmpty()) {
			getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, Constante.LISTE_RESULTAT_VIDE);
			return null;
		}
		
		setDisplayGroup(null);		//Réinitialiser le displayGroup pour afficher les résultats de la recherche
		
		return null;
	}
	
	/**
	 * Export des tableaux de bord
	 * @return null
	 */
	public WOActionResults exporterTableauBord() {
		//génération du xml
		FluxXmlSuiviValidations flux = new FluxXmlSuiviValidations(edc());
		
		//On exporte la liste de toutes les répartitions qui répondent aux critères
		generateurXLSCtrl = new GenerateurXLSCtrl(flux.getFlux(RechercheFicheService.getInstance().rechercherListeFichesServices(
				this.edc(), getCriteresRecherche(), false), getCriteresRecherche()));
		generateurXLSCtrl.setReportFilename("suiviValidations.xls");
		generateurXLSCtrl.genererTableauDeBord();
		
		return null;
	}
	
	/**
	 * Renvoie la liste des étapes utilisées dans PECHE
	 * @return liste des étapes
	 */
	public List<EtapePeche> listeEtapes() {
		List<EtapePeche> liste = new ArrayList<EtapePeche>(Arrays.asList(EtapePeche.values()));
		//On enlève l'étape qui concerne uniquement le circuit de validation de la fiche de voeux
		liste.remove(EtapePeche.VALID_ENSEIGNANT_FICHE_VOEUX);
		
		return liste;
	}
	
	/**
	 * @return le displayGroup
	 */
	public ERXDisplayGroup<EORepartService> getDisplayGroup() {
		if (displayGroup == null) {
			displayGroup = new ERXDisplayGroup<EORepartService>();
			EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EORepartService.class), edc());
			dataSource.setArray(new NSArray<EORepartService>(getListeRepartitionService()));
			displayGroup.setDataSource(dataSource); 
			displayGroup.setDelegate(this);
			
			displayGroup.setSelectsFirstObjectAfterFetch(false);
			displayGroup.setNumberOfObjectsPerBatch(getNumberOfObjectsPerBatch());
			displayGroup.fetch();
		}
		
		return displayGroup;
	}
	
	/**
	 * @return l'affichage de l'année universitaire selectionnée
	 */
	public String anneesDisplayString() {
		return (String) listeAnneesUniversitaires().valueForKey(getCurrentAnnee());
	}
	
	/**
	 * @return libellé de l'étape du circuit de validation
	 */
	public String getCurrentLibelleEtape() {
		if (getCurrentEtape() != null) {
			return " " + getCurrentEtape().getEtatDemande();
		}
		
		return "";
	}
	
	/**
	 * @return libellé de l'étape de la demande de la fiche de service
	 */
	public String getCurrentLibelleEtapeDemande() {
		if (getCurrentItem() != null) {
			return EtapePeche.getEtape(getCurrentItem().toDemande().toEtape()).getEtatDemande();
		}
		
		return "";
	}

	/**
	 * @return libellé de la composante de l'enseignant
	 */
	public String getCurrentComposanteEnseignant() {
		String composante = "";

		if (getCurrentItem() == null) {
			return composante;
		}

		if (Constante.VACATAIRE.equals(getSelectedPopulation())  
				&& (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_CS))) {
			return getCurrentItem().toStructure().lcStructure();
		} 

		if (Constante.VACATAIRE.equals(getSelectedPopulation())  
				&& (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_DE))) {

			EOStructure parent = getCurrentItem().toStructure().toStructurePere();
			if (parent != null && basicPecheCtrl.isStructureDeTypeGroupe(parent, EOTypeGroupe.TGRP_CODE_CS)) {
				return parent.lcStructure();
			}

			parent = getCurrentItem().toStructure().toStructurePere().toStructurePere();
			if (parent != null && basicPecheCtrl.isStructureDeTypeGroupe(parent, EOTypeGroupe.TGRP_CODE_CS)) {
				return parent.lcStructure();
			}
		}

		if (getCurrentItem().toService().enseignant() != null) {
			EOStructure struct = getCurrentItem().toService().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			if (struct != null) {
				composante = struct.lcStructure();
			}
		}		

		return composante;
	}

	/**
	 * @return libellé du département de l'enseignant
	 */
	public String getCurrentDepartementEnseignant() {
		String dept = "";

		if (getCurrentItem() == null) {
			return dept;
		}

		if (Constante.VACATAIRE.equals(getSelectedPopulation()) 
				&& (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_DE))) {
			dept = getCurrentItem().toStructure().lcStructure();
		}	
		else {
			EOStructure departement = getCurrentItem().toService().enseignant().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			if (departement != null) {
				dept = departement.lcStructure();
			}

		}

		return dept;
	}

	
	public boolean isListeRepartitionRenseigne() {
		return (getListeRepartitionService().size() == 0);
	}

	/**
	 * Méthode de contournement pour sauter une ligne après une case à cocher
	 * 	=> sinon erreur de parsing sur la balise BR par Wolips 
	 *   (ERROR ... suffix = "<br ' has a quote left open.)
	 * @return balise <br>
	 */
	public String baliseBR() {
		return "<br>";
	}
	
	/**
	 * @return la liste des types de population
	 */
	public List<String> listeTypesPopulation() {
		List<String> listeTypesPopulation = new ArrayList<String>();
		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire())  {
			listeTypesPopulation.add(Constante.VACATAIRE);	
		}

		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsStatutaires())  {
			listeTypesPopulation.add(Constante.STATUTAIRE);	
		}
		return listeTypesPopulation;
	}
	
	/**
	 * @return la liste des types de population
	 */
	public List<String> listeTypesFiche() {
		List<String> listeTypesFiche = new ArrayList<String>();
		listeTypesFiche.add(Constante.PREVISIONNEL);
		listeTypesFiche.add(Constante.DEFINITIF);

		return listeTypesFiche;
	}
	
	/**
	 * Population sélectionnée par défaut est "Vacataires"
	 * @return population sélectionnée
	 */
	public String getSelectedPopulation() {		
		if (selectedPopulation == null 
				&& getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire())  {
			selectedPopulation = Constante.VACATAIRE;	
		}
		if (selectedPopulation == null 
				&& !getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire()
				&& getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires())  {
			selectedPopulation = Constante.STATUTAIRE;	
		}		
		return selectedPopulation;
	}

	/**
	 * Type de fiche sélectionné par défaut est "Définitif"
	 * @return type de fiche sélectionné
	 */
	public String getSelectedTypeFiche() {
		if (selectedTypeFiche == null) {
			selectedTypeFiche = Constante.DEFINITIF;				
		}
		return selectedTypeFiche;
	}

	public void setSelectedTypeFiche(String selectedTypeFiche) {
		this.selectedTypeFiche = selectedTypeFiche;
	}

	public void setSelectedPopulation(String selectedPopulation) {
		this.selectedPopulation = selectedPopulation;
	}

	public String getCurrentPopulation() {
		return currentPopulation;
	}

	public void setCurrentPopulation(String currentPopulation) {
		this.currentPopulation = currentPopulation;
	}

	public String getCurrentAnnee() {
		return currentAnnee;
	}

	public void setCurrentAnnee(String currentAnnee) {
		this.currentAnnee = currentAnnee;
	}

	public String getCurrentTypeFiche() {
		return currentTypeFiche;
	}

	public void setCurrentTypeFiche(String currentTypeFiche) {
		this.currentTypeFiche = currentTypeFiche;
	}

	public EOStructure getCurrentComposante() {
		return currentComposante;
	}

	public void setCurrentComposante(EOStructure currentComposante) {
		this.currentComposante = currentComposante;
	}

	public EOStructure getSelectedComposante() {
		return selectedComposante;
	}

	public void setSelectedComposante(EOStructure selectedComposante) {
		this.selectedComposante = selectedComposante;
	}

	public String getSelectedAnnee() {
		return selectedAnnee;
	}

	public void setSelectedAnnee(String selectedAnnee) {
		this.selectedAnnee = selectedAnnee;
	}
	
	public EtapePeche getCurrentEtape() {
		return currentEtape;
	}

	public void setCurrentEtape(EtapePeche currentEtape) {
		this.currentEtape = currentEtape;
	}

	public List<EtapePeche> getSelectedListeEtapes() {
		return selectedListeEtapes;
	}

	public void setSelectedListeEtapes(List<EtapePeche> selectedListeEtapes) {
		this.selectedListeEtapes = selectedListeEtapes;
	}

	public List<EORepartService> getListeRepartitionService() {
		return listeRepartitionService;
	}

	public void setListeRepartitionService(List<EORepartService> listeRepartitionService) {
		this.listeRepartitionService = listeRepartitionService;
	}

	public EORepartService getCurrentItem() {
		return currentItem;
	}

	public void setCurrentItem(EORepartService currentItem) {
		this.currentItem = currentItem;
	}
	
	public void setDisplayGroup(ERXDisplayGroup<EORepartService> displayGroup) {
		this.displayGroup = displayGroup;
	}

	public CriteresRechercheService getCriteresRecherche() {
		return criteresRecherche;
	}

	public void setCriteresRecherche(CriteresRechercheService criteresRecherche) {
		this.criteresRecherche = criteresRecherche;
	}

	public GenerateurXLSCtrl getGenerateurXLSCtrl() {
		return generateurXLSCtrl;
	}

	public void setGenerateurXLSCtrl(GenerateurXLSCtrl generateurXLSCtrl) {
		this.generateurXLSCtrl = generateurXLSCtrl;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.TABLEAU_DE_BORD.getId();
	}
	
	
	public boolean affichageUniqueComposante() {
		if (listeComposantes().size() == 1) {			
			criteresRecherche.setStructure(listeComposantes().get(0));
			return true;
		}
		return false;
	}
			
	public List<IStructure> listeComposantes() {
		List<IStructure> liste = null;
		if (Constante.VACATAIRE.equals(getSelectedPopulation())) {
			liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire();
			new StructureTri().trierParLibelleCourt(liste);
			return liste;			
		} else {
			liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
			new StructureTri().trierParLibelleCourt(liste);
			return liste;
		}
	}
	
	public String listeStructureContainerId() {
		return LISTE_STRUCTURE_CONTAINER_ID;
	}

	public String displayComposantes() {
		if (getCurrentComposante() != null) {
			return getCurrentComposante().lcStructure() + " (" + getCurrentComposante().toStructurePere().lcStructure() + ")";  
		}
		return null;		
	}

	
}