package org.cocktail.peche.components.ui;

import java.text.DecimalFormat;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Représente un nombre formaté avec possibilité d'indiquer des couleurs
 * différentes si la valeur est négative.
 * 
 * @author Yannick Mauray
 * 
 */
public class CktlNumber extends WOComponent {

	private static final String DEFAULT_NUMBER_FORMAT = "0.00";

	private static final long serialVersionUID = -3581243964400739094L;
	
	private String numberformat;

	/**
	 * Constructeur.
	 * @param context le contexte d'exécution.
	 */
	public CktlNumber(WOContext context) {
		super(context);
	}
	
	private Number value() {
		return (Number) valueForBinding("value");
	}
	
	private Number compareTo() {
		return (Number) valueForNumberBinding("compareTo", 0.0d);
	}
	
	/**
	 * @return la valeur formatée.
	 */
	public String getFormatedValue() {
		
		Number val = value();
		Number compareTo = compareTo();
		
		if (val == null) return "";
		
		DecimalFormat format = new DecimalFormat(getNumberformat());
		String formatedValue = format.format(val);
		if (val.doubleValue() < compareTo.doubleValue()) {
			formatedValue = "<span class=\"cktl-number cktl-number-negative\">" + formatedValue + "</span>";
		} else if (val.doubleValue() > compareTo.doubleValue()) {
			formatedValue = "<span class=\"cktl-number cktl-number-positive\">" + formatedValue + "</span>";
		} else {
			formatedValue = "<span class=\"cktl-number cktl-number-zero\">" + formatedValue + "</span>";
		}
		return formatedValue;
	}
	
	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	/**
	 * @return le format des nombres.
	 */
	public String getNumberformat() {
		if ((String) valueForBinding("numberformat") != null) {
			return (String) valueForBinding("numberformat");
		}
		if (numberformat == null) {
			numberformat = DEFAULT_NUMBER_FORMAT;
		}
		return numberformat;
	}

	public void setNumberformat(String numberformat) {
		this.numberformat = numberformat;
	}
}
