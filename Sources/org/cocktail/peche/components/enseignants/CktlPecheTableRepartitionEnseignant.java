package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;

/**
 * Classe commune pour l'affichage des répartitions.
 * @author Equipe GRH
 */
public abstract class CktlPecheTableRepartitionEnseignant extends CktlPecheTableComponent<EOServiceDetail> {
	
	private static final long serialVersionUID = 1852234770673277601L;
	
	private boolean isMemeComposante;
	
	
	private ComposantCtrl composantCtrl;
	
	
	public CktlPecheTableRepartitionEnseignant(WOContext context) {
		super(context);
		this.composantCtrl = new ComposantCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	/**
	 * @return l'état de la répartition croisée s'il y en a une.
	 */
	public String getEtatRepartition() {
		return getEtatRepartition(getCurrentItem().etat());
	}
	
	/**
	 * Renvoie la composante du parent direct de l'AP
	 * @return composante du parent
	 */
	public String getComposanteParentAP() {
		return getComposantCtrl().getComposanteParentAP(getCurrentItem());
	}
	
	/**
	 * @return l'état de la répartition croisée s'il y en a une.
	 */
	public String getEtatRepartitionService() {
		return getEtatRepartition(getCurrentItem().etat());
	}
	
	
	public boolean isMemeComposante() {
		return isMemeComposante;
	}

	public boolean getBoutonsActifs() {
		return valueForBooleanBinding("boutonsActifs", true);
	}

	public boolean getBoutonsInactifs() {
		return getSelectedObject() == null;
	}

	public ComposantCtrl getComposantCtrl() {
		return composantCtrl;
	}
	

	public boolean hasCommentaire() {		
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return (getCurrentItem().commentaire().trim().length() > 0);
		}
		return false;
	}

	public String currentItemCommentaire() {
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return StringCtrl.replace(getCurrentItem().commentaire(), "\n", "<br>");			
		}
		return "";		
	}

	
	
}
