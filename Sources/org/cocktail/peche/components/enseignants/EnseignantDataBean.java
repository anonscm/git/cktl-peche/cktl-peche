package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOEnseignantGenerique;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXKey;

/**
 * Classe représentant un bean d'un enseignant
 * 	=> soit un enseignant générique soit un enseignant de l'établissement (statutaire ou vacataire)
 * 
 * @author Chama LAATIK
 */
public class EnseignantDataBean {
	
	private EOActuelEnseignant enseignant;
	private EOEnseignantGenerique enseignantGenerique;
	
	private String nom;
	private String prenom;
	private IStructure composante;
	private IStructure departement;
	private double nbHeuresDu; 
	private double deltaAttribue;
	private String statut;
	private EOStructure structureReferente;
	private boolean isStatutaire;
	
	//permet de filtrer
	public static final ERXKey<String> NOM = new ERXKey<String>("nom");
	public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
	
	
	/**
	 * Constructeur pour un enseignant générique
	 * @param enseignantGenerique : enseignant générique
	 */
	public EnseignantDataBean(EOEnseignantGenerique enseignantGenerique, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		this.enseignantGenerique = enseignantGenerique;
		this.enseignant = null;
		
		this.nom = enseignantGenerique.nom();
		this.prenom = enseignantGenerique.prenom();
		this.composante = getComposanteEnsGenerique(enseignantGenerique, annee, isFormatAnneeExerciceAnneeCivile);
		this.departement = getDepartementEnsGenerique(enseignantGenerique, annee, isFormatAnneeExerciceAnneeCivile);
		this.nbHeuresDu = enseignantGenerique.heuresPrevues();
		this.deltaAttribue = calculerDeltaAttribue(enseignantGenerique, annee, isFormatAnneeExerciceAnneeCivile);		
		this.statut = enseignantGenerique.getStatut();
		this.isStatutaire = enseignantGenerique.isEnseignantStatutaire();
	}
	
	/**
	 * Constructeur pour un enseignant statutaire ou vacataire 
	 * @param enseignant : enseignant statutaire ou vacataire
	 * @param annee : annee pour laquelle on calcule les heures dues d'un enseignant
	 */
	public EnseignantDataBean(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		this.enseignant = enseignant;
		this.enseignantGenerique = null;
		
		this.nom = enseignant.toIndividu().nomAffichage();
		this.prenom = enseignant.toIndividu().prenomAffichage();
		this.composante = enseignant.getComposante(annee, isFormatAnneeExerciceAnneeCivile);
		this.departement = enseignant.getDepartementEnseignement(annee, isFormatAnneeExerciceAnneeCivile);
		this.statut = enseignant.getStatut(annee, isFormatAnneeExerciceAnneeCivile);
		this.nbHeuresDu = calculerNbHeuresDu(enseignant, annee, isFormatAnneeExerciceAnneeCivile);
		this.deltaAttribue = calculerDeltaAttribue(enseignant, annee, isFormatAnneeExerciceAnneeCivile);
		this.isStatutaire = enseignant.isStatutaire(annee, isFormatAnneeExerciceAnneeCivile);		
	}
	
	/**
	 * Calcule les heures dues d'un eneignant statutaire ou vacataire
	 * @param enseignant : eneignant statutaire ou vacataire
	 * @param annee : année utilisée pour le calcul du service dû
	 * @return nombre d'heures
	 */
	public double calculerNbHeuresDu(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double nbHeures = 0;
		if (enseignant != null) {
			if (enseignant.isStatutaire(annee, isFormatAnneeExerciceAnneeCivile)) {
				EnseignantsStatutairesCtrl controleurStatutaire = new EnseignantsStatutairesCtrl(enseignant.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
				nbHeures = controleurStatutaire.getServiceDu(enseignant, annee);
			} else {
				EnseignantsVacatairesCtrl controleurVacataire = new EnseignantsVacatairesCtrl(enseignant.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
				nbHeures = controleurVacataire.getTotalNbHeuresVacation(enseignant);
			}
		}

		return nbHeures;
	}
	
	public double calculerDeltaAttribue(EOActuelEnseignant enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double nbHeures = 0;
		if (enseignant != null) {
			if (enseignant.isStatutaire(annee, isFormatAnneeExerciceAnneeCivile)) {
				EnseignantsStatutairesCtrl controleurStatutaire = new EnseignantsStatutairesCtrl(enseignant.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
				nbHeures = controleurStatutaire.getServiceAttribue(enseignant) - nbHeuresDu;
			} else {
				EnseignantsVacatairesCtrl controleurVacataire = new EnseignantsVacatairesCtrl(enseignant.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
				nbHeures = controleurVacataire.getServiceAttribue(enseignant) - nbHeuresDu;
			}
		}

		return nbHeures;
	}
	
	
	public double calculerDeltaAttribue(EOEnseignantGenerique enseignant, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		double nbHeures = 0;
		if (enseignant != null) {	
			EnseignantsGeneriquesCtrl controleur = new EnseignantsGeneriquesCtrl(enseignant.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);			
			nbHeures = controleur.getServiceAttribue(enseignant) - nbHeuresDu;
		}

		return nbHeures;
	}
	
	private IStructure getComposanteEnsGenerique(EOEnseignantGenerique enseignantGenerique, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		EnseignantsGeneriquesCtrl controleurGenerique = new EnseignantsGeneriquesCtrl(enseignantGenerique.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
		return controleurGenerique.getStructureComposante(enseignantGenerique);
	}
	
	private IStructure getDepartementEnsGenerique(EOEnseignantGenerique enseignantGenerique, Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		EnseignantsGeneriquesCtrl controleurGenerique = new EnseignantsGeneriquesCtrl(enseignantGenerique.editingContext(), annee, isFormatAnneeExerciceAnneeCivile);
		return controleurGenerique.getStructureDepartement(enseignantGenerique);
	}
	
	public boolean isGenerique() {
		return (this.enseignantGenerique != null);
	}
	
	public boolean isStatutaire() {
		return this.isStatutaire;
	}
	
	/**
	 * Renvoie le qualifier pour filtrer les enseignantBean par nom
	 * @param tokenNom : nom qu'on veut filtrer
	 * @return qualifier
	 */
	public static EOQualifier getQualifierEnseignantBeanFiltrerParNom(String tokenNom) {
    	return EnseignantDataBean.NOM.contains(tokenNom);
    }
	
	/**
	 * Renvoie le qualifier pour filtrer les enseignantBean par prénom
	 * @param tokenPrenom : prénom qu'on veut filtrer
	 * @return qualifier
	 */
	public static EOQualifier getQualifierEnseignantBeanFiltrerParPrenom(String tokenPrenom) {
    	return EnseignantDataBean.PRENOM.contains(tokenPrenom);
    }
	
	/* ========== Getters & Setters ========== */
	
	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(EOActuelEnseignant enseignant) {
		this.enseignant = enseignant;
	}
	public EOEnseignantGenerique getEnseignantGenerique() {
		return enseignantGenerique;
	}
	public void setEnseignantGenerique(EOEnseignantGenerique enseignantGenerique) {
		this.enseignantGenerique = enseignantGenerique;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public IStructure getComposante() {
		return composante;
	}

	public void setComposante(IStructure composante) {
		this.composante = composante;
	}

	public double getNbHeuresDu() {
		return nbHeuresDu;
	}

	public void setNbHeuresADu(double nbHeuresDu) {
		this.nbHeuresDu = nbHeuresDu;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public IStructure getDepartement() {
		return departement;
	}

	public void setDepartement(IStructure departement) {
		this.departement = departement;
	}

	public double getDeltaAttribue() {
		return deltaAttribue;
	}

	public EOStructure getStructureReferente() {
		return structureReferente;
	}

	public void setStructureReferente(EOStructure structureReferente) {
		this.structureReferente = structureReferente;
	}
}