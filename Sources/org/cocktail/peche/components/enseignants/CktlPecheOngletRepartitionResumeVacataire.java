package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.PecheApplicationUser;

import com.webobjects.appserver.WOContext;

/**
 * 
 * @author juliencallewaert
 *
 */
public class CktlPecheOngletRepartitionResumeVacataire extends CktlPecheTableRepartitionEnseignantVacataire {
    
	/**
	 * 
	 * @param context contexte
	 */
	public CktlPecheOngletRepartitionResumeVacataire(WOContext context) {
		super(context);
	}


	/**
	 * 
	 */
	private static final long serialVersionUID = 1157716745344020608L;

	/**
	 * Enregistre le commentaire de la fiche liée à cette structure
	 */
	public void enregistrerCommentaireSousFiche() {
		try {
			PecheApplicationUser appUser = getPecheSession().getApplicationUser();
			EOPersonne personne = appUser.getPersonne();

			getRepartService().majDonnesAuditModification(personne);
			edc().saveChanges();

			getPecheSession().addSimpleInfoMessage(message("ficheService.message.commentaire.sauvegarde.titre"), message("ficheService.message.commentaire.sauvegarde.detail"));
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
		}
	}

	@Override
	protected String getTableKey() {
		return null;
	}

	
	
}