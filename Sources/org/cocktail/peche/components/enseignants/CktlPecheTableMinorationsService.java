package org.cocktail.peche.components.enseignants;


import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Cette classe représente la table contenant les minorations de service de l'enseignant
 * 
 * @author Chama LAATIK
 */
public class CktlPecheTableMinorationsService extends CktlPecheTableComponent<MinorationsServiceDataBean> {
	
	private static final long serialVersionUID = -976604851839862043L;
	
	private EOActuelEnseignant enseignant;
	private EnseignantsStatutairesCtrl controleur;
	
	private NSArray<MinorationsServiceDataBean> listeMinorationsService;
	private HceCtrl hceCtrl;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableMinorationsService(WOContext context) {
        super(context);
        
        this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        this.hceCtrl = new HceCtrl(this.edc());
    }
	
	@Override
	public void awake() {
		enseignant = (EOActuelEnseignant) valueForBinding("enseignant");
		
		this.listeMinorationsService = controleur.rechercherReductionsService(getEnseignant(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		this.getDisplayGroup().setObjectArray(listeMinorationsService);
	}

	/**
	 * Renvoie le nombre d'heures de la minoration de service
	 * @return nombre d'heures
	 */
	public String getNbHeures() {
		if (getCurrentItem().getAbsence() != null) {
			//Si le nombre d'heures de la modalité de service n'est pas renseigné => on calcule au prorata 
			if (getCurrentItem().getAbsence().isModalite() && getCurrentItem().getNbHeures() == null) {
				return String.format("%.2f", hceCtrl.getNbHeuresModaliteService(getEnseignant(), getCurrentItem().getAbsence(), 
						getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			}
		}
		
		if (getCurrentItem().getNbHeures() == null) {
			return Constante.NON_DEFINI;
		}
		
		return String.format("%.2f", getCurrentItem().getNbHeures());
	}
	
	/**
	 * Dégrise le bouton si la minoration est une absence ou une modalité de service
	 * @return vrai/faux
	 */
	public boolean getGriserBouton() {
		if (getSelectedObject() != null) {
			return !(getSelectedObject().isAbsence() && getAutorisation().hasDroitUtilisationParametrageHeuresAbsence());
		}
		
		return true;
	}
	
	/**
	 * Surcharge le nombre d'heures pour une absence
	 * @return formulaire de saisie
	 */
	public WOActionResults surchargerNbHeuresAbsence() {
		 if (getSelectedObject() != null) {
			CktlPecheFormMinorationService page = (CktlPecheFormMinorationService) pageWithName(CktlPecheFormMinorationService.class.getName());
			page.setEnseignant(getEnseignant());
			page.setEditedObject(getSelectedObject());
			return page;
		}
		 
		return null;
	}
	
	/**
	 * Permet de savoir si la minoration permet de faire des heures complémentaires ou non
	 * @return Oui/Non
	 */
	public String getTemoinHComp() {
		 if (Constante.OUI.equalsIgnoreCase(getCurrentItem().getTemoinHcomp())) {
			return message(Messages.OUI_LIBELLE);
		 }
		
		return message(Messages.NON_LIBELLE);
	}
	
	/* ========== Getters & Setters ========== */
	
	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}

	public EOIndividu getIndividu() {
		return enseignant.toIndividu();
	}
	
	public NSArray<MinorationsServiceDataBean> getListeMinorationsService() {
		return listeMinorationsService;
	}

	public void setListeMinorationsService(NSArray<MinorationsServiceDataBean> listeMinorationsService) {
		this.listeMinorationsService = listeMinorationsService;
	}
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	public Double getSousTotalHeures() {
		return hceCtrl.getTotalNbHeuresReduction(getEnseignant(), 
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
}