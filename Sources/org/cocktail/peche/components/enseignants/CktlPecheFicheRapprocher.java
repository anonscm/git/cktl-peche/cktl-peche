package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOContext;

//FIXME : Classe à supprimer?
public class CktlPecheFicheRapprocher extends CktlPecheBaseComponent {
	
	private EOActuelEnseignant editedObject;
	
    public CktlPecheFicheRapprocher(WOContext context) {
        super(context);
    }
    

    
  
	public void setEditedObject(EOActuelEnseignant editedObject) {
		this.editedObject = editedObject;

	}
	
	public EOActuelEnseignant getEditedObject() {
		return this.editedObject;
	}
}