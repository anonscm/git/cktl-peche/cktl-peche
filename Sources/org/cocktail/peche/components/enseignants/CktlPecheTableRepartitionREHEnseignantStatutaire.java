package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

/**
 * Cette classe représente la table contenant la répartition des heures de
 * service au titre du référentiel équivalence horaire.
 * 
 * @author yannick
 * 
 */
public class CktlPecheTableRepartitionREHEnseignantStatutaire extends
		CktlPecheTableRepartitionEnseignantStatutaire {

	private static final long serialVersionUID = -7951661334775289880L;
	

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionREHEnseignantStatutaire(WOContext context) {
		super(context);
    }

	
	@Override
	protected String getTableKey() {
		return EOServiceDetail.REH_KEY;
	}

	/**
	 * Action générique de modification.
	 * ServiceInitialForm
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			CktlPecheFormServiceEnseignantStatutaire page = (CktlPecheFormServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFormServiceEnseignantStatutaire.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(getSelectedObject());
			page.setEnseignant(getEnseignant());
			page.setRepartService(getRepartService());
			page.setRepartitionREH(true);
			return page;
		} else {
			return null;
		}
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantStatutaire page = (CktlPecheFormServiceEnseignantStatutaire) 
				pageWithName(CktlPecheFormServiceEnseignantStatutaire.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setEnseignant(getEnseignant());
		page.setRepartService(getRepartService());
		page.setRepartitionREH(true);
		return page;
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 * @throws Exception 
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				if (isEnseignantAutreComposante(getSelectedObject())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				if ((getSelectedObject().heuresPrevues() != 0)  &&  (isCircuitDefinitif())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				
				
				edc().deleteObject(getSelectedObject());
				getRepartService().toService().majDonnesServiceReparti(edc(),
						getPecheSession().getApplicationUser().getPersonne(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				getControleur().recalculerIndicateurValidationAuto(getRepartService());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}
		AjaxUpdateContainer.updateContainerWithID("serviceHETD", context());
		return doNothing();
	}
	
	/**
	 * Renvoie vrai si 
	 *    le répartiteur n'appartient pas à la meme composante que l'enseignant
	 * et le service détail n'est pas en attente (soit Null soit Accepté/Refusé) 
	 * 
	 * @param serviceDetail : détail du service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean isEnseignantAutreComposante(EOServiceDetail serviceDetail) {
		if ((serviceDetail.service().enseignant() != null) && (serviceDetail.service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(
					serviceDetail.service().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
							getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))
					&& !(Constante.ATTENTE.equals(serviceDetail.etat()));
		}
		return false;
	}
	
	public boolean isCircuitDefinitif() {
		return !getControleur().isCircuitPrevisionnel(getRepartService().toDemande());
	}


	@Override
	public boolean hasCommentaire() {
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return (getCurrentItem().commentaire().trim().length() > 0);
		}
		return false;
	}

	@Override
	public String currentItemCommentaire() {
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return StringCtrl.replace(getCurrentItem().commentaire(), "\n", "<br>");			
		}
		return "";		
	}
	
}
