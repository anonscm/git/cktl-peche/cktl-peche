package org.cocktail.peche.components.enseignants;

/**
 * 
 * @author Yannick Mauray
 * 
 */
public class EOVPersonnelActuelEnsExtra {
	private String service;
	private String composante;
	private double serviceStatutaire;
	private double serviceDu;
	private double serviceAttribue;
	private double serviceAFaire;

	/**
	 * Constructeur.
	 */
	public EOVPersonnelActuelEnsExtra() {
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public String getComposante() {
		return composante;
	}

	public void setComposante(String composante) {
		this.composante = composante;
	}

	public double getServiceStatutaire() {
		return serviceStatutaire;
	}

	public void setServiceStatutaire(double serviceStatutaire) {
		this.serviceStatutaire = serviceStatutaire;
	}

	public double getServiceDu() {
		return serviceDu;
	}

	public void setServiceDu(double serviceDu) {
		this.serviceDu = serviceDu;
	}

	public double getServiceAttribue() {
		return serviceAttribue;
	}

	public void setServiceAttribue(double serviceAttribue) {
		this.serviceAttribue = serviceAttribue;
	}

	public double getServiceAFaire() {
		return serviceAFaire;
	}

	public void setServiceAFaire(double serviceAFaire) {
		this.serviceAFaire = serviceAFaire;
	}
};
