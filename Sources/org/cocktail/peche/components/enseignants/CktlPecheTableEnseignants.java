package org.cocktail.peche.components.enseignants;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOEnseignantGenerique;
import org.cocktail.peche.entity.EOPecheParametre;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

/**
 * Table qui renvoie la liste des enseignants 
 * 
 * @author Chama LAATIK
 */
public class CktlPecheTableEnseignants extends CktlPecheTableComponent<EnseignantDataBean> {
	
	private static final long serialVersionUID = -7713645376905791738L;
	private BasicPecheCtrl controleur;

	private String containerParent;
	
	private String nomEnseignantToutesComposantes;
	private EOStructure currentComposanteScolarite;
	private EOStructure composanteScolariteSelectionnee;
	
	private EOQualifier qualifierRepartitionCroisee;
	private EnseignantsStatutairesCtrl controleurStatutaires;
	private EnseignantsVacatairesCtrl controleurVacataires;
	
	private String tokenNom;
	private String tokenPrenom;
	
	private List<EOAP> listeAPs;
	private EOAP currentAP;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'accès à la base de données.
	 */
    public CktlPecheTableEnseignants(WOContext context) {
        super(context);
        this.controleurStatutaires = new EnseignantsStatutairesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        this.controleurVacataires = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    }
  
    @Override
	public void awake() {
		super.awake();

		//permet de mettre à jour un élement du parent
		if (hasBinding("containerParent")) {
			setContainerParent((String) valueForBinding("containerParent")); 
		}
		
		if (hasBinding("typeFiche")) {
			setTypeFiche((String) valueForBinding("typeFiche"));
		}
		
		
	}

        @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	getDisplayGroup().setCurrentBatchIndex(1); // Page 1
    	//Important, en combinant les deux filtres, on peut se retrouver avec le nombre de résultat (donc moins de 20) dans cette zone !
		getDisplayGroup().setNumberOfObjectsPerBatch(getNumberOfObjectsPerBatch()); 
		
    	this.getDisplayGroup().setObjectArray(filtrerDoublons(getListeEnseignants()));

    	super.appendToResponse(response, context);
    }
    
    private NSArray<EnseignantDataBean> filtrerDoublons(NSArray<EnseignantDataBean> liste) {

    	NSMutableArray<Integer> entrees = new NSMutableArray<Integer>();
    	NSMutableArray<Integer> entreesGenerique = new NSMutableArray<Integer>();
    	NSMutableArray<EnseignantDataBean> sortie = new NSMutableArray<EnseignantDataBean>();
    	EnseignantDataBean enseignantDataBean;

    	for (int i = 0; i < liste.count(); i++) {
    		enseignantDataBean = (EnseignantDataBean) liste.get(i);
    		if (enseignantDataBean.getEnseignant() != null && (!entrees.contains(enseignantDataBean.getEnseignant().noDossierPers()))) {
    			sortie.add(enseignantDataBean);
    			entrees.add(enseignantDataBean.getEnseignant().noDossierPers());
    		}
    		if (enseignantDataBean.getEnseignantGenerique() != null && (!entreesGenerique.contains(enseignantDataBean.getEnseignantGenerique().id()))) {
    			sortie.add(enseignantDataBean);
    			entreesGenerique.add(enseignantDataBean.getEnseignantGenerique().id());
    		}
    	}

    	return sortie.immutableClone();
    }
    
    /**
     * @return la liste des enseignantsBean
     */
    public NSArray<EnseignantDataBean> getListeEnseignants() {
    	NSArray<EnseignantDataBean> listeEnseignants = new NSMutableArray<EnseignantDataBean>();
    	List<EOActuelEnseignant> listeActuelEnseignants = getListeActuelEnseignants();
    	List<EOEnseignantGenerique> listeEnseignantGenerique = getListeEnseignantsGeneriques();

    	//On récupère les enseignants
    	for (EOActuelEnseignant enseignant : listeActuelEnseignants) {
    		listeEnseignants.add(new EnseignantDataBean(enseignant, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
    				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
    	}

    	//On récupère les enseignants génériques
    	for (EOEnseignantGenerique enseignantGenerique : listeEnseignantGenerique) {
    		listeEnseignants.add(new EnseignantDataBean(enseignantGenerique, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
    				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
    	}

    	return listeEnseignants;
    }

    /**
     * Retourne la liste des enseignants vacataires et statutaires de l'établissement
     *  en fonction des droits sur les cibles enseignant de l'utilisateur
     * @return liste EOActuelEneignant
     */
    public List<EOActuelEnseignant> getListeActuelEnseignants() {
    	EOQualifier qualifierEnseignants = null;
		EOQualifier qualifierStatutaire = null;
		EOQualifier qualifierVacataire = null;
    	List<EOActuelEnseignant> liste = new ArrayList<EOActuelEnseignant>();
    	boolean isFicheVacatairePrevisionnelUtilisation = EOPecheParametre.isFicheVacatairePrevisionnelUtilisation(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());

    	this.controleurStatutaires.setTypePopulation(getTypeFiche());

    	//Par défaut, les enseignants de la cible enseignants de l'utilisateur
    	if (getQualifierRepartitionCroisee() == null) {
    		if (isFicheVacatairePrevisionnelUtilisation) { 			
    			if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires()) {
    				qualifierStatutaire = this.controleurStatutaires.getListeEnseignants(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(),
							getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
    			}
    			
    			if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsVacataire()) {
    				qualifierVacataire = controleurVacataires.getListeEnseignants(getTypeFiche(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire(), null);
    			}
    			
    			qualifierEnseignants = ERXQ.or(qualifierStatutaire, qualifierVacataire);
    		} else {
    			if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires()) {
        			qualifierEnseignants = this.controleurStatutaires.getListeEnseignants(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(),
        					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
    			}
    		}
    	} else {
    		//S'il y a une répartition croisée, les enseignants filtrés suivant la recherche + vacataires (si activé)
    		if (isFicheVacatairePrevisionnelUtilisation) {
    			if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsVacataire()) {
    				qualifierVacataire = controleurVacataires.getListeEnseignants(getTypeFiche(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire(), null);
    			}
    			
    			qualifierEnseignants = ERXQ.or(getQualifierRepartitionCroisee(), qualifierVacataire);
    		} else {
    			qualifierEnseignants = getQualifierRepartitionCroisee();
    		}
    	} 
    	
    	if (qualifierEnseignants != null) {
	    	ERXFetchSpecification<EOActuelEnseignant> fetchSpec = EOActuelEnseignant.fetchSpec().qualify(qualifierEnseignants).sort(EOActuelEnseignant.getTriParDefaut());
	    	fetchSpec.setUsesDistinct(true);
	    	liste.addAll(fetchSpec.fetchObjects(edc()));
    	}
    	
    	return liste;
    }

    /**
     * @return liste des enseginants génériques statutaires ou vacataires 
     */
    public List<EOEnseignantGenerique> getListeEnseignantsGeneriques() {
    	List<EOEnseignantGenerique> listeEnseignantGenerique = new ArrayList<EOEnseignantGenerique>();
    	if (isTypeFichePrevisionnel()) {
	    	//On récupère les enseignants génériques statutaires
	    	if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires()) {
	    		listeEnseignantGenerique.addAll(EOEnseignantGenerique.getListeEnseignantGenerique(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
	        			getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(), false));
	    	}
	    	//On récupère les enseignants génériques vacataires
	    	if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsVacataire()) {
	    		listeEnseignantGenerique.addAll(EOEnseignantGenerique.getListeEnseignantGenerique(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
	        			getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire(), true));
	    	}
    	}
    	
    	return listeEnseignantGenerique;
    }
    
	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter.
	 *
	 * @return null.
	 */
	public WOActionResults filtrer() {
		EOQualifier qualifierNom = null;
		EOQualifier qualifierPrenom = null;
							
		if (!StringCtrl.isEmpty(tokenNom)) {
			qualifierNom = EnseignantDataBean.getQualifierEnseignantBeanFiltrerParNom(tokenNom);
		}		
	
		if (!StringCtrl.isEmpty(tokenPrenom)) {
			qualifierPrenom = EnseignantDataBean.getQualifierEnseignantBeanFiltrerParPrenom(tokenPrenom);
		} 
	
		filtrerDisplayGroup(ERXQ.and(qualifierNom, qualifierPrenom));
		
		return doNothing();
	}
	
    /**
	 * Permet de  filtrer le displayGroup par le nom
	 * @return null.
	 */
    public WOActionResults filtrerNom() {
    	if (!StringCtrl.isEmpty(tokenNom)) {
    		filtrerDisplayGroup(EnseignantDataBean.getQualifierEnseignantBeanFiltrerParNom(tokenNom));
		} else {
			filtrerDisplayGroup(null);
		}
    	
		tokenPrenom = "";
		return doNothing();
    }
    
    /**
	 * Permet de  filtrer le displayGroup par le prénom
	 * @return null.
	 */
    public WOActionResults filtrerPrenom() {
    	if (!StringCtrl.isEmpty(tokenPrenom)) {
    		filtrerDisplayGroup(EnseignantDataBean.getQualifierEnseignantBeanFiltrerParPrenom(tokenPrenom));
		} else {
			filtrerDisplayGroup(null);
		}
    	
    	tokenNom = "";
		return doNothing();
    }
    
    /**
     * @return controleur passé par Binding
     */
    public BasicPecheCtrl getControleur() {
    	if (controleur == null) {
    		controleur = (BasicPecheCtrl) valueForBinding("controleur");
		}
		return controleur;
	}

	/**
	 * Set le controleur
	 * @param controleur : controleur
	 */
	public void setControleur(BasicPecheCtrl controleur) {
		if (controleur == null) {
			throw new NullPointerException("Le controleur ne peut pas être nul");
		}
		this.controleur = controleur;
	}
	
	@Override
	public void setSelectedObject(EnseignantDataBean selectedObject) {
		super.setSelectedObject(selectedObject);
		getControleur().setEnseignantBean(selectedObject);
	}
	
	/**
	 * @return Le nombre de colonne affichées par AP
	 */
	public int getNbColonnesAffichees() {
		int nbColonne = 0;
		
		if (getListeAPs() != null) {
			nbColonne = getListeAPs().size();
		}
		
		return nbColonne;
	}
	
	/**
	 * Renvoie le nombre d'heures souhaitées par l'enseignant statutaire, 0 sinon pour chaque AP de l'UE/EC
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresSouhaitees() {
		double nbHeuresSouhaitees = 0;
		if (getCurrentItem().isStatutaire() && !getCurrentItem().isGenerique()) {
			nbHeuresSouhaitees = controleurStatutaires.getNbHeuresSouhaiteesParAP(getCurrentItem().getEnseignant(), getCurrentAP());
		}
		
		return nbHeuresSouhaitees;
	}
	
	/**
	 * Renvoie le nombre d'heures réalisées par l'enseignant statutaire, 0 sinon pour chaque AP de l'UE/EC
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresRealisees() {
		double nbHeuresRealisees = 0;
		if (getCurrentItem().isStatutaire() && !getCurrentItem().isGenerique()) {
			nbHeuresRealisees = controleurStatutaires.getNbHeuresRealiseesParAP(getCurrentItem().getEnseignant(), getCurrentAP());
		} else if (!getCurrentItem().isGenerique()){
			nbHeuresRealisees = controleurVacataires.getNbHeuresRealiseesParAP(getCurrentItem().getEnseignant(), getCurrentAP());
		}
		
		return nbHeuresRealisees;
	}
	
	/**
	 * On affiche la recherche pour la répartition croisée uniquement si l'utilisateur a une cible enseignant statutaire
	 * @return Vrai/faux
	 */
	public boolean isAffichageRepartitionCroisee() {
		return getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires();
	}
	
	public String getNomEnseignantToutesComposantes() {
		return nomEnseignantToutesComposantes;
	}

	public void setNomEnseignantToutesComposantes(String nomEnseignantToutesComposantes) {
		this.nomEnseignantToutesComposantes = nomEnseignantToutesComposantes;
	}

	public EOStructure getCurrentComposanteScolarite() {
		return currentComposanteScolarite;
	}

	public void setCurrentComposanteScolarite(EOStructure currentComposanteScolarite) {
		this.currentComposanteScolarite = currentComposanteScolarite;
	}

	public EOStructure getComposanteScolariteSelectionnee() {
		return composanteScolariteSelectionnee;
	}

	public void setComposanteScolariteSelectionnee(EOStructure composanteScolariteSelectionnee) {
		this.composanteScolariteSelectionnee = composanteScolariteSelectionnee;
	}
	
	public EOQualifier getQualifierRepartitionCroisee() {
		return qualifierRepartitionCroisee;
	}

	public void setQualifierRepartitionCroisee(EOQualifier qualifier) {
		this.qualifierRepartitionCroisee = qualifier;
	}

	public String getTokenNom() {
		return tokenNom;
	}

	public void setTokenNom(String tokenNom) {
		this.tokenNom = tokenNom;
	}

	public String getTokenPrenom() {
		return tokenPrenom;
	}

	public void setTokenPrenom(String tokenPrenom) {
		this.tokenPrenom = tokenPrenom;
	}

	/**
	 * @return liste des AP passés par binding
	 */
	@SuppressWarnings("unchecked")
	public List<EOAP> getListeAPs() {
		if (listeAPs == null) {
			listeAPs = (List<EOAP>) valueForBinding("listeAPs");
		}
		return listeAPs;
	}

	public void setListeAPs(List<EOAP> listeAPs) {
		this.listeAPs = listeAPs;
	}

	public EOAP getCurrentAP() {
		return currentAP;
	}

	public void setCurrentAP(EOAP currentAP) {
		this.currentAP = currentAP;
	}
	

	/**
	 * 
	 * @return liste codes étapes
	 */
	public NSArray<String> determinerListeEtapesFiltre() {
		NSArray<String> listeCodesEtapesFiltres = new NSMutableArray<String>();
		if (getFonctionRepartiteur()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_REPARTITEUR.getCodeEtape());
		}
		if (getFonctionPresident()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_PRESIDENT.getCodeEtape());
		}
		if (getFonctionDirecteurCollege()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_COLLEGE.getCodeEtape());
		}
		if (getFonctionDirecteurComposante()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_COMPOSANTE.getCodeEtape());
		}
		if (getFonctionDirecteurDepartement()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
		}
		if (getFonctionDrh()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DRH.getCodeEtape());
		}
		return listeCodesEtapesFiltres;
	}

	public String getContainerParent() {
		return containerParent;
	}

	public void setContainerParent(String containerParent) {
		this.containerParent = containerParent;
	}

}