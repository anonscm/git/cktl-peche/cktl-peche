package org.cocktail.peche.components.enseignants.repartitionCroisee;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.enseignants.CktlPecheFicheServiceEnseignantStatutaire;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Liste des enseignements du répartiteur pour lesquels il a besoin d'un enseignant
 * d'une autre composante
 * 
 * @author Chama LAATIK
 *
 */
public class ListeEnseignement  extends
	ListeRepartitionCroisee {

	private static final long serialVersionUID = 3340757101774233302L;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution.
	 */
	public ListeEnseignement(WOContext context) {
        super(context);
        EOQualifier qualifier = getListeEnseignements();
		ERXKey<EOIndividu> dotIndividu = EORepartEnseignant.SERVICE_DETAIL.dot(EOServiceDetail.SERVICE).dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU);
		ERXSortOrderings sortOrderings = dotIndividu.dot(EOIndividu.NOM_AFFICHAGE_KEY).asc()
				.then(dotIndividu.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).asc());
		ERXFetchSpecification<EORepartEnseignant> fetchSpec = EORepartEnseignant.fetchSpec().qualify(qualifier).sort(sortOrderings);
		fetchDisplay(fetchSpec);
    }
	
	/**
	 * Action pour ouvrir la fiche de service de l'enseignant
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ouvrirFicheService() {
		if (getSelectedObject() != null) {
			CktlPecheFicheServiceEnseignantStatutaire form = (CktlPecheFicheServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			EOActuelEnseignant enseignant = getSelectedObject().serviceDetail().service().enseignant();
			form.setEditedObject(enseignant);
			return form;
		} else {
			return null;
		}
	}
	
	
    /**
     * Retourne la fiche de service de l'enseignant.
     * @return la page.
     */
    public WOActionResults getLinkToFicheEnseignant() {
		if (getCurrentItem() != null) {
			CktlPecheFicheServiceEnseignantStatutaire form = (CktlPecheFicheServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			EOActuelEnseignant enseignant = getCurrentItem().serviceDetail().service().enseignant();
			form.setEditedObject(enseignant);
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return form;
		} else {
			return null;
		}
	} 

	/**
	 * Renvoie la liste des enseignements pour lesquels le répartiteur est responsable
	 * @return
	 */
	private EOQualifier getListeEnseignements() {
		EOIndividu individu = getPecheSession().getApplicationUser().getIndividu();
		
		EOQualifier qualifier = EORepartEnseignant.REPARTITEUR_DEMANDEUR.eq(individu);
		EOQualifier qualifierAnnee = EORepartEnseignant.SERVICE_DETAIL.dot(EOServiceDetail.SERVICE).dot(EOService.ANNEE_KEY).eq(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		return ERXQ.and(qualifier,qualifierAnnee);
		
	}
	
	/**
	 * @return l'état de la répartition s'il y en a une.
	 */
	public String getEtatRepartition() {
		String temoin = getCurrentItem().serviceDetail().etat();
		return getEtatRepartition(temoin);
	}
	
	/**
	 * @return Le libellé de la composant de l'enseignant
	 */
	public String getLibelleComposante() {
		EOStructure composante = getCurrentItem().serviceDetail().service().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = "?";
		
		if (composante != null) {
			nomComposante = composante.lcStructure();
		}

		return nomComposante;
	}
	
	/**
	 * @return Le libellé du département d'enseignement de l'enseignant
	 */
	public String getLibelleDepartementEnseignement() {
		EOStructure composante = getCurrentItem().serviceDetail().service().enseignant().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = "";
		
		if (composante != null) {
			nomComposante = composante.lcStructure();
		}

		return nomComposante;
	}

	/**
	 * Renvoie le composant parent direct de l'AP
	 * @return composant
	 */
	public String affichageEnseignement() {
		return ComposantAffichageHelper.getInstance().affichageEnseignementParent(getCurrentItem().serviceDetail().composantAP());
	}
	
	/**
	 * 
	 * @return bouton Fiche de service inactif ?
	 */
	public boolean boutonFicheServiceInactif() {
		return this.getSelectedObject() == null;
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.MES_COURS.getId();
	}
	
	
	
}