package org.cocktail.peche.components.enseignants.repartitionCroisee;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * Composant de recherche pour les enseignants statutaires 
 * 	==> soit par nom soit par composante
 * 
 * @author Chama LAATIK
 */
public class RechercheEnseignantToutesComposantes extends CktlPecheBaseComponent {
	
	private static final long serialVersionUID = 3885133904209507910L;
	private String nomEnseignantToutesComposantes;
	private IStructure currentComposanteScolarite;
	private IStructure composanteScolariteSelectionnee;
	
	private EnseignantsStatutairesCtrl controleur;
	private static String BINDING_QUALIFIER = "qualifier";
	private EOQualifier qualifier;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'éxécution.
	 */
    public RechercheEnseignantToutesComposantes(WOContext context) {
        super(context);
        this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    }
    
    /**
	 * Cette méthode permet de voir les enseignants de toutes les composantes
	 * filtré par le nom ou composante
	 *
	 * @return null.
	 */
    public WOActionResults filtrerTousEnseignants() {
		if (StringCtrl.isEmpty(nomEnseignantToutesComposantes) && composanteScolariteSelectionnee == null) {
			effacerFiltre();
		} else {
			EOQualifier qualifierGlobal = this.controleur.getListeEnseignantsStatutairesParNomEtStructure(nomEnseignantToutesComposantes, 
					getPecheSession().getStructures(composanteScolariteSelectionnee),
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			setQualifier(qualifierGlobal);
		}
		
		return null;
	}
    
    /**
	 * Cette méthode efface le filtre et remet le displayGroup en mode restreint
	 * aux structures auxquelles il a droit
	 *
	 * @return null.
	 */
    public WOActionResults effacerFiltre() {
		setNomEnseignantToutesComposantes("");
		setComposanteScolariteSelectionnee(null);
		
		setQualifier(this.controleur.getListeEnseignants(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
		return null;
    }
    
    public EOQualifier getQualifier() {
		return qualifier;
	}

    /**
     * @param qualifier : qualifier à passer au parent du composant
     */
	public void setQualifier(EOQualifier qualifier) {
		setValueForBinding(qualifier, BINDING_QUALIFIER);
	}
    
    public List<IStructure> getListeComposantesScolarite() {
		return getPecheSession().getPecheAutorisationCache().getComposantesScolaritePourRepartCroisee(getPecheSession());		
	}

	public String getNomEnseignantToutesComposantes() {
		return nomEnseignantToutesComposantes;
	}

	public void setNomEnseignantToutesComposantes(String nomEnseignantToutesComposantes) {
		this.nomEnseignantToutesComposantes = nomEnseignantToutesComposantes;
	}

	public IStructure getCurrentComposanteScolarite() {
		return currentComposanteScolarite;
	}

	public void setCurrentComposanteScolarite(IStructure currentComposanteScolarite) {
		this.currentComposanteScolarite = currentComposanteScolarite;
	}

	public IStructure getComposanteScolariteSelectionnee() {
		return composanteScolariteSelectionnee;
	}

	public void setComposanteScolariteSelectionnee(IStructure composanteScolariteSelectionnee) {
		this.composanteScolariteSelectionnee = composanteScolariteSelectionnee;
	}
}