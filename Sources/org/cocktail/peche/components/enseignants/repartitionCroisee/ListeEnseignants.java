package org.cocktail.peche.components.enseignants.repartitionCroisee;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.enseignants.CktlPecheFicheServiceEnseignantStatutaire;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Liste des enseignants du répartiteur qui ont une demande d'affectation
 * dans une autre composante que celle de leur affectation
 * 
 * @author Chama LAATIK
 *
 */
public class ListeEnseignants extends ListeRepartitionCroisee {

	private static final long serialVersionUID = 4120650932720829995L;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution.
	 */
	public ListeEnseignants(WOContext context) {
        super(context);
        EOQualifier qualifier = getListeEnseignants();
		ERXKey<EOIndividu> dotIndividu = EORepartEnseignant.SERVICE_DETAIL.dot(EOServiceDetail.SERVICE).dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU);
		ERXSortOrderings sortOrderings = dotIndividu.dot(EOIndividu.NOM_AFFICHAGE_KEY).asc()
				.then(dotIndividu.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).asc());
		ERXFetchSpecification<EORepartEnseignant> fetchSpec = EORepartEnseignant.fetchSpec().qualify(qualifier).sort(sortOrderings);
		fetchDisplay(fetchSpec);
    }

	/**
	 * Renvoie la liste des enseignants dont le répartiteur est le répartiteur courant
	 * @return qualifier
	 */
	protected EOQualifier getListeEnseignants() {
		List<IStructure> listeStructures = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
		
		EOQualifier qualifier = EORepartEnseignant.COMPOSANTE_REFERENTE.in(getListHelper().bidouilleListeStructurePourWO(listeStructures));
		EOQualifier qualifierAnnee = EORepartEnseignant.SERVICE_DETAIL.dot(EOServiceDetail.SERVICE).dot(EOService.ANNEE_KEY).eq(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		return ERXQ.and(qualifier, qualifierAnnee);
	}
	
	/**
	 * Action pour ouvrir la fiche de service de l'enseignant
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ouvrirFicheService() {
		if (getSelectedObject() != null) {
			CktlPecheFicheServiceEnseignantStatutaire form = (CktlPecheFicheServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			EOActuelEnseignant enseignant = getSelectedObject().serviceDetail().service().enseignant();
			form.setEditedObject(enseignant);
			return form;
		} else {
			return null;
		}
	} 
	
	
    /**
     * Retourne la fiche de service de l'enseignant.
     * @return la page.
     */
    public WOActionResults getLinkToFicheEnseignant() {
		if (getCurrentItem() != null) {
			CktlPecheFicheServiceEnseignantStatutaire form = (CktlPecheFicheServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			EOActuelEnseignant enseignant = getCurrentItem().serviceDetail().service().enseignant();
			form.setEditedObject(enseignant);
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return form;
		} else {
			return null;
		}
	} 
	
	/**
	 * @return l'état de la répartition croisée s'il y en a une.
	 */
	public String getEtatRepartition() {
		String temoin = getCurrentItem().serviceDetail().etat();
		return getEtatRepartition(temoin);
	}
	
	/**
	 * Renvoie le composant parent direct de l'AP
	 * @return composant
	 */
	public String affichageEnseignement() {
		return ComposantAffichageHelper.getInstance().affichageEnseignementParent(getCurrentItem().serviceDetail().composantAP());
	}
	
	/**
	 * Renvoie la composante du parent direct de l'AP
	 * @return composante du parent
	 */
	public String getComposanteParentAP() {
		return getComposantCtrl().getComposanteParentAP(getCurrentItem().serviceDetail());
	}
	
	/**
	 * 
	 * @return true si bouton fiche de service désactivé ?
	 */
	public boolean boutonFicheServiceInactif() {
		return this.getSelectedObject() == null;
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.MES_ENSEIGNANTS.getId();
	}
	
	
}