package org.cocktail.peche.components.enseignants.repartitionCroisee;

import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.entity.EORepartEnseignant;

import com.webobjects.appserver.WOContext;

public class ListeRepartitionCroisee extends CktlPecheTableComponent<EORepartEnseignant> {

	private ComposantCtrl composantCtrl;
	
	public ListeRepartitionCroisee(WOContext context) {
		super(context);
		this.composantCtrl = new ComposantCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}


	public ComposantCtrl getComposantCtrl() {
		return composantCtrl;
	}

	
	
}
