package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.IEnseignantsVacatairesCtrl;
import org.cocktail.peche.components.enseignants.services.IServiceOngletEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.google.inject.Inject;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public abstract class CktlPecheTableRepartitionEnseignantVacataireOnglet extends CktlPecheTableRepartitionEnseignant {

	
	private EOService service;
	private EORepartService repartService;
	
	@Inject
	private IServiceOngletEnseignant serviceOngletEnseignantVacataire;
	
	
	public CktlPecheTableRepartitionEnseignantVacataireOnglet(WOContext context) {
		super(context);
	}
	
	@Override
	public void awake() {
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
        service = (EOService) valueForBinding("service");

        String pathService = ERXQ.keyPath(EOServiceDetail.SERVICE_KEY);
        if (getTableKey() != null) {

        	EOQualifier qualifier =
        			ERXQ.and(
        					ERXQ.equals(pathService, service),
        					ERXQ.isNotNull(getTableKey())
        					);

        	((ERXDatabaseDataSource) dg.dataSource()).setAuxiliaryQualifier(qualifier);
        	dg.fetch();

        }

    }
	
	public EOService getService() {
		return service;
	}
	
	protected abstract String getTableKey();
	
	/**
	 * 
	 * @return true or false
	 */
	public boolean afficherLigneRepartition() {
		// si le repartService du service detail en cours est le même que celui sélectionné via les onglets
		// ou
		// si nous sommes sur l'onglet Tous
		// on affiche le service detail
		if (getCurrentItem()!=null && getCurrentItem().toRepartService() != null && getCurrentItem().toRepartService().equals(getRepartService()) || !estPasOngletTous()) {
			return true;
		}

		return false;
	}

	
	
	
	
	/**
	 * Est-ce que les boutons ajout, modifier, supprimer sous le bloc "Répartition REH hors offre de formation" doivent être présents  ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean hasDroitUtilisationRepartitionRehVacataire() {
		return getAutorisation().hasDroitUtilisationSaisieRehVacataire();
	}
	
	/**
	 * @return
	 */
	public boolean isMemeComposanteOnglet() {
		return getServiceOngletEnseignantVacataire().estOngletSelectionneDansListeStructure(this.getOngletComposanteSelectionne(), 
				getPecheSession().getPecheAutorisationCache().getStructuresPourVacataires(getControleur(), getService()));
	}
	
	public abstract IEnseignantsVacatairesCtrl getControleur();

	

	/**
	 * 
	 * @return repartService
	 */
	public EORepartService getRepartService() {
		if (repartService == null && this.getOngletComposanteSelectionne() != null) {
			EOQualifier qualifier = ERXQ.and(ERXQ.equals(EORepartService.TO_STRUCTURE_KEY, serviceOngletEnseignantVacataire.recupererStructureOngletSelectionne(getOngletComposanteSelectionne())),
					ERXQ.equals(EORepartService.TO_SERVICE_KEY, getService()));
			
			this.repartService = EORepartService.fetchEORepartService(edc(), qualifier);
		}
		return repartService;
	}
	
	
	
	
	
	public IServiceOngletEnseignant getServiceOngletEnseignantVacataire() {
		return serviceOngletEnseignantVacataire;
	}
	
	
	/**
	 * @return the boutonsInactifsModifierRepartitionReh
	 */
	public boolean boutonInactifModifierRepartitionReh() {
		//Si la personne n'est pas de la composante mais a le droit de saisir les heures à payer
		// => on autorise la modification
		return !hasDroitUtilisationRepartitionRehVacataire() 
				|| !(isMemeComposanteOnglet() || getAutorisation().hasDroitUtilisationSaisieHeuresAPayer());
	}
	
	public boolean boutonInactifModifierRepartitionService() {
		return !(isMemeComposanteOnglet() || getAutorisation().hasDroitUtilisationSaisieHeuresAPayer());
	}
	
	public boolean getBoutonsInactifsRepartitionReh() {
		return !hasDroitUtilisationRepartitionRehVacataire() || !isMemeComposanteOnglet();
	}
	
	public boolean getBoutonsInactifsRepartitionService() {
		return !(isMemeComposanteOnglet());
	}
	
	public boolean getBoutonsActifsRepartitionService() {
		return getBoutonsInactifsRepartitionService();
	}
	
	public boolean getBoutonModifierActifRepartitionService() {
		return boutonInactifModifierRepartitionService() || getSelectedObject() == null;
	}
	
	public boolean getBoutonSupprimerActifRepartitionService() {
		return getBoutonsActifsRepartitionService() || getSelectedObject() == null;
	}
	
	public boolean getBoutonsActifsRepartitionReh() {
		return getBoutonsInactifsRepartitionReh();
	}
	
	public boolean getBoutonModifierActifRepartitionReh() {
		return boutonInactifModifierRepartitionReh() || getSelectedObject() == null;
	}
	
	public boolean getBoutonSupprimerActifRepartitionReh() {
		return getBoutonsActifsRepartitionReh() || getSelectedObject() == null;
	}
	
	/**
	 * 
	 * @return
	 */
	protected NSArray<EOServiceDetail> listerServiceDetailParRepartService() {
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
	    NSArray<EOServiceDetail> listeServiceDetail =  dg.allObjects();
	    NSArray<EOServiceDetail> listeServiceDetailSortie =  new NSMutableArray<EOServiceDetail>();
	    
	    for (EOServiceDetail serviceDetail : listeServiceDetail) {
			if (getRepartService() == null || (getRepartService() != null && getRepartService().equals(serviceDetail.toRepartService()))) {
				listeServiceDetailSortie.add(serviceDetail);
			}
		}
	    
	    return listeServiceDetailSortie;
	    
	}
	
	
}
