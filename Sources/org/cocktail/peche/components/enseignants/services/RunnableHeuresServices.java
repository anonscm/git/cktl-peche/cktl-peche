package org.cocktail.peche.components.enseignants.services;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOEnseignantGenerique;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.concurrency.ERXTaskObjectStoreCoordinatorPool;
import er.extensions.eof.ERXDatabaseContextDelegate.ObjectNotAvailableException;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXExistsQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;
import er.extensions.qualifiers.ERXOrQualifier;

/**
 * Classe lancée de façon asynchrone qui contrôle
 * et met à jour les heures des services
 * pour les fiches de voeux et les fiches de service
 *  
 *  * @author Laurent Pringot
 *
 */
public class RunnableHeuresServices implements Runnable {

	private EOEditingContext editingContext = null;
	private Integer createurId = null;
	private Integer anneeUniversitaireEnCours = new Integer(2014);
	private EOObjectStoreCoordinator osc;


	private static final Logger log = Logger.getLogger(RunnableHeuresServices.class);


	public RunnableHeuresServices(Integer creatorId,Integer anneeUniv) {
		super();
		createurId = creatorId;
		anneeUniversitaireEnCours = anneeUniv;		
	}

	public void run() {
		try {
			log.info("**** BEGIN Execution process HeuresServices " + Thread.currentThread().getName());

			editingContext = newEditingContextForJobs();
			editingContext.lock();
			log.info("**** Start process HeuresServices annee" + anneeUniversitaireEnCours);
			traitementDonnees(anneeUniversitaireEnCours, true);

			if (((CktlWebApplication) WOApplication.application()).config().booleanForKey("org.cocktail.peche.async.heures.traitement.annee.moins.1")) {			
				log.info("**** Start process HeuresServices annee" + (anneeUniversitaireEnCours - 1));
				traitementDonnees(anneeUniversitaireEnCours - 1, false);
			}

			log.info("**** Start process HeuresServices annee" + (anneeUniversitaireEnCours + 1));
			traitementDonnees(anneeUniversitaireEnCours + 1, true);

			log.info("**** END Execution process HeuresServices " + Thread.currentThread().getName());
		} catch (Exception e) {
			log.info(e);
			e.printStackTrace();
		} finally {			
			editingContext.unlock();			
		}

		editingContext.invalidateAllObjects();

		editingContext.dispose();
		editingContext = null;
		Runtime.getRuntime().gc();

	}

	private void traitementDonnees(Integer annee,boolean traitementFicheVoeux) {

		NSTimestamp now = new NSTimestamp();
		HceCtrl hceCtrl = new HceCtrl(editingContext);
		IPecheParametres pecheParametres = new PecheParametres(editingContext);

		EOPersonne createur = EOPersonne.fetchByKeyValue(editingContext, EOPersonne.PERS_ID_KEY, createurId);

		// Fiche voeux
		if (traitementFicheVoeux && EOPecheParametre.isFicheVoeuxUtilisation(editingContext, annee)) {
			log.info("TRAITEMENT Fiche voeux " + annee);
			EOQualifier qualifierEnseignantVoeux = getListeEnseignantsStatutaires(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), false);
			NSArray<EOActuelEnseignant> enseignantsStatutairesVoeux = EOActuelEnseignant.fetchEOActuelEnseignants(editingContext, qualifierEnseignantVoeux, EOActuelEnseignant.getTriParDefaut());
			log.info("NB voeux " + annee + " : " + enseignantsStatutairesVoeux.size());

			for (EOActuelEnseignant enseignantVoeux : enseignantsStatutairesVoeux) {
				NSArray<EOFicheVoeux> listeFichesVoeux = enseignantVoeux.listeFichesVoeux(EOFicheVoeux.ANNEE.eq(annee));

				if (listeFichesVoeux.size() > 0) {

					EOFicheVoeux ficheVoeux = listeFichesVoeux.get(0);

					boolean etatbascule = false;

					EODemande demande = ficheVoeux.toDemande();

					if (demande != null) {
						String etatEtape = EtapePeche.getEtape(demande.toEtape()).getEtatDemande();
						if (EtapePeche.VALIDEE.getEtatDemande().equals(etatEtape)) {
							etatbascule = true;
						}
					}
					double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(enseignantVoeux, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
					double serviceSouhaitees = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceSouhaite(enseignantVoeux, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), ficheVoeux, serviceDu));

					boolean modificationEnCours = false;

					if (ficheVoeux.heuresServiceDu() == null) {
						/*				log.info("ATTENTION CHANGEMENT VOEUX annee:"+annee
									+" enseignant statutaire:"+enseignantVoeux.toIndividu().nomAffichage()
									+ " "+enseignantVoeux.toIndividu().prenomAffichage()
									+ " service du trouvé :"+serviceDu
									+ " service du en base :" + ficheVoeux.heuresServiceDu()
									);*/
						ficheVoeux.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
						modificationEnCours = true;
					} else if (!etatbascule && ficheVoeux.heuresServiceDu().compareTo(BigDecimal.valueOf(serviceDu)) != 0) {
						StringBuffer message = new StringBuffer("CHANGEMENT VOEUX annee:").append(annee);
						message.append(" enseignant statutaire:").append(enseignantVoeux.toIndividu().nomAffichage());
						message.append(" ").append(enseignantVoeux.toIndividu().prenomAffichage());
						message.append(" (").append(enseignantVoeux.toIndividu().persId()).append(")");
						message.append(" service Du de ").append(ficheVoeux.heuresServiceDu());
						message.append(" a ").append(serviceDu);
						log.info(message.toString());

						StringBuffer commentaires = new StringBuffer();
						commentaires.append(DateCtrl.dateToString(now));
						commentaires.append(": Changement Service Du de ").append(ficheVoeux.heuresServiceDu());
						commentaires.append(" à ").append(serviceDu);
						ficheVoeux.addSuivi(commentaires.toString());

						ficheVoeux.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
						modificationEnCours = true;															
					}

					if (ficheVoeux.heuresSouhaitees() == null) {
						/*log.info("ATTENTION CHANGEMENT VOEUX annee:"+annee
									+" enseignant statutaire:"+enseignantVoeux.toIndividu().nomAffichage()
									+ " "+enseignantVoeux.toIndividu().prenomAffichage()
									+ " service souhaitees trouvé :"+serviceSouhaitees
									+ " service souhaitees en base :" + ficheVoeux.heuresSouhaitees()
									);*/
						ficheVoeux.setHeuresSouhaitees(BigDecimal.valueOf(serviceSouhaitees));
						modificationEnCours = true;
					} else if (!etatbascule && ficheVoeux.heuresSouhaitees().compareTo(BigDecimal.valueOf(serviceSouhaitees)) != 0) {

						StringBuffer message = new StringBuffer("CHANGEMENT VOEUX annee:").append(annee);
						message.append(" enseignant statutaire:").append(enseignantVoeux.toIndividu().nomAffichage());
						message.append(" ").append(enseignantVoeux.toIndividu().prenomAffichage());
						message.append(" (").append(enseignantVoeux.toIndividu().persId()).append(")");
						message.append(" heures souhaitees de ").append(ficheVoeux.heuresSouhaitees());
						message.append(" a ").append(serviceSouhaitees);
						log.info(message.toString());

						StringBuffer commentaires = new StringBuffer();
						commentaires.append(DateCtrl.dateToString(now));
						commentaires.append(": Changement heures souhaitées de ").append(ficheVoeux.heuresSouhaitees());
						commentaires.append(" à ").append(serviceSouhaitees);
						ficheVoeux.addSuivi(commentaires.toString());						
						ficheVoeux.setPersonneModification(createur);									
						ficheVoeux.setDateModification(now);		
						ficheVoeux.setHeuresSouhaitees(BigDecimal.valueOf(serviceSouhaitees));
						modificationEnCours = true;															
					}

					if (modificationEnCours) {
						try {
							editingContext.saveChanges();
						} catch (ValidationException e) {
							editingContext.invalidateAllObjects();
							editingContext.revert(); 
							log.info("erreur:" + e);
						}
					}

				} else {
					double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(enseignantVoeux, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
					/*	log.info("CREATION Fiche VOEUX annee:"+annee
								+" enseignant statutaire:"+enseignantVoeux.toIndividu().nomAffichage()
								+ " "+enseignantVoeux.toIndividu().prenomAffichage()
								+ " service du :"+serviceDu
								);
					 */					
					//creation fiche de voeux
					EOFicheVoeux ficheVoeux = EOFicheVoeux.creerEtInitialiser(editingContext, createur.persId());
					try {
						ficheVoeux.setEnseignant(enseignantVoeux);
						ficheVoeux.setTemoinValide(Constante.OUI);
						ficheVoeux.setPersonneCreation(createur);
						ficheVoeux.setDateCreation(now);
						ficheVoeux.setPersonneModification(createur);									
						ficheVoeux.setDateModification(now);		
						ficheVoeux.setAnnee(annee);
						ficheVoeux.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
						editingContext.saveChanges();
					} catch (ValidationException e) {
						log.error("INCIDENT creation fiche voeux persId:" + enseignantVoeux.toIndividu().persId() + " " + e);
						editingContext.revert();
					}
				}
			}
		}

		log.info("TRAITEMENT Fiche services EXISTANTES " + annee);
		EOQualifier qualifierService = EOService.ANNEE.eq(annee);            
		NSArray<EOService> services = EOService.fetchEOServices(editingContext, qualifierService, null);

		log.info("NB Fiches services annee " + annee + " : " + services.size());

		for (EOService service : services) {
			EOActuelEnseignant enseignantServices = service.enseignant();
			EOEnseignantGenerique enseignantGenerique = service.toEnseignantGenerique();
			try {
				if (enseignantServices != null) {
					if (enseignantServices.isStatutaire(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile())) {						
						double serviceStatutaire = HceCtrl.arrondirAuCentieme(enseignantServices.getNbHeuresServiceStatutaire(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
						double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
						double serviceAttribue = hceCtrl.getServiceAttribue(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), service, serviceDu);
						double serviceRealise = hceCtrl.getServiceRealise(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), service, serviceDu);                     		

						log.debug(" annee:" + service.annee()
									+ " enseignant statutaire:" + enseignantServices.toIndividu().nomAffichage()
									+ " " + enseignantServices.toIndividu().prenomAffichage()
									+ " service :" + serviceStatutaire
									+ " service du:" + serviceDu
									+ " service attribue:" + serviceAttribue
									+ " service realise:" + serviceRealise               		
									);

						boolean modificationEnCours = false;
						boolean initialisationCircuit = false;

						if (service.heuresService() == null) {
							service.setHeuresService(BigDecimal.valueOf(serviceStatutaire));
							modificationEnCours = true;
						} else if (service.heuresService().compareTo(BigDecimal.valueOf(serviceStatutaire)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant statutaire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" service statutaire de ").append(service.heuresService());
							message.append(" a ").append(serviceStatutaire);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement service statutaire de ").append(service.heuresService());
							commentaires.append(" à ").append(serviceStatutaire);
							service.addSuivi(commentaires.toString());

							service.setHeuresService(BigDecimal.valueOf(serviceStatutaire));
							modificationEnCours = true;							
							initialisationCircuit = true;
						}

						if (service.heuresServiceDu() == null) {
							service.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
							modificationEnCours = true;
						} else if (service.heuresServiceDu().compareTo(BigDecimal.valueOf(serviceDu)) != 0) {

							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant statutaire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" service Du de ").append(service.heuresServiceDu());
							message.append(" a ").append(serviceDu);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement service Du de ").append(service.heuresServiceDu());
							commentaires.append(" à ").append(serviceDu);
							service.addSuivi(commentaires.toString());

							service.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
							modificationEnCours = true;
							initialisationCircuit = true;
						}
						if (service.heuresServiceAttribue() == null) {
							service.setHeuresServiceAttribue(BigDecimal.valueOf(serviceAttribue));
							modificationEnCours = true;
						} else if (service.heuresServiceAttribue().compareTo(BigDecimal.valueOf(serviceAttribue)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant statutaire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" heures attribuees de ").append(service.heuresServiceAttribue());
							message.append(" a ").append(serviceAttribue);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement heures attribuées de :").append(service.heuresServiceAttribue());
							commentaires.append(" à ").append(serviceAttribue);
							service.addSuivi(commentaires.toString());

							service.setHeuresServiceAttribue(BigDecimal.valueOf(serviceAttribue));
							modificationEnCours = true;
						}
						
						if (service.heuresServiceRealise() == null) {
							service.setHeuresServiceRealise(BigDecimal.valueOf(serviceRealise));
							modificationEnCours = true;
						} else if (service.heuresServiceRealise().compareTo(BigDecimal.valueOf(serviceRealise)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant statutaire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" heures realisees de ").append(service.heuresServiceRealise());
							message.append(" a ").append(serviceRealise);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement heures réalisées de ").append(service.heuresServiceRealise());
							commentaires.append(" à ").append(serviceRealise);
							service.addSuivi(commentaires.toString());

							service.setHeuresServiceRealise(BigDecimal.valueOf(serviceRealise));
							modificationEnCours = true;
						}

						if (modificationEnCours) {
							try {
								service.setPersonneModification(createur);									
								service.setDateModification(now);				
								editingContext.saveChanges();
							} catch (ValidationException e) {
								editingContext.invalidateAllObjects();
								editingContext.revert();
								log.error("INCIDENT maj fiche service statutaire persId:" + enseignantServices.toIndividu().persId() + " " + e);
							}
						}

						if (initialisationCircuit && (!service.toListeRepartService().isEmpty())) {
							EODemande demande = service.toListeRepartService().get(0).toDemande();
							if ((demande != null) && (!demande.estSurEtapeInitiale())) {
								try {
									service.toListeRepartService().get(0).setTemIncident(Constante.OUI);
									//retour arrière : mise en place Incident
									/*StringBuffer commentaires = new StringBuffer();
									commentaires.append(DateCtrl.dateToString(now));
									commentaires.append(": Circuit de validation réinitialisé");
									service.addCommentaire(commentaires.toString());
									demande.reinitialiser();
									service.setPersonneModification(createur);									
									service.setDateModification(now);*/				
									editingContext.saveChanges();

								} catch (ValidationException e) {
									editingContext.invalidateAllObjects();
									editingContext.revert();
									log.error("INCIDENT temoin incident circuit fiche service statutaire persId:" + enseignantServices.toIndividu().persId() + " " + e);
								}
							}

						}

					} else {
						double heuresContrat = HceCtrl.arrondirAuCentieme(hceCtrl.getTotalNbHeuresVacation(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
						double serviceAttribue = hceCtrl.getServiceAttribue(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), service, heuresContrat);
						double serviceRealise = hceCtrl.getServiceRealise(enseignantServices, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), service, heuresContrat);                        		

								log.debug(" annee:" + service.annee()
									+ " enseignant vacataire :" + enseignantServices.toIndividu().nomAffichage()
									+ " " + enseignantServices.toIndividu().prenomAffichage()
									+ " heures contrat:" + heuresContrat
									+ " service attribue:" + serviceAttribue
									+ " service realise:" + serviceRealise                        		
									);    
						boolean modificationEnCours = false;
						boolean initialisationCircuit = false;

						if (service.heuresService() == null) {
							service.setHeuresService(BigDecimal.valueOf(heuresContrat));
							modificationEnCours = true;
						} else if (service.heuresService().compareTo(BigDecimal.valueOf(heuresContrat)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant vacataire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" heures service de ").append(service.heuresService());
							message.append(" a ").append(heuresContrat);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement service de ").append(service.heuresService());
							commentaires.append(" à ").append(heuresContrat);
							service.addSuivi(commentaires.toString());

							service.setHeuresService(BigDecimal.valueOf(heuresContrat));
							modificationEnCours = true;
							initialisationCircuit = true;
						}
						if (service.heuresServiceAttribue() == null) {
							service.setHeuresServiceAttribue(BigDecimal.valueOf(serviceAttribue));
							modificationEnCours = true;
						} else if (service.heuresServiceAttribue().compareTo(BigDecimal.valueOf(serviceAttribue)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant vacataire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" heures service attribue de ").append(service.heuresServiceAttribue());
							message.append(" a ").append(serviceAttribue);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement service attribué de ").append(service.heuresServiceAttribue());
							commentaires.append(" à ").append(serviceAttribue);
							service.addSuivi(commentaires.toString());

							service.setHeuresServiceAttribue(BigDecimal.valueOf(serviceAttribue));
							modificationEnCours = true;
						}

						if (service.heuresServiceRealise() == null) {
							service.setHeuresServiceRealise(BigDecimal.valueOf(serviceRealise));
							modificationEnCours = true;
						} else if (service.heuresServiceRealise().compareTo(BigDecimal.valueOf(serviceRealise)) != 0) {
							StringBuffer message = new StringBuffer("CHANGEMENT SERVICE annee:").append(annee);
							message.append(" enseignant vacataire:").append(enseignantServices.toIndividu().nomAffichage());
							message.append(" ").append(enseignantServices.toIndividu().prenomAffichage());
							message.append(" (").append(enseignantServices.toIndividu().persId()).append(")");
							message.append(" heures service realise de ").append(service.heuresServiceRealise());
							message.append(" a ").append(serviceRealise);
							log.info(message.toString());

							StringBuffer commentaires = new StringBuffer();
							commentaires.append(DateCtrl.dateToString(now));
							commentaires.append(": Changement service realisé de ").append(service.heuresServiceRealise());
							commentaires.append(" à ").append(serviceRealise);
							service.addSuivi(commentaires.toString());

							service.setHeuresServiceRealise(BigDecimal.valueOf(serviceRealise));
							modificationEnCours = true;
						}
						if (modificationEnCours) {
							try {
								service.setPersonneModification(createur);									
								service.setDateModification(now);				
								editingContext.saveChanges();
							} catch (ValidationException e) {
								editingContext.revert();
								log.error("INCIDENT maj fiche service vacataire persId:" + enseignantServices.toIndividu().persId() + " " + e);
							}
						}

						if (initialisationCircuit && (!service.toListeRepartService().isEmpty())) {
							NSArray<EORepartService> repartServices = service.toListeRepartService();
							for (EORepartService repartService : repartServices) {
								EODemande demande = repartService.toDemande();
								if ((demande != null) && (!demande.estSurEtapeInitiale())) {
									try {
										repartService.setTemIncident(Constante.OUI);		
										//retour arrière : mise en place Incident
										/*StringBuffer commentaires = new StringBuffer();
										commentaires.append(DateCtrl.dateToString(now));
										commentaires.append(": Circuit de validation réinitialisé");
										repartService.addCommentaire(commentaires.toString());
										demande.reinitialiser();
										repartService.setPersonneModification(createur);									
										repartService.setDateModification(now);*/									
										editingContext.saveChanges();
									} catch (ValidationException e) {
										editingContext.invalidateAllObjects();
										editingContext.revert();
										log.error("INCIDENT temoin incident fiche service vacataire persId:" + enseignantServices.toIndividu().persId() + " " + e);
									}
								}
							}
						}

					}
				}
				if (enseignantGenerique != null) {
					// NON TRAITEMENT DES GENERIQUES
				}
			} catch (ObjectNotAvailableException erx) {
				// non disponible , exception ignorée
			}
		}

		if (!EOPecheParametre.isFicheVoeuxUtilisation(editingContext, annee)) {

			log.info("TRAITEMENT reste Fiche services NON EXISTANTES Statutaires" + annee);
			EOQualifier qualifierEnseignantStatutaires = getListeEnseignantsStatutaires(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile(), true); 
			NSArray<EOActuelEnseignant> enseignantsStatutaires = EOActuelEnseignant.fetchEOActuelEnseignants(editingContext, qualifierEnseignantStatutaires, EOActuelEnseignant.getTriParDefaut());

			log.info("liste statutaires :" + enseignantsStatutaires.size());

			for (EOActuelEnseignant enseignantStatutaire : enseignantsStatutaires) {
				if (enseignantStatutaire.listeServices(EOService.ANNEE.eq(annee)).size() == 0) {					
					log.info("enseignant statutaire SANS Fiche service :" + enseignantStatutaire.toIndividu().nomAffichage()
							+ " (" + enseignantStatutaire.toIndividu().persId() + ")");

					EOService service = EOService.creerEtInitialiser(editingContext);
					try {
						service.setPersonneCreation(createur);
						service.setDateCreation(now);
						service.setTemoinEnseignantGenerique("N");
						service.setTemoinValide("O");
						service.setDateModification(now);
						service.setEnseignant(enseignantStatutaire);
						service.setAnnee(annee);
						double serviceStatutaire = HceCtrl.arrondirAuCentieme(enseignantStatutaire.getNbHeuresServiceStatutaire(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
						double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(enseignantStatutaire, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
						service.setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
						service.setHeuresService(BigDecimal.valueOf(serviceStatutaire));
						StringBuffer message = new StringBuffer("CREATION SERVICE annee:").append(annee);
						message.append(" enseignant statutaire:").append(enseignantStatutaire.toIndividu().nomAffichage());
						message.append(" ").append(enseignantStatutaire.toIndividu().prenomAffichage());
						message.append(" (").append(enseignantStatutaire.toIndividu().persId()).append(")");
						message.append(" service statutaire :").append(serviceStatutaire);
						message.append(" service du :").append(serviceDu);
						log.info(message.toString());

						EORepartService.creerNouvelRepartService(editingContext, service, null, createur,
								CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), annee);
						
						editingContext.saveChanges();
					} catch (ValidationException e) {
						editingContext.revert();
						log.error("INCIDENT creation fiche service statutaire persId:" + enseignantStatutaire.toIndividu().persId() + " " + e);
					}
				}
			}

		}

		log.info("TRAITEMENT reste Fiche services NON EXISTANTES Vacataires" + annee);

		EOStructure etablissement = EOStructure.rechercherEtablissement(editingContext);
		
		EOQualifier qualifierEnseignantVacataires = getListeEnseignantsVacatairesSansFicheService(annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()); 
		NSArray<EOActuelEnseignant> enseignantsVacataires = EOActuelEnseignant.fetchEOActuelEnseignants(editingContext, qualifierEnseignantVacataires, EOActuelEnseignant.getTriParDefaut());

		log.info("liste vacataires :" + enseignantsVacataires.size());

		for (EOActuelEnseignant enseignantVacataire : enseignantsVacataires) {
			if (enseignantVacataire.listeServices(EOService.ANNEE.eq(annee)).size() == 0) {
				log.info("Sans Fiche enseignant vacataire :" + enseignantVacataire.toIndividu().nomAffichage()
						+ " (" + enseignantVacataire.toIndividu().persId() + ")");

				EOService service = EOService.creerEtInitialiser(editingContext);
				try {
					service.setPersonneCreation(createur);
					service.setDateCreation(now);
					service.setTemoinEnseignantGenerique("N");
					service.setTemoinValide("N");
					service.setDateModification(now);
					service.setEnseignant(enseignantVacataire);
					service.setAnnee(annee);
					double heuresContrat = HceCtrl.arrondirAuCentieme(hceCtrl.getTotalNbHeuresVacation(enseignantVacataire, annee, pecheParametres.isFormatAnneeExerciceAnneeCivile()));
					service.setHeuresServiceDu(BigDecimal.valueOf(heuresContrat));
					StringBuffer message = new StringBuffer("CREATION SERVICE annee:").append(annee);
					message.append(" enseignant vacataire:").append(enseignantVacataire.toIndividu().nomAffichage());
					message.append(" ").append(enseignantVacataire.toIndividu().prenomAffichage());
					message.append(" (").append(enseignantVacataire.toIndividu().persId()).append(")");
					message.append(" service :").append(heuresContrat);						
					log.info(message.toString());
					
					EORepartService.creerNouvelRepartService(editingContext, service, etablissement, createur, Constante.CIRCUIT_VACATAIRE_A_DETERMINER, annee);
					
					editingContext.saveChanges();
				} catch (ValidationException e) {
					editingContext.revert();
					log.error("INCIDENT creation fiche service vacataire persId:" + enseignantVacataire.toIndividu().persId() + " " + e);
				}
			}
		}
	}

	private EOQualifier getListeEnseignantsVacatairesSansFicheService(Integer annee,boolean isFormatAnneeExerciceAnneeCivile) {

		ERXKeyValueQualifier dDebQual = ERXQ.lessThan(
				EOActuelEnseignant.DATE_DEBUT_KEY,
				OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinQual = ERXQ.greaterThanOrEqualTo(
				EOActuelEnseignant.DATE_FIN_KEY,
				OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinIsNullQual = ERXQ
				.isNull(EOActuelEnseignant.DATE_FIN_KEY);

		ERXKeyValueQualifier temVacataire = ERXQ.equals(EOActuelEnseignant.TEM_STATUTAIRE.key(), Constante.NON);

		EOQualifier listeServiceNullQual = 
				ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), annee), EOActuelEnseignant.LISTE_SERVICES.key()));

		EOQualifier qualifier = ERXQ.and(dDebQual, ERXQ.or(dFinQual, dFinIsNullQual), temVacataire, listeServiceNullQual);

		return qualifier;
	}


	private EOQualifier getListeEnseignantsStatutaires(Integer annee, boolean isFormatAnneeExerciceAnneeCivile, boolean sansFicheService) {

		// AFFECTATION
		ERXKeyValueQualifier dDebAffectationQual = ERXQ.lessThanOrEqualTo(
				EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_DEB_AFFECTATION_KEY).key(),
						OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinAffectationValideQual = ERXQ.greaterThanOrEqualTo(
				EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_FIN_AFFECTATION_KEY).key(),
						OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinAffectationIsNullQual = ERXQ
				.isNull(EOActuelEnseignant.TO_AFFECTATION.dot(
						EOAffectation.D_FIN_AFFECTATION_KEY).key());

		ERXOrQualifier dFinAffectationQual = ERXQ.or(dFinAffectationValideQual, dFinAffectationIsNullQual);

		ERXKeyValueQualifier temValideQual = ERXQ.equals(
				EOActuelEnseignant.TO_AFFECTATION.dot(EOAffectation.TEM_VALIDE_KEY).key(), Constante.OUI);

		// Date de début et de fin du contrat ou de l'élement de carrière
		ERXKeyValueQualifier dDebElementOuContratQual = 
				ERXQ.lessThanOrEqualTo(EOActuelEnseignant.DATE_DEBUT_KEY,
						OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinElementOuContratQual = 
				ERXQ.greaterThanOrEqualTo(EOActuelEnseignant.DATE_FIN_KEY,
						OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinElementOuContratIsNullQual = ERXQ.isNull(EOActuelEnseignant.DATE_FIN_KEY);

		ERXOrQualifier dateFinElementOuContratQual = ERXQ.or(dFinElementOuContratQual, dFinElementOuContratIsNullQual);


		ERXKeyValueQualifier temStatutaireQual = ERXQ.equals(EOActuelEnseignant.TEM_STATUTAIRE.key(), Constante.OUI);

		NSArray<EOQualifier> qualifierAutres = new NSMutableArray<EOQualifier>();

		qualifierAutres.add(dDebAffectationQual);
		qualifierAutres.add(dFinAffectationQual);
		qualifierAutres.add(dDebElementOuContratQual);
		qualifierAutres.add(dateFinElementOuContratQual);
		qualifierAutres.add(temValideQual);
		qualifierAutres.add(temStatutaireQual);

		if (sansFicheService) {
			EOQualifier listeServiceNullQual = 
					ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), annee), EOActuelEnseignant.LISTE_SERVICES.key()));
			qualifierAutres.add(listeServiceNullQual);
		}

		EOQualifier qualifier = ERXQ.and(qualifierAutres);

		return qualifier;
	}

	/**
	 * @return un nouvel editing context dans la stack eof réservée
	 *      à l'exécution de jobs. Ceci permet d'isoler
	 *      au maximum les pbs éventuels pouvant arriver lors de l'exécution de jobs.
	 */
	public synchronized EOEditingContext newEditingContextForJobs() {
		if (osc == null) {
			osc = ERXTaskObjectStoreCoordinatorPool.objectStoreCoordinator();
		}
		ERXEC ec = (ERXEC) ERXEC.newEditingContext(osc);
		ec.setUseAutoLock(false);
		ec.setCoalesceAutoLocks(false);
		return ec;
	}

}

