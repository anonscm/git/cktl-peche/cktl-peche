package org.cocktail.peche.components.enseignants.services;

import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.commun.OngletComposante;
import org.cocktail.peche.entity.EOService;

import com.webobjects.foundation.NSArray;

/**
 * 
 *
 */
public interface IServiceOngletEnseignant {

	
	/**
	 * Récupére une map des structures concernées à partir des répartitions service et REH existant
	 * @param service la fiche de service
	 * @param composantesUtilisateur les composantes de l'utilisateur
	 * @param visualisationOngletComposantePrincipale Ajout de cette composante dans la map dans le cas où true
	 * @param rechercheDepuisRepartService Recherche depuis les repartService (true) ou les serviceDetail (false)
	 * @return mapCodeStructure
	 */
	Map<String, IStructure> recupererMapCodeStructure(EOService service, List<IStructure> composantesUtilisateur,  boolean visualisationOngletComposantePrincipale, boolean rechercheDepuisRepartService);
	
	/**
	 * 
	 * @param ongletComposante 
	 * @return la structure faisant référence à l'ongletComposante
	 */
	IStructure recupererStructureOngletSelectionne(OngletComposante ongletComposante);
	
	
	/**
	 * 
	 * @param codeStructure le code de la structure
	 * @param listeOngletComposantes liste d'onglets
	 * @return l'objet OngletComposante ayant pour code celui passé en paramétre
	 */
	OngletComposante recupererOngletComposanteParCode(String codeStructure, NSArray<OngletComposante> listeOngletComposantes);
	
	
	/**
	 * 
	 * @param onglet un onglet
	 * @param listeStructures liste
	 * @return true si l'onglet est dans la liste de structure
	 */
	boolean estOngletSelectionneDansListeStructure(OngletComposante onglet, List<IStructure> listeStructures);
	
}
