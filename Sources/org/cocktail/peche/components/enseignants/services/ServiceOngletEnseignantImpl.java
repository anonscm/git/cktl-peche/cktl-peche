package org.cocktail.peche.components.enseignants.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.commun.OngletComposante;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.foundation.NSArray;

/**
 * 
 *
 */
public class ServiceOngletEnseignantImpl implements IServiceOngletEnseignant {

	private Map<String, IStructure> mapCodeStructure = new HashMap<String, IStructure>();
	
	/**
	 * Récupére une map des structures concernées à partir des répartitions service et REH existant
	 * @param service la fiche de service
	 * @param composantesUtilisateur les composantes de l'utilisateur
	 * @param visualisationOngletComposantePrincipale Ajout de cette composante dans la map dans le cas où true
	 * @return mapCodeStructure
	 */
	private Map<String, IStructure> construireMapCodeStructure(EOService service, List<IStructure> composantesUtilisateur,  boolean visualisationOngletComposantePrincipale) {
		
		mapCodeStructure.clear();
		
		if (service != null) {
				
			NSArray<EORepartService> listeRepartService = service.toListeRepartService();
			
			for (EORepartService eoRepartService : listeRepartService) {
				
				EOStructure structure = eoRepartService.toStructure();
				if (structure != null) {
					mapCodeStructure.put(structure.cStructure(), structure);
				}
			}
		}
		if (visualisationOngletComposantePrincipale && composantesUtilisateur != null && composantesUtilisateur.size() > 0) {
			for (IStructure structure : composantesUtilisateur) {
				mapCodeStructure.put(structure.cStructure(), structure);
			}
		}
		return mapCodeStructure;
	}
	
	/**
	 * Récupére une map des structures concernées à partir des répartitions service et REH existant
	 * @param service la fiche de service
	 * @return mapCodeStructure
	 */
	private Map<String, IStructure> construireMapCodeStructureParServiceDetail(EOService service) {
		
		mapCodeStructure.clear();
		
		if (service != null) {
				
			ComposantCtrl composantCtrl = new ComposantCtrl();
				
			NSArray<EOServiceDetail> listeServiceDetail = service.listeServiceDetails();
				
			for (EOServiceDetail eoServiceDetail : listeServiceDetail) {
				if (eoServiceDetail.composantAP() != null) {
					// cas des répartitions de type service					
					EOStructure composanteParentAP = composantCtrl.getStructureAP(eoServiceDetail.composantAP());
					if (composanteParentAP != null) {
						mapCodeStructure.put(composanteParentAP.cStructure(), composanteParentAP);
					}
				} else if (eoServiceDetail.toStructure() != null) {
					// cas des répartitions de type REH
					EOStructure structure = eoServiceDetail.toStructure();
					mapCodeStructure.put(structure.cStructure(), structure);
				}
			}
		}
		return mapCodeStructure;
	}
	
	
	/**
	 * {@inheritDoc}
	 */	
	public Map<String, IStructure> recupererMapCodeStructure(EOService service, List<IStructure> composantesUtilisateur,  boolean visualisationOngletComposantePrincipale, boolean rechercheDepuisRepartService) {
	
		if (rechercheDepuisRepartService) {
			return construireMapCodeStructure(service, composantesUtilisateur, visualisationOngletComposantePrincipale);
		} else {
			return construireMapCodeStructureParServiceDetail(service);
		}
		
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	public IStructure recupererStructureOngletSelectionne(OngletComposante ongletComposante) {
		if (ongletComposante != null) {
			return mapCodeStructure.get(ongletComposante.getCode());
		}
		return null;
	}
	

	public Map<String, IStructure> getMapCodeStructure() {
		return mapCodeStructure;
	}

	
	/**
	 * {@inheritDoc}
	 */
	public OngletComposante recupererOngletComposanteParCode(String codeStructure, NSArray<OngletComposante> listeOngletComposantes) {
		
		for (OngletComposante ongletComposante : listeOngletComposantes) {
			if (codeStructure.equals(ongletComposante.getCode())) {
				return ongletComposante;
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean estOngletSelectionneDansListeStructure(OngletComposante onglet, List<IStructure> listeStructures) {

		if (listeStructures != null) {
			for (IStructure structure : listeStructures) {
				if (onglet.getCode().equals(structure.cStructure())) {
					return true;
				}
			}
		}
		return false;
		
	}
	
}

