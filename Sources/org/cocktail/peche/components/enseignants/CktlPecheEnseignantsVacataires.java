package org.cocktail.peche.components.enseignants;

import org.apache.commons.lang.StringUtils;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOContext;

public class CktlPecheEnseignantsVacataires extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 2648247905725211880L;

	/* prévisionnel ou définitif */
	private String typePopulation;
	/* Ecran "mes validations" */
	private boolean mesValidations;
	/* service pour rapprochement */
	EOService serviceARapprocher;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution.
	 */
	public CktlPecheEnseignantsVacataires(WOContext context) {
        super(context);
    }


	public String getTypePopulation() {
		return typePopulation;
	}


	public void setTypePopulation(String typePopulation) {
		this.typePopulation = typePopulation;
	}
	
	/**
	 * 
	 * @return id sélectionné dans le menu
	 */
	public String getSelectedItemId() {
		if (Constante.PREVISIONNEL.equals(getTypePopulation())) {
			if (isMesValidations()) {
				return PecheMenuItem.MES_VALIDATIONS_VACATAIRES_PREVISIONNEL.getId();
			} else {
				return PecheMenuItem.ENSEIGNANTS_VACATAIRES_PREVISIONNEL.getId();
			}
		}
		if (Constante.DEFINITIF.equals(getTypePopulation())) {
			if (isMesValidations()) {
				return PecheMenuItem.MES_VALIDATIONS_VACATAIRES_DEFINITIF.getId();
			} else {
				return PecheMenuItem.ENSEIGNANTS_VACATAIRES_DEFINITIF.getId();
			}
		}
		return StringUtils.EMPTY;
	}


	public boolean isMesValidations() {
		return mesValidations;
	}


	public void setMesValidations(boolean mesValidations) {
		this.mesValidations = mesValidations;
	}
	
	/**
	 * 
	 * @return titre de la page
	 */
	public String titre() {
		if (getServiceARapprocher() != null) {
			return "Rapprochement avec un enseignant vacataire";			
		}	
		if (isMesValidations()) {
			return message("EnseignantVacataire.validation.titre");
		} else {
			return message("EnseignantVacataire.titre");
		}
	}
	
	public EOService getServiceARapprocher() {
		return serviceARapprocher;
	}

	public void setServiceARapprocher(EOService serviceARapprocher) {
		this.serviceARapprocher = serviceARapprocher;
	}	
	
	public boolean isRapprochement() {
		return (getServiceARapprocher() != null);
	}
	
	public double serviceGeneriqueAttribue() {
		if (getServiceARapprocher() != null) {
			EnseignantsGeneriquesCtrl controleur = new EnseignantsGeneriquesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			return controleur.getServiceAttribue(getServiceARapprocher());
		}
		return 0;
	}
	
}