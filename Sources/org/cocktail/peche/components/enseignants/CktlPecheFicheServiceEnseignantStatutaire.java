package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.DemandeCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.CktlAjaxUtils;

/**
 * Classe permettant de voir un récapitulatif de la fiche de service de
 * l'enseignant statutaire.
 * 
 * @author Yannick MAURAY
 * @author Etienne RAGONNEAU
 * @author Chama LAATIK
 * 
 */
public class CktlPecheFicheServiceEnseignantStatutaire extends CktlPecheBaseComponent {

	private static final String TEMPS_CONTRAT_PAS_EN_HEURE = "Attention, le nombre d'heures du contrat n'est pas exprimé en heures.";

	private static final String UNITE_DE_TEMPS_NON_RENSEIGNE = "Attention, l'unité de temps (heures, jours...) dans laquelle est exprimé le service dû n'est pas renseigné.";

	private static final String NOMBRE_HEURE_CONTRAT_ZERO = "Attention, le nombre d'heures du contrat est à zéro.";

	private static final String REH_SANS_COMPOSANTE = "Attention, toutes les répartitions REH ne sont pas liées à une composante.";
	
	private static final String EXISTENCE_REPARTITION_EN_ATTENTE = "La validation de la fiche n'est pas possible tant qu'il reste des répartitions à l'état &quot;En attente&quot;";
	
	private static final String HEURES = "H";

	private static final long serialVersionUID = 3189445263417811908L;

	private EOActuelEnseignant editedObject;
	private EnseignantsStatutairesCtrl controleur;
	private EOService service;
	private GenerateurPDFCtrl generateurPDFCtrl;
	private HceCtrl hceCtrl;

	private String motifRefus;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheFicheServiceEnseignantStatutaire(WOContext context) {
		super(context);
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		this.hceCtrl = new HceCtrl(this.edc());
	}

	private void verificationHeuresContrats() {
		if (this.getServiceStatutaire() == 0) {
			getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, NOMBRE_HEURE_CONTRAT_ZERO);
			return;
		}
		
		NSArray<EOContrat> contrats = editedObject.getListeContrats(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		for (EOContrat contrat : contrats) {
			EOContratAvenant contratAvenant = contrat.avenantSuivantPeriode(OutilsValidation.getNSTDebutAnneeUniversitaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
	        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()),
					OutilsValidation.getNSTFinAnneeUniversitaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
			        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			if (contratAvenant != null) {
				if (contratAvenant.toTypeUniteTemps() == null) {
					getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, UNITE_DE_TEMPS_NON_RENSEIGNE);
					return;
				}
				if (contratAvenant.toTypeUniteTemps().code() == null) {
					getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, UNITE_DE_TEMPS_NON_RENSEIGNE);
					return;
				}
				if (!contratAvenant.toTypeUniteTemps().code().equals(HEURES)) {
					getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, TEMPS_CONTRAT_PAS_EN_HEURE);
					return;
				}
			}
		}

	}
	
	/**
	 * Si une répartition de type REH n'a pas de composante associée,
	 * on lève un message d'information
	 */
	private void verificationComposantesReh() {
		
		NSArray<EOServiceDetail> listeServiceDetails = getService().listeServiceDetails();
		for (EOServiceDetail serviceDetail : listeServiceDetails) {
			if (serviceDetail.reh() != null && serviceDetail.toStructure() == null) {
				getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, REH_SANS_COMPOSANTE);
				return;
			}
		}

	}

	/**
	 * Définit l'objet Enseignant et crée la fiche de service correspondante
	 * 
	 * @param editedObject
	 *            : EOActuelEnseignant
	 */
	public void setEditedObject(EOActuelEnseignant editedObject) {
		this.editedObject = editedObject;
		ModeEdition modeEdition = ModeEdition.MODIFICATION;

		if (getService() == null || getService().toListeRepartService().size() == 0) {
			modeEdition = ModeEdition.CREATION;
			
			if (getService() == null) {
				service = EOService.creerEtInitialiser(edc());
			} 
			if (getService().toListeRepartService().size() == 0) {
				EORepartService.creerNouvelRepartService(edc(), service, null, getPecheSession()
						.getApplicationUser().getPersonne(), CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), 
						getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			}
			
			try {
				beforeSaveChanges(modeEdition);
				edc().saveChanges();
			} catch (ValidationException e) {
				this.setEditedObject(null);
				edc().invalidateAllObjects();
				edc().revert();
				throw e;
			}
		}
		verificationHeuresContrats();
		verificationComposantesReh();
	}

	/**
	 * Renvoie l'enseignant si on accède directement à la fiche, renvoie le
	 * binding correspondant
	 * 
	 * @return enseignant
	 */
	public EOActuelEnseignant getEditedObject() {
		if (editedObject == null) {
			this.editedObject = (EOActuelEnseignant) valueForBinding("enseignant");
		}
		return this.editedObject;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app",
				"styles/ficheService.css");
	}

	/**
	 * Renvoie le service statutaire de l'enseignant
	 * 
	 * @return service statutaire.
	 */
	public double getServiceStatutaire() {
		EOService service = getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service.heuresService() != null) {
			return service.getHeuresService().doubleValue();			
		} else {
			return getEditedObject().getNbHeuresServiceStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}
	}

	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 * 
	 * @return nombre d'heures
	 */
	public double getServiceAttribue() {
		EOService service = getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service.heuresServiceAttribue() != null) {
			return service.getHeuresServiceAttribue().doubleValue();			
		} else {
		return controleur.getServiceAttribue(getEditedObject());
		}
	}

	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 * 
	 * @return nombre d'heures
	 */
	public double getRehAttribue() {
		return controleur.getRehAttribue(getEditedObject());
	}
	
	
	public double getServiceRealise() {
		EOService service = getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service.heuresServiceRealise() != null) {
			return service.getHeuresServiceRealise().doubleValue();			
		} else {
			return controleur.getServiceRealise(getEditedObject());
		}
	}

	
	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 * 
	 * @return nombre d'heures
	 */
	public double getRehRealise() {
		return controleur.getRehRealise(getEditedObject());
	}
	
	/**
	 * renvoie le service dû de l'enseignant
	 * 
	 * @return le service dû
	 */
	public double getServiceDu() {
		return controleur.getServiceDu(getEditedObject(), getPecheSession()
				.getPecheParametres().getAnneeUniversitaireEnCours());
	}

	public double getReportAnneeAnterieure() {
		return controleur.getReportAnneeAnterieure(getEditedObject(), controleur.getAnneeUniversitairePrecedente(getPecheSession()
				.getPecheParametres().getAnneeUniversitaireEnCours()));
	}
	
	public double getReportAnneeAnterieureOppose() {
		return -getReportAnneeAnterieure();
	}

	/**
	 * Renvoie le service à faire après déduction de la modalité de service
	 * 
	 * @return nombre d'heures
	 */
	public double getTotalModaliteService() {
		return hceCtrl.getTotalNbHeuresModaliteService(getEditedObject(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * @return Le nombre d'heures opposés de la modalité de service (pour
	 *         l'affichage)
	 */
	public double getTotalModaliteServiceOppose() {
		return -getTotalModaliteService();
	}

	/**
	 * @return la différence entre le service attribué et le service dû
	 */
	public double getDeltaRealise() {
		double nbHeures = getServiceRealise() - getServiceDu();
		return nbHeures;
	}

	/**
	 * @return le nombre d'heures complémentaires prévisionelles.
	 */
	public double getHcompPrev() {
		return controleur.getHcompPrev(getService());
	}

	/**
	 * @return le nombre d'heures complémentaires réalisées.
	 */
	public double getHcompRealise() {
		return controleur.getHcompRealise(getService());
	}

	/**
	 * retourne le nombre d'heures total de décharges prévues pour un
	 * intervenant
	 * 
	 * @return le total d'heures des décharges
	 */
	public double getTotalDecharges() {
		return hceCtrl.getTotalNbHeuresDecharges(getEditedObject().toIndividu(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());		
	}

	/**
	 * @return Le nombre d'heures opposés du nombre d'heures de décharge (pour
	 *         l'affichage)
	 */
	public double getTotalDechargesOppose() {
		return -getTotalDecharges();
	}
	
	/**
	 * retourne le nombre d'heures total des minorations prévues pour un
	 * intervenant
	 * 
	 * @return le total d'heures des minorations
	 */
	public double getTotalMinorations() {
		return hceCtrl.getTotalNbHeuresMinorations(getEditedObject(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
	
	/**
	 * @return Le nombre d'heures opposés du nombre d'heures de minoration (pour
	 *         l'affichage)
	 */
	public double getTotalMinorationsOppose() {
		return -getTotalMinorations();
	}

	/**
	 * renvoie le libelle long du corps de l'enseignant
	 * 
	 * @return corps de l'intervenant
	 */
	public String getLibelleCorps() {
		String libelle = controleur.getLibelleLongCorpsOuContrat(getEditedObject());
		return libelle;
	}

	public String getLibelleTypePopulation() {
		String libelle = controleur.getLibelleLongTypePopulation(getEditedObject());
		return libelle;
	}
	
	/**
	 * @return le statut de l'enseignant.
	 */
	public String getStatut() {
		return controleur.getStatut(getEditedObject());
	}

	/**
	 * Permet de récuperer le nom de la composante.
	 * 
	 * @return le nom de la composante.
	 */
	public String getCodeComposante() {
		EOStructure composante = getEditedObject().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = "?";

		if (composante != null) {
			nomComposante = composante.llStructure();
		}

		return nomComposante;
	}

	/**
	 * Permet de récuperer le nom du département d'enseignement.
	 * 
	 * @return le nom du département d'enseignement.
	 */
	public String getCodeDepartementEnseignement() {
		EOStructure composante = getEditedObject().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = null;

		if (composante != null) {
			nomComposante = composante.llStructure();
		}

		return nomComposante;
	}

	/**
	 * Enregistre le service d'un intervenant.
	 * 
	 * @throws ValidationException
	 *             : exception au cours de l'enregistrement.
	 */
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		Session pecheSession = getPecheSession();
		EOPersonne personne = personneConnecte();

		if (modeEdition == ModeEdition.CREATION) {
			service.setPersonneCreation(personne);
			service.setDateCreation(now);
		}

		if (modeEdition == ModeEdition.MODIFICATION) {
			service.setPersonneModification(personne);
		}

		if (!StringCtrl.isEmpty(service.commentaire())) {
			service.setDateCommentaire(now);
		}

		service.setTemoinEnseignantGenerique("N");
		service.setTemoinValide("O");
		service.setDateModification(now);
		service.setEnseignant(editedObject);
		service.setAnnee(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
		service.majDonnesServiceDuStatutaire(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		service.majDonnesServiceReparti(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * Renvoie le service de l'enseignant
	 * 
	 * @return un service
	 */
	public EOService getService() {
		if (service == null) {
			this.service = editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		}
		return service;
	}
	
	public void setService(EOService service) {
		this.service = service;
	}

	/**
	 * Enregistre le commentaire de la fiche de l'enseignant
	 */
	public void enregistrerCommentaireFiche() {
		try {
			EOPersonne personne = personneConnecte();

			service.majDonnesAuditModification(personne);
			edc().saveChanges();

			getPecheSession()
					.addSimpleSuccessMessage(
							message("ficheService.message.commentaire.sauvegarde.titre"),
							message("ficheService.message.commentaire.sauvegarde.detail"));
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}

	// Gestion du circuit de validation
	// ********************************

	/**
	 * Est-ce que le bouton "Valider la fiche" doit être affiché ?
	 * 
	 * @return <code>true</code> si affiché
	 */
	public boolean hasDroitShowBoutonValiderFiche() {
		return getAutorisation().hasDroitShowChemin(getDemande(),
				TypeCheminPeche.VALIDER);
	}

	/**
	 * Est-ce que le bouton "Refuser la fiche" doit être affiché ?
	 * 
	 * @return <code>true</code> si affiché
	 */
	public boolean hasDroitShowBoutonRefuserFiche() {
		return getAutorisation().hasDroitShowChemin(getDemande(),
				TypeCheminPeche.REFUSER);
	}

	public boolean hasDroitShowBoutonValiderFicheOuRefuserFiche() {
		return (hasDroitShowBoutonValiderFiche() || hasDroitShowBoutonRefuserFiche());
	}
      
    public boolean isIncidentEnCours() {
    	return controleur.getRepartService(getService()).isIncidentPresent();
    	}
    
    
    public boolean isBoutonReinitFicheInactif() {
    	return (isBoutonValiderFicheInactif() && isBoutonRefuserFicheInactif());
    }
	
	/**
	 * Est-ce que le bouton "Valider la fiche" doit être inactif (disable) ?
	 * <p>
	 * Le répartiteur ne peut valider que les fiches de sa composante.
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean isBoutonValiderFicheInactif() {
		EODemande demande = getDemande();

		boolean boutonActif = true;
		boolean isBooleanCalcule = false;
		
		if (getFonctionToutesValidations()) {
			return false;
		}
		
		// On est sur l'étape "Validation par le répartition" et qu'on peut
		// "Valider"
		// et qu'on est répartiteur ==> On ne peut valider que des fiches de la
		// même structure
		if (getFonctionRepartiteur()
				&& (getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER) 
						|| getAutorisation().hasDroitUtilisationChemin(demande,	TypeCheminPeche.VALIDER))) {
			boutonActif = getPecheSession().getPecheAutorisationCache().isEnseignantContenueDansCibleEnseignantStatutaire(getEditedObject());
			isBooleanCalcule = true;
		} 
		if (getFonctionEnseignant()  && demande.toEtape().codeEtape().equals(EtapePeche.VALID_ENSEIGNANT.getCodeEtape())) {
			// Si on est enseignant => on ne peut valider que sa propre fiche de service
			if (getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_ENSEIGNANT, TypeCheminPeche.VALIDER) 
					&& getPecheSession().getApplicationUser().isFicheEnseignantConnecte(getService())) {
				boutonActif = true;
			} else {
				boutonActif = false;
			}
			isBooleanCalcule = true;
		}
		if (!isBooleanCalcule) {
			boutonActif = getAutorisation().hasDroitUtilisationChemin(demande,
					TypeCheminPeche.VALIDER);
		}
		return !boutonActif;
	}

	/**
	 * Est-ce que le bouton "Refuser la fiche" doit être inactif (disable) ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean isBoutonRefuserFicheInactif() {
		
		if (getFonctionToutesValidations()) {
			return false;
		}
		
		if (getFonctionEnseignant() && getDemande().toEtape().codeEtape().equals(EtapePeche.VALID_ENSEIGNANT.getCodeEtape())) {
			return !getAutorisation().hasDroitUtilisationChemin(getDemande(), EtapePeche.VALID_ENSEIGNANT, TypeCheminPeche.REFUSER) 
					|| !getPecheSession().getApplicationUser().isFicheEnseignantConnecte(getService());
		}
		return !getAutorisation().hasDroitUtilisationChemin(getDemande(),
				TypeCheminPeche.REFUSER);
	}

	public WOActionResults reinitialiserCircuit() {

		if (controlerPresenceRepartitionEnAttente()) {
			EORepartService repartService = controleur.getRepartService(getService());
			EODemande demande = repartService.toDemande();

			if ((demande != null) && (!demande.estSurEtapeInitiale())) {
				demande.reinitialiser();
				repartService.setTemIncident(null);
				/*StringBuffer suivi = new StringBuffer();
				suivi.append(DateCtrl.dateToString(DateCtrl.now()));
				suivi.append(": Circuit réinitialisé");
				getService().addSuivi(suivi.toString());*/
				getPecheSession().addSimpleSuccessMessage(message("ficheService.titre.reinit.circuit"), message("ficheService.details.reinit.circuit"));
			}
			getService().editingContext().saveChanges();
		}
		return null;
	}

	/**
	 * Valider la fiche et la fait avancer à l'étape suivante.
	 * si le circuit prévisionnel est terminé, retour à la liste
	 */
	public WOActionResults validerFiche() {
		
		if (controlerPresenceRepartitionEnAttente()) {
		
			boolean changementCircuit = false;
	
			changementCircuit = controleur.validerFiche(controleur.getRepartService(getService()), personneConnecte().persId());
	
			// Initialisation des heures réalisées lors de la bascule de la fiche de
			// prévisionnelle à définitive
			// Sur l'étape initiale du circuit définitif
			if (!isSurCircuitPrevisionnel() && getDemande().toEtape().isInitiale()) {
				for (EOServiceDetail sd : getService().listeServiceDetails()) {
					sd.setHeuresRealisees(sd.heuresPrevues());
				}
			}			
			controleur.getRepartService(getService()).setTemIncident(null);				
			getService().editingContext().saveChanges();
	
			if (changementCircuit) {
				getPecheSession().addSimpleInfoMessage(message("ficheService.titre.fin.circuit.previsionnel"), message("ficheService.details.fin.circuit.previsionnel"));
				return getPecheSession().pagePrecedente();
			}
		}
		setMotifRefus(null);
		return null;
	}
	

	/**
	 * 
	 * @return true s'il existe des répartitions à l'état en attente 
	 */
	private boolean controlerPresenceRepartitionEnAttente() {
		
		boolean validationOK = true;
		
		if (controleur.isCircuitPrevisionnel(getDemande())) {
			for (EOServiceDetail serviceDetail : getService().listeServiceDetails()) {
				if (Constante.ATTENTE.equals(serviceDetail.etat())) {
					validationOK = false;
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, EXISTENCE_REPARTITION_EN_ATTENTE);
					break;
				}
			}
		}
		
		return validationOK;
	}

	/**
	 * Est-ce que la demande de cette fiche de service est sur le circuit
	 * présivionnel ?
	 * 
	 * @return <code>true</code> si cette fiche de service est sur le circuit
	 *         présivionnel
	 */
	public boolean isSurCircuitPrevisionnel() {
		return controleur.isCircuitPrevisionnel(getDemande());
	}

	/**
	 * Refuser la fiche et la renvoie à l'étape précédente.
	 */

	public void refuserFiche() {

		if (StringCtrl.isEmpty(motifRefus)) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "Vous devez renseigner un motif de refus");
			return;
		}

		EORepartService repartService = controleur.getRepartService(getService());

		repartService.setTemIncident(null);

		String etatEtape = new DemandeCtrl(edc()).etatEtape(getDemande());

		StringBuffer commentaire = new StringBuffer();
		commentaire.append(DateCtrl.dateToString(DateCtrl.now()));
		commentaire.append(": Motif du refus (").append(etatEtape).append("): ");
		commentaire.append(motifRefus).append("\n");
		if (service.commentaire() != null) {
			commentaire.append(service.commentaire());
		}
		service.setCommentaire(StringCtrl.cut(commentaire.toString(), EOService.CommentairesMax));

		setMotifRefus(null);

		controleur.refuserFiche(repartService, personneConnecte().persId());
	}

	/**
	 * Imprimer la fiche 
	 */

	public WOActionResults imprimerFiche() {
		//génération du xml
		
		String fluxXml = controleur.getFluxFicheService(getEditedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession(), getComponentCtrl(), "Fiche individuelle de service");
		
		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);
		
		EODemande demande = controleur.getDemande(getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));

		generateurPDFCtrl.setReportFilename(controleur.getFilenameFicheService(getEditedObject(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), false, controleur.isCircuitPrevisionnel(demande)) + ".pdf");
		generateurPDFCtrl.genererFicheService("Reports/ficheServiceStatutaire/fiche_service_stat.jasper", true);
		
		return null;
	}

	public WOActionResults imprimerFicheNmoinsDeux() {
		return imprimerFicheNmoins(2);
	}
	
	public WOActionResults imprimerFicheNmoinsUn() {
		return imprimerFicheNmoins(1);
	}
	
	public WOActionResults imprimerFicheNmoins(int annee) {
		EnseignantsStatutairesCtrl controleurNmoins = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee,
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		//génération du xml
		String fluxXml = controleurNmoins.getFluxFicheService(getEditedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee, getPecheSession(), getComponentCtrl(), "Fiche individuelle de service");
		
		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);
		
		EOService serviceNmoins = getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee);
		EODemande demande = controleurNmoins.getDemande(serviceNmoins);

		generateurPDFCtrl.setReportFilename(controleurNmoins.getFilenameFicheService(getEditedObject(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee, false, controleurNmoins.isCircuitPrevisionnel(demande)) + ".pdf");
		generateurPDFCtrl.genererFicheService("Reports/ficheServiceStatutaire/fiche_service_stat.jasper", true);
		
		return null;
	}

		
	public GenerateurPDFCtrl getGenerateurPDFCtrl() {
		return generateurPDFCtrl;
	}

	public boolean historiquesPresent() {		
		return historiqueNmoinsUnPresent() ||  historiqueNmoinsDeuxPresent();
	}

	public boolean historiqueNmoinsUnPresent() {		
		return (editedObject.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 1, 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())
				&& (editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 1) != null));
	}

	public boolean historiqueNmoinsDeuxPresent() {		
		return (editedObject.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 2, 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())
				&& (editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 2) != null));
	}


	/**
	 * Boutons du service et REH actifs si la demande est sur l'étape validation
	 * du répartiteur
	 * 
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifsRepartitionService() {
		return getBoutonsInactifsRepartitionService(getDemande()) || !getPecheSession().getPecheAutorisationCache().isEnseignantContenueDansCibleEnseignantStatutaire(getEditedObject());
	}

	public EODemande getDemande() {
		return controleur.getDemande(getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
	}

	/**
	 * Renvoie le menu selectionné ==> sert pour les personnes qui cumulent 2
	 * profils dont celui d'enseignant
	 * 
	 * @return id du menu
	 */
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", "statutaires");
	}

	/**
	 * Renvoie l'état de la fiche de service suivant son circuit de validation
	 * 
	 * @return état de la fiche
	 */
	public String getEtatFicheService() {
		String etatFiche = null;
		if (isSurCircuitPrevisionnel()) {
			etatFiche = Constante.PREVISIONNEL;
		} else {
			etatFiche = Constante.DEFINITIF;
		}

		return etatFiche;
	}

	/**
	 * @return L'état de la demande
	 */
	public String getEtatEtapeDemande() {
		return EtapePeche.getEtape(getDemande().toEtape()).getEtatDemande();
	}

	public EOFicheVoeux getFicheVoeux() {
		return controleur.getFicheVoeux(getEditedObject());
	}
    
    /**
	 * Enregistre la répartition des heures complémentaires de l'enseignant
	 * @return fiche de service
	 */
	public WOActionResults enregistrerRepartitionHComp() {
		try {
			boolean controleOK = true;
			//Vérifie dans le cas d'un sous service de l'enseignant
			if (isSousServiceEtDroitReportOK()) {
				controleOK = controleBornesReport(true);				
			} else {
				controleOK = controleHcomp();				
			}
			
			if (controleOK) {							
				service.majDonnesAuditModification(personneConnecte());
				edc().saveChanges();
			}
			
			if (isSousServiceEtDroitReportOK() && controleOK) {
				getPecheSession().addSimpleInfoMessage("Sous-service", message("ficheService.enregistre.sousService"));
			} else if (controleOK) {
				getPecheSession().addSimpleInfoMessage("Heures complémentaires", message("ficheService.enregistre.heuresCompl"));
			}	
				
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}

		return null;
	}
	
	/**
	 * @return vrai si sous-sevice de l'enseignant et droit au report activé
	 */
	public boolean isSousServiceEtDroitReportOK() {
		return (getAutorisation().hasDroitUtilisationReportHeuresComplementaires() && (getDeltaRealise() < 0));
	}
	
	/**
	 * Controles à vérifier pour les heures complémentaires
	 * @return faux si exception levé
	 */
	public boolean controleHcomp() {
		boolean controleOK = true;
		
		//FIXME : trouver solution pour contrat ex: ATER ==> Attendre contrat ONP
		//Vérifie que les décharges/modalités/minorations permettent les heures complémentaires
		if (isHeuresComplementairesInterdit() && ((getService().getHeuresAPayer().compareTo(BigDecimal.ZERO) != 0))) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, String.format(message("ficheService.controle.hCompInterditSiReductionService"), 
					controleur.getLibelleMinorationInterdisantHComp(getService().enseignant(), getPecheSession().getPecheParametres()
					.getAnneeUniversitaireEnCours())));
			controleOK = false;
		}
		
		//Vérifie que le total des heures est égal aux heures complémentaires réalisées
		if (!controleur.getTotalHcompOK(getService())) {
			if (getAutorisation().hasDroitUtilisationReportHeuresComplementaires()) {
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, String.format(message("ficheService.controle.totalHeuresSiReport"), 
						getApp3DecimalesFormatter().format(controleur.getHcompRealise(getService()))));
			} else {
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, String.format(message("ficheService.controle.totalHeuresSiPasReport"), 
						getApp3DecimalesFormatter().format(controleur.getHcompRealise(getService()))));
			}
			controleOK = false;
		}
		
		if (!controleBornesReport(getDeltaRealise() < 0)) {
			controleOK = false;
		}
		
		return controleOK;
	}
	
	/**
	 * Controle les bornes inférieure et supérieure du report
	 * @param isEnseignantEnSousService : l'enseignant est en sous-service : vrai/faux
	 * @return faux si exception levé
	 */
	public boolean controleBornesReport(boolean isEnseignantEnSousService) {
		boolean controleOK = true;

		if (getAutorisation().hasDroitUtilisationReportHeuresComplementaires()) {
			//Borne limite du report (inférieure ou supérieure)
			double borneLimite = 0.0;
			
			if (isEnseignantEnSousService) {
				borneLimite = EOPecheParametre.getNbHeureReportBorneInferieur(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			} else {
				borneLimite = EOPecheParametre.getNbHeureReportBorneSuperieur(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			}
			
			TypeMessage typeErreur;
			if (getAutorisation().hasDroitUtilisationValidationParPresident()) {
				typeErreur = TypeMessage.ATTENTION;
			} else {
				typeErreur = TypeMessage.ERREUR;
			}
			
			//Vérifie report enseignant en sous service
			if (isEnseignantEnSousService) {					
				if (getService().getHeuresAReporterAttribuees().doubleValue() > 0.0) {
					getPecheSession().addSimpleMessage(typeErreur, String.format(message("ficheService.controle.heuresReportPositifSiSousService"), borneLimite));
					controleOK = false;
				} else if (getService().getHeuresAReporterAttribuees().doubleValue() < borneLimite) {
					getPecheSession().addSimpleMessage(typeErreur, String.format(message("ficheService.controle.bornesInferieursHeuresReportSiSousService"), borneLimite));
					controleOK = false;
				}
			}
			//Vérifie report enseignant pas en sous service
			else if (!isEnseignantEnSousService) {				
				if (getService().getHeuresAReporterAttribuees().doubleValue() < 0.0) {
					getPecheSession().addSimpleMessage(typeErreur, String.format(message("ficheService.controle.heuresReportNegatifSiHcomp"), borneLimite));
					controleOK = false;
				} else if (getService().getHeuresAReporterAttribuees().doubleValue() > borneLimite) {
					getPecheSession().addSimpleMessage(typeErreur, String.format(message("ficheService.controle.bornesSuperieursHeuresReportSiHcomp"), borneLimite));
					controleOK = false;
				}
			}
			
			if (getAutorisation().hasDroitUtilisationValidationParPresident()) {
				controleOK = true;
			}
			
		}
		return controleOK;
	}
	
	/**
	 * @return vrai si le libellé de la minoration de service est non null
	 */
	public boolean isHeuresComplementairesInterdit() {
		return controleur.getLibelleMinorationInterdisantHComp(getService().enseignant(), getPecheSession().getPecheParametres()
				.getAnneeUniversitaireEnCours()) != null;
	}
	
	/**
	 * @return vrai si sous-sevice de l'enseignant et droit au report activé
	 */
	public boolean isHeuresComplementaires() {
		return (getHcompRealise() > 0);
	}
	
	/**
	 * Formatter à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp2DecimalesFormatter() {
		return OutilsValidation.getApp2DecimalesFormatter();	
	}

	/**
	 * Formatter à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp3DecimalesFormatter() {
		return OutilsValidation.getApp3DecimalesFormatter();	
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}
	/**
	 * Formatter à deux decimales : utiliser pour les données numériques pouvant être négatives.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp2DecimalesFormatterNegatif() {
		return OutilsValidation.getApp2DecimalesFormatterNegatif();
	}
	
	/**
	 * Formatter à trois decimales : utiliser pour les données numériques pouvant être négatives.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp3DecimalesFormatterNegatif() {
		return OutilsValidation.getApp3DecimalesFormatterNegatif();
	}
	

	/**
	 * @return the isHeuresPayeesNonNulles
	 */
	public boolean isHeuresPayeesNonNulles() {
		return getService().getHeuresPayees().doubleValue() > 0;
	}

	/**
	 * 
	 * @return true si le récapitulatif des fiches de voeux doit être affiché
	 */
	public boolean hasDroitShowRecapitulatifVoeux() {
		return EOPecheParametre.isFicheVoeuxUtilisation(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}

	/**
	 * @return la methode de calcul
	 */
	public String getMethodeCalcul() {
		return getEditedObject().getMethodeCalcul(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * @return the motifRefus
	 */
	public String motifRefus() {
		return motifRefus;
	}

	/**
	 * @param motifRefus the motifRefus to set
	 */
	public void setMotifRefus(String motifRefus) {
		this.motifRefus = motifRefus;
	}
	
	public EORepartService getRepartService() {
		return getService().toListeRepartService().get(0);
	}

	public boolean presenceTypePopulation() {
		return (getLibelleTypePopulation().trim().length() > 0);
	}
	
}