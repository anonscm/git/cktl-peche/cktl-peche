package org.cocktail.peche.components.enseignants;

import org.apache.commons.lang.StringUtils;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOContext;

public class CktlPecheEnseignantsStatutaires extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 1774257889734314306L;

	/* prévisionnel ou définitif */
	private String typePopulation;
	/* Ecran "mes validations" */
	private boolean mesValidations;
	/* service pour rapprochement */
	EOService serviceARapprocher;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheEnseignantsStatutaires(WOContext context) {
        super(context);
    }

	public String getTypePopulation() {
		return typePopulation;
	}

	public void setTypePopulation(String typePopulation) {
		this.typePopulation = typePopulation;
	}
	
	/**
	 * 
	 * @return id sélectionné dans le menu
	 */
	public String getSelectedItemId() {

		if (getServiceARapprocher() != null) {
			return PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId();			
		}

		if (Constante.PREVISIONNEL.equals(getTypePopulation())) {
			if (isMesValidations()) {
				return PecheMenuItem.MES_VALIDATIONS_STATUTAIRES_PREVISIONNEL.getId();
			} else {
				return PecheMenuItem.ENSEIGNANTS_STATUTAIRES_PREVISIONNEL.getId();
			}
		}
		if (Constante.DEFINITIF.equals(getTypePopulation())) {
			if (isMesValidations()) {
				return PecheMenuItem.MES_VALIDATIONS_STATUTAIRES_DEFINITIF.getId();
			} else {
				return PecheMenuItem.ENSEIGNANTS_STATUTAIRES_DEFINITIF.getId();
			}
		}
		
		return StringUtils.EMPTY;
	}

	/**
	 * 
	 * @return titre affichée sur la page
	 */
	public String titre() {
		if (getServiceARapprocher() != null) {
			return "Rapprochement avec un enseignant statutaire";			
		}		
		if (!isMesValidations()) {
			return message("EnseignantStatutaire.titre");
		} else {
			return message("EnseignantStatutaire.validation.titre");
		}
	}
	
	public boolean isMesValidations() {
		return mesValidations;
	}

	public void setMesValidations(boolean mesValidations) {
		this.mesValidations = mesValidations;
	}

	public EOService getServiceARapprocher() {
		return serviceARapprocher;
	}

	public void setServiceARapprocher(EOService serviceARapprocher) {
		this.serviceARapprocher = serviceARapprocher;
	}	
	
	public boolean isRapprochement() {
		return (getServiceARapprocher() != null);
	}

	public double serviceGeneriqueAttribue() {
		if (getServiceARapprocher() != null) {
			EnseignantsGeneriquesCtrl controleur = new EnseignantsGeneriquesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			return controleur.getServiceAttribue(getServiceARapprocher());
		}
		return 0;
	}
	
	public String libelleStructureGenerique() {
		String libelleDepartement = null;
		String libelleComposante = null;
		if (getServiceARapprocher() != null) {	
			EnseignantsGeneriquesCtrl controleur = new EnseignantsGeneriquesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

			if ((getServiceARapprocher().toEnseignantGenerique().toStructure() != null) 
					&& (controleur.getDepartementEnseignement(getServiceARapprocher().toEnseignantGenerique().toStructure()) != null)) {
				libelleDepartement = controleur.getDepartementEnseignement(getServiceARapprocher().toEnseignantGenerique().toStructure()).lcStructure();			
			}

			if ((getServiceARapprocher().toEnseignantGenerique().toStructure() != null) 
					&& (controleur.getComposanteTypeCS(getServiceARapprocher().toEnseignantGenerique().toStructure()) != null)) {
				libelleComposante = controleur.getComposanteTypeCS(getServiceARapprocher().toEnseignantGenerique().toStructure()).lcStructure();
			}

			if (libelleDepartement != null && libelleComposante != null) {
				return libelleComposante + " / " +  libelleDepartement;
			}

			if (libelleDepartement != null) {
				return libelleDepartement;
			}

			if (libelleComposante != null) {
				return libelleComposante;
			}
		}
		return "";
	}
			
}