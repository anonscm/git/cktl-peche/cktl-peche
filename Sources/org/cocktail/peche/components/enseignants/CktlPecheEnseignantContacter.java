package org.cocktail.peche.components.enseignants;

import java.util.ArrayList;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;


/**
 * Cette classe permet à un enseignant de contacter son ou ses répartiteur via un formulaire qui envoi un mail.
 * @author etienne
 *
 */
public class CktlPecheEnseignantContacter extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 4075852112237816112L;
	
//	private MailGenerateur mailGenerateur = null;
	private EnseignantsStatutairesCtrl enseignantsStatutairCtrl= null;
	private String sujetMessage = null;
	private String message = null;
	private ArrayList<EOIndividu> destinatairesMessage = new ArrayList<EOIndividu>();
	private NSArray<EOIndividu> listeRepartiteurs = null;
	private EOIndividu currentRepartiteur = null;
	private EOIndividu selectedRepartiteur = null;
	
	/**
	 * Constructeur
	 * @param context le contexte de l'application.
	 */
	public CktlPecheEnseignantContacter(WOContext context) {
        super(context);
//        mailGenerateur = new MailGenerateur(edc());
        enseignantsStatutairCtrl = new EnseignantsStatutairesCtrl(edc(),getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
//        setListeRepartiteurs(getRepartiteurs());
    }
	
	
	
	
	
	public void addCurrentItemToDestinataire() {
		setDestinataires(currentRepartiteur);
	}
	
	
	/**
	 * Retourne la liste des repartiteurs de l'enseignants connecté.
	 * @return 
	 */
//	public NSArray<EOIndividu> getRepartiteurs() {
//		return enseignantsStatutairCtrl.getRepartiteurs(individuConnecte());
//	}


	public String getDestinatairesAffichage() {
		return getDestinataires().toString();  //TODO, metre en forme l'affichage
	}

	/**
	 * Retourne le sujet du message.
	 * @return
	 */
	public String getSujet() {
		return sujetMessage;
	}



	/**
	 * Definit le sujet du message.
	 * @param sujet
	 */
	public void setSujet(String sujet) {
		this.sujetMessage = sujet;
	}



	/**
	 * Retourne le message.
	 * @return
	 */
	public String getMessage() {
		return message;
	}



	/**
	 * Deffinit le message.
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}



	/**
	 * Retourne la liste des destinataires.
	 * @return
	 */
	public ArrayList<EOIndividu> getDestinataires() {
		return destinatairesMessage;
	}



	/**
	 * Définit la iste des destinataire.
	 * @param destinataires
	 */
	public void setDestinataires(EOIndividu destinataires) {
		this.destinatairesMessage.add(destinataires);
	}






	public NSArray<EOIndividu> getListeRepartiteurs() {
		return listeRepartiteurs;
	}






	public void setListeRepartiteurs(NSArray<EOIndividu> listeRepartiteurs) {
		this.listeRepartiteurs = listeRepartiteurs;
	}






	public EOIndividu getCurrentRepartiteur() {
		return currentRepartiteur;
	}






	public void setCurrentRepartiteur(EOIndividu currentIndividue) {
		this.currentRepartiteur = currentIndividue;
	}






	public EOIndividu getSelectedRepartiteur() {
		return selectedRepartiteur;
	}






	public void setSelectedRepartiteur(EOIndividu selectedRepartiteur) {
		this.selectedRepartiteur = selectedRepartiteur;
	}
	
}