package org.cocktail.peche.components.enseignants;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.OngletComposante;
import org.cocktail.peche.components.controlers.IEnseignantsVacatairesCtrl;
import org.cocktail.peche.components.enseignants.services.IServiceOngletEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.CktlAjaxUtils;
import er.extensions.eof.ERXQ;

public abstract class CktlPecheOngletRepartServiceAbstract extends CktlPecheBaseComponent {

	private EOService service;
	private EORepartService repartService;
	private String currentDates;
	
	private NSArray<OngletComposante> listeOngletComposantes = new NSMutableArray<OngletComposante>();
	
	@Inject
	private IServiceOngletEnseignant serviceOngletEnseignantVacataire;
	
	
	
	public CktlPecheOngletRepartServiceAbstract(WOContext context) {
		super(context);
	}

	

	public String actionChangerOnglet() {
		return "changerOnglet";
	}
	
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		initialiserListeOngletComposante();
		gererCreationRepartServiceTousOnglets();
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/ficheService.css");
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/ongletComposante.css");
	}
	
	
	/**
	 * 
	 */
	private void gererCreationRepartServiceTousOnglets() {
		
		// gestion du repartService correspondant à l'onglet sélectionné
		gererCreationRepartServiceOngletSelectionne();
		// gestion des repartService correspondant aux autres onglets
		gererCreationRepartServiceAutresOnglets();
		
		edc().saveChanges();
	}

	private void gererCreationRepartServiceOngletSelectionne() {
		
		// gestion du repartService correspondant à l'onglet sélectionné
		if (this.repartService == null || this.repartService.toStructure() == null) {
			IStructure structure = serviceOngletEnseignantVacataire.recupererStructureOngletSelectionne(getOngletComposanteSelectionne());
			if (getRepartService() == null && structure != null && (getFonctionRepartiteur() || getFonctionDirecteurComposante() || getFonctionDirecteurDepartement())) {
				gererCreationTypeRepartService(structure);
			} 
		}
		
	}
	
	private void gererCreationRepartServiceAutresOnglets() {
		
		NSArray<EORepartService> listeRepartService = getService().toListeRepartService();
		for (OngletComposante ongletComposante : listeOngletComposantes) {
			String code = ongletComposante.getCode();
			boolean estRepartServiceDejaExistant = false;
			if (!Constante.ONGLET_COMPOSANTE_TOUS_CODE.equals(code) && !getOngletComposanteSelectionne().getCode().equals(code)) {
				for (EORepartService eoRepartService : listeRepartService) {
					if ((eoRepartService.toStructure() != null) 
							&& (code.equals(eoRepartService.toStructure().cStructure()))) {
						estRepartServiceDejaExistant = true;
					}
				}
				if (!estRepartServiceDejaExistant) {
					gererCreationTypeRepartService(getServiceOngletEnseignantVacataire().recupererStructureOngletSelectionne(ongletComposante));
				}
			}
		}
		
	}
	
	protected abstract void gererCreationTypeRepartService(IStructure structure);
	
	
	/**
	 * Renvoie le service de l'enseignant
	 * @return un service
	 */
	public EOService getService() {
		return service;
	}

	public void setService(EOService service) {
		this.service = service;
	}

	/**
	 * Enregistre le commentaire de la fiche de l'enseignant
	 */
	public void enregistrerCommentaireFiche() {
		try {
			PecheApplicationUser appUser = getPecheSession().getApplicationUser();
			EOPersonne personne = appUser.getPersonne();

			service.majDonnesAuditModification(personne);
			edc().saveChanges();

			getPecheSession().addSimpleInfoMessage(message("ficheService.message.commentaire.sauvegarde.titre"), message("ficheService.message.commentaire.sauvegarde.detail"));
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}

	
	/**
	 * Est-ce que le bloc "Répartition REH hors offre de formation" doit être présent  ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean hasDroitShowRepartitionRehVacataire() {
		return getAutorisation().hasDroitShowSaisieRehVacataire();
	}

	public String getCurrentDates() {
		return currentDates;
	}

	public void setCurrentDates(String currentDates) {
		this.currentDates = currentDates;
	}

	public NSArray<OngletComposante> getListeOngletComposantes() {
		return listeOngletComposantes;
	}

	public void setListeOngletComposantes(NSArray<OngletComposante> listeOngletComposantes) {
		this.listeOngletComposantes = listeOngletComposantes;
	}

	/**
	 * 
	 * @return null
	 */
	public WOActionResults changerOnglet() {
		return null;
	}
	
	/**
	 * Gestion du choix de l'onglet sélectionné par défaut à l'arrivée sur sa fiche de service
	 * @return
	 */
	private OngletComposante determinerOngletComposanteDefaut() {
		
		if (getPecheSession().getOngletComposanteSelectionne() != null) {
			// 1er cas : un onglet a déjà sélectionné précédemment
			// on vérifie qu'un repartService existe bien pour cet onglet (cas de l'enseignant allant sur une fiche de service n'existant pas)
			for (OngletComposante ongletComposante :  this.listeOngletComposantes) {
				if (ongletComposante.equals(getPecheSession().getOngletComposanteSelectionne())) {
					NSArray<EORepartService> listeRepartService = getService().toListeRepartService();
					for (EORepartService eoRepartService : listeRepartService) {
						if (eoRepartService.toStructure() != null && eoRepartService.toStructure().cStructure().equals(getPecheSession().getOngletComposanteSelectionne().getCode())) {
							return ongletComposante;
						}
					}					
				}
			}
		}		
		
		if (getFonctionRepartiteur()) {			
			IStructure structure = getPecheSession().getPecheAutorisationCache().getStructurePrincipaleVacataire();
			if (structure != null) {
				OngletComposante ongletComposante = serviceOngletEnseignantVacataire.recupererOngletComposanteParCode(structure.cStructure(), this.listeOngletComposantes);
				if (ongletComposante != null) {
					return ongletComposante;
				}
			} else {
				return serviceOngletEnseignantVacataire.recupererOngletComposanteParCode(Constante.ONGLET_COMPOSANTE_TOUS_CODE, this.listeOngletComposantes);
			}
		}		

		return serviceOngletEnseignantVacataire.recupererOngletComposanteParCode(Constante.ONGLET_COMPOSANTE_TOUS_CODE, this.listeOngletComposantes);
	}

	/**
	 * 
	 */
	private void initialiserListeOngletComposante() {
		
		Map<String, IStructure> mapCodeLibelleStructure = new HashMap<String, IStructure>();
		
		this.listeOngletComposantes.clear();

		this.listeOngletComposantes.add(new OngletComposante(Constante.ONGLET_COMPOSANTE_TOUS_CODE, Constante.ONGLET_COMPOSANTE_TOUS_LIBELLE));
		
		if (getFonctionRepartiteur()) {
			mapCodeLibelleStructure = gererInitialiserListeOngletRepartiteur();
		} else {
			mapCodeLibelleStructure = gererInitialiserListeOngletAutre();
		}
		
		for (String code : mapCodeLibelleStructure.keySet()) {
			IStructure structure = mapCodeLibelleStructure.get(code);
			String libelleOnglet; 
			if (StringCtrl.isEmpty(structure.lcStructure())) {
				libelleOnglet = structure.llStructure();
			} else {
				libelleOnglet = structure.lcStructure();
			}
			this.listeOngletComposantes.add(new OngletComposante(code, libelleOnglet));
		}

		setOngletComposanteSelectionne(determinerOngletComposanteDefaut());		

	}

	
	/**
	 * 
	 * @param
	 */
	private Map<String, IStructure> gererInitialiserListeOngletAutre() {
		return getServiceOngletEnseignantVacataire().recupererMapCodeStructure(getService(), null, false, true);		
	}
	
	
	/**
	 * 
	 * @return repartService
	 */
	public EORepartService getRepartService() {
		if (this.repartService == null && this.getOngletComposanteSelectionne() != null) {
			EOQualifier qualifier = ERXQ.and(ERXQ.equals(EORepartService.TO_STRUCTURE_KEY, getServiceOngletEnseignantVacataire().recupererStructureOngletSelectionne(getOngletComposanteSelectionne())),
					ERXQ.equals(EORepartService.TO_SERVICE_KEY, getService()));
			
			this.repartService = EORepartService.fetchEORepartService(edc(), qualifier);
		}
		return repartService;
	}
	
	

	public IServiceOngletEnseignant getServiceOngletEnseignantVacataire() {
		return serviceOngletEnseignantVacataire;
	}
	
	@Override
	public void awake() {
	    service = (EOService) valueForBinding("service");
        repartService = (EORepartService) valueForBinding("repartService");
    }
		
	/**
	 * 
	 * @param 
	 */
	protected Map<String, IStructure> gererInitialiserListeOngletRepartiteur() {
		return getServiceOngletEnseignantVacataire().recupererMapCodeStructure(getService(), getPecheSession().getPecheAutorisationCache().getStructuresPourVacataires(getControleur(), getService()), true, true);
	}



	public abstract IEnseignantsVacatairesCtrl getControleur();

	
	

	
	
}
