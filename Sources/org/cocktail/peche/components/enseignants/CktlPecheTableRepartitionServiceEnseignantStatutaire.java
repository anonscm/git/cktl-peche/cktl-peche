package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

/**
 * Classe permettant l'affichage de la liste des affectations d'un intervenant
 * dans l'offre de formation
 *
 * @author Yannick MAURAY
 * @author Chama LAATIK
 *
 */
public class CktlPecheTableRepartitionServiceEnseignantStatutaire extends CktlPecheTableRepartitionEnseignantStatutaire {

	private static final long serialVersionUID = 5589248537831973292L;
		
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionServiceEnseignantStatutaire(WOContext context) {
        super(context);
    }

	@Override
	protected String getTableKey() {
		return EOServiceDetail.COMPOSANT_AP_KEY;
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			CktlPecheFormServiceEnseignantStatutaire page = (CktlPecheFormServiceEnseignantStatutaire) pageWithName(CktlPecheFormServiceEnseignantStatutaire.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(getSelectedObject());
			page.setEnseignant(getEnseignant());
			page.setRepartService(getRepartService());
			return page;
		} else {
			return null;
		}
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantStatutaire page = (CktlPecheFormServiceEnseignantStatutaire) pageWithName(CktlPecheFormServiceEnseignantStatutaire.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setEnseignant(getEnseignant());
		page.setRepartService(getRepartService());
		page.setRepartitionREH(false);
		return page;
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults deleteAction() {
		try {
			if (getSelectedObject() != null) {
				if (isEnseignantAutreComposante(getSelectedObject())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				
				if ((getSelectedObject().heuresPrevues() != 0) && (isCircuitDefinitif())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				
				//supprime la répartition
				EORepartEnseignant.supprimerRepartition(getSelectedObject(), edc());
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				getRepartService().toService().majDonnesServiceReparti(edc(), 
						getPecheSession().getApplicationUser().getPersonne(), 
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				getControleur().recalculerIndicateurValidationAuto(getRepartService());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
		}
		AjaxUpdateContainer.updateContainerWithID("serviceHETD", context());
		return doNothing();
	}

	/**
	 * Renvoie vrai si 
	 *    le répartiteur n'appartient pas à la meme composante que l'enseignant
	 * et le service détail n'est pas en attente (soit Null soit Accepté/Refusé) 
	 * 
	 * @param serviceDetail : détail du service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean isEnseignantAutreComposante(EOServiceDetail serviceDetail) {
		if ((serviceDetail.service().enseignant() != null) && (serviceDetail.service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(
					serviceDetail.service().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
							getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))
					&& !(Constante.ATTENTE.equals(serviceDetail.etat()));
		}
		return false;
	}

	public boolean isCircuitDefinitif() {
		return !getControleur().isCircuitPrevisionnel(getRepartService().toDemande());
	}

	
}
