package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.services.IServiceOngletEnseignant;
import org.cocktail.peche.components.enseignements.ListeAP;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiement;
import org.cocktail.peche.outils.OutilsValidation;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe permettant d'ajouter ou modifier les affectations de la fiche de
 * service de l'enseignant vacataire.
 * 
 * @author Chama LAATIK
 * 
 */
public class CktlPecheFormServiceEnseignantVacataire extends
		CktlPecheFormServiceEnseignantAbstract implements BasicPecheObservateur {

	private static final long serialVersionUID = -6566550377890898229L;

	private EOAP currentComposantAP;

	private EORepartService repartService; 
	
	private EnseignantsVacatairesCtrl controleur;
	private UECtrl controleurUE;
	private ComposantCtrl controleurComposant;

	private boolean modeReh;
	
	private NSArray<EOPeriode> listePeriode;
	private EOPeriode currentPeriode;
	private EOPeriode selectedPeriode;

	private EOServiceDetail currentServiceDetail;
	private NSArray<EOServiceDetail> listeServiceDetail;
	private NSArray<EOServiceDetail> listeServiceDetailDejaCree;
	private NSArray<EOServiceDetail> listeServiceDetailCree;
		
	private boolean modeValidationUeEc;
	private String typeFicheAValider;
	private boolean modeParListeRepartition;
		
	@Inject
	private IServiceOngletEnseignant serviceOngletEnseignantVacataire;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheFormServiceEnseignantVacataire(WOContext context) {
		super(context);
		this.controleurUE = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		this.controleurComposant = new ComposantCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		this.controleur = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		this.controleur.addObservateur(this);

		// initialisation de la liste des périodes
		listePeriode = EOPeriode.fetchEOPeriodes(
				edc(),
				ERXQ.equals(EOPeriode.TYPE_KEY, EOPecheParametre.getChoixTypePeriode(edc())
								+ getPecheSession().getPecheParametres().getCodeFormatAnnee()),
				EOPeriode.ORDRE_AFFICHAGE.ascs());		
		
	}

	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauServiceContainerId() {
		return getComponentId() + "_nouveauServiceContainerId";
	}
	
	/**
	 * @return l'identifiant de la description reh
	 */
	public String descriptionRehId() {
		return getComponentId() + "_descriptionRehId";
	}

	@Override
	protected String getFormTitreCreationKey() {
		if (isCircuitDefinitif()) {
			return Messages.SERVICEREALISE_FORM_TITRE_AJOUTER;
		}
		return Messages.SERVICE_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		if (isCircuitDefinitif()) {
			return Messages.SERVICEREALISE_FORM_TITRE_MODIFIER;
		}
		return Messages.SERVICE_FORM_TITRE_MODIFIER;
	}

	@Override
	protected WOActionResults pageGestion() {
		if (isModeValidationUeEc()) {
			ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
			page.setTypeFicheAValider(getTypeFicheAValider());
			page.setModeValidationParUeEc(true);
			page.setParentAP((EOComposant) editedObject().composantAP().parents().get(0));
			return page;
		} 
		
		if (isModeParListeRepartition()) {
			WOComponent pageRetour = (WOComponent) getPecheSession().objectForKey(Session.BOUTONRETOUR);
			return pageRetour;
		}
		
		
		CktlPecheFicheServiceEnseignantVacataire page = (CktlPecheFicheServiceEnseignantVacataire) pageWithName(CktlPecheFicheServiceEnseignantVacataire.class
					.getName());
			page.setService(getRepartService().toService());
			page.setEditedObject(getRepartService().toService().enseignant());
			return page;
		
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		controlerSaisie();
		boolean depassementHeuresMaquette = false;
		String  messageDepassement = "";
		
		if (controleur.getAP() != null) {
			if (modeEdition == ModeEdition.CREATION)  {
				for (EOServiceDetail detail : getListeServiceDetail()) {
					if ((detail.heuresPrevues() != 0.0) || (detail.heuresRealisees() != 0.0)) {
						try {
							controleHeuresMaquette(detail);
							controleSaisieHeures(detail);
						} catch (ValidationException e) {
							depassementHeuresMaquette = true;
							messageDepassement = e.getMessage();
						}
					}
				}
			}
		} else if (isModeReh()) {
			// REH
		} else {
			//Modification
			if (editedObject()!=null) {
				controleHeuresMaquette(editedObject());
			}
		}

		if (depassementHeuresMaquette) {			
			throw new ValidationException(messageDepassement);			
		}
		// En création avec un enseignement connu
		if (controleur.getAP() != null) {
			if (modeEdition == ModeEdition.CREATION) {

				for (EOServiceDetail detail : getListeServiceDetail()) {
					if ((detail.heuresPrevues() != 0.0) || (detail.heuresRealisees() != 0.0)) {
						detail.setService(getRepartService().toService());						
						detail.setToRepartService(getRepartService());
						detail.setToStructureRelationship(composanteEnCours());
						detail.majDonnesAuditCreation(personne);

						majCommentaire(detail, editedObject().commentaire());
					} else {
						// Si pas d'heures saisies, on n'enregistre pas cette ligne
						if (detail != null && detail.editingContext() != null
								&& detail.heuresPrevues() == 0.0 && detail.heuresRealisees() == 0.0) {
							detail.editingContext().deleteObject(detail);
						}	
					}
				}

				//on supprime l'editedObject car déjà traité
				if (editedObject() != null && editedObject().editingContext() != null) {
					editedObject().editingContext().deleteObject(editedObject());
				}
			} 
		} else if (isModeReh()) {
			if (modeEdition == ModeEdition.CREATION) {
				editedObject().setService(getRepartService().toService());				
				editedObject().setToRepartService(getRepartService());
				editedObject().setToStructureRelationship(composanteEnCours());
				editedObject().majDonnesAuditCreation(personne);
			}
		} 

		//On controle les heures saisies de l'objet si on est en modification(REH + AP) ou en création du REH
		if (!isCreationAP()) {
			controleSaisieHeures(editedObject());
		}
		if (editedObject() != null) {
			majCommentaire(editedObject(), editedObject().commentaire());
			editedObject().majDonnesAuditModification(personne);
			if (getRepartService().toService() != null) {
				getRepartService().toService().majDonnesServiceReparti(editedObject().editingContext(), personne, 
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			} else if (editedObject().service() != null) {
				editedObject().service().majDonnesServiceReparti(editedObject().editingContext(), personne, 
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

			}
		}
		getControleur().recalculerIndicateurValidationAuto(getRepartService());
	}
	
	private void controleSaisieHeures(EOServiceDetail serviceDetail) {
		
		if (getListeServiceDetailDejaCree() != null && getListeServiceDetailDejaCree().contains(serviceDetail)) {
			return;
		}
		
		// Si la personne a le droit de saisir les heures à payer
		if (getAutorisation().hasDroitUtilisationSaisieHeuresAPayer()) {
			// Pas de saisie d'heures à payer si la période est à null
			if ((serviceDetail.heuresAPayer() != null) && (serviceDetail.toPeriode() == null)) {
				throw new ValidationException(Constante.REPARTITION_ERREUR_HEURES_A_PAYER_INTERDIT_SI_PERIODE_NULL);
			}

			BigDecimal nbHeuresRealisees = new BigDecimal(serviceDetail.heuresRealisees());
			BigDecimal totalNbHeures = getHeuresAPayer(serviceDetail).add(getHeuresPayees(serviceDetail));

			// Les heures réalisées doivent être supérieures au total des heures payées et à payer
			// Ainsi, les heures à payer doivent être inférieures au total des heures réalisées et des heures payées
			if (nbHeuresRealisees.compareTo(totalNbHeures) < 0) {
				throw new ValidationException(Constante.HEURES_REALISEES_INFERIEUR_TOTAL_HEURES_PAYEES_ET_HEURES_A_PAYER);
			}

			// les heures à payer doivent être supérieures aux heures en attente de paiement ==> c'est le plancher de la mise en paiement
			if (getHeuresAPayer(serviceDetail).compareTo(getHeuresEnAttentePaiement(serviceDetail)) < 0) {
				throw new ValidationException(Constante.HEURES_A_PAYER_INFERIEUR_HEURES_EN_ATTENTE);
			}
		} else { // pas le droit de saisir les heures à payer
			if (isParentAPDansEtablissement() || isModeReh()) {
				BigDecimal heuresEnPaiement = getHeuresEnAttentePaiement(serviceDetail).add(getHeuresPayees(serviceDetail));
				if (BigDecimal.valueOf(editedObject().heuresRealisees()).compareTo(heuresEnPaiement) < 0) {
					throw new ValidationException(String.format(Constante.HEURES_REALISEES_INFERIEUR_HEURES_EN_PAIEMENT, heuresEnPaiement));
				} else {
					// les heures à payer sont égales aux heures réalisées
					BigDecimal heuresAPayer = BigDecimal.valueOf(editedObject().heuresRealisees()).subtract(heuresEnPaiement);
					if (heuresAPayer.compareTo(BigDecimal.ZERO) <= 0) {
						heuresAPayer = null;
					}
					editedObject().setHeuresAPayer(heuresAPayer);
				}
			}
		}
	}
	
	/**
	 * Contrôler la saisie utilisateur.
	 * 
	 * Lève une exception si un contrôle est KO.
	 */
	private void controlerSaisie() throws ValidationException {
		//En cas de répartition de service, on vérifie qu'on a selectionné un AP 
		if (modeCreation() && !isModeReh() && controleur.getAP() == null) {
			throw new ValidationException(Constante.REPARTITION_ERREUR_AP_NON_RENSEIGNE);
		}
		
		// on vérifie si un REH a été saisi
		if (modeCreation() && isModeReh() && controleur.getReh() == null) {
			throw new ValidationException(Constante.LISTE_REH_NON_RENSEIGNE);
		}
	}
	
	/**
	 * Contrôle que les heuresRepartition <= heuresMaquettes
	 * Lève une exception si un contrôle est KO.
	 */
	private void controleHeuresMaquette(EOServiceDetail serviceDetail) {

		if (getListeServiceDetailDejaCree() != null && getListeServiceDetailDejaCree().contains(serviceDetail)) {
			return;
		}

		if (serviceDetail.composantAP() != null) {
			if  (getModeEdition() == ModeEdition.CREATION) {
				if ((!isCircuitDefinitif()) && (serviceDetail.heuresPrevues() > getHeuresResteARepartir(serviceDetail))) {				
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

				if ((isCircuitDefinitif()) && (serviceDetail.heuresRealisees() > getHeuresResteARealiser(serviceDetail))) {
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

			} else {
				float heuresRepartition = getTotalHeuresPrevues(serviceDetail);
				float heuresRealisees = getTotalHeuresRealisees(serviceDetail);
				float heuresMaquette = getHeuresMaquette(serviceDetail.composantAP());

				if ((!isCircuitDefinitif()) && (heuresRepartition > heuresMaquette)) {				
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

				if ((isCircuitDefinitif()) && (heuresRealisees > heuresMaquette)) {
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}
			}
		}
	}
	
	/**
	 * Retourne la composante associée à la composante sélectionnée
	 * @return structure en cours
	 */
	public IStructure composanteEnCours() {
		return serviceOngletEnseignantVacataire.recupererStructureOngletSelectionne(getPecheSession().getOngletComposanteSelectionne()); 
	}

	@Override
	protected EOServiceDetail creerNouvelObjet() {
		EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(edc());
		return serviceDetail;
	}

	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct suivant le mode
	 * d'édition
	 * 
	 * @return affichage
	 */
	public String affichageAPEtParent() {
		if ((getModeEdition() == ModeEdition.MODIFICATION)) {
			return this.editedObject().composantAP().typeAP().code() + " - " + controleurComposant.affichageAPEtParent(this.editedObject().composantAP());
			
		} else {
			return controleurComposant.affichageAPEtParent(getCurrentComposantAP());
		}
	}
	
	/**
	 * @return code et libellé du parent direct de l'AP
	 */
	public String affichageParentAP() {
		if (controleur.getAP() != null) {
			return controleurComposant.getParentAPCodeEtLibelle(controleur.getAP());
		}
		
		return null;
	}

	public boolean isCircuitDefinitif() {
		return !this.controleur.isCircuitPrevisionnel(getDemande());
	}

	public boolean isNbHeuresDejaCrees() {
		return getListeServiceDetailDejaCree().contains(getCurrentServiceDetail());
	}
	
	/**
	 * Renvoie le total des heures maquette de de l'AP ==> Si l'AP est
	 * mutualisé, le paramètre false permet de calculer sa charge propre sans
	 * prendre en compte le nombre de parent
	 * 
	 * @param ap : AP
	 * @return les heures maquette
	 */
	private float getHeuresMaquette(EOAP ap) {
		return this.controleurUE.getHeuresMaquette(ap, false);
	}

	/**
	 * Renvoie le total des heures prévues
	 * @param ap : AP
	 * @return les heures prévues
	 */
	private float getTotalHeuresPrevues(EOServiceDetail serviceDetail) {
		return this.controleurUE.getTotalHeuresPrevues(serviceDetail.composantAP(), getModeEdition(),
				serviceDetail.heuresPrevues(), false);
	}
	
	private float getTotalHeuresRealisees(EOServiceDetail serviceDetail) {
		return this.controleurUE.getTotalHeuresRealisees(serviceDetail.composantAP(), getModeEdition(),
				serviceDetail.heuresRealisees(), false);
	}

	/**
	 * @return heures en attente de paiement
	 */
	private BigDecimal getHeuresEnAttentePaiement(EOServiceDetail serviceDetail) {
		return MiseEnPaiement.getInstance().calculerAPayerEnCours(
				editedObject().service(), serviceDetail);
	}

	/**
	 * @return heures à payer, si null renvoie 0
	 */
	private BigDecimal getHeuresAPayer(EOServiceDetail serviceDetail) {
		return OutilsValidation.retournerBigDecimalNonNull(serviceDetail.heuresAPayer());
	}

	/**
	 * @return heures payées, si null renvoie 0
	 */
	private BigDecimal getHeuresPayees(EOServiceDetail serviceDetail) {
		return OutilsValidation.retournerBigDecimalNonNull(serviceDetail.heuresPayees());
	}

	public float getHeuresMaquette() {
		return this.controleurUE.getHeuresMaquette(editedObject().composantAP(), false);
	}
	
	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getHeuresResteARealiser() {
		if (editedObject() != null & editedObject().composantAP()!=null) {
			return controleurUE.getNbHeuresResteARealiser(editedObject().composantAP());
		}
		return 0f;
	}
	
	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getHeuresResteARepartir() {
		if (editedObject() != null & editedObject().composantAP()!=null) {
			return controleurUE.getNbHeuresResteARepartir(editedObject().composantAP());
		}
		return 0f;
	}
	
	/**
	 * Est-ce que l'UE/EC est dans l'établissement ?
	 * 
	 * @return Vrai/Faux
	 */
	public boolean isParentAPDansEtablissement() {
		boolean parentAPDansEtablissement = false;

		if (editedObject().composantAP() != null) {
			EORepartUefEtablissement repartition = EORepartUefEtablissement
					.rechercherEtablissementPourUE(edc(),
							this.controleurComposant.getParentAP(editedObject().composantAP()));

			if (repartition == null) {
				parentAPDansEtablissement = true;
			}
		}
		return parentAPDansEtablissement;
	}

	/**
	 * action de notifier
	 */
	public void notifier() {
		if (isModeReh()) {
			EOReh reh = this.controleur.getReh();
			editedObject().setReh(reh);
		} else {
			EOAP ap = this.controleur.getAP();
			editedObject().setComposantAP(ap);
			majListeServiceDetails();
		}
	}
	
	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		if (getCurrentServiceDetail() != null) {
		return controleurUE.getNbHeuresResteARepartir(getCurrentServiceDetail().composantAP());
		}
		return 0f;
	}
	
	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser() {
		if (getCurrentServiceDetail() != null) {
			return controleurUE.getNbHeuresResteARealiser(getCurrentServiceDetail().composantAP());
		}
		return 0f;
	}
	
	public float getHeuresResteARealiser(EOServiceDetail serviceDetail) {
		return controleurUE.getNbHeuresResteARealiser(serviceDetail.composantAP());
	}
	
	public float getHeuresResteARepartir(EOServiceDetail serviceDetail) {
		return controleurUE.getNbHeuresResteARepartir(serviceDetail.composantAP());
	}
	/**
	 * @return les heures maquette par AP
	 */
	public float getNbHeuresMaquette() {
		return controleurUE.getHeuresMaquette(getCurrentServiceDetail().composantAP(),false);
	}
	
	/**
	 * Est-ce qu'on affiche les détails de l'AP
	 * @return Vrai/Faux
	 */
	public boolean isAfficherDetailAP() {
		return (controleur.getAP() != null);
	}
	
	/**
	 * Est-ce qu'on affiche les détails de l'AP
	 * @return Vrai/Faux
	 */
	public boolean isCreationAP() {
		return (modeCreation() && !isModeReh());
	}

	/**
	 * met à jout les heures à payer à partir des heures réalisées
	 */
	public void majHeuresAPayer() {
		if (getCurrentServiceDetail() != null) {
			getCurrentServiceDetail().setHeuresAPayer(new BigDecimal(getCurrentServiceDetail().heuresRealisees()));
		} else if (editedObject() != null) {
			editedObject().setHeuresAPayer(new BigDecimal(editedObject().heuresRealisees()));
		}
	}
	
	/**
	 * Met à jour le commentaire de la répartition du service
	 * @param detail : répartition du service
	 * @param commentaire : commentaire
	 */
	private void majCommentaire(EOServiceDetail detail, String commentaire) {
		if (!StringCtrl.isEmpty(commentaire)) {
			detail.setCommentaire(commentaire);
			detail.setDateCommentaire(new NSTimestamp());
		}
	}

	/* ========== Getters & Setters ========== */

	public EOAP getCurrentComposantAP() {
		return currentComposantAP;
	}

	public void setCurrentComposantAP(EOAP currentComposantAP) {
		this.currentComposantAP = currentComposantAP;
	}

	public EnseignantsVacatairesCtrl getControleur() {
		return controleur;
	}

	public void setControleur(EnseignantsVacatairesCtrl controleur) {
		this.controleur = controleur;
	}

	public NSArray<EOPeriode> getListePeriode() {
		return listePeriode;
	}

	public void setListePeriode(NSArray<EOPeriode> listePeriode) {
		this.listePeriode = listePeriode;
	}

	public EOPeriode getCurrentPeriode() {
		return currentPeriode;
	}

	public void setCurrentPeriode(EOPeriode currentPeriode) {
		this.currentPeriode = currentPeriode;
	}

	public EOPeriode getSelectedPeriode() {
		return selectedPeriode;
	}

	public void setSelectedPeriode(EOPeriode selectedPeriode) {
		this.selectedPeriode = selectedPeriode;
	}

	public boolean isModeReh() {
		return modeReh;
	}

	public void setModeReh(boolean modeReh) {
		this.modeReh = modeReh;
	}
	
	/**
	 * @return demande en cours
	 */
	public EODemande getDemande() {
		return controleur.getDemande(getRepartService());
	}

	public boolean isAfficherInfosReh() {
		return this.editedObject().reh() != null;
	}

	@Override
	public EORepartService getRepartService() {
		return repartService;
	}

	public void setRepartService(EORepartService repartService) {
		this.repartService = repartService;
	}

	public EOServiceDetail getCurrentServiceDetail() {
		return currentServiceDetail;
	}

	public void setCurrentServiceDetail(EOServiceDetail currentServiceDetail) {
		this.currentServiceDetail = currentServiceDetail;
	}

	public NSArray<EOServiceDetail> getListeServiceDetail() {
		return listeServiceDetail;
	}

	public void setListeServiceDetail(NSArray<EOServiceDetail> listeServiceDetail) {
		this.listeServiceDetail = listeServiceDetail;
	}

	public boolean isModeValidationUeEc() {
		return modeValidationUeEc;
	}

	public void setModeValidationUeEc(boolean modeValidationUeEc) {
		this.modeValidationUeEc = modeValidationUeEc;
	}

	public String getTypeFicheAValider() {
		return typeFicheAValider;
	}

	public void setTypeFicheAValider(String typeFicheAValider) {
		this.typeFicheAValider = typeFicheAValider;
	}

	public NSArray<EOServiceDetail> getListeServiceDetailDejaCree() {
		return listeServiceDetailDejaCree;
	}

	public void setListeServiceDetailDejaCree(
			NSArray<EOServiceDetail> listeServiceDetailDejaCree) {
		this.listeServiceDetailDejaCree = listeServiceDetailDejaCree;
	}

	public NSArray<EOServiceDetail> getListeServiceDetailCree() {
		return listeServiceDetailCree;
	}

	public void setListeServiceDetailCree(
			NSArray<EOServiceDetail> listeServiceDetailCree) {
		this.listeServiceDetailCree = listeServiceDetailCree;
	}

	public boolean isModeParListeRepartition() {
		return modeParListeRepartition;
	}

	public void setModeParListeRepartition(boolean modeParListeRepartition) {
		this.modeParListeRepartition = modeParListeRepartition;
	}

}