package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.entity.EOReh;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;

/**
 * Liste des REH éligibles à une répartition
 * @author juliencallewaert
 *
 */
public class CktlPecheTableREH extends CktlPecheTableComponent<EOReh> {
    
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7186565252400592939L;
	private BasicPecheCtrl controleur;
	
	public CktlPecheTableREH(WOContext context) {
        super(context);
        
        getRehs();
        
    }
    
	
	public void getRehs() {
		
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOReh.ENTITY_NAME);
		
		EOQualifier qualifier = EOReh.getQualifierPourListeRehVisibles(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(
				EOReh.LIBELLE_COURT_KEY, EOSortOrdering.CompareAscending));
		
		ERXFetchSpecification<EOReh> fetchSpec = EOReh.fetchSpec().qualify(qualifier).sort(sortOrderings);
		
		dataSource.setFetchSpecification(fetchSpec);
		fetchSpec.setUsesDistinct(true);
		ERXDisplayGroup<EOReh> dg = this.getDisplayGroup();		
		dg.setDataSource(dataSource);		
		dg.fetch();
		
		
	}
	
	
	
	/**
	 * @return Le contrôleur de la page appelante
	 */
	public BasicPecheCtrl getControleur() {
		controleur = (BasicPecheCtrl) valueForBinding("controleur");

		if (controleur == null) {
			throw new NullPointerException("Le controleur ne peut pas être nul");
		}

		return controleur;
	}
	
    
	@Override
	public void setSelectedObject(EOReh selectedObject) {
		super.setSelectedObject(selectedObject);
		getControleur().setSelectedReh(selectedObject);
	}

}