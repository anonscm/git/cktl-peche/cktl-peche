package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;

/**
 * Classe permettant l'affichage de la liste des affectations d'un intervenant
 * dans l'offre de formation
 *
 * @author Yannick MAURAY
 * @author Chama LAATIK
 *
 */
public class CktlPecheTableRepartitionServiceEnseignantVacataire extends CktlPecheTableRepartitionEnseignantVacataire {

	private static final long serialVersionUID = -5625211120024164786L;


	private boolean isAffichageHeuresPresentielles = true;
	private boolean isAffichageHeuresHETD = false;
	private ComposantCtrl controleurComposant;


	private BigDecimal sousTotalHeuresPrevuesHETD = BigDecimal.valueOf(0);
	private BigDecimal sousTotalHeuresRealiseesHETD = BigDecimal.valueOf(0);
	private BigDecimal sousTotalHeuresAPayerHETD = BigDecimal.valueOf(0);
	private BigDecimal sousTotalHeuresPayeesHETD = BigDecimal.valueOf(0);
	private BigDecimal sousTotalHeuresRestantesHETD = BigDecimal.valueOf(0);

	private boolean estSousTotauxHETDDejaCalcules; 
	
	
	@Override
	protected String getTableKey() {
		return EOServiceDetail.COMPOSANT_AP_KEY;
	}


	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionServiceEnseignantVacataire(WOContext context) {
		super(context);
		this.controleurComposant = new ComposantCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		estSousTotauxHETDDejaCalcules = false;
		
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOServiceDetail.ENTITY_NAME);
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.fetch();
	}


	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}


	@Override
	public WOActionResults modifierAction() {
		CktlPecheFormServiceEnseignantVacataire results = (CktlPecheFormServiceEnseignantVacataire) super.modifierAction();
		if (results != null) {
			results.setModeReh(false);
		}
		return results;
	}



	@Override
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantVacataire results = (CktlPecheFormServiceEnseignantVacataire) super.ajouterAction();
		if (results != null) {
			results.setModeReh(false);
		}
		return results;
	}

	/**
	 * @return les heures à payer en HETD par AP
	 */
	public BigDecimal heuresAPayerHETD() {
		BigDecimal heuresAPayerHETD = getControleur().getServiceHeuresHETDParAP(getService(), getCurrentItem().composantAP(), getCurrentItem().heuresAPayer());
		if (!estSousTotauxHETDDejaCalcules && heuresAPayerHETD != null) {
			this.sousTotalHeuresAPayerHETD = this.sousTotalHeuresAPayerHETD.add(heuresAPayerHETD);
		}
		return heuresAPayerHETD;
	}

	/**
	 * @return les heures payées en HETD par AP
	 */
	public BigDecimal heuresPayeesHETD() {
		BigDecimal heuresPayeesHETD = getControleur().getServiceHeuresHETDParAP(getService(), getCurrentItem().composantAP(), getCurrentItem().heuresPayees());
		if (!estSousTotauxHETDDejaCalcules && heuresPayeesHETD != null) {
			this.sousTotalHeuresPayeesHETD = this.sousTotalHeuresPayeesHETD.add(heuresPayeesHETD);
		}
		return heuresPayeesHETD;
	}

	/**
	 * @return les heures réalisées en HETD par AP
	 */
	public BigDecimal heuresRealiseesHETD() {
		BigDecimal heuresRealiseesHETD = getControleur().getHeuresPresentielEnHETDParAP(getService(), getCurrentItem().composantAP(), getCurrentItem().heuresRealisees());
		if (!estSousTotauxHETDDejaCalcules && heuresRealiseesHETD != null) {
			this.sousTotalHeuresRealiseesHETD = this.sousTotalHeuresRealiseesHETD.add(heuresRealiseesHETD);
		}
		return heuresRealiseesHETD;
	}

	/**
	 * @return les heures réalisées en HETD par AP
	 */
	public BigDecimal heuresPrevuesHETD() {
		BigDecimal heuresPrevuesHETD = getControleur().getHeuresPresentielEnHETDParAP(getService(), getCurrentItem().composantAP(), getCurrentItem().heuresPrevues());
		if (!estSousTotauxHETDDejaCalcules && heuresPrevuesHETD != null) {
			this.sousTotalHeuresPrevuesHETD = this.sousTotalHeuresPrevuesHETD.add(heuresPrevuesHETD);
		}
		return heuresPrevuesHETD;
	}

	/**
	 * @return le delta des heures en HETD entre les heures payées et les heures réalisées par l'intervenant
	 */
	public BigDecimal deltaHeuresPayeEtRealiseHETD() {
		BigDecimal deltaHeuresPayeEtRealiseHETD = getControleur().getDeltaHeuresPayeEtRealiseHETD(getService(), getCurrentItem().composantAP(), getCurrentItem().heuresPayees(), getCurrentItem().heuresRealisees());
		if (!estSousTotauxHETDDejaCalcules && deltaHeuresPayeEtRealiseHETD != null) {
			this.sousTotalHeuresRestantesHETD = this.sousTotalHeuresRestantesHETD.add(deltaHeuresPayeEtRealiseHETD);
		}
		return deltaHeuresPayeEtRealiseHETD;
	}

	
	
	public boolean isAffichageHeuresHETD() {
		return isAffichageHeuresHETD;
	}

	public void setIsAffichageHeuresHETD(boolean isAffichageHeuresHETD) {
		this.estSousTotauxHETDDejaCalcules = false;
		
		sousTotalHeuresPrevuesHETD = BigDecimal.valueOf(0);
		sousTotalHeuresRealiseesHETD = BigDecimal.valueOf(0);
		sousTotalHeuresAPayerHETD = BigDecimal.valueOf(0);
		sousTotalHeuresPayeesHETD = BigDecimal.valueOf(0);
		sousTotalHeuresRestantesHETD = BigDecimal.valueOf(0);
		
		this.isAffichageHeuresHETD = isAffichageHeuresHETD;
	}

	public boolean isAffichageHeuresPresentielles() {
		return isAffichageHeuresPresentielles;
	}

	public void setIsAffichageHeuresPresentielles(boolean isAffichageHeuresPresentielles) {
		this.estSousTotauxHETDDejaCalcules = true;
		this.isAffichageHeuresPresentielles = isAffichageHeuresPresentielles;
	}

	public String getChoixAffichageHeuresId() {
		return getContainerId() + "_choixAffichage";
	}

	public String getCommentaireServiceId() {
		return getContainerId() + "_service_commentaire";
	}

	/**
	 * met à jour la liste des paiements
	 * @return null
	 */
	public WOActionResults updateListePaiement() {  
		if (isCircuitDefinitif()) {
			AjaxUpdateContainer.updateContainerWithID("listePaiementRepartitionServiceId", context());  
		}
		return null;  
	}

	/**
	 * Renvoie le composant parent direct de l'AP selectionné
	 * @return composant
	 */
	public EOComposant getParentAPSelected() {
		return (EOComposant)controleurComposant.getParentAP(getSelectedObject());
	}

	public BigDecimal getSousTotalHeuresPrevuesHETD() {
		return sousTotalHeuresPrevuesHETD;
	}
	
	public BigDecimal getSousTotalHeuresRealiseesHETD() {
		return sousTotalHeuresRealiseesHETD;
	}
	
	public BigDecimal getSousTotalHeuresAPayerHETD() {
		return sousTotalHeuresAPayerHETD;
	}
	
	public BigDecimal getSousTotalHeuresPayeesHETD() {
		return sousTotalHeuresPayeesHETD;
	}
	
	public BigDecimal getSousTotalHeuresRestantesHETD() {
		return sousTotalHeuresRestantesHETD;
	}


	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		estSousTotauxHETDDejaCalcules = true;
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}

	public String affichageEnseignement() {
		return ComposantAffichageHelper.getInstance().affichageEnseignementParent(getSelectedObject().composantAP());
	}



}