package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * 
 * @author juliencallewaert
 *
 */
public abstract class CktlPecheTableEnseignantsAbstract extends CktlPecheTableComponent<EOActuelEnseignant> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8749368394149113392L;

	/**
	 * 
	 * @param context context
	 */
	public CktlPecheTableEnseignantsAbstract(WOContext context) {
		super(context);
	}
	
	/**
	 * 
	 * @param enseignant enseignant en cours
	 * @return état de la fiche
	 */
	public abstract String etatFiche(EOActuelEnseignant enseignant);
	
	/**
	 * TODO duplication avec CktlPecheTableAPParUeEc.determinerListeEtapesFiltre() voir comment mutualiser
	 * @return liste codes étapes
	 */
	public NSArray<EtapePeche> determinerListeEtapesFiltre() {
		NSArray<EtapePeche> listeCodesEtapesFiltres = new NSMutableArray<EtapePeche>();
		if (getFonctionRepartiteur()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_REPARTITEUR);
		}
		if (getFonctionPresident()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_PRESIDENT);
		}
		if (getFonctionDirecteurCollege()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_COLLEGE);
		}
		if (getFonctionDirecteurComposante()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_COMPOSANTE);
		}
		if (getFonctionDirecteurDepartement()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DIR_DEPARTEM);
		}
		if (getFonctionDrh()) {
			listeCodesEtapesFiltres.add(EtapePeche.VALID_DRH);
		}
		return listeCodesEtapesFiltres;
	}

}
