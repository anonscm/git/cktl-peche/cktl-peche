package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe permet d'afficher une liste des enseignants statutaires.
 *
 * @author Yannick Mauray
 * @author Etienne Ragonneau
 * @author Chama Laatik
 *
 */
public class CktlPecheTableEnseignantsStatutaires extends CktlPecheTableEnseignantsAbstract {

	private static final long serialVersionUID = -5360347026546204499L;

	private String tokenNom;
	private String tokenPrenom;
	private String tokenComposante;
	private String tokenDepartement;
	private String tokenCorps;
	private String nomEnseignantToutesComposantes;
	
	private EnseignantsStatutairesCtrl controleur;
	private EOQualifier qualifier;

	private boolean mesValidations;
	
	private EOService serviceARapprocher;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'éxécution.
	 */
	public CktlPecheTableEnseignantsStatutaires(WOContext context) {
		super(context);
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
	
	
	@Override
	public void awake() {
		super.awake();
		if (hasBinding("typePopulation")) {
			this.controleur.setTypePopulation((String) valueForBinding("typePopulation"));
		}
		if (hasBinding("mesValidations")) {
			setMesValidations(valueForBooleanBinding("mesValidations", false));
		}
		if (hasBinding("serviceARapprocher")) {
			setServiceARapprocher((EOService) valueForBinding("serviceARapprocher"));
		}				
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (getQualifier() == null) {
			getEnseignants();
		} else {
			ERXFetchSpecification<EOActuelEnseignant> fetchSpec = EOActuelEnseignant.fetchSpec().qualify(getQualifier()).sort(getTriParDefaut());
			fetchDisplay(fetchSpec);
		}
		
		NSArray<EOActuelEnseignant> listeEns = filtrerDoublons(this.getDisplayGroup().allObjects());	
		
		// mise en mémoire pour tous les enseignants
		// des colonnes etat, composante, departement et corps/contrat
		// pour utiliser les tris et les filtres		
		for (EOActuelEnseignant enseignant : listeEns) {			
			enseignant.setEtatFiche(etatFiche(enseignant));
			enseignant.getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			enseignant.getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			enseignant.getLibelleCourtCorpsouContrat(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}
	
		this.getDisplayGroup().setObjectArray(listeEns);

		super.appendToResponse(response, context);
	}
	
	/**
	 * Est-ce que cet enseignant peut être rapproché avec un générique ?
	 * <p>
	 * Oui si l'enseignant n'a pas de repartion de service ou si c'est une fiche de service prévisionnelle.
	 * 
	 * @return <code>true</code> si cet enseignant peut être rapproché avec un générique
	 */
	public boolean isRapprochable() {
		if (this.getSelectedObject() == null) {
			return false;
		} 
		EOService service = this.getSelectedObject().getService(controleur.getAnneeUniversitaire());
		if (service == null 
				|| (service != null
				&& (!service.toListeRepartService().isEmpty())
				&& controleur.isCircuitPrevisionnel(controleur.getDemande(service)))) {
			return true;
		}

		return false;
	}

	
    /**
     * Retourne la fiche de service de l'enseignant
     * @return la page.
     */
	public WOActionResults getLinkToFicheEnseignant() {
		if (checkEnseignantAffecte(controleur, getCurrentItem())) {
			return null;
		}

		CktlPecheFicheServiceEnseignantStatutaire page = (CktlPecheFicheServiceEnseignantStatutaire) 
				pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
		page.setEditedObject(getCurrentItem());
		this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
		return page;
	}
	
	public String getTokenNom() {
		return tokenNom;
	}

	public void setTokenNom(String tokenNom) {
		this.tokenNom = tokenNom;
	}

	public String getTokenPrenom() {
		return tokenPrenom;
	}

	public void setTokenPrenom(String tokenPrenom) {
		this.tokenPrenom = tokenPrenom;
	}

	public String getTokenComposante() {
		return tokenComposante;
	}

	public void setTokenComposante(String tokenComposante) {
		this.tokenComposante = tokenComposante;
	}

	public String getTokenDepartement() {
		return tokenDepartement;
	}

	public void setTokenDepartement(String tokenDepartement) {
		this.tokenDepartement = tokenDepartement;
	}


	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter.
	 *
	 * @return null.
	 */
	public WOActionResults filtrer() {
		EOQualifier qualifierNom = null;
		EOQualifier qualifierPrenom = null;
		EOQualifier qualifierComposante = null;
		EOQualifier qualifierDepartement = null;
		EOQualifier qualifierCorps = null;
							
		if (!StringCtrl.isEmpty(tokenNom)) {
			qualifierNom = EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(tokenNom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));
		}		
	
		if (!StringCtrl.isEmpty(tokenPrenom)) {
			qualifierPrenom = EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).contains(tokenPrenom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));			
		} 
	
		if (!StringCtrl.isEmpty(tokenComposante)) {
			qualifierComposante = EOActuelEnseignant.COMPOSANTE.dot(EOStructure.LC_STRUCTURE_KEY).contains(tokenComposante);
		}

		if (!StringCtrl.isEmpty(tokenDepartement)) {
			qualifierDepartement = EOActuelEnseignant.DEPARTEMENT_ENSEIGNEMENT.dot(EOStructure.LC_STRUCTURE_KEY).contains(tokenDepartement);
		}
		
		if (!StringCtrl.isEmpty(tokenCorps)) {			
			qualifierCorps = EOActuelEnseignant.LIBELLECOURTCORPSOUCONTRAT.contains(tokenCorps);
		}
		
		filtrerDisplayGroup(ERXQ.and(qualifierNom, qualifierPrenom, qualifierComposante, qualifierDepartement, qualifierCorps));

		return doNothing();
	}

	/**
	 * Renvoie le service statutaire de l'enseignant
	 *
	 * @return service statutaire.
	 */
	public double getServiceStatutaire() {
		return controleur.getServiceStatutaire(getCurrentItem());
	}

	/**
	 * renvoie le total des heures d'affectation que l'enseignant a réalisé
	 *
	 * @return nombre d'heures
	 */
	public double getServiceRealise() {
		EOService service = getCurrentItem().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service != null) {
			return controleur.getServiceRealise(getCurrentItem());		
		}
		return 0;
	}
	
	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 *
	 * @return nombre d'heures
	 */
	public double getServiceAttribue() {
		EOService service = getCurrentItem().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service != null) {
			return controleur.getServiceAttribue(getCurrentItem());		
		}
		return 0;		
	}
	
	
	/**
	 * renvoie le service dû de l'enseignant
	 * @return le service dû
	 */
	public double getServiceDu() {
		EOService service = getCurrentItem().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (service != null) {
			return controleur.getServiceDu(getCurrentItem(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		}
		return 0;
}

	/**
	 * renvoie différence entre le service attribué et le service dû
	 *
	 * @return delta
	 */
	public double getDeltaAttribue() {
		double nbHeures = getServiceAttribue() - getServiceDu();
		return nbHeures;
	}

	/**
	 * renvoie différence entre le service realisé et le service dû
	 *
	 * @return delta
	 */
	public double getDeltaRealise() {
		double nbHeures = getServiceRealise() - getServiceDu();
		return nbHeures;
	}

	/**
	 * renvoie le libelle court du corps de l'enseignant
	 * ou le type de contrat de travail
	 *
	 * @return corps de l'intervenant
	 */
	public String getLibelleCorpsOuContrat() {
		String libelle = controleur.getLibelleCourtCorpsOuContrat(getCurrentItem());
		return libelle;
	}

	/**
	 * Permet de récuperer le nom de la composante.
	 *
	 * @return le nom de la composante.
	 */
	public String getCodeComposante() {
		EOStructure composante = getCurrentItem().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
		String nomComposante = "";
		
		if (composante != null) {
			nomComposante = composante.lcStructure();
		}

		return nomComposante;
	}

	/**
	 * Permet de récuperer le nom du département d'affectation.
	 *
	 * @return le nom du département
	 */
	public String getCodeDepartement() {
		EOStructure departement = getCurrentItem().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomDepartement = "";

		if (departement != null) {
			nomDepartement = departement.lcStructure();
		}
		return nomDepartement;
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ouvrirFicheService() {
		if (getSelectedObject() != null) {
			if (checkEnseignantAffecte(controleur, getSelectedObject())) {
				return null;
			}

			CktlPecheFicheServiceEnseignantStatutaire page = (CktlPecheFicheServiceEnseignantStatutaire) 
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			page.setEditedObject(getSelectedObject());
			this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
			return page;

		} else {
			return doNothing();
		}
	}

	
	public WOActionResults rapprocherEnseignantGenerique() {
		if (getSelectedObject() != null) {	
			if (getSelectedObject().rapprocherEnseignant(
					getServiceARapprocher(),
					controleur, 
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession()
					,true)) {
				CktlPecheEnseignantsGeneriques page = (CktlPecheEnseignantsGeneriques) pageWithName(CktlPecheEnseignantsGeneriques.class.getName());
				page.setSelectedItemId(PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId());
				page.setTypeStatutaire(true);
				return page;
			}
		}

		return doNothing();
	}

	
	/**
	 * 
	 * @return true/false
	 */
	public boolean estPopulationPrevisionnelle() {
		return Constante.PREVISIONNEL.equals(controleur.getTypePopulation());
	}
	
	public boolean isBoutonRapprocherVisible() {
		return estPopulationPrevisionnelle() && getAutorisation().hasDroitShowParametrageEnseignantsGeneriques() && isServiceARapprocher();
	}
	
	public boolean isBoutonRapprocherDisabled() {
		return !getAutorisation().hasDroitUtilisationParametrageEnseignantsGeneriques() || !isRapprochable();
	}
	
	/**
	 * 
	 * @return true si bouton diche de service désactivé
	 */
	public boolean isBoutonFicheDeServiceDisabled() {
		return this.getSelectedObject() == null;
	}
	
	public String getNomEnseignantToutesComposantes() {
		return nomEnseignantToutesComposantes;
	}

	public void setNomEnseignantToutesComposantes(String nomEnseignant) {
		this.nomEnseignantToutesComposantes = nomEnseignant;
	}

	/**
	 * Renvoie la liste des enseignants statutaires en fonction du profil connecté
	 */
	public void getEnseignants() {
		EOQualifier qualifierEnseignant;

		NSArray<EtapePeche> listeEtapesFiltres = new NSMutableArray<EtapePeche>();

		if (isMesValidations()) {
			listeEtapesFiltres = determinerListeEtapesFiltre();
		}

		qualifierEnseignant = this.controleur.getListeEnseignants(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), EOPecheParametre.isFicheVoeuxUtilisation(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()), true, listeEtapesFiltres);

		ERXFetchSpecification<EOActuelEnseignant> fetchSpec = EOActuelEnseignant.fetchSpec().qualify(qualifierEnseignant).sort(getTriParDefaut());
		fetchDisplay(fetchSpec);
	}
		

	private NSArray<EOActuelEnseignant> filtrerDoublons(NSArray<EOActuelEnseignant> liste) {

		NSMutableArray<Integer> entrees = new NSMutableArray<Integer>();
		NSMutableArray<EOActuelEnseignant> sortie = new NSMutableArray<EOActuelEnseignant>();
		EOActuelEnseignant actuelEnseignant;

		for (int i = 0; i < liste.count(); i++) {
			actuelEnseignant = (EOActuelEnseignant) liste.get(i);
			if (!entrees.contains(actuelEnseignant.toIndividu().persId())) {
				sortie.add(actuelEnseignant);
				entrees.add(actuelEnseignant.toIndividu().persId());
			}
		}

		return sortie.immutableClone();
	}
	
	
	/**
	 * 
	 * @return true si le lien vers la fiche de service de l'enseignant doit être affiché
	 */
	public boolean afficherLienToFicheEnseignant() {
		return getFonctionRepartiteur() || getFonctionDrh() || getFonctionPresident() || getFonctionDirecteur();
	}
	
	/**
	 * 
	 * @return true si le bouton fiche de service peut être présent
	 */
	public boolean afficherBoutonFicheService() {
		return afficherLienToFicheEnseignant();
	}

	/**
	 * @return Le tri par défaut de la liste
	 */
	private ERXSortOrderings getTriParDefaut() {
		return EOActuelEnseignant.getTriParDefaut();
	}
	
	/**
	 * Activer l'affichage de la recherche inter-composante si répartiteur
	 * @return vrai/faux
	 */
	public boolean getAffichageRepartitionCroisee() {
		return getFonctionRepartiteur() && !isMesValidations() && !isServiceARapprocher();
	}

	/**
	 * 
	 * @param enseignant enseignant en cours
	 * @return état de la fiche de service
	 */
	public String etatFiche(EOActuelEnseignant enseignant) {
		String etatFiche;
		EOService service = null;

		etatFiche = Constante.ETAT_NON_INITIALISE;
		
		service = enseignant.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());	
		
		if (service == null || (service != null && service.toListeRepartService().isEmpty())) {
			etatFiche = Constante.ETAT_NON_INITIALISE;
		} else {
			etatFiche = EtapePeche.getEtape(controleur.getDemande(service).toEtape()).getEtatDemande();
		}
		enseignant.setEtatFiche(etatFiche);

		return etatFiche;
	}
	
	public EOQualifier getQualifier() {
		return qualifier;
	}

	public void setQualifier(EOQualifier qualifier) {
		this.qualifier = qualifier;
	}

	public String getTokenCorps() {
		return tokenCorps;
	}

	public void setTokenCorps(String tokenCorps) {
		this.tokenCorps = tokenCorps;
	}


	public boolean isMesValidations() {
		return mesValidations;
	}


	public void setMesValidations(boolean mesValidations) {
		this.mesValidations = mesValidations;
	}


	public EOService getServiceARapprocher() {
		return serviceARapprocher;
	}


	public void setServiceARapprocher(EOService serviceARapprocher) {
		this.serviceARapprocher = serviceARapprocher;
	}
	
	public boolean isServiceARapprocher() {
		return (getServiceARapprocher() != null);
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}
	
}
