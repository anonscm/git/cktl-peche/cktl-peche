package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOContext;

/**
 * Classe permettant d'afficher la fiche de service de l'enseignant en fonction de
 * son type : statutaire ou vacataire
 *
 * @author Chama LAATIK
 */
public class FicheEnseignantComponent extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 6282687443770815447L;

	private String selectedItemId;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public FicheEnseignantComponent(WOContext context) {
        super(context);
    }

	/**
	 * Renvoie l'enseignant connecté.
	 * @return enseignant
	 */
	public EOActuelEnseignant getEditedObject() {
		return getPecheSession().getApplicationUser().getEnseignant();
	}

	/**
	 * Renvoie si l'enseignant connecté est statutaire.
	 * @return Vrai/Faux
	 */
	public boolean getEnseignantStatutaire() {
		return getPecheSession().getApplicationUser().isEnseignantStatutaire();
	}
	
	public boolean getEnseignant() {
		return getPecheSession().getApplicationUser().isEnseignant();
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}
}