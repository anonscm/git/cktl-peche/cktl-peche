package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

public class CktlPecheTableRepartitionREHEnseignantGeneriqueVac  extends CktlPecheTableRepartitionEnseignantGeneriqueVac {
	
    public CktlPecheTableRepartitionREHEnseignantGeneriqueVac(WOContext context) {
        super(context);
    }

	@Override
	protected String getTableKey() {
		return EOServiceDetail.REH_KEY;
	}
    
	
	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			CktlPecheFormServiceEnseignantGenerique page = (CktlPecheFormServiceEnseignantGenerique) 
					pageWithName(CktlPecheFormServiceEnseignantGenerique.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(getSelectedObject());
			page.setRepartService(getRepartService());
			page.setRepartitionREH(true);
			page.setSelectedItemId(getSelectedItemId());
			return page;
		} else {
			return null;
		}
	}

	/**
	 * Action générique d'ajout
	 * 
	 * @return une instance du formulaire en mode création.
	 */
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantGenerique page = (CktlPecheFormServiceEnseignantGenerique) 
				pageWithName(CktlPecheFormServiceEnseignantGenerique.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setRepartService(getRepartService());
		page.setRepartitionREH(true);
		page.setSelectedItemId(getSelectedItemId());
		return page;
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 * @throws Exception 
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}
		AjaxUpdateContainer.updateContainerWithID("serviceHETDgenerique", context());
		return doNothing();
	}

	@Override
	public boolean hasCommentaire() {		
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return (getCurrentItem().commentaire().trim().length() > 0);
		}
		return false;
	}

	@Override
	public String currentItemCommentaire() {
		if ((getCurrentItem() != null) && (getCurrentItem().commentaire() != null)) {
			return StringCtrl.replace(getCurrentItem().commentaire(), "\n", "<br>");			
		}
		return "";		
	}
	
    
}