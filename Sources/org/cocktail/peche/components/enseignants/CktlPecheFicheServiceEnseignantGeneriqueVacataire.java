package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.controlers.EnseignantsGeneriquesVacCtrl;
import org.cocktail.peche.components.controlers.IEnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOContext;

public class CktlPecheFicheServiceEnseignantGeneriqueVacataire extends CktlPecheFicheEnseignantGeneriqueAbstract {
    
	private String selectedItemId;
	private IEnseignantsVacatairesCtrl controleur;
	
	public CktlPecheFicheServiceEnseignantGeneriqueVacataire(WOContext context) {
        super(context);
        controleur = new EnseignantsGeneriquesVacCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    }
	
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

	@Override
	public void setEditedObject(EOService editedObject) {
		super.setEditedObject(editedObject);
		controleur.suppressionRepartServiceSansRepartition(edc(), getEditedObject(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire());
	}

	
	
}