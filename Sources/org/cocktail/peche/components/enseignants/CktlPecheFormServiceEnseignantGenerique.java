package org.cocktail.peche.components.enseignants;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;



/**
 * Classe permettant d'ajouter ou modifier les affectations de la fiche de
 * service de l'enseignant generique.
 *
 * @author Etienne
 *
 */
public class CktlPecheFormServiceEnseignantGenerique extends CktlPecheFormServiceEnseignantAbstract implements BasicPecheObservateur {
	
	private static final long serialVersionUID = 5852723545779742967L;

	private EOAP currentComposantAP;

	private EnseignantsStatutairesCtrl controleur; // TODO "Normal" que ce soit Statutaire pour "Liste des AP" (A revoir)
	private UECtrl controleurUE;
	
	private boolean repartitionREH;
	private EOStructure currentStructure;

	private EOServiceDetail currentServiceDetail;
	
	private String selectedItemId;
	private EORepartService repartService;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution.
	 */
	public CktlPecheFormServiceEnseignantGenerique(WOContext context) {
		super(context);
		this.controleurUE = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		this.controleur.addObservateur(this);

	}
	
	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		controlerSaisie();
		
		// En création avec un enseignement connu
		if (controleur.getAP() != null) {
			if (modeEdition == ModeEdition.CREATION) {
				
				for (EOServiceDetail detail : getListeServiceDetail()) {
					if ((detail.heuresPrevues() != 0.0) || (detail.heuresRealisees() != 0.0)) {
						controleHeuresMaquette(detail);
						detail.setService(getRepartService().toService());						
						detail.setToRepartService(getRepartService());
						detail.majDonnesAuditCreation(personne);
						
						majCommentaire(detail, editedObject().commentaire());
					} else {
						// Si pas d'heures saisies, on n'enregistre pas cette ligne
						detail.editingContext().deleteObject(detail);
					}
				}
				
				//on supprime l'editedObject car déjà traité
				editedObject().editingContext().deleteObject(editedObject());
			} 
		} else if (controleur.getReh() != null) {
			if (modeEdition == ModeEdition.CREATION) {
				editedObject().setService(getRepartService().toService());				
				editedObject().setToRepartService(getRepartService());
				editedObject().majDonnesAuditCreation(personne);
			}
		} 
		
		majCommentaire(editedObject(), editedObject().commentaire());
		this.editedObject().majDonnesAuditModification(personne);
		
		getRepartService().toService().setDateModification(now);
		getRepartService().toService().setPersonneModification(personne);
	}
	
	/**
	 * Contrôler la saisie utilisateur.
	 * 
	 * Lève une exception si un contrôle est KO.
	 */
	private void controlerSaisie() throws ValidationException {
		//En cas de répartition de service, on vérifie qu'on a selectionné un AP 
		if (modeCreation() && !isRepartitionREH() && controleur.getAP() == null) {
			throw new ValidationException(Constante.REPARTITION_ERREUR_AP_NON_RENSEIGNE);
		}
		
		// on vérifie si un REH a été saisi
		if (modeCreation() && isRepartitionREH() && this.controleur.getReh() == null) {
			throw new ValidationException(Constante.LISTE_REH_NON_RENSEIGNE);
		}
		
		// on vérifie si une structure a été saisie dans le cas de REH
		if (this.editedObject().toStructure() == null &&  isRepartitionREH()) {
			throw new ValidationException(Constante.LISTE_COMP_NON_RENSEIGNE);
		}
	}

	/**
	 * Contrôle que les heuresRepartition <= heuresMaquettes
	 * Lève une exception si un contrôle est KO.
	 */
	private void controleHeuresMaquette(EOServiceDetail serviceDetail) throws ValidationException {
		if (serviceDetail.composantAP() != null) {
			float heuresRepartition = getTotalHeuresPrevues(serviceDetail); 
					
			if (heuresRepartition > getHeuresMaquette(serviceDetail.composantAP())) {
				throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);
			}
		}		
	}
	
	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		// On ne tient pas compte des messages standards WO
	}

	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct suivant
	 * le mode d'édition
	 * @return affichage
	 */
	public String affichageAPEtParent() {
		if ((getModeEdition() == ModeEdition.MODIFICATION)) {
			return getControleurComposant().affichageAPEtParent(this.editedObject().composantAP());
		} else {
			return getControleurComposant().affichageAPEtParent(getCurrentComposantAP());
		}
	}

	/**
	 * action de notifier
	 * 	==> permet de récupérer l'AP selectionné
	 */
	public void notifier() {
		if (isRepartitionREH()) {
			editedObject().setReh(controleur.getReh());			
		} else {
			if (controleur.getAP() != null) {
				editedObject().setComposantAP(controleur.getAP());
				majListeServiceDetails();
			}
		}
	}
	
	/**
	 * Met à jour le commentaire de la répartition du service
	 * @param detail : répartition du service
	 * @param commentaire : commentaire
	 */
	private void majCommentaire(EOServiceDetail detail, String commentaire) {
		if (!StringCtrl.isEmpty(commentaire)) {
			detail.setCommentaire(commentaire);
			detail.setDateCommentaire(new NSTimestamp());
		}
	}
	
	/**
	 * Renvoie le total des heures maquette de de l'AP
	 * ==> Si l'AP est mutualisé, le paramètre false permet de calculer sa charge propre sans prendre 
	 * en compte le nombre de parent 
	 * 
	 * @param ap : AP
	 * @return les heures maquette
	 */
	private float getHeuresMaquette(EOAP ap) {
		return this.controleurUE.getHeuresMaquette(ap, false);
	}
	
	/**
	 * Renvoie le total des heures prévues
	 * @param ap : AP
	 * @return les heures prévues
	 */
	private float getTotalHeuresPrevues(EOServiceDetail serviceDetail) {
		return this.controleurUE.getTotalHeuresPrevues(serviceDetail.composantAP(), getModeEdition(), serviceDetail.heuresPrevues(), false);
	}
	
	/**
	 * 
	 * @return true si la structure est non null
	 */
	public boolean isStructureNonNull() {
		return this.editedObject().toStructure() != null && !"".equals(this.editedObject().toStructure()); 
	}

	/**
	 * Est-ce qu'on affiche les détails de l'AP
	 * @return Vrai/Faux
	 */
	public boolean isCreationAP() {
		return (modeCreation() && !isRepartitionREH());
	}
	
	/**
	 * Est-ce qu'on affiche les détails de l'AP
	 * @return Vrai/Faux
	 */
	public boolean isAfficherDetailAP() {
		return (controleur.getAP() != null);
	}

	/**
	 * En mode modification, renvoit booléen pour savoir si Reh édité ou non
	 * 
	 * @return modeReh
	 */
	public boolean modeReh() {
		return this.editedObject().reh() != null;
	}
	
	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		return controleurUE.getNbHeuresResteARepartir(getCurrentServiceDetail().composantAP());
	}
	
	/**
	 * @return code et libellé du parent direct de l'AP
	 */
	public String affichageParentAP() {
		if (controleur.getAP() != null) {
			return getControleurComposant().getParentAPCodeEtLibelle(controleur.getAP());
		}
		
		return null;
	}
	
	
	public boolean isStatutaire() {
		return getRepartService().toService().toEnseignantGenerique().isEnseignantStatutaire();
	}
	

	@Override
	protected WOActionResults pageGestion() {
		if (isStatutaire()) {
			CktlPecheFicheServiceEnseignantGeneriqueStatutaire page = (CktlPecheFicheServiceEnseignantGeneriqueStatutaire) pageWithName(CktlPecheFicheServiceEnseignantGeneriqueStatutaire.class
					.getName());
				page.setEditedObject(getRepartService().toService());
				page.setSelectedItemId(getSelectedItemId());
				return page;
		} else {
			CktlPecheFicheServiceEnseignantGeneriqueVacataire page = (CktlPecheFicheServiceEnseignantGeneriqueVacataire) pageWithName(CktlPecheFicheServiceEnseignantGeneriqueVacataire.class
				.getName());
			page.setEditedObject(getRepartService().toService());
			page.setSelectedItemId(getSelectedItemId());
			return page;
		}
		
	}
	
	@Override
	protected EOServiceDetail creerNouvelObjet() {
		EOServiceDetail unService = EOServiceDetail.creerEtInitialiser(edc());
		return unService;
	}

	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauServiceContainerId() {
		return getComponentId() + "_nouveauServiceContainerId";
	}

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.SERVICE_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.SERVICE_FORM_TITRE_MODIFIER;
	}

	/* ========== Getters & Setters ========== */
	
	public List<IStructure> getListeStructures() {
		if (isStatutaire()) {			
			return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementStatutaires();			
		} else {			
			return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire();			
		}
	}

	public EOAP getCurrentComposantAP() {
		return currentComposantAP;
	}

	public void setCurrentComposantAP(EOAP currentComposantAP) {
		this.currentComposantAP = currentComposantAP;
	}

	public EnseignantsStatutairesCtrl getControleur() {
		return controleur;
	}

	public void setControleur(EnseignantsStatutairesCtrl controleur) {
		this.controleur = controleur;
	}

	public boolean isRepartitionREH() {
		return repartitionREH;
	}

	public void setRepartitionREH(boolean repartitionREH) {
		this.repartitionREH = repartitionREH;
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	public EOServiceDetail getCurrentServiceDetail() {
		return currentServiceDetail;
	}

	public void setCurrentServiceDetail(EOServiceDetail currentServiceDetail) {
		this.currentServiceDetail = currentServiceDetail;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

	@Override
	public EORepartService getRepartService() {
		return repartService;
	}

	public void setRepartService(EORepartService repartService) {
		this.repartService = repartService;
	}

	

}