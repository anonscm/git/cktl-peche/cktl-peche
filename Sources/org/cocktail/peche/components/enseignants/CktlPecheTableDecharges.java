package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlgrh.common.metier.EODecharge;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;


/**
 * Cette classe permet d'afficher la liste des décharges d'un enseignant.
 * 
 * @author Chama Laatik
 * 
 */
public class CktlPecheTableDecharges extends CktlPecheTableComponent<EODecharge> {
	private static final long serialVersionUID = 1168349605856191785L;
	private EOActuelEnseignant enseignant;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableDecharges(WOContext context) {
        super(context);
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EODecharge.ENTITY_NAME);
		ERXDisplayGroup<EODecharge> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
    }
	
	@Override
	public void awake() {
		ERXDisplayGroup<EODecharge> dg = this.getDisplayGroup();
		enseignant = (EOActuelEnseignant) valueForBinding("enseignant");
	    EOIndividu ind = enseignant.toIndividu();
	    
		NSArray<EODecharge> currentItem = EODecharge.dechargeAnneeUniversitaire(edc(), ind, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
	    dg.setObjectArray(currentItem);
	}
	
	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}
}