package org.cocktail.peche.components.enseignants;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.CktlPecheCRUDTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.entity.EOEnseignantGenerique;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;

/**
 * Tableau de la liste des enseignants génériques.
 */
public class CktlPecheTableEnseignantsGeneriques extends CktlPecheCRUDTableComponent<EOService, CktlPecheFormEnseignantsGeneriques> {
	
	private static final long serialVersionUID = 8131912494374204457L;
	private static final Logger log = Logger.getLogger(CktlPecheTableEnseignantsGeneriques.class);
	
	private String tokenNom;
	private String tokenPrenom;
	
	private EnseignantsGeneriquesCtrl controleur;
	private Boolean isTypeStatutaire;
	
	/**
	 * Constructeur
	 * @param context de l'execution
	 */
	public CktlPecheTableEnseignantsGeneriques(WOContext context) {
        super(context);
        
        controleur = new EnseignantsGeneriquesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    }

	/**
	 * @return qualifier pour les enseignants génériques
	 */
	private EOQualifier getQualifierPourEnseignantsGeneriques() {
		EOQualifier qualifier;
		
		EOQualifier qualifierEnseignant = 
				ERXQ.and(
						EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TEMOIN_STATUTAIRE_KEY).eq(isEnseignantsGeneriquesStatutaire()),
						EOService.ANNEE.eq(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())
						);
		
		if (getIsTypeStatutaire()) {
			NSArray<EOQualifier> prequalifierPourStructures = new NSMutableArray<EOQualifier>();
			List<IStructure> listeCibleEnseignantsStatutaires = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
			
			if (!CollectionUtils.isEmpty(listeCibleEnseignantsStatutaires)) {
				for (IStructure structure : listeCibleEnseignantsStatutaires) {
					prequalifierPourStructures.add(
							ERXQ.equals(
									EOEnseignantGenerique.TO_STRUCTURE.dot(EOStructure.C_STRUCTURE_KEY).key(), structure.cStructure())
									);
				}
			}
			// Passage par une sous-requête pour récupérer à la fois les enseignants génériques qui ont une structure null et d'autres ayant une structure dans le périmétre de l'utlisateur connecté
			// (la jointure entre enseignantGenerique et Structure ne devant se faire que dans la sous-requête)
			EOQualifier qualStructures = new ERXQualifierInSubquery(ERXQ.or(prequalifierPourStructures), EOEnseignantGenerique.ENTITY_NAME, 
					EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.ID_KEY).key(), EOEnseignantGenerique.ID_KEY);
			
			EOQualifier qualifierStructureNull = ERXQ.isNull(EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TO_STRUCTURE).key());
			
			qualifier = ERXQ.and(qualifierEnseignant, ERXQ.or(qualStructures, qualifierStructureNull));
		} else {
			qualifier = qualifierEnseignant;
		}
		
		return qualifier;
	}
	
	@Override
	public void awake() {
		super.awake();
		isTypeStatutaire = (Boolean) valueForBinding("isTypeStatutaire");
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		fetchDisplay(getQualifierPourEnseignantsGeneriques(), EOService.ENTITY_NAME);
		super.appendToResponse(response, context);
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ouvrirFicheService() {
		if (getSelectedObject() != null) {
			return getFicheEnsGeneriqueParType(getSelectedObject(), getIsTypeStatutaire());
		}
		
		return null;
	}
	
    /**
     * Retourne la fiche de service de l'enseignant.
     * @return la page.
     */
    public WOActionResults getLinkToFicheEnseignant() {
    	if (getCurrentItem() != null) {
    		return getFicheEnsGeneriqueParType(getCurrentItem(), getIsTypeStatutaire());
    	}
    	
    	return null;
    }

	@Override
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				for (EOServiceDetail serviceDetail : getSelectedObject().listeServiceDetails()) {
					serviceDetail.delete();
				}

				for (EORepartService repartService : getSelectedObject().toListeRepartService()) {
					repartService.delete();
				}

				//On supprime l'enseignant générique
				EOEnseignantGenerique enseignantSelectionne = getSelectedObject().toEnseignantGenerique();
				getSelectedObject().setToEnseignantGenerique(null);
				getSelectedObject().delete();			
				//getSelectedObject().setToEnseignantGeneriqueRelationship(null);
				edc().saveChanges();
				
				enseignantSelectionne.delete();
				edc().saveChanges();
				
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}

		return doNothing();

	}

	@Override
	protected String getConfirmSupressMessageKey() {
		String message = Messages.ENSEIGNANT_GENERIQUE_SUPPRIMER;
		
		//Message de suppression différent si des répartitions de service existent
		if (controleRepartitionExistante()) {
			message = Messages.ENSEIGNANT_GENERIQUE_REPARTITION_SUPPRIMER;
		}
		
		return message;
	}
	
	/**
	 * Action d'ajout.
	 * 
	 * @return une instance du formulaire en mode création.
	 */
	@Override
	public WOActionResults ajouterAction() {
		CktlPecheFormEnseignantsGeneriques form = getFormPage(ModeEdition.CREATION);
		form.setIsTypeStatutaire(getIsTypeStatutaire());
		return form;
	}

	/**
	 * Action de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	@Override
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			CktlPecheFormEnseignantsGeneriques form = getFormPage(ModeEdition.MODIFICATION);
			form.setEditedObject(getSelectedObject());
			form.setIsTypeStatutaire(getIsTypeStatutaire());
			return form;
		} 
		
		return null;
	}

	/**
	 * Vérifie s'il existe des répartitions pour l'enseignant générique selectionné
	 * @return vrai/faux
	 */
	private boolean controleRepartitionExistante() {
		if ((getSelectedObject() != null) && (getSelectedObject().listeServiceDetails().size() > 0)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filtrer.
	 *
	 * @return null.
	 */
	public WOActionResults filtrer() {
		EOQualifier qualifierNom = null;
		EOQualifier qualifierPrenom = null;
							
		if (!StringCtrl.isEmpty(tokenNom)) {
			qualifierNom = EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.NOM_KEY).contains(tokenNom);
		}		
	
		if (!StringCtrl.isEmpty(tokenPrenom)) {
			qualifierPrenom = EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.PRENOM_KEY).contains(tokenPrenom);
		}
				
		filtrerDisplayGroup(ERXQ.and(qualifierNom, qualifierPrenom));
		return doNothing();
	}
	
	/**
	 * Affichage du bouton Supprimer si enseignant selectionné
	 * @return vrai/faux
	 */
	public boolean isEnseignantGeneriqueSelected() {
		return (getSelectedObject() == null);
	}
	
	/**
	 * Est-ce que l'enseignant générique a une correspondance avec un invidu de Grhum?
	 * @return vrai/faux
	 */
	public boolean vientDeGrhum() {
		return ((getCurrentItem().toEnseignantGenerique() != null) && (getCurrentItem().toEnseignantGenerique().persID() != null));
	}
	
	public double getServiceAttribue() {
		return controleur.getServiceAttribue(getCurrentItem());
	}

	public double getDelta() {
		return controleur.getHeuresService(getCurrentItem()) - getServiceAttribue();
	}
	
	/**
	 * @return le libellé du département
	 */
	public String getLibelleDepartement() {
		String libelleStructure = "";
		if (controleur.getStructureDepartement(getCurrentItem().toEnseignantGenerique()) != null) {
			libelleStructure = controleur.getStructureDepartement(getCurrentItem().toEnseignantGenerique()).lcStructure();
		}
		return libelleStructure;
	}
	
	/**
	 * @return le libellé de la composante
	 */
	public String getLibelleComposante() {
		String libelleStructure = "";
		if (controleur.getStructureComposante(getCurrentItem().toEnseignantGenerique()) != null) {
			libelleStructure = controleur.getStructureComposante(getCurrentItem().toEnseignantGenerique()).lcStructure();
		}
		return libelleStructure;
	}

	@Override
	public String getSelectedItemId() {
		if (getIsTypeStatutaire()) {
			return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId());
		} else {
			return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId());
		}
	}
	
	/**
	 * Est-ce que l'enseignant générique est statutaire?
	 * @return O/N
	 */
	public String isEnseignantsGeneriquesStatutaire() {
		if (getIsTypeStatutaire()) {
			return Constante.OUI;
		}
		return Constante.NON;
	}
	
    /**
     * @param unService : service
     * @param isTypeStatutaire : statutaire?
     * @return la page de la fiche de service en fonction du type de l'enseignant générique
     */
    private WOActionResults getFicheEnsGeneriqueParType(EOService unService, boolean isTypeStatutaire) {
    	if (isTypeStatutaire) {
    		CktlPecheFicheServiceEnseignantGeneriqueStatutaire page = (CktlPecheFicheServiceEnseignantGeneriqueStatutaire) pageWithName(
    				CktlPecheFicheServiceEnseignantGeneriqueStatutaire.class.getName());
    		page.setEditedObject(unService);
    		page.setSelectedItemId(getSelectedItemId());
    		this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
    		return page; 
    	} else {
    		CktlPecheFicheServiceEnseignantGeneriqueVacataire page = (CktlPecheFicheServiceEnseignantGeneriqueVacataire) pageWithName(
    				CktlPecheFicheServiceEnseignantGeneriqueVacataire.class.getName());
    		page.setEditedObject(unService);
    		page.setSelectedItemId(getSelectedItemId());
    		this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
    		return page; 
    	}
    }

	public String getTokenNom() {
		return tokenNom;
	}

	public void setTokenNom(String tokenNom) {
		this.tokenNom = tokenNom;
	}

	public String getTokenPrenom() {
		return tokenPrenom;
	}

	public void setTokenPrenom(String tokenPrenom) {
		this.tokenPrenom = tokenPrenom;
	}
	
	@Override
	protected Class<?> getFormClass() {
		return CktlPecheFormEnseignantsGeneriques.class;
	}

	public Boolean getIsTypeStatutaire() {
		return isTypeStatutaire;
	}

	public void setIsTypeStatutaire(Boolean isTypeStatutaire) {
		this.isTypeStatutaire = isTypeStatutaire;
	}

	/**
	 * Vers la page des enseignants génériques pour rapprochement.
	 * @return Vers la page des enseignants génériques
	 */
	public WOActionResults rapprocherEnseignantGenerique() {
		if (getSelectedObject() != null && getIsTypeStatutaire()) {		
			CktlPecheEnseignantsStatutaires page = (CktlPecheEnseignantsStatutaires) pageWithName(CktlPecheEnseignantsStatutaires.class.getName());
			page.setTypePopulation(Constante.PREVISIONNEL);
			page.setMesValidations(false);
			page.setServiceARapprocher(getSelectedObject());
			return page;
		} 
		
		if (getSelectedObject() != null && !getIsTypeStatutaire()) {		
			CktlPecheEnseignantsVacataires page = (CktlPecheEnseignantsVacataires) pageWithName(CktlPecheEnseignantsVacataires.class.getName());
			page.setTypePopulation(Constante.PREVISIONNEL);	
			page.setServiceARapprocher(getSelectedObject());
			return page;
		}
		
		return doNothing();
		
	}
	

}