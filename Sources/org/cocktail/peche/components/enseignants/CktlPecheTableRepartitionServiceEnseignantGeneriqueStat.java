package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;


/**
 * Cette classe représente la table contenant la répartition des heures de
 * service au titre du référentiel équivalence horaire pour les enseignants
 * génériques.
 * 
 * @author Etienne Ragonneau
 * 
 */
public class CktlPecheTableRepartitionServiceEnseignantGeneriqueStat extends
		CktlPecheTableRepartitionEnseignantGeneriqueStat {

	private static final long serialVersionUID = -8201953897294121851L;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionServiceEnseignantGeneriqueStat(WOContext context) {
		super(context);
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			CktlPecheFormServiceEnseignantGenerique page = (CktlPecheFormServiceEnseignantGenerique) pageWithName(CktlPecheFormServiceEnseignantGenerique.class
					.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(getSelectedObject());
			page.setRepartService(getRepartService());
			page.setSelectedItemId(getSelectedItemId());
			return page;
		}
		return doNothing();
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantGenerique page = (CktlPecheFormServiceEnseignantGenerique) pageWithName(CktlPecheFormServiceEnseignantGenerique.class
				.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setRepartService(getRepartService());
		page.setSelectedItemId(getSelectedItemId());
		page.setRepartitionREH(false);
		return page;
	}

	/**
	 * Action générique de modification.
	 * 
	 * @return une instance du formulaire en mode modification.
	 * @throws Exception 
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}
		AjaxUpdateContainer.updateContainerWithID("serviceHETDgenerique", context());
		return doNothing();
	}
	
	@Override
	public String onClickBefore() {
		String msgConfirm = message(Messages.SERVICE_ALERTE_CONFIRM_SUPPRESS);
		return "confirm('" + msgConfirm + "')";
	}

	@Override
	protected String getTableKey() {
		return EOServiceDetail.COMPOSANT_AP_KEY;
	}
	
}