package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;

/**
 * 
 * @author juliencallewaert
 * 
 */
public class CktlPecheTableRepartitionREHEnseignantVacataire extends
		CktlPecheTableRepartitionEnseignantVacataire {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2829610319643293265L;

	/**
	 * Constructeur de la classe
	 * 
	 * @param context
	 *            WOContext
	 */
	public CktlPecheTableRepartitionREHEnseignantVacataire(WOContext context) {
		super(context);
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(),
				EOServiceDetail.ENTITY_NAME);
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.fetch();

	}

	@Override
	protected String getTableKey() {
		return EOServiceDetail.REH_KEY;
	}

	@Override
	public WOActionResults modifierAction() {
		CktlPecheFormServiceEnseignantVacataire results = (CktlPecheFormServiceEnseignantVacataire) super
				.modifierAction();
		if (results != null) {
			results.setModeReh(true);
		}
		return results;
	}

	@Override
	public WOActionResults ajouterAction() {
		CktlPecheFormServiceEnseignantVacataire results = (CktlPecheFormServiceEnseignantVacataire) super
				.ajouterAction();
		if (results != null) {
			results.setModeReh(true);
		}
		return results;
	}

	public String getCommentaireRehId() {
		return getContainerId() + "_reh_commentaire";
	}

	/**
	 * met à jour la liste des paiements
	 * 
	 * @return null
	 */
	public WOActionResults updateListePaiement() {
		if (isCircuitDefinitif()) {
			AjaxUpdateContainer.updateContainerWithID(
					"listePaiementRepartitionREHId", context());
		}
		return null;
	}

	public String getRehLibelleLongCourt() {
		return getSelectedObject().reh().getLibelleLongCourt();
	}

}