package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.DemandeCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl;
import org.cocktail.peche.entity.EORepartService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * 
 *
 */
public class CktlPecheOngletBoutonsActionsVacataire extends CktlPecheTableRepartitionEnseignantVacataire {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2784313551278247756L;

	private EnseignantsVacatairesCtrl controleur;
	private GenerateurPDFCtrl generateurPDFCtrl;

	private String motifRefus;

	/**
	 * @param context wocontext
	 */
	public CktlPecheOngletBoutonsActionsVacataire(WOContext context) {
		super(context);
	}

	/**
	 * Est-ce que le bouton "Valider la fiche" doit être affiché ?
	 * 
	 * @return <code>true</code> si affiché
	 */
	public boolean hasDroitShowBoutonValider() {
		return estPasOngletTous() && aDroitValidationFicheSelonComposante() && getAutorisation().hasDroitShowChemin(getDemande(), TypeCheminPeche.VALIDER);
	}

	/**
	 * Est-ce que le bouton "Refuser la fiche" doit être affiché ?
	 * 
	 * @return <code>true</code> si affiché
	 */
	public boolean hasDroitShowBoutonRefuser() {
		return estPasOngletTous() && aDroitValidationFicheSelonComposante() && getAutorisation().hasDroitShowChemin(getDemande(), TypeCheminPeche.REFUSER);
	}

	/**
	 * Est-ce que le bouton "Passer en définitive" doit être affiché ?
	 * 
	 * @return <code>true</code> si affiché
	 */
	public boolean hasDroitShowBoutonPasserDefinitif() {
		return estPasOngletTous() && aDroitValidationFicheSelonComposante() && getFonctionRepartiteur() && controleur.estFicheServiceNonDefinitif(getService());
	}

	/**
	 * 
	 * @return <code>true</code> si l'utilisateur connecté a le droit de voir le bouton
	 */
	private boolean aDroitValidationFicheSelonComposante() {

		if (getFonctionToutesValidations()) {
			return true;
		}
		// les répartiteurs et les directeurs de composante ne peuvent valider ou refuser une fiche que sur leur composante
		// président et DRH le peuvent sur toutes les composantes
		// l'enseignant ne le peut que sur sa fiche
		if (getFonctionEnseignant() && controleur.getDemande(getRepartService()).toEtape().codeEtape().equals(EtapePeche.VALID_ENSEIGNANT.getCodeEtape())) {
			if (getPecheSession().getApplicationUser().isFicheEnseignantConnecte(getService())) {
				return true;
			}
			return false;
		} else if ((getFonctionPresident() || getFonctionDrh()) && !getFonctionRepartiteur()) {
			return true;
		} else if (getFonctionDirecteur() || getFonctionRepartiteur()) {
			return getServiceOngletEnseignantVacataire().estOngletSelectionneDansListeStructure(this.getOngletComposanteSelectionne(), 
					getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire());
		} else {
			return false;
		}
	}

	/**
	 * Est-ce que le bouton "Valider la fiche" doit être inactif (disable) ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean isBoutonValiderInactif() {
		return !getAutorisation().hasDroitUtilisationChemin(getDemande(), TypeCheminPeche.VALIDER) || !aDroitValidationFicheSelonComposante();
	}

	/**
	 * Est-ce que le bouton "Refuser la fiche" doit être inactif (disable) ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean isBoutonRefuserInactif() {
		return !getAutorisation().hasDroitUtilisationChemin(getDemande(), TypeCheminPeche.REFUSER) || !aDroitValidationFicheSelonComposante();
	}


	/**
	 * Est-ce que le bouton "Passer en définitive" doit être inactif (disable) ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean isBoutonPasserEnDefinitifInactif() {
		return !controleur.estToutesDemandesPrevisionnellesEtatFinal(getService());
	}

	/**
	 * Valide la fiche et la fait avancer à l'étape suivante
	 */
	public void validerFiche() {
		if (getDemande().toEtape().codeEtape().equals(EtapePeche.VALID_REPARTITEUR.getCodeEtape()) && controleur.isCircuitDefinitif(getDemande())) {
			if (!controleur.estToutesAutresDemandeAvecRepartitionDefinitives(getService(), getDemande())) {
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_DEMANDE_AUTRE_PREVISIONNEL);
			} else {
				if (controleHeuresContrat(getService().enseignant().toIndividu())) {
					getRepartService().setTemIncident(null);
					controleur.validerFiche(getRepartService(), personneConnecte().persId());
					setMotifRefus(null);
				}
			}
		} else {
			getRepartService().setTemIncident(null);
			controleur.validerFiche(getRepartService(), personneConnecte().persId());
			setMotifRefus(null);
		}
	}


	public WOActionResults reinitialiserCircuit() {
		EODemande demande = getRepartService().toDemande();

		if ((demande != null) && (!demande.estSurEtapeInitiale())) {
			demande.reinitialiser();
			getRepartService().setTemIncident(null);
			/*StringBuffer suivi = new StringBuffer();
				suivi.append(DateCtrl.dateToString(DateCtrl.now()));
				suivi.append(": Circuit réinitialisé");
				getService().addSuivi(suivi.toString());*/
			getPecheSession().addSimpleSuccessMessage(message("ficheService.titre.reinit.circuit"), message("ficheService.details.reinit.circuit"));
			getRepartService().editingContext().saveChanges();
		}
		setMotifRefus(null);
		return null;
	}

	/**
	 * Passe toutes les fiches dans le circuit définitif
	 */
	public void passerEnDefinitif() {		
		controleur.passerEnFicheDefinitive(edc(), getService(), personneConnecte().persId(), getPecheSession().getPecheAutorisationCache().getStructuresPourVacataires(controleur, getService()));
	}

	/**
	 * Refuse la fiche et la renvoie à l'étape précédente
	 */
	public void refuserFiche() {
		if (StringCtrl.isEmpty(motifRefus)) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "Vous devez renseigner un motif de refus");
			return;
		}
		
		getRepartService().setTemIncident(null);
		
		DemandeCtrl controleur = new DemandeCtrl(edc());
		String etatEtape = controleur.etatEtape(getRepartService().toDemande());
		
		StringBuffer commentaire = new StringBuffer();
		commentaire.append(DateCtrl.dateToString(DateCtrl.now()));
		commentaire.append(": Motif du refus (").append(etatEtape).append("): ");
		commentaire.append(motifRefus).append("\n");
		if (getRepartService().commentaire() != null) {
			commentaire.append(getRepartService().commentaire());
		}
		getRepartService().setCommentaire(StringCtrl.cut(commentaire.toString(), EORepartService.CommentairesMax));
		
		controleur.refuserFiche(getRepartService(), personneConnecte().persId());
		
		setMotifRefus(null);
	}

	public boolean hasDroitShowBoutonValiderFicheOuRefuserFiche() {
		return (hasDroitShowBoutonValider() || hasDroitShowBoutonRefuser());
	}

	public boolean isIncidentEnCours() {
		EORepartService repartService = getRepartService();
		if (repartService != null) {			
			return getRepartService().isIncidentPresent();
		}
		return false;
	}

	public boolean isBoutonReinitFicheInactif() {		
		return (isBoutonValiderInactif() && isBoutonRefuserInactif());
	}


	/**
	 * Vérifie que les heures du service définitif ne dépassent pas les heures du contrat
	 * 
	 * @param individu : intervenant vacataire
	 * @return true si reste realisé >= 0
	 */
	private boolean controleHeuresContrat(EOIndividu individu) {
		if (getResteRealise() < 0) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_DEF_SUPERIEUR_HEURES_CONTRAT);
			return false;
		} else {
			return true;
		}
	}

	/**
	 * renvoie la différence entre le nombre d'heures du contrat et le nombre d'heures
	 * réalisées par l'enseignant
	 * @return nombre d'heures
	 */
	public double getResteRealise() {
		return getHeuresContrat() - getServiceRealise();
	}

	/**
	 * renvoie les heures du contrat de travail d'un intervenant
	 * @return heures du contrat
	 */
	public double getHeuresContrat() {
		return this.controleur.getTotalNbHeuresVacation(getService());
	}

	/**
	 * @return le total des heures réalisées répartis que l'intervenant doit faire en HETD
	 */
	public double getServiceRealise() {
		return controleur.getServiceRealise(getService());
	}

	@Override
	protected String getTableKey() {
		return null;
	}


	@Override
	public void awake() {
		super.awake();
		this.controleur = (EnseignantsVacatairesCtrl) valueForBinding("controleur");
	}


	public WOActionResults imprimerFiche() {
		//génération du xml

		String fluxXml = controleur.getFluxFicheService(getService().enseignant(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession(), getComponentCtrl(), "Fiche individuelle de service");

		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

		EODemande demande = controleur.getDemande(getService());

		generateurPDFCtrl.setReportFilename(controleur.getFilenameFicheService(getService().enseignant(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), false, controleur.isCircuitPrevisionnel(demande)) + ".pdf");
		generateurPDFCtrl.genererFicheService("Reports/ficheServiceVacataire/fiche_service_vacat.jasper", false);

		return null;
	}

	public GenerateurPDFCtrl getGenerateurPDFCtrl() {
		return generateurPDFCtrl;
	}

	/**
	 * @return the motifRefus
	 */
	public String motifRefus() {
		return motifRefus;
	}

	/**
	 * @param motifRefus the motifRefus to set
	 */
	public void setMotifRefus(String motifRefus) {
		this.motifRefus = motifRefus;
	}



}