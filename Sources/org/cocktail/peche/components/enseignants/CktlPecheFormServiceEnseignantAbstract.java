package org.cocktail.peche.components.enseignants;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.IEnseignantsCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public abstract class CktlPecheFormServiceEnseignantAbstract extends PecheCreationForm<EOServiceDetail>{

	private NSArray<EOServiceDetail> listeServiceDetailDejaCree;	
	private NSArray<EOServiceDetail> listeServiceDetailCree;
	
	private ComposantCtrl controleurComposant;
	private NSArray<EOServiceDetail> listeServiceDetail;
	
	public CktlPecheFormServiceEnseignantAbstract(WOContext context) {
		super(context);
		this.controleurComposant = new ComposantCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	public abstract EORepartService getRepartService();

	protected abstract IEnseignantsCtrl getControleur();
	
	/**
	 * met à jour la liste des APs à afficher
	 */
	public void majListeServiceDetails() {
		NSArray<EOServiceDetail> listeServiceDetails = new NSMutableArray<EOServiceDetail>();
		NSArray<EOServiceDetail> listeDetail = new NSMutableArray<EOServiceDetail>();
		NSArray<EOServiceDetail> listeDetailDejaCree = new NSMutableArray<EOServiceDetail>();
		NSArray<EOServiceDetail> listeDetailCree = new NSMutableArray<EOServiceDetail>();
		
		// on supprimer les éventuels serviceDetail crées lors la sélection 
		// d'une autre ligne
		if (getListeServiceDetailCree() != null) {
			for (EOServiceDetail serviceDetail : getListeServiceDetailCree()) {			
				edc().deleteObject(serviceDetail);
			}
		}
		
		if (getRepartService() != null) {
			listeServiceDetails = EOServiceDetail.fetchEOServiceDetails(edc(), getQualifierAPPourServiceDetail(
					getRepartService()), null);
		}

		if (getControleur().getAP() != null) {
			ITypeComposant typeComposantAP = EOTypeComposant.typeAP(edc());
			List<IComposant> listeAPs = controleurComposant.getParentAP(getControleur().getAP()).childs(typeComposantAP);

			for (IComposant ap : listeAPs) {
				EOServiceDetail serviceDetailTrouve = null;

				//on ajoute les serviceDetail si on a le meme type d'AP
				for (EOServiceDetail serviceDetail : listeServiceDetails) {
					if ((serviceDetail.composantAP() != null) && (serviceDetail.composantAP().id().equals(ap.id()))) {
						//permet de récupérer l'existant
						serviceDetailTrouve = serviceDetail;
						listeDetailDejaCree.add(serviceDetailTrouve);
						break;
					}
				}

				//si serviceDetail non trouvé, on le crée
				if (serviceDetailTrouve == null) {
					serviceDetailTrouve = EOServiceDetail.creerEtInitialiser(edc());
					serviceDetailTrouve.setComposantAPRelationship((EOAP) ap);			
					listeDetailCree.add(serviceDetailTrouve);
				}
				listeDetail.add(serviceDetailTrouve);
			}
		}
		setListeServiceDetail(listeDetail);
		setListeServiceDetailDejaCree(listeDetailDejaCree);
		setListeServiceDetailCree(listeDetailCree);
	}
	
	private EOQualifier getQualifierAPPourServiceDetail(EORepartService repartService) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOServiceDetail.TO_REPART_SERVICE_KEY, repartService),
						ERXQ.isNotNull(EOServiceDetail.COMPOSANT_AP_KEY)
						);
		
		return qualifier;
	}
	
	@Override
	protected void reinitialiserChamps() {
		getControleur().setSelectedAP(null);
		setListeServiceDetailDejaCree(null);
		setListeServiceDetailCree(null);
		setListeServiceDetail(null);
		setEditedObject(null);
	}
	
	public NSArray<EOServiceDetail> getListeServiceDetailDejaCree() {
		return listeServiceDetailDejaCree;
	}

	public void setListeServiceDetailDejaCree(
			NSArray<EOServiceDetail> listeServiceDetailDejaCree) {
		this.listeServiceDetailDejaCree = listeServiceDetailDejaCree;
	}

	public NSArray<EOServiceDetail> getListeServiceDetailCree() {
		return listeServiceDetailCree;
	}

	public void setListeServiceDetailCree(
			NSArray<EOServiceDetail> listeServiceDetailCree) {
		this.listeServiceDetailCree = listeServiceDetailCree;
	}
	
	public NSArray<EOServiceDetail> getListeServiceDetail() {
		return listeServiceDetail;
	}

	public void setListeServiceDetail(NSArray<EOServiceDetail> listeServiceDetail) {
		this.listeServiceDetail = listeServiceDetail;
	}


	public ComposantCtrl getControleurComposant() {
		return controleurComposant;
	}
	
}
