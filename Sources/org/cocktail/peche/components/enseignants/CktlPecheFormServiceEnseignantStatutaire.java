package org.cocktail.peche.components.enseignants;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignements.ListeAP;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant d'ajouter ou modifier les affectations de la fiche de
 * service de l'enseignant statutaire.
 *
 * @author Yannick MAURAY
 * @author Chama LAATIK
 *
 */
public class CktlPecheFormServiceEnseignantStatutaire extends CktlPecheFormServiceEnseignantAbstract implements BasicPecheObservateur {

	private static final long serialVersionUID = 5852723545779742967L;

	private EOAP currentComposantAP;
	private boolean repartitionREH;

	private EOActuelEnseignant enseignant;
	
	private EnseignantsStatutairesCtrl controleur;
	private UECtrl controleurUE;
	private ComposantCtrl controleurComposant;

	private String currentEtat;

	private EOStructure currentStructure;

	private EOServiceDetail currentServiceDetail;
	private NSArray<EOServiceDetail> listeServiceDetail;

	private boolean modeValidationUeEc;
	private String typeFicheAValider;
	
	private EORepartService repartService;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'exécution.
	 */
	public CktlPecheFormServiceEnseignantStatutaire(WOContext context) {
		super(context);
		this.controleurUE = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		this.controleurComposant = new ComposantCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		this.controleur.addObservateur(this);
	}

	@Override
	protected WOActionResults pageGestion() {
		if (isModeValidationUeEc()) {
			ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
			page.setTypeFicheAValider(getTypeFicheAValider());
			page.setModeValidationParUeEc(true);
			page.setParentAP((EOComposant) editedObject().composantAP().parents().get(0));
			return page;
		} else {
			CktlPecheFicheServiceEnseignantStatutaire page = (CktlPecheFicheServiceEnseignantStatutaire)
					pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
			page.setEditedObject(getEnseignant());
			return page;
		}
	}

	protected void beforeSaveChanges(ModeEdition modeEdition) {

		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		controlerSaisie();
		boolean depassementHeuresMaquette = false;

		if (controleur.getAP() != null) {
			if (modeEdition == ModeEdition.CREATION) {
				for (EOServiceDetail detail : getListeServiceDetail()) {
					if ((detail.heuresPrevues() != 0.0) || (detail.heuresRealisees() != 0.0)) {
						try {
							controleHeuresMaquette(detail);
						} catch (ValidationException e) {
							depassementHeuresMaquette = true;
						}
					}
				}
			}
		} else if (isRepartitionREH()) {
			// REH
		}
		else {
			//Modification
			if (editedObject()!=null) {
				controleHeuresMaquette(editedObject());
			}
		}

		if (depassementHeuresMaquette) {		
			throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);			
		}

		// En création avec un enseignement connu
		if (controleur.getAP() != null) {
			if (modeEdition == ModeEdition.CREATION) {

				for (EOServiceDetail detail : getListeServiceDetail()) {
					if ((detail.heuresPrevues() != 0.0) || (detail.heuresRealisees() != 0.0)) {
						detail.setService(getRepartService().toService());						
						detail.setToRepartService(getRepartService());
						detail.majDonnesAuditCreation(personne);
						majDetailSiRepartitionCroisee(getEnseignant(), detail);
						majCommentaire(detail, editedObject().commentaire());
					} else {						
						// Si pas d'heures saisies, on n'enregistre pas cette ligne
						if (detail != null && detail.editingContext() != null
								&& detail.heuresPrevues() == 0.0 && detail.heuresRealisees() == 0.0) {
							detail.editingContext() .deleteObject(detail);
						}						
					}
				}
				//on supprime l'editedObject car déjà traité
				if (editedObject() != null && editedObject().editingContext() != null) {
					editedObject().editingContext().deleteObject(editedObject());
				}
			} 
		} else if (isRepartitionREH()) {
			if (modeEdition == ModeEdition.CREATION) {
				editedObject().setService(getRepartService().toService());				
				editedObject().setToRepartService(getRepartService());
				editedObject().majDonnesAuditCreation(personne);
			}
		} 
		if (editedObject() != null) {
			majCommentaire(editedObject(), editedObject().commentaire());
			this.editedObject().majDonnesAuditModification(personne);
		}
		getRepartService().toService().majDonnesServiceReparti(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		controleur.recalculerIndicateurValidationAuto(getRepartService());		
	}

	/**
	 * Contrôler la saisie utilisateur.
	 * 
	 * Lève une exception si un contrôle est KO.
	 */
	private void controlerSaisie() throws ValidationException {
		// on vérifie si une structure a été saisie dans le cas de REH
		if (this.editedObject().toStructure() == null &&  isRepartitionREH()) {
			throw new ValidationException(Constante.LISTE_COMP_NON_RENSEIGNE);
		}

		// on vérifie si un REH a été saisi
		if (modeCreation() && this.controleur.getReh() == null &&  isRepartitionREH()) {
			throw new ValidationException(Constante.LISTE_REH_NON_RENSEIGNE);
		}

		//En cas de répartition de service, on vérifie qu'on a selectionné un AP 
		if (modeCreation() && !isRepartitionREH() && controleur.getAP() == null) {
			throw new ValidationException(Constante.REPARTITION_ERREUR_AP_NON_RENSEIGNE);
		}

	}

	/**
	 * Contrôle des heures saisies
	 * Lève une exception si un contrôle est KO.
	 */
	private void controleHeuresMaquette(EOServiceDetail serviceDetail) {

		if (getListeServiceDetailDejaCree() != null && getListeServiceDetailDejaCree().contains(serviceDetail)) {
			return;
		}

		if (serviceDetail.composantAP() != null) {
			if  (getModeEdition() == ModeEdition.CREATION) {

				if ((!isCircuitDefinitif()) && (serviceDetail.heuresPrevues() > getHeuresResteARepartir(serviceDetail))) {				
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

				if ((isCircuitDefinitif()) && (serviceDetail.heuresRealisees() > getHeuresResteARealiser(serviceDetail))) {
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

			} else {
				float heuresRepartition = getTotalHeuresPrevues(serviceDetail);
				float heuresRealisees = getTotalHeuresRealisees(serviceDetail);
				float heuresMaquette = getHeuresMaquette(serviceDetail.composantAP());

				if ((!isCircuitDefinitif()) && (heuresRepartition > heuresMaquette)) {				
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

				if ((isCircuitDefinitif()) && (heuresRealisees > heuresMaquette)) {
					throw new ValidationException(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE);				
				}

			}
		}

	}

	@Override
	protected EOServiceDetail creerNouvelObjet() {
		EOServiceDetail serviceDetail = EOServiceDetail.creerEtInitialiser(edc());
		return serviceDetail;
	}

	/**
	 * En mode modification, renvoit booléen pour savoir si Reh édité ou non
	 *
	 * @return modeReh
	 */
	public boolean modeReh() {
		return this.editedObject().reh() != null;
	}

	/**
	 * @return etat
	 */
	public String etat() {
		return controleur.etat(currentEtat);
	}

	/**
	 * @return liste des états
	 */
	public NSArray<String> listeEtat() {
		return controleur.listeEtat();
	}

	/**
	 * crée et initialise la répartition croisée si elle existe
	 *
	 * @param serviceDetail : la répartition du service
	 * @return une nouvelle répartition croisée
	 */
	protected EORepartEnseignant creerNouvelleRepart(EOServiceDetail serviceDetail) {
		EOPersonne personneConnecte = getPecheSession().getApplicationUser().getPersonne();
		EOIndividu repartiteurDemandeur =  getPecheSession().getApplicationUser().getIndividu();
		IStructure composanteReferent = serviceDetail.service().enseignant().getStructureAffectation(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		return EORepartEnseignant.creerNouvelleRepartition(serviceDetail, repartiteurDemandeur, 
				composanteReferent, personneConnecte, edc());
	}

	/**
	 * Vérifie que la composante de l'intervenant est la même que
	 * celle du répartiteur connecté.
	 *    ==> Vérifie les conditions dans lesquelles on peut permettre l'affichage de l'état de la répartition
	 *
	 * @return si la répartition est croisée ou non.
	 */
	public boolean isModeRepartCroisee() {
		EORepartEnseignant repartition = EORepartEnseignant.rechercherServiceDetail(edc(), this.editedObject());

		if (repartition != null) {
			return getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(repartition.composanteReferente());
		}

		return false;
	}

	/**
	 * Renvoie vrai si la personne connectée manipule un individu qui n'appartient pas au périmètre
	 * de ces structures (les structures de ces composantes scolarité).
	 * ==> permet de savoir si on est dans le cas d'une répartition croisée ou non
	 * @param enseignant L'enseignant manipulé
	 * @return vrai/faux
	 */
	public boolean isRepartitionCroisee(EOActuelEnseignant enseignant) {
		IStructure composanteEnseignant = enseignant.getStructureAffectation(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(composanteEnseignant);
	}

	/**
	 * Est-ce que l'état de la répartition croisée est en attente 
	 * 	ou l'enseignant n'est pas de la meme composante?
	 * @return vrai/faux
	 */
	public boolean isRepartitionCroiseeEtatEnAttente() {
		boolean etatRepartition = false;

		if (this.editedObject().service().enseignant() != null) {
			etatRepartition = ((Constante.ATTENTE.equals(this.editedObject().etat()) 
					&& isRepartitionCroisee(this.editedObject().service().enseignant()))
					|| !(isRepartitionCroisee(this.editedObject().service().enseignant())));
		}

		return etatRepartition;
	}

	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct suivant le mode d'édition
	 * @return affichage
	 */
	public String affichageAPEtParent() {
		if ((getModeEdition() == ModeEdition.MODIFICATION)) {
			return this.editedObject().composantAP().typeAP().code() + " - " + controleurComposant.affichageAPEtParent(this.editedObject().composantAP());
		} else {
			return controleurComposant.affichageAPEtParent(getCurrentComposantAP());
		}
	}

	/**
	 * action de notifier
	 */
	public void notifier() {
		if (isRepartitionREH()) {
			EOReh reh = this.controleur.getReh();
			editedObject().setReh(reh);
		} else {
			EOAP ap = this.controleur.getAP();
			if (ap != null) {
				editedObject().setComposantAP(ap);
				majListeServiceDetails();
			}
		}
	}

	/**
	 * Renvoie le total des heures maquette de de l'AP
	 * ==> Si l'AP est mutualisé, le paramètre false permet de calculer sa charge propre sans prendre 
	 * en compte le nombre de parent 
	 * 
	 * @param ap : AP
	 * @return les heures maquette
	 */
	private float getHeuresMaquette(EOAP ap) {
		return this.controleurUE.getHeuresMaquette(ap, false);
	}

	public float getHeuresMaquette() {
		return this.controleurUE.getHeuresMaquette(editedObject().composantAP(), false);
	}

	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getHeuresResteARepartir() {
		return controleurUE.getNbHeuresResteARepartir(editedObject().composantAP());
	}

	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getHeuresResteARealiser() {
		if (editedObject() != null & editedObject().composantAP()!=null) {
			return controleurUE.getNbHeuresResteARealiser(editedObject().composantAP());
		}
		return 0f;
	}

	public float getHeuresResteARepartir(EOServiceDetail serviceDetail) {
		return controleurUE.getNbHeuresResteARepartir(serviceDetail.composantAP());
	}

	public float getHeuresResteARealiser(EOServiceDetail serviceDetail) {
		return controleurUE.getNbHeuresResteARealiser(serviceDetail.composantAP());
	}

	/**
	 * @return les heures souhaitées par l'enseignant sur cet AP
	 */
	public double getHeuresSouhaitees() {
		return controleur.getNbHeuresSouhaiteesParAP(getEnseignant(), editedObject().composantAP());
	}

	/**
	 * Renvoie le total des heures prévues
	 * @param ap : AP
	 * @return les heures prévues
	 */
	private float getTotalHeuresPrevues(EOServiceDetail serviceDetail) {
		return this.controleurUE.getTotalHeuresPrevues(serviceDetail.composantAP(), getModeEdition(), serviceDetail.heuresPrevues(), false);
	}

	/**
	 * Renvoie le total des heures réalisées
	 * @param ap : AP
	 * @return les heures prévues
	 */
	private float getTotalHeuresRealisees(EOServiceDetail serviceDetail) {
		return this.controleurUE.getTotalHeuresRealisees(serviceDetail.composantAP(), getModeEdition(), serviceDetail.heuresRealisees(), false);
	}

	/**
	 * @return true si la structure est non null
	 */
	public boolean isStructureNonNull() {
		return this.editedObject().toStructure() != null && !"".equals(this.editedObject().toStructure()); 
	}

	/**
	 * Met à jour le commentaire de la répartition du service
	 * @param detail : répartition du service
	 * @param commentaire : commentaire
	 */
	private void majCommentaire(EOServiceDetail detail, String commentaire) {
		if (!StringCtrl.isEmpty(commentaire)) {
			detail.setCommentaire(commentaire);
			detail.setDateCommentaire(new NSTimestamp());
		}
	}

	/**
	 * Met à jour le détail du service si la répartition est croisée
	 * @param enseignant : enseignant
	 * @param modeEdition : mode
	 * @param detail
	 */
	private void majDetailSiRepartitionCroisee(EOActuelEnseignant enseignant, EOServiceDetail detail) {
		if (isRepartitionCroisee(enseignant)) {
			creerNouvelleRepart(detail);
			detail.setEtat(Constante.ATTENTE);
		}
	}
	/* ========== Getters & Setters ========== */

	
	public List<IStructure> getListeStructures() {
		return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementStatutaires();
	}
	
	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauServiceContainerId() {
		return getComponentId() + "_nouveauServiceContainerId";
	}

	@Override
	protected String getFormTitreCreationKey() {
		if (isCircuitDefinitif()) {
			return Messages.SERVICEREALISE_FORM_TITRE_AJOUTER;
		}
		return Messages.SERVICE_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		if (isCircuitDefinitif()) {
			return Messages.SERVICEREALISE_FORM_TITRE_MODIFIER;
		}
		return Messages.SERVICE_FORM_TITRE_MODIFIER;
	}

	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(EOActuelEnseignant enseignant) {
		this.enseignant = enseignant;
	}

	public void setRepartitionREH(boolean repartitionREH) {
		this.repartitionREH = repartitionREH;
	}

	public boolean isRepartitionREH() {
		return this.repartitionREH;
	}

	public EnseignantsStatutairesCtrl getControleur() {
		return controleur;
	}

	public void setControleur(EnseignantsStatutairesCtrl controleur) {
		this.controleur = controleur;
	}

	public String getCurrentEtat() {
		return currentEtat;
	}

	public void setCurrentEtat(String etat) {
		this.currentEtat = etat;
	}

	public EOAP getCurrentComposantAP() {
		return currentComposantAP;
	}

	public void setCurrentComposantAP(EOAP currentComposantAP) {
		this.currentComposantAP = currentComposantAP;
	}

	public boolean isCircuitDefinitif() {
		return !this.controleur.isCircuitPrevisionnel(getRepartService().toDemande());
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	public EOServiceDetail getCurrentServiceDetail() {
		return currentServiceDetail;
	}

	public void setCurrentServiceDetail(EOServiceDetail currentServiceDetail) {
		this.currentServiceDetail = currentServiceDetail;
	}

	/**
	 * Récupère la liste des serviceDetail pour un AP
	 * 	=> Si connu, on récupère, sinon, on l'initialise
	 * 
	 * @return liste EOServiceDetail
	 */
	public NSArray<EOServiceDetail> getListeServiceDetail() {
		return listeServiceDetail;
	}

	public void setListeServiceDetail(NSArray<EOServiceDetail> listeServiceDetail) {
		this.listeServiceDetail = listeServiceDetail;
	}

	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		if (getCurrentServiceDetail() != null) {
			return controleurUE.getNbHeuresResteARepartir(getCurrentServiceDetail().composantAP());
		}
		return 0f;
	}

	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser() {
		if (getCurrentServiceDetail() != null) {
			return controleurUE.getNbHeuresResteARealiser(getCurrentServiceDetail().composantAP());
		}
		return 0f;
	}

	public boolean isNbHeuresDejaCrees() {
		return getListeServiceDetailDejaCree().contains(getCurrentServiceDetail());
	}

	/**
	 * @return les heures maquette par AP
	 */
	public float getNbHeuresMaquette() {
		return controleurUE.getHeuresMaquette(getCurrentServiceDetail().composantAP(),false);
	}

	/**
	 * Est-ce qu'on affiche les détails de l'AP
	 * @return Vrai/Faux
	 */
	public boolean isAfficherDetailAP() {
		return (controleur.getAP() != null);
	}

	/**
	 * @return code et libellé du parent direct de l'AP
	 */
	public String affichageParentAP() {
		if (controleur.getAP() != null) {
			return controleurComposant.getParentAPCodeEtLibelle(controleur.getAP());
		}

		return null;
	}

	public boolean isModeValidationUeEc() {
		return modeValidationUeEc;
	}

	public void setModeValidationUeEc(boolean modeValidationUeEc) {
		this.modeValidationUeEc = modeValidationUeEc;
	}

	public String getTypeFicheAValider() {
		return typeFicheAValider;
	}

	public void setTypeFicheAValider(String typeFicheAValider) {
		this.typeFicheAValider = typeFicheAValider;
	}

	@Override
	public EORepartService getRepartService() {
		return repartService;
	}

	public void setRepartService(EORepartService repartService) {
		this.repartService = repartService;
	}

}