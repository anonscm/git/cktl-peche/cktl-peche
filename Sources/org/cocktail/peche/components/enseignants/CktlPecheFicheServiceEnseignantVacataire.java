package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl;
import org.cocktail.peche.components.enseignants.services.IServiceOngletEnseignant;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.outils.OutilsValidation;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.CktlAjaxUtils;

/**
 * Classe permettant de voir un récapitulatif de la fiche de service de
 * l'enseignant vacataire.
 *
 * @author Chama LAATIK
 *
 */
public class CktlPecheFicheServiceEnseignantVacataire extends CktlPecheBaseComponent {

	private static final long serialVersionUID = -3083589129445941763L;

	private EOActuelEnseignant editedObject;
	private EnseignantsVacatairesCtrl controleur;
	private EOService service;
	private String currentDates;
	
	private GenerateurPDFCtrl generateurPDFCtrl;
	
	@Inject
	private IServiceOngletEnseignant serviceOngletEnseignantVacataire;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheFicheServiceEnseignantVacataire(WOContext context) {
		super(context);
		this.controleur = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	private void verificationHeuresContrats() {
		if (this.controleur.getTotalNbHeuresVacation(editedObject) == 0) {
			getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, "Attention, le nombre d'heures du contrat est à zéro");
		}
	}

	/**
	 * Définit l'objet Enseignant et crée la fiche de service correspondante
	 * @param editedObject : EOActuelEnseignant
	 */
	public void setEditedObject(EOActuelEnseignant editedObject) {
		this.editedObject = editedObject;
		ModeEdition modeEdition = ModeEdition.MODIFICATION;
		verificationHeuresContrats();
		EOService serviceLocal = getService();
		if (serviceLocal == null) {
			modeEdition = ModeEdition.CREATION;
			if (serviceLocal == null) {
				service = EOService.creerEtInitialiser(edc());
			}
			
			try {
				beforeSaveChanges(modeEdition);
				edc().saveChanges();
			} catch (ValidationException e) {
				e.printStackTrace();
				this.setEditedObject(null);
				edc().invalidateAllObjects();
				edc().revert();
				throw e;
			}
		}
		
		controleur.suppressionRepartServiceSansRepartition(edc(), getService(), getPecheSession().getPecheAutorisationCache().getStructuresPourVacataires(controleur, getService()));
		
	}

	/**
	 * Renvoie l'enseignant
	 * 	si on accède directement à la fiche, renvoie le binding correspondant
	 * @return enseignant
	 */
	public EOActuelEnseignant getEditedObject() {
		if (editedObject == null) {
			this.editedObject = (EOActuelEnseignant) valueForBinding("enseignant");
		}
		return this.editedObject;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/ficheService.css");
	}
	
	/**
	 * renvoie la différence entre le nombre d'heures du contrat et le nombre d'heures
	 * réalisées par l'enseignant
	 * @return nombre d'heures
	 */
	public double getResteRealise() {
		return getHeuresContrat() - getServiceRealise();
	}
	
	/**
	 * renvoie les heures du contrat de travail d'un intervenant
	 * @return heures du contrat
	 */
	public double getHeuresContrat() {
		return this.controleur.getTotalNbHeuresVacation(editedObject);
	}

	/**
	 * renvoie le libellé du type de vacation
	 * @return type contrat travail
	 */
	public String getLibelleTypeVacation() {
		return this.controleur.getLibelleTypeVacation(editedObject);
	}

	/**
	 * renvoie le libellé du type associe à la vacation
	 * @return type contrat travail
	 */
	public String getLibelleTypeAssocieVacation() {
		return this.controleur.getLibelleTypeAssocieVacation(editedObject);
	}
	
	public boolean libelleTypeAssocieVacationExiste() {
		return !StringCtrl.isEmpty(getLibelleTypeAssocieVacation());
	}
	
	/**
	 *@return le total des heures prévisionnelles répartis que l'intervenant doit faire en HETD
	 */
	public double getServiceAttribue() {		
		return controleur.getServiceAttribue(getEditedObject());
	}
	
	
	/**
	 *@return le total des heures prévisionnelles REH répartis que l'intervenant doit faire en HETD
	 */
	public double getRehAttribue() {
		return controleur.getRehAttribue(getEditedObject());
	}

	/**
	 * renvoie la différence entre le nombre d'heures du contrat et le nombre d'heures
	 * prévisionnelles par l'enseignant
	 * @return nombre d'heures
	 */
	public double getReste() {
		return getHeuresContrat() - getServiceAttribue();
	}

	

	/**
	 * @return le total des heures réalisées répartis que l'intervenant doit faire en HETD
	 */
	public double getServiceRealise() {
		return controleur.getServiceRealise(getEditedObject());
	}


	/**
	 * @return le total des heures réalisées Reh répartis que l'intervenant doit faire en HETD
	 */
	public double getRehRealise() {
		return controleur.getRehRealise(getEditedObject());
	}
	
	/**
	 * @return le total des heures à payer en HETD
	 */
	public BigDecimal getServiceAPayer() {
		return controleur.getServiceTotalHeuresAPayerHETD(getEditedObject(), true);
	}

	/**
	 * @return le total des heures payées en HETD
	 */
	public BigDecimal getServicePaye() {
		return controleur.getServiceTotalHeuresAPayerHETD(getEditedObject(), false);
	}

	/**
	 * renvoie différence entre le nombre d'heures totales payées et le nombre d'heures totales
	 * réalisées par l'enseignant
	 * @return nombre d'heures
	 */
	public BigDecimal getDeltaPaye() {
		return getServicePaye().subtract(new BigDecimal(getServiceRealise()));
	}

	/**
	 * Retourne les dates des contrats de l'enseignant.
	 * @return les dates
	 */
	public NSMutableArray<String> getListeDatesContratsAnneeEnCours() {
		List<IVacataires> listeVacations = getEditedObject().getListeVacationsCourantesPourAnneeUniversitaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
		NSMutableArray<String> datesContrats = new NSMutableArray<String>();
		String dateDebut;
		String dateFin;
		String date;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

		for (IVacataires vacation : listeVacations) {
			dateDebut = formatter.format(vacation.getDateDebut().getTime());
			dateFin = formatter.format(vacation.getDateFin().getTime());
			date = "du " + dateDebut + " au " + dateFin;
			datesContrats.add(date);
		}
		return datesContrats;
	}

	/**
	 * Enregistre le service d'un intervenant.
	 *
	 * @throws ValidationException
	 *             : exception au cours de l'enregistrement.
	 */
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();

		Session pecheSession = getPecheSession();
		EOPersonne personne = pecheSession.getApplicationUser().getPersonne();

		if (modeEdition == ModeEdition.CREATION) {
			service.setPersonneCreation(personne);
			service.setDateCreation(now);
		}

		if (modeEdition == ModeEdition.MODIFICATION) {
			service.setPersonneModification(personne);
		}

		if (!StringCtrl.isEmpty(service.commentaire())) {
			service.setDateCommentaire(now);
		}

		service.setTemoinEnseignantGenerique("N");
		service.setTemoinValide("O");
		service.setDateModification(now);
		service.setEnseignant(editedObject);
		service.setAnnee(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
		service.majDonnesServiceDuStatutaire(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		service.majDonnesServiceReparti(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
	}

	/**
	 * Renvoie le service de l'enseignant
	 * @return un service
	 */
	public EOService getService() {
		if (service == null) {
			this.service = getEditedObject().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		}
		return service;
	}

	public void setService(EOService service) {
		this.service = service;
	}

	/**
	 * Enregistre le commentaire de la fiche de l'enseignant
	 */
	public void enregistrerCommentaireFiche() {
		try {
			PecheApplicationUser appUser = getPecheSession().getApplicationUser();
			EOPersonne personne = appUser.getPersonne();

			service.majDonnesAuditModification(personne);
			edc().saveChanges();

			getPecheSession().addSimpleInfoMessage(message("ficheService.message.commentaire.sauvegarde.titre"), message("ficheService.message.commentaire.sauvegarde.detail"));
		} catch (ValidationException e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}

	// Gestion du circuit de validation
	// ********************************



	/**
	 * Est-ce que le bloc "Répartition REH hors offre de formation" doit être présent  ?
	 * 
	 * @return <code>true</code> si inactif
	 */
	public boolean hasDroitShowRepartitionRehVacataire() {
		return getAutorisation().hasDroitShowSaisieRehVacataire();
	}

	
	/**
	 * 
	 * @return <code>true</code> si circuit prévisionnel
	 */
	public boolean isSurCircuitPrevisionnel() {
		return controleur.estFicheServiceNonDefinitif(getService());
	}

	/**
	 * Renvoie le menu selectionné
	 * ==> sert pour les personnes qui cumulent 2 profils dont celui d'enseignant
	 * @return id du menu
	 */
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", "vacataires");
	}

	/**
	 * Renvoie l'état de la fiche de service suivant son circuit de validation 
	 * @return état de la fiche
	 */
	public String getEtatFicheService() {
		String etatFiche = null;
		if (controleur.estFicheServiceNonDefinitif(getService())) {
			etatFiche = Constante.PREVISIONNEL;
		} else {
			etatFiche = Constante.DEFINITIF;
		}
		return etatFiche;
	}

	public String getCurrentDates() {
		return currentDates;
	}

	public void setCurrentDates(String currentDates) {
		this.currentDates = currentDates;
	}

	public IServiceOngletEnseignant getServiceOngletEnseignantVacataire() {
		return serviceOngletEnseignantVacataire;
	}
	
	
	public EnseignantsVacatairesCtrl getControleur() {
		return controleur;
	}

	/**
	 * @return la methode de calcul
	 */
	public String getMethodeCalcul() {
		return getEditedObject().getMethodeCalcul(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
	
	public boolean historiquesPresent() {		
		return historiqueNmoinsUnPresent() || historiqueNmoinsDeuxPresent();
	}

	public boolean historiqueNmoinsUnPresent() {
		return (!editedObject.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 1, 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())
				&& (editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 1) != null));
	}

	public boolean historiqueNmoinsDeuxPresent() {
		return (!editedObject.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 2, 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())
				&& (editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - 2) != null));
	}

	public WOActionResults imprimerFicheNmoinsDeux() {
		return imprimerFicheNmoins(2);
	}
	
	public WOActionResults imprimerFicheNmoinsUn() {
		return imprimerFicheNmoins(1);
	}
	
	public WOActionResults imprimerFicheNmoins(int annee) {
		EnseignantsVacatairesCtrl controleurNmoins = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee,
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		//génération du xml
		String fluxXml = controleurNmoins.getFluxFicheService(editedObject, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee, getPecheSession(), getComponentCtrl(), "Fiche individuelle de service");

		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

		EOService serviceNmoins = editedObject.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee);
		EODemande demande = controleurNmoins.getDemande(serviceNmoins);

		generateurPDFCtrl.setReportFilename(controleurNmoins.getFilenameFicheService(editedObject,
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours() - annee, false, controleurNmoins.isCircuitPrevisionnel(demande)) + ".pdf");
		generateurPDFCtrl.genererFicheService("Reports/ficheServiceVacataire/fiche_service_vacat.jasper", false);

		return null;
	}
	
	public GenerateurPDFCtrl getGenerateurPDFCtrl() {
		return generateurPDFCtrl;
	}
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}
}