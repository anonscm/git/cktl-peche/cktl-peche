package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartAbsence;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * Classe permettant de modifier le nombre d'heures d'absences d'un enseignant statutaire
 * 
 * @author Chama LAATIK
 */
public class CktlPecheFormMinorationService extends CktlPecheTableComponent<MinorationsServiceDataBean> {
	
	private static final long serialVersionUID = -4319807255025691646L;
	
	private EOActuelEnseignant enseignant;
	private MinorationsServiceDataBean editedObject;
	private BigDecimal nbHeuresAbsence;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public CktlPecheFormMinorationService(WOContext context) {
        super(context);
    }
    
    /**
	 * Enregistrement du nombre d'heures.
	 * @return le formulaire complet.
	 */
	public WOActionResults enregistrerEtRetour() {
		enregistrer();
		this.setEditedObject(null);
		
		return pageGestion();
	}
	
	private void enregistrer() {
		try {
			beforeSaveChanges();
			edc().saveChanges();
		} catch (ValidationException e) {
			this.setEditedObject(null);
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}
	
	protected void beforeSaveChanges() {
		EORepartAbsence uneRepartAbsence = null;
		EOService service = getEnseignant().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		if (getEditedObject().getAbsence().getRepartAbsence(service) == null) {
			//Si pas de répartion d'heures, on en crée
			uneRepartAbsence = EORepartAbsence.creerEtInitialiser(edc(), this.editedObject.getAbsence(), service,
					getPecheSession().getApplicationUser().getPersonne());
		} else {
			//Sinon on récupère la répartition existante
			uneRepartAbsence = editedObject.getAbsence().getRepartAbsence(service);
		}
		
		uneRepartAbsence.setNbHeures(getNbHeuresAbsence());
		uneRepartAbsence.majDonnesAuditModification(getPecheSession().getApplicationUser().getPersonne());
		service.majDonnesServiceDuStatutaire(edc(),
				getPecheSession().getApplicationUser().getPersonne(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

	}
    
    /**
	 * @return la fiche de service de l'enseignant.
	 */
	public WOActionResults annuler() {
		edc().revert();
		return pageGestion();
	}

	protected WOActionResults pageGestion() {
		CktlPecheFicheServiceEnseignantStatutaire page = (CktlPecheFicheServiceEnseignantStatutaire)
				pageWithName(CktlPecheFicheServiceEnseignantStatutaire.class.getName());
		page.setEditedObject(getEnseignant());
		return page;
	}
    
	/**
	 * Formatter à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp2DecimalesFormatter() {
		return OutilsValidation.getApp2DecimalesFormatter();	
	}
	
    /**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauMinorationContainerId() {
		return getComponentId() + "_nouveauMinorationContainerId";
	}
	
	/**
	 * @return Titre du formulaire
	 */
	public String formTitre() {
		return message(Messages.ABSENCE_FORM_TITRE_MODIFIER);
	}

	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}

	public void setEnseignant(EOActuelEnseignant enseignant) {
		this.enseignant = enseignant;
	}

	public MinorationsServiceDataBean getEditedObject() {
		return editedObject;
	}

	/**
	 * @param editedObject : absence
	 */
	public void setEditedObject(MinorationsServiceDataBean editedObject) {
		if (editedObject != null) {
			this.editedObject = editedObject;
	
			//On récupère les heures de l'absence déjà renseignées
			if (getEditedObject().isAbsence()) {
				setNbHeuresAbsence(getEditedObject().getAbsence().getNbHeuresAbsence(
						getEnseignant().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())));
			}
		}
	}

	/**
	 * @return le nombre d'heures de l'absence
	 */
	public BigDecimal getNbHeuresAbsence() {
		return nbHeuresAbsence;
	}

	public void setNbHeuresAbsence(BigDecimal nbHeuresAbsence) {
		this.nbHeuresAbsence = nbHeuresAbsence;
	}
}