package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesVacCtrl;
import org.cocktail.peche.components.controlers.IEnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EORepartService;

import com.webobjects.appserver.WOContext;

public class CktlPecheOngletRepartServiceEnseignantGenerique extends CktlPecheOngletRepartServiceAbstract {
    
	
	private EnseignantsGeneriquesVacCtrl controleur;
	
	
	public CktlPecheOngletRepartServiceEnseignantGenerique(WOContext context) {
        super(context);
        controleur = new EnseignantsGeneriquesVacCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    }

	@Override
	protected void gererCreationTypeRepartService(IStructure structure) {
		EORepartService.creerNouvelRepartService(edc(), getService(), structure, 
				getPecheSession().getApplicationUser().getPersonne());
		edc().saveChanges();
	}

	@Override
	public IEnseignantsVacatairesCtrl getControleur() {
		return controleur;
	}
}