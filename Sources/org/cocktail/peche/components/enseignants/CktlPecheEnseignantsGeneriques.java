package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;

import com.webobjects.appserver.WOContext;

/**
 * Affichage des enseignants génériques.
 */
public class CktlPecheEnseignantsGeneriques extends CktlPecheBaseComponent {

	private static final long serialVersionUID = -2612649137508452219L;

	private String selectedItemId;
	private boolean isTypeStatutaire;
	
	/**
	 * Constructeur.
	 * @param context le context WebObjects.
	 */
	public CktlPecheEnseignantsGeneriques(WOContext context) {
		super(context);
	}
	
	/**
	 * @return titre de la page
	 */
	public String titre() {
		String titre = message("EnseignantGenerique.Liste.Vacataire");
		if (isTypeStatutaire()) {
			titre = message("EnseignantGenerique.Liste.Statutaire");
		}
		
		return titre;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

	public boolean isTypeStatutaire() {
		return isTypeStatutaire;
	}

	public void setTypeStatutaire(boolean isTypeStatutaire) {
		this.isTypeStatutaire = isTypeStatutaire;
	}
	
}