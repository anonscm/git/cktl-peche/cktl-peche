package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public abstract class CktlPecheTableRepartitionEnseignantGeneriqueStat extends CktlPecheTableRepartitionEnseignant {

	
private static final long serialVersionUID = 182418446105775964L;

	private EnseignantsStatutairesCtrl enseignantsStatutairesCtrl;
	
	private EORepartService repartService;
	
	/**
	 * Constructeur.
	 * @param context
	 */
	public CktlPecheTableRepartitionEnseignantGeneriqueStat(WOContext context) {
		super(context);
		this.enseignantsStatutairesCtrl = new EnseignantsStatutairesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOServiceDetail.ENTITY_NAME);
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
	}

	@Override
	public void awake() {
		repartService = (EORepartService) valueForBinding("repartService");
		
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		
		String path = ERXQ.keyPath(EOServiceDetail.TO_REPART_SERVICE_KEY);
		EOQualifier qualifier = ERXQ.and(
			ERXQ.equals(path, repartService),
			ERXQ.isNotNull(getTableKey())
			);
        ((ERXDatabaseDataSource) dg.dataSource()).setAuxiliaryQualifier(qualifier);
		dg.fetch();
	}

	protected abstract String getTableKey();

	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	
	public EORepartService getRepartService() {
		return repartService;
	}
	
	
	@Override
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId());
	}
	
	/**
	 * 
	 * @return sous total heures prévues
	 */
	public Double getSousTotalHeuresPrevues() {
		return enseignantsStatutairesCtrl.getTotalHeuresPrevues(getDisplayGroup().allObjects());
	}
	
	
	
}
