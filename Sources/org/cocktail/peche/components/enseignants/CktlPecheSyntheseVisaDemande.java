package org.cocktail.peche.components.enseignants;

import java.util.Map;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.DemandeCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Bloc synthèse des visas d'une demande (toutes les actions "VALIDER" des circuits d'une demande).
 * <p>
 * Apparait dans les fiches de service des enseignants statutaires et vacataires.
 * 
 * @author Pascal
 */
public class CktlPecheSyntheseVisaDemande extends WOComponent {
	/** Numéro de série. */
	private static final long serialVersionUID = -7380392546046640683L;

	private EODemande demande;
	private boolean isLibelleTypeCircuitAffiche;
	private EOCircuitValidation currentCircuit;
	private EOEtape currentEtapeUtilisee;
	private boolean estFicheVoeux;

	private boolean flagVisa;
	private DemandeCtrl controleur;
	private EOEditingContext editingContext;


	/**
	 * Constructeur.
	 * 
	 * @param context Un contexte WO
	 */
	public CktlPecheSyntheseVisaDemande(WOContext context) {
		super(context);
		flagVisa = true;
		controleur = new DemandeCtrl(editingContext);
	}

	@Override
	public void pullValuesFromParent() {
		super.pullValuesFromParent();
		setDemande((EODemande) valueForBinding("demande"));
		setLibelleTypeCircuitAffiche(valueForBooleanBinding("libelleTypeCircuitAffiche", true));
		setEstFicheVoeux(valueForBooleanBinding("estFicheVoeux", false));
	}

	public EODemande getDemande() {
		return demande;
	}

	public void setDemande(EODemande demande) {
		this.demande = demande;
	}

	public boolean isLibelleTypeCircuitAffiche() {
		return isLibelleTypeCircuitAffiche;
	}

	public void setLibelleTypeCircuitAffiche(boolean isLibelleTypeCircuitAffiche) {
		this.isLibelleTypeCircuitAffiche = isLibelleTypeCircuitAffiche;
	}

	public EOCircuitValidation getCurrentCircuit() {
		return currentCircuit;
	}

	public void setCurrentCircuit(EOCircuitValidation currentCircuit) {
		this.currentCircuit = currentCircuit;
	}

	/**
	 * Retourne le libellé du type de la fiche déterminé en fonction du circuit emprunté.
	 * 
	 * @return Un libellé du type de la fiche
	 */
	public String getLibelleFicheTypeCircuit() {
		if (CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit().equals(currentCircuit.codeCircuitValidation())
				|| CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(currentCircuit.codeCircuitValidation())) {
			return "Fiche prévisionnelle";
		} else {
			return "Fiche définitive";
		}
	}

	/**
	 * Retourne les étapes utilisées dans le circuit et utilisées pour l'affichage des visas.
	 * C'est-à-dire tous sauf l'étape finale.
	 * 
	 * @return Les étapes utilisées par cette demande dans le circuit en cours de parcours 
	 */
	public NSArray<EOEtape> getEtapesUtilisees() {
		NSMutableArray<EOEtape> etapesUtiliseesTemp = currentCircuit.etapesUtiliseesSuivantOrdreChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), TypeCheminPeche.REFUSER.getCodeTypeChemin()).mutableClone();
		for (int iEtape = 0; iEtape < etapesUtiliseesTemp.size(); iEtape++) {
			EOEtape etape = etapesUtiliseesTemp.get(iEtape);

			if (etape.isFinale()) {
				etapesUtiliseesTemp.remove(etape);
			}
		}
		flagVisa = true;
		return etapesUtiliseesTemp;
	}

	public EOHistoriqueDemande getCurrentVisa() {
		return getMapVisas().get(currentEtapeUtilisee.codeEtape());
	}
	
	public Map<String, EOHistoriqueDemande> getMapVisas() {
		return controleur.getMapVisas(demande, getCurrentCircuit());
	}

	/**
	 * @return <code>true</code> si l'étape est l'étape en cours
	 */
	public boolean isEtapeEnCours() {
		if (demande == null) {
			return false;
		}
		
		boolean flag = (currentEtapeUtilisee == demande.toEtape()); 
		
		if (flagVisa && flag) {
			flagVisa = false;
		}

		return flag;
	}


	public EOEtape getCurrentEtapeUtilisee() {
		return currentEtapeUtilisee;
	}

	public void setCurrentEtapeUtilisee(EOEtape currentEtapeUtilisee) {
		this.currentEtapeUtilisee = currentEtapeUtilisee;
	}

	/**
	 * @return La liste des circuits empruntés par cette demande
	 */
	public NSArray<EOCircuitValidation> getCircuitsEmpruntes() {
		if (demande == null) {
			return new NSArray<EOCircuitValidation>();
		}

		return demande.rechercherCircuitsEmpruntes(true);
	}

	/**
	 * @return Le texte de l'étape en cours
	 */
	public String getEtatEtape() {
		String etatEtape = controleur.etatEtape(demande);
		if (estFicheVoeux()) {
			if (EtapePeche.VALIDEE.getEtatDemande().equals(etatEtape)) {
				etatEtape = Constante.ETAT_BASCULE;
			}
		}
		return etatEtape;
	}

	public String getCurrentEtapeUtiliseeLibelleVisa() {
		return EtapePeche.getEtape(currentEtapeUtilisee).getLibelleVisa();
	}

	/**
	 * La date d'initialisation du dossier est la date de rentré dans le circuit.
	 * 
	 * @return La date d'entrée de la demande sur son premier circuit
	 */
	public NSTimestamp getDateInitialisation() {
		return controleur.getDateInitialisation(demande);
	}

	/**
	 * C'est la date de première étape validées dans l'historique d'une demande.
	 * 
	 * @return La date de publication de la demande
	 */
	public NSTimestamp getDatePremierePublication() {
		if (demande == null) {
			return null;
		}

		NSArray<EOHistoriqueDemande> listeHistoriqueDemande = demande.historiqueChronologique(EOHistoriqueDemande.TO_ETAPE_DEPART.isNotNull());

		NSTimestamp datePremierPublication = null;

		if (listeHistoriqueDemande.size() > 0) {
			datePremierPublication = listeHistoriqueDemande.get(0).dateModification();
		}

		return datePremierPublication;
	}

	public boolean isFlagVisa() {
		return flagVisa;
	}

	/**
	 * 
	 * @return affiche-t-on la ligne Visa ?
	 */
	public boolean estLigneVisaAffichee() {
		
		boolean ok = false;
		
		for (EOHistoriqueDemande historiqueDemande : getMapVisas().values()) {
			
			if (getCurrentEtapeUtilisee().equals(historiqueDemande.toEtapeDepart())) {
				ok = true;
			}
			
		}
		
		
		
		return ok;
	}
	
	public void setFlagVisa(boolean flagVisa) {
		this.flagVisa = flagVisa;
	}

	public String getSrcImage() {
		return "images/valider16x16.png";
	}
	
	/**
	 * @return Est-ce que la fiche est validée?
	 */
	public boolean isDemandeValidee() {
		return controleur.isDemandeValidee(demande);
	}

	@Override
	public void awake() {
		super.awake();
		editingContext = (EOEditingContext) valueForBinding("editingContext");
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public boolean estFicheVoeux() {
		return estFicheVoeux;
	}

	public void setEstFicheVoeux(boolean isFicheVoeux) {
		this.estFicheVoeux = isFicheVoeux;
	}
}