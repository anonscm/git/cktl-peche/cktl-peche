package org.cocktail.peche.components.enseignants;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.controlers.DemandeCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 *
 */
public class CktlPecheResumeEtatDossierVacataire extends CktlPecheBaseComponent {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = -7259211319983668738L;

	private EOService service;
	private EORepartService currentRepartService;
	private DemandeCtrl controller;
	private EnseignantsVacatairesCtrl controllerVacataire;
	private EOEditingContext editingContext;
	private EOCircuitValidation currentCircuit;
	private EOEtape currentEtapeUtilisee;
	private boolean hasFichePrevisionnelle;
	private Map<String, EOHistoriqueDemande> mapVisas = new HashMap<String, EOHistoriqueDemande>();
	
	/**
	 * 
	 * @param context contexte
	 */
	public CktlPecheResumeEtatDossierVacataire(WOContext context) {
        super(context);
        controller = new DemandeCtrl(editingContext);
        controllerVacataire = new EnseignantsVacatairesCtrl(editingContext,getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
     
    }

	@Override
	public void awake() {
		service = (EOService) valueForBinding("service");
		editingContext = (EOEditingContext) valueForBinding("editingContext");
		super.awake();
	}

	
	/**
	 * 
	 * @return état de la demande
	 */
	public String etatEtape() {
		return controller.etatEtape(getCurrentRepartService().toDemande());
	}
	
	
	/**
	 * La date d'initialisation du dossier est la date de rentré dans le circuit.
	 * 
	 * @return La date d'entrée de la demande sur son premier circuit
	 */
	public NSTimestamp getDateInitialisation() {
		return controller.getDateInitialisation(getPremiereDemande());
	}
	
	/**
	 * @return Est-ce que la fiche est validée?
	 */
	public boolean isDemandeValidee() {
		return controller.isDemandeValidee(getCurrentRepartService().toDemande());
	}
	
	
	/**
	 * Retourne le libellé du type de la fiche déterminé en fonction du circuit emprunté.
	 * 
	 * @return Un libellé du type de la fiche
	 */
	public String getLibelleFicheTypeCircuit() {
		if (CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(currentCircuit.codeCircuitValidation())) {
			hasFichePrevisionnelle = true;
			return "Fiche prévisionnelle";
		} else {
			return "Fiche définitive";
		}
	}
	
	/**
	 * 
	 * @return  boolean
	 */
	public boolean afficherVisaEtDate() {
		if (CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(getCurrentCircuit().codeCircuitValidation())) {
			return true;
		}		
		/*if (CircuitPeche.PECHE_VACATAIRE.getCodeCircuit().equals(getCurrentCircuit().codeCircuitValidation())) {
			if (hasFichePrevisionnelle) {
				return false;
			}
		}*/
		return false;
	}
	
	/**
	 * 
	 * @return liste circuits
	 */
	public NSArray<EOCircuitValidation> circuitsEmpruntes() {
		if (getPremiereDemande() == null) {
			return new NSArray<EOCircuitValidation>();
		}
		if (!controllerVacataire.estToutesDemandeAvecRepartitionDefinitives(getService())) {
			// dans le cas où toutes les fiches ne sont pas dans le circuit définitif, on n'affiche pas de visa répartiteur pour la fiche prévisionnelle
			NSArray<EOCircuitValidation> listeCircuitsEmpruntes = getPremiereDemande().rechercherCircuitsEmpruntes(true);
			NSArray<EOCircuitValidation> listeCircuits = new NSMutableArray<EOCircuitValidation>();
			listeCircuits.add(listeCircuitsEmpruntes.get(0));
			return listeCircuits;
		}
		return getPremiereDemande().rechercherCircuitsEmpruntes(true); 		
	}
	
	
	public String getSrcImage() {
		return "images/valider16x16.png";
	}
	
	/**
	 * 
	 * @return libellé du circuit courant
	 */
	public String getCurrentEtapeUtiliseeLibelleVisa() {
		return EtapePeche.getEtape(getCurrentEtapeUtilisee()).getLibelleVisa();
	}
	
	
	/**
	 * @return <code>true</code> si l'étape est l'étape en cours ?
	 */
	public boolean isEtapeEnCours() {
		
		initVisa();
		setCurrentEtapeUtilisee(currentCircuit.etapeInitiale());
		
		if (getPremiereDemande() == null || !controllerVacataire.estToutesDemandeAvecRepartitionDefinitives(getService())) {
			return true;
		}
		
		
		return (currentEtapeUtilisee == getPremiereDemande().toEtape()); 
		
	}
	
	
	/**
	 * On récupère les visa des chemins "VALIDER". Mais uniquement du premier de chaque circuit en cours
	 */
	public void initVisa() {
		mapVisas.clear();

		if (getPremiereDemande() != null && controllerVacataire.estToutesDemandeAvecRepartitionDefinitives(getService())) {
			NSArray<EOHistoriqueDemande> listeHistoriqueDemande = 
					getPremiereDemande().historiqueChronologique(EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(currentCircuit.codeCircuitValidation()));

			if (listeHistoriqueDemande != null && listeHistoriqueDemande.size() > 0) {
				EOEtape etapeArrivee = listeHistoriqueDemande.get(0).toEtapeArrivee();

				if (etapeArrivee != null) {
					EOChemin cheminValider = etapeArrivee.chemin(TypeCheminPeche.VALIDER.getCodeTypeChemin());

					if (cheminValider != null && listeHistoriqueDemande.size() > 1 
							&& cheminValider.toEtapeArrivee().equals(listeHistoriqueDemande.get(1).toEtapeArrivee())) {

						mapVisas.put(etapeArrivee.codeEtape(), listeHistoriqueDemande.get(0));
						
					}
				}
			}
		}
	}
	
	/**
	 * 
	 * @return true si image à afficher
	 */
	public boolean afficherImage() {
		return getCurrentVisa().dateModification() != null;
	}
	
	public EOHistoriqueDemande getCurrentVisa() {
		return mapVisas.get(getCurrentEtapeUtilisee().codeEtape());
	}
	
	
	private EODemande getPremiereDemande() {
		if (getService().toListeRepartService() == null || getService().toListeRepartService().isEmpty()) {
			return null;
		}
		return getService().toListeRepartService().get(0).toDemande();
	}
	
	
	public EOService getService() {
		return service;
	}

	public void setService(EOService service) {
		this.service = service;
	}

	public EORepartService getCurrentRepartService() {
		return currentRepartService;
	}

	public void setCurrentRepartService(EORepartService currentRepartService) {
		this.currentRepartService = currentRepartService;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public EOCircuitValidation getCurrentCircuit() {
		return currentCircuit;
	}

	public void setCurrentCircuit(EOCircuitValidation currentCircuit) {
		this.currentCircuit = currentCircuit;
	}

	public EOEtape getCurrentEtapeUtilisee() {
		return currentEtapeUtilisee;
	}

	public void setCurrentEtapeUtilisee(EOEtape currentEtapeUtilisee) {
		this.currentEtapeUtilisee = currentEtapeUtilisee;
	}


}