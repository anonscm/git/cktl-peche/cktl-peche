package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EORepartService;

import com.webobjects.appserver.WOContext;

public class CktlPecheOngletRepartServiceEnseignantVacataire extends CktlPecheOngletRepartServiceAbstract {
    
    
    private static final long serialVersionUID = -3083589129445941763L;

	private EnseignantsVacatairesCtrl controleur;
	

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheOngletRepartServiceEnseignantVacataire(WOContext context) {
		super(context);
		controleur = new EnseignantsVacatairesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * Renvoie le menu selectionné
	 * ==> sert pour les personnes qui cumulent 2 profils dont celui d'enseignant
	 * @return id du menu
	 */
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", "vacataires");
	}

	public EnseignantsVacatairesCtrl getControleur() {
		return controleur;
	}
	
	
	/**
	 * Renvoie l'état de la fiche de service suivant son circuit de validation 
	 * @return état de la fiche
	 */
	public String getEtatFicheService() {
		String etatFiche = null;
		if (estPasOngletTous()) {
			if (getControleur().isCircuitPrevisionnel(getControleur().getDemande(getRepartService()))) {
				etatFiche = Constante.PREVISIONNEL;
			} else {
				etatFiche = Constante.DEFINITIF;
			}
		}
		return etatFiche;
	}

	/**
	 * @return L'état de la demande
	 */
	public String getEtatEtapeDemande() {
		return EtapePeche.getEtape(getDemande().toEtape()).getEtatDemande();
	}
	
	
	
	 
	/**
	 * 
	 * @return <code>true</code> si circuit prévisionnel
	 */
	public boolean isSurCircuitPrevisionnel() {
		return getControleur().estFicheServiceNonDefinitif(getService());
	}

	/**
	 * 
	 * @return demande en cours
	 */
	public EODemande getDemande() {
		return getControleur().getDemande(getRepartService());
	}

	@Override
	protected void gererCreationTypeRepartService(IStructure structure) {
		
		if (getControleur().estToutesDemandesPrevisionnelles(getService())) {
			EORepartService.creerNouvelRepartService(edc(), getService(), structure, 
					getPecheSession().getApplicationUser().getPersonne(), Constante.CIRCUIT_VACATAIRE_A_DETERMINER, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		} else {
			EORepartService.creerNouvelRepartService(edc(), getService(), structure, 
					getPecheSession().getApplicationUser().getPersonne(), CircuitPeche.PECHE_VACATAIRE.getCodeCircuit(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		}
		edc().saveChanges();		
	}
	
	
    
}