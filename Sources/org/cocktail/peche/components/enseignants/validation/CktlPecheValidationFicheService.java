package org.cocktail.peche.components.enseignants.validation;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlgrh.common.metier.EOAffectation;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.IndicateurValidationAuto;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public class CktlPecheValidationFicheService extends CktlPecheTableComponent<EORepartService> {
	
	private String typeFiche;
	
	private EnseignantsStatutairesCtrl controleurStat;
	private EnseignantsVacatairesCtrl controleurVac;
	private BasicPecheCtrl basicPecheCtrl;
	
    public CktlPecheValidationFicheService(WOContext context) {
        super(context);
        controleurStat = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        
        controleurVac = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        
        basicPecheCtrl = new BasicPecheCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());    
        
    }
    

	public String getTypeFiche() {
		return typeFiche;
	}

	public void setTypeFiche(String typeFiche) {
		this.typeFiche = typeFiche;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EORepartService.ENTITY_NAME);
        ERXDisplayGroup<EORepartService> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		
		NSArray<EORepartService> listeRepartServiceStatutaire = getRepartServiceStatutaire();
		NSArray<EORepartService> listeRepartServiceVacataire = getRepartServiceVacataire();
		
		NSArray<EORepartService> listeRepartService = NSArrayCtrl.unionOfNSArrays(new NSArray[] {
				listeRepartServiceStatutaire, listeRepartServiceVacataire
		});
		
		NSArray<EOSortOrdering> tri = EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE).ascInsensitives()
				.then(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.PRENOM_AFFICHAGE).asc())
				.then(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU).asc());
		
		EOSortOrdering.sortedArrayUsingKeyOrderArray(listeRepartService, tri);
		dg.setObjectArray(listeRepartService);
		
		super.appendToResponse(response, context);		
	}


	private NSArray<EORepartService> getRepartServiceVacataire() {
		NSArray<EORepartService> listeRepartServiceVacataire = new NSMutableArray<EORepartService>();
		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignementsVacataire()) {
			listeRepartServiceVacataire = EORepartService.fetchEORepartServices(edc(), getQualifierValidationVacataire(), null);
		}
		return listeRepartServiceVacataire;
	}


	private NSArray<EORepartService> getRepartServiceStatutaire() {
		NSArray<EORepartService> listeRepartServiceStatutaire = new NSMutableArray<EORepartService>();
		if (getPecheSession().getPecheAutorisationCache().hasListeCibleEnseignantsStatutaires()) {
			listeRepartServiceStatutaire = EORepartService.fetchEORepartServices(edc(), getQualifierValidationStatutaire(), null);
		}
		return listeRepartServiceStatutaire;
	}
	
	
	private EOQualifier getQualifierValidationStatutaire() {
		EOQualifier qualStatutaire = ERXQ.equals(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TEM_STATUTAIRE).key(), Constante.OUI);
		List<IStructure> listeStructuresStatutaires = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
		NSArray<EOQualifier> qualifierPourStructuresStatutaires = getQualifierStructuresStatutaires(listeStructuresStatutaires);
		EOQualifier qualStructuresStatutaire = ERXQ.and(ERXQ.or(qualifierPourStructuresStatutaires), qualStatutaire);
		
		return ERXQ.and(getQualifierValidationCommun(true), qualStructuresStatutaire);
	}
	
	private EOQualifier getQualifierValidationVacataire() {
		EOQualifier qualVacataire = ERXQ.equals(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TEM_STATUTAIRE).key(), Constante.NON);
		List<IStructure> listeStructuresVacataires = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire();
		NSArray<EOQualifier> qualifierPourStructuresVacataires = getQualifierStructuresVacataires(listeStructuresVacataires);
		EOQualifier qualStructuresVacataire = ERXQ.and(ERXQ.or(qualifierPourStructuresVacataires), qualVacataire);
		
		return ERXQ.and(getQualifierValidationCommun(false), qualStructuresVacataire);
		
	}
	
	
	private EOQualifier getQualifierValidationCommun(boolean isStatutaire) {
		
		Integer annee = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
		
		NSArray<String> circuits = new NSMutableArray<String>();
		if (Constante.PREVISIONNEL.equals(getTypeFiche())) {
			
			if (isStatutaire) {
				circuits.add(CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit());
			} else {
				circuits.add(CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit());
			}
			
		} else {
			
			if (isStatutaire) {
				circuits.add(CircuitPeche.PECHE_STATUTAIRE.getCodeCircuit());
			} else {
				circuits.add(CircuitPeche.PECHE_VACATAIRE.getCodeCircuit());
			}
			
		}
		NSArray<String> etapes = new NSMutableArray<String>();
		etapes.add(EtapePeche.VALID_REPARTITEUR.getCodeEtape());
		etapes.add(EtapePeche.VALID_DIR_COMPOSANTE.getCodeEtape());
		etapes.add(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape());
		
		
		EOQualifier qualAnnee = ERXQ.equals(EORepartService.TO_SERVICE.dot(EOService.ANNEE).key(), annee);
		EOQualifier qualcircuits = ERXQ.in(EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.TO_CIRCUIT_VALIDATION.dot(
				EOCircuitValidation.CODE_CIRCUIT_VALIDATION_KEY))).key(), circuits);
		EOQualifier qualetapes = ERXQ.in(EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE.dot(EOEtape.CODE_ETAPE_KEY)).key(), etapes);
		EOQualifier qualIndicateurREH = ERXQ.equals(EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY, IndicateurValidationAuto.ETAT_IND_REH.getCodeIndicateur());
		EOQualifier qualIndicateurUEF = ERXQ.equals(EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY, IndicateurValidationAuto.ETAT_IND_UEF.getCodeIndicateur());
		EOQualifier qualIndicateurREF = ERXQ.equals(EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY, IndicateurValidationAuto.ETAT_IND_REF.getCodeIndicateur());
		EOQualifier qualIndicateurVAL = ERXQ.equals(EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY, IndicateurValidationAuto.ETAT_IND_VALIDE.getCodeIndicateur());
		
		EOQualifier qualIndicateurs = ERXQ.or(qualIndicateurREH,qualIndicateurUEF,qualIndicateurREF,qualIndicateurVAL);
		
		EOQualifier qual = ERXQ.and(qualAnnee, qualcircuits, qualIndicateurs, qualetapes);		
		
		return qual;
		
	}

	private NSArray<EOQualifier> getQualifierStructuresStatutaires(List<IStructure> listeStructures) {
		NSArray<EOQualifier> qualifierPourStructures = new NSMutableArray<EOQualifier>();
		if (!CollectionUtils.isEmpty(listeStructures)) {
			for (IStructure structure : listeStructures) {
				qualifierPourStructures.add(
						ERXQ.and(
								ERXQ.equals(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_AFFECTATION).dot(EOAffectation.C_STRUCTURE_KEY).key(), structure.cStructure()),
								ERXQ.equals(EORepartService.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_AFFECTATION).dot(EOAffectation.TEM_PRINCIPALE_KEY).key(), Constante.OUI))		
						);
			}
		}
		return qualifierPourStructures;
	}

	private NSArray<EOQualifier> getQualifierStructuresVacataires(List<IStructure> listeStructures) {
		NSArray<EOQualifier> qualifierPourStructures = new NSMutableArray<EOQualifier>();
		
		if (!CollectionUtils.isEmpty(listeStructures)) {
			qualifierPourStructures.add(ERXQ.in(EORepartService.TO_STRUCTURE_KEY, getListHelper().bidouilleListeStructurePourWO(listeStructures)));
		}

		return qualifierPourStructures;
	}
	

	/**
	 * 
	 * @return état de la fiche
	 */
	public String etapeEnCours() {
		return EtapePeche.getEtape(getCurrentItem().toDemande().toEtape()).getEtatDemande();
	}

	/**
	 * 
	 * @return étape suivante de la fiche
	 */
	public String etapeSuivante() {
		return EtapePeche.getEtape(getCurrentItem().toDemande().toEtape().etapeSuivante(TypeCheminPeche.VALIDER.getCodeTypeChemin())).getEtatDemande();
	}

	/**
	 * 
	 * @return selectedItemId
	 */
	public String getSelectedItemId() {
		if (Constante.PREVISIONNEL.equals(getTypeFiche())) {
			return PecheMenuItem.ENSEIGNANTS_VALIDATION_PREVISIONNEL.getId();
		} else {
			return PecheMenuItem.ENSEIGNANTS_VALIDATION_DEFINITIF.getId();
		}
	}
	

	/**
	 * 
	 * @return selectedItemId
	 */
	public String getTitre() {
		if (Constante.PREVISIONNEL.equals(getTypeFiche())) {
			return message("ValidationFicheService.titre.previsionnel");
		} else {
			return message("ValidationFicheService.titre.definitif");
		}
	}

	

	/**
	 * 
	 * @return même page
	 */
	public WOActionResults validerAction() {
		int nbFichesValidesAuto = 0;
		
		for (EORepartService repartService : getDisplayGroup().allObjects()) {
			if (IndicateurValidationAuto.ETAT_IND_VALIDE.getCodeIndicateur().equals(repartService.indValidationAutoUeEc())) {
				if (repartService.toService().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
					controleurStat.validerFiche(repartService, personneConnecte().persId());
					nbFichesValidesAuto++;
				} else {
					controleurVac.validerFiche(repartService, personneConnecte().persId());
					nbFichesValidesAuto++;
				}
				controleurStat.recalculerIndicateurValidationAuto(repartService);
			}
		}
		edc().saveChanges();
		getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, String.format(Constante.VALIDATION_AUTO_FICHE_VALIDE, String.valueOf(nbFichesValidesAuto)));
		
		return doNothing();
	}
	
	
	public String getSrcImage() {
		return "images/avertissement16x16.png";
	}

	/**
	 * 
	 * @return true/false
	 */
	public boolean estCurrentItemNonValidable() {
		return !IndicateurValidationAuto.ETAT_IND_VALIDE.getCodeIndicateur().equals(getCurrentItem().indValidationAutoUeEc());

	}
	
	/**
	 * 
	 * @return titre du message d'avertissement
	 */
	public String getTitreValidationNonPossible() {
		return (IndicateurValidationAuto.getIndicateurValidation(getCurrentItem().indValidationAutoUeEc())).getMessageAvertissement();
	}

	public EOStructure getComposante() {
		if (getCurrentItem().toService().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			return getCurrentItem().toService().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}
		if (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_CS)) {
			return getCurrentItem().toStructure();
		} else if (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_DE)) {
			EOStructure parent = getCurrentItem().toStructure().toStructurePere();
			if (parent != null && basicPecheCtrl.isStructureDeTypeGroupe(parent, EOTypeGroupe.TGRP_CODE_CS)) {
				return parent;
			}
		}

		return null;
	}
	
	public EOStructure getDepartement() {
		if (getCurrentItem().toService().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			return getCurrentItem().toService().enseignant().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}
		if (basicPecheCtrl.isStructureDeTypeGroupe(getCurrentItem().toStructure(), EOTypeGroupe.TGRP_CODE_DE)) {
			return getCurrentItem().toStructure();
		} 
		return null;
	}

	
	public String statut() {
		if (getCurrentItem() != null 
				&& getCurrentItem().toService() != null
				&& getCurrentItem().toService().enseignant() != null) {
			return getCurrentItem().toService().enseignant().getStatut(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}

		return null;
	}

}