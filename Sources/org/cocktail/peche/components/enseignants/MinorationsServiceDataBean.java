package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgrh.common.metier.EODecharge;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOVueAbsences;

/**
 * Classe représentant un bean d'une minoration de service
 * 	=> soit une décharge soit une absence (modalité, minoration ou position)
 * 
 * @author Chama LAATIK
 */
public class MinorationsServiceDataBean {
	
	private EODecharge decharge;
	private EOVueAbsences absence;
	
	private String type;
	private String periode;
	private String libelle;
	private BigDecimal nbHeures;
	private String temoinHcomp;
	
	private EOService service;
	
	/**
	 * Constructeur pour une décharge.
	 * @param decharge : décharge
	 */
	public MinorationsServiceDataBean(EODecharge decharge) {
		this.decharge = decharge;
		this.absence = null;
		this.service = null;
		
		this.type = Constante.DECHARGE;
		this.libelle = this.decharge.typeDecharge().libelleCourt();
		this.periode = this.decharge.periodeDecharge();
		this.nbHeures = this.decharge.nbHDecharge();
		this.temoinHcomp = this.decharge.typeDecharge().temHComp();
	}
	
	/**
	 * Constructeur pour une absence (minoration de service ou modalité)
	 * @param absence : absence
	 * @param serviceEnseignant : service de l'enseignant
	 */
	public MinorationsServiceDataBean(EOVueAbsences absence, EOService serviceEnseignant) {
		this.absence = absence;
		this.decharge = null;
		this.service = serviceEnseignant;
		
		this.type = absence.type();
		this.libelle = this.absence.toTypeAbsence().lcTypeAbsence();
		this.periode = DateCtrl.formatDateShort(this.absence.dateDebut()) + " - " 
				+ DateCtrl.formatDateShort(this.absence.dateFin());
		this.nbHeures = nbHeuresAbsence();
		this.temoinHcomp = this.absence.toTypeAbsence().temHCOMP();
	}
	
	public boolean isAbsence() {
		return (this.absence != null);
	}
	
	/**
	 * @return le nombre d'heures de l'absence
	 */
	private BigDecimal nbHeuresAbsence() {
		if (isAbsence() && getAbsence().getRepartAbsence(getService()) != null) {
			return getAbsence().getNbHeuresAbsence(getService());
		}

		return null;
	}
	
	
	/* ========== Getters & Setters ========== */
	
	/**
	 * Met à jour le nombre d'heures
	 * @param nbHeures : nombre d'heures
	 */
	public void setNbHeures(BigDecimal nbHeures) {
		this.nbHeures = nbHeures;
	}
	
	public BigDecimal getNbHeures() {
		return nbHeures;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPeriode() {
		return periode;
	}

	public void setPeriode(String periode) {
		this.periode = periode;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public EODecharge getDecharge() {
		return decharge;
	}

	public void setDecharge(EODecharge decharge) {
		this.decharge = decharge;
	}

	public String getTemoinHcomp() {
		return temoinHcomp;
	}

	public void setTemoinHcomp(String temoinHcomp) {
		this.temoinHcomp = temoinHcomp;
	}

	public EOVueAbsences getAbsence() {
		return absence;
	}

	public void setAbsence(EOVueAbsences absence) {
		this.absence = absence;
	}

	public EOService getService() {
		return service;
	}

	public void setService(EOService service) {
		this.service = service;
	}

}
