package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public abstract class CktlPecheFicheEnseignantGeneriqueAbstract extends CktlPecheBaseComponent {
	
	
	private EOService editedObject = null;
	
	private static final String REH_SANS_COMPOSANTE = "Attention, toutes les répartitions REH ne sont pas liées à une composante.";
	
	private EnseignantsGeneriquesCtrl controleur;
	
	
	public CktlPecheFicheEnseignantGeneriqueAbstract(WOContext context) {
		super(context);
		controleur = new EnseignantsGeneriquesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	
	
	/**
	 * Renvoie le service de l'enseignant
	 *
	 * @return service
	 */
	public double getHeuresService() {
		return controleur.getHeuresService(getEditedObject());
	}

	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 * @return nombre d'heures
	 */
	public double getServiceAttribue() {
		return controleur.getServiceAttribue(getEditedObject());
	}
	
	/**
	 * renvoie le total des heures d'affectation que l'enseignant doit faire
	 * @return nombre d'heures
	 */
	public double getRehAttribue() {
		return controleur.getRehAttribue(getEditedObject());
	}
	
	/**
	 * renvoie la différence entre le nombre d'heures prévues et le nombre d'heures
	 * affectées à l'enseignant
	 * @return nombre d'heures
	 */
	public double getDelta() {
		return getHeuresService() - getServiceAttribue(); 
	}

	
	/**
	 * Définit l'objet Enseignant et récupère la fiche de service correspondante
	 * @param editedObject : enseignantgenerique
	 */
	public void setEditedObject(EOService editedObject) {
		this.editedObject = editedObject;
		verificationComposantesReh();
	}
	
	
	/**
	 * Si une répartition de type REH n'a pas de composante associée,
	 * on lève un message d'information
	 */
	private void verificationComposantesReh() {
		
		NSArray<EOServiceDetail> listeServiceDetails = getEditedObject().listeServiceDetails();
		for (EOServiceDetail serviceDetail : listeServiceDetails) {
			if (serviceDetail.reh() != null && serviceDetail.toStructure() == null) {
				getPecheSession().addSimpleMessage(TypeMessage.ATTENTION, REH_SANS_COMPOSANTE);
				return;
			}
		}

	}



	/**
	 * Enregistre le service d'un intervenant.
	 *
	 * @throws ValidationException
	 *             : exception au cours de l'enregistrement.
	 */
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();

		Session pecheSession = getPecheSession();
		EOPersonne personne = pecheSession.getApplicationUser().getPersonne();

		if (modeEdition == ModeEdition.CREATION) {
			getEditedObject().setPersonneCreation(personne);
			getEditedObject().setDateCreation(now);
		}

		if (modeEdition == ModeEdition.MODIFICATION) {
			getEditedObject().setPersonneModification(personne);
		}

		if (!StringCtrl.isEmpty(getEditedObject().commentaire())) {
			getEditedObject().setDateCommentaire(now);
		}

		getEditedObject().setTemoinEnseignantGenerique(Constante.OUI);
		getEditedObject().setTemoinValide(Constante.OUI);
		getEditedObject().setDateModification(now);
		getEditedObject().setAnnee(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
	}
	

	/**
	 * Enregistrer le commentaire.
	 */
	public void enregistrerCommentaireFiche() {
		try {
			PecheApplicationUser appUser = getPecheSession().getApplicationUser();
			EOPersonne personne = appUser.getPersonne();

			getEditedObject().majDonnesAuditModification(personne);
			edc().saveChanges();
			
			getPecheSession().addSimpleInfoMessage(message("ficheService.message.commentaire.sauvegarde.titre"), message("ficheService.message.commentaire.sauvegarde.detail"));
		} catch (ValidationException e) {
		    edc().invalidateAllObjects();
		    edc().revert();
		    throw e;
		}
	}
	


	public EOService getEditedObject() {
		return editedObject;
	}


}
