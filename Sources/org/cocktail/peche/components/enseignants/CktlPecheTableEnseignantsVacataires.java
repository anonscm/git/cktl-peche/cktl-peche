package org.cocktail.peche.components.enseignants;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe modélise la liste des enseignants vacataires.
 * @author Yannick Mauray
 * @author Chama Laatik
 * @author Etienne Ragonneau
 */
public class CktlPecheTableEnseignantsVacataires extends CktlPecheTableEnseignantsAbstract {

	private static final long serialVersionUID = 7086885264626772681L;

	private EnseignantsVacatairesCtrl controleur;

	private String typePopulation;
	private boolean mesValidations;
	
	private String tokenNom;
	private String tokenPrenom;
	
	private EOService serviceARapprocher;
	
	
	/**
	 * Constructeur.
	 * @param context le contexte.
	 */
	public CktlPecheTableEnseignantsVacataires(WOContext context) {
        super(context);
        this.controleur = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
 	}
	
	@Override
	public void awake() {
		super.awake();
		if (hasBinding("typePopulation")) {
			typePopulation = ((String) valueForBinding("typePopulation"));
		}
		if (hasBinding("mesValidations")) {
			mesValidations = valueForBooleanBinding("mesValidations", false);
		}
		if (hasBinding("serviceARapprocher")) {
			setServiceARapprocher((EOService) valueForBinding("serviceARapprocher"));
		}
	}
	
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		getEnseignants();
		super.appendToResponse(response, context);
	}
	
	/**
	 * Renvoie la liste des enseignants vacataires en fonction du profil connecté
	 */
	public void getEnseignants() {
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOActuelEnseignant.ENTITY_NAME);
		
		NSArray<EtapePeche> listeEtape = null;
		
		if (isMesValidations()) {
			listeEtape = determinerListeEtapesFiltre();			  
		}
		
		
		EOQualifier qualifier = controleur.getListeEnseignants(getPopulationFiche(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire(), listeEtape);
		ERXFetchSpecification<EOActuelEnseignant> fetchSpec = EOActuelEnseignant.fetchSpec().qualify(qualifier).sort(getTriParDefaut());
		
		dataSource.setFetchSpecification(fetchSpec);
		fetchSpec.setUsesDistinct(true);
		ERXDisplayGroup<EOActuelEnseignant> dg = this.getDisplayGroup();		
		dg.setDataSource(dataSource);		
		dg.fetch();
		
		NSArray<EOActuelEnseignant> listeEns = filtrerDoublons(dg.allObjects());
		
		for (EOActuelEnseignant enseignant : listeEns) {
			enseignant.setEtatFiche(etatFiche(enseignant));
		}
		
		dg.setObjectArray(listeEns);	
	}
	
	private NSArray<EOActuelEnseignant> filtrerDoublons(NSArray<EOActuelEnseignant> liste) {

		NSMutableArray<Integer> entrees = new NSMutableArray<Integer>();
		NSMutableArray<EOActuelEnseignant> sortie = new NSMutableArray<EOActuelEnseignant>();
		EOActuelEnseignant actuelEnseignant;

		for (int i = 0; i < liste.count(); i++) {
			actuelEnseignant = (EOActuelEnseignant) liste.get(i);
			if (!entrees.contains(actuelEnseignant.toIndividu().persId())) {
				sortie.add(actuelEnseignant);
				entrees.add(actuelEnseignant.toIndividu().persId());
			}
		}

		return sortie.immutableClone();
	}

	/**
     * Retourne la fiche de service de l'enseignant.
     * @return la page.
     */
	 public WOActionResults getLinkToFicheEnseignant() {
		
		CktlPecheFicheServiceEnseignantVacataire page = (CktlPecheFicheServiceEnseignantVacataire) 
					pageWithName(CktlPecheFicheServiceEnseignantVacataire.class.getName());
			page.setEditedObject(getCurrentItem());
			this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
			return page;
 }
	
	
	/**
	 * @return Le tri par défaut de la liste
	 */
	private ERXSortOrderings getTriParDefaut() {
		return EOActuelEnseignant.getTriParDefaut();
	}

	/**
	 * renvoie le libellé du type du vacation
	 * @return type contrat travail
	 */
	public String getTypeVacation() {
		String libelle = controleur.getLibelleTypeAssocieVacation(getCurrentItem());
		if (!StringCtrl.isEmpty(libelle)) {
			return libelle;
		}
		return controleur.getLibelleTypeVacation(getCurrentItem());
	}

	/**
	 * renvoie le nombre d'heures du contrat d'un enseignant
	 * @return heures du contrat
	 */
	public double getHeuresContrat() {
		double service = controleur.getTotalNbHeuresVacation(getCurrentItem());		
		return service;
	}
	
	/**
	 * renvoie le total des heures d'affectation que l'intervenant doit faire
	 * @return nombre d'heures
	 */
	public double getHeuresAttribuees() {
		double service = controleur.getServiceAttribue(getCurrentItem());
		return service;
	}
	
	/**
	 * renvoie le total des heures d'affectation que l'intervenant doit faire
	 * @return nombre d'heures
	 */
	public double getHeuresRealisees() {
		double service = controleur.getServiceRealise(getCurrentItem());
		return service;
	}
	
	/**
	 * renvoie la différence entre le nombre d'heures prévues sur le contrat et le nombre d'heures
	 * réalisées par l'enseignant
	 * @return nombre d'heures
	 */
	public double getDeltaRealise() {
		return getHeuresContrat() - getHeuresRealisees();
	}
	
	/**
	 * renvoie la différence entre le nombre d'heures prévues sur le contrat et le nombre d'heures
	 * affectées à l'enseignant
	 * @return nombre d'heures
	 */
	public double getDeltaAttribue() {
		return getHeuresContrat() - getHeuresAttribuees();
	}
	
	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ouvrirFicheService() {
		if (getSelectedObject() != null) {
			
			/*if (controleur.getComposante(getSelectedObject()) == null) {
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "La composante d'affectation n'est pas renseignée");
				return doNothing();
			}*/
			
			CktlPecheFicheServiceEnseignantVacataire form = (CktlPecheFicheServiceEnseignantVacataire)
					pageWithName(CktlPecheFicheServiceEnseignantVacataire.class.getName());
			form.setEditedObject(getSelectedObject());
			this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
			return form;
		} else {
			return null;
		}
	}

	
	/**
	 * Permet de savoir si l'enseignant vacataire peur etre rapprocher avec un enseignant générique 
	 * en fonction du status de sa fiche (prévisionelle ou non).
	 * @return true si l'ensegnant est sur le circuit prévisionel.
	 */
	public boolean isRapprochable() {
		if (this.getSelectedObject() == null) {
			return false;
		} else if (this.getSelectedObject().listeServices().size() == 0 
				|| controleur.estToutesDemandesPrevisionnelles(this.getSelectedObject().getService(controleur.getAnneeUniversitaire()))) {
			return true;
		}			 
		return false;
	}
	


	public WOActionResults rapprocherEnseignantGenerique() {
		if (getSelectedObject() != null) {	
			if (getSelectedObject().rapprocherEnseignant(
					getServiceARapprocher(),
					controleur, 
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession()
					,false)) {
				CktlPecheEnseignantsGeneriques page = (CktlPecheEnseignantsGeneriques) pageWithName(CktlPecheEnseignantsGeneriques.class.getName());
				page.setSelectedItemId(PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId());
				page.setTypeStatutaire(false);
				return page;
			}
		}

		return doNothing();
	}

	public boolean isBoutonRapprocherDisabled() {
		return !getAutorisation().hasDroitUtilisationParametrageEnseignantsGeneriques() || !isRapprochable();
	}
	
	
	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter.
	 *
	 * @return null.
	 */
	public WOActionResults filtrer() {
		EOQualifier qualifierNom = null;
		EOQualifier qualifierPrenom = null;
							
		if (!StringCtrl.isEmpty(tokenNom)) {
			qualifierNom = EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(tokenNom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.NON));
		}		
	
		if (!StringCtrl.isEmpty(tokenPrenom)) {
			qualifierPrenom = EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).contains(tokenPrenom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.NON));

		} 
	
		filtrerDisplayGroup(ERXQ.and(qualifierNom, qualifierPrenom));
		
		return doNothing();
	}
	
	/**
	 * Filtrage de la liste sur le nom.
	 *
	 * @return null.
	 */
	public WOActionResults filtrerNom() {
		EOQualifier qualifier = EOActuelEnseignant.TO_INDIVIDU
				.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(tokenNom)
				.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.NON));

		if (!StringCtrl.isEmpty(tokenNom)) {
			filtrerDisplayGroup(qualifier);
		} else {
			filtrerDisplayGroup(null);
		}

		tokenPrenom = "";
		return doNothing();
	}

	/**
	 * Filtrage de la liste sur le prenom.
	 *
	 * @return null.
	*/
	public WOActionResults filtrerPrenom() {
		EOQualifier qualifier = EOActuelEnseignant.TO_INDIVIDU
				.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).contains(tokenPrenom)
				.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.NON));

		if (!StringCtrl.isEmpty(tokenPrenom)) {
			filtrerDisplayGroup(qualifier);
		} else {
			filtrerDisplayGroup(null);
		}

		tokenNom = "";
		return doNothing();
	}
	
	/**
	 * 
	 * @param enseignant enseignant en cours
	 * @return état de la fiche de service
	 */
	public String etatFiche(EOActuelEnseignant enseignant) {
		String etatFiche = "";
		EOService service = null;
		
		service = enseignant.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		if (service == null || (service != null && service.toListeRepartService().isEmpty())) {
			etatFiche = Constante.ETAT_NON_INITIALISE;
		} else {
			
			IStructure structure = getPecheSession().getPecheAutorisationCache().getStructurePrincipaleVacataire();
			
			NSArray<EORepartService> listeRepartService = service.toListeRepartService();
			
			String etatFicheTemp;
			boolean tousRepartServiceValide = true;
			
			for (EORepartService repartService : listeRepartService) {
				etatFicheTemp = EtapePeche.getEtape(controleur.getDemande(repartService).toEtape()).getEtatDemande();
				if ((structure != null) && structure.equals(repartService.toStructure())) {
					etatFiche = etatFicheTemp;
				}
				if (!EtapePeche.VALIDEE.getEtatDemande().equals(etatFicheTemp)) {
					tousRepartServiceValide = false;
				}
			} 
			if (StringUtils.isEmpty(etatFiche)) {
				if (tousRepartServiceValide) {
					etatFiche = EtapePeche.VALIDEE.getEtatDemande();
				} else {
					etatFiche = Constante.ETAT_EN_VALIDATION;
				}
			}
		}
		enseignant.setEtatFiche(etatFiche);
		
		return etatFiche;
	}
	
	public String etatFiche() {
		return etatFiche(getCurrentItem());
	}
	
	/**
	 * 
	 * @return true si le lien vers la fiche de service de l'enseignant doit être affiché
	 */
	public boolean afficherLienToFicheEnseignant() {
		return getFonctionRepartiteur() || getFonctionDrh() || getFonctionPresident() || getFonctionDirecteur();
	}

	// ========== Getters & Setters ==========

	public String getTokenNom() {
		return tokenNom;
	}

	public void setTokenNom(String tokenNom) {
		this.tokenNom = tokenNom;
	}

	public String getTokenPrenom() {
		return this.tokenPrenom;
	}

	public void setTokenPrenom(String tokenPrenom) {
		this.tokenPrenom = tokenPrenom;
	}
	
	/**
	 * 
	 * @return true/false
	 */
	public boolean estPopulationPrevisionnelle() {
		return Constante.PREVISIONNEL.equals(getPopulationFiche());
	}
	
	public boolean isBoutonRapprocherVisible() {
		return estPopulationPrevisionnelle() && getAutorisation().hasDroitShowParametrageEnseignantsGeneriques() && isServiceARapprocher();
	}
	
	/**
	 * 
	 * @return true si le bouton fiche de voeux doit être désactivé
	 */
	public boolean boutonFicheServiceInactif() {
		return (getSelectedObject() == null);
	}

	/**
	 * 
	 * @return true si le bouton fiche de service peut être présent
	 */
	public boolean afficherBoutonFicheService() {
		return afficherLienToFicheEnseignant();
	}


	public String getPopulationFiche() {
		return typePopulation;
	}


	public void setPopulationFiche(String typePopulation) {
		this.typePopulation = typePopulation;
	}

	public boolean isMesValidations() {
		return mesValidations;
	}

	public void setMesValidations(boolean mesValidations) {
		this.mesValidations = mesValidations;
	}

	public EOService getServiceARapprocher() {
		return serviceARapprocher;
	}

	public void setServiceARapprocher(EOService serviceARapprocher) {
		this.serviceARapprocher = serviceARapprocher;
	}
	
	public boolean isServiceARapprocher() {
		return (getServiceARapprocher() != null);
	}	
	
	public String getApp3DecimalesNumberFormat() {
		return OutilsValidation.getApp3DecimalesNumberFormat();	
	}
	
}