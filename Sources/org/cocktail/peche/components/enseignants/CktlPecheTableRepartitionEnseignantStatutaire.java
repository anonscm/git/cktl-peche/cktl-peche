package org.cocktail.peche.components.enseignants;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Cette classe permet de récupérer la liste des répartitions d'un enseignant séparemment (REH et Service).
 *
 * @author Yannick Mauray
 * @author Chama LAATIK
 */
public abstract class CktlPecheTableRepartitionEnseignantStatutaire extends CktlPecheTableRepartitionEnseignant {

	private static final long serialVersionUID = -6430800101350804617L;
	private EOActuelEnseignant enseignant;
	private EORepartService repartService;
	
	private EnseignantsStatutairesCtrl controleur = null;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionEnseignantStatutaire(WOContext context) {
		super(context);
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(),getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()); 
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOServiceDetail.ENTITY_NAME);
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
	}

	@Override
	public void awake() {
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
        enseignant = (EOActuelEnseignant) valueForBinding("enseignant");
        repartService = (EORepartService) valueForBinding("repartService");
        
        String path = ERXQ.keyPath(EOServiceDetail.TO_REPART_SERVICE_KEY);
        EOQualifier qualifier =
        		ERXQ.and(
						ERXQ.equals(path, repartService),
						ERXQ.isNotNull(getTableKey())
				);
        ((ERXDatabaseDataSource) dg.dataSource()).setAuxiliaryQualifier(qualifier);
        
		dg.fetch();
		
	}

	protected abstract String getTableKey();

	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}

	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}
	
	public boolean getBoutonsModifsActifs() {
		return getBoutonsActifs() || getSelectedObject() == null;
	}
	
	/**
	 * 
	 * @return sous total d'heures prévues
	 */
	public Double getSousTotalHeuresPrevues() {
		return controleur.getTotalHeuresPrevues(getDisplayGroup().allObjects());
	}
	
	/**
	 * 
	 * @return sous total d'heures réalisées
	 */
	public Double getSousTotalHeuresRealisees() {
		return controleur.getTotalHeuresRealisees(getDisplayGroup().allObjects());
	}

	public EnseignantsStatutairesCtrl getControleur() {
		return controleur;
	}

	public EORepartService getRepartService() {
		return repartService;
	}
	
	
}
