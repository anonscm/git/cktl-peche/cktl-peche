package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.entity.EORepartService;

import com.webobjects.appserver.WOContext;

/**
 * Classe permettant de voir un récapitulatif de la fiche de service de
 * l'enseignant genetique.
 *
 * @author Etienne RAGONNEAU
 *
 */
public class CktlPecheFicheServiceEnseignantGeneriqueStatutaire extends CktlPecheFicheEnseignantGeneriqueAbstract {
	

	private static final long serialVersionUID = -3189892851946513618L;
	
	
	private String selectedItemId;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public CktlPecheFicheServiceEnseignantGeneriqueStatutaire(WOContext context) {
        super(context);
        
    }
    	

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}
	
	public EORepartService getRepartService() {
		return getEditedObject().toListeRepartService().get(0);
	}
	
	
}