package org.cocktail.peche.components.enseignants.ficheVoeux;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;

/**
 * Classe permettant d'afficher les enseignants affectés sur un AP
 * 
 * @author Julien Callewaert
 */
public class ListeRepartitionParAP extends CktlPecheTableComponent<EOFicheVoeuxDetail> {
	
	private static final long serialVersionUID = -979788725638516362L;
	
	private EOAP ap;
	private NSArray<EOAP> listeAP;
	
	private UECtrl controleur;
	private ComposantCtrl controleurComposant;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'accès à la base de données.
	 */
    public ListeRepartitionParAP(WOContext context) {
        super(context);
        this.controleur = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        this.controleurComposant = new ComposantCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOFicheVoeuxDetail.ENTITY_NAME);
		ERXDisplayGroup<EOFicheVoeuxDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
    }
    
    /**
     * @return nom de l'enseignant
     */
	public String getEnseignantNom() {
		String nom = null;

		if (getCurrentItem().ficheVoeux().enseignant() != null) {
			nom = getCurrentItem().ficheVoeux().enseignant().toIndividu().nomAffichage();
		} 
		
		return nom.toUpperCase();
	}
	
	
	
	
	/**
     * @return prenom de l'enseignant
     */
	public String getEnseignantPrenom() {
		String prenom = null;
		if (getCurrentItem().ficheVoeux().enseignant() != null) {
			prenom = getCurrentItem().ficheVoeux().enseignant().toIndividu().prenomAffichage();
		}
		return prenom;
	}
	
	/**
	 * @return composante de l'enseignant
	 */
	public String getEnseignantComposante() {
		String composante = null;
		if ((getCurrentItem().ficheVoeux().enseignant() != null) 
				&& (getCurrentItem().ficheVoeux().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()) != null)) {
			composante = getCurrentItem().ficheVoeux().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()).lcStructure();
		} 
		return composante;
	}
	
	/**
	 * @return corps de l'enseignant
	 */
	public EOCorps getEnseignantCorps() {
		EOCorps corps = null;
		if (getCurrentItem().ficheVoeux().enseignant() != null) {
			corps = getCurrentItem().ficheVoeux().enseignant().getCorps(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		}		
		return corps;
	}
	
	/**
	 * @return statut de l'enseignant : statutaire
	 */
	public String getEnseignantStatut() {
		String statut = null;
		if (getCurrentItem().ficheVoeux().enseignant() != null) {
			statut = getCurrentItem().ficheVoeux().enseignant().getStatut(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		} 
		return statut;
	}

	/**
	  * 
	  * @return parent de l'AP
	  */
	 public IComposant getParentAP() {
		 EOAP composantAP = getAp();
		 return this.controleurComposant.getParentAP(composantAP);
	 }
	 
	 public boolean isAPNonNull() {
		 return getAp() != null;
	 }
	
	
	/**
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			FicheVoeuxDetailForm page =  (FicheVoeuxDetailForm) pageWithName(FicheVoeuxDetailForm.class.getName());
			page.setModeArbitrage(Boolean.TRUE);
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setFicheVoeux(getSelectedObject().ficheVoeux());
			page.setEditedObject(getSelectedObject());
			page.setListeAP(listeAP);
			
			return page;
		} else {
			return null;
		}
	}
	
	/**
	 * Supprime l'élement sélectionné
	 * @return null
	 * @throws Exception imposé par EOComposant.supprimer().
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}

		return doNothing();
	}
	
	/**
	 * @return Message avant suppression
	 */
	@Override
	public String onClickBefore() {
		String msgConfirm = message(Messages.SERVICE_ALERTE_CONFIRM_SUPPRESS);
		return "confirm('" + msgConfirm + "')";
	}
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}
	
	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String tableRepartitionId() {
		return getComponentId() + "_tableRepartition";
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		ap = (EOAP) valueForBinding("ap");
		
		if (getAp() != null) {
		
			EOQualifier qualifier = controleur.getQualifierPourFicheVoeuxAArbitrer(getAp(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			this.getDisplayGroup().setSortOrderings(EOFicheVoeuxDetail.getTriParDefaut());
			((ERXDatabaseDataSource) this.getDisplayGroup().dataSource()).setAuxiliaryQualifier(qualifier);
			this.getDisplayGroup().fetch();
		
		}
		
		super.appendToResponse(response, context);
	}
	
	/**
	 * 
	 * @return renvoie true si les boutons doivent être inactifs
	 */
	public boolean boutonsInactifs() {
		
		boolean estRepartiteurDansMemeComposante = false;
		
		if (getSelectedObject() != null) {
			EOStructure structureEnseignant = getSelectedObject().ficheVoeux().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), 
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			List<IStructure> listeStructuresRepartiteur = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
			if (listeStructuresRepartiteur != null && structureEnseignant != null && listeStructuresRepartiteur.contains(structureEnseignant)) {
				estRepartiteurDansMemeComposante = true;
			}
			
		}
		return !getFonctionRepartiteur() || (getSelectedObject() == null) || !estRepartiteurDansMemeComposante; 
	}
	

	public EOAP getAp() {
		return ap;
	}

	public void setAp(EOAP ap) {
		this.ap = ap;
	}

	public NSArray<EOAP> getListeAP() {
		return listeAP;
	}

	public void setListeAP(NSArray<EOAP> listeAP) {
		this.listeAP = listeAP;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void awake() {
		listeAP = (NSArray<EOAP>) valueForBinding("listeAP");
		super.awake();
	}
	
}