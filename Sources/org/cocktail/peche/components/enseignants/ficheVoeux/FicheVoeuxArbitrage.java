package org.cocktail.peche.components.enseignants.ficheVoeux;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.peche.Application;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.IServiceConstructionArbitrageAP;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;

/**
 * 
 * @author juliencallewaert
 *
 */
public class FicheVoeuxArbitrage extends CktlPecheTableComponent<EOAP> {

	private static final long serialVersionUID = -4616587114697743891L;

	private UECtrl controleurUE;
	
	private String menuSelectionne;
	
	private NSArray<EOAP> listeAP;
	
	private ERXDisplayGroup<EOAP> displayGroup;
	
	private ComposantCtrl controleurComposant;
	
	
	@Inject
	private IServiceConstructionArbitrageAP serviceConstructionArbitrageAP;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public FicheVoeuxArbitrage(WOContext context) {
        super(context);
        this.controleurUE = new UECtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        this.controleurComposant = new ComposantCtrl(edc(),getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        serviceConstructionArbitrageAP = Application.application().injector().getInstance(IServiceConstructionArbitrageAP.class);
        getListeAP();
    }

	/**
	 * Renvoie le nombre de groupe de l'AP s'il est connu
	 * @return nombre de groupe
	 */
	 public String getNbGroupeAP() {
    	if (getCurrentItem() != null) {
    		EOAP composantAP = (EOAP) getCurrentItem();
    		
    		IChargeEnseignement chargeEnseignement = controleurUE.getChargeEnseignement(composantAP);
    		
	    	if (chargeEnseignement != null && chargeEnseignement.nbGroupes() != null) {
	    		return chargeEnseignement.nbGroupes().toString();
	    	}
    	}

    	return Constante.NON_DEFINI;
    }
	 
	 /**
	  * 
	  * @return parent de l'AP
	  */
	 public IComposant getParentAP() {
		 EOAP composantAP = (EOAP) getCurrentItem();
		 return this.controleurComposant.getParentAP(composantAP);
	 }
	 

	/**
	 * Renvoie le code du type de l'AP
	 * 
	 * @return code
	 */
	public String getChildItemTypeAP() {
		EOAP composantAP = (EOAP) getCurrentItem();
		return composantAP.typeAP().code();
	}

	/**
	 * Renvoie le nombre d'heures de la maquette de l'AP
	 * 
	 * @return nombre d'heures
	 */
	public Float getChildItemNbHeuresAP() {
		EOAP composantAP = (EOAP) getCurrentItem();

		IChargeEnseignement chargeEnseignement = controleurUE.getChargeEnseignement(composantAP);
		
		if (chargeEnseignement != null && chargeEnseignement.valeurHeures() != null) {
			return chargeEnseignement.valeurHeures().floatValue();
		}

		return 0.0f;
	}
	
	public float getNbHeuresRepartition() {
		return controleurUE.getTotalHeuresPrevues((EOAP) getCurrentItem(), false);
	}
	
	public float getNbHeuresSouhaitees() {
		return controleurUE.getTotalHeuresSouhaitees((EOAP) getCurrentItem(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	/**
	 * @return les heures qui sont en trop par AP
	 */
	public float getNbHeuresDepassement() {
		return controleurUE.getTotalHeuresEnDepassement((EOAP) getCurrentItem(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	/**
	 * @return total du nombre d'heures maquette
	 * 	==> On ne prend pas en compte le nombre de parent pour les AP mutualisés
	 */
	public float getTotalNbHeuresMaquette() {
		return controleurUE.getHeuresMaquette((EOAP) getCurrentItem(), false);
	}
	
	/**
	 * Renvoie les AP 
	 * @return display Group
	 */
	public ERXDisplayGroup<EOAP> getDisplayGroup() {
		displayGroup = new ERXDisplayGroup<EOAP>();
		EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOAP.class), edc());
		dataSource.setArray(new NSArray<EOAP>(getListeAP()));
		displayGroup.setDataSource(dataSource);
		displayGroup.setDelegate(this);
		displayGroup.setSelectsFirstObjectAfterFetch(false);
		if (getSelectedObject() != null) {
			displayGroup.setSelectedObjects(new NSArray<EOAP>(getSelectedObject()));
		}
		displayGroup.fetch();
		return displayGroup;
	}
	
	
	/**
	 * @return page précédente
	 */
	public WOActionResults annuler() {
		WOComponent pageRetour = (WOComponent) this.session().objectForKey("retourOffreDeFormation");
		if (pageRetour != null) {
			return pageRetour;
		}
		return null;
	}
	
	/**
	 * 
	 * @return nom du menu
	 */
	public String getMenuSelectionne() {
		if (menuSelectionne == null) {
			setMenuSelectionne(PecheMenuItem.FICHE_VOEUX_ARBITRAGE.getId());
		}
		return menuSelectionne;
	}

	public void setMenuSelectionne(String menuSelectionne) {
		this.menuSelectionne = menuSelectionne;
	} 

	/**
	 * 
	 * @return listeEOAP
	 */
	public NSArray<EOAP> getListeAP() {
		
		if (listeAP == null) {
			listeAP = getServiceConstructionArbitrageAP().getAPHeuresEnDepassement(edc(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementStatutaires(), 
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), controleurUE);
		}
		
		return listeAP;
	}

	public void setListeAP(NSArray<EOAP> listeAP) {
		this.listeAP = listeAP;
	} 
	
	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String tableRepartitionId() {
		return getComponentId() + "_tableRepartition";
	}

	public IServiceConstructionArbitrageAP getServiceConstructionArbitrageAP() {
		return serviceConstructionArbitrageAP;
	}
	
	/**
	 * On retourne l'AP sélectionné s'il fait bien partie de la liste des AP de l'écran
	 * Cette liste d'AP est recalculée à chaque affichage, mais le SelectedObject peut être conservé lorsque l'on procède à un arbitrage
	 * Il peut donc y avoir à l'écran une répartition d'AP qui n'est plus affichée dans la liste des AP.
	 * Ce que l'on évite par cette méthode
	 * @return AP Sélectionnée
	 */
	public EOAP getSelectedObjectAP() {
		if (getSelectedObject() != null && listeAP.contains(getSelectedObject())) {
			return getSelectedObject();
		}
		return null;
	}
	
	/**
	 * 
	 * @return true si listeAP Vide
	 */
	public boolean estListeAPNonVide() {
		return !this.listeAP.isEmpty();
	}
	
	/**
	 * 
	 * @return le nombre d'heures en dépassement est-il positif ?
	 */
	public boolean nbHeuresEnDepassementPositif() {
		return controleurUE.getTotalHeuresEnDepassement((EOAP) getCurrentItem(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()) > 0.0f;
	}
	
}