package org.cocktail.peche.components.enseignants.ficheVoeux;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.IServiceConstructionArbitrageAP;
import org.cocktail.peche.components.enseignements.IRetourUeSelectionnee;
import org.cocktail.peche.components.enseignements.OffreFormationVoeux;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant d'ajouter ou modifier les affectations souhaitées de
 * l'enseignant sur sa fiche de voeux.
 *
 * @author Chama LAATIK
 */
public class FicheVoeuxDetailForm extends PecheCreationForm<EOFicheVoeuxDetail> {

	private static final long serialVersionUID = 2431052609190012883L;
	
	private UECtrl ueControleur;
	private ComposantCtrl controleurComposant;

	private EOFicheVoeux ficheVoeux;

	private NSArray<EOAP> listeComposantAP = null;
	private EOAP currentComposantAP;
	private String libelleEnseignement;
	private NSArray<EOFicheVoeuxDetail> listeVoeux;
	private EOFicheVoeuxDetail currentFicheVoeuxDetail;
	private NSArray<EOAP> listeAp;
	private EOAP currentAp;
	
	private boolean modeArbitrage;
	private NSArray<EOAP> listeAP;
	
	@Inject
	private IServiceConstructionArbitrageAP serviceConstructionArbitrageAP; 

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'exécution.
	 */
	public FicheVoeuxDetailForm(WOContext context) {
		super(context);
		ueControleur = new UECtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		controleurComposant = new ComposantCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();

		// En création
		if (modeEdition == ModeEdition.CREATION) {
			controlerSaisie();
			
			// En création avec un enseignement connu
			if (getLibelleEnseignement() != null) {
				for (EOFicheVoeuxDetail detail : getListeVoeux()) {
					if (detail.heuresSouhaitees() != null) {
						controleHeuresMaquette(detail);
						detail.setFicheVoeux(getFicheVoeux());
						detail.setCommentaire(editedObject().commentaire());
						detail.majDonneesAuditCreation(now, personne);						
					} else {
						// Si pas d'heures saisis, on n'enregistre pas cette ligne
						detail.editingContext().deleteObject(detail);
					}
				}
				getFicheVoeux().majDonnees(edc(), now, personne, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				editedObject().editingContext().deleteObject(editedObject());
			} else {
				// En création avec un enseignement non connu
				editedObject().setFicheVoeux(getFicheVoeux());
				// Passage en majuscule des champs saisi
				editedObject().setEtablissementExterne(editedObject().etablissementExterne().toUpperCase());
				if (editedObject().enseignementExterne() != null) {
					editedObject().setEnseignementExterne(editedObject().enseignementExterne().toUpperCase());
				}				
				editedObject().majDonneesAuditCreation(now, personne);
				getFicheVoeux().majDonnees(edc(), now, personne, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
			}
		} else {
			// Modification
			if (editedObject().composantAP() != null) {
				controleHeuresMaquette(editedObject());
			}
			editedObject().majDonneesAuditModification(now, personne);
			getFicheVoeux().majDonnees(edc(), now, personne, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	
		}
	}

	/**
	 * Contrôler la saisie utilisateur.
	 * <p>
	 * Lève une exception si un contrôle est ko.
	 */
	private void controlerSaisie() throws ValidationException {
		// L'enseignement est obligatoire (soit connu soit non connu)
		if (getLibelleEnseignement() == null && StringCtrl.isEmpty(editedObject().etablissementExterne())) {
			throw new ValidationException(Constante.VOEUX_RENSEIGNER_AU_MOINS_ENSEIGNEMENT_OU_ETAB_EXTERNE);
		} else if (getLibelleEnseignement() != null && (editedObject().etablissementExterne() != null || editedObject().enseignementExterne() != null)) {
			throw new ValidationException(Constante.VOEUX_RENSEIGNER_UN_ENSEIGNEMENT_OU_UN_ETAB_EXTERNE);
		}
	}
	
	/**
	 * Contrôle que les heuresSouhaitees <= heuresMaquettes
	 * Lève une exception si le contrôle est ko.
	 */
	private void controleHeuresMaquette(EOFicheVoeuxDetail voeuxDetail) throws ValidationException {
		if (voeuxDetail.heuresSouhaitees() > ueControleur.getHeuresMaquette(voeuxDetail.composantAP(), false)) {
			String message = String.format(Constante.VOEUX_SUPERIEUR_HEURES_MAQUETTE_PARAMETRE, 
					voeuxDetail.composantAP().typeAP().code());
			
			throw new ValidationException(message);
		}
	}
	
	/**
	 * En mode modification, renvoit booléen pour savoir si un AP est édité ou non
	 * @return vrai si AP non null
	 */
	public boolean modeComposantAP() {
		return this.editedObject().composantAP() != null;
	}

	/**
	 * Méthode pour l'affichage de l'AP et de son parent direct suivant
	 * le mode d'édition
	 * @return affichage
	 */
	public String affichageAPEtParent() {
		if ((getModeEdition() == ModeEdition.MODIFICATION)) {
			return controleurComposant.affichageAPEtParent(this.editedObject().composantAP());
		} else {
			return controleurComposant.affichageAPEtParent(getCurrentComposantAP());
		}
	}

	@Override
	protected EOFicheVoeuxDetail creerNouvelObjet() {
		EOFicheVoeuxDetail ficheVoeuxDetail = EOFicheVoeuxDetail.creerEtInitialiser(edc());
		return ficheVoeuxDetail;
	}

	@Override
	protected WOActionResults pageGestion() {
		if (modeArbitrage) {
			FicheVoeuxArbitrage page = (FicheVoeuxArbitrage) pageWithName(FicheVoeuxArbitrage.class.getName());
			page.setSelectedObject(this.editedObject().composantAP());
			page.setListeAP(getListeAP());
			return page;
		} else {
			FicheVoeuxEnseignant page = (FicheVoeuxEnseignant) pageWithName(FicheVoeuxEnseignant.class.getName());
			page.setEditedObject(getEnseignant());
			return page;
		}
	}

	/**
	 * Appel de la page "Offre de formation pour sélectionner un enseignement.
	 * 
	 * @return La page à appeler
	 */
	
	public WOActionResults choisirEnseignement() {
		OffreFormationVoeux page = (OffreFormationVoeux) pageWithName(OffreFormationVoeux.class.getName());
		page.setSelectedMenuItem(PecheMenuItem.FICHE_VOEUX);
		page.setTitreEtendu("Sélectionnez un enseignement");
		page.setBoutonDetailAffiche(false);
		page.setBoutonsSelectionAffiches(true, this, new IRetourUeSelectionnee() {
			public void setUeSelectionnee(String libelleUe, List<EOAP> listeAps) {
				setLibelleEnseignement(libelleUe);
				
				NSArray<EOFicheVoeuxDetail> listeVoeuxDetail = new NSMutableArray<EOFicheVoeuxDetail>();
				
				for (EOAP ap : listeAps) {
					EOFicheVoeuxDetail ficheVoeuxDetail = EOFicheVoeuxDetail.creerEtInitialiser(edc());
					ficheVoeuxDetail.setComposantAPRelationship(ap);
					listeVoeuxDetail.add(ficheVoeuxDetail);
				}
				
				setListeVoeux(listeVoeuxDetail);
				setListeAp(new NSArray<EOAP>(listeAps));
			}
		});
		page.setAffichageConcis(true);
		page.setLireEnseignementsDiplomants(true);
		page.setLireEnseignementsNonDiplomantsDansEtablissement(true);
		page.setLireEnseignementsNonDiplomantsHorsEtablissement(true);
		page.setColonneRepartitionAffichee(false);
		
		return page;
	}
	
	/**
	 * Effacer l'enseignement sélectionné.
	 * 
	 * @return <code>null</code>, on reste sur la même page
	 */
	public WOActionResults effacerEnseignement() {
		setLibelleEnseignement(null);
		
		if (getListeVoeux() != null) {
			getListeVoeux().clear();
		}
		
		return null;
	}
	
	
	
	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		// On ne tiens pas compte des messages standards WO
	}
	
	/**
	 * Retourne le nombre d'heures maquette pour l'AP en cours.
	 * <p>
	 * Si on boucle sur un détail fiche vœux, le nombre d'heure de ce détail (cas de la création),
	 * sinon le nombre de d'heure de l'AP en cours (cas de la modification).
	 * 
	 * @return Un nombre d'heures
	 */
	public float getHeuresMaquette() {
		if (currentFicheVoeuxDetail != null) {
			return ueControleur.getHeuresMaquette(currentFicheVoeuxDetail.composantAP(), false);
		}
		
		return ueControleur.getHeuresMaquette(editedObject().composantAP(), false);
	}

	/* ========== Getters & Setters ========== */

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.VOEUX_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.VOEUX_FORM_TITRE_MODIFIER;
	}

	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauVoeuxContainerId() {
		return getComponentId() + "_nouveauVoeuxContainerId";
	}

	public EOFicheVoeux getFicheVoeux() {
		return ficheVoeux;
	}

	public void setFicheVoeux(EOFicheVoeux ficheVoeux) {
		this.ficheVoeux = ficheVoeux;
	}

	public EOActuelEnseignant getEnseignant() {
		return ficheVoeux.enseignant();
	}

	/**
	 * Initialisation de la liste des AP
	 * @return liste AP
	 */
	public NSArray<EOAP> getListeComposantAP() {
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(
				EOAP.LIBELLE_KEY, EOSortOrdering.CompareAscending));

		this.listeComposantAP = EOAP.fetchAllSco_APs(edc(), sortOrderings);
		return this.listeComposantAP;
	}

	public void setListeComposantAP(NSArray<EOAP> listeComposantAP) {
		this.listeComposantAP = listeComposantAP;
	}

	public EOAP getCurrentComposantAP() {
		return currentComposantAP;
	}

	public void setCurrentComposantAP(EOAP currentComposantAP) {
		this.currentComposantAP = currentComposantAP;
	}
	
	public NSArray<EOAP> getListeAp() {
		return listeAp;
	}

	public void setListeAp(NSArray<EOAP> listeAp) {
		this.listeAp = listeAp;
	}

	public EOAP getCurrentAp() {
		return currentAp;
	}

	public void setCurrentAp(EOAP currentAp) {
		this.currentAp = currentAp;
	}

	public String getLibelleEnseignement() {
		return libelleEnseignement;
	}

	public void setLibelleEnseignement(String libelleEnseignement) {
		this.libelleEnseignement = libelleEnseignement;
	}
	
	public NSArray<EOFicheVoeuxDetail> getListeVoeux() {
		return listeVoeux;
	}

	public void setListeVoeux(NSArray<EOFicheVoeuxDetail> listeVoeux) {
		this.listeVoeux = listeVoeux;
	}

	public EOFicheVoeuxDetail getCurrentFicheVoeuxDetail() {
		return currentFicheVoeuxDetail;
	}

	public void setCurrentFicheVoeuxDetail(
			EOFicheVoeuxDetail currentFicheVoeuxDetail) {
		this.currentFicheVoeuxDetail = currentFicheVoeuxDetail;
	}

	public boolean isModeArbitrage() {
		return modeArbitrage;
	}

	public void setModeArbitrage(boolean modeArbitrage) {
		this.modeArbitrage = modeArbitrage;
	}

	/**
	 * 
	 * @return nom du bouton
	 */
	public String getSelectedItemId() {
		if (isModeArbitrage()) {
			return PecheMenuItem.FICHE_VOEUX_ARBITRAGE.getId();
		} else {
			return PecheMenuItem.FICHE_VOEUX.getId();
		}
		
	}
	
	public IServiceConstructionArbitrageAP getServiceConstructionArbitrageAP() {
		return serviceConstructionArbitrageAP;
	}

	public NSArray<EOAP> getListeAP() {
		return listeAP;
	}

	public void setListeAP(NSArray<EOAP> listeAP) {
		this.listeAP = listeAP;
	}
	
}