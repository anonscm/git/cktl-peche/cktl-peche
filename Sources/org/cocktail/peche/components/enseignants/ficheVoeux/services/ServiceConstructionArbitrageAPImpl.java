package org.cocktail.peche.components.enseignants.ficheVoeux.services;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPeda;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * 
 *
 */
public class ServiceConstructionArbitrageAPImpl implements IServiceConstructionArbitrageAP {
	
	private NSArray<EOAP> listeAPEnDepassement = new NSMutableArray<EOAP>();
	private NSArray<Integer> idsApEnDepassement = new NSMutableArray<Integer>();
	
	// boolean pour l'instant jamais passé à true, à modeler si problème de performance constaté
	private boolean rechercheDejaEffectuee = false;
	
	/**
	 * {@inheritDoc}
	 */	
	public NSArray<EOAP> getAPHeuresEnDepassement(EOEditingContext edc, List<IStructure> listeStructures, Integer annee, UECtrl controller) {

		if (!rechercheDejaEffectuee) {
			
			listeAPEnDepassement.clear();
			idsApEnDepassement.clear();
			
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT DISTINCT C.ID_COMPOSANT FROM SCO_SCOLARITE.COMPOSANT C");
			sql.append(" INNER JOIN GRH_PECHE.FICHE_VOEUX_DETAIL FVD ON (C.ID_COMPOSANT = FVD.ID_AP)");
			sql.append(" INNER JOIN GRH_PECHE.FICHE_VOEUX FV ON (FV.ID_FICHE_VOEUX = FVD.ID_FICHE_VOEUX)");
			sql.append(" INNER JOIN WORKFLOW.DEMANDE D ON (FV.ID_DEMANDE = D.ID_DEMANDE)");
			sql.append(" INNER JOIN WORKFLOW.ETAPE E ON (D.ID_ETAPE = E.ID_ETAPE)");
			sql.append(" INNER JOIN SCO_SCOLARITE.LIEN L ON (L.CHILD_ID = C.ID_COMPOSANT)");
			sql.append(" INNER JOIN SCO_SCOLARITE.COMPOSANT CP ON (L.PARENT_ID = CP.ID_COMPOSANT)");
			sql.append(" INNER JOIN ");
			sql.append(" SCO_SCOLARITE.STRUCTURES_COMPOSANT SC ON (SC.COMPOSANT_ID = CP.ID_COMPOSANT)");
			sql.append(" WHERE C.TYPE_COMPOSANT_ID = " + EOTypeComposant.typeAP(edc).id());
			sql.append(" AND ( CP.TAG_APPLICATION IS NULL ");
			sql.append(" OR SC.COMPOSANT_ID IS NULL");
			
			if (!CollectionUtils.isEmpty(listeStructures)) {
				NSArray<String> listeCodesStructures = new NSMutableArray<String>(listeStructures.size());
				for (IStructure structure : listeStructures) {
					listeCodesStructures.add("'" + structure.cStructure() + "'");
				}
				
				sql.append(" OR SC.C_STRUCTURE IN (" + listeCodesStructures.componentsJoinedByString(", ") + ")");
			}
			sql.append(") AND FV.ANNEE = " + annee);
			sql.append(" AND E.C_ETAPE = '" + EtapePeche.VALID_REPARTITEUR + "'");
			
			@SuppressWarnings("rawtypes")
			NSArray result = EOUtilities.rawRowsForSQL(edc, FwkCktlScolPeda.MODEL_NAME, sql.toString(), null);
			
			@SuppressWarnings("unchecked")
			EOQualifier qualifier = EOAP.ID.in((NSArray<Integer>) result.valueForKey("ID_COMPOSANT"));
			
			NSArray<EOAP> listeAP = EOAP.fetchSco_APs(edc, qualifier, null);
			
			for (EOAP ap : listeAP) {
				float nbHeuresRestantsARepartir = controller.getNbHeuresResteARepartir(ap);
				float nbHeuresSouhaitees = controller.getTotalHeuresSouhaitees(ap, annee);
				
				if (nbHeuresSouhaitees > nbHeuresRestantsARepartir) {
					idsApEnDepassement.add(ap.id());
					listeAPEnDepassement.add(ap);
				}
			}
		}
		return listeAPEnDepassement;
	}

	/**
	 * {@inheritDoc}
	 */		
	public NSArray<EOActuelEnseignant> getListeEnseignantAvecAPEnDepassement(EOEditingContext edc,	NSArray<EOActuelEnseignant> listeEnseignants,
			List<IStructure> listeStructures, Integer annee, UECtrl controller) {
		
		NSArray<EOAP> listeAPAArbitrer = getAPHeuresEnDepassement(edc, listeStructures, annee, controller);
		if (listeAPAArbitrer != null && !listeAPAArbitrer.isEmpty()) {
			return getListeEnseignantAvecIndicateurArbitrage(edc, listeEnseignants, listeAPAArbitrer);
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */		
	public NSArray<EOAP> getListeAPEnDepassement(EOActuelEnseignant enseignant, Integer annee) {

		NSArray<EOAP> listeAPAArbitrerEnseignant = new NSMutableArray<EOAP>();

		EOFicheVoeux ficheVoeux = enseignant.getFicheVoeux(annee);
		if (ficheVoeux != null) {
			NSArray<EOFicheVoeuxDetail> listeFicheVoeuxDetail = ficheVoeux.listeFicheVoeuxDetails();

			for (EOFicheVoeuxDetail eoFicheVoeuxDetail : listeFicheVoeuxDetail) {
				EOAP ap = eoFicheVoeuxDetail.composantAP();
				if (ap != null && listeAPEnDepassement.contains(ap)) {
					if (!listeAPAArbitrerEnseignant.contains(ap)) {
						listeAPAArbitrerEnseignant.add(ap);
					}
				}
			}
		}
		return listeAPAArbitrerEnseignant;
	}
	
	
	/**
	 * 
	 * @param edc
	 * @param listeEnseignants
	 * @param listeAPAArbitrer
	 * @return
	 */
	private NSArray<EOActuelEnseignant> getListeEnseignantAvecIndicateurArbitrage(EOEditingContext edc, NSArray<EOActuelEnseignant> listeEnseignants, NSArray<EOAP> listeAPAArbitrer) {
		
		NSArray<EOActuelEnseignant> listeEnseignantsAArbitrer = new NSMutableArray<EOActuelEnseignant>();
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT VPAE.NO_DOSSIER_PERS FROM SCO_SCOLARITE.COMPOSANT C");
		sql.append(" INNER JOIN GRH_PECHE.FICHE_VOEUX_DETAIL FVD ON (C.ID_COMPOSANT = FVD.ID_AP)");
		sql.append(" INNER JOIN GRH_PECHE.FICHE_VOEUX FV ON (FV.ID_FICHE_VOEUX = FVD.ID_FICHE_VOEUX)");
		sql.append(" INNER JOIN WORKFLOW.DEMANDE D ON (FV.ID_DEMANDE = D.ID_DEMANDE)");
		sql.append(" INNER JOIN WORKFLOW.ETAPE E ON (D.ID_ETAPE = E.ID_ETAPE)");
		sql.append(" INNER JOIN GRH_PECHE.V_PERSONNEL_ACTUEL_ENS VPAE ON (FV.NO_INDIVIDU = VPAE.NO_DOSSIER_PERS)");
		sql.append(" WHERE C.ID_COMPOSANT IN (" + idsApEnDepassement.componentsJoinedByString(", ") + ")");
		sql.append(" AND E.C_ETAPE = '" + EtapePeche.VALID_REPARTITEUR + "'");
		
		@SuppressWarnings("unchecked")
		NSArray<BigDecimal> result = EOUtilities.rawRowsForSQL(edc, "Peche", sql.toString(), null);
		
		@SuppressWarnings("unchecked")
		NSArray<BigDecimal> idsNoDossier = (NSArray<BigDecimal>) result.valueForKey("NO_DOSSIER_PERS");
		
		for (EOActuelEnseignant enseignant : listeEnseignants) {
			if (idsNoDossier.contains(BigDecimal.valueOf(enseignant.noDossierPers()))) {
				listeEnseignantsAArbitrer.add(enseignant);
			} 			
		}
		
		return listeEnseignantsAArbitrer;
		
	}
	
	/**
	 * {@inheritDoc}
	 */		
	public void reinitialiserRecherche() {
		this.rechercheDejaEffectuee = false;
	}
	
}
