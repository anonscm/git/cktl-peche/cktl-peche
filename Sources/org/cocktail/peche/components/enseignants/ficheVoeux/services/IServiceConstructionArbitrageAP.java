package org.cocktail.peche.components.enseignants.ficheVoeux.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * 
 * @author juliencallewaert
 *
 */
public interface IServiceConstructionArbitrageAP {

	/**
	 * Retourne une liste d'AP dont la somme des nombre d'heures souhaitées sur les fiches de voeux est supérieur à (heures maquette - heures déjà réparties)
	 * @param edc editingContext
	 * @param listeStructures listeStructures
	 * @param annee annee en cours
	 * @param controller instance de UECtrl
	 * @return listeAP
	 */
	NSArray<EOAP> getAPHeuresEnDepassement(EOEditingContext edc, List<IStructure> listeStructures, Integer annee, UECtrl controller);
	
	
	/**
	 * Sur la liste d'enseignants passé en paramètre, on retourne les enseignants qui font partie d'un AP pour lequel il devrait y avoir un arbitrage
	 * de réalisé, c'est à dire si sur un AP (somme heures souhaitées > (heures maquette - heures déjà réparties))
	 * @param edc editingContext
	 * @param listeEnseignants listeEnseignants
	 * @param listeStructures liste de structures
	 * @param annee annee en cours
	 * @param controller instance de UECtrl
	 * @return liste d'enseignants
	 */
	NSArray<EOActuelEnseignant> getListeEnseignantAvecAPEnDepassement(EOEditingContext edc,	NSArray<EOActuelEnseignant> listeEnseignants,
			List<IStructure> listeStructures, Integer annee, UECtrl controller);
	
	
	/**
	 * Renvoie une liste d'AP sur lequel un arbitrage doit être fait, pour un enseignant donné
	 * @param enseignant enseignant 
	 * @param annee année en cours
	 * @return liste d'AP
	 */
	NSArray<EOAP> getListeAPEnDepassement(EOActuelEnseignant enseignant, Integer annee);
	
	
	/**
	 * 
	 */
	void reinitialiserRecherche();
	
}
