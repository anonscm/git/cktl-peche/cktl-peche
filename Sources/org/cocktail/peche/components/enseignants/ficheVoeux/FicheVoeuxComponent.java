package org.cocktail.peche.components.enseignants.ficheVoeux;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOActuelEnseignant;

import com.webobjects.appserver.WOContext;

/**
 * Classe permettant d'afficher soit la liste des fiches de voeux soit la fiche
 * de voeux de l'enseignant en fonction du profil connecté
 *
 * @author Chama LAATIK
 *
 */
public class FicheVoeuxComponent extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 5187673448449177301L;


	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public FicheVoeuxComponent(WOContext context) {
		super(context);
	}

	/**
	 * Renvoie l'enseignant connecté, uniquement si le profil est celui d'un enseignant
	 * @return enseignant
	 */
	public EOActuelEnseignant getEditedObject() {
		return getPecheSession().getApplicationUser().getEnseignant();
	}

	public String getSelectedItemId() {
		return PecheMenuItem.MA_FICHE_VOEUX.getId();
	}
}