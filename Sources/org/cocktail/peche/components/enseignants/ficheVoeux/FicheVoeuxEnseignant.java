package org.cocktail.peche.components.enseignants.ficheVoeux;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.GenerateurPDFCtrl;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.IServiceConstructionArbitrageAP;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant de voir la fiche de voeux de l'enseignant statutaire.
 *
 * @author Chama LAATIK
 *
 */
public class FicheVoeuxEnseignant extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 2070474574138465813L;

	private EOActuelEnseignant editedObject;
	private EnseignantsStatutairesCtrl controleur;
	private EOFicheVoeux ficheVoeux;
	private GenerateurPDFCtrl generateurPDFCtrl;
	private HceCtrl hceCtrl;
	
	@Inject
	private IServiceConstructionArbitrageAP serviceConstructionArbitrageAP;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public FicheVoeuxEnseignant(WOContext context) {
        super(context);
        this.controleur = new EnseignantsStatutairesCtrl(this.edc(),getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        this.hceCtrl = new HceCtrl(this.edc());
    }

	/**
	 * Enregistre le commentaire de la fiche de voeux de l'enseignant
	 */
	public void enregistrerCommentaireFiche() {
		try {
			NSTimestamp now = new NSTimestamp();
			PecheApplicationUser appUser = getPecheSession().getApplicationUser();
			EOPersonne personne = appUser.getPersonne();

			if (!StringCtrl.isEmpty(ficheVoeux.commentaire())) {
				ficheVoeux.setDateCommentaire(now);
			}

			ficheVoeux.setPersonneModification(personne);
			ficheVoeux.setDateModification(now);
			edc().saveChanges();
		} catch (ValidationException e) {
		    edc().invalidateAllObjects();
		    edc().revert();
		    throw e;
		}
	}

	/**
	 * Création et initialisation de la fiche de voeux
	 * @param editedObject enseignant
	 */
	public void setEditedObject(EOActuelEnseignant editedObject) {
			this.editedObject = editedObject;
			ModeEdition modeEdition = ModeEdition.MODIFICATION;
			this.ficheVoeux = getFicheVoeux();

		if (ficheVoeux == null) {
			modeEdition = ModeEdition.CREATION;
			int persId = personneConnecte().persId();

			ficheVoeux = EOFicheVoeux.creerEtInitialiser(edc(), persId);

			try {
				beforeSaveChanges(modeEdition);
				edc().saveChanges();
			} catch (ValidationException e) {
			    this.setEditedObject(null);
			    edc().invalidateAllObjects();
			    edc().revert();
			    throw e;
			}
		}
	}

	/**
	 * Enregistre le service d'un intervenant.
	 *
	 * @throws ValidationException
	 *             : exception au cours de l'enregistrement.
	 */
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();

		Session pecheSession = getPecheSession();
		EOPersonne personne = pecheSession.getApplicationUser().getPersonne();

		ficheVoeux.setEnseignant(getEditedObject());
		ficheVoeux.setTemoinValide(Constante.OUI);
		ficheVoeux.setPersonneCreation(personne);
		ficheVoeux.setDateCreation(now);
		ficheVoeux.setDateModification(now);
		ficheVoeux.setAnnee(pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
		
	}

	/**
	 * valide la fiche et la fait avancer à l'étape suivante
	 */
	public void validerFiche() {
		
		EtapePeche etapePeche = getFicheVoeux().enseignant().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		getDemande().faireAvancerSurChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), personneConnecte().persId());
			
		if (EtapePeche.VALID_REPARTITEUR.equals(etapePeche)) {
			controleur.basculerFicheVoeuxEnFicheService(getFicheVoeux().enseignant(), ficheVoeux, getPecheSession(), 
					getServiceConstructionArbitrageAP().getListeAPEnDepassement(getFicheVoeux().enseignant(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
		}
		
		edc().saveChanges();
	}

	/**
	 * rend actif/inactif le bouton Valider de la fiche de voeux
	 * @return vrai/faux
	 */
	public boolean getBoutonActif() {
		if (getFicheVoeux() != null) {
			return getBoutonsActifsRepartitionVoeux()
					|| (isEnseignantDansCibleEnseignantStatutaire() && getAutorisation().hasDroitUtilisationChemin(getDemande(), EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER));
		}
		return false;
	}

	/**
	 * Boutons des voeux actifs si la demande est sur l'étape validation de l'enseignant
	 * @return vrai/faux
	 */
	public boolean getBoutonsActifsRepartitionVoeux() {
		if (getFicheVoeux() != null) {
			return getAutorisation().hasDroitUtilisationChemin(getDemande(), EtapePeche.VALID_ENSEIGNANT, TypeCheminPeche.VALIDER);
		}
		return false;
	}
	
	private boolean isEnseignantDansCibleEnseignantStatutaire() {
		if (getFonctionRepartiteur()) {
			return getAutorisation().isEnseignantContenueDansCibleEnseignantStatutaire(getEditedObject());
		}
		return false;
	}

	/**
	 * Renvoie vrai si le répartiteur force la validation de la fiche de voeux (==> l'enseignant ne l'a pas validé avant)
	 * @return vrai/faux
	 */
	public boolean getValidationForcee() {
		EtapePeche etape = getEditedObject().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		return (EtapePeche.VALID_ENSEIGNANT_FICHE_VOEUX.equals(etape) || etape == null) 
				&& !(getEditedObject().equals(getPecheSession().getApplicationUser().getEnseignant()));
	}

	/**
	 * renvoie le libelle long du corps de l'enseignant
	 * @return corps de l'intervenant
	 */
	public String getLibelleCorps() {
		String libelle = controleur.getLibelleLongCorpsOuContrat(getEditedObject());
		return libelle;
	}

	/**
	 * @return le statut de l'enseignant.
	 */
	public String getStatut() {
		return controleur.getStatut(getEditedObject());
	}

	/**
	 * Permet de récuperer le nom de la composante.
	 *
	 * @return le nom de la composante.
	 */
	public String getCodeComposante() {
		EOStructure composante = getEditedObject().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = "?";
		
		if (composante != null) {
			nomComposante = composante.lcStructure();
		}

		return nomComposante;
	}

	/**
	 * Permet de récuperer le nom du département d'enseignement.
	 *
	 * @return le nom du département d'enseignement.
	 */
	public String getCodeDepartementEnseignement() {
		EOStructure composante = getEditedObject().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		String nomComposante = null;
		
		if (composante != null) {
			nomComposante = composante.lcStructure();
		}

		return nomComposante;
	}

	/**
	 * Renvoie le service statutaire de l'enseignant
	 *
	 * @return service statutaire.
	 */
	public double getServiceStatutaire() {
		return getEditedObject().getNbHeuresServiceStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * @return Le nombre d'heures total des réductions de service (pour l'affichage)
	 */
	public double getTotalNbHeuresReductionOppose() {
		return -hceCtrl.getTotalNbHeuresReduction(getEditedObject(), 
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}

	/**
	 * renvoie le service dû de l'enseignant
	 * 
	 * @return le service dû
	 */
	public double getServiceDu() {
		return controleur.getServiceDu(getEditedObject(), getPecheSession()
				.getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	/**
	 * renvoie le total des heures que l'enseignant souhaite faire dans son service en HETD
	 * @return nombre d'heures
	 */
	public double getServiceSouhaite() {
		return controleur.getVoeuxSouhaite(getEditedObject());
	}

	/**
	 * renvoie la différence entre le service souhaité et le service dû en HETD
	 * @return delta
	 */
	public double getDelta() {
		return getServiceSouhaite() - getServiceDu();
	}

	/**
	 * Renvoie l'enseignant
	 * 	si on accède directement à la fiche, renvoie le binding correspondant
	 * @return enseignant
	 */
	public EOActuelEnseignant getEditedObject() {
		if (editedObject == null) {
			this.editedObject = (EOActuelEnseignant) valueForBinding("enseignant");
		}
		return editedObject;
	}

	public EOFicheVoeux getFicheVoeux() {
		return controleur.getFicheVoeux(editedObject);
	}

	public void setFicheVoeux(EOFicheVoeux ficheVoeux) {
		this.ficheVoeux = ficheVoeux;
	}

	public EnseignantsStatutairesCtrl getControleur() {
		return controleur;
	}

	public void setControleur(EnseignantsStatutairesCtrl controleur) {
		this.controleur = controleur;
	}

	/**
	 * @return la demande sur le circuit de validation
	 */
	public EODemande getDemande() {
		EOFicheVoeux eoFicheVoeux = getEditedObject().getFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (eoFicheVoeux != null) {
			return eoFicheVoeux.toDemande();
		}
		return null;
	}
	
	/**
	 * Imprimer la fiche 
	 */

	public WOActionResults imprimerFiche() {
		//génération du xml
		
		String fluxXml = controleur.getFluxFicheVoeux(getEditedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession(), getComponentCtrl(), "Fiche de voeux");
		
		//génération du pdf
		generateurPDFCtrl = new GenerateurPDFCtrl(fluxXml);

		generateurPDFCtrl.setReportFilename(controleur.getFilenameFicheService(getEditedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), true, false) + ".pdf");
			
		generateurPDFCtrl.genererFicheVoeux("Reports/ficheVoeux/fiche_voeux.jasper");
		
		return null;
	}
	
	public GenerateurPDFCtrl getGenerateurPDFCtrl() {
		return generateurPDFCtrl;
	}
	/**
	 * Renvoie le menu sélectionné
	 * ==> sert pour les personnes qui cumulent 2 profils dont celui d'enseignant
	 * @return id du menu
	 */
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", "listeFicheVoeux");
	}
	
	public IServiceConstructionArbitrageAP getServiceConstructionArbitrageAP() {
		return serviceConstructionArbitrageAP;
	}
	
	/**
	 * @return la methode de calcul
	 */
	public String getMethodeCalcul() {
		return getEditedObject().getMethodeCalcul(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
	
	
}