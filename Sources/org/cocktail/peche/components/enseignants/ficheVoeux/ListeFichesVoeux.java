package org.cocktail.peche.components.enseignants.ficheVoeux;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.IServiceConstructionArbitrageAP;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;

import com.google.inject.Inject;
import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe permet d'afficher la liste des fiches de voeux des enseignants.
 *
 * @author Chama Laatik
 *
 */
public class ListeFichesVoeux extends CktlPecheTableComponent<EOActuelEnseignant> {

	private static final long serialVersionUID = 7995663078137349902L;

	private EnseignantsStatutairesCtrl controleur;
	private UECtrl controleurUE;
	private String tokenNom;
	private String tokenPrenom;

	private NSArray<EOActuelEnseignant> listeEnseignantsAArbitrer; 
	
	@Inject
	private IServiceConstructionArbitrageAP serviceConstructionArbitrageAP;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'éxécution.
	 */
	public ListeFichesVoeux(WOContext context) {
		super(context);
		this.controleur = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
		this.controleurUE = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOActuelEnseignant.ENTITY_NAME);
		
		EOQualifier qualifier = this.controleur.getListeEnseignants(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		ERXFetchSpecification<EOActuelEnseignant> fetchSpec = EOActuelEnseignant.fetchSpec().qualify(qualifier).sort(getTriParDefaut());

		dataSource.setFetchSpecification(fetchSpec);
		fetchSpec.setUsesDistinct(true);
		ERXDisplayGroup<EOActuelEnseignant> dg = this.getDisplayGroup();		
		dg.setDataSource(dataSource);		
		dg.fetch();
		dg.setObjectArray(filtrerDoublons(dg.allObjects()));
		
	}

	private NSArray<EOActuelEnseignant> filtrerDoublons(NSArray<EOActuelEnseignant> liste) {

		NSMutableArray<Integer> entrees = new NSMutableArray<Integer>();
		NSMutableArray<EOActuelEnseignant> sortie = new NSMutableArray<EOActuelEnseignant>();
		EOActuelEnseignant actuelEnseignant;

		for (int i = 0; i < liste.count(); i++) {
			actuelEnseignant = (EOActuelEnseignant) liste.get(i);
			if (!entrees.contains(actuelEnseignant.toIndividu().persId())) {
				sortie.add(actuelEnseignant);
				entrees.add(actuelEnseignant.toIndividu().persId());
			}
		}

		return sortie.immutableClone();
	}
	
	/**
	 * @return Le tri par défaut de la liste
	 */
	private ERXSortOrderings getTriParDefaut() {
		return EOActuelEnseignant.TO_INDIVIDU.dot(EOIndividu.NOM_AFFICHAGE_KEY).asc().then(EOActuelEnseignant.TO_INDIVIDU.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).asc());
	}

	/**
	 * Action d'ouverture de la fiche de voeux.
	 *
	 * @return la fiche de voeux de l'enseignant.
	 */
	public WOActionResults ouvrirFicheVoeux() {
		if (getSelectedObject() != null) {
			if (checkEnseignantAffecte(controleur, getSelectedObject())) {
				return null;
			}
			
			FicheVoeuxEnseignant page = (FicheVoeuxEnseignant) pageWithName(FicheVoeuxEnseignant.class.getName());
			page.setEditedObject(getSelectedObject());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} else {
			return doNothing();
		}
	}

	/**
	 * Action d'ouverture de l'arbitrage
	 *
	 * @return la fiche de voeux de l'enseignant.
	 */
	public WOActionResults ouvrirArbitrage() {
		if (getSelectedObject() != null) {
			if (checkEnseignantAffecte(controleur, getSelectedObject())) {
				return null;
			}
			
			FicheVoeuxArbitrage page = (FicheVoeuxArbitrage) pageWithName(FicheVoeuxArbitrage.class.getName());
			
			page.setListeAP(getServiceConstructionArbitrageAP().getListeAPEnDepassement(getSelectedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
			
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} else {
			return doNothing();
		}
	}
	
	
    /**
     * Retourne la fiche de service de l'enseignant.
     * @return la page.
     */
    public WOActionResults getLinkToFicheEnseignant() {
		if (getCurrentItem() != null) {
			if (checkEnseignantAffecte(controleur, getCurrentItem())) {
				return null;
			}
			
			FicheVoeuxEnseignant page = (FicheVoeuxEnseignant) pageWithName(FicheVoeuxEnseignant.class.getName());
			page.setEditedObject(getCurrentItem());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} else {
			return doNothing();
		}
	}
	
    
	/**
	 * Validation de la fiche de voeux de l'enseignant
	 *
	 * @return null
	 */
    public WOActionResults validerFiche() {
		
		EtapePeche etapePeche = getSelectedObject().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		EOFicheVoeux ficheVoeux = getSelectedObject().getFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		if (EtapePeche.VALID_ENSEIGNANT_FICHE_VOEUX.equals(etapePeche)) {
			ficheVoeux.toDemande().faireAvancerSurChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), personneConnecte().persId());
		} else if (EtapePeche.VALID_REPARTITEUR.equals(etapePeche)) {
			
			ficheVoeux.toDemande().faireAvancerSurChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), personneConnecte().persId());
			
			controleur.basculerFicheVoeuxEnFicheService(getSelectedObject(), ficheVoeux, getPecheSession(), 
					getServiceConstructionArbitrageAP().getListeAPEnDepassement(getSelectedObject(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
			
		}
		
		edc().saveChanges();
		
		return doNothing();
	}
    
    
	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter.
	 *
	 * @return null.
	 */
	public WOActionResults filtrer() {
		EOQualifier qualifierNom = null;
		EOQualifier qualifierPrenom = null;
							
		if (!StringCtrl.isEmpty(tokenNom)) {
			qualifierNom = 	EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(tokenNom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));
		}		
	
		if (!StringCtrl.isEmpty(tokenPrenom)) {
			qualifierPrenom = EOActuelEnseignant.TO_INDIVIDU
					.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).contains(tokenPrenom)
					.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));

		} 
	
		filtrerDisplayGroup(ERXQ.and(qualifierNom, qualifierPrenom));
		
		return doNothing();
	}
	
	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter le
	 * displayGroup selon le nom.
	 *
	 * @return null.
	 */
	public WOActionResults filtrerNom() {
		EOQualifier qualifier = EOActuelEnseignant.TO_INDIVIDU
				.dot(EOIndividu.NOM_AFFICHAGE_KEY).contains(tokenNom)
				.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));

		if (!StringCtrl.isEmpty(tokenNom)) {
			filtrerDisplayGroup(qualifier);
		} else {
			filtrerDisplayGroup(null);
		}

		tokenPrenom = "";
		return null;
	}

	/**
	 * Cette méthode est appelée par WO en mode Ajax pour filter le
	 * displayGroup selon le prénom.
	 *
	 * @return null.
	 */
	public WOActionResults filtrerPrenom() {
		EOQualifier qualifier = EOActuelEnseignant.TO_INDIVIDU
				.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).contains(tokenPrenom)
				.and(EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.OUI));

		if (!StringCtrl.isEmpty(tokenPrenom)) {
			filtrerDisplayGroup(qualifier);
		} else {
			filtrerDisplayGroup(null);
		}

		tokenNom = "";
		return null;
	}

	/**
	 * renvoie la différence entre le service statutaire et les décharges de
	 * l'enseignant
	 *
	 * @return le service dû
	 */
	public double getServiceDu() {
		EOFicheVoeux ficheVoeux = getCurrentItem().getFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	
		if (ficheVoeux != null && ficheVoeux.heuresServiceDu() != null) {
			return ficheVoeux.getHeuresServiceDu().doubleValue();
		}
		
		return controleur.getServiceDu(getCurrentItem(), getPecheSession()
				.getPecheParametres().getAnneeUniversitaireEnCours());
	}

	/**
	 * 
	 * @return l'état de la fiche de voeux
	 */
	public String etatFicheVoeux() {
		String etat = Constante.ETAT_NON_INITIALISE;
		if (getCurrentItem() != null) {
			EtapePeche etape = getCurrentItem().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			if (etape != null) {
				etat = etape.getEtatDemande();
			} else {
				etat = Constante.ETAT_NON_INITIALISE;
			}
			if (EtapePeche.VALIDEE.getEtatDemande().equals(etat)) {
				etat = Constante.ETAT_BASCULE;
			}
		}
		getCurrentItem().setEtatFicheVoeux(etat);
		return etat;
	}
	
	public String getSrcImage() {
		return "images/avertissement16x16.png";
	}
	
	/**
	 * 
	 * @return boutons désactivés
	 */
	public boolean boutonsInactifs() {
		return getSelectedObject() == null;
	}
	
	/**
	 * 
	 * @return bouton arbitrage activé ou non
	 */
	public boolean boutonArbitrageInactif() {
		return boutonsInactifs() || !estSelectedObjectArbitrable();
	}
	
	
	/**
	 * 
	 * @return bouton arbitrage activé ou non
	 */
	public boolean boutonValiderInactif() {
		return boutonsInactifs() || estEtatNonValidable();
	}
	
	/**
	 * 
	 * @return renvoie true si non validable
	 */
	public boolean estEtatNonValidable() {
		EtapePeche etapePeche = getSelectedObject().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		if (etapePeche != null) {
			return EtapePeche.VALIDEE.equals(etapePeche);
		} else {
			return true;
		}
	}
	
	
	/**
	 * Renvoie le total du nombre d'heures souhaitées par l'enseignant sur sa fiche de voeux
	 *
	 * @return nombre d'heures
	 */
	public double getNbHeuresSouhaitees() {
		EOFicheVoeux ficheVoeux = getCurrentItem().getFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		if (ficheVoeux != null && ficheVoeux.heuresSouhaitees() != null) {
			return ficheVoeux.getHeuresServiceSouhaite().doubleValue();
		}
		
		return controleur.getVoeuxSouhaite(getCurrentItem());
	}

	public String getTokenNom() {
		return tokenNom;
	}

	public void setTokenNom(String tokenNom) {
		this.tokenNom = tokenNom;
	}

	public String getTokenPrenom() {
		return tokenPrenom;
	}

	public void setTokenPrenom(String tokenPrenom) {
		this.tokenPrenom = tokenPrenom;
	}

	/**
	 * @return the afficherBoutonFicheDeVoeux
	 */
	public boolean afficherBoutonFicheDeVoeux() {
		return getSelectedObject() == null;
	}

	
	/**
	 * 
	 * @return true si un arbitrage est proposé sur cet enseignant
	 */
	public boolean estSelectedObjectArbitrable() {
		boolean estArbitrable = false;
		if (getListeEnseignantsAArbitrer() != null) {
			if (getSelectedObject() != null) {
				estArbitrable = getListeEnseignantsAArbitrer().contains(getSelectedObject());
			} 
		} 
		return estArbitrable;
	}

	/**
	 * 
	 * @return true si un arbitrage est proposé sur cet enseignant
	 */
	public boolean estCurrentItemArbitrable() {
		boolean estArbitrable = false;
		if (getListeEnseignantsAArbitrer() != null) {
			if (getCurrentItem() != null) {
				if (getListeEnseignantsAArbitrer().contains(getCurrentItem())) {
					estArbitrable = true;
				} else {
					estArbitrable = false;
				}
			} 
		} 
		return estArbitrable;
	}
	
	
	/**
	 * 
	 * @return true si le message d'avertissement doit être présent
	 */
	public boolean estMessageAvertissementValidationPresent() {
		if (getSelectedObject() == null) {
			return false;
		}
		return estSelectedObjectArbitrable() && EtapePeche.VALID_REPARTITEUR.equals(getSelectedObject().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
	}
	
	/**
	 * 
	 * @return true si fiche de voeux en validation par le répartiteur
	 */
	public boolean estValidationRepartiteur() {
		if (getSelectedObject() == null) {
			return false;
		}
		return EtapePeche.VALID_REPARTITEUR.equals(getSelectedObject().getEtapePecheFicheVoeux(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
	}
	
	
	public IServiceConstructionArbitrageAP getServiceConstructionArbitrageAP() {
		return serviceConstructionArbitrageAP;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		listeEnseignantsAArbitrer = getServiceConstructionArbitrageAP().getListeEnseignantAvecAPEnDepassement(edc(), getDisplayGroup().allObjects(), getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire(),
				getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), this.controleurUE);
		super.appendToResponse(response, context);
	}

	public NSArray<EOActuelEnseignant> getListeEnseignantsAArbitrer() {
		return listeEnseignantsAArbitrer;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.FICHE_VOEUX.getId();
	}
	
}
