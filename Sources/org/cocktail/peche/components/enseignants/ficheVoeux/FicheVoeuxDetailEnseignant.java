package org.cocktail.peche.components.enseignants.ficheVoeux;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

/**
 * Cette classe permet de récupérer la liste des voeux d'un enseignant.
 *
 * @author Chama LAATIK
 */
public class FicheVoeuxDetailEnseignant extends CktlPecheTableComponent<EOFicheVoeuxDetail> {

	private static final long serialVersionUID = -1473158345839935579L;
	private EOFicheVoeux ficheVoeux;
	
	private ComposantCtrl controleurComposant;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public FicheVoeuxDetailEnseignant(WOContext context) {
        super(context);
        
        this.controleurComposant = new ComposantCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOFicheVoeuxDetail.ENTITY_NAME);
		ERXDisplayGroup<EOFicheVoeuxDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
    }

	@Override
	public void awake() {
		ERXDisplayGroup<EOFicheVoeuxDetail> dg = this.getDisplayGroup();
        ficheVoeux = (EOFicheVoeux) valueForBinding("ficheVoeux");
        if (ficheVoeux != null) {
	        String path = ERXQ.keyPath(EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
	        EOQualifier qualifier = ERXQ.equals(path, ficheVoeux);

	        ((ERXDatabaseDataSource) dg.dataSource()).setAuxiliaryQualifier(qualifier);
			dg.fetch();
        }
	}

	/**
	 * affiche la composante ou l'établissement externe où aura lieu l'enseignement
	 * @return composante ou etablissement externe
	 */
	public String affichageLieuEnseignement() {
		if (getCurrentItem().composantAP() != null) {
			return this.controleurComposant.getComposanteParentAP(getCurrentItem().composantAP());
		} else {
			return getCurrentItem().etablissementExterne();
		}
	}


	/**
	 * Action d'ajout.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ajouterAction() {
		FicheVoeuxDetailForm page = (FicheVoeuxDetailForm)
				pageWithName(FicheVoeuxDetailForm.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setFicheVoeux(ficheVoeux);
		return page;
	}

	/**
	 * Action de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			FicheVoeuxDetailForm page = (FicheVoeuxDetailForm)
					pageWithName(FicheVoeuxDetailForm.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setFicheVoeux(ficheVoeux);
			page.setEditedObject(getSelectedObject());
			return page;
		} else {
			return null;
		}
	}

	/**
	 * Action de suppression.
	 *
	 * @return une instance du formulaire en mode modification.
	 * @throws Exception imposé par EOComposant.supprimer().
	 */
	public WOActionResults deleteAction() throws Exception {
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		try {
			if (getSelectedObject() != null) {
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				getFicheVoeux().majDonnees(edc(), now, personne, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}
		AjaxUpdateContainer.updateContainerWithID("ficheDeVoeux", context());
		return doNothing();
	}

	public boolean getBoutonsActifs() {
		return !valueForBooleanBinding("boutonsActifs", true);
	}

	public boolean getBoutonsModifActifs() {
		return !valueForBooleanBinding("boutonsActifs", true) || getSelectedObject() == null;
	}
	
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	public EOFicheVoeux getFicheVoeux() {
		return ficheVoeux;
	}
	
	public boolean getAffichageBoutons() {
		return valueForBooleanBinding("affichageBoutons", true);
	}
}