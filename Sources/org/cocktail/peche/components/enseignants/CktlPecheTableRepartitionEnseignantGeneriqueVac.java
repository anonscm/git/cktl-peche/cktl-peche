package org.cocktail.peche.components.enseignants;

import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesVacCtrl;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public abstract class CktlPecheTableRepartitionEnseignantGeneriqueVac extends CktlPecheTableRepartitionEnseignantVacataireOnglet {

	private static final long serialVersionUID = 182418446105775964L;
	
	private EOService service;
	
	private EnseignantsGeneriquesVacCtrl controleur;
	
	/**
	 * Constructeur.
	 * @param context
	 */
	public CktlPecheTableRepartitionEnseignantGeneriqueVac(WOContext context) {
		super(context);
		this.controleur = new EnseignantsGeneriquesVacCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOServiceDetail.ENTITY_NAME);
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
	}

	@Override
	public void awake() {
		service = (EOService) valueForBinding("service");
		
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		
		String path = ERXQ.keyPath(EOServiceDetail.SERVICE_KEY);
		EOQualifier qualifier = ERXQ.and(
			ERXQ.equals(path, service),
			ERXQ.isNotNull(getTableKey())
			);
        ((ERXDatabaseDataSource) dg.dataSource()).setAuxiliaryQualifier(qualifier);
		dg.fetch();
	}

	protected abstract String getTableKey();

	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	public EOService getService() {
		return service;
	}

	
	public String getSelectedItemId() {
		return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId());
	}
	
	/**
	 * 
	 * @return sous total heures prévues
	 */
	public Double getSousTotalHeuresPrevues() {
		return controleur.getTotalHeuresPrevues(listerServiceDetailParRepartService());
	}

	public EnseignantsGeneriquesVacCtrl getControleur() {
		return controleur;
	}
	
	
	/**
	 * Boutons du service actifs si la demande est sur l'étape validation du répartiteur
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifsRepartitionService() {
		return !(isMemeComposanteOnglet());
	}

	/**
	 * @return the boutonsInactifsModifierRepartitionService
	 */
	public boolean boutonInactifModifierRepartitionService() {
		//Si la personne n'est pas de la composante mais a le droit de saisir les heures à payer (ex DRH)
		// => on autorise la modification
		return !(isMemeComposanteOnglet() || getAutorisation().hasDroitUtilisationSaisieHeuresAPayer());
	}

	/**
	 * Boutons du reh actifs si la demande est sur l'étape validation du répartiteur
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifsRepartitionReh() {
		return !hasDroitUtilisationRepartitionRehVacataire() || !isMemeComposanteOnglet();
	}
	
	public boolean getBoutonsActifsRepartitionService() {
		return getBoutonsInactifsRepartitionService();
	}
	
	public boolean getBoutonModifierActifRepartitionService() {
		return boutonInactifModifierRepartitionService() || getSelectedObject() == null;
	}
	
	public boolean getBoutonSupprimerActifRepartitionService() {
		return getBoutonsActifsRepartitionService() || getSelectedObject() == null;
	}
	
	public boolean getBoutonsActifsRepartitionReh() {
		return getBoutonsInactifsRepartitionReh();
	}
	
	public boolean getBoutonModifierActifRepartitionReh() {
		return boutonInactifModifierRepartitionReh() || getSelectedObject() == null;
	}
	
	public boolean getBoutonSupprimerActifRepartitionReh() {
		return getBoutonsActifsRepartitionReh() || getSelectedObject() == null;
	}
	
	
}
