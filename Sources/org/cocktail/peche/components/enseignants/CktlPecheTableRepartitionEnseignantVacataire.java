package org.cocktail.peche.components.enseignants;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

import er.ajax.AjaxUpdateContainer;

/**
 * Cette classe permet de récupérer la liste des répartitions d'un enseignant séparemment (REH et Service).
 *
 * @author Yannick Mauray
 * @author Chama LAATIK
 */
public abstract class CktlPecheTableRepartitionEnseignantVacataire extends CktlPecheTableRepartitionEnseignantVacataireOnglet {

	private static final long serialVersionUID = -6430800101350804617L;
	
	private EnseignantsVacatairesCtrl  controleur;
	
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public CktlPecheTableRepartitionEnseignantVacataire(WOContext context) {
		super(context);

		this.controleur = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	    
	}
	
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}

	@Override
	public EnseignantsVacatairesCtrl getControleur() {
		return controleur;
	}

	
	/**
	 * Bouton modifier service actif 
	 * 	si 
	 * 		la demande est sur l'étape validation du répartiteur 
	 * 	OU 
	 * 		l'utilisateur a le droit de saisir des heures à payer et a le droit de valider la fiche
	 * 
	 * @param demande : la demande
	 * @return vrai/faux
	 */
	public boolean getBoutonInactifModifierRepartitionService(EODemande demande) {	
		return demande == null || !(getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER) 
				|| (getAutorisation().hasDroitUtilisationSaisieHeuresAPayer() && getAutorisation().hasDroitUtilisationChemin(demande, TypeCheminPeche.VALIDER))
				|| isBouclageFicheServicePossible(demande));
	}
	
	/**
	 * @return vrai si commentaire présent dans le serviceDetail
	 */
	public boolean isCommentairePresent() {
		if (getSelectedObject() != null) {
			return getSelectedObject().commentaire() != null;
		}
		
		return false;
	}
	
	public boolean isHeuresPayeesOuAPayerNonNull() {
		return ((getSelectedObject() != null) && ((getSelectedObject().heuresAPayer() != null)
				|| (getSelectedObject().heuresPayees() != null)));
	}
	
	/**
	 * 	
	 * @return true si circuit définitif
	 */
	public boolean isCircuitDefinitif() {
		// si l'onglet Tous est sélectionné, il n'y a pas de demande associée, on fait donc le test sur la première demande
		if (Constante.ONGLET_COMPOSANTE_TOUS_CODE.equals(getOngletComposanteSelectionne().getCode())) {
			if (getService().toListeRepartService().isEmpty()) {
				return false;
			}
			return !this.controleur.isCircuitPrevisionnel(getService().toListeRepartService().get(0).toDemande());
		}
		return !this.controleur.isCircuitPrevisionnel(getDemande());
	}
	
	/**
	 * @return le delta des heures en présentiel entre les heures payées et les heures réalisées par l'intervenant
	 */
	public BigDecimal deltaHeuresPayeEtRealisePresentiel() {
		return getControleur().getDeltaHeuresPayeEtRealisePresentiel(getCurrentItem().heuresPayees(), getCurrentItem().heuresRealisees());
	}

	
	
	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			faireBouclerFicheServiceSiNecessaire(getDemande());
			
			CktlPecheFormServiceEnseignantVacataire page = (CktlPecheFormServiceEnseignantVacataire) pageWithName(CktlPecheFormServiceEnseignantVacataire.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(getSelectedObject());
			page.setRepartService(getRepartService());
			return page;
		} else {
			return null;
		}
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults ajouterAction() {
		faireBouclerFicheServiceSiNecessaire(getDemande());
		
		CktlPecheFormServiceEnseignantVacataire page = (CktlPecheFormServiceEnseignantVacataire) pageWithName(CktlPecheFormServiceEnseignantVacataire.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setRepartService(getRepartService());
		return page;
	}

	/**
	 * Action générique de modification.
	 *
	 * @return une instance du formulaire en mode modification.
	 * @throws Exception 
	 */
	public WOActionResults deleteAction() throws Exception {
		try {
			if (getSelectedObject() != null) {
				// S'il y a des heures prévisionnelles et qu'on n'est plus dans le circuit prévisionnel, on ne peut pas supprimer
				if ((getSelectedObject().heuresPrevues() != 0) && (isCircuitDefinitif())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				
				// S'il y a des heures payées, on ne peut pas supprimer
				if (getSelectedObject().heuresPayees() != null) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION_PAYEES);
					return null;
				}
				
				// S'il y a des paiements (en attente ou payé), on ne peut pas supprimer
				if (!getSelectedObject().toListeMisesEnPaiement().isEmpty()) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION_PAIEMENT);
					return null;
				}
				
				faireBouclerFicheServiceSiNecessaire(getDemande());
				
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				getService().majDonnesServiceReparti(edc(), 
						getPecheSession().getApplicationUser().getPersonne(), 
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				controleur.recalculerIndicateurValidationAuto(getRepartService());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
			throw(e);
		}
		AjaxUpdateContainer.updateContainerWithID("serviceHETDvacataire", context());
		return doNothing();
	}

	/**
	 * 
	 * @return sous total heures prévues
	 */
	public Double getSousTotalHeuresPrevues() {
	    return controleur.getTotalHeuresPrevues(listerServiceDetailParRepartService());	
	}

	/**
	 * 
	 * @return sous total heures réalisées
	 */
	public Double getSousTotalHeuresRealisees() {
		return controleur.getTotalHeuresRealisees(listerServiceDetailParRepartService());
	}

	/**
	 * 
	 * @return sous total heures à payer
	 */
	public BigDecimal getSousTotalHeuresAPayer() {
		return controleur.getTotalHeuresAPayer(listerServiceDetailParRepartService(), getServiceOngletEnseignantVacataire().recupererStructureOngletSelectionne(getOngletComposanteSelectionne()));
	}

	/**
	 * 
	 * @return sous total heures payees
	 */
	public BigDecimal getSousTotalHeuresPayees() {
		return controleur.getTotalHeuresPayees(listerServiceDetailParRepartService(), getServiceOngletEnseignantVacataire().recupererStructureOngletSelectionne(getOngletComposanteSelectionne()));
	}


	
	/**
	 * 
	 * @return sous total heures restantes
	 */
	public BigDecimal getSousTotalHeuresRestantes() {
		return getControleur().getDeltaHeuresPayeEtRealisePresentiel(getSousTotalHeuresPayees(), getSousTotalHeuresRealisees());
	}

	
	/**
	 * 
	 * @return demande en cours
	 */
	public EODemande getDemande() {
		return controleur.getDemande(getRepartService());
	}
	
	
	/**
	 * Boutons du service actifs si la demande est sur l'étape validation du répartiteur
	 * @return vrai/faux
	 */
	@Override
	public boolean getBoutonsInactifsRepartitionService() {
		return getBoutonsInactifsRepartitionService(getDemande()) || super.getBoutonsInactifsRepartitionService();
	}

	/**
	 * @return the boutonsInactifsModifierRepartitionService
	 */
	@Override
	public boolean boutonInactifModifierRepartitionService() {
		//Si la personne n'est pas de la composante mais a le droit de saisir les heures à payer (ex DRH)
		// => on autorise la modification
		return getBoutonInactifModifierRepartitionService(getDemande())	|| super.boutonInactifModifierRepartitionService();
	}

	/**
	 * Boutons du reh actifs si la demande est sur l'étape validation du répartiteur
	 * @return vrai/faux
	 */
	@Override
	public boolean getBoutonsInactifsRepartitionReh() {
		return getBoutonsInactifsRepartitionService(getDemande()) || super.getBoutonsInactifsRepartitionReh();
	}

	/**
	 * @return the boutonsInactifsModifierRepartitionReh
	 */
	@Override
	public boolean boutonInactifModifierRepartitionReh() {
		//Si la personne n'est pas de la composante mais a le droit de saisir les heures à payer
		// => on autorise la modification
		return getBoutonInactifModifierRepartitionService(getDemande()) || super.boutonInactifModifierRepartitionReh();
	}
	

}
