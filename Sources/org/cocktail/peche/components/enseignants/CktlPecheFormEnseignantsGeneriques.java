package org.cocktail.peche.components.enseignants;

import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOEnseignantGenerique;
import org.cocktail.peche.entity.EOService;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;

/**
 * Classe permettant d'ajouter ou modifier un enseignant générique.
 */
public class CktlPecheFormEnseignantsGeneriques extends PecheCreationForm<EOService> {

	private static final long serialVersionUID = 7381067561347768090L;
	
	private WODisplayGroup displayGroupSrch;
	private IPersonne selectedPersonne;
	
	private Boolean isTypeStatutaire;
	private Boolean isDejaRecherche;
	private EOStructure currentStructure;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'exécution.
	 */
	public CktlPecheFormEnseignantsGeneriques(WOContext context) {
		super(context);
		displayGroupSrch = new WODisplayGroup();
		isDejaRecherche = false;
	}
	
	/**
	 * @return fenetre de séléction des individus de Grhum
	 */
	public WOActionResults openPopupWindow() {
		if ((editedObject().toEnseignantGenerique().nom() == null) || (editedObject().toEnseignantGenerique().nom().trim().length() < 3)) {
			getPecheSession().addSimpleErrorMessage(Constante.GENERIQUE_CONTROLE_LONGUEUR_NOM);
			return null;
    	}
		
		getDisplayGroupSrch().setObjectArray(rechercherListeIndividus());
		CktlAjaxWindow.open(context(), getInfosPopupWindowId());
		editedObject().toEnseignantGenerique().setNom(null);
		return null;
	}
	
	/**
	 * @return liste des individus valides avec un filtre sur le nom
	 */
	public NSArray<EOIndividu> rechercherListeIndividus() {
		return EOIndividu.fetchAllValides(edc(), EOIndividu.NOM_AFFICHAGE.contains(editedObject().toEnseignantGenerique().nom()), null);
	}
	
	public WODisplayGroup getDisplayGroupSrch() {
		if (displayGroupSrch == null) {
			displayGroupSrch = new WODisplayGroup();
		}
		displayGroupSrch.setDelegate(new DgDelegate());
		return displayGroupSrch;
	}	
	
	/**
	 * @return action de séléction des individus
	 */
	public WOActionResults doSelection() {
		isDejaRecherche = true;
		if (getSelectedPersonne() != null) {
			editedObject().toEnseignantGenerique().setNom((((EOIndividu)getSelectedPersonne()).nomAffichage()));
			editedObject().toEnseignantGenerique().setPrenom((((EOIndividu)getSelectedPersonne()).prenomAffichage()));
		}
		
		return null;
	}
	
	/**
	 * Individu vient de Grhum? 
	 * @return Vrai/Faux
	 */
	public boolean vientDeGrhum() {
		if ((editedObject().toEnseignantGenerique().persID() != null) || isDejaRecherche) {
			return true;
		}
		
		return false;
	}
	
	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		controlerSaisie();
		
		if (modeEdition == ModeEdition.CREATION) {
			editedObject().setAnnee(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			editedObject().majDonnesAuditCreation(getPecheSession().getApplicationUser().getPersonne());
			editedObject().toEnseignantGenerique().majDonnesAuditCreation(getPecheSession().getApplicationUser().getPersonne());
			editedObject().toEnseignantGenerique().setEnseignantStatutaire(getIsTypeStatutaire());
		}
		
		if (selectedPersonne != null && selectedPersonne.persId() != null) {
			editedObject().toEnseignantGenerique().setPersID(selectedPersonne.persId());
		}
		
		editedObject().majDonnesAuditModification(getPecheSession().getApplicationUser().getPersonne());
		editedObject().toEnseignantGenerique().majDonnesAuditModification(getPecheSession().getApplicationUser().getPersonne());
	}
	
	/**
	 * Contrôler la saisie utilisateur.
	 * 
	 * Lève une exception si un contrôle est KO.
	 */
	private void controlerSaisie() throws ValidationException {
		//On vérifie que les heures sont supérieures à 0
		if (editedObject().toEnseignantGenerique().heuresPrevues() <= 0) {
			throw new ValidationException(Constante.HEURES_INFERIEURES_A_ZERO);
		}
	}

	@Override
	protected WOActionResults pageGestion() {
		CktlPecheEnseignantsGeneriques page = (CktlPecheEnseignantsGeneriques) pageWithName(CktlPecheEnseignantsGeneriques.class.getName());
		page.setTypeStatutaire(getIsTypeStatutaire());
		page.setSelectedItemId(getSelectedItemId());
		return page;
	}
	
	@Override
	protected EOService creerNouvelObjet() {
		
		EOService unService = null;
		if (getIsTypeStatutaire()) {
			unService = EOService.creerEtInitialiserAvecRepart(edc(), getPecheSession().getApplicationUser().getPersonne(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		} else {
			unService = EOService.creerEtInitialiser(edc());
		}
		unService.setTemoinValide(Constante.OUI);
		unService.setToEnseignantGenerique(EOEnseignantGenerique.creerEtInitialiser(edc()));
		unService.setTemoinEnseignantGenerique(Constante.OUI);    //TODO : voir si vraiment utile?

		return unService;
	}

	@Override
	protected String getFormTitreCreationKey() {
		String titre = null;
		if (getIsTypeStatutaire()) {
			titre = Messages.ENSEIGNANT_GENERIQUE_FORM_CREATION_STATUTAIRE;
		} else {
			titre = Messages.ENSEIGNANT_GENERIQUE_FORM_CREATION_VACATAIRE;
		}
		return titre;
	}

	@Override
	protected String getFormTitreModificationKey() {
		String titre = null;
		if (getIsTypeStatutaire()) {
			titre = Messages.ENSEIGNANT_GENERIQUE_FORM_MODIFICATION_STATUTAIRE;
		} else {
			titre = Messages.ENSEIGNANT_GENERIQUE_FORM_MODIFICATION_VACATAIRE;
		}
		return titre;
	}

	public String getInfosPopupWindowId() {
        return getComponentId() + "_infosPopupWindowId"; 
    }
	
	/**
	 * 
	 * @return List<IStructure>
	 */
	public List<IStructure> getListeStructures() {
		if (getIsTypeStatutaire()) {
			return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
		} else {
			return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire();
		}
	}
	
	
	/* ========== Getters & Setters ========== */

	@Override
	public String getSelectedItemId() {
		if (getIsTypeStatutaire()) {
			return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_STAT.getId());
		} else {
			return valueForStringBinding("selectedItemId", PecheMenuItem.ENSEIGNANTS_GENERIQUES_VAC.getId());
		}
	}

	public Boolean getIsTypeStatutaire() {
		return isTypeStatutaire;
	}

	public void setIsTypeStatutaire(Boolean isTypeStatutaire) {
		this.isTypeStatutaire = isTypeStatutaire;
	}

	public EOStructure getCurrentStructure() {
		return currentStructure;
	}

	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	public IPersonne getSelectedPersonne() {
        return selectedPersonne;
    }
    
    public void setSelectedPersonne(IPersonne personne) {
    	selectedPersonne = personne;
    }
	
    /**
     * Classe de délégation
     */
	public class DgDelegate {
		public void displayGroupDidChangeSelection(WODisplayGroup group) {
			setSelectedPersonne((IPersonne) group.selectedObject());
		}
	}

}