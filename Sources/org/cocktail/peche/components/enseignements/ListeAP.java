package org.cocktail.peche.components.enseignements;

import java.util.List;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.controlers.UECtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * Classe permettant d'afficher les AP d'un EC/UE
 *
 * @author Chama LAATIK
 *
 */
public class ListeAP extends CktlPecheTableComponent<EOLienComposer> {

	private static final long serialVersionUID = -4616587114697743891L;

	private CktlTreeTableNode<Noeud> editedObject;
	private EOComposant parentAP;
	
	private boolean isModifiable;
	
	private boolean modeValidationParUeEc;
	private String typeFicheAValider;

	private EOArrayDataSource aPsDataSource = null;
	private ERXDisplayGroup<EOLienComposer> displayGroupLienAPs = null;
	
	private UECtrl controleur;
	
	private String selectedItemId;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public ListeAP(WOContext context) {
        super(context);
        this.controleur = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
    }

	/**
	 * Récupère les lienComposer avec les enfants de type AP
	 * @return lienComposer
	 */
	private NSArray<EOLienComposer> getLienComposerEnfantsTypeAP() {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOComposant.ID_KEY, getIDParrentAP()),
						ERXQ.equals(EOComposant.TYPE_COMPOSANT_KEY, getTypeComposantParentAP())
				);
		EOComposant parent = EOComposant.fetchSco_Composant(edc(), qualifier);
		
		NSMutableArray<EOLienComposer> lientmp = new NSMutableArray<EOLienComposer>();
		NSArray<EOLienComposer> lienComposants = parent.liensComposerAvecLesComposantsEnfants(EOTypeComposant.typeAP(edc()));
		
		
		NSComparator nsComparatorlienComposants = new NSComparator() {		
			@Override
			public int compare(Object lien0, Object lien1) {
				return ((EOLienComposer) lien0).child().code().compareTo(((EOLienComposer) lien1).child().code());
			}
		};
		
		try {
			lienComposants = lienComposants.sortedArrayUsingComparator(nsComparatorlienComposants);
		} catch (ComparisonException e) {
			// exception non traitée
		}
		
		for (EOLienComposer lien : lienComposants) {
			if (lien.child().composantInfoVersion().annee().equals(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())) {
				lientmp.add(lien);
			}
		}
		
		return lientmp.immutableClone();
	}

	/**
	 * Affiche le libellé du parent de l'AP
	 * @return libellé
	 */
	public String getAffichageParentAP() {
		if (getEditedObject() != null) {
			return getEditedObject().getData().getLibelle();
		} else if (getParentAP() != null) {
			return ComposantAffichageHelper.getInstance().affichageEnseignement(getParentAP());
		}
		return "";
	}

	/**
	 * Renvoie le nombre de groupe de l'AP sil est est connu
	 * @return nombre de groupe
	 */
	 public String getNbGroupeAP() {
    	if (getCurrentItem() != null) {
    		EOAP composantAP = (EOAP) getCurrentItem().child();
    		
    		IChargeEnseignement chargeEnseignement = controleur.getChargeEnseignement(composantAP);
    		
	    	if (chargeEnseignement != null && chargeEnseignement.nbGroupes() != null) {
	    		return chargeEnseignement.nbGroupes().toString();
	    	}
    	}

    	return Constante.NON_DEFINI;
    }

	/**
	 * Renvoie le code du type de l'AP
	 * 
	 * @return code
	 */
	public String getChildItemTypeAP() {
		EOAP composantAP = (EOAP) getCurrentItem().child();
		return composantAP.typeAP().code();
	}

	/**
	 * Renvoie le nombre d'heures de la maquette de l'AP
	 * 
	 * @return nombre d'heures
	 */
	public Float getChildItemNbHeuresAP() {
		EOAP composantAP = (EOAP) getCurrentItem().child();

		IChargeEnseignement chargeEnseignement = controleur.getChargeEnseignement(composantAP);
		
		if (chargeEnseignement != null && chargeEnseignement.valeurHeures() != null) {
			return chargeEnseignement.valeurHeures().floatValue();
		}

		return 0.0f;
	}

	/**
	 * Modifie le nombre de groupe initial de l'AP
	 * @return nombre de groupe surchargé
	 */
	public WOActionResults surchargerNbGroupeAP() {
		 if (getSelectedObject() != null) {
			APSurchargeNbGroupeForm page = (APSurchargeNbGroupeForm) pageWithName(APSurchargeNbGroupeForm.class.getName());
			page.setTypeFiche(getTypeFiche());			
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setSelectedItemId(getSelectedItemId());
			page.setEditedObject(getSelectedObject());			
			return page;
		}
		 return null;
	}

	/**
	 * Renvoie les AP du composant édité
	 * @return display Group
	 */
	public ERXDisplayGroup<EOLienComposer> getDisplayGroupLienAPs() {
		if (displayGroupLienAPs == null) {
			displayGroupLienAPs = new ERXDisplayGroup<EOLienComposer>();
			getAPsDataSource().setArray(getLienComposerEnfantsTypeAP());
			displayGroupLienAPs.setDataSource(getAPsDataSource());
			displayGroupLienAPs.setDelegate(this);
			displayGroupLienAPs.setSelectsFirstObjectAfterFetch(true);
			displayGroupLienAPs.fetch();
		}
		return displayGroupLienAPs;
	}

	private EOArrayDataSource getAPsDataSource() {
		if (aPsDataSource == null) {
			aPsDataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOLienComposer.class), edc());
		}
		return aPsDataSource;
	}

	
	/**
	 * Renvoie le parent de l'AP
	 * @return l'id du parent de l'AP
	 */
	public int getIDParrentAP() {
		if (parentAP != null) {
			return parentAP.id();
		}
		return getEditedObject().getData().getId();
	}
	
	
	/**
	 * Renvoie le parent de l'AP
	 * @return le code du parent de l'AP
	 */
	public String getCodeParentAP() {
		if (parentAP != null) {
			return ComposantAffichageHelper.getInstance().affichageCode(parentAP);
		}
		return getEditedObject().getData().getCode();
	}

	public EOComposant getParentAP() {
		return parentAP;
	}

	public void setParentAP(EOComposant parentAP) {
		this.parentAP = parentAP;
	}

	public CktlTreeTableNode<Noeud> getEditedObject() {
		return editedObject;
	}

	public void setEditedObject(CktlTreeTableNode<Noeud> selectedNode) {
		this.editedObject = selectedNode;
	}

	public boolean getAffichageSurchargeGroupe() {
		return !(getAutorisation().hasDroitUtilisationParametrageNbGroupe() 
				|| getAutorisation().hasDroitUtilisationParametrageNbHeureMaquette()) || getSelectedObject() == null;
	}
	
	public float getNbHeuresRepartition() {
		return controleur.getTotalHeuresPrevues((EOAP) getCurrentItem().child(), false);
	}
	
	public float getNbHeuresRealisees() {
		return controleur.getTotalHeuresRealisees((EOAP) getCurrentItem().child(), false);
	}
	
	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		return controleur.getNbHeuresResteARepartir((EOAP) getCurrentItem().child());
	}
	
	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser() {
		return controleur.getNbHeuresResteARealiser((EOAP) getCurrentItem().child());
	}
	
	
	/**
	 * @return total du nombre d'heures maquette
	 * 	==> On ne prend pas en compte le nombre de parent pour les AP mutualisés
	 */
	public float getTotalNbHeuresMaquette() {
		return controleur.getHeuresMaquette((EOAP) getCurrentItem().child(), false);
	}
	
	
	/**
	 * @return page précédente
	 */
	public WOActionResults annuler() {
		WOComponent pageRetour = (WOComponent) this.session().objectForKey("retourOffreDeFormation");
		if (pageRetour != null) {
			return pageRetour;
		}
		return null;
	}

	@Override
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	} 

	/**
	 * Renvoie le parent de l'AP
	 * @return le code du parent de l'AP
	 */
	public ITypeComposant getTypeComposantParentAP() {
		if (parentAP != null) {
			return parentAP.typeComposant();
		}
		return getEditedObject().getData().getTypeComposant();
	} 


	public boolean getDroitsSurParentAP() {
		if (getEditedObject() == null || getEditedObject().getData() == null ) {
			return true;
		}
		EOQualifier qual = EOComposant.ID.eq(getEditedObject().getData().getId());
		EOComposant parentAP = EOComposant.fetchSco_Composant(this.edc(), qual);
		
		
		EOStructure structureComposant = null;
		NSArray<EOStructure> listeStructure = null;
		if (parentAP != null) {
			listeStructure = parentAP.structures();
		}
		if (listeStructure.size() >	 0) {
			for (EOStructure structure : listeStructure) {
				structureComposant = structure;
			}
		} else {       
			List<IComposant> composantsParents = parentAP.parents();
			for (IComposant composant : composantsParents) {
				List<IStructure> listeStructure2 = (List<IStructure>)composant.structures();
				if (listeStructure2.size() > 0) {
					for (IStructure structure : listeStructure2) {
						structureComposant = (EOStructure) structure;
					}
				}
			}
		}
		
		if (structureComposant == null) {
			this.setModifiable(true);
			return true;
		} else {
			EOIndividu individuConnecte = getPecheSession().getApplicationUser().getIndividu();
			NSArray<EOStructure> composantesIndividuConnecte = individuConnecte.getServices();
			for (EOStructure structure : composantesIndividuConnecte) {
				if (structureComposant.cStructure().equals(structure.cStructure())) {
					this.setModifiable(true);
					return true;
				}
			}
		}
		this.setModifiable(false);
		return false;
	}

	public boolean isModifiable() {
		return isModifiable;
	}

	public void setModifiable(boolean isModifiable) {
		this.isModifiable = isModifiable;
	}
	
	public boolean ismodificationListeAP() {
		return (!getAffichageSurchargeGroupe() && isModifiable());
	}

	public boolean isModeValidationParUeEc() {
		return modeValidationParUeEc;
	}

	public void setModeValidationParUeEc(boolean modeValidationParUeEc) {
		this.modeValidationParUeEc = modeValidationParUeEc;
	}

	public String getTypeFicheAValider() {
		return typeFicheAValider;
	}

	public void setTypeFicheAValider(String typeFicheAValider) {
		this.typeFicheAValider = typeFicheAValider;		
	}
	
}
