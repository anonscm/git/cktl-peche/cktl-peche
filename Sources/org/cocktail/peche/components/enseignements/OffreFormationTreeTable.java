package org.cocktail.peche.components.enseignements;

import java.util.ArrayList;
import java.util.Collection;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.peche.Session;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Composant arbre "Offre de formation".
 * 
 * @author Pascal MACOUIN
 */
public class OffreFormationTreeTable extends WOComponent {
	/** Numéro de série. */
	private static final long serialVersionUID = -1444855378739306454L;
	
	private CktlTreeTableNode<Noeud> currentNode;
	private CktlTreeTableNode<Noeud> selectedNode;
	private CktlTreeTableNode<Noeud> rootNode;
	
	private Collection<String> listeCodesAP = new ArrayList<String>();
	private String currentCodeAP;

	private boolean colonneRepartitionAffichee = true;
	private boolean activerLien = true;
	private String updateContainerID;
	//Point d'entrée pour la liste AP (offre de formation ou UE flottante)
	private String menuSelectionne;
	private String typeFiche;
	
	/**
	 * Constructeur.
	 * @param context le contexte WO
	 */
    public OffreFormationTreeTable(WOContext context) {
        super(context);
    }


    /**
     * Retourne true si c'est une EC.
     * @return true si EC
     */
    public boolean isEC() {
		if (getCurrentNode().getData().isParenAP()) {
    		return true;
    	}
    	return false;
    }
    
    
    /**
     * Retourne la page de l'EC
     * @return la page
     */
    public WOActionResults getLibelleAction() {
    		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
			page.setEditedObject(currentNode);			
			page.setSelectedItemId(getMenuSelectionne());
			page.setTypeFiche(getTypeFiche());
			session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
			
			return page;
    }
    
    
	public CktlTreeTableNode<Noeud> getCurrentNode() {
		return currentNode;
	}

	public void setCurrentNode(CktlTreeTableNode<Noeud> currentNode) {
		this.currentNode = currentNode;
	}

	public CktlTreeTableNode<Noeud> getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CktlTreeTableNode<Noeud> selectedNode) {
		this.selectedNode = selectedNode;
	}

	public CktlTreeTableNode<Noeud> getRootNode() {
		return rootNode;
	}

	public void setRootNode(CktlTreeTableNode<Noeud> rootNode) {
		this.rootNode = rootNode;
	}

	public Collection<String> getListeCodesAP() {
		return listeCodesAP;
	}

	public void setListeCodesAP(Collection<String> listeCodesAP) {
		this.listeCodesAP = listeCodesAP;
	}

	public String getCurrentCodeAP() {
		return currentCodeAP;
	}

	public void setCurrentCodeAP(String currentCodeAP) {
		this.currentCodeAP = currentCodeAP;
	}

	public boolean isColonneRepartitionAffichee() {
		return colonneRepartitionAffichee;
	}

	public void setColonneRepartitionAffichee(boolean colonneRepartitionAffichee) {
		this.colonneRepartitionAffichee = colonneRepartitionAffichee;
	}

	public String getContainerBoutons() {
		return (String) valueForKey("updateContainerID");
	}
	
	public String getUpdateContainerID() {
		return updateContainerID;
	}

	public void setUpdateContainerID(String updateContainerID) {
		this.updateContainerID = updateContainerID;
	}

	/**
	 * @return Le nombre de colonne affichées par AP
	 */
	public int getNbColonnesAfficheesParAp() {
		int nbColonnes = 1;
		
		if (colonneRepartitionAffichee) {
			nbColonnes = 3;
		}
		
		return nbColonnes;
	}

	public String getMenuSelectionne() {
		return menuSelectionne;
	}

	public void setMenuSelectionne(String menuSelectionne) {
		this.menuSelectionne = menuSelectionne;
	}
	
	public String getSrcImage() {
		if (getCurrentNode().getData().getTypeComposant() != null) {
			return ((EOTypeComposant)getCurrentNode().getData().getTypeComposant()).srcImage();
		}
		return "images/Matrice/default.png";
	}


	public boolean isActiverLien() {
		return activerLien;
	}


	public void setActiverLien(boolean activerLien) {
		this.activerLien = activerLien;
	}
	
	@Override
	public void awake() {
		super.awake();
		
		if (hasBinding("activerLien")) {
			activerLien = false;
		} else {
			activerLien = true;
		}
		
		
	}


	public String getTypeFiche() {
		return typeFiche;
	}


	public void setTypeFiche(String typeFiche) {
		this.typeFiche = typeFiche;
	}
}