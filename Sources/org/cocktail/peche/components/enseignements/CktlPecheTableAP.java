package org.cocktail.peche.components.enseignements;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.OffreFormationCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Table qui renvoie la liste des UE
 * 
 * @author Equipe PECHE
 */
public class CktlPecheTableAP extends CktlAbstractPecheTableAP {

	private static final long serialVersionUID = 1367194801568058732L;

	private EOService exclureService;

	private EnseignantsStatutairesCtrl controleurStatutaire;

	private OffreFormationCtrl offreFormationCtrl;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'accès à la base de données.
	 */
	public CktlPecheTableAP(WOContext context) {
		super(context);
	}

	/**
	 * Renvoie le nombre d'heures souhaitées par l'enseignant statutaire pour chaque AP, 0 sinon
	 * @return nombre d'heures souhaitées
	 */
	public double getNbHeuresSouhaitees() {
		double nbHeuresSouhaitees = 0.0;

		if (isNbHeuresSouhaiteesAffichee()) {
			nbHeuresSouhaitees = controleurStatutaire.getNbHeuresSouhaiteesParAP(getEnseignant(), getCurrentItem());
		}

		return nbHeuresSouhaitees;
	}

	public boolean isNbHeuresSouhaiteesAffichee() {
		return ((getEnseignant() != null) && (getEnseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) && !isCircuitDefinitif());
	}

	public EOActuelEnseignant getEnseignant() {
		return (EOActuelEnseignant) valueForBinding("enseignant");
	}

	public boolean isStatutaire() {
		return valueForBooleanBinding("isStatutaire", false);
	}

	public boolean isCircuitDefinitif() {
		Boolean definitif = (Boolean) valueForBinding("circuitDefinitif");
		if (definitif != null) {
			return definitif;
		}
		return false;
	}

	@Override
	public void awake() {
		super.awake();

		if (hasBinding("exclureService")) {
			exclureService = (EOService) valueForBinding("exclureService");
		} else {
			exclureService = null;
		}

	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		EOQualifier qualifier;

		this.controleurStatutaire = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		offreFormationCtrl = new OffreFormationCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());

		List<IStructure> listeStructures = construireListeStructures();
		List<IStructure> listeUEFs = getPecheSession().getPecheAutorisationCache().getListeStructAdditionnellesUEFlottantes();
		
		if (listeStructures != null) {
			NSArray<Integer> listeAPaExclure = new NSMutableArray<Integer>();
			if (exclureService != null) {
				listeAPaExclure = listeAPServiceDetails();				
			}
			if (!listeAPaExclure.isEmpty()) {
				qualifier = ERXQ.and(EOAP.ID.in(offreFormationCtrl.getAPsAnnee(listeStructures,listeUEFs)),
						EOAP.ID.notIn(listeAPaExclure));				
			} else {			
				qualifier = EOAP.ID.in(offreFormationCtrl.getAPsAnnee(listeStructures,listeUEFs));
			}

			ERXSortOrderings sortOrderings = EOAP.TAG_APPLICATION.asc()
					.then(EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.CODE).asc());
			fetchDisplay(qualifier, EOAP.ENTITY_NAME, sortOrderings);
		}
		super.appendToResponse(response, context);
	}

	/**
	 * 
	 * @return
	 */
	private List<IStructure> construireListeStructures() {
		List<IStructure> listeStructures = new ArrayList<IStructure>();        	
		if (isStatutaire() || (getEnseignant() != null && getEnseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			listeStructures.addAll(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementStatutaires());
		} else {
			listeStructures.addAll(getPecheSession().getPecheAutorisationCache().getListeCibleEnseignementVacataire());
		}
		return listeStructures;
	}

	private NSArray<Integer> listeAPServiceDetails() {
		NSArray<EOServiceDetail> listeServiceDetails = new NSMutableArray<EOServiceDetail>();
		NSArray<Integer> listeAPs = new NSMutableArray<Integer>();

		listeServiceDetails = EOServiceDetail.fetchEOServiceDetails(edc(), getQualifierAPPourServiceDetail(
				getEnseignant().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())), null);

		for (EOServiceDetail serviceDetail : listeServiceDetails) {
			if (serviceDetail.composantAP() != null) {
				listeAPs.add(serviceDetail.composantAP().id());
			}
		}
		return listeAPs;
	}

	private EOQualifier getQualifierAPPourServiceDetail(EOService service) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOServiceDetail.SERVICE_KEY, service),
						ERXQ.isNotNull(EOServiceDetail.COMPOSANT_AP_KEY)
						);

		return qualifier;
	}
}