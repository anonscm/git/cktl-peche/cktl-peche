package org.cocktail.peche.components.enseignements;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.UECtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * 
 * @author anthony
 */
public class CktlAbstractPecheTableAP extends CktlPecheTableComponent<EOAP> {

	private static final long serialVersionUID = 7512949710848409786L;
	private String tokenCode;
	private String tokenLibelle;
	private String tokenType;

	private UECtrl ueControleur;

	private String containerParent;

	private BasicPecheCtrl basicControleur;

	private Map<String, IComposant> mapComposantParentCache = new HashMap<String, IComposant>();
	private Map<String, String> mapLieuAPCache = new HashMap<String, String>();

	public CktlAbstractPecheTableAP(WOContext context) {
		super(context);
		this.ueControleur = new UECtrl(this.edc(), getPecheSession().getPecheParametres()
				.getAnneeUniversitaireEnCours());

	}

	@Override
	public void awake() {
		super.awake();

		// permet de mettre à jour un élement du parent
		if (hasBinding("containerParent")) {
			setContainerParent((String) valueForBinding("containerParent"));
		}
	}

	@Override
	public void setSelectedObject(EOAP selectedObject) {
		super.setSelectedObject(selectedObject);
		getControleur().setSelectedAP(selectedObject);
	}

	/**
	 * Filtre la liste avec les filtres des en-têtes de colonne.
	 * 
	 * @return <code>null</code>, on recharge le container de la liste
	 */
	public WOActionResults filtrer() {
		EOQualifier matchingCodeOrLibelle = null;
		EOQualifier matchingLibelle = null;
		EOQualifier matchingType = null;
		EOQualifier tagMatchingPeche = null;

		if (!StringCtrl.isEmpty(tokenCode)) {
			EOQualifier matchingCode = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.CODE).contains(tokenCode);
			EOQualifier libelleMatchingCode = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.LIBELLE).contains(tokenCode);
			tagMatchingPeche = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.TAG_APPLICATION_KEY).contains(Constante.APP_PECHE);
			//Dans le cas d'une UE Flottante (Parent taggé PECHE) le code de l'UE a été généré donc on filtre sur le libellé et pas sur le code.
			matchingCodeOrLibelle = ERXQ.or(matchingCode, ERXQ.and(libelleMatchingCode, tagMatchingPeche));
		}

		if (!StringCtrl.isEmpty(tokenLibelle)) {
			matchingLibelle = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.LIBELLE).contains(tokenLibelle);
		}

		if (!StringCtrl.isEmpty(tokenType)) {
			matchingType = EOAP.TYPE_AP.dot(EOTypeAP.CODE).contains(tokenType);
		}

		filtrerDisplayGroup(ERXQ.and(matchingCodeOrLibelle, matchingLibelle, matchingType));

		return doNothing();
	}

	public IComposant getComposantParent() {
		EOAP currentItem = getCurrentItem();
		String currentItemCode = currentItem.code();
		IComposant composantParent = mapComposantParentCache.get(currentItemCode);
		if (composantParent != null) {
			return composantParent;
		}
		if (currentItem.parents() != null && currentItem.parents().size() > 0) {
			composantParent = currentItem.parents().get(0);
			mapComposantParentCache.put(currentItemCode, composantParent);
		}
		return composantParent;
	}

	/**
	 * Renvoie le lieu (composante / Etablissement externe) du parent de l'AP
	 * 
	 * @return lieu
	 */
	public String getLieuAP() {
		String lieuAP = null;
		IComposant composantParent = getComposantParent();

		if (composantParent == null) {
			return "INCONNU";
		}

		lieuAP = mapLieuAPCache.get(composantParent.code());
		if (lieuAP != null) {
			return lieuAP;
		}

		if ((composantParent.tagApplication() != null)
				&& (composantParent.tagApplication().equals(Constante.APP_PECHE))) {
			lieuAP = ueControleur.getEtablissementExterne(composantParent);

			// Si l'UE flottante se déroule dans l'établissement, récupère la
			// composante de l'UE
			if (lieuAP == null) {
				if (composantParent.structures().isEmpty()) {
					lieuAP = "";
				} else {
					lieuAP = composantParent.structures().get(0).lcStructure();
				}
			}
		} else {
			if (composantParent.structures().isEmpty()) {
				lieuAP = "";
			} else {
				lieuAP = composantParent.structures().get(0).lcStructure();
			}
		}
		mapLieuAPCache.put(composantParent.code(), lieuAP);
		return lieuAP;
	}

	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		return ueControleur.getNbHeuresResteARepartir(getCurrentItem());
	}

	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser() {
		return ueControleur.getNbHeuresResteARealiser(getCurrentItem());
	}

	public float getHeuresMaquette() {
		return this.ueControleur.getHeuresMaquette(getCurrentItem(), false);
	}

	/**
	 * @return le libellé du parent de l'atome pédagogique si c'est une UE Flottante.
	 */
	public String getCodeParent() {
		IComposant composantParent = getComposantParent();
		return ComposantAffichageHelper.getInstance().affichageCode(composantParent);
	}
	
	/**
	 * @return le libellé du parent de l'atome pédagogique si c'est une UE Flottante.
	 */
	public String tagApplication() {
		IComposant composantParent = getComposantParent();
		if(composantParent == null){
			return "IN";
		}
		return composantParent.tagApplication();
	}

	/**
	 * 
	 * @return
	 */
	public String tooltipTexteMutualise() {
		return "Mutualisé";
	}

	/**
	 * 
	 * @return true/false
	 */
	public boolean isMutualise() {
		if (getComposantParent() != null) {
			return getComposantParent().isMutualise();
		}
		return false;
	}

	/**
	 * @return Le contrôleur de la page appelante
	 */
	public BasicPecheCtrl getControleur() {
		basicControleur = (BasicPecheCtrl) valueForBinding("controleur");

		if (basicControleur == null) {
			throw new NullPointerException("Le controleur ne peut pas être nul");
		}

		return basicControleur;
	}

	/**
	 * Renvoie le libellé du parent de l'AP
	 * 
	 * @return libellé
	 */
	public String libelleParent() {
		if (getComposantParent() != null) {
			return getComposantParent().libelle();
		}
		return "UE/EC INCONNU pour AP : " + getCurrentItem().libelle();
	}

	public String getTokenCode() {
		return tokenCode;
	}

	public void setTokenCode(String tokenCode) {
		this.tokenCode = tokenCode;
	}

	public String getTokenLibelle() {
		return tokenLibelle;
	}

	public void setTokenLibelle(String tokenLibelle) {
		this.tokenLibelle = tokenLibelle;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	/**
	 * @return the containerParent
	 */
	public String containerParent() {
		return containerParent;
	}

	/**
	 * @param containerParent
	 *            the containerParent to set
	 */
	public void setContainerParent(String containerParent) {
		this.containerParent = containerParent;
	}

	public UECtrl getControleurUE() {
		return ueControleur;
	}
}
