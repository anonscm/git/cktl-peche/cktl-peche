package org.cocktail.peche.components.enseignements;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.EnseignantDataBean;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiement;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe permettant d'ajouter ou modifier les affectations d'un enseignant pour un UE/EC
 * 
 * @author Chama LAATIK
 */
public class RepartitionParUEForm extends PecheCreationForm<EOComposant> implements BasicPecheObservateur {
    
	private static final long serialVersionUID = -1462984154691840580L;
	
	private EnseignantDataBean enseignantBean;
	private EOComposant parentAP;
	
	private UECtrl controleurUE;
	private BasicPecheCtrl controleur;
		
	private String currentEtat;
	private List<EOAP> listeAPs;
	
	private EOServiceDetail currentServiceDetail;
	private NSArray<EOServiceDetail> listeServiceDetail;
	private NSArray<EOServiceDetail> listeServiceDetailDejaCree;
	private NSArray<EOServiceDetail> listeServiceDetailCree;
	
	private String commentaire;
	
	private IStructure structureReferente;
	private EOStructure currentStructureReferente;
	
	private String selectedItemId;
 
	
	private List<EOPeriode> listePeriode;
	private EOPeriode currentPeriode;
	private EOPeriode selectedPeriode;
	

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'accès à la base de données.
	 */
	public RepartitionParUEForm(WOContext context) {
        super(context);
        this.controleurUE = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        
        this.controleur = new BasicPecheCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		this.controleur.addObservateur(this);
		
		// initialisation de la liste des périodes
		listePeriode = EOPeriode.fetchEOPeriodes(edc(),
												ERXQ.equals(EOPeriode.TYPE_KEY, EOPecheParametre.getChoixTypePeriode(edc())
												+ getPecheSession().getPecheParametres().getCodeFormatAnnee()),
												EOPeriode.ORDRE_AFFICHAGE.ascs());		
		
    }

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		
		controlerSaisie();
		
		EOService service = recupererService();
		
		List<EOServiceDetail> listeServiceDetailToDelete = new ArrayList<EOServiceDetail>();
		
		for (EOServiceDetail serviceDetail : getListeServiceDetail()) {
			if ((serviceDetail.heuresPrevues() != 0.0)  || (serviceDetail.heuresRealisees() != 0.0)) {
				modeEdition = modeEdition(serviceDetail);
				
				//On vérifie qu'on ne dépasse pas les heures maquette lors d'une répartition de service
				if (serviceDetail.composantAP() != null) {
					if (isTypeFichePrevisionnel()) {
						controleHeuresMaquettePrevues(serviceDetail);
					} else {
						controleHeuresMaquetteRealisees(serviceDetail);
					}
					
					if (!getEnseignantBean().isStatutaire() && !getEnseignantBean().isGenerique()) {
						controleSaisieHeures(serviceDetail);
					}
				}
				
				if (modeEdition == ModeEdition.CREATION) {
					serviceDetail.setService(service);					
					serviceDetail.majDonnesAuditCreation(personne);
					
					//Répartition croisée
					if ((getEnseignantBean().isStatutaire() && isRepartitionCroisee(getEnseignantBean().getEnseignant(), service))) {
						creerNouvelleRepart(serviceDetail);
						serviceDetail.setEtat(Constante.ATTENTE);
					}
					
				} else {
					serviceDetail.majDonnesAuditModification(personne);
				}
				
				if (!StringCtrl.isEmpty(getCommentaire())) {
					serviceDetail.setCommentaire(getCommentaire());
					serviceDetail.setDateCommentaire(now);
				}
				if (getEnseignantBean().isStatutaire()) {
					serviceDetail.setToRepartService(service.toListeRepartService().get(0));
				} else {
					EORepartService repartService = EORepartService.getOrCreateRepartService(edc(), service, getEnseignantBean().getStructureReferente(), 
																						determinerCodeCircuit(), getPecheSession().getApplicationUser().getPersonne(), 
																						getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
					serviceDetail.setToRepartService(repartService);
				}
				
			} else {
				listeServiceDetailToDelete.add(serviceDetail);
			}
		}
		
		for (EOServiceDetail serviceDetail : listeServiceDetailToDelete) {
			serviceDetail.editingContext().deleteObject(serviceDetail);
		}
		
		if (service != null) {
			if (service.enseignant() != null) {
				service.majDonnesServiceDuStatutaire(this.edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				service.majDonnesServiceReparti(this.edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			}
			service.setDateModification(now);
			service.setPersonneModification(personne);
		}
	}
	
	private String determinerCodeCircuit() {
		if (isTypeFichePrevisionnel()) {
			return CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit();
		} 
		return CircuitPeche.PECHE_VACATAIRE.getCodeCircuit();
	}

	/**
	 * Création d'un nouveau service pour l'enseignant si inexistant
	 * @return service de l'enseignant
	 */
	private EOService creerNouveauService() {
		EOService nouveauService = creerEtInitialiserService();
		NSTimestamp now = new NSTimestamp();
		EOPersonne personne = getPecheSession().getApplicationUser().getPersonne();
		
		if ((getEnseignantBean() != null) && (getEnseignantBean().isGenerique())) {
			nouveauService.setToEnseignantGenerique(getEnseignantBean().getEnseignantGenerique());
		} else {
			nouveauService.setEnseignant(getEnseignantBean().getEnseignant());
		}
		
		nouveauService.setPersonneCreation(personne);
		nouveauService.setDateCreation(now);
		nouveauService.setPersonneModification(personne);
		nouveauService.setDateModification(now);
		nouveauService.setTemoinEnseignantGenerique(Constante.NON);
		nouveauService.setTemoinValide(Constante.OUI);
		nouveauService.setAnnee(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		nouveauService.majDonnesServiceDuStatutaire(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		nouveauService.majDonnesServiceReparti(edc(), personne, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		return nouveauService;
	}
	
	/**
	 * Crée et intialise le service sur le bon circuit de validation en fonction du profil de l'enseignant
	 * selectionné : générique, vacataire ou statutaire
	 * @return le service intialisé
	 */
	private EOService creerEtInitialiserService() {
		EOService nouveauService = null;
		
		if ((getEnseignantBean().getEnseignant() != null) && (getEnseignantBean().getEnseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			nouveauService = EOService.creerEtInitialiser(edc());
			EORepartService.creerNouvelRepartService(edc(), nouveauService, null, getPecheSession()
					.getApplicationUser().getPersonne(), CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
			
		} else {
			nouveauService = EOService.creerEtInitialiser(edc());
			EORepartService.creerNouvelRepartService(edc(), nouveauService, null, getPecheSession()
					.getApplicationUser().getPersonne(), Constante.CIRCUIT_VACATAIRE_A_DETERMINER, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		}
		return nouveauService;
	}
	
	
	private void controleSaisieHeures(EOServiceDetail serviceDetail) {
		
		if (getListeServiceDetailDejaCree() != null && getListeServiceDetailDejaCree().contains(serviceDetail)) {
			return;
		}
		
		// Si la personne a le droit de saisir les heures à payer
		if (getAutorisation().hasDroitUtilisationSaisieHeuresAPayer()) {
			// Pas de saisie d'heures à payer si la période est à null
			if ((serviceDetail.heuresAPayer() != null) && (serviceDetail.toPeriode() == null)) {
				throw new ValidationException(Constante.REPARTITION_ERREUR_HEURES_A_PAYER_INTERDIT_SI_PERIODE_NULL);
			}

			BigDecimal nbHeuresRealisees = new BigDecimal(serviceDetail.heuresRealisees());
			BigDecimal totalNbHeures = getHeuresAPayer(serviceDetail).add(getHeuresPayees(serviceDetail));

			// Les heures réalisées doivent être supérieures au total des heures payées et à payer
			// Ainsi, les heures à payer doivent être inférieures au total des heures réalisées et des heures payées
			if (nbHeuresRealisees.compareTo(totalNbHeures) < 0) {
				throw new ValidationException(Constante.HEURES_REALISEES_INFERIEUR_TOTAL_HEURES_PAYEES_ET_HEURES_A_PAYER);
			}

			// les heures à payer doivent être supérieures aux heures en attente de paiement ==> c'est le plancher de la mise en paiement
			if (getHeuresAPayer(serviceDetail).compareTo(getHeuresEnAttentePaiement(serviceDetail)) < 0) {
				throw new ValidationException(Constante.HEURES_A_PAYER_INFERIEUR_HEURES_EN_ATTENTE);
			}
		} else { // pas le droit de saisir les heures à payer
			BigDecimal heuresEnPaiement = getHeuresEnAttentePaiement(serviceDetail).add(getHeuresPayees(serviceDetail));
			if (BigDecimal.valueOf(serviceDetail.heuresRealisees()).compareTo(heuresEnPaiement) < 0) {
				throw new ValidationException(String.format(Constante.HEURES_REALISEES_INFERIEUR_HEURES_EN_PAIEMENT, heuresEnPaiement));
			} else {
				// les heures à payer sont égales aux heures réalisées
				BigDecimal heuresAPayer = BigDecimal.valueOf(serviceDetail.heuresRealisees()).subtract(heuresEnPaiement);
				if (heuresAPayer.compareTo(BigDecimal.ZERO) <= 0) {
					heuresAPayer = null;
				}
				rechercherService().setHeuresAPayer(heuresAPayer);
			}
			
		}
	}
	
	/**
	 * Recherche le service d'un enseignant générique ou pas
	 * @return service de l'enseignant
	 */
	private EOService rechercherService() {
		EOService serviceRecherche = null;
		
		if ((getEnseignantBean() != null) && (getEnseignantBean().isGenerique())) {
			serviceRecherche = getEnseignantBean().getEnseignantGenerique().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		} else {
			serviceRecherche = getEnseignantBean().getEnseignant().getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		} 
		
		return serviceRecherche;
	}
	
	/**
	 * Renvoie soit le service de l'enseignant si connu soit crée un nouveau
	 * @return service
	 */
	private EOService recupererService() {
		EOService service = rechercherService();
		
		if (service == null) {
			service = creerNouveauService();
		}
		
		return service;
	}

	/**
	 * Récupère la liste des serviceDetail pour un AP
	 * 	=> Si connu, on récupère, sinon, on l'initialise
	 * 
	 * @return liste EOServiceDetail
	 */
	public NSArray<EOServiceDetail> getListeServiceDetail() {
		if (listeServiceDetail == null) {
			NSArray<EOServiceDetail> listeServiceDetails = new NSMutableArray<EOServiceDetail>();
			NSArray<EOServiceDetail> listeDetail = new NSMutableArray<EOServiceDetail>();
			
			if (getEnseignantBean() != null) {
				listeServiceDetails = EOServiceDetail.fetchEOServiceDetails(edc(), ERXQ.equals(EOServiceDetail.SERVICE_KEY, recupererService()), null);
			}
			
			for (EOAP ap : getListeAPs()) {
				EOServiceDetail serviceDetailTrouve = null;
				
				//on ajoute les serviceDetail si on a le meme type d'AP
				for (EOServiceDetail serviceDetail : listeServiceDetails) {
					if ((serviceDetail.composantAP() != null) && (serviceDetail.composantAP().primaryKey().equals(ap.primaryKey()))) {
						serviceDetailTrouve = serviceDetail;
						break;
					}
				}
			
				//si serviceDetail non trouvé, on le crée
				if (serviceDetailTrouve == null) {
					serviceDetailTrouve = EOServiceDetail.creerEtInitialiser(edc());
					serviceDetailTrouve.setComposantAPRelationship(ap);
				}
				
				listeDetail.add(serviceDetailTrouve);
			}
			
			setListeServiceDetail(listeDetail);
		}
		return listeServiceDetail;
	}
	
	/**
	 * Contrôler la saisie utilisateur.
	 * Lève une exception si un contrôle est ko.
	 */
	private void controlerSaisie() throws ValidationException {
		//On vérifie qu'on a selectionné un enseignant
		if (getEnseignantBean() == null) {
			throw new ValidationException(Constante.REPARTITION_UE_RENSEIGNER_UN_ENSEIGNANT);
		}
		
		//On vérifie qu'on n'ajoute pas un service déjà validé sur la structure demandée
		if ((getEnseignantBean().getEnseignant() != null && (isFicheValidee(getEnseignantBean().getEnseignant())))) {
			String message = String.format(Constante.REPARTITION_UE_FICHE_DEJA_VALIDEE, 
					getEnseignantBean().getStructureReferente().lcStructure());
			
			throw new ValidationException(message);
		}
	}
	
	/**
	 * Contrôle que les heuresRepartition <= heuresMaquettes
	 * Lève une exception si un contrôle est ko.
	 */
	private void controleHeuresMaquettePrevues(EOServiceDetail serviceDetail) throws ValidationException {
		float heuresRepartition = getTotalHeuresPrevues(serviceDetail.composantAP(), modeEdition(serviceDetail), serviceDetail.heuresPrevues()); 
		
		if (heuresRepartition > getHeuresMaquette(serviceDetail.composantAP())) {
			String message = String.format(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE_PARAMETRE, 
					serviceDetail.composantAP().typeAP().code());
			
			throw new ValidationException(message);
		}
	}
	
	private void controleHeuresMaquetteRealisees(EOServiceDetail serviceDetail) throws ValidationException {
		Double heuresRealisees = serviceDetail.heuresRealisees();
		if (heuresRealisees > getNbHeuresResteARealiser(serviceDetail.composantAP())) {
			String message = String.format(Constante.REPARTITION_SUPERIEUR_HEURES_MAQUETTE_PARAMETRE, 
					serviceDetail.composantAP().typeAP().code());
			
			throw new ValidationException(message);
		}
	}
	
	/**
	 * Renvoie vrai si la fiche de service de l'enseignant a déja été validée
	 * @param enseignant : enseignant de l'établissement
	 * @return vrai/faux
	 */
	public boolean isFicheValidee(EOActuelEnseignant enseignant) {
		boolean isFicheValidee = false;
		EOService service = enseignant.getService(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		if ((enseignant != null) && (service != null)) {
			//On récupère la demande qui concerne la structure demandée
			EODemande demande = controleur.getDemande(EORepartService.getRepartService(service, getEnseignantBean().getStructureReferente()));
			
			isFicheValidee = !getAutorisation().hasDroitUtilisationChemin(demande, EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER);
		}
		
		return isFicheValidee;
	}
	
	/**
	 * Renvoie le total des heures maquette de de l'AP
	 * ==> Si l'AP est mutualisé, le paramètre false permet de calculer sa charge propre sans prendre 
	 * en compte le nombre de parent 
	 * 
	 * @param ap : AP
	 * @return les heures maquette
	 */
	private float getHeuresMaquette(EOAP ap) {
		return this.controleurUE.getHeuresMaquette(ap, false);
	}
	
	/**
	 * Renvoie le total des heures prévues
	 * @param ap : AP
	 * @return les heures prévues
	 */
	private float getTotalHeuresPrevues(EOAP ap, ModeEdition modeEdition, Double heuresPrevues) {
		return this.controleurUE.getTotalHeuresPrevues(ap, modeEdition, heuresPrevues, false);
	}
	
	/**
	 * @return les heures qui restent à répartir par AP
	 */
	public float getNbHeuresResteARepartir() {
		return controleurUE.getNbHeuresResteARepartir(getCurrentServiceDetail().composantAP());
	}
	
	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser() {
		return getNbHeuresResteARealiser(getCurrentServiceDetail().composantAP());
	}

	/**
	 * @return les heures qui restent à réaliser par AP
	 */
	public float getNbHeuresResteARealiser(EOAP ap) {
		return controleurUE.getNbHeuresResteARealiser(ap);
	}
	
	
	/**
	 * @return les heures réalisées par AP
	 */
	public float getTotalHeuresRealisees(EOAP ap) {
		return controleurUE.getTotalHeuresRealisees(ap, false);
	}
	
	/******************** Répartition croisée ***************/
	
	/**
	 * Renvoie vrai si la personne connectée manipule un individu qui n'appartient pas au périmètre
	 * de ses structures (les structures de ces composantes scolarité).
	 * ==> permet de savoir si on est dans le cas d'une répartition croisée ou non
	 * 
	 * @param enseignant L'enseignant manipulé
	 * @param service : service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean isRepartitionCroisee(EOActuelEnseignant enseignant, EOService service) {
		if ((service.enseignant() != null) && (service.enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			IStructure composanteEnseignant = enseignant.getStructureAffectation(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(composanteEnseignant);
		}
		
		return false;
	}
	
	private EORepartEnseignant creerNouvelleRepart(EOServiceDetail serviceDetail) {
		EOPersonne personneConnecte = getPecheSession().getApplicationUser().getPersonne();
		EOIndividu repartiteurDemandeur =  getPecheSession().getApplicationUser().getIndividu();

		IStructure composanteReferent = serviceDetail.service().enseignant().getStructureAffectation(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		return EORepartEnseignant.creerNouvelleRepartition(serviceDetail, repartiteurDemandeur, 
				composanteReferent, personneConnecte, edc());
	}
	
	/**
	 * Vérifie que la composante de l'intervenant est la même que
	 * celle du répartiteur connecté.
	 *    ==> Vérifie les conditions dans lesquelles on peut permettre l'affichage 
	 *    de l'état de la répartition pour un serviceDetail donné
	 *
	 * @return si la répartition est croisée ou non.
	 */
	public boolean isModeRepartCroisee() {
		if (isEnseignantStatutaire()) {
			EORepartEnseignant repartition = EORepartEnseignant.rechercherServiceDetail(edc(), getCurrentServiceDetail());
	
			if (repartition != null) {
				return getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(repartition.composanteReferente());
			}
		}
		
		return false;
	}
	
	/**
	 * Est-ce que l'état de la répartition croisée est en attente 
	 * 	ou l'enseignant n'est pas de la meme composante?
	 * @return vrai/faux
	 */
	public boolean isRepartitionCroiseeEtatEnAttente() {
		boolean etatRepartition = true;
		
		if (isEnseignantStatutaire()) {
			etatRepartition = ((Constante.ATTENTE.equals(getCurrentServiceDetail().etat()) 
					 && isRepartitionCroisee(getEnseignantBean().getEnseignant(), getCurrentServiceDetail().service()))
					 || !(isRepartitionCroisee(getEnseignantBean().getEnseignant(), getCurrentServiceDetail().service())));
		}
		
		return etatRepartition;
	}
	
	
	private boolean isEnseignantStatutaire() {
		if ((getCurrentServiceDetail() != null) && (getCurrentServiceDetail().service() != null) 
				&& (getCurrentServiceDetail().service().enseignant() != null) && (getCurrentServiceDetail().service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			return true;
		}
		
		return false;
	}
	/*************************************************************/
	
	/**
	 * @param serviceDetail : serviceDetail
	 * @return le modeEdition utilisé CREATION ou MODIFICATION
	 */
	public ModeEdition modeEdition(EOServiceDetail serviceDetail) {
		ModeEdition modeEdition = null;
		
		if (serviceDetail.personneCreation() == null) {
			modeEdition = ModeEdition.CREATION;
		} else {
			modeEdition = ModeEdition.MODIFICATION;
		}
		
		return modeEdition;
	}
	
	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		// On ne tient pas compte des messages standards WO
	}
	
	@Override
	protected WOActionResults pageGestion() {
		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
		page.setParentAP(getParentAP());
		page.setTypeFiche(getTypeFiche());
		page.setSelectedItemId(getSelectedItemId());		
		return page;
	}
	
	@Override
	protected String getFormTitreCreationKey() {
		if (!isTypeFichePrevisionnel()) {
			return Messages.SERVICEREALISE_FORM_TITRE_AJOUTER;
		}
		return Messages.SERVICE_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		if (!isTypeFichePrevisionnel()) {
			return Messages.SERVICEREALISE_FORM_TITRE_MODIFIER;
		}
		return Messages.SERVICE_FORM_TITRE_MODIFIER;
	}

	
	/**
	 * action de notifier
	 * 	==> permet de récupérer l'enseignant selectionné
	 */
	public void notifier() {
		setCurrentServiceDetail(null);
		setEnseignantBean(this.controleur.getEnseignantBean());	
		//TODO: à revoir
	}

	public BasicPecheCtrl getControleur() {
		return controleur;
	}

	public void setControleur(BasicPecheCtrl controleur) {
		this.controleur = controleur;
	}

	public EnseignantDataBean getEnseignantBean() {
		return enseignantBean;
	}

	public void setEnseignantBean(EnseignantDataBean enseignantBean) {
		this.enseignantBean = enseignantBean;
	}

	public String getCurrentEtat() {
		return currentEtat;
	}

	public void setCurrentEtat(String currentEtat) {
		this.currentEtat = currentEtat;
	}
	
	/**
	 * @return etat
	 */
	public String etat() {
		return controleur.etat(currentEtat);
	}

	/**
	 * @return liste des états
	 */
	public NSArray<String> listeEtat() {
		return controleur.listeEtat();
	}

	/**
	 * @return le libelle d'affichage
	 */
	public String affichageEnseignement(){
		return ComposantAffichageHelper.getInstance().affichageEnseignement(parentAP);
	}
	
	public EOComposant getParentAP() {
		return parentAP;
	}

	public void setParentAP(EOComposant parentAP) {
		this.parentAP = parentAP;
	}

	public EOServiceDetail getCurrentServiceDetail() {
		return currentServiceDetail;
	}

	public void setCurrentServiceDetail(EOServiceDetail currentServiceDetail) {
		this.currentServiceDetail = currentServiceDetail;
	}

	public void setListeServiceDetail(NSArray<EOServiceDetail> listeServiceDetail) {
		this.listeServiceDetail = listeServiceDetail;
	}

	public List<EOAP> getListeAPs() {
		return listeAPs;
	}

	public void setListeAPs(List<EOAP> listeAPs) {
		this.listeAPs = listeAPs;
	}

	/**
	 * Renvoie le premier commentaire trouvé des AP de l'UE
	 * @return commentaire de l'UE
	 */
	public String getCommentaire() {
		if (commentaire == null) {
			//récupère le 1er commentaire non nul pour un AP
			for (EOServiceDetail serviceDetail : getListeServiceDetail()) {
				if (serviceDetail.commentaire() != null) {
					commentaire = serviceDetail.commentaire();
					break;
				}
			}
		}
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	@Override
	protected EOComposant creerNouvelObjet() {
		return null;
	}
	
	@Override
	public EOComposant editedObject() {
		return null;
	}

	public boolean isEnseignantBeanNotNull() {
		return (enseignantBean != null);
	}
	
	public boolean isEnseignantBeanVacataire() {
		if (enseignantBean != null) {
			return (!enseignantBean.isStatutaire() && !enseignantBean.isGenerique());
		}
		return false;
	}
	
	public boolean affichageUniqueComposanteVacataire() {
		List<IStructure> liste = listeComposantesVacataire();
		
		if (liste.size() == 1) {			
			setStructureReferente(liste.get(0));
			return true;
		}
		return false;
	}
			
	public List<IStructure> listeComposantesVacataire() {	
		if (getFonctionPresident() || getFonctionDrh()) {
			return getPecheSession().getComposantesScolarite();
		}
		EnseignantsVacatairesCtrl controleurVacataires = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		
		return getPecheSession().getPecheAutorisationCache().getStructuresPourVacataires(controleurVacataires, rechercherService());
	}

	public IStructure getStructureReferente() {
		return structureReferente;
	}

	public void setStructureReferente(IStructure structureReferente) {
		this.structureReferente = structureReferente;
	}
	
	/**
	 * met à jout les heures à payer à partir des heures réalisées
	 */
	public void majHeuresAPayer() {
		if (getCurrentServiceDetail() != null) {
			getCurrentServiceDetail().setHeuresAPayer(new BigDecimal(getCurrentServiceDetail().heuresRealisees()));
		}
	}
	
	
	/**
	 * renvoie si la personne connectée a la fonction de validation du drh
	 * @return vrai/faux
	 */
	public boolean getFonctionDrh() {
		return getAutorisation() != null &&  getAutorisation().hasDroitUtilisationValidationParDrh();
	}

	/**
	 * renvoie si la personne connectée a la fonction de validation du président
	 * @return vrai/faux
	 */
	public boolean getFonctionPresident() {
		return getAutorisation() != null && getAutorisation().hasDroitUtilisationValidationParPresident();
	}

	/**
	 * @return heures en attente de paiement
	 */
	private BigDecimal getHeuresEnAttentePaiement(EOServiceDetail serviceDetail) {
		return MiseEnPaiement.getInstance().calculerAPayerEnCours(
				serviceDetail.service(), serviceDetail);
	}
	
	/**
	 * @return heures à payer, si null renvoie 0
	 */
	private BigDecimal getHeuresAPayer(EOServiceDetail serviceDetail) {
		return OutilsValidation.retournerBigDecimalNonNull(serviceDetail.heuresAPayer());
	}

	/**
	 * @return heures payées, si null renvoie 0
	 */
	private BigDecimal getHeuresPayees(EOServiceDetail serviceDetail) {
		return OutilsValidation.retournerBigDecimalNonNull(serviceDetail.heuresPayees());
	}
	
	public EOStructure getCurrentStructureReferente() {
		return currentStructureReferente;
	}

	public void setCurrentStructureReferente(EOStructure currentStructureReferente) {
		this.currentStructureReferente = currentStructureReferente;
	}
	
	public boolean isNbHeuresDejaCrees() {
		return getListeServiceDetailDejaCree().contains(getCurrentServiceDetail());
	}

	public NSArray<EOServiceDetail> getListeServiceDetailDejaCree() {
		return listeServiceDetailDejaCree;
	}

	public void setListeServiceDetailDejaCree(
			NSArray<EOServiceDetail> listeServiceDetailDejaCree) {
		this.listeServiceDetailDejaCree = listeServiceDetailDejaCree;
	}

	public NSArray<EOServiceDetail> getListeServiceDetailCree() {
		return listeServiceDetailCree;
	}

	public void setListeServiceDetailCree(
			NSArray<EOServiceDetail> listeServiceDetailCree) {
		this.listeServiceDetailCree = listeServiceDetailCree;
	}

	@Override
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

	public List<EOPeriode> getListePeriode() {
		return listePeriode;
	}

	public EOPeriode getCurrentPeriode() {
		return currentPeriode;
	}

	public EOPeriode getSelectedPeriode() {
		return selectedPeriode;
	}

	public void setListePeriode(List<EOPeriode> listePeriode) {
		this.listePeriode = listePeriode;
	}

	public void setCurrentPeriode(EOPeriode currentPeriode) {
		this.currentPeriode = currentPeriode;
	}

	public void setSelectedPeriode(EOPeriode selectedPeriode) {
		this.selectedPeriode = selectedPeriode;
	}
}