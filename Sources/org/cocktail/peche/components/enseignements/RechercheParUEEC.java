package org.cocktail.peche.components.enseignements;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Classe représentant la recherche par UE/EC
 */
public class RechercheParUEEC extends CktlPecheBaseComponent implements BasicPecheObservateur {

	private static final long serialVersionUID = -217252404247099236L;
	private EOStructure currentStructure;
	private EOStructure selectedStructure;

	private BasicPecheCtrl controleur;

	private CriteresRechercheUeEc criteresRecherche;
	private boolean affichageResultats;

	private EOAP apSelected;
	private String selectedItemId;


	public RechercheParUEEC(WOContext context) {
		super(context);
		controleur = new BasicPecheCtrl(edc());
		criteresRecherche = new CriteresRechercheUeEc();
		this.controleur.addObservateur(this);	
		setApSelected(null);		
	}

	public String getTitre() {
		if (isModeConsultation()) {
			return (String) getPecheSession().localizer().valueForKey("RechercheParUEEC.titre.consultation");
		}
		if (Constante.PREVISIONNEL.equals(getTypeFiche())) {
			return (String) getPecheSession().localizer().valueForKey("RechercheParUEEC.titre.fiche.previsionnelle");
		} else {
			return (String) getPecheSession().localizer().valueForKey("RechercheParUEEC.titre.fiche.definitive");
		}		
	}
	
	public WOActionResults rechercherUeEc() {
		if (StringCtrl.isEmpty(criteresRecherche.getCodeUeEc()) && StringCtrl.isEmpty(criteresRecherche.getLibelleUeEc())) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "vous devez saisir un code et/ou un libellé");
			affichageResultats = false;
			return null;
		} 
		
		if (!StringCtrl.isEmpty(criteresRecherche.getLibelleUeEc()) 
				&& criteresRecherche.getLibelleUeEc().trim().length() < 3) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "vous devez saisir au moins 3 caractères pour le libellé");
			affichageResultats = false;
			return null;
		}

		if (!StringCtrl.isEmpty(criteresRecherche.getCodeUeEc()) 
				&& criteresRecherche.getCodeUeEc().trim().length() < 3) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, "vous devez saisir au moins 3 caractères pour le code");
			affichageResultats = false;
			return null;
		}

		affichageResultats = true;
		return null;
	}

	@Override
	public void setTypeFiche(String typeFiche) {		
		super.setTypeFiche(typeFiche);
		criteresRecherche.setIsRechercheFicheDef(true);
		criteresRecherche.setIsRechercheFichePrev(false);
		if (Constante.PREVISIONNEL.equals(typeFiche)) {
			criteresRecherche.setIsRechercheFicheDef(false);
			criteresRecherche.setIsRechercheFichePrev(true);
		}		
	}
	
	/**
	 * 
	 * @return List<IStructure>
	 */
	public List<IStructure> listeStructure() {
		
		// si consultation, toutes les structures CS de l'établissement
		if (isModeConsultation()) {
			if (getPecheSession() != null) {
				List<IStructure> liste = getPecheSession().getComposantesScolarite();
				new StructureTri().trierParLibelleLongPuisParentAsc(liste);
				return liste;
			}			
		}
		
		List<IStructure> liste = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignement();
		new StructureTri().trierParLibelleLongPuisParentAsc(liste);
		return liste;
	}

	/**
	 * 
	 * @return true / false
	 */
	public boolean affichageUniqueStructure() {
		if (!CollectionUtils.isEmpty(listeStructure()) && listeStructure().size() == 1) {
			criteresRecherche.setStructure(listeStructure().get(0));
			return true;
		}
		return false;
	}


	/**
	 * @return the currentStructure
	 */
	public EOStructure currentStructure() {
		return currentStructure;
	}

	/**
	 * @param currentStructure the currentStructure to set
	 */
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}

	/**
	 * @return the selectedStructure
	 */
	public EOStructure selectedStructure() {
		return selectedStructure;
	}

	/**
	 * @param selectedStructure the selectedStructure to set
	 */
	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
	}

	public CriteresRechercheUeEc getCriteresRecherche() {
		return criteresRecherche;
	}

	public void setCriteresRecherche(CriteresRechercheUeEc criteresRecherche) {
		this.criteresRecherche = criteresRecherche;
	}

	/**
	 * @return the affichageResultats
	 */
	public boolean affichageResultats() {
		return affichageResultats;
	}

	/**
	 * @param affichageResultats the affichageResultats to set
	 */
	public void setAffichageResultats(boolean affichageResultats) {
		this.affichageResultats = affichageResultats;
	}

	public boolean boutonDetailInactif() {
		if (getApSelected() == null) {
			return true;
		}
		if (getApSelected().parents() != null && !getApSelected().parents().isEmpty()) {
			return false;
		}
		return true;
	}

	public void notifier() {
		EOAP ap = this.controleur.getAP();
		setApSelected(ap);		
	}

	public EOAP getApSelected() {
		return apSelected;
	}

	public void setApSelected(EOAP apSelected) {
		this.apSelected = apSelected;
	}

	public BasicPecheCtrl getControleur() {
		return controleur;
	}

	public WOActionResults ouvrirParentAP() {
		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
		page.setParentAP((EOComposant) getApSelected().parents().get(0));
		page.setSelectedItemId(getSelectedItemId());		
		page.setTypeFiche(getTypeFiche());	
		page.setModeConsultation(isModeConsultation());
		this.session().setObjectForKey(this, Session.BOUTONRETOUR);
		return page;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

	public String displayStructures() {
		if (currentStructure() != null) {
			return currentStructure().lcStructure() + " (" + currentStructure().toStructurePere().lcStructure() + ")"; 
		}
		return null;
	}
}