package org.cocktail.peche.components.enseignements.UEFlottantes;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EORepartUefDetail;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EORepartUefStructure;
import org.cocktail.peche.outils.OutilsValidation;
import org.cocktail.peche.outils.RandomHelper;
import org.joda.time.DateTime;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * Formulaire d'édition des UE flottantes
 * 
 * @author Chama LAATIK
 */
public class CktlPecheEditUEFlottante extends CktlPecheTableComponent<EOUE> {

	private static final long serialVersionUID = 1L;

	private EOUE editedObject;
	private boolean modeCreation;

	private NSArray<EOTypeAP> listeTypesAP;
	private EOTypeAP currentTypeAP;

	private List<EOAP> listeAPs;
	private List<EOAP> listeAPsEnregistres;
	private EOAP currentAP;
	
	private List<IStructure> listeComposantes;
	private EOStructure currentComposante;

	private EOPersonne personne;
	private UECtrl controleur;
	
	private EORepartUefEtablissement repartition;
	private EORepartUefDetail detailUE;
	private String etablissementExterne;
	
	private boolean validationDidFail = false;

	private StructureTri structureTri;
	private String selectedItemId;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'éxécution.
	 */
	public CktlPecheEditUEFlottante(WOContext context) {
		super(context);
		
		this.personne = personneConnecte(edc());
		structureTri = new StructureTri();
		
		this.setListeTypesAP(EOTypeAP.fetchAllSco_TypeAPs(edc(), EOTypeAP.CODE.ascs()));
		this.controleur = new UECtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		
		
		this.setListeComposantes(construireListeComposantes());
	}

	protected void beforeSaveChanges() throws Exception {
		DateTime now = new DateTime();
		handleAPBeforeSave(this.getListeAPs());
		handleUEBeforeSave(editedObject);
		handleRepartitionBeforeSave(now);
		handleDetailBeforeSave(now);
	}

	private void handleDetailBeforeSave(DateTime now) {
		//Dans le cas d'une nouvelle UE ou d'une UE sur laquelle on a jamais saisi de détail 
		if(detailUE.id() == null){
			detailUE.setPersonneCreation(personne);
			detailUE.setDateCreation(now);
		} else {
			detailUE.setPersonneModification(personne);
			detailUE.setDateModification(now);
		}
		detailUE.setComposant(editedObject);
	}

	private void handleRepartitionBeforeSave(DateTime now) {
		if (etablissementExterne != null) {
			if (isModeCreation()) {
				repartition = EORepartUefEtablissement.creerEtInitialiser(edc());
				repartition.setComposantUE(editedObject);
				repartition.setPersonneCreation(personne);
				repartition.setDateCreation(now);
			}
			
			repartition.setEtablissementExterne(etablissementExterne);
			repartition.setDateModification(now);
			repartition.setPersonneModification(personne);
		}
	}

	/**
	 * @return le libellé d'affichage du titre.
	 */
	public String affichageTitre(){
		StringBuilder result = new StringBuilder();
		if(isModeCreation()){
			result.append("Ajouter une nouvelle");
		} else {
			result.append("Modifier une");
		}
		result.append(" UE Flottante");
		return result.toString();
	}
	
	/**
	 * Traitement du code l'UE flottante.
	 * Pour éviter les collisions il est généré.
	 * @param ue
	 */
	private void handleUEBeforeSave(EOUE ue) {
		if(ue.code() == null){
			ue.setCode(RandomHelper.getInstance().getRandomStringWithPrefix(Constante.CODE_UE_PREFIX));
		}
	}

	/**
	 * Traitement des atomes pédagogiques avant sauvegarde.
	 * Génération des codes et initialisation des valeurs par défaut.
	 * @throws Exception
	 */
	private void handleAPBeforeSave(List<EOAP> aps) throws Exception {
		for (EOAP ap : aps) {
			//on supprime les AP sans heure ou à 0h
			if (ap.chargeEnseignementPrevisionnelle().valeurHeures() == null || BigDecimal.ZERO.compareTo(ap.chargeEnseignementPrevisionnelle().valeurHeures()) == 0) {
				ap.supprimer();
			} else {
				if (ap.code() == null) {
					controleur.creerlienAP(ap, editedObject);
				}
				
				if (ap.chargeEnseignementPrevisionnelle().nbGroupes() == null) {
					ap.chargeEnseignementPrevisionnelle().setNbGroupes(1);
				}
			}
		}
	}

	/**
	 * la liste des composantes comprend :
	 * - les composantes de scolarité
	 * - les composantes sélectionnées sur la page de paramétrage dédiée
	 */
	private List<IStructure> construireListeComposantes() {
		
		List<IStructure> listeCompScol = new ArrayList<IStructure>();
		
		listeCompScol.addAll(getPecheSession().getPecheAutorisationCache().getListeComposantesScolariteEnseignement());	
				
		NSArray<EORepartUefStructure> repartUefStructureArray = EORepartUefStructure.fetchAllEORepartUefStructures(edc());
		
		for (EORepartUefStructure repartUefStructure : repartUefStructureArray) {
			if (!listeCompScol.contains(repartUefStructure.toStructure())) {
				listeCompScol.add(repartUefStructure.toStructure());
			}
		}
		
		structureTri.trierParLibelleCourt(listeCompScol);
		
		return listeCompScol;
	}
	
	private void enregistrer() throws Exception {
		try {
			beforeSaveChanges();
			edc().saveChanges();
		} catch (ValidationException e) {
			this.setEditedObject(null);
			edc().invalidateAllObjects();
			edc().revert();
			throw e;
		}
	}
	
	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		validationDidFail = true;
		getPecheSession().addSimpleMessage(TypeMessage.ERREUR, e.getMessage());
		super.validationFailedWithException(e, obj, keyPath);
	}

	/**
	 * @return l'action à exécuter pour faire une annulation.
	 */
	public WOActionResults annuler() {
		getPecheSession().getUiMessages().clear();
		edc().revert();
		
		return pageGestion();
	}

	/**
	 * @return l'action à exécuter pour enregistrer une création/mise à jour et
	 *         retourner à l'écran précédent.
	 * @throws Exception 
	 */
	public WOActionResults enregistrerEtRetour() throws Exception {
		if (!statutValidation()) {
			return null;
		}
		if (verifierEntiteNonNull(editedObject.structures(), etablissementExterne)) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.RENSEIGNER_UN_ENSEIGNEMENT_OU_UN_ETAB_EXTERNE);
			return null;
		}
		listeAPsEnregistres = new ArrayList<EOAP>(getListeAPs());
		for (EOAP ap : this.getListeAPs()) {
			if (ap.chargeEnseignementPrevisionnelle().valeurHeures() == null || BigDecimal.ZERO.compareTo(ap.chargeEnseignementPrevisionnelle().valeurHeures()) == 0) {
				listeAPsEnregistres.remove(ap);
			}
		}
		
		if (listeAPsEnregistres.size() == 0) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.LISTE_AP_NON_RENSEIGNE);
			return null;
		}
		
		enregistrer();
		this.setEditedObject(null);

		return pageGestion();
	}
	
	/**
	 * @return le statut de la validation.
	 */
	public boolean statutValidation() {
		boolean statutOK = true;
		if (validationDidFail) {
			validationDidFail = false;
			statutOK = false;
		}

		return statutOK;
	}

	/**
	 * Renvoie l'editedObject
	 * @return l'UE
	 */
	public EOUE getEditedObject() {
		if (editedObject == null) {
			try {
				editedObject = EOComposantFactory.createComposant(EOUE.class, personneConnecte().persId(), edc(), Integer.valueOf(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()), null);
				editedObject.setTagApplication(Constante.APP_PECHE);
				editedObject.setComposantInfoVersion(EOComposantInfoVersion.create(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), null));
	
				List<EOAP> aps = new ArrayList<EOAP>();
				
				for (EOTypeAP typeAP : this.getListeTypesAP()) {
					EOAP composantAP = controleur.initialiserAP(typeAP, personneConnecte().persId(), Integer.valueOf(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
					aps.add(composantAP);
				}

				this.setListeAPs(aps);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return editedObject;
	}
	
	
	/**
	 * Setter de l'editedObject
	 * @param editedObject : editedObject
	 */
	public void setEditedObject(EOUE editedObject) {
		if (editedObject != null) {
			this.editedObject = (EOUE) EOComposant.fetchSco_Composant(edc(), EOUE.ID_KEY, editedObject.id());
			etablissementExterne = controleur.getEtablissementExterne(getEditedObject());
			repartition = controleur.getRepartition(getEditedObject());
			detailUE = EORepartUefDetail.rechercherDetailForUE(edc(), getEditedObject());
		}
	}

	/**
	 * Renvoie les AP de l'UE éditée
	 * @return liste de lienComposer
	 */
	//TODO : Refactorer : présent dans CktlPecheTableUEFlottante
	public NSArray<EOLienComposer> getLienComposerEnfantsTypeAP() {
		return controleur.getLienComposerEnfantsTypeAP(getEditedObject());
	}

	/**
	 * Renvoie la liste des AP 
	 * 
	 * @return liste des AP
	 */
	public List<EOAP> getListeAPs() {
		if (listeAPs == null) {
			List<EOAP> aps = new ArrayList<EOAP>();
			NSArray<EOLienComposer> lienComposer = getLienComposerEnfantsTypeAP();
			
			for (EOTypeAP typeAP : this.getListeTypesAP()) {
				EOAP apTrouve = null;
				//on ajoute les AP présents dans lienComposer
				for (int i = 0; i < lienComposer.size(); i++) {
					EOAP composantAP = (EOAP) lienComposer.get(i).child();
					
					if (composantAP.typeAP().code().equals(typeAP.code())) {
						apTrouve = composantAP;
						break;
					}
				}
				//si AP non trouvé, on le crée
				if (apTrouve == null) {
					apTrouve = controleur.initialiserAP(typeAP, personneConnecte().persId(), Integer.valueOf(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
				}
				
				aps.add(apTrouve);
			}
			
			this.setListeAPs(aps);
		}
		
		return listeAPs;
	}

	protected WOActionResults pageGestion() {
		CktlPecheTableUEFlottante page = (CktlPecheTableUEFlottante) pageWithName(CktlPecheTableUEFlottante.class.getName());
		page.setTypeFiche(getTypeFiche());
		page.setSelectedItemId(getSelectedItemId());
		return page;
	}
	
	/**
	 * Formatter à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public NSNumberFormatter getApp2DecimalesFormatter() {
		return OutilsValidation.getApp2DecimalesFormatter();
	}
	
	/**
	 * Vérifie qu'une seule des 2 entités est non nulle
	 * @param composante : composante dans l'établissement
	 * @param etablissementExterne : etablissement externe
	 * @return vrai/faux
	 */
	private boolean verifierEntiteNonNull(NSArray<EOStructure> composante, String etablissementExterne) {
		return (((composante.size() == 0) && (etablissementExterne == null)) 
				|| ((composante.size() != 0) && (etablissementExterne != null)));
	}
	
	/**
	 * En mode modification, renvoit booléen pour savoir si la composante est renseigné ou pas
	 * @return vrai si AP non null
	 */
	public boolean modeComposanteRenseigne() {
		return (this.editedObject.structures().size() != 0);
	}
	
	/* ========== Getters & Setters ========== */
	
	public void setListeAPs(List<EOAP> listeAPs) {
		this.listeAPs = listeAPs;
	}

	public NSArray<EOTypeAP> getListeTypesAP() {
		return listeTypesAP;
	}

	public void setListeTypesAP(NSArray<EOTypeAP> listeTypesAP) {
		this.listeTypesAP = listeTypesAP;
	}

	public EOTypeAP getCurrentTypeAP() {
		return currentTypeAP;
	}

	public void setCurrentTypeAP(EOTypeAP currentTypeAP) {
		this.currentTypeAP = currentTypeAP;
	}

	public EOAP getCurrentAP() {
		return currentAP;
	}

	public void setCurrentAP(EOAP currentAP) {
		this.currentAP = currentAP;
	}

	public boolean isModeCreation() {
		return modeCreation;
	}

	public void setModeCreation(boolean modeCreation) {
		this.modeCreation = modeCreation;
	}
	
	public String getEtablissementExterne() {
		return etablissementExterne;
	}
	
	public void setEtablissementExterne(String etablissement) {
		this.etablissementExterne = etablissement;
	}
	
	public void setRepartition(EORepartUefEtablissement repartition) {
		this.repartition = repartition;
	}

	public EORepartUefDetail getDetailUE() {
		if(detailUE == null){
			detailUE = EORepartUefDetail.creerEtInitialiser(edc());
		}
		return detailUE;
	}

	public void setDetailUE(EORepartUefDetail detailUE) {
		this.detailUE = detailUE;
	}

	public List<IStructure> getListeComposantes() {
		return listeComposantes;
	}

	public void setListeComposantes(List<IStructure> listeComposantes) {
		this.listeComposantes = listeComposantes;	
	}

	public EOStructure getCurrentComposante() {
		return currentComposante;
	}

	public void setCurrentComposante(EOStructure currentComposante) {
		this.currentComposante = currentComposante;
	}

	public String displayComposante() {
		if (getCurrentComposante() != null) {
			return getCurrentComposante().lcStructure() + " (" + getCurrentComposante().toStructurePere().lcStructure() + ")";  
		}
		return null;
	}

	@Override
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

}