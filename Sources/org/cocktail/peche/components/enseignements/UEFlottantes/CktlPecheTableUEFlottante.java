package org.cocktail.peche.components.enseignements.UEFlottantes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeSet;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantInfoVersion;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.OffreFormationCtrl;
import org.cocktail.peche.components.controlers.TypeApCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignements.ListeAP;
import org.cocktail.peche.components.enseignements.Noeud;
import org.cocktail.peche.components.enseignements.NoeudTable;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Cette classe modélise la liste des UE flottantes.
 * 
 * @author Chama LAATIK
 * 
 */
public class CktlPecheTableUEFlottante extends CktlPecheTableComponent<EOUE> {

	private static final long serialVersionUID = -785729038351664290L;
	
	private CktlTreeTableNode<Noeud> rootNode;
	private CktlTreeTableNode<Noeud> selectedNode;
	private Collection<String> listeCodesAP = new ArrayList<String>();
	
	private UECtrl controleurUe;
	private OffreFormationCtrl controleurOffreFormation;
	private TypeApCtrl controleurTypeAp;

	private String selectedItemId;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'éxécution.
	 */
	public CktlPecheTableUEFlottante(WOContext context) {
		super(context);
		this.controleurUe = new UECtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		controleurOffreFormation = new OffreFormationCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		controleurTypeAp = new TypeApCtrl();

		chargerDonneesArbre();
	}

	private void chargerDonneesArbre() {
		// La racine de l'arbre
		Noeud data = new Noeud(null);
		data.setCode("Racine");
		this.rootNode = new NoeudTable(data, null, false);
		
		Collection<String> listeCodesApNonTries = new TreeSet<String>();
		List<IStructure> listeCibleEnseignement =  null;

		// si ModeConsultation, affichage de toutes les UEF donc listeCibleEnseignement = null
		if (!isModeConsultation()) {
			listeCibleEnseignement = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignement();		
			listeCibleEnseignement.addAll(getPecheSession().getPecheAutorisationCache().getListeStructAdditionnellesUEFlottantes());
		}
		
		NSArray<EOUE> listeUeDansEtablissement = controleurOffreFormation.rechercherUeNonDiplomanteDansEtablissement(this.edc(), listeCibleEnseignement);
		
		controleurOffreFormation.ajouterUeNonDiplomanteDansEtablissementDansArborescence(rootNode, listeUeDansEtablissement, listeCodesApNonTries, edc());

		NSArray<EORepartUefEtablissement> listeUeHorsEtablissement = controleurOffreFormation.rechercherUeNonDiplomanteHorsEtablissement(edc());
		controleurOffreFormation.ajouterUeNonDiplomanteHorsEtablissementDansArborescence(rootNode, listeUeHorsEtablissement, listeCodesApNonTries, edc());

		listeCodesAP.clear();
		listeCodesAP.addAll(listeCodesApNonTries);
		controleurTypeAp.trierListeCodesAp(listeCodesAP);
	}
	
	/**
	 * Action de modification.
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults editerUE() {
		if (isSelectedNodeIsUe()) {
			EOUE ue = getSelectedUe();
			CktlPecheEditUEFlottante form = (CktlPecheEditUEFlottante) pageWithName(CktlPecheEditUEFlottante.class.getName());
			form.setModeCreation(false);
			form.setEditedObject(ue);
			form.setTypeFiche(getTypeFiche());
			form.setSelectedItemId(getSelectedItemId());					
			return form;
		} 
		
		return null;
	}
	
	/**
	 * Action d'ajout
	 *
	 * @return une instance du formulaire en mode création.
	 */
	public WOActionResults ajouterUE() {
		CktlPecheEditUEFlottante form = (CktlPecheEditUEFlottante) pageWithName(CktlPecheEditUEFlottante.class.getName());
		form.setModeCreation(true);
		form.setTypeFiche(getTypeFiche());
		form.setSelectedItemId(getSelectedItemId());		
		return form;
	}
	
	/**
	 * Action de suppression. Supprime l'objet sélectionné. Cette
	 * action doit être appelée après confirmation par l'utilisateur.
	 * @return null;
	 */
	public WOActionResults supprimerUE() {
		try {
			if (isSelectedNodeIsUe()) {
				EOUE ue = getSelectedUe();
				
				if (ue.getComposantSuivant() != null) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.UE_ANNEE_SUIVANTE_EXISTANTE);
					throw new ValidationException(Constante.UE_ANNEE_SUIVANTE_EXISTANTE);
				}
				
				NSArray<EOLienComposer> listeLienComposer = controleurUe.getLienComposerEnfantsTypeAP(ue);
				
				for (EOLienComposer lienComposer : listeLienComposer) {
					if (isRepartitionService(lienComposer.child())) {
						getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_EXISTANTE);
						throw new ValidationException(Constante.REPARTITION_EXISTANTE);
					}
					lienComposer.child().supprimer();
				}
				controleurUe.supprimerRepartitionEtablissement(ue);
				controleurUe.supprimerDetail(ue);
				ue.supprimer();
				edc().saveChanges();
				chargerDonneesArbre();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
		}
		return null;
	}
	
	/**
	 * Renvoie vrai si le nœud sélectionné est une UE (parent direct d'APs pour les UE flottantes), faux sinon
	 * @return vrai/faux
	 */
	public boolean isSelectedNodeIsUe() {
		if (getSelectedNode() != null) {
			return getSelectedNode().getData().isParenAP();
		}
		return false;
	}

	/**
	 * Retourne l'EOUE correspondant au nœud UE sélectionné.
	 * @return l'EOUE correspondant au nœud UE sélectionné
	 */
	public EOUE getSelectedUe() {
		if (!isSelectedNodeIsUe()) {
			return null;
		}
		
		EOQualifier qualifier = ERXQ.and(EOUE.CODE.eq(getSelectedNode().getData().getCode()), 
				EOUE.COMPOSANT_INFO_VERSION.dot(EOComposantInfoVersion.ANNEE_KEY).eq(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
		EOUE ue = EOUE.fetchSco_UE(edc(), qualifier);
		
		return ue;
	}
	
	/**
	 * Vérifie s'il existe une répartition sur le service liée à l'AP
	 * @param ap : AP
	 * @return vrai/faux
	 */
	public boolean isRepartitionService(EOComposant ap) {
		NSArray<EOServiceDetail> listeServiceDetail = EOServiceDetail.fetchEOServiceDetails(edc(), ERXQ.equals(EOServiceDetail.COMPOSANT_AP_KEY, ap), null);
		
		if (listeServiceDetail.size() > 0) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * @return le message d'avertissement à afficher avant de supprimer 
	 *         une UE.
	 */
	public String avantSupprimerUE() {
		String msgConfirm = message(Messages.UE_FLOTTANTE_ALERTE_CONFIRM_SUPPRESS);
		return "confirm('" + msgConfirm + "')";
	}
	
	public boolean isBoutonsDisabled() {
		return !isSelectedNodeIsUe();
	}
	
	
	public WOActionResults ouvrirParentAP() {
		if (isSelectedNodeIsUe()) {
			ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
			page.setEditedObject(getSelectedNode());
			page.setTypeFiche(getTypeFiche());
			page.setSelectedItemId(getSelectedItemId());
			page.setModeConsultation(isModeConsultation());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} else {
			return null;
		}
	}
	
	
	/* ========== Getters & Setters ========== */

	public CktlTreeTableNode<Noeud> getRootNode() {
		return rootNode;
	}

	public void setRootNode(CktlTreeTableNode<Noeud> rootNode) {
		this.rootNode = rootNode;
	}

	public CktlTreeTableNode<Noeud> getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(CktlTreeTableNode<Noeud> selectedNode) {
		this.selectedNode = selectedNode;
	}

	public Collection<String> getListeCodesAP() {
		return listeCodesAP;
	}

	public void setListeCodesAP(Collection<String> listeCodesAP) {
		this.listeCodesAP = listeCodesAP;
	}

	@Override
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

}