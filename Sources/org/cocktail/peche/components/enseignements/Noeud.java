package org.cocktail.peche.components.enseignements;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;

/**
 * Représente les données d'un nœud du TreeTable "Offre de formation".
 * <p>
 * Note : Cette classe n'aurait pas dû être spécifique à l'offre de formation..
 */
public class Noeud extends HashMap<String, Float> {

	private static final String SUFFIX_HEURES_PREVUES = "prevues";
	private static final String SUFFIX_HEURES_REALISEES = "realisees";

	private static final long serialVersionUID = -3491714707436173446L;
	
	private static Logger log = Logger.getLogger(Noeud.class);

	private int id;
	private String code;
	private String libelle;
	private ITypeComposant typeComposant;
	private String currentCodeAP;
	private boolean parenAP = false;

	private Noeud parent;

	private Float heuresMaquettePourAPCourante;

	private Float heuresPrevuesPourAPCourante;

	private Float heuresRealiseesPourAPCourante;
	
	//private String cssClassPourAPCourante;
	
	private Map<String, EOAP> listeAp;

	/**
	 * Constructeur.
	 * 
	 * @param parent les données du nœud parent
	 */
	public Noeud(Noeud parent) {
		this.parent = parent;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * Ajouter des données à ce nœud.
	 * 
	 * @param ap Une AP l'AP sur laquelle ajouter les heures
	 * @param heuresMaquette Le nombre d'heures maquette à ajouter
	 * @param heuresPrevues Le nombre d'heures affecté à ajouter
	 */
	public void add(EOAP ap, Float heuresMaquette, Float heuresPrevues, Float heuresRealisees) {
		String codeTypeAP = ap.typeAP().code();
		
		// Ajout des heures maquette
		if (heuresMaquette != null) {
			Float heuresMaquettesActuelles = this.get(codeTypeAP);
			if (heuresMaquettesActuelles == null) {
				heuresMaquettesActuelles = 0.0f;
			}
		
			heuresMaquettesActuelles += heuresMaquette;
			this.put(codeTypeAP, heuresMaquettesActuelles);
		}
		
		// Ajout des heures affectées
		if (heuresPrevues != null) {
			Float heuresPrevuesActuelles = this.get(codeTypeAP + SUFFIX_HEURES_PREVUES);
			if (heuresPrevuesActuelles == null) {
				heuresPrevuesActuelles = 0.0f;
			}
		
			heuresPrevuesActuelles += heuresPrevues;
			this.put(codeTypeAP + SUFFIX_HEURES_PREVUES, heuresPrevuesActuelles);
		}

		// Ajout des heures réalisées
		if (heuresRealisees != null) {
			Float heuresRealiseesActuelles = this.get(codeTypeAP + SUFFIX_HEURES_REALISEES);
			if (heuresRealiseesActuelles == null) {
				heuresRealiseesActuelles = 0.0f;
			}

			heuresRealiseesActuelles += heuresRealisees;
			this.put(codeTypeAP + SUFFIX_HEURES_REALISEES, heuresRealiseesActuelles);
		}

		// Ajout de l'AP
		if (listeAp == null) {
			listeAp = new HashMap<String, EOAP>();
		}
		
		listeAp.put(codeTypeAP, ap);
		
		if (this.parent != null) {
			this.parent.add(ap, heuresMaquette, heuresPrevues, heuresRealisees);
		}
	}

	@Override
	public String toString() {
		return "{code: " + this.code + ", map: " + super.toString() + "}";
	}

	/**
	 * @return the currentCodeAP
	 */
	public String getCurrentCodeAP() {
		return currentCodeAP;
	}

	/**
	 * @param currentCodeAP the currentCodeAP to set
	 */
	public void setCurrentCodeAP(String currentCodeAP) {
		if (currentCodeAP == null) {
			return;
		}
		
		this.currentCodeAP = currentCodeAP;
		
		if (log.isTraceEnabled()) {
			log.trace(this.code + " : " + this.currentCodeAP + " : " + this.get(this.currentCodeAP) + " : " + this.get(this.currentCodeAP + SUFFIX_HEURES_PREVUES));
		}
		
		this.heuresMaquettePourAPCourante = this.get(this.currentCodeAP);
		this.heuresPrevuesPourAPCourante = this.get(this.currentCodeAP + SUFFIX_HEURES_PREVUES);
		this.heuresRealiseesPourAPCourante = this.get(this.currentCodeAP + SUFFIX_HEURES_REALISEES);
		
	/*	if (this.heuresPrevuesPourAPCourante == this.heuresMaquettePourAPCourante) {
			this.cssClassPourAPCourante = "";
		} else if (this.heuresPrevuesPourAPCourante < this.heuresMaquettePourAPCourante) {
			this.cssClassPourAPCourante = "cktl-number-negative";
		} else {
			this.cssClassPourAPCourante = "cktl-number-positive";
		}*/
		
	}

	public Float getHeuresMaquettePourAPCourante() {
		return this.heuresMaquettePourAPCourante;
	}

	public Float getHeuresPrevuesPourAPCourante() {
		return this.heuresPrevuesPourAPCourante;
	}
	
	public Float getHeuresRealiseesPourAPCourante() {
		return this.heuresRealiseesPourAPCourante;
	}

	/*public String getCssClassPourAPCourante() {
		return this.cssClassPourAPCourante;
	}*/

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public boolean isParenAP() {
		return parenAP;
	}

	public void setParenAP(boolean parenAP) {
		this.parenAP = parenAP;
	}

	public Map<String, EOAP> getListeAp() {
		return listeAp;
	}
	
	public ITypeComposant getTypeComposant() {
		return typeComposant;
	}

	public void setTypeComposant(ITypeComposant typeComposant) {
		this.typeComposant = typeComposant;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
