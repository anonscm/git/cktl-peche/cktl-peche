package org.cocktail.peche.components.enseignements;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposantFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeChargeEnseignement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IChargeEnseignement;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.controlers.UECtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Surcharge du nombre de groupe par AP
 *
 * @author Chama LAATIK
 *
 */
public class APSurchargeNbGroupeForm extends PecheCreationForm<EOLienComposer> {

	private static final long serialVersionUID = -4926845268081725891L;
	private EOComposant parentAP;
	private UECtrl ueCtrl = new UECtrl(null, getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	private IChargeEnseignement chargeEnseignementPrevisionnelle;

	private String selectedItemId;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public APSurchargeNbGroupeForm(WOContext context) {
        super(context);
    }

    /**
     * Renvoie le nombre de groupe initial prévu pour l'AP
     * @return nombre de groupe initial
     */
    public String getNbGroupeAPInitial() {
    	if (editedObject() != null) {
    		setParentAP(editedObject().parent());
    		
    		IChargeEnseignement chargeEnseignement = ueCtrl.getChargeEnseignementTheorique(editedObject());
    		
    		if (chargeEnseignement == null) {
    			chargeEnseignement = ((EOAP) editedObject().child()).chargeEnseignementPrevisionnelle();
    		}
    		
	    	if (chargeEnseignement != null && chargeEnseignement.nbGroupes() != null) {
	    		return chargeEnseignement.nbGroupes().toString();
	    	}
    	}
    	
    	return Constante.NON_DEFINI;
    }

    /**
     * Renvoie le nombre d'heures initial présent dans la maquette
     * @return nombre d'heures
     */
    public String getNbHeuresMaquetteInitial() {
    	if (editedObject() != null) {
    		setParentAP(editedObject().parent());
    		
    		IChargeEnseignement chargeEnseignement = ueCtrl.getChargeEnseignementTheorique(editedObject());
    		
    		if (chargeEnseignement == null) {
    			chargeEnseignement = ((EOAP) editedObject().child()).chargeEnseignementPrevisionnelle();
    		}
    		
	    	if (chargeEnseignement != null && chargeEnseignement.valeurHeures() != null) {
	    		return chargeEnseignement.valeurHeures().toString();
	    	}
    	}
    	
    	return Constante.NON_DEFINI;
    }
    
    /**
     * Retourne le nombre de groupe surchargés.
     * @return le nombre de groupe surchargés
     */
    public Integer nbGroupesSurcharges() {
    	if (chargeEnseignementPrevisionnelle == null) {
    		chargeEnseignementPrevisionnelle = editedObject().chargeEnseignementPrevisionnelle();
    	}
    	
    	if (chargeEnseignementPrevisionnelle != null) {
    		return chargeEnseignementPrevisionnelle.nbGroupes();
    	}
    	
    	return null;
    }
    
    /**
     * Retourne le nombre d'heures surchargées.
     * @return le nombre d'heures surchargées
     */
    public BigDecimal valeurHeuresSurchargees() {
    	if (chargeEnseignementPrevisionnelle == null) {
    		chargeEnseignementPrevisionnelle = editedObject().chargeEnseignementPrevisionnelle();
    	}
    	
    	if (chargeEnseignementPrevisionnelle != null) {
    		return chargeEnseignementPrevisionnelle.valeurHeures();
    	}
    	
    	return null;
    }
    
    /**
     * Affecte le nombre de groupe surchargés.
     * @param nbGroupesSurcharges un nombre de groupe surchargés
     */
    public void setNbGroupesSurcharges(String nbGroupesSurcharges) {
    	if (nbGroupesSurcharges != null && chargeEnseignementPrevisionnelle == null) {
    		chargeEnseignementPrevisionnelle = EOComposantFactory.createChargeEnseignementSurLien(editedObject(), 
    				EOTypeChargeEnseignement.typePrevisionnelle(editedObject().editingContext()), personneConnecte().persId());
    	}
    	
    	if (chargeEnseignementPrevisionnelle != null) {
    		try {
	    		Integer nbGroupesSurchargesInt = string2int(nbGroupesSurcharges);
	    		chargeEnseignementPrevisionnelle.setNbGroupes(nbGroupesSurchargesInt);
    		} catch (NumberFormatException e) {
    			throw new ValidationException(message(Messages.SURCHARGE_AP_FORM_MESSAGE_NOMBRE_GROUPES_DOIT_ETRE_ENTIER));
			}
    	}
    }
    
    /**
     * Affecte le nombre d'heure surchargées.
     * @param valeurHeuresSurchargees un nombre d'heure surchargées
     */
    public void setValeurHeuresSurchargees(String valeurHeuresSurchargees) {
    	if (valeurHeuresSurchargees != null && chargeEnseignementPrevisionnelle == null) {
    		chargeEnseignementPrevisionnelle = EOComposantFactory.createChargeEnseignementSurLien(editedObject(), 
    				EOTypeChargeEnseignement.typePrevisionnelle(editedObject().editingContext()), personneConnecte().persId());
    	}
    	
    	if (chargeEnseignementPrevisionnelle != null) {
    		try {
    			BigDecimal valeurHeuresSurchargeesBigDec = string2bigdec(valeurHeuresSurchargees);
    			chargeEnseignementPrevisionnelle.setValeurHeures(valeurHeuresSurchargeesBigDec);
    			
    			if (valeurHeuresSurchargeesBigDec != null) {
	    			if (valeurHeuresSurchargeesBigDec.scale() > 2) {
	        			throw new ValidationException(message(Messages.SURCHARGE_AP_FORM_MESSAGE_NOMBRE_HEURES_DEUX_DECIMALES_MAXIMUM));
	    			} else {
	    				// On force deux positions décimale
	    				chargeEnseignementPrevisionnelle.setValeurHeures(valeurHeuresSurchargeesBigDec.setScale(2));
	    			}
    			}
    		} catch (NumberFormatException e) {
    			throw new ValidationException(message(Messages.SURCHARGE_AP_FORM_MESSAGE_NOMBRE_HEURES_DOIT_ETRE_NOMBRE));
			}
    	}
    }

    @Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
    	// Si aucune charge d'enseignement n'est surchargée, on supprime l'occurence en table
    	if (chargeEnseignementPrevisionnelle != null && chargeEnseignementPrevisionnelle.valeurHeures() == null && chargeEnseignementPrevisionnelle.nbGroupes() == null) {
    		chargeEnseignementPrevisionnelle.supprimer();
    	}
    	
    	majLienComposerAPMutualise();
		this.editedObject().child().setPersIdModification(getPecheSession().getApplicationUser().getPersId());
		this.editedObject().child().setDateModification(new NSTimestamp());
	}
    
    /**
     * Met à jour le nombre de groupes de tous les liensComposer pour les AP mutualisés
     */
    //TODO : A optimiser : requête inutile si AP non mutualisé
    public void majLienComposerAPMutualise() {
	    EOQualifier qualifier = ERXQ.equals(EOLienComposer.CHILD_ID_KEY, this.editedObject().child().id());
		NSArray<EOLienComposer> listeLiensComposer = EOLienComposer.fetchSco_LienComposers(edc(), qualifier, 
				EOLienComposer.CHILD_ID.ascs());
		
		if (listeLiensComposer.size() > 1) {
			for (EOLienComposer lienComposer : listeLiensComposer) {
				IChargeEnseignement chargeEnseignementLien = lienComposer.chargeEnseignementPrevisionnelle();
				
				if (valeurHeuresSurchargees() == null && nbGroupesSurcharges() == null) {
					if (chargeEnseignementLien != null) {
						// S'il n'y a plus aucune surcharge (ni d'heure ni de groupe), on supprimer l'occurence en table
						chargeEnseignementLien.supprimer();
					}
				} else {
					if (chargeEnseignementLien == null) {
						// On crée une charge d'enseignement prévisionelle (surcharge Peche) sur le lien
			    		chargeEnseignementLien = EOComposantFactory.createChargeEnseignementSurLien(lienComposer, 
			    				EOTypeChargeEnseignement.typePrevisionnelle(lienComposer.editingContext()), personneConnecte().persId());
					}
					
					chargeEnseignementLien.setNbGroupes(nbGroupesSurcharges());
					chargeEnseignementLien.setValeurHeures(valeurHeuresSurchargees());
				}
			}
		}
    }
    
    public void setParentAP(EOComposant parentAP) {
    	this.parentAP = parentAP;
    }

    public EOComposant getParentAP() {
    	return this.parentAP;
    }

    /**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauSurchargeAPContainerId() {
		return getComponentId() + "_nouveauSurchargeAPContainerId";
	}

    @Override
	protected WOActionResults pageGestion() {
		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
		page.setParentAP(getParentAP());
		page.setTypeFiche(getTypeFiche());
		page.setSelectedItemId(getSelectedItemId());
		return page;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.SURCHARGE_A_P_FORM_TITRE_MODIFIER;
	}

	@Override
	protected String getFormTitreCreationKey() {
		return null;
	}

	@Override
	protected EOLienComposer creerNouvelObjet() {
		return null;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}
	
}