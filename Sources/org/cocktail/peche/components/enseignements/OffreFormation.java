package org.cocktail.peche.components.enseignements;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.OffreFormationCtrl;
import org.cocktail.peche.components.controlers.TypeApCtrl;
import org.cocktail.peche.entity.EORepartUefEtablissement;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSMutableArray;

/**
 * Cette classe modélise la liste des UE de l'offre d'enseignement.
 * @author Yannick Mauray
 * @author Chama Laatik
 */
public class OffreFormation extends PecheGestionComponent<EOAP, OffreFormationDetail> {
	private static final long serialVersionUID = 7451069145509713507L;
	
	/** logger. */
	private static Logger log = Logger.getLogger(OffreFormation.class);
	
	/** Structure virtuelle contenant les UE flottantes. */
	private static final String STRUCTURE_UE_FLOTTANTES = "UE Flottantes";

	private PecheMenuItem selectedMenuItem;
	
	private String titreEtendu;
	private boolean boutonDetailAffiche = true;
	private boolean boutonsSelectionAffiches;
	private WOComponent pageAppelante;
	private IRetourUeSelectionnee retourUeSelectionnee;
	private CktlTreeTableNode<Noeud> selectedNode;
	private CktlTreeTableNode<Noeud> rootNode;
	private Map<String, EOStructure> mapComposantes;
	private Map<String, NSArray<EODiplome>> mapComposantesDiplomes;
	private EOStructure currentComposante;
	private EODiplome currentDiplome;
	private EOGradeUniversitaire currentGradeUniversitaire;
	private EOGradeUniversitaire gradeUniversitaireSelectionne;
	private EOStructure composanteSelectionnee;
	private EODiplome diplomeSelectionnee;
	private String codeUeEc;
	private String libelleUeEc;
	private Collection<String> listeCodesAP = new ArrayList<String>();
	private Collection<String> listeCodesAPEnseignementsDiplomants = new TreeSet<String>();
	private Collection<String> listeCodesAPEnseignementsNonDiplomantsDansEtablissement = new TreeSet<String>();
	private Collection<String> listeCodesAPEnseignementsNonDiplomantsHorsEtablissement = new TreeSet<String>();

	private boolean affichageConcis;
	private boolean lireEnseignementsDiplomants = true;
	private boolean lireEnseignementsNonDiplomantsDansEtablissement;
	private boolean lireEnseignementsNonDiplomantsHorsEtablissement;
	private boolean colonneRepartitionAffichee = true;

	private OffreFormationCtrl offreFormationCtrl;
	private TypeApCtrl typeApCtrl;

	private EOVersionDiplome selectedVersionDiplome;
	
	/**
	 * Constructeur.
	 * @param context le contexte d'exécution.
	 */
	public OffreFormation(WOContext context) {
		super(context);
		offreFormationCtrl = new OffreFormationCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
		typeApCtrl = new TypeApCtrl();

		Noeud data = new Noeud(null);
		data.setCode("Racine");
		this.rootNode = new NoeudTable(data, null, false);
		typeApCtrl.trierListeCodesAp(listeCodesAP);
		getListeComposantes();
//		fetchspecComposant(null);
	}
	

	// *********************
	// Critères de recherche
	// *********************
	/**
	 * Initialiser les données nécessaires à l'affichage des critères de recherche.
	 */
	private void initialiserDonneesCriteresDeRecherche() {
		if (mapComposantes == null) {
			mapComposantes = new TreeMap<String, EOStructure>();
			mapComposantesDiplomes = new HashMap<String, NSArray<EODiplome>>();

			if (lireEnseignementsDiplomants) {
				int anneeEnCours = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
				NSArray<EODiplome> listeDiplomes = offreFormationCtrl.rechercherEnseignementsDiplomants(edc(), anneeEnCours);
				
				for (EODiplome diplome : listeDiplomes) {
					if (diplome.structures().size() == 0) {
						ajouterDiplomeAStructure(null, diplome);
					} else {
						for (EOStructure structure : diplome.structures()) {
							ajouterDiplomeAStructure(structure, diplome);
						}
					}
				}

				// La première composante est sélectionnée
				for (EOStructure composante : mapComposantes.values()) {
					setComposanteSelectionnee(composante);
					break;
				}

				// On trie les listes des diplômes sur le code
				for (Map.Entry<String, NSArray<EODiplome>> entry : mapComposantesDiplomes.entrySet()) {
					try {
						entry.setValue(entry.getValue().sortedArrayUsingComparator(new NSComparator() {
							@Override
							public int compare(Object diplome0, Object diplome1) {
								return ((EODiplome) diplome0).code().compareTo(((EODiplome) diplome1).code());
							}
						}));
					} catch (ComparisonException e) {
						throw new RuntimeException(e);
					}
				}
			}

			initChoixUeFlottantes();
		}
	}

	private void initChoixUeFlottantes() {
		initialiserDonneesCriteresDeRecherche();

		// Si on affiche les UE Flottantes
		if (lireEnseignementsNonDiplomantsDansEtablissement || lireEnseignementsNonDiplomantsHorsEtablissement) {
			if (!mapComposantes.containsKey(STRUCTURE_UE_FLOTTANTES)) {
				EOStructure structureUeFlottantes = new EOStructure();
				structureUeFlottantes.setCStructure(STRUCTURE_UE_FLOTTANTES);
				mapComposantes.put(STRUCTURE_UE_FLOTTANTES, structureUeFlottantes);
			}
		} else {
			mapComposantes.remove(STRUCTURE_UE_FLOTTANTES);
		}
	}

	private void ajouterDiplomeAStructure(EOStructure structure, EODiplome diplome) {
		// Si on est pas en sélection d'UE,
		// On ajoute à la liste que si l'utilisateur connecté est autorisé à la structure
		if (!boutonsSelectionAffiches && structure != null && !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignement(structure)) {
			return;
		}

		String codeStructure;

		if (structure == null) {
			codeStructure = "";
		} else {
			codeStructure = structure.cStructure();
		}

		if (!mapComposantes.containsKey(codeStructure)) {
			if (structure == null) {
				structure = new EOStructure();
			}

			mapComposantes.put(codeStructure, structure);
			mapComposantesDiplomes.put(codeStructure, new NSMutableArray<EODiplome>());
		}

		NSArray<EODiplome> listeDiplomesComposante = mapComposantesDiplomes.get(codeStructure);
		listeDiplomesComposante.add(diplome);
	}

	/**
	 * Retourne le libellé à afficher devant la combo-box "Liste des composantes".
	 * @return le libellé à afficher devant la combo-box "Liste des composantes"
	 */
	public String getLibelleComboComposantes() {
		if (lireEnseignementsNonDiplomantsDansEtablissement || lireEnseignementsNonDiplomantsHorsEtablissement) {
			return message("OffreFormation.Criteres.ComposanteUeFlottante");
		}

		return message("OffreFormation.Criteres.Composante");
	}

	/**
	 * @return la liste des composantes de tous les diplômes affichables par l'utilisateur.
	 */
	public NSArray<EOStructure> getListeComposantes() {
		initialiserDonneesCriteresDeRecherche();
		NSArray<EOStructure> listeComposantes = new NSMutableArray<EOStructure>(mapComposantes.size());

		for (EOStructure structure : mapComposantes.values()) {
			listeComposantes.add(structure);
		}

		return listeComposantes;
	}

	public EOStructure getCurrentComposante() {
		return currentComposante;
	}

	public void setCurrentComposante(EOStructure currentComposante) {
		this.currentComposante = currentComposante;
	}

	/**
	 * @return le libellé à afficher pour la composante dans la combo-box "Composante".
	 */
	public String getCurrentComposanteDisplayString() {
		if (STRUCTURE_UE_FLOTTANTES.equals(currentComposante.cStructure())) {
			return message("OffreFormation.Critere.Composante.Libelle.UeFlottantes");
		} else if (currentComposante.lcStructure() == null) {
			return message("OffreFormation.Critere.Composante.Libelle.DiplomesSansComposante");
		}

		return currentComposante.lcStructure();
	}

	public EOStructure getComposanteSelectionnee() {
		return composanteSelectionnee;
	}

	/**
	 * Affecte la nouvelle composante sélectionnée. Efface l'arbre (pour rechargement) si la composante sélectionnée est "<UE Flottantes>".
	 * @param composanteSelectionnee la composante sélectionnée
	 */
	public void setComposanteSelectionnee(EOStructure composanteSelectionnee) {
		this.composanteSelectionnee = composanteSelectionnee;
	}

	public boolean isGroupeRechercheUeFlottantesSelectionne() {
		return getComposanteSelectionnee() != null && STRUCTURE_UE_FLOTTANTES.equals(getComposanteSelectionnee().cStructure());
	}

	/**
	 * @return la liste des diplômes pour la composante sélectionnée
	 */
	public NSArray<EODiplome> getListeDiplomesComposante() {
		NSArray<EODiplome> diplomes;
		if (getComposanteSelectionnee() == null || getComposanteSelectionnee().cStructure() == null) {
			diplomes = mapComposantesDiplomes.get("");
		} else {
			diplomes = mapComposantesDiplomes.get(getComposanteSelectionnee().cStructure());
		}

		if (currentGradeUniversitaire == null || currentGradeUniversitaire.equals("-")) {
			return diplomes;
		}
		
		NSArray<EODiplome> result = new NSMutableArray<EODiplome>();
		if (diplomes != null) {
			for (EODiplome diplome : diplomes) {
				if (diplome.gradeUniversitaire() != null && diplome.gradeUniversitaire().type() != null && gradeUniversitaireSelectionne != null) {
					if (diplome.gradeUniversitaire().type().equals(gradeUniversitaireSelectionne.type())) {
						result.add(diplome);
					}
				}
			}
		}
		return result;
	}
	
	
	
	/**
	 * @return la liste des grades pour un diplome
	 */
	public NSArray<EOGradeUniversitaire> getListeGradeUniversitaire() {
		NSArray<EOGradeUniversitaire> listeGradeUniversitaire = EOGradeUniversitaire.fetchAllSco_GradeUniversitaires(edc(),EOGradeUniversitaire.ID.ascs());
		getListeDiplomesComposante();
		return listeGradeUniversitaire;
	}

	public EODiplome getCurrentDiplome() {
		return currentDiplome;
	}

	public void setCurrentDiplome(EODiplome currentDiplome) {
		this.currentDiplome = currentDiplome;
	}

	public String getCurrentDiplomeDisplayString() {
		return getCurrentDiplome().code() + " - " + getCurrentDiplome().libelle();
	}

	public EODiplome getDiplomeSelectionnee() {
		return diplomeSelectionnee;
	}

	public void setDiplomeSelectionnee(EODiplome diplomeSelectionnee) {
		this.diplomeSelectionnee = diplomeSelectionnee;
	}

	public String getCodeUeEc() {
		return codeUeEc;
	}

	/**
	 * Affecte le code UE/EC des critères de recherche (avec trim et mise en majuscule).
	 * @param codeUeEc le code UE/EC à rechercher
	 */
	public void setCodeUeEc(String codeUeEc) {
		if (codeUeEc != null) {
			codeUeEc = codeUeEc.trim().toUpperCase();
		}

		this.codeUeEc = codeUeEc;
	}

	public String getLibelleUeEc() {
		return libelleUeEc;
	}

	/**
	 * Affecte le libelle UE/EC des critères de recherche (avec trim).
	 * @param libelleUeEc le libelle UE/EC à rechercher
	 */
	public void setLibelleUeEc(String libelleUeEc) {
		if (libelleUeEc != null) {
			libelleUeEc = libelleUeEc.trim();
		}

		this.libelleUeEc = libelleUeEc;
	}

	/**
	 * Action déclenchée sur le changement de valeur dans la combo-box "Composante".
	 * @return <code>null</code> (réaffiche la page)
	 */
	public WOActionResults changerComposante() {
		// Pour les UE flottantes, la recherche se lance toute seule
		if (isGroupeRechercheUeFlottantesSelectionne()) {
			razDonneesArbre();

			if (lireEnseignementsNonDiplomantsDansEtablissement) {
				lireEnseignementsNonDiplomantsDansEtablissement();
			}
			if (lireEnseignementsNonDiplomantsHorsEtablissement) {
				lireEnseignementsNonDiplomantsHorsEtablissement();
			}

			listeCodesAP.clear();
			listeCodesAP.addAll(listeCodesAPEnseignementsNonDiplomantsDansEtablissement);
			listeCodesAP.addAll(listeCodesAPEnseignementsNonDiplomantsHorsEtablissement);
			typeApCtrl.trierListeCodesAp(listeCodesAP);
		}

		return null;
	}

	
	
	/**
	 * Rechercher (lire) le diplôme sélectionné.
	 * @return <code>null</code> (réaffiche la page)
	 */
	public WOActionResults rechercherDiplome() {
		razDonneesArbre();

		if (!StringCtrl.isEmpty(getCodeUeEc()) || !StringCtrl.isEmpty(getLibelleUeEc())) {
			lireEnseignementsDiplomants(getCodeUeEc(), getLibelleUeEc());
		} else if (!isGroupeRechercheUeFlottantesSelectionne()) {
			if (lireEnseignementsDiplomants) {
				lireEnseignementsDiplomants();
			}
		} else {
			if (lireEnseignementsNonDiplomantsDansEtablissement) {
				lireEnseignementsNonDiplomantsDansEtablissement();
			}
			if (lireEnseignementsNonDiplomantsHorsEtablissement) {
				lireEnseignementsNonDiplomantsHorsEtablissement();
			}
		}

		// getRootNode();

		listeCodesAP.clear();
		listeCodesAP.addAll(listeCodesAPEnseignementsDiplomants);
		listeCodesAP.addAll(listeCodesAPEnseignementsNonDiplomantsDansEtablissement);
		listeCodesAP.addAll(listeCodesAPEnseignementsNonDiplomantsHorsEtablissement);
		typeApCtrl.trierListeCodesAp(listeCodesAP);

		return null;
	}

	// *****************
	// Affichage d'arbre
	// *****************
	private void razDonneesArbre() {
		rootNode.getChildren().clear();
		setSelectedNode(null);
	}

	public void setLireEnseignementsDiplomants(boolean lireEnseignementsDiplomants) {
		this.lireEnseignementsDiplomants = lireEnseignementsDiplomants;
	}

	/**
	 * Est-ce qu'on doit donner la possibilité de rechercher les UE flottantes de l'établissement ?
	 * @param lireEnseignementsNonDiplomantsDansEtablissement Est-ce qu'on doit donner la possibilité de rechercher les UE flottantes de l'établissement ?
	 */
	public void setLireEnseignementsNonDiplomantsDansEtablissement(boolean lireEnseignementsNonDiplomantsDansEtablissement) {
		this.lireEnseignementsNonDiplomantsDansEtablissement = lireEnseignementsNonDiplomantsDansEtablissement;
		initChoixUeFlottantes();
	}

	/**
	 * Est-ce qu'on doit donner la possibilité de rechercher les UE flottantes hors de l'établissement ?
	 * @param lireEnseignementsNonDiplomantsHorsEtablissement Est-ce qu'on doit donner la possibilité de rechercher les UE flottantes hors de l'établissement ?
	 */
	public void setLireEnseignementsNonDiplomantsHorsEtablissement(boolean lireEnseignementsNonDiplomantsHorsEtablissement) {
		this.lireEnseignementsNonDiplomantsHorsEtablissement = lireEnseignementsNonDiplomantsHorsEtablissement;
		initChoixUeFlottantes();
	}

	/**
	 * Lire les enseignements diplômants (dans l'établissement) contenant un composante EC ou UE ayant le code passé en paramètre.
	 * @param codeUeEc Le code UE ou EC à rechercher
	 * @param libelleUeEc Le libelle UE ou EC à rechercher
	 */
	public void lireEnseignementsDiplomants(String codeUeEc, String libelleUeEc) {
		listeCodesAPEnseignementsDiplomants.clear();
		int anneeEnCours = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
		NSArray<EODiplome> listeDiplomes = offreFormationCtrl.rechercherEnseignementsDiplomants(edc(), codeUeEc, libelleUeEc, anneeEnCours);
		NSArray<EODiplome> listeDiplomesHabilites = offreFormationCtrl.filtrerDiplomesHabilites(listeDiplomes, getPecheSession().getPecheAutorisationCache()
		    .getListeCibleEnseignement());

		// On génère l'arbre
		CktlTreeTableNode<Noeud> noeudSelectionne = offreFormationCtrl.ajouterEnseignementsDiplomantsDansArborescencePourAnnee(rootNode, listeDiplomesHabilites,
		    affichageConcis, listeCodesAPEnseignementsDiplomants, codeUeEc, libelleUeEc, edc());
		setSelectedNode(noeudSelectionne);

		// Si la recherche a donnée quelque chose, on "set" les critères de recherche
		if (!listeDiplomesHabilites.isEmpty()) {
			setCodeUeEc(null);
			setLibelleUeEc(null);

			EODiplome diplome = listeDiplomesHabilites.get(0);
			setDiplomeSelectionnee(diplome);

			if (diplome.structures().isEmpty()) {
				setComposanteSelectionnee(getListeComposantes().get(0));
			} else {
				for (EOStructure structureDiplome : diplome.structures()) {
					if (mapComposantes.values().contains(structureDiplome)) {
						setComposanteSelectionnee(structureDiplome);
					}
				}
			}
		}
	}

	/**
	 * Lire les enseignements diplômants (dans l'établissement).
	 */
	public void lireEnseignementsDiplomants() {
		listeCodesAPEnseignementsDiplomants.clear();

		if (getDiplomeSelectionnee() != null) {
			NSArray<EODiplome> diplomes = new NSArray<EODiplome>(getDiplomeSelectionnee());
			offreFormationCtrl.ajouterEnseignementsDiplomantsDansArborescencePourAnnee(rootNode, diplomes, affichageConcis, listeCodesAPEnseignementsDiplomants, null, null,
			    edc());
		}
	}

	/**
	 * Lire les enseignements non diplômants de l'établissement.
	 */
	public void lireEnseignementsNonDiplomantsDansEtablissement() {
		listeCodesAPEnseignementsNonDiplomantsDansEtablissement.clear();

		NSArray<EOUE> listeUeDansEtablissement = offreFormationCtrl.rechercherUeNonDiplomanteDansEtablissement(this.edc());
		offreFormationCtrl.ajouterUeNonDiplomanteDansEtablissementDansArborescence(rootNode, listeUeDansEtablissement,
		    listeCodesAPEnseignementsNonDiplomantsDansEtablissement, edc());
	}

	/**
	 * Lire les enseignements non diplômants hors établissement.
	 */
	public void lireEnseignementsNonDiplomantsHorsEtablissement() {
		listeCodesAPEnseignementsNonDiplomantsHorsEtablissement.clear();

		NSArray<EORepartUefEtablissement> listeUeHorsEtablissement = offreFormationCtrl.rechercherUeNonDiplomanteHorsEtablissement(this.edc());
		offreFormationCtrl.ajouterUeNonDiplomanteHorsEtablissementDansArborescence(rootNode, listeUeHorsEtablissement,
		    listeCodesAPEnseignementsNonDiplomantsHorsEtablissement, edc());
	}

	public boolean isAffichageConcis() {
		return affichageConcis;
	}

	public void setAffichageConcis(boolean affichageConcis) {
		this.affichageConcis = affichageConcis;
	}

	public boolean isColonneRepartitionAffichee() {
		return colonneRepartitionAffichee;
	}

	public void setColonneRepartitionAffichee(boolean colonneRepartitionAffichee) {
		this.colonneRepartitionAffichee = colonneRepartitionAffichee;
	}

	/**
	 * @return Le nombre de colonne affichées par AP
	 */
	public int getNbColonnesAfficheesParAp() {
		int nbColonnes = 1;

		if (colonneRepartitionAffichee) {
			nbColonnes++;
		}

		return nbColonnes;
	}

	/**
	 * @return Liste des types d'AP de toutes les UE affichées (CM/TP/TD en premier)
	 */
	public Collection<String> getListeCodesAP() {
		return listeCodesAP;
	}

	@Override
	protected String getEntityName() {
		return EOAP.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return OffreFormationDetail.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return null;
	}

	/**
	 * @return the rootNode
	 */
	public CktlTreeTableNode<Noeud> getRootNode() {
		return this.rootNode;
	}

	/**
	 * Action d'ouverture du détail de L'AP.
	 * @return la fiche de voeux de l'enseignant.
	 */
	public WOActionResults ouvrirParentAP() {
		if (selectedNode() != null) {
			ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
			page.setEditedObject(selectedNode());
			page.setSelectedItemId(getSelectedMenuItem().getId());
			page.setTypeFiche(getTypeFiche());
			this.session().setObjectForKey(this, Session.BOUTONRETOUR);
			return page;
		} else {
			return doNothing();
		}
	}

	/**
	 * @return Le titre de la page
	 */
	public String getTitre() {
		String titre = (String) getPecheSession().localizer().valueForKey("OffreFormation.titre");

		if (!StringCtrl.isEmpty(titreEtendu)) {
			titre += " - " + titreEtendu;
		}

		return titre;
	}

	/**
	 * Renvoie vrai si ce n'est pas le parent direct de l'AP, faux sinon
	 * @return vrai/faux
	 */
	public boolean getGriserBouton() {
		if (selectedNode() != null) {
			return !selectedNode().getData().isParenAP();
		}
		return true;
	}

	/**
	 * @return the selectedNode
	 */
	public CktlTreeTableNode<Noeud> selectedNode() {
		return selectedNode;
	}

	/**
	 * @param selectedNode the selectedNode to set
	 */
	public void setSelectedNode(CktlTreeTableNode<Noeud> selectedNode) {
		this.selectedNode = selectedNode;
	}

	public PecheMenuItem getSelectedMenuItem() {
		return selectedMenuItem;
	}

	public void setSelectedMenuItem(PecheMenuItem selectedMenuItem) {
		this.selectedMenuItem = selectedMenuItem;
	}

	public String getTitreEtendu() {
		return titreEtendu;
	}

	public void setTitreEtendu(String titreEtendu) {
		this.titreEtendu = titreEtendu;
	}

	public boolean isBoutonDetailAffiche() {
		return boutonDetailAffiche;
	}

	public void setBoutonDetailAffiche(boolean boutonDetailAffiche) {
		this.boutonDetailAffiche = boutonDetailAffiche;
	}

	public boolean isBoutonsSelectionAffiches() {
		return boutonsSelectionAffiches;
	}

	/**
	 * Permet (ou non) la sélection pour choix d'une UE.
	 * @param boutonsSelectionAffiches <code>true</code> pour pouvoir choisir une UE
	 * @param pageAppelante La page à appeler après cette sélection
	 * @param retourUeSelectionnee Le composant qui va réceptionner l'UE choisie
	 */
	public void setBoutonsSelectionAffiches(boolean boutonsSelectionAffiches, WOComponent pageAppelante, IRetourUeSelectionnee retourUeSelectionnee) {
		this.boutonsSelectionAffiches = boutonsSelectionAffiches;
		this.pageAppelante = pageAppelante;
		this.retourUeSelectionnee = retourUeSelectionnee;
	}

	/**
	 * Retourner l'UE sélectionnée à la page appelante.
	 * @return La page appelante
	 */
	public WOActionResults selectionnerUe() {
		Noeud data = selectedNode().getData();
		String libelleUe = data.getLibelle();
		Map<String, EOAP> listeAp = data.getListeAp();

		// On retourne les Aps dans l'ordre affiché
		List<EOAP> listeApTriee = new ArrayList<EOAP>();

		for (String codesAP : listeCodesAP) {
			EOAP ap = listeAp.get(codesAP);

			if (ap != null) {
				listeApTriee.add(ap);
			}
		}

		retourUeSelectionnee.setUeSelectionnee(libelleUe, listeApTriee);

		return pageAppelante;
	}

	/**
	 * Annuler l'action de sélectionner une UE.
	 * @return La page appelante
	 */
	public WOActionResults annuler() {
		return pageAppelante;
	}
	
	/**
	 * @return menu pour l'offre d formation
	 */
	public String getMenuOffreFormation() {		
		return getSelectedMenuItem().getId();
	}
	
	public String getListeDeroulanteDiplomesId() {
		return getComponentId() + "_listeDeroulanteDiplomesId";
	}

	public EOVersionDiplome getSelectedVersionDiplome() {
		return selectedVersionDiplome;
	}

	public void setSelectedVersionDiplome(EOVersionDiplome selectedVersionDiplome) {
		this.selectedVersionDiplome = selectedVersionDiplome;
	}

	public EOGradeUniversitaire getCurrentGradeUniversitaire() {
		return currentGradeUniversitaire;
	}

	public void setCurrentGradeUniversitaire(EOGradeUniversitaire currentGraduUniversitaire) {
		this.currentGradeUniversitaire = currentGraduUniversitaire;
	}

	public EOGradeUniversitaire getGradeUniversitaireSelectionne() {
		return gradeUniversitaireSelectionne;
	}

	public void setGradeUniversitaireSelectionne(
			EOGradeUniversitaire gradeUniversitaireSelectionne) {
		this.gradeUniversitaireSelectionne = gradeUniversitaireSelectionne;
	}

}
