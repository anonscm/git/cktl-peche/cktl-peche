package org.cocktail.peche.components.enseignements;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Classe qui affiche les détails d'un élement de la maquette
 * 
 * @author Chama LAATIK
 * 
 */
public class OffreFormationDetail extends PecheCreationForm<EOAP> {

	private static final long serialVersionUID = -1893762230628853181L;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public OffreFormationDetail(WOContext context) {
		super(context);
	}

	@Override
	protected String getFormTitreCreationKey() {
		return null;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.OFFRE_FORMATION_DETAIL_TITRE;
	}

	@Override
	protected WOActionResults pageGestion() {
		OffreFormation page = (OffreFormation) pageWithName(OffreFormation.class.getName());
		return page;
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {

	}

	@Override
	protected EOAP creerNouvelObjet() {
		return null;
	}
	
	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String offreFormationDetailContainerId() {
		return getComponentId() + "_offreFormationDetailContainerId";
	}
	
} 