package org.cocktail.peche.components.enseignements;

import org.cocktail.fwkcktlajaxwebext.serveur.component.treetable.CktlTreeTableNode;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Représente un nœud d'une treeTable.
 */
public class NoeudTable extends CktlTreeTableNode<Noeud> {
	
	private NSArray<CktlTreeTableNode<Noeud>> children;

	/**
	 * Constructeur.
	 * 
	 * @param data les données du nœud
	 * @param parent le nœud parent
	 * @param visible est-ce que ce nœud est visible dans l'arbre ?
	 */
	public NoeudTable(Noeud data, CktlTreeTableNode<Noeud> parent, Boolean visible) {
		super(data, parent, visible);
		
		this.children = new NSMutableArray<CktlTreeTableNode<Noeud>>();
		if (parent != null) {
			parent.getChildren().add(this);
		}
	}

	@Override
	public NSArray<CktlTreeTableNode<Noeud>> getChildren() {
		return this.children;
	}
}
