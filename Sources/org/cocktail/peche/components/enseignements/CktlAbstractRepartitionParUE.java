package org.cocktail.peche.components.enseignements;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLienComposer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.EnseignantsGeneriquesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.components.enseignants.EnseignantDataBean;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXQ;

public class CktlAbstractRepartitionParUE extends CktlPecheTableComponent<EOServiceDetail>{

	private static final long serialVersionUID = 2599727705505248418L;

	private EOComposant parentAP;
	private UECtrl controleur;
	private EnseignantsStatutairesCtrl controleurStatutaire;
	private EnseignantsVacatairesCtrl controleurVacataire;
	private EnseignantsGeneriquesCtrl controleurGenerique;
	
	
	public CktlAbstractRepartitionParUE(WOContext context) {
		super(context);
		
		int annee = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
        this.controleur = new UECtrl(this.edc(), annee);
    	this.controleurStatutaire = new EnseignantsStatutairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    	this.controleurVacataire = new EnseignantsVacatairesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    	this.controleurGenerique = new EnseignantsGeneriquesCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
    	
        
        ERXDatabaseDataSource dataSource = new ERXDatabaseDataSource(edc(), EOServiceDetail.ENTITY_NAME);
        EOQualifier qualAnnee = EOServiceDetail.SERVICE.dot(EOService.ANNEE_KEY).eq(annee);
        EOFetchSpecification f = new EOFetchSpecification(EOServiceDetail.ENTITY_NAME, qualAnnee, EOServiceDetail.COMPOSANT_AP.dot(EOAP.TYPE_AP).dot(EOTypeAP.CODE_KEY).ascs());
        		
        dataSource.setFetchSpecification(f);
        
		ERXDisplayGroup<EOServiceDetail> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		
		
	}

	@Override
    public void appendToResponse(WOResponse response, WOContext context) {
		if (hasBinding("parentAP")) {
			parentAP = (EOComposant) valueForBinding("parentAP");
		} else {
			if (getSelectedObject() != null) {
				parentAP = (EOComposant) getSelectedObject().composantAP().parents().get(0);
			}
		}
	   	EOQualifier qualifier = getQualifierAPPourServiceDetail();
		
		((ERXDatabaseDataSource) this.getDisplayGroup().dataSource()).setAuxiliaryQualifier(qualifier);
		this.getDisplayGroup().setSortOrderings(EOServiceDetail.getTriParDefaut());
		this.getDisplayGroup().fetch();
		
    	super.appendToResponse(response, context);
    }
    
    /**
     * On fabrique un qualifier pour récupérer la liste des EOServiceDetail qui ont pour 
     * composantAP l'un des enfants du parentAP
     * 
     * @return qualifier
     */
    private EOQualifier getQualifierAPPourServiceDetail() {
    	EOQualifier qualifier = null;
    	
    	//On récupère la liste des AP de l'UE/EC 
    	NSArray<EOLienComposer> listeLienComposer = getControleur().getLienComposerEnfantsTypeAP(parentAP);
    	//TODO error
//			qualifier = EOServiceDetail.SERVICE.dot("ANNEE").eq(2013);
		for (EOLienComposer lienComposer : listeLienComposer) {
			EOAP ap = (EOAP) lienComposer.child();
			//On fabrique le qualifier des services détail pour chaque AP récupéré 
			qualifier = ERXQ.or(qualifier, ERXQ.equals(EOServiceDetail.COMPOSANT_AP_KEY, ap));
		}
		
		return qualifier;
    }

    /**
     * @return nom de l'enseignant
     */
	public String getEnseignantBeanNom() {
		String nom = null;
		
		//Si c'est le service d'un enseignant (EOActueEnseignant)
		if (getCurrentItem().service().enseignant() != null) {
			nom = getCurrentItem().service().enseignant().toIndividu().nomAffichage();
		} else {
			nom = getCurrentItem().service().toEnseignantGenerique().nom();
		}
		
		return nom.toUpperCase();
	}
	
	/**
     * @return prenom de l'enseignant
     */
	public String getEnseignantBeanPrenom() {
		String prenom = null;
		
		//Si c'est le service d'un enseignant (EOActueEnseignant)
		if (getCurrentItem().service().enseignant() != null) {
			prenom = getCurrentItem().service().enseignant().toIndividu().prenomAffichage();
		} else {
			prenom = getCurrentItem().service().toEnseignantGenerique().prenom();
		}
		
		return prenom;
	}
	
	/**
	 * @return composante de l'enseignant
	 */
	public String getEnseignantBeanComposante() {
		String composante = null;
		
		if ((getCurrentItem().service().iEnseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			
			if (getCurrentItem().service().isServiceGenerique()) {
				composante = libelleCourtNonNull(controleurGenerique.getStructureComposante(getCurrentItem().service().toEnseignantGenerique()));
				
			} else {
				composante = libelleCourtNonNull(getCurrentItem().service().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			}
		} else {
			composante = libelleCourtNonNull(getStructureCS(getCurrentItem().toRepartService().toStructure()));
		}
		
		return composante;
	}
	
	/**
	 * @return departement de l'enseignant
	 */
	public String getEnseignantBeanDepartement() {
		String departement = null;
		
		if ((getCurrentItem().service().iEnseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
	
			if (getCurrentItem().service().isServiceGenerique()) {
				departement = libelleCourtNonNull(controleurGenerique.getStructureDepartement(getCurrentItem().service().toEnseignantGenerique()));
				
			} else {
				departement = libelleCourtNonNull(getCurrentItem().service().enseignant().getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
			}
		} else {
			departement = libelleCourtNonNull(getStructureDE(getCurrentItem().toRepartService().toStructure()));
		}
		
		return departement;
	}
	
	/**
	 * @return corps/contrat de l'enseignant
	 */
	public String getEnseignantBeanCorps() {
		if (getCurrentItem().service().enseignant() != null) {
			if (getCurrentItem().service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()
					, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {			
				return getCurrentItem().service().enseignant().getLibelleCourtCorpsouContrat(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()
						, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			} else {				
				return controleurVacataire.getLibelleCourtTypeAssocieVacation(getCurrentItem().service().enseignant());
			}
		} else {
			return null;
		}
	}
	
	/**
	 * @return statut de l'enseignant : statutaire, vacataire ou générique
	 */
	public String getEnseignantBeanStatut() {
		String statut = null;
		//Si c'est le service d'un enseignant (EOActueEnseignant)
		if (getCurrentItem().service().enseignant() != null) {
			statut = getCurrentItem().service().enseignant().getStatut(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()
					, getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		} else {
			statut = getCurrentItem().service().toEnseignantGenerique().getStatut();
		}
		
		return statut;
	}
	
	public String getEnseignantBeanEtatFicheService() {
		String statut = getEnseignantBeanStatut();

		if (Constante.GENERIQUE.equals(statut)) {
			return Constante.PREVISIONNEL;
		}
		
		if (getCurrentItem().service().toListeRepartService() != null
				&& getCurrentItem().service().toListeRepartService().size() > 0) {
			if (Constante.STATUTAIRE.equals(statut)) {
				if (controleurStatutaire.isCircuitPrevisionnel(getCurrentItem().service().toListeRepartService().get(0).toDemande())) {
					return Constante.PREVISIONNEL;
				} else {
					return Constante.DEFINITIF;
				}
			}
			if (Constante.VACATAIRE.equals(statut)) {
				if (controleurVacataire.isCircuitPrevisionnel(getCurrentItem().service().toListeRepartService().get(0).toDemande())) {
					return Constante.PREVISIONNEL;
				} else {
					return Constante.DEFINITIF;
				}
			}
		}
		return null;
	}

	
	public EOComposant getParentAP() {
		return parentAP;
	}

	public void setAp(EOComposant parentAP) {
		this.parentAP = parentAP;
	}
		
	/**
	 * @return liste des APs de l'UE/EC
	 */
	public List<EOAP> getListeAPs() {
		List<EOAP> listeAPs = new ArrayList<EOAP>();
		
		for (EOLienComposer lienComposer : getControleur().getLienComposerEnfantsTypeAP(getParentAP())) {
			EOAP ap = (EOAP) lienComposer.child();
			listeAPs.add(ap);
		}
		return listeAPs;
	}
	

	
	
	/**
	 * Setter de l'enseignant bean en fonction de l'enseignant à modifier : générique ou pas
	 * @param page : formulaire d'édition de la répartition par UE
	 */
	public void setEnseignantBean(RepartitionParUEForm page) {
		if (getSelectedObject().service().enseignant() != null) {
			page.setEnseignantBean(new EnseignantDataBean(getSelectedObject().service().enseignant(), 
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
		} else {
			page.setEnseignantBean(new EnseignantDataBean(getSelectedObject().service().toEnseignantGenerique(), 
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
		}
	}
	
	/**
	 * Renvoie vrai si la fiche de service de l'enseignant est validée 
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifs() {
		boolean boutonsInactifs = false;

		if (getSelectedObject() == null) {
			return true;
		}

		if (getSelectedObject().toRepartService() != null && (getSelectedObject().service().enseignant() != null)) {
			boutonsInactifs = !getAutorisation().hasDroitUtilisationChemin(getSelectedObject().toRepartService().toDemande(), 
					EtapePeche.VALID_REPARTITEUR, TypeCheminPeche.VALIDER);

			CircuitPeche circuit = CircuitPeche.getCircuit(getSelectedObject().toRepartService().toDemande().toEtape().toCircuitValidation());
			if (circuit != null) {
				boutonsInactifs &= circuit.isCircuitDefinitif();
			}
		}
		return boutonsInactifs;
	}
	
	
	/**
	 * @return l'état de la répartition s'il y en a une.
	 */
	public String getEtatRepartition() {
		return getEtatRepartition(getCurrentItem().etat());
	}
	
	
	@Override
	public int getNumberOfObjectsPerBatch() {
		return 0;
	}
	
	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String tableRepartitionId() {
		return getComponentId() + "_tableRepartition";
	}
	
	    
	public UECtrl getControleur() {
		return controleur;
	}

	private String currentCodePourStyle = null;

	/**
	 * retourne vrai pour affichage trait de séparation entre les types d'AP
	 * dans le tableau
	 * @return vrai/faux
	 */
	public boolean getStyleLigneChangementAP() {
		String code = null;
		if (getCurrentItem() == null) {
			return false;
		}
		code = getCurrentItem().composantAP().typeAP().code();
		if (currentCodePourStyle == null) {
			currentCodePourStyle = code;			
		}
		if (code.compareTo(currentCodePourStyle) != 0) {
			currentCodePourStyle = code;
			return true;			
		} 
		return false;
	}
	
	/**
	 * Retourne vrai si l'enseignant est dans la cible de l'agent connecté
	 * @param service
	 * @return Vrai/Faux
	 */
	public boolean isEnseignantsDansCible(EOService service) {
		boolean isEnseignantDansCible = true;
		
		if (getSelectedObject() != null) {
			boolean isEnseignantStatutaire = getSelectedObject().service().iEnseignant().isStatutaire(
					getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			
			if (isEnseignantStatutaire) {
				isEnseignantDansCible = getAutorisation().isEnseignantContenueDansCibleEnseignantStatutaire(getSelectedObject().service().iEnseignant());
			} else {
				//TODO CL : mis en place temporairement pour les vacataires avant la gestion par affectation
				IStructure structure = getSelectedObject().toRepartService().toStructure();
				isEnseignantDansCible = getAutorisation().isStructureDansCibleEnseignementVacataire(structure);
			}
		}
		
		return isEnseignantDansCible;
	}
	
	/**
	 * @param structure : structure
	 * @return Structure de type Compsante de Scolarité
	 */
	public IStructure getStructureCS(IStructure structure) {
		return controleur.getComposanteTypeCS(structure);
	}
	
	/**
	 * @param structure : structure
	 * @return Structure de type Departement d'Enseignement
	 */
	public IStructure getStructureDE(IStructure structure) {
		return controleur.getDepartementEnseignement(structure);
	}
	
	/**
	 * @param structure : structure
	 * @return le libellé court de la structure si renseigné sinon chaine vide
	 */
	private String libelleCourtNonNull(IStructure structure) {
		String libelle = "";
		
		if (structure != null) {
			libelle = structure.lcStructure();
		}
		
		return libelle;
	}
	
}
