package org.cocktail.peche.components.enseignements;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

public class CriteresRechercheUeEc {

	
	private String codeUeEc;
	private String libelleUeEc;
	
	private IStructure structure;

	private boolean rechercheFichePrev;
	private boolean rechercheFicheDef;
	
	public String getCodeUeEc() {
		return codeUeEc;
	}

	public void setCodeUeEc(String codeUeEc) {
		this.codeUeEc = codeUeEc;
	}

	public String getLibelleUeEc() {
		return libelleUeEc;
	}

	public void setLibelleUeEc(String libelleUeEc) {
		this.libelleUeEc = libelleUeEc;
	}


	public boolean isRechercheFichePrev() {
		return rechercheFichePrev;
	}

	public void setIsRechercheFichePrev(boolean rechercheFichePrev) {
		this.rechercheFichePrev = rechercheFichePrev;
	}

	public boolean isRechercheFicheDef() {
		return rechercheFicheDef;
	}

	public void setIsRechercheFicheDef(boolean rechercheFicheDef) {
		this.rechercheFicheDef = rechercheFicheDef;
	}

	public IStructure getStructure() {
		return structure;
	}

	public void setStructure(IStructure structure) {
		this.structure = structure;
	}
	
}
