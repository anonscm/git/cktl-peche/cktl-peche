package org.cocktail.peche.components.enseignements;

import org.cocktail.peche.Messages;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.enseignants.CktlPecheFormServiceEnseignantVacataire;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartEnseignant;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

/**
 * Classe permettant d'afficher les enseignants affectés sur un AP
 * 
 * @author Chama LAATIK
 */
public class ListeRepartitionParUE extends CktlAbstractRepartitionParUE {
	
	private static final long serialVersionUID = -979788725638516362L;
	private boolean isNotModifiable;
	private String selectedItemId;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le contexte d'accès à la base de données.
	 */
    public ListeRepartitionParUE(WOContext context) {
        super(context);
        
        setNotModifiable(false);
    }
    
	@Override
	public void awake() {
		super.awake();

		if (hasBinding("isNotModifiable")) {
			setNotModifiable((Boolean) valueForBinding("isNotModifiable"));
		} else {
			setNotModifiable(true);
		}
		if (hasBinding("typeFiche")) {
			setTypeFiche((String) valueForBinding("typeFiche"));
		}
		if (hasBinding("selectedItemId")) {
			setSelectedItemId((String) valueForBinding("selectedItemId"));
		}
		if (hasBinding("modeConsultation")) {
			setModeConsultation((Boolean) valueForBinding("modeConsultation"));
		}
				
	}

	public boolean getBoutonsInactifsAndIsNotModifiable() {
		if (isNotModifiable()) {
			return true;
		} else {
			boolean enseignantDansCibleKO = true;
			
			if (getSelectedObject() != null) {
				enseignantDansCibleKO = !isEnseignantsDansCible(getSelectedObject().service());
			}
			
			return getBoutonsInactifs() || enseignantDansCibleKO;
		}
	}
	
	/**
	 * @return une instance du formulaire en mode ajout.
	 */
	public WOActionResults ajouterAction() {
		RepartitionParUEForm page =  (RepartitionParUEForm) pageWithName(RepartitionParUEForm.class.getName());
		page.setModeEdition(ModeEdition.CREATION);
		page.setTypeFiche(getTypeFiche());
		page.setSelectedItemId(getSelectedItemId());
		page.setParentAP(getParentAP());
		page.setListeAPs(getListeAPs());
		
		return page;
	}
	
	
	/**
	 * Supprime l'élement sélectionné
	 * @return null
	 */
	public WOActionResults deleteAction() {
		try {
			if (getSelectedObject() != null) {
				if (isEnseignantAutreComposante(getSelectedObject())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_UE_INTERDIT_SUPPRESSION);
					return null;
				}
				
				EORepartEnseignant.supprimerRepartition(getSelectedObject(), edc());
			
				edc().deleteObject(getSelectedObject());
				edc().saveChanges();
				setSelectedObject(null);
				getDisplayGroup().fetch();
			}
		} catch (Exception e) {
			edc().invalidateAllObjects();
			edc().revert();
		}

		return doNothing();
	}
	
	/**
	 * Renvoie vrai si 
	 *    le répartiteur n'appartient pas à la meme composante que l'enseignant
	 * et le service détail n'est pas en attente (soit Null soit Accepté/Refusé) 
	 * 
	 * @param serviceDetail : détail du service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean isEnseignantAutreComposante(EOServiceDetail serviceDetail) {
		if ((serviceDetail.service().enseignant() != null) && (serviceDetail.service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(serviceDetail.service().enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))
					&& !(Constante.ATTENTE.equals(serviceDetail.etat()));
		}
		return false;
	}
	
	/**
	 * @return Message avant suppression
	 */
	@Override
	public String onClickBefore() {
		String msgConfirm = message(Messages.SERVICE_ALERTE_CONFIRM_SUPPRESS);
		return "confirm('" + msgConfirm + "')";
	}
	
	/**
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {		
		EOServiceDetail serviceDetail = getSelectedObject();		
		EOActuelEnseignant enseignant = null;
		
		if (serviceDetail != null) {
			enseignant = serviceDetail.service().enseignant();
		}
		
		if (enseignant != null 
				&& !enseignant.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			CktlPecheFormServiceEnseignantVacataire page = (CktlPecheFormServiceEnseignantVacataire) pageWithName(CktlPecheFormServiceEnseignantVacataire.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setEditedObject(serviceDetail);
			page.setRepartService(serviceDetail.toRepartService());
			page.setModeParListeRepartition(true);
			this.session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
			return page;			
		}		
		
		if (serviceDetail != null) {
			RepartitionParUEForm page =  (RepartitionParUEForm) pageWithName(RepartitionParUEForm.class.getName());
			page.setModeEdition(ModeEdition.MODIFICATION);
			page.setTypeFiche(getTypeFiche());
			page.setSelectedItemId(getSelectedItemId());
			setEnseignantBean(page);
			page.setParentAP(getParentAP());
			page.setListeAPs(getListeAPs());
			return page;			
		} 
		
		return null;
	}
		
	/**
	 * Affichage des boutons pour la répartition accessible uniquement au répartiteur
	 * @return vrai/faux
	 */
	public boolean getAffichageBoutons() {
		if (isModeConsultation()) {
			return false;
		}
		return getFonctionRepartiteur();
	}

	public boolean isNotModifiable() {
		return isNotModifiable;
	}

	public void setNotModifiable(boolean isNotModifiable) {
		this.isNotModifiable = isNotModifiable;
	}

	@Override
	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}
}