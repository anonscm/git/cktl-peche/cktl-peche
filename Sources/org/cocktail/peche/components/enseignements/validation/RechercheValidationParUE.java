package org.cocktail.peche.components.enseignements.validation;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.BasicPecheObservateur;
import org.cocktail.peche.components.enseignements.CriteresRechercheUeEc;
import org.cocktail.peche.components.enseignements.ListeAP;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class RechercheValidationParUE extends CktlPecheBaseComponent implements BasicPecheObservateur {

	private EOStructure currentStructure;
	private EOStructure selectedStructure;

	private BasicPecheCtrl controleur;

	private CriteresRechercheUeEc criteresRecherche;
	private boolean affichageResultats;

	private EOAP apSelected;
	private String selectedItemId;
	
	
	public RechercheValidationParUE(WOContext context) {
		super(context);
		controleur = new BasicPecheCtrl(edc());
		criteresRecherche = new CriteresRechercheUeEc();
		this.controleur.addObservateur(this);
	}

	public WOActionResults rechercherUeEc() {
		affichageResultats = true;
		return null;
	}

	@Override
	public void setTypeFiche(String typeFiche) {		
		super.setTypeFiche(typeFiche);
		criteresRecherche.setIsRechercheFicheDef(true);
		criteresRecherche.setIsRechercheFichePrev(false);
		if (Constante.PREVISIONNEL.equals(typeFiche)) {
			criteresRecherche.setIsRechercheFicheDef(false);
			criteresRecherche.setIsRechercheFichePrev(true);
		}		
	}
	/**
	 * 
	 * @return List<IStructure>
	 */
	public List<IStructure> listeStructure() {
		return getPecheSession().getPecheAutorisationCache().getListeCibleEnseignement();
	}

	/**
	 * 
	 * @return true / false
	 */
	public boolean affichageUniqueStructure() {
		if (!CollectionUtils.isEmpty(listeStructure()) && listeStructure().size() == 1) {
			criteresRecherche.setStructure(listeStructure().get(0));
			return true;
		}
		return false;
	}


	/**
	 * @return the currentStructure
	 */
	public EOStructure currentStructure() {
		return currentStructure;
	}


	/**
	 * @param currentStructure the currentStructure to set
	 */
	public void setCurrentStructure(EOStructure currentStructure) {
		this.currentStructure = currentStructure;
	}


	/**
	 * @return the selectedStructure
	 */
	public EOStructure selectedStructure() {
		return selectedStructure;
	}


	/**
	 * @param selectedStructure the selectedStructure to set
	 */
	public void setSelectedStructure(EOStructure selectedStructure) {
		this.selectedStructure = selectedStructure;
	}

	public CriteresRechercheUeEc getCriteresRecherche() {
		return criteresRecherche;
	}

	public void setCriteresRecherche(CriteresRechercheUeEc criteresRecherche) {
		this.criteresRecherche = criteresRecherche;
	}

	/**
	 * @return the affichageResultats
	 */
	public boolean affichageResultats() {
		return affichageResultats;
	}

	/**
	 * @param affichageResultats the affichageResultats to set
	 */
	public void setAffichageResultats(boolean affichageResultats) {
		this.affichageResultats = affichageResultats;
	}

	public boolean boutonDetailInactif() {
		if (getApSelected() == null) {
			return true;
		}
		if (getApSelected().parents() != null && getApSelected().parents().size() > 0) {
			return false;
		}
		return true;
	}

	public void notifier() {
		EOAP ap = this.controleur.getAP();
		setApSelected(ap);		
	}

	public EOAP getApSelected() {
		return apSelected;
	}

	public void setApSelected(EOAP apSelected) {
		this.apSelected = apSelected;
	}

	public BasicPecheCtrl getControleur() {
		return controleur;
	}

	public WOActionResults validationParUeEc() {
		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
		page.setParentAP((EOComposant) getApSelected().parents().get(0));
		page.setSelectedItemId(getSelectedItemId());		
		page.setModeValidationParUeEc(true);
		page.setTypeFicheAValider(criteresRecherche.isRechercheFicheDef() ? Constante.DEFINITIF : Constante.PREVISIONNEL);
		this.session().setObjectForKey(this, Session.BOUTONRETOUR);
		return page;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}



}