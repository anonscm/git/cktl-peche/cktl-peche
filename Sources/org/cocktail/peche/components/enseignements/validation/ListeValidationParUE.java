package org.cocktail.peche.components.enseignements.validation;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.enseignants.CktlPecheFormServiceEnseignantStatutaire;
import org.cocktail.peche.components.enseignants.CktlPecheFormServiceEnseignantVacataire;
import org.cocktail.peche.components.enseignements.CktlAbstractRepartitionParUE;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSMutableArray;

public class ListeValidationParUE extends CktlAbstractRepartitionParUE {

	private static final long serialVersionUID = 5790712966324801296L;

	private NSMutableArray<EOServiceDetail> listeServiceDetailAValider = new NSMutableArray<EOServiceDetail>();

	private String typeFicheAValider;

	private boolean checkTout;

	public ListeValidationParUE(WOContext context) {
		super(context);	
	}

	/**
	 * 
	 * @return même page
	 */
	public WOActionResults validerAction() {
		for (EOServiceDetail serviceDetail : listeServiceDetailAValider) {
			serviceDetail.setEtat(Constante.VALIDE);
			getControleur().recalculerIndicateurValidationAuto(serviceDetail.toRepartService());
		}
		edc().saveChanges();
		listeServiceDetailAValider.clear();
		return null;
	}

	/**
	 * 
	 * @return true/false
	 */
	public boolean boutonModifierInactif() {
		boolean enseignantDansCibleKO = true;
		
		if (getSelectedObject() != null) {
			enseignantDansCibleKO = !isEnseignantsDansCible(getSelectedObject().service());
		}
		
		return getBoutonsInactifs() || enseignantDansCibleKO;
	}

	/**
	 * 
	 * @return true/false
	 */
	public boolean boutonValiderInactif() {
		return NSArrayCtrl.isEmpty(listeServiceDetailAValider);
	}

	public boolean isSelectedServiceDetailItem() {
		return listeServiceDetailAValider.contains(getCurrentItem());
	}

	public void setIsSelectedServiceDetailItem(boolean value) {
		if (value && !isSelectedServiceDetailItem()) {
			listeServiceDetailAValider.addObject(getCurrentItem());
		} else if (!value && isSelectedServiceDetailItem()) {
			listeServiceDetailAValider.removeIdenticalObject(getCurrentItem());
		}
	}

	public boolean afficherValidationPossible() {

		if (getCurrentItem().service().toEnseignantGenerique() != null) {
			return false;
		}
		
		if (Constante.VALIDE.equals(getCurrentItem().etat())) {
			return false;	
		}

		
		if (typeFicheAValider != null)  {
			if (!typeFicheAValider.equals(getEnseignantBeanEtatFicheService())) {
				return false;
			}
		}
	
		if (getCurrentItem().service().enseignant() != null) {
			EOActuelEnseignant enseignant = getCurrentItem().service().enseignant();			
			if (!checkPerimetreUtilisateur(enseignant) 
					&& (!Constante.ACCEPTE.equals(getCurrentItem().etat()))) {
				// cas répartition croisée
				// l'enseignant n'est pas dans la cible enseignant 
				// et il n'a pas l'état "accepté" de la répartition croisée
				// sinon on autorise la validation
				// TODO: (LPR) Ce test répartition croisée est à revoir
				// en amont car les enseignants d'autres périmètres ne sont pas 
				// pris dans la recherche initiale
				return false;
			}
		}
	

		EOEtape etape;
		try {
			etape = getCurrentItem().toRepartService().toDemande().toEtape();
		} catch (Exception e) {
			return false;
		}

		if (etape == null) {
			return false;
		}
		
		if (!getFonctionRepartiteur() && etape.codeEtape().equals(EtapePeche.VALID_REPARTITEUR.getCodeEtape())) {
			return false;
		}
		if (!getFonctionDirecteurComposante() && etape.codeEtape().equals(EtapePeche.VALID_DIR_COMPOSANTE.getCodeEtape())) {
			return false;
		}
		if (!getFonctionDirecteurDepartement() && etape.codeEtape().equals(EtapePeche.VALID_DIR_DEPARTEM.getCodeEtape())) {
			return false;
		}

		return true;
	}

	
	/**
	 * Vérification du périmètre de l'utilisateur connecté
	 * @param enseignant un enseignant
	 * @param listeStructuresStat 
	 * @param listeStructuresVac
	 * @return
	 */
	private boolean checkPerimetreUtilisateur(EOActuelEnseignant enseignant) {
		boolean checkOK = true;
		List<IStructure> listeStructuresStat = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsStatutaires();
		List<IStructure> listeStructuresVac = getPecheSession().getPecheAutorisationCache().getListeCibleEnseignantsVacataire();
		
		if (CollectionUtils.isNotEmpty(listeStructuresStat) || CollectionUtils.isNotEmpty(listeStructuresVac)) {
			EOStructure departement = enseignant.getDepartementEnseignement(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			EOStructure composante = enseignant.getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());

			if (enseignant.isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
			
				checkOK = isDepCompContenuDansPerimetreUtilisateur(listeStructuresStat,
						departement, composante);
				
			} else {
				checkOK = isDepCompContenuDansPerimetreUtilisateur(listeStructuresVac,
						departement, composante);
			}
		}
		return checkOK;
	}

	private boolean isDepCompContenuDansPerimetreUtilisateur(List<IStructure> listeStructures, EOStructure departement, EOStructure composante) {
		boolean isContenuDansPerimetreUtilisateur = true;
		if (departement != null && !listeStructures.contains(departement)) {
			isContenuDansPerimetreUtilisateur = false;
		} else if (composante != null && !listeStructures.contains(composante)) {
			isContenuDansPerimetreUtilisateur = false;
		}
		return isContenuDansPerimetreUtilisateur;
	}

	/**
	 * Renvoie vrai si la personne connectée manipule un individu qui n'appartient pas au périmètre
	 * de ses structures (les structures de ces composantes scolarité).
	 * ==> permet de savoir si on est dans le cas d'une répartition croisée ou non
	 * 
	 * @param enseignant L'enseignant manipulé
	 * @param service : service de l'enseignant
	 * @return vrai/faux
	 */
	public boolean isRepartitionCroisee(EOService service) {
		if (service.enseignant() != null) {
			return !checkPerimetreUtilisateur(service.enseignant());
		}
		return false;		
		/*if ((service.enseignant() != null) && (service.enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()))) {
			EOStructure composanteEnseignant = service.enseignant().getComposante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			return !getPecheSession().getPecheAutorisationCache().isComposanteContenueDansCibleEnseignant(composanteEnseignant);
		}

		return false;*/
	}

	@Override
	public void awake() {
		super.awake();
		if (hasBinding("typeFicheAValider")) {
			typeFicheAValider = (String) valueForBinding("typeFicheAValider");
		}
	}


	/**
	 * @return une instance du formulaire en mode modification.
	 */
	public WOActionResults modifierAction() {
		if (getSelectedObject() != null) {
			if (!getSelectedObject().service().enseignant().isStatutaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
					getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
				CktlPecheFormServiceEnseignantVacataire page = (CktlPecheFormServiceEnseignantVacataire) pageWithName(CktlPecheFormServiceEnseignantVacataire.class.getName());
				page.setModeEdition(ModeEdition.MODIFICATION);
				page.setEditedObject(getSelectedObject());				
				page.setRepartService(getSelectedObject().toRepartService());
				page.setModeReh(false);
				page.setModeValidationUeEc(true);
				page.setTypeFicheAValider(typeFicheAValider);
				return page;
			} else {
				CktlPecheFormServiceEnseignantStatutaire page = (CktlPecheFormServiceEnseignantStatutaire) pageWithName(CktlPecheFormServiceEnseignantStatutaire.class.getName());
				page.setModeEdition(ModeEdition.MODIFICATION);
				page.setEditedObject(getSelectedObject());
				page.setEnseignant(getSelectedObject().service().enseignant());
				page.setRepartService(getSelectedObject().toRepartService());
				page.setRepartitionREH(false);
				page.setModeValidationUeEc(true);
				page.setTypeFicheAValider(typeFicheAValider);
				return page;
			}
		} else {
			return null;
		}
	}

	public String getTypeFicheAValider() {
		return typeFicheAValider;
	}

	public void setTypeFicheAValider(String typeFicheAValider) {
		this.typeFicheAValider = typeFicheAValider;
	}

	public boolean isCheckTout() {
		return checkTout;
	}

	public void setCheckTout(boolean checkTout) {
		this.checkTout = checkTout;
	}


}