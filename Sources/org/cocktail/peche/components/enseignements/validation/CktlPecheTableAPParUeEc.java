package org.cocktail.peche.components.enseignements.validation;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.components.controlers.OffreFormationCtrl;
import org.cocktail.peche.components.enseignements.CktlAbstractPecheTableAP;
import org.cocktail.peche.components.enseignements.CriteresRechercheUeEc;
import org.cocktail.peche.components.enseignements.ListeAP;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

public class CktlPecheTableAPParUeEc extends CktlAbstractPecheTableAP {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5803410030279308808L;

	private CriteresRechercheUeEc criteresRecherche;

	private EOQualifier qualifier;
	private OffreFormationCtrl offreFormationCtrl;

	private List<IStructure> listeStructure;
	
	private String selectedItemId;

	public CktlPecheTableAPParUeEc(WOContext context) {
		super(context);
		offreFormationCtrl = new OffreFormationCtrl(this.edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}

	/**
	 * @return the nbHeuresRealisees
	 */
	public float nbHeuresRealisees() {
		return getControleurUE().getTotalHeuresRealisees(getCurrentItem(), false);
	}

	/**
	 * @return the nbHeuresAttribueesPrev
	 */
	public float nbHeuresAttribueesPrev() {
		return getControleurUE().getTotalHeuresPrevues(getCurrentItem(), false);
	}

	@Override
	public void awake() {
		super.awake();
		if (hasBinding("criteresRecherche")) {
			criteresRecherche = (CriteresRechercheUeEc) valueForBinding("criteresRecherche");
		}

		if (hasBinding("listeStructure")) {
			listeStructure = (List<IStructure>) valueForBinding("listeStructure");
		} 
		
		if (hasBinding("typeFiche")) {
			setTypeFiche((String) valueForBinding("typeFiche"));
		}

		if (hasBinding("modeConsultation")) {
			setModeConsultation((Boolean) valueForBinding("modeConsultation"));
		}	

		if (hasBinding("selectedMenu")) {
			setSelectedItemId((String) valueForBinding("selectedMenu"));
		}
				
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		NSArray<Integer> listeIdsUeEc = new NSMutableArray<Integer>();

		if (!StringCtrl.isEmpty(criteresRecherche.getCodeUeEc()) || !StringCtrl.isEmpty(criteresRecherche.getLibelleUeEc())) {			
			if (criteresRecherche.getStructure() == null) {
				if (isModeConsultation()) {
					// mode consultation et toutes structures = toutes structures de l'établissement
					// donc inutile de filtrer par structures 
					listeIdsUeEc = lireIdsUEECDiplomants(criteresRecherche.getCodeUeEc(), criteresRecherche.getLibelleUeEc(), null);
				} else {
					listeIdsUeEc = lireIdsUEECDiplomants(criteresRecherche.getCodeUeEc(), criteresRecherche.getLibelleUeEc(), listeStructure);
				}
			} else {
				List<IStructure> liste = new ArrayList<IStructure>();
				liste.add(criteresRecherche.getStructure());
				listeIdsUeEc = lireIdsUEECDiplomants(criteresRecherche.getCodeUeEc(), criteresRecherche.getLibelleUeEc(), liste);
			}
		}
		// recherche des AP ayant comme parent un EC/EC de la liste listeIdsUeEc
		qualifier = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.ID).in(listeIdsUeEc);
		ERXSortOrderings sortOrderings = EOAP.LIENS_CHILDS.dot(EOLien.PARENT).dot(EOComposant.CODE).ascs();
		fetchDisplay(qualifier, EOAP.ENTITY_NAME, sortOrderings);		
		super.appendToResponse(response, context);	

	}

	/**
	 * Lire les enseignements diplômants (dans l'établissement) contenant un composante EC ou UE ayant le code passé en paramètre.
	 * @param codeUeEc Le code UE ou EC à rechercher
	 * @param libelleUeEc Le libelle UE ou EC à rechercher
	 * @param structuresRecherche liste des structures de recherche
	 * @return liste d'Id d'UE/EC
	 */
	public NSArray<Integer> lireIdsUEECDiplomants(String codeUeEc, String libelleUeEc, List<IStructure> structuresRecherche) {
		int anneeEnCours = getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours();
		return offreFormationCtrl.rechercherIdsUeEcDiplomants(edc(), codeUeEc, libelleUeEc, structuresRecherche, anneeEnCours);

	}

	public NSArray<String> getTableauAutresParents() {
		NSArray<String> mutualises =  new NSMutableArray<String>();
		if (getCurrentItem().parents() != null 
				&& !getCurrentItem().parents().isEmpty() 
				&& getCurrentItem().parents().get(0).parents() != null
				&& !getCurrentItem().parents().get(0).parents().isEmpty()) {
			for (IComposant c : getCurrentItem().parents().get(0).parents()) {
				StringBuffer libelle = new StringBuffer(c.libelle());

				IComposant item = c;
				// on remonte dans l'arborescence 
				while (item.parents() != null && !item.parents().isEmpty()) {
					if (item.parents().get(0) instanceof EOVersionDiplome) {
						// pas d'affichage de l'année version du diplôme
					} else {
						libelle = libelle.insert(0, item.parents().get(0).libelle() + " - ");
					}
					item = item.parents().get(0);
				}
				mutualises.add(libelle.toString());
			}
		}
		return mutualises;
	}

	public String getTableauParents() {
		StringBuffer libelle = new StringBuffer("");
		if (getCurrentItem().parents() != null 
				&& getCurrentItem().parents().size() > 0 
				&& getCurrentItem().parents().get(0).parents() != null
				&& !getCurrentItem().parents().get(0).parents().isEmpty()) { 

			IComposant c = getCurrentItem().parents().get(0).parents().get(0);
			libelle = new StringBuffer(c.libelle());

			IComposant item = c;

			while (item.parents() != null && !item.parents().isEmpty()) {
				if (item.parents().get(0) instanceof EOVersionDiplome) {
					// pas d'affichage de l'année version du diplôme
				} else {
					libelle = libelle.insert(0, item.parents().get(0).libelle() + " - ");
				}
				item = item.parents().get(0);
			}

		}

		return libelle.toString();
	}

	public String getCurrentCodeParent() {
		return currentCodeParent;
	}
	public void setCurrentCodeParent(String currentCodeParent) {
		this.currentCodeParent = currentCodeParent;
	}

	private String currentCodeParent;

	public String getSrcImage() {
		if (getCurrentItem().parents() != null 
				&& !getCurrentItem().parents().isEmpty()) { 

			if (getCurrentItem().parents().get(0).typeComposant() != null) {
				return ((EOTypeComposant)getCurrentItem().parents().get(0).typeComposant()).srcImage();
			}
		}
		return "images/Matrice/default.png";

	}
	
	public WOActionResults ouvrirParentAP() {
		ListeAP page = (ListeAP) pageWithName(ListeAP.class.getName());
		page.setParentAP((EOComposant) getCurrentItem().parents().get(0));
		page.setSelectedItemId(getSelectedItemId());		
		page.setTypeFiche(getTypeFiche());	
		page.setModeConsultation(isModeConsultation());			
		session().setObjectForKey(this.parent(), Session.BOUTONRETOUR);
		return page;
	}

	public String getSelectedItemId() {
		return selectedItemId;
	}

	public void setSelectedItemId(String selectedItemId) {
		this.selectedItemId = selectedItemId;
	}

}