package org.cocktail.peche.components.enseignements;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;

/**
 * Interface à implémenter pour recéptionner l'UE choisie via l'arbre de l'offre de formation.
 */
public interface IRetourUeSelectionnee {
	/**
	 * La méthode qui sera appelée lors du choix de l'UE.
	 * 
	 * @param libelleUe Le libellé de l'UE
	 * @param listeAps La liste des AP de cette UE
	 */
	void setUeSelectionnee(String libelleUe, List<EOAP> listeAps);
}
