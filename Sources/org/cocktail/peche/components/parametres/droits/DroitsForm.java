package org.cocktail.peche.components.parametres.droits;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.entity.EODroit;
import org.cocktail.peche.entity.EOTypePopulation;
import org.cocktail.peche.metier.finder.DroitsFinder;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class DroitsForm extends PecheCreationForm<EODroit> {
    
	private EOIndividu selectedUtilisateur;
	
	private IStructure selectedStructureEnseignant;
	private IStructure selectedStructureEnseignement;
	
	private boolean cibleEnseignementsAvecEnfants;
	private boolean cibleEnseignantsAvecEnfants;
	
	private EOGdProfil currentProfil;
	private EOGdProfil selectedProfil;
	private List<EOGdProfil> listeTousProfils;
	
	private EOTypePopulation typePopulationSelected;
	private EOTypePopulation currentTypePopulation;
	private List<EOTypePopulation> listeTypePopulation;
	
	private BasicPecheCtrl ctrl;
	
	public DroitsForm(WOContext context) {
        super(context);
        initialiser();
        ctrl = new BasicPecheCtrl(edc());
    }

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.DROITS_FORM_TITRE_AJOUTER;
	}
	
	@Override
	protected String getFormTitreModificationKey() {
		return Messages.DROITS_FORM_TITRE_MODIFIER;
	}

	@Override
	protected WOActionResults pageGestion() {
		TableDroits page = (TableDroits) pageWithName(TableDroits.class.getName());
		return page;
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		controlerSaisie();
		gererCreationDroit();
	}

	/**
	 * Fait ici car non utilisation de l'editedObject dans le html
	 */
	private void gererCreationDroit() {
		editedObject().setToIndividuRelationship(getSelectedUtilisateur());
		editedObject().setToProfilRelationship(getSelectedProfil());
		editedObject().setToStructureEnseignantRelationship((EOStructure) getSelectedStructureEnseignant());
		editedObject().setToStructureEnseignementRelationship((EOStructure) getSelectedStructureEnseignement());
		editedObject().setTemoinEnseignantEnfant(isCibleEnseignantsAvecEnfants());
		editedObject().setTemoinEnseignementEnfant(isCibleEnseignementsAvecEnfants());
		editedObject().setToTypePopulationRelationship(getTypePopulationSelected());
		
		if (modeCreation()) {
			editedObject().majDonnesAuditCreation(getPecheSession().getApplicationUser().getPersonne());
		} else {
			editedObject().majDonnesAuditModification(getPecheSession().getApplicationUser().getPersonne());
		}
	}
	
	@Override
	public void setEditedObject(EODroit editedObject) {
		super.setEditedObject(editedObject);	
		if (modeModification()) {
			setSelectedUtilisateur((EOIndividu) (editedObject().getToIndividu()));
			setSelectedProfil(editedObject().getToProfil());		
			setSelectedStructureEnseignement(editedObject().getToStructureEnseignement());
			setIsCibleEnseignementsAvecEnfants(editedObject().isTemoinEnseignementEnfant());		
			setSelectedStructureEnseignant(editedObject().getToStructureEnseignant());
			setIsCibleEnseignantsAvecEnfants(editedObject().isTemoinEnseignantEnfant());		
			setTypePopulationSelected((EOTypePopulation) (editedObject().getToTypePopulation()));
		}
	}
	/**
	 * Contrôler la saisie utilisateur.
	 * 
	 * Lève une exception si un contrôle est KO.
	 */
	private void controlerSaisie() throws ValidationException {
		
		// Contrôles surfaciques 
		if (getSelectedUtilisateur() == null) {
			throw new ValidationException(String.format(message(Messages.SAISIE_DROITS_ERR_UTILISATEUR)));
		}
		if (getSelectedProfil() == null) {
			throw new ValidationException(String.format(message(Messages.SAISIE_DROITS_ERR_PROFIL)));
		}
		if (getSelectedStructureEnseignant() == null) {
			throw new ValidationException(String.format(message(Messages.SAISIE_DROITS_ERR_CIBLE_ENSEIGNANT)));
		}
		if (getSelectedStructureEnseignement() == null) {
			throw new ValidationException(String.format(message(Messages.SAISIE_DROITS_ERR_CIBLE_ENSEIGNEMENT)));
		}
		
		// Controles métiers
		List<EODroit> listeDroits = DroitsFinder.sharedInstance().getListeDroitsUtilisateur(edc(), getSelectedUtilisateur());
		for (EODroit eoDroit : listeDroits) {
			boolean erreur = false;
			if (modeCreation()) {
				erreur = !eoDroit.toProfil().equals(getSelectedProfil());					
			} else {
				// en modification, on peut changer le profil de l'objet selectionné
				erreur = !eoDroit.toProfil().equals(getSelectedProfil()) 
						&& !eoDroit.id().equals(editedObject().id());
			}
			if (erreur) {
				throw new ValidationException(String.format(message(Messages.SAISIE_DROITS_ERR_INDIVIDU_PLUSIEURS_PROFIL), eoDroit.toProfil().prLc()));				
			}
		}
	}
	

	@Override
	protected EODroit creerNouvelObjet() {
		return EODroit.creerEtInitialiser(edc());
	}

	
	private void initialiser() {		
		setSelectedUtilisateur(null);
		reinitialiserChampsStructures();
		setListeTousProfils(DroitsFinder.sharedInstance().getListeProfilVisible(edc()));
		setTypePopulationSelected(EOTypePopulation.getTypePopulationTous(edc()));
	}

	private void reinitialiserChampsStructures() {
		setSelectedStructureEnseignant(null);
		setSelectedStructureEnseignement(null);
		setIsCibleEnseignantsAvecEnfants(true);
		setIsCibleEnseignementsAvecEnfants(true);
	}
	
	public IStructure getSelectedStructureEnseignant() {
		return selectedStructureEnseignant;
	}

	public void setSelectedStructureEnseignant(
			IStructure selectedStructureEnseignant) {
		this.selectedStructureEnseignant = selectedStructureEnseignant;
	}

	public IStructure getSelectedStructureEnseignement() {
		return selectedStructureEnseignement;
	}

	public void setSelectedStructureEnseignement(
			IStructure selectedStructureEnseignement) {
		this.selectedStructureEnseignement = selectedStructureEnseignement;
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_GESTION_DROITS.getId();
	}

	public boolean isCibleEnseignantsAvecEnfants() {
		return cibleEnseignantsAvecEnfants;
	}

	public boolean isCibleEnseignantsSansEnfants() {
		return !isCibleEnseignantsAvecEnfants();
	}
	
	public void setIsCibleEnseignementsAvecEnfants(
			boolean cibleEnseignementsAvecEnfants) {
		this.cibleEnseignementsAvecEnfants = cibleEnseignementsAvecEnfants;
	}

	public void setIsCibleEnseignantsAvecEnfants(boolean cibleEnseignantsAvecEnfants) {
		this.cibleEnseignantsAvecEnfants = cibleEnseignantsAvecEnfants;
	}

	public boolean isCibleEnseignementsAvecEnfants() {
		return cibleEnseignementsAvecEnfants;
	}
	
	public boolean isCibleEnseignementsSansEnfants() {
		return !isCibleEnseignementsAvecEnfants();
	}

	public EOIndividu getSelectedUtilisateur() {
		return selectedUtilisateur;
	}

	public void setSelectedUtilisateur(EOIndividu selectedUtilisateur) {
		this.selectedUtilisateur = selectedUtilisateur;
		if (getSelectedUtilisateur() != null) {
			setSelectedStructureEnseignant(ctrl.getStructureAffectationPrincipale(getSelectedUtilisateur()));
			setSelectedStructureEnseignement(ctrl.getStructureAffectationPrincipale(getSelectedUtilisateur()));
		}
	}
	

	public EOGdProfil getCurrentProfil() {
		return currentProfil;
	}

	public EOGdProfil getSelectedProfil() {
		return selectedProfil;
	}

	public List<EOGdProfil> getListeTousProfils() {
		Collections.sort(listeTousProfils, new Comparator<EOGdProfil>() {
			public int compare(EOGdProfil  profil1, EOGdProfil  profil2) {
				return profil1.prLc().compareTo(profil2.prLc());
			}
		});
		return listeTousProfils;
	}

	public void setCurrentProfil(EOGdProfil currentProfil) {
		this.currentProfil = currentProfil;
	}

	public void setSelectedProfil(EOGdProfil selectedProfil) {
		this.selectedProfil = selectedProfil;
	}

	public void setListeTousProfils(List<EOGdProfil> listeTousProfils) {
		this.listeTousProfils = listeTousProfils;
	}

	public String getCiblesEnsId() {
		return "CiblesEnsId";
	}
	
	public EOTypePopulation getTypePopulationSelected() {
		return typePopulationSelected;
	}

	public void setTypePopulationSelected(EOTypePopulation typePopulationSelected) {
		this.typePopulationSelected = typePopulationSelected;
	}

	@Override
	protected void reinitialiserChamps() {
		reinitialiserChampsStructures();
		super.reinitialiserChamps();
	}

	public List<EOTypePopulation> getListeTypePopulation() {
		if (listeTypePopulation == null) {
			listeTypePopulation = EOTypePopulation.getListeTypePopulation(edc());
		}
		return listeTypePopulation;
	}

	public void setListeTypePopulation(List<EOTypePopulation> listeTypePopulation) {
		this.listeTypePopulation = listeTypePopulation;
	}

	public EOTypePopulation getCurrentTypePopulation() {
		return currentTypePopulation;
	}

	public void setCurrentTypePopulation(EOTypePopulation currentTypePopulation) {
		this.currentTypePopulation = currentTypePopulation;
	}

	public boolean modeModification() {
		return !modeCreation();
	}
	
}