package org.cocktail.peche.components.parametres.droits;

import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.CktlPecheCRUDTableComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.entity.EODroit;
import org.cocktail.peche.metier.finder.DroitsFinder;

import com.webobjects.appserver.WOContext;

/**
 * Table de gestion du périmètre des données
 * 
 * @author Chama LAATIK
 */
public class TableDroits extends CktlPecheCRUDTableComponent<EODroit, DroitsForm> {
	
	private static final long serialVersionUID = -2665790347288939295L;

	private BasicPecheCtrl controleur = null;
	
	/**
	 * Constructeur.
	 * @param context le context WebObjects.
	 */
	public TableDroits(WOContext context) {
		super(context);
		controleur = new BasicPecheCtrl(edc());
		getDisplayGroup().setObjectArray(DroitsFinder.sharedInstance().getListeDroits(edc()));
	}
	
	public boolean isCibleEnseignementAvecEnfants() {
		return getCurrentItem().isTemoinEnseignementEnfant();
	}
	
	public boolean isCibleEnseignantAvecEnfants() {
		return getCurrentItem().isTemoinEnseignantEnfant();
	}
	
	/**
	 * Affichage des boutons grisés ou non si ligne non selectionnée
	 * @return vrai/faux
	 */
	public boolean getBoutonsInactifs() {
		return getSelectedObject() == null;
	}
	
	/**
	 * @return the menuSelected
	 */
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_GESTION_DROITS.getId();
	}

	@Override
	protected Class<?> getFormClass() {
		return DroitsForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.GESTION_DROITS_SUPPRIMER;
	}

	public String getLibelleStructureEnseignement() {
		return getControleur().getLibelleAffichage(getCurrentItem().toStructureEnseignement());
	}
	
	public String getLibelleStructureEnseignant() {
		return getControleur().getLibelleAffichage(getCurrentItem().toStructureEnseignant());
	}

	public BasicPecheCtrl getControleur() {
		return controleur;
	}
	
	
	
}