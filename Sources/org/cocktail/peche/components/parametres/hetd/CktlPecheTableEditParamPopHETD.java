package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.peche.components.controlers.ParametresHETDCtrl;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EORepartMethodeCorps;
import org.cocktail.peche.entity.EORepartMethodeTypeContrat;
import org.cocktail.peche.entity.EORepartMethodeTypePopulation;
import org.cocktail.peche.outils.CorpsHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

/**
 * Sous-formulaire d'édition des heures complémentaires pour un corps.
 * 
 * @author yannick
 * 
 */
public class CktlPecheTableEditParamPopHETD extends CktlPecheAbstractTableParamPopHETD {

	private static final String POPULATION = "Population";
	private static final String CORPS = "Corps";
	private static final String CONTRAT = "Contrat";
	private static final long serialVersionUID = 7501839641575911075L;
	private ParamPopHETDDataBean dataBean;
	private EOMethodesCalculHComp selectionMethode;
	private Coefficients parametre;
	private CorpsHelper corpsHelper;
	
	private String typePersonne;
	private String selectionTypePersonne;
	private NSArray<String> listeTypePersonne;
	
//	private String typePopulationSelectionne

	/**
	 * Constructeur.
	 * @param context le context d'exécution de WebObject.
	 */
	public CktlPecheTableEditParamPopHETD(WOContext context) {
		super(context);
		this.corpsHelper = new CorpsHelper(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString());
		listeTypePersonne = new NSMutableArray<String>();
		listeTypePersonne.add(CONTRAT);
		listeTypePersonne.add(CORPS);
		listeTypePersonne.add(POPULATION);
		typePersonne = "";
	}
	
	@Override
	public void awake() {
		super.awake();
		
		this.corpsHelper.setSelectionCorps(null);
		this.corpsHelper.setSelectionContratTravail(null);
		this.corpsHelper.setSelectionPopulation(null);
        this.setController(new ParametresHETDCtrl(edc(), getPecheSession().getApplicationUser().getPersonne(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
        NSArray<ParamPopHETDDataBean> array = new NSMutableArray<ParamPopHETDDataBean>();
        this.dataBean = (ParamPopHETDDataBean) valueForBinding("dataBean");
        this.setSelectionMethode(this.dataBean.getMethode());
		array.add(this.dataBean);
		this.getDisplayGroup().setObjectArray(array);
	}

	/**
	 * Enregistrement des paramètres.
	 * @return le formulaire complet.
	 */
	public WOActionResults enregisterParametres() {
		
		NSTimestamp now = new NSTimestamp();

		if (getSelectionMethode() == null) {
			getPecheSession().addSimpleErrorMessage("Pas de méthode selectionnée");
			return doNothing();
		}
		
		if (isModeCreation()) {
			if (corpsHelper.getSelectionPopulation() == null) {
				if (corpsHelper.getSelectionCorps() == null && corpsHelper.getSelectionContratTravail() == null && corpsHelper.getSelectionPopulation() == null) {
					getPecheSession().addSimpleErrorMessage("Pas de type sélectionné");
					return doNothing();
				}
			}
		}

		if (isModeCreation()) {
			if (corpsHelper.getSelectionCorps() == null && corpsHelper.getSelectionContratTravail() == null && corpsHelper.getSelectionPopulation() == null) {
				return doNothing();
			}
			if (isCorpSelectionne()) {
				dataBean.setCorps(corpsHelper.getSelectionCorps());
			}
			if (isContratSelectionne()) {
				dataBean.setContrat(corpsHelper.getSelectionContratTravail());
			}
			if (isPopulationSelectionne()) {
				dataBean.setPopulation(corpsHelper.getSelectionPopulation());
			}
		}
		
		NSArray<Coefficients> parametres = dataBean.getParametres();
		for (Coefficients p : parametres) {
			EOParamPopHetd paramPopHetd = null;
				
			//test coeficients
			if (p.getEnService().getDenominateur() == 0 || p.getEnService().getNumerateur() == 0 || p.getHorsService().getDenominateur() == 0 || p.getHorsService().getNumerateur() == 0){
				getPecheSession().addSimpleErrorMessage("Le numérateur ou le dénominateur ne peut pas être égal à 0 pour l'atome pédagogique " + p.getTypeAP().code());
				return doNothing();
			}
			
			
				if (dataBean.getCorps() != null) {
					paramPopHetd = EOParamPopHetd.fetchEOParamPopHetd(edc(), EOParamPopHetd.CORPS.eq(dataBean.getCorps()).and(EOParamPopHetd.TYPE_AP.eq(p.getTypeAP())));
					if (paramPopHetd == null) {
						paramPopHetd = EOParamPopHetd.creerEtInitialiser(edc());
					}
					paramPopHetd.setTypeContratTravail(null);
					paramPopHetd.setCorps(dataBean.getCorps());
				}
				
				if (dataBean.getContrat() != null) {
					paramPopHetd = EOParamPopHetd.fetchEOParamPopHetd(edc(), EOParamPopHetd.T_CONTRAT.eq(dataBean.getContrat()).and(EOParamPopHetd.TYPE_AP.eq(p.getTypeAP())));
					if (paramPopHetd == null) {
						paramPopHetd = EOParamPopHetd.creerEtInitialiser(edc());
					}
					paramPopHetd.setTContrat(dataBean.getContrat());
					paramPopHetd.setCorps(null);
				}
				
				if (dataBean.getPopulation() != null) {
					paramPopHetd = EOParamPopHetd.fetchEOParamPopHetd(edc(), EOParamPopHetd.TYPE_POPULATION.eq(dataBean.getPopulation()).and(EOParamPopHetd.TYPE_AP.eq(p.getTypeAP())));
					if (paramPopHetd == null) {
						paramPopHetd = EOParamPopHetd.creerEtInitialiser(edc());
					}
					paramPopHetd.setTypePopulation(dataBean.getPopulation());
					paramPopHetd.setCorps(null);
				}
				
				if (dataBean.getCorps() == null && dataBean.getContrat() == null && dataBean.getPopulation() == null) {
					paramPopHetd = EOParamPopHetd.fetchEOParamPopHetd(edc(), EOParamPopHetd.TYPE_POPULATION.eq(dataBean.getPopulation()).and(EOParamPopHetd.TYPE_AP.eq(p.getTypeAP()))
							.and(EOParamPopHetd.T_CONTRAT.eq(dataBean.getContrat()))
							.and(EOParamPopHetd.CORPS.eq(dataBean.getCorps())));
					if (paramPopHetd == null) {
						paramPopHetd = EOParamPopHetd.creerEtInitialiser(edc());
					}
					paramPopHetd.setTypePopulation(dataBean.getPopulation());
					paramPopHetd.setCorps(null);
				}
				
				
				
				if (paramPopHetd == null) {
					paramPopHetd = EOParamPopHetd.creerEtInitialiser(edc());
					paramPopHetd.setCorps(dataBean.getCorps());
					paramPopHetd.setTypeContratTravail(dataBean.getContrat());
					paramPopHetd.setTypePopulation(dataBean.getPopulation());
					paramPopHetd.setTypeAP(p.getTypeAP());
					paramPopHetd.setDateCreation(now);
					paramPopHetd.setPersonneCreation(getPecheSession().getApplicationUser().getPersonne());
					paramPopHetd.setDateDebut(new NSTimestamp(NSTimestamp.valueOf("2013-01-01 00:00:00")));
					paramPopHetd.setDateFin(new NSTimestamp(NSTimestamp.valueOf("2023-01-01 00:00:00")));
				}
			
			paramPopHetd.setTypeAP(p.getTypeAP());
			paramPopHetd.setDateCreation(now);
			paramPopHetd.setPersonneCreation(getPecheSession().getApplicationUser().getPersonne());
			paramPopHetd.setDateDebut(new NSTimestamp(NSTimestamp.valueOf("2013-01-01 00:00:00")));
			paramPopHetd.setDateFin(new NSTimestamp(NSTimestamp.valueOf("2023-01-01 00:00:00")));
			paramPopHetd.setNumerateurService(p.getEnService().getNumerateur());
			paramPopHetd.setDenominateurService(p.getEnService().getDenominateur());
			paramPopHetd.setNumerateurHorsService(p.getHorsService().getNumerateur());
			paramPopHetd.setDenominateurHorsService(p.getHorsService().getDenominateur());
			paramPopHetd.setDateModification(now);
			paramPopHetd.setPersonneModification(getPecheSession().getApplicationUser().getPersonne());
		}

		
		//corp selectionné
		if (dataBean.getCorps() != null && dataBean.getContrat() == null && dataBean.getPopulation() == null) {
			EORepartMethodeCorps repart = EORepartMethodeCorps.fetchEORepartMethodeCorps(edc(), EORepartMethodeCorps.CORPS.eq(dataBean.getCorps()));
			if (repart == null) {
				repart = EORepartMethodeCorps.createEORepartMethodeCorps(edc(), now, now, dataBean.getCorps(), getSelectionMethode());
			} else {
				repart.setMethode(getSelectionMethode());
			}
		}
		//Type contrat selectionné
		if (dataBean.getCorps() == null && dataBean.getContrat() != null && dataBean.getPopulation() == null) {
			EORepartMethodeTypeContrat repart = EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrat(edc(), EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL.eq(dataBean.getContrat()));
			if (repart == null) {
				repart = EORepartMethodeTypeContrat.createEORepartMethodeTypeContrat(edc(), now, now, getSelectionMethode(), dataBean.getContrat());
			} else {
				repart.setMethode(getSelectionMethode());
			}
		}
		
		//Type population selectionné
		if (dataBean.getCorps() == null && dataBean.getContrat() == null && dataBean.getPopulation() != null) {
			EORepartMethodeTypePopulation repart = EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulation(edc(), EORepartMethodeTypePopulation.TYPE_POPULATION.eq(dataBean.getPopulation()));
			if (repart == null) {
				repart = EORepartMethodeTypePopulation.createEORepartMethodeTypePopulation(edc(), now, now, getSelectionMethode(), dataBean.getPopulation());
			} else {
				repart.setMethode(getSelectionMethode());
			}
		}
		
		//Rien de selectionné
		if (dataBean.getCorps() == null && dataBean.getContrat() == null) {
			getController().setMethodeParDefaut(getSelectionMethode());
		}

		edc().saveChanges();
		
		return pageWithName(CktlPecheParamPopHETD.class.getName());
	}
	
	/**
	 * @return la page de liste des paramètres.
	 */
	public WOActionResults annulerEnregistrement() {
		return pageWithName(CktlPecheParamPopHETD.class.getName());
	}

	public ParamPopHETDDataBean getDataBean() {
		return dataBean;
	}

	/**
	 * @return the selectionMethode
	 */
	public EOMethodesCalculHComp getSelectionMethode() {
		return selectionMethode;
	}

	/**
	 * @param selectionMethode the selectionMethode to set
	 */
	public void setSelectionMethode(EOMethodesCalculHComp selectionMethode) {
		this.selectionMethode = selectionMethode;
	}

	public Coefficients getParametre() {
		return this.parametre;
	}

	public void setParametre(Coefficients parametre) {
		this.parametre = parametre;
	}
	
	public java.text.Format getFormatter() {
		return new NSNumberFormatter("0");
	}
	
	public boolean isModeCreation() {
		return valueForBooleanBinding("modeCreation", false);
	}

	/**
	 * @return the corpsHelper
	 */
	public CorpsHelper getCorpsHelper() {
		return corpsHelper;
	}

	/**
	 * @param corpsHelper the corpsHelper to set
	 */
	public void setCorpsHelper(CorpsHelper corpsHelper) {
		this.corpsHelper = corpsHelper;
	}

	public NSArray<String> getListeTypePersonne() {
		return listeTypePersonne;
	}

	public void setListeTypePersonne(NSArray<String> listeTypePersonne) {
		this.listeTypePersonne = listeTypePersonne;
	}

	public String getTypePersonne() {
//		AjaxUpdateContainer.updateContainerWithID("tableauDD", context());
		return typePersonne;
	}

	public void setTypePersonne(String typePersonne) {
		this.typePersonne = typePersonne;
	}
	
	public boolean isCorpSelectionne() {
		if (selectionTypePersonne == null) {
			return false;
		}
		if (selectionTypePersonne.equals(CORPS)) {
			return true;
		}
		return false;
	}
	
	public boolean isPopulationSelectionne() {
		if (selectionTypePersonne == null) {
			return false;
		}
		if (selectionTypePersonne.equals(POPULATION)) {
			return true;
		}
		return false;
	}
	
	public boolean isContratSelectionne() {
		if (selectionTypePersonne == null) {
			return false;
		}
		if (selectionTypePersonne.equals(CONTRAT)) {
			return true;
		}
		return false;
	}

	public boolean pasDeSelection() {
		return (!isCorpSelectionne() && !isPopulationSelectionne() && !isContratSelectionne());
	}
	
	
	public String getSelectionTypePersonne() {
		return selectionTypePersonne;
	}

	public void setSelectionTypePersonne(String selectionTypePersonne) {
		this.selectionTypePersonne = selectionTypePersonne;
	}
	
}