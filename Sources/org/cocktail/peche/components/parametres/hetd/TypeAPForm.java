package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.Messages;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

public class TypeAPForm extends PecheCreationForm<EOTypeAP> {
	private static final long serialVersionUID = -8650821461058129029L;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public TypeAPForm(WOContext context) {
        super(context);
    }

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.TYPE_AP_FORM_TITRE_AJOUTER;
	}
	
	@Override
	protected String getFormTitreModificationKey() {
		return Messages.TYPE_AP_FORM_TITRE_MODIFIER;
	}

	@Override
	protected EOTypeAP creerNouvelObjet() {
		EOTypeAP typeAP = new EOTypeAP();
		return typeAP;
	}

	@Override
	protected WOActionResults pageGestion() {
		TypesAPGestion page = (TypesAPGestion) pageWithName(TypesAPGestion.class.getName());
		return page;
	}

	public String nouveauTypeHoraireContainerId() {
		return getComponentId() + "_nouveauTypeHoraireContainerId";
	}
	//TODO : voir avec la scol si ajout de creation+modif

	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOTypeAP o = this.editedObject();
		
		PecheApplicationUser appUser = getPecheSession().getApplicationUser();
		EOPersonne personne = appUser.getPersonne();
		if (modeEdition == ModeEdition.CREATION) {
			o.setReadOnly(Integer.valueOf(0));
		}
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_TYPE_AP.getId();
	}
	
}
