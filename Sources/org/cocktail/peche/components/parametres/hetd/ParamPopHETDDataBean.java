package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.metier.hce.Fraction;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Equipe PECHE.
 */
public class ParamPopHETDDataBean {

	private EOMethodesCalculHComp methode;
	private EOCorps corps;
	private EOTypeContratTravail contrat;
	private EOTypePopulation population;
	private String typePopulation;
	private NSArray<Coefficients> parametres;
	
	/**
	 * Constructeur.
	 */
	public ParamPopHETDDataBean() {
		this.parametres = new NSMutableArray<Coefficients>();
	}
	
	/**
	 * @return true si le corps est null, false sinon.
	 */
	public boolean corpsNull() {
		return this.corps == null;
	}
	
	/**
	 * @return the methodeSelectionee
	 */
	public EOMethodesCalculHComp getMethode() {
		return methode;
	}

	public void setMethode(EOMethodesCalculHComp methode) {
		this.methode = methode;
	}
	
	/**
	 * @return le libellé de la méthode de calcul.
	 */
	public String getLibelleMethode() {
		return this.methode.libelle();
	}
	
	public EOCorps getCorps() {
		return this.corps;
	}

	public void setCorps(EOCorps corps) {
		this.corps = corps;
	}
	
	/**
	 * @return le libellé du corps.
	 */
	public String getLibelleCorps() {
		if (this.corps == null) {
			return "*";
		} else {
			return this.corps.llCorps();
		}
	}
	
	
	public String getLibelle() {
		if (this.corps != null) {		
			return this.corps.lcCorps() + " (" + this.corps.cCorps() + ")";
		}
		if (this.contrat != null) {
			return this.contrat.lcTypeContratTrav() + " (" + this.contrat.cTypeContratTrav() + ")";
		}
		if (this.population != null) {
			return this.population.lcTypePopulation() + " (" + this.population.lcTypePopulation() + ")";
		}
		if (this.contrat == null && this.corps == null) {
			return "*";
		}
		return "";
	}
	
	/**
	 * @param typeAP type d'AP.
	 */
	public void setParam(EOTypeAP typeAP) {
		Coefficients coefficients = new Coefficients(typeAP);
		this.parametres.add(coefficients);
	}

	/**
	 * @param typeAP type d'AP.
	 * @param enService coefficient dans le service.
	 * @param horsService coefficient hors du service.
	 */
	public void setParam(EOTypeAP typeAP, Fraction enService, Fraction horsService) {
		Coefficients coefficients = new Coefficients(typeAP, enService, horsService);
		this.parametres.add(coefficients);
	}

	/**
	 * @return the parametres
	 */
	public NSArray<Coefficients> getParametres() {
		return parametres;
	}

	/**
	 * @param parametres the parametres to set
	 */
	public void setParametres(NSArray<Coefficients> parametres) {
		this.parametres = parametres;
	}

	public EOTypePopulation getPopulation() {
		return population;
	}

	public void setPopulation(EOTypePopulation population) {
		this.population = population;
	}

	public EOTypeContratTravail getContrat() {
		return contrat;
	}

	public void setContrat(EOTypeContratTravail contrat) {
		this.contrat = contrat;
	}

	public String getTypePopulation() {
		return typePopulation;
	}

	public void setTypePopulation(String typePopulation) {
		this.typePopulation = typePopulation;
	}
}
