package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;

import com.webobjects.appserver.WOContext;

/**
 * Formulaire d'édition des paramètres de calcul des heures complémentaires,
 * pour un corps.
 * 
 * @author yannick
 * 
 */
public class CktlPecheEditParamPopHETD extends CktlPecheBaseComponent {

	private static final long serialVersionUID = -4678253190287306052L;
	
	private ParamPopHETDDataBean dataBean;
	private boolean modeCreation;

	/**
	 * Constructeur.
	 * @param context le context d'exécution de WebObjects.
	 */
	public CktlPecheEditParamPopHETD(WOContext context) {
		super(context);
		modeCreation = false;
	}

	public ParamPopHETDDataBean getDataBean() {
		return this.dataBean;
	}
	
	public void setDataBean(ParamPopHETDDataBean dataBean) {
		this.dataBean = dataBean;
	}

	/**
	 * @return the modeEdition
	 */
	public boolean isModeCreation() {
		return modeCreation;
	}

	/**
	 * @param modeEdition the modeEdition to set
	 */
	public void setModeCreation(boolean modeEdition) {
		this.modeCreation = modeEdition;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_HETD.getId();
	}
}