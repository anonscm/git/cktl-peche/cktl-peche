package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.outils.CorpsHelper;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class ParamPopHetdForm extends PecheCreationForm<EOParamPopHetd> {
    /**
	 * 
	 */
	private static final long serialVersionUID = -7527101671176107627L;
	
	private NSArray<EOTypeAP> listeTypeHoraire;

	private EOTypeAP selectionTypeHoraire;

	private CorpsHelper corpsHelper = null;
	 
	
	public ParamPopHetdForm(WOContext context) {
        super(context);
    	this.corpsHelper = new CorpsHelper(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString());
    }
	
	@Override
	protected String getFormTitreCreationKey() {
		return Messages.PARAM_POP_HETD_FORM_TITRE_AJOUTER;
	}
	
	@Override
	protected String getFormTitreModificationKey() {
		return Messages.PARAM_POP_HETD_FORM_TITRE_MODIFIER;
	}

	@Override
	protected EOParamPopHetd creerNouvelObjet() {
		return EOParamPopHetd.creerEtInitialiser(edc());		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected WOActionResults pageGestion() {
		PecheGestionComponent<EOParamPopHetd, ParamPopHetdForm> page = (PecheGestionComponent<EOParamPopHetd, ParamPopHetdForm>) pageWithName(ParamsPopHetdGestion.class.getName());
		return page;
	}

	public String nouveauParamPopHetdContainerId() {
		return getComponentId() + "_nouveauParamPopHetdContainerId";
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		OutilsValidation.verifierOrdreDates(this.editedObject().dateDebut(), this.editedObject().dateFin(), getPecheSession());
		if (modeEdition == ModeEdition.CREATION) {
			this.editedObject().setPersonneCreation(getPecheSession().getApplicationUser().getPersonne());
			this.editedObject().setDateCreation(now);
			this.editedObject().setDateDebut(now);
		}
		this.editedObject().setPersonneModification(getPecheSession().getApplicationUser().getPersonne());
		this.editedObject().setDateModification(now);
	}
	
	/**
	 * @return the listeTypeHoraire
	 */
	public NSArray<EOTypeAP> listeTypeHoraire() {
		if (this.listeTypeHoraire == null) {
			this.listeTypeHoraire = EOTypeAP.fetchAllSco_TypeAPs(edc());
		}
		return this.listeTypeHoraire;
	}

	/**
	 * @param listeTypeHoraire the listeTypeHoraire to set
	 */
	public void setListeTypeHoraire(NSArray<EOTypeAP> listeTypeHoraire) {
		this.listeTypeHoraire = listeTypeHoraire;
	}

	/**
	 * @return the selectionTypeHoraire
	 */
	public EOTypeAP selectionTypeHoraire() {
		return selectionTypeHoraire;
	}

	/**
	 * @param selectionTypeHoraire the selectionTypeHoraire to set
	 */
	public void setSelectionTypeHoraire(EOTypeAP selectionTypeHoraire) {
		this.selectionTypeHoraire = selectionTypeHoraire;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_HETD.getId();
	}

	public CorpsHelper getCorpsHelper() {
		return corpsHelper;
	}
}
