package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;

import com.webobjects.appserver.WOContext;

/**
 * Classe permettant la gestion des types d'AP de l'offre de formation
 *
 * @author Chama LAATIK
 *
 */
public class TypesAPGestion extends PecheGestionComponent<EOTypeAP, TypeAPForm> {
	private static final long serialVersionUID = -5250465989580736963L;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public TypesAPGestion(WOContext context) {
        super(context);
    }

	@Override
	protected String getEntityName() {
		return EOTypeAP.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return TypeAPForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.TYPES_AP_GESTION_ALERTE_CONFIRM_SUPPRESS;
	}

	/**
	 * Affichage des boutons grisés ou non en fonction des droits
	 * @return vrai/faux
	 */
	public boolean isBoutonsGrises() {
		return getSelectedItem() == null || (getSelectedItem().readOnly() != 0);
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_TYPE_AP.getId();
	}
}