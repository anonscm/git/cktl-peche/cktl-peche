package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.ParametresHETDCtrl;
import org.cocktail.peche.entity.EOMethodesCalculHComp;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

/**
 * Composant de paramétrage des populations pour le calcul des heures
 * complémentaires d'enseignements.
 * 
 * @author Equipe PECHE.
 * 
 */
public class CktlPecheParamPopHETD extends CktlPecheBaseComponent {

	private static final long serialVersionUID = 3532972838355522391L;
	private EOMethodesCalculHComp methode;
	private ParametresHETDCtrl controller;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le contexte d'exécution WebObject.
	 */
	public CktlPecheParamPopHETD(WOContext context) {
		super(context);
		this.controller = new ParametresHETDCtrl(edc(), getPecheSession().getApplicationUser().getPersonne(),getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
	}
	
	public NSArray<EOMethodesCalculHComp> getMethodes() {
		return controller.getMethodes();
	}

	public EOMethodesCalculHComp getMethode() {
		return this.methode;
	}

	public void setMethode(EOMethodesCalculHComp methode) {
		this.methode = methode;
	}
	
	public EOMethodesCalculHComp getMethodeSelectionee() {
		return this.controller.getMethodeParDefaut();
	}

	/**
	 * @param methodeSelectionnee the selectionMethode to set
	 */
	public void setMethodeSelectionee(EOMethodesCalculHComp methodeSelectionnee) {
		this.controller.setMethodeParDefaut(methodeSelectionnee);
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_HETD.getId();
	}
}
