package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.controlers.ParametresHETDCtrl;
import org.cocktail.peche.entity.EOMethodesCalculHComp;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Classe de base pour les table d'édition ou d'affichage des paramètres de
 * calcul de heures complémentaires.
 * 
 * @author yannick
 * 
 */
public class CktlPecheAbstractTableParamPopHETD extends
		CktlPecheTableComponent<ParamPopHETDDataBean> {

	private static final long serialVersionUID = -1248093276472939995L;
	private NSArray<EOTypeAP> typesAP;
	private EOTypeAP typeAP;
	private EOMethodesCalculHComp methode;
	private ParametresHETDCtrl controller;

	/**
	 * Constructeur.
	 * @param context le context d'excécution de WebObjects.
	 */
	public CktlPecheAbstractTableParamPopHETD(WOContext context) {
		super(context);
		NSArray<EOSortOrdering> alphaSort = EOTypeAP.CODE.ascs();
        this.setTypesAP(EOTypeAP.fetchAllSco_TypeAPs(edc(), alphaSort));
	}

	public NSArray<EOTypeAP> getTypesAP() {
		return typesAP;
	}

	public void setTypesAP(NSArray<EOTypeAP> typesAP) {
		this.typesAP = typesAP;
	}

	public EOTypeAP getTypeAP() {
		return this.typeAP;
	}

	public void setTypeAP(EOTypeAP typeAP) {
		this.typeAP = typeAP;
	}

	public NSArray<EOMethodesCalculHComp> getMethodes() {
		return controller.getMethodes();
	}
	
	public EOMethodesCalculHComp getMethode() {
		return methode;
	}

	public void setMethode(EOMethodesCalculHComp methode) {
		this.methode = methode;
	}

	public ParametresHETDCtrl getController() {
		return controller;
	}

	public void setController(ParametresHETDCtrl controller) {
		this.controller = controller;
	}

}
