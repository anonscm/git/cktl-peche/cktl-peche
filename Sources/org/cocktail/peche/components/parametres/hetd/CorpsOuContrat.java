package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;

public class CorpsOuContrat {
	
	private EOCorps corps;
	
	private EOTypeContratTravail contrat;
	
	public CorpsOuContrat(EOCorps corps) {
		this.corps = corps;
		this.contrat = null;
	}
	
	public CorpsOuContrat(EOTypeContratTravail contrat) {
		this.contrat = contrat;
		this.corps = null;
	}
	
	public String libelle() {
		if (this.corps != null) {
			return this.corps.lcCorps();
		} else {
			return this.contrat.lcTypeContratTrav();
		}
	}
	
	public EOCorps corps() {
		return this.corps;
	}
	
	public EOTypeContratTravail typeContratTravail() {
		return this.contrat;
	}

	public boolean isCorps() {
		return (this.corps != null);
	}
}
