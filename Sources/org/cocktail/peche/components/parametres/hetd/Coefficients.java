package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.peche.metier.hce.Fraction;

public class Coefficients {

	private EOTypeAP typeAP;
	private Fraction enService;
	private Fraction horsService;

	/**
	 * Les coefficients En/Hors service auront les valeurs par défaut pour l'AP.
	 * 
	 * @param typeAP Un type d'AP
	 */
	public Coefficients(EOTypeAP typeAP) {
		this(typeAP, getCoeffServiceParDefaut(typeAP), getCoeffHorsServiceParDefaut(typeAP));
	}

	/**
	 * @param typeAP Un type d'AP
	 * @param enService Un coefficient HETD "dans le service"
	 * @param horsService Un coefficient HETD "hors service"
	 */
	public Coefficients(EOTypeAP typeAP, Fraction enService, Fraction horsService) {
		this.typeAP = typeAP;
		this.enService = enService;
		this.horsService = horsService;
	}

	/**
	 * @return the enService
	 */
	public Fraction getEnService() {
		return enService;
	}

	/**
	 * @param enService the enService to set
	 */
	public void setEnService(Fraction enService) {
		this.enService = enService;
	}

	/**
	 * @return the horsService
	 */
	public Fraction getHorsService() {
		return horsService;
	}

	/**
	 * @param horsService the horsService to set
	 */
	public void setHorsService(Fraction horsService) {
		this.horsService = horsService;
	}

	/**
	 * @return the typeAP
	 */
	public EOTypeAP getTypeAP() {
		return typeAP;
	}

	/**
	 * @param typeAp Un type d'AP
	 * @return Le coefficient HETD "dans le service" par défaut pour le type d'AP
	 */
	public static Fraction getCoeffServiceParDefaut(EOTypeAP typeAp) {
		if (EOTypeAP.TYPECOURS_CODE.equals(typeAp.code())) {
			return new Fraction(3, 2);
		}
		
		return new Fraction(1, 1);
	}
	
	/**
	 * @param typeAp Un type d'AP
	 * @return Le coefficient HETD "hors service" par défaut pour le type d'AP
	 */
	public static Fraction getCoeffHorsServiceParDefaut(EOTypeAP typeAp) {
		if (EOTypeAP.TYPECOURS_CODE.equals(typeAp.code())) {
			return new Fraction(3, 2);
		} else if (EOTypeAP.TYPETP_CODE.equals(typeAp.code())) {
			return new Fraction(2, 3);
		}
		
		return new Fraction(1, 1);
		
	}
}
