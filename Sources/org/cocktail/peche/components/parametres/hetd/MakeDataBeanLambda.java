package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.peche.entity.EOParamPopHetd;

import com.webobjects.foundation.NSArray;

/**
 * Interface de modélisation d'une fonction Lambda appelée pour fabriquer les
 * DataBean
 */
interface MakeDataBeanLambda {
	NSArray<EOParamPopHetd> apply();
}
