package org.cocktail.peche.components.parametres.hetd;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.outils.CorpsHelper;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

/**
 * Classe permettant la gestion du paramétrage du décompte des heures en HETD par type de population
 *
 * @author Yannick MAURAY
 */
public class ParamsPopHetdGestion extends PecheGestionComponent<EOParamPopHetd, ParamPopHetdForm> {

	private static final long serialVersionUID = -4261562675700041941L;

    private CorpsHelper corpsHelper = null;

	private NSArray<CorpsOuContrat> listeCorpsOuContrats;

	private EOCorps cibleCorps;

	private EOTypeContratTravail cibleTypeContratTravail;

	private CorpsOuContrat selectionCorpsOuContrat;

	private CorpsOuContrat sourceCorpsOuContrat;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
    public ParamsPopHetdGestion(WOContext context) {
		super(context);
		this.corpsHelper = new CorpsHelper(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours().toString());
		  
		this.listeCorpsOuContrats = new NSMutableArray<CorpsOuContrat>();

		NSArray<Object> d = ERXEOControlUtilities.distinctValuesForKeyPath(edc(), EOParamPopHetd.ENTITY_NAME, EOParamPopHetd.CORPS.dot(EOCorps.C_CORPS).key(), null, null);
		for (Object o : d) {
			EOCorps corps = EOCorps.fetchByKeyValue(edc(), EOCorps.C_CORPS_KEY, o);
			this.listeCorpsOuContrats.add(new CorpsOuContrat(corps));
		}

		d = ERXEOControlUtilities.distinctValuesForKeyPath(edc(), EOParamPopHetd.ENTITY_NAME, EOParamPopHetd.TYPE_CONTRAT_TRAVAIL.dot(EOTypeContratTravail.C_TYPE_CONTRAT_TRAV).key(), null, null);
		for (Object o : d) {
			EOTypeContratTravail typeContratTravail = EOTypeContratTravail.fetchByKeyValue(edc(), EOTypeContratTravail.C_TYPE_CONTRAT_TRAV_KEY, o);
			this.listeCorpsOuContrats.add(new CorpsOuContrat(typeContratTravail));
		}

	}

	@Override
	protected String getEntityName() {
		return EOParamPopHetd.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return ParamPopHetdForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.PARAMS_POP_HETD_GESTION_ALERTE_CONFIRM_SUPPRESS;
	}

	/**
	 * @return the cibleCorps
	 */
	public EOCorps cibleCorps() {
		return cibleCorps;
	}

	/**
	 * @param cibleCorps the cibleCorps to set
	 */
	public void setCibleCorps(EOCorps cibleCorps) {
		this.cibleCorps = cibleCorps;
		this.cibleTypeContratTravail = null;
	}

	/**
	 * @return the cibleTypeContratTravail
	 */
	public EOTypeContratTravail cibleTypeContratTravail() {
		return cibleTypeContratTravail;
	}

	/**
	 * @param cibleTypeContratTravail the cibleTypeContratTravail to set
	 */
	public void setCibleTypeContratTravail(EOTypeContratTravail cibleTypeContratTravail) {
		this.cibleTypeContratTravail = cibleTypeContratTravail;
		this.cibleCorps = null;
	}

	public NSArray<CorpsOuContrat> getListeCorpsOuContrats() {
		return listeCorpsOuContrats;
	}

	public void setListeCorpsOuContrats(NSArray<CorpsOuContrat> listeCorpsOuContrats) {
		this.listeCorpsOuContrats = listeCorpsOuContrats;
	}

	/**
	 * @return the selectionCorpsOuContrat
	 */
	public CorpsOuContrat selectionCorpsOuContrat() {
		return selectionCorpsOuContrat;
	}

	/**
	 * @param selectionCorpsOuContrat the selectionCorpsOuContrat to set
	 */
	public void setSelectionCorpsOuContrat(CorpsOuContrat selectionCorpsOuContrat) {
		this.selectionCorpsOuContrat = selectionCorpsOuContrat;
	}

	/**
	 * @return the sourceCorpsOuContrat
	 */
	public CorpsOuContrat sourceCorpsOuContrat() {
		return sourceCorpsOuContrat;
	}

	/**
	 * @param sourceCorpsOuContrat the sourceCorpsOuContrat to set
	 */
	public void setSourceCorpsOuContrat(CorpsOuContrat sourceCorpsOuContrat) {
		this.sourceCorpsOuContrat = sourceCorpsOuContrat;
	}

	/**
	 * Duplique le corps ou le contrat demandé
	 * @return null
	 */
	public WOActionResults dupliquerCorpsOuContrat() {
		NSArray<EOParamPopHetd> sourceParams;
		if (this.sourceCorpsOuContrat.isCorps()) {
			sourceParams = EOParamPopHetd.fetchEOParamPopHetds(edc(), ERXQ.equals(EOParamPopHetd.CORPS_KEY, this.sourceCorpsOuContrat().corps()), null);
		} else {
			sourceParams = EOParamPopHetd.fetchEOParamPopHetds(edc(), ERXQ.equals(EOParamPopHetd.TYPE_CONTRAT_TRAVAIL_KEY, this.sourceCorpsOuContrat().typeContratTravail()), null);
		}
		for (EOParamPopHetd sourceParam : sourceParams) {
			EOParamPopHetd copie = EOParamPopHetd.dupliquer(sourceParam, edc());
			copie.setCorps(null);
			copie.setTypeContratTravail(null);
			if (cibleCorps() != null) {
				EOCorps c = cibleCorps.localInstanceIn(edc());
				copie.setCorps(c);
			} else {
				EOTypeContratTravail c = cibleTypeContratTravail.localInstanceIn(edc());
				copie.setTypeContratTravail(c);
			}
			NSTimestamp maintenant = new NSTimestamp();
			copie.setDateCreation(maintenant);
			copie.setDateModification(maintenant);
			copie.setPersonneCreation(getPecheSession().getApplicationUser().getPersonne());
			copie.setPersonneModification(getPecheSession().getApplicationUser().getPersonne());
			edc().saveChanges();
		}
		return null;
	}

	/**
	 *
	 * @return l'identifiant du conteneur de duplication.
	 */
	public String duplicationContainerId() {
		return getComponentId() + "_duplicationContainerId";
	}

	/**
	 * Affichage des boutons grisés ou non en fonction des droits
	 * @return vrai/faux
	 */
	public boolean getAffichageBoutons() {
		return !getAutorisation().hasDroitUtilisationParametrageParametresPopulations();
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_HETD.getId();
	}

	public CorpsHelper getCorpsHelper() {
		return corpsHelper;
	}
}
