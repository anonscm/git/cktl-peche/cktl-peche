package org.cocktail.peche.components.parametres.hetd;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.peche.components.controlers.ParametresHETDCtrl;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EORepartMethodeCorps;
import org.cocktail.peche.entity.EORepartMethodeTypeContrat;
import org.cocktail.peche.entity.EORepartMethodeTypePopulation;
import org.cocktail.peche.metier.hce.Fraction;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Composant d'affichage des paramètres de calcul des heures complémentaires.
 * 
 * @author Equipe PECHE.
 * 
 */
public class CktlPecheTableParamPopHETD extends
		CktlPecheAbstractTableParamPopHETD {

	private static final long serialVersionUID = 7311738529084214892L;
	private String itemCorps;
	private String itemTypeContrat;
	private Coefficients parametre;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le context d'exécution de WebObject.
	 */
	public CktlPecheTableParamPopHETD(WOContext context) {
		super(context);
		this.setController(new ParametresHETDCtrl(edc(), getPecheSession().getApplicationUser().getPersonne(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
        		getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile()));

		this.getDisplayGroup().setObjectArray(fetchDisplayGroupData());
	}

	private NSArray<ParamPopHETDDataBean> fetchDisplayGroupData() {
		NSArray<ParamPopHETDDataBean> array = new NSMutableArray<ParamPopHETDDataBean>();

		/*
		 * Méthode par défaut. Toujours en premier, existe toujours.
		 */
		EOCorps corp = null;
		ParamPopHETDDataBean element = makeDataBean(corp, this.getController()
				.getMethodeParDefaut(), new MakeDataBeanLambda() {
			public NSArray<EOParamPopHetd> apply() {
				return EOParamPopHetd.fetchEOParamPopHetds(
						edc(),
						EOParamPopHetd.CORPS.isNull().and(
								EOParamPopHetd.T_CONTRAT.isNull())
								.and(EOParamPopHetd.TYPE_POPULATION.isNull()),
						EOParamPopHetd.TYPE_AP.dot(EOTypeAP.CODE).ascs());
			}
		});
		element.setTypePopulation("");
		array.add(element);

		/*
		 * Méthode pour les autres corps
		 */
		NSArray<EORepartMethodeCorps> reparts = EORepartMethodeCorps
				.fetchAllEORepartMethodeCorpses(edc(),
						EORepartMethodeCorps.CORPS.dot(EOCorps.LC_CORPS).ascs());
		for (final EORepartMethodeCorps repart : reparts) {
			EOCorps c = repart.corps();
			EOMethodesCalculHComp m = repart.methode();
			final EOQualifier q = EOParamPopHetd.CORPS.eq(c).and(
					EOParamPopHetd.T_CONTRAT.isNull())
					.and(EOParamPopHetd.TYPE_POPULATION.isNull());
			final NSArray<EOSortOrdering> so = EOParamPopHetd.TYPE_AP.dot(
					EOTypeAP.CODE).ascs();
			element = makeDataBean(c, m, new MakeDataBeanLambda() {
				public NSArray<EOParamPopHetd> apply() {
					return EOParamPopHetd.fetchEOParamPopHetds(edc(), q, so);
				}
			});
			element.setTypePopulation("Corps");
			array.add(element);
		}
		
		
		/*
		 * Méthode pour les Type de contrat
		 */
		NSArray<EORepartMethodeTypeContrat> repartsTypeContrat = EORepartMethodeTypeContrat
				.fetchAllEORepartMethodeTypeContrats(edc(),
						EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL.dot(EOTypeContratTravail.LC_TYPE_CONTRAT_TRAV).ascs());
		for (final EORepartMethodeTypeContrat repart : repartsTypeContrat) {
			EOTypeContratTravail c = repart.typeContratTravail();
			EOMethodesCalculHComp m = repart.methode();
			final EOQualifier q1 = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_POPULATION.isNull())
					.and(EOParamPopHetd.T_CONTRAT.eq(c));
			final NSArray<EOSortOrdering> so = EOParamPopHetd.TYPE_AP.dot(
					EOTypeAP.CODE).ascs();
			element = makeDataBean(c, m, new MakeDataBeanLambda() {
				public NSArray<EOParamPopHetd> apply() {
					return EOParamPopHetd.fetchEOParamPopHetds(edc(), q1, so);
				}
			});
			element.setTypePopulation("Contrat");
			array.add(element);
		}
		
		
		/*
		 * Méthode pour les Type de population
		 */
		NSArray<EORepartMethodeTypePopulation> repartsTypePopulation = EORepartMethodeTypePopulation.
				fetchAllEORepartMethodeTypePopulations(edc(),
						EORepartMethodeTypePopulation.TYPE_POPULATION.dot(EOTypePopulation.LC_TYPE_POPULATION).ascs());
	
		
		for (final EORepartMethodeTypePopulation repart : repartsTypePopulation) {
			EOTypePopulation c = repart.typePopulation();
			EOMethodesCalculHComp m = repart.methode();
			final EOQualifier q1 = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_CONTRAT_TRAVAIL.isNull())
					.and(EOParamPopHetd.TYPE_POPULATION.eq(c));;
			final NSArray<EOSortOrdering> so = EOParamPopHetd.TYPE_AP.dot(
					EOTypeAP.CODE).ascs();
			element = makeDataBean(c, m, new MakeDataBeanLambda() {
				public NSArray<EOParamPopHetd> apply() {
					return EOParamPopHetd.fetchEOParamPopHetds(edc(), q1, so);
				}
			});
			element.setTypePopulation("Population");
			array.add(element);
		}
		
		return array;
	}

	private ParamPopHETDDataBean makeDataBean(EOCorps corps,
			EOMethodesCalculHComp methode, MakeDataBeanLambda lambda) {
		ParamPopHETDDataBean element = new ParamPopHETDDataBean();
		element.setCorps(corps);
		element.setMethode(methode);
		if (corps != null) {
			element.setTypePopulation("Corps");
		} else {
			element.setTypePopulation("*");
		}
		NSArray<EOParamPopHetd> params = lambda.apply();
		Map<String, EOParamPopHetd> map = new HashMap<String, EOParamPopHetd>();
		for (EOParamPopHetd p : params) {
			map.put(p.typeAP().code(), p);
		}
		for (EOTypeAP typeAP : this.getTypesAP()) {
			EOParamPopHetd p = map.get(typeAP.code());
			if (p != null) {
				element.setParam(
						typeAP,
						new Fraction(p.numerateurService(), p
								.denominateurService()),
						new Fraction(p.numerateurHorsService(), p
								.denominateurHorsService()));
			} else {
				element.setParam(typeAP);
			}
		}
		return element;
	}
	
	
	private ParamPopHETDDataBean makeDataBean(EOTypeContratTravail typeContrat,
			EOMethodesCalculHComp methode, MakeDataBeanLambda lambda) {
		ParamPopHETDDataBean element = new ParamPopHETDDataBean();
		element.setContrat(typeContrat);
		element.setMethode(methode);
		element.setTypePopulation("Type de contrat");
		NSArray<EOParamPopHetd> params = lambda.apply();
		Map<String, EOParamPopHetd> map = new HashMap<String, EOParamPopHetd>();
		for (EOParamPopHetd p : params) {
			map.put(p.typeAP().code(), p);
		}
		for (EOTypeAP typeAP : this.getTypesAP()) {
			EOParamPopHetd p = map.get(typeAP.code());
			if (p != null) {
				element.setParam(
						typeAP,
						new Fraction(p.numerateurService(), p
								.denominateurService()),
						new Fraction(p.numerateurHorsService(), p
								.denominateurHorsService()));
			} else {
				element.setParam(typeAP);
			}
		}
		return element;
	}
	
	
	private ParamPopHETDDataBean makeDataBean(EOTypePopulation typePopulation,
			EOMethodesCalculHComp methode, MakeDataBeanLambda lambda) {
		ParamPopHETDDataBean element = new ParamPopHETDDataBean();
		element.setContrat(null);
		element.setPopulation(typePopulation);
		element.setMethode(methode);
		element.setTypePopulation("Population");
		NSArray<EOParamPopHetd> params = lambda.apply();
		Map<String, EOParamPopHetd> map = new HashMap<String, EOParamPopHetd>();
		for (EOParamPopHetd p : params) {
			map.put(p.typeAP().code(), p);
		}
		for (EOTypeAP typeAP : this.getTypesAP()) {
			EOParamPopHetd p = map.get(typeAP.code());
			if (p != null) {
				element.setParam(
						typeAP,
						new Fraction(p.numerateurService(), p
								.denominateurService()),
						new Fraction(p.numerateurHorsService(), p
								.denominateurHorsService()));
			} else {
				element.setParam(typeAP);
			}
		}
		return element;
	}
	

	public String getItemCorps() {
		return itemCorps;
	}

	public void setItemCorps(String itemCorps) {
		this.itemCorps = itemCorps;
	}

	/**
	 * @return le formulaire permettant d'éditer les paramètres d'un corps.
	 */
	public WOActionResults editerCorps() {
		if (getSelectedObject() == null) {
			return doNothing();
		} else {
			CktlPecheEditParamPopHETD form = (CktlPecheEditParamPopHETD) pageWithName(CktlPecheEditParamPopHETD.class
					.getName());
			form.setDataBean(this.getSelectedObject());
			form.setModeCreation(false);
			return form;
		}
	}

	/**
	 * @return le formulaire permettant d'ajouter les paramètres d'un corps.
	 */
	public WOActionResults ajouterCorps() {
		CktlPecheEditParamPopHETD form = (CktlPecheEditParamPopHETD) pageWithName(CktlPecheEditParamPopHETD.class
				.getName());
		ParamPopHETDDataBean element = new ParamPopHETDDataBean();
		for (EOTypeAP typeAP : this.getTypesAP()) {
			element.setParam(typeAP);
		}
		form.setDataBean(element);
		form.setModeCreation(true);
		return form;
	}

	/**
	 * Supprime le paramétrage d'un corps.
	 * @return ce formulaire.
	 */
	public WOActionResults supprimerCorps() {
		if (getSelectedObject() == null) {
			return doNothing();
		}
		if (getSelectedObject().getCorps() == null && getSelectedObject().getPopulation() == null && getSelectedObject().getContrat() == null) {
			return doNothing();
		}
		NSArray<EOParamPopHetd> params;
		
		
		if (getSelectedObject().getCorps() != null && getSelectedObject().getPopulation() == null && getSelectedObject().getContrat() == null) {
			EORepartMethodeCorps repartCorp = EORepartMethodeCorps.fetchEORepartMethodeCorps(edc(), EORepartMethodeCorps.CORPS.eq(getSelectedObject().getCorps()));
			params = EOParamPopHetd.fetchEOParamPopHetds(edc(), EOParamPopHetd.CORPS.eq(getSelectedObject().getCorps()), null);
			for (EOParamPopHetd param : params) {
				edc().deleteObject(param);
			}
			edc().deleteObject(repartCorp);
		}
		if (getSelectedObject().getCorps() == null && getSelectedObject().getPopulation() != null && getSelectedObject().getContrat() == null) {
			EORepartMethodeTypePopulation repartPopulation = EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulation(edc(), EORepartMethodeTypePopulation.TYPE_POPULATION.eq(getSelectedObject().getPopulation()));		
			params = EOParamPopHetd.fetchEOParamPopHetds(edc(), EOParamPopHetd.TYPE_POPULATION.eq(getSelectedObject().getPopulation()), null);
			for (EOParamPopHetd param : params) {
				edc().deleteObject(param);
			}
			edc().deleteObject(repartPopulation);
		}
		if (getSelectedObject().getCorps() == null && getSelectedObject().getPopulation() == null && getSelectedObject().getContrat() != null) {
			EORepartMethodeTypeContrat repartContrat = EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrat(edc(), EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL.eq(getSelectedObject().getContrat()));
			params = EOParamPopHetd.fetchEOParamPopHetds(edc(), EOParamPopHetd.T_CONTRAT.eq(getSelectedObject().getContrat()), null);
			for (EOParamPopHetd param : params) {
				edc().deleteObject(param);
			}
			edc().deleteObject(repartContrat);
		}
		
		edc().saveChanges();
		
		setSelectedObject(null);
		getDisplayGroup().setObjectArray(fetchDisplayGroupData());
		
		return doNothing();
	}

	/**
	 * @return le message d'avertissement à afficher avant de supprimer un
	 *         paramètre.
	 */
	public String avantSupprimerCorps() {
		return "confirm('Etes-vous certain de vouloir supprimer ce paramètre ? Cette action est irréversible')";
	}

	/**
	 * @return the parametre
	 */
	public Coefficients getParametre() {
		return parametre;
	}

	/**
	 * @param parametre
	 *            the parametre to set
	 */
	public void setParametre(Coefficients parametre) {
		this.parametre = parametre;
	}
	

	/**
	 * @return the nePasAutoriserBoutonSuppression
	 */
	public Boolean nePasAutoriserBoutonSuppression() {
		if (this.getSelectedObject() != null) {
			if (this.getSelectedObject().getTypePopulation().equals("")) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	
	/**
	 * @return the nePasAutoriserBoutonModification
	 */
	public Boolean nePasAutoriserBoutonModification() {
		ParamPopHETDDataBean eoReh1 = this.getSelectedObject();
		if (eoReh1 != null)  {
			if (eoReh1.getContrat() != null) {
				NSTimestamp debutAnneeUniv = OutilsValidation.getNSTDebutAnneeUniversitaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				NSTimestamp finAnneeUniv = OutilsValidation.getNSTFinAnneeUniversitaire(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
						getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				NSTimestamp debutContrat = eoReh1.getContrat().dDebVal();
				NSTimestamp finContrat = eoReh1.getContrat().dFinVal();

				if (debutContrat.after(finAnneeUniv)) {
					return true;
				}
				if (finContrat != null && finContrat.before(debutAnneeUniv)) {
					return true;
				}
			}
			return false;
		}
		return true;
	}

	public String getItemTypeContrat() {
		return itemTypeContrat;
	}

	public void setItemTypeContrat(String itemTypeContrat) {
		this.itemTypeContrat = itemTypeContrat;
	}	
	
	
}
