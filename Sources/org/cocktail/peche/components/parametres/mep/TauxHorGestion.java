package org.cocktail.peche.components.parametres.mep;

import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOTauxHoraire;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant la gestion des taux horaires
 *
 * @author Laurent Pringot
 *
 */
public class TauxHorGestion extends PecheGestionComponent<EOTauxHoraire, TauxHorForm> {

	private static final long serialVersionUID = 9213617561115769708L;
	
	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public TauxHorGestion(WOContext context) {
		super(context);		
	}
	
	@Override
	protected String getEntityName() {
		return EOTauxHoraire.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return TauxHorForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.TAUX_HORAIRE_CONFIRM_SUPPRESS;
	}
	
	@Override
	public WOActionResults deleteAction() {
		getSelectedItem().majDonnesAuditSuppression();
		super.deleteAction();
		return null;
	}
		

	@Override
	protected void filtrerDisplayGroup(EOQualifier qualifier) {
		super.filtrerDisplayGroup(qualifier);
	}
	

	/**
	 * @return the nePasAutoriserBoutonSuppression
	 */
	public Boolean nePasAutoriserBoutonSuppression() {
		EOTauxHoraire eoTauxHoraire = getSelectedItem();
		if (eoTauxHoraire != null) {
			NSTimestamp dateFin = eoTauxHoraire.dateFinApplication();
			if (dateFin == null) {
				return false;
			}
		}		
		return true;		
	}

	/**
	 * @return the nePasAutoriserBoutonModification
	 */
	public Boolean nePasAutoriserBoutonModification() {
		EOTauxHoraire eoTauxHoraire = getSelectedItem();
		if (eoTauxHoraire != null) {
			return false;
		}		
		return true;		
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_TAUX_HORAIRE.getId();
	}
	
	
	
}
