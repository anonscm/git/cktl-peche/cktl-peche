package org.cocktail.peche.components.parametres.mep;

import org.cocktail.peche.entity.EOTypeFluxPaiement;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
/**
 * Helper permettant la construction du formulaire selon le type de flux de paiement sélectionné 
 * @author juliencallewaert
 *
 */
public class ChampsFluxPaieHelper {

	
	private EOEditingContext edc;
	
	private NSArray<EOTypeFluxPaiement> listeTypeFluxPaiement;
	private EOTypeFluxPaiement typeFluxPaiement;
	private EOTypeFluxPaiement typeFluxPaiementSelection;

	/**
	 * 
	 * @param edc
	 */
	public ChampsFluxPaieHelper(EOEditingContext edc) {
		setEdc(edc);
		this.listeTypeFluxPaiement = EOTypeFluxPaiement.fetchAllEOTypeFluxPaiements(edc());
		
		if (this.typeFluxPaiementSelection == null) {
			this.typeFluxPaiementSelection = this.listeTypeFluxPaiement.get(0);
		}
	}
	
	private EOEditingContext edc() {
		return this.edc;
	}
	
	private void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
	
	/**
	 * 
	 * @return
	 */
	public EOTypeFluxPaiement typeFluxPaiement() {
		return this.typeFluxPaiement;
	}
	
	/**
	 * 
	 * @return
	 */
	public EOTypeFluxPaiement typeFluxPaiementSelection() {
		
		return this.typeFluxPaiementSelection;
	}

	public void setTypeFluxPaiementSelection(EOTypeFluxPaiement typeFluxPaiementSelection) {
		this.typeFluxPaiementSelection = typeFluxPaiementSelection;
	}

	/**
	 * 
	 * @return
	 */
	public NSArray<EOTypeFluxPaiement> listeTypeFluxPaiement() {
		return this.listeTypeFluxPaiement;
	}

	public void setListeTypeFluxPaiement(
			NSArray<EOTypeFluxPaiement> listeTypeFluxPaiement) {
		this.listeTypeFluxPaiement = listeTypeFluxPaiement;
	}

	public void setTypeFluxPaiement(
			EOTypeFluxPaiement typeFluxPaiement) {
		this.typeFluxPaiement = typeFluxPaiement;
	}
	
	
}
