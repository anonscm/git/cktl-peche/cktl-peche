package org.cocktail.peche.components.parametres.mep;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.Messages;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EODefinitionFluxPaiement;
import org.cocktail.peche.entity.EODetailFluxPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * @author juliencallewaert
 *
 */
public class FluxPaieForm extends PecheCreationForm<EOParametresFluxPaiement> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2947888238680837813L;

	private ChampsFluxPaieHelper helper = null;

	private EODetailFluxPaiement detailFluxPaiement = null;

	private NSTimestamp dateDebutDernierTaux = null;


	/**
	 * 
	 * @return
	 */
	public ChampsFluxPaieHelper helper() {
		return this.helper;
	}

	/**
	 * 
	 * @param context
	 */
	public FluxPaieForm(WOContext context) {
		super(context);
		this.helper = new ChampsFluxPaieHelper(edc());
	}

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.FLUX_PAIEMENT_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.FLUX_PAIEMENT_FORM_TITRE_MODIFIER;
	}

	@Override
	protected WOActionResults pageGestion() {
		FluxPaieGestion page = (FluxPaieGestion) pageWithName(FluxPaieGestion.class
				.getName());
		
		return page;
	}

	@Override
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		EOParametresFluxPaiement pfp = this.editedObject();

		PecheApplicationUser appUser = getPecheSession().getApplicationUser();
		EOPersonne personne = appUser.getPersonne();

		dateDebutDernierTaux = pfp.dateDebutDernierParametre(this.helper.typeFluxPaiementSelection());

		if (modeEdition == ModeEdition.CREATION) {
			pfp.majDonnesAuditCreation(personne);
			NSArray<EODetailFluxPaiement> listeDetailFluxPaiement = pfp.toListeDetailFluxPaiement();
			
			if (!listeDetailFluxPaiement.isEmpty()) {
				for (EODetailFluxPaiement dfp : listeDetailFluxPaiement) {
					dfp.majDonnesAuditCreation(personne);
				}
			}
		}

		if (modeEdition == ModeEdition.MODIFICATION) {
			pfp.majDonnesAuditModification(personne);
			NSArray<EODetailFluxPaiement> listeDetailFluxPaiement = pfp.toListeDetailFluxPaiement();
			
			if (!listeDetailFluxPaiement.isEmpty()) {
				for (EODetailFluxPaiement dfp : listeDetailFluxPaiement) {
					dfp.majDonnesAuditModification(personne);
				}
			}
		}
	}

	/**
	 * Valide les données spécifiques à un flux de paiement saisies sur le formulaire
	 * (définition de ces données présentes dans GRH_PECHE.DEFINITION_FLUX_PAIEMENT)
	 * @return
	 */
	protected boolean validerDonnees() {

		boolean controleOK = true;
		
		EOParametresFluxPaiement pfp = this.editedObject();
		
		// La date de début d'application saisie est-elle bien après la dernière date de début ?
		if (ModeEdition.CREATION.equals(getModeEdition())) {
			if (dateDebutDernierTaux == null) {
				dateDebutDernierTaux = pfp.dateDebutDernierParametre(this.helper.typeFluxPaiementSelection());
			}
			if (dateDebutDernierTaux != null) {
				if (!(pfp.dateDebutApplication().after(dateDebutDernierTaux))) {
					SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, 
							Constante.PARAM_MEP_PFP_DATE_NON_VALIDE + SDF.format(dateDebutDernierTaux));
					controleOK = false;
				}
			}

		}
		
		// Vérification des champs spécifiques au type de flux : présence et longueur
		NSArray<EODetailFluxPaiement> listeDetailFluxPaiement = pfp.toListeDetailFluxPaiement();
		
		for (EODetailFluxPaiement eoDetailFluxPaiement : listeDetailFluxPaiement) {
			String valeur = eoDetailFluxPaiement.valeurChamp();
			Integer longueurChampMax = eoDetailFluxPaiement.toDefinitionFluxPaiement().longueurChamp();
			
			if (valeur == null) {
				String message = String.format(message(Messages.FLUX_PAIEMENT_CHAMP_OBLIGATOIRE),
						eoDetailFluxPaiement.toDefinitionFluxPaiement().libelleChamp());
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, message);
				
				controleOK = false;
				
				continue;
			}			
			Integer longueurSaisie = valeur.length();
			if (longueurSaisie > longueurChampMax) {
				
				String message = String.format(message(Messages.FLUX_PAIEMENT_LONGUEUR_SAISIE_EXCESSIVE),
						eoDetailFluxPaiement.toDefinitionFluxPaiement().libelleChamp(), longueurChampMax);
				
				controleOK = false;
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, message);
			}
		}
		
		//On vérifie que le champ Administration a été saisi
		if (editedObject().administration() == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, String.format(message(Messages.FLUX_PAIEMENT_CHAMP_OBLIGATOIRE),
					Constante.ADMINISTRATION));
			controleOK = false;
		}
		
		//On vérifie que le champ Département a été saisi
		if (editedObject().departement() == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, String.format(message(Messages.FLUX_PAIEMENT_CHAMP_OBLIGATOIRE),
					Constante.DEPARTEMENT));
			controleOK = false;
		}
		
		return controleOK;		
	}
	
	@Override
	public WOActionResults enregistrerEtRetour() {	
		if (!validerDonnees()) {
			return null;
		} 
		
		if (ModeEdition.CREATION.equals(getModeEdition())) {
			getPecheSession().addSimpleInfoMessage(message(Messages.FLUX_PAIEMENT_MESSAGE_AJOUT_TITRE), message(Messages.FLUX_PAIEMENT_MESSAGE_AJOUT_DETAIL));
		} else if (ModeEdition.MODIFICATION.equals(getModeEdition())) {
			getPecheSession().addSimpleInfoMessage(message(Messages.FLUX_PAIEMENT_MESSAGE_AJOUT_TITRE), message(Messages.FLUX_PAIEMENT_MESSAGE_MODIFICATION_DETAIL));
		}
		
		return super.enregistrerEtRetour();
	}
	
	@Override
	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		// On ne tient pas compte des messages standards WO
	}

	@Override
	protected EOParametresFluxPaiement creerNouvelObjet() {
		return chargerListeChamps();
	}
	
	/**
	 * 
	 * @return
	 */
	public String nouveauFluxPaiementContainerId() {
		return getComponentId() + "_nouveauFluxPaiementContainerId";
	}

	/**
	 * Action générique d'ajout.
	 * 
	 * @return une instance du formulaire en mode création.
	 */
	public WOActionResults rechargerListeChampsAction() {

		rechargerListeChamps();

		return doNothing();
	}

	/**
	 * Recharge la liste de champs selon le type de flux sélectionné lorsqu'un
	 * changement a été observé dans le formulaire
	 * 
	 * @return
	 */
	private void rechargerListeChamps() {

		edc().revert();

		setEditedObject(chargerListeChamps());

	}

	/**
	 * Retourne la liste de champs selon le type de flux sélectionné lors du
	 * premier affichage du formulaire
	 * 
	 * @return
	 */
	private EOParametresFluxPaiement chargerListeChamps() {

		EOParametresFluxPaiement eoParametresFluxPaiement = EOParametresFluxPaiement
				.creerEtInitialiser(edc(),
						this.helper.typeFluxPaiementSelection());

		NSArray<EODefinitionFluxPaiement> listeDefinitionFluxPaiement = this.helper
				.typeFluxPaiementSelection().toListeDefinitionFluxPaiement();

		for (EODefinitionFluxPaiement eoDefinitionFluxPaiement : listeDefinitionFluxPaiement) {

			EODetailFluxPaiement.creerEtInitialiser(edc(),
					eoDefinitionFluxPaiement, eoParametresFluxPaiement);

		}

		return eoParametresFluxPaiement;

	}

	/**
	 * @return the editiondateFin
	 */
	public String editionDateFin() {

		if (this.editedObject().dateFinApplication() == null || !getAutorisation().hasDroitUtilisationParametrageTypeFluxPaiement()) {
			return "false";
		}
		return "true";
	}
	
	/**
	 * @return the etatdatedebut
	 */
	public boolean etatdatedebut() {
		if (ModeEdition.CREATION.equals(getModeEdition())) {
			return false;
		}
		
		return true;
	}	
	
	/**
	 * @return the etatdatefin
	 */
	public boolean etatdatefin() {
		return true;
	}

	/**
	 * 
	 * @return
	 */
	public boolean droitUtilisationParamTypeFluxPaiement() {
		return getAutorisation().hasDroitUtilisationParametrageTypeFluxPaiement(); 
	}

	
	/**
	 * 
	 * @return
	 */
	public boolean droitConnaissanceParamTypeFluxPaiement() {
		return getAutorisation().hasDroitConnaissanceParametrageTypeFluxPaiement(); 
	}

	/**
	 * @return la liste triée sur le codeChamp de la définition
	 */
	public NSArray<EODetailFluxPaiement> listeDetailFluxPaiementTriee() {
		NSArray<EODetailFluxPaiement> liste  = this.editedObject().toListeDetailFluxPaiement();
		NSArray<EODetailFluxPaiement> listeTriee  = new NSMutableArray<EODetailFluxPaiement>();

		HashMap<String, EODetailFluxPaiement> map = new HashMap<String, EODetailFluxPaiement>(); 
		List<String> listeCode = new ArrayList<String>();

		for (EODetailFluxPaiement detail : liste) {
			String code = detail.toDefinitionFluxPaiement().codeChamp();
			listeCode.add(code);
			map.put(code, detail);
		}		
		Collections.sort(listeCode);
		for (String code : listeCode) {
			listeTriee.add(map.get(code));
		}

		return listeTriee;	
	}
	
	public EODetailFluxPaiement getDetailFluxPaiement() {
		return detailFluxPaiement;
	}

	public void setDetailFluxPaiement(EODetailFluxPaiement detailFluxPaiement) {
		this.detailFluxPaiement = detailFluxPaiement;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_FLUX_PAIEMENT.getId();
	}
}