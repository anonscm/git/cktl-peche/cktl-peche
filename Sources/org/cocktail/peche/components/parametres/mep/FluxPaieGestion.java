package org.cocktail.peche.components.parametres.mep;

import org.cocktail.peche.Messages;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant la gestion des flux de paiements
 * 
 * @author juliencallewaert
 *
 */
public class FluxPaieGestion extends PecheGestionComponent<EOParametresFluxPaiement, FluxPaieForm> {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5994852332206271034L;

	@Override
	protected String getEntityName() {
		return EOParametresFluxPaiement.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return FluxPaieForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.PARAMETRES_FLUX_PAIEMENT_CONFIRM_SUPPRESS;
	}

	/**
	 * 
	 * @param context
	 */
	public FluxPaieGestion(WOContext context) {
        super(context);
        displayGroup().setSortOrderings(EOParametresFluxPaiement.DATE_DEBUT_APPLICATION.ascs());
		displayGroup().fetch();
    }
	
	@Override
	public WOActionResults deleteAction() {
		getSelectedItem().majDonnesAuditSuppression();
		super.deleteAction();
		getPecheSession().addSimpleInfoMessage(message(Messages.FLUX_PAIEMENT_MESSAGE_SUPPRESSION_TITRE), message(Messages.FLUX_PAIEMENT_MESSAGE_SUPPRESSION_DETAIL));
		return null;
	}

	/**
	 * @return the nePasAutoriserBoutonSuppression
	 */
	public Boolean nePasAutoriserBoutonSuppression() {
		EOParametresFluxPaiement eoParametresFluxPaiement = getSelectedItem();
		if (eoParametresFluxPaiement != null) {
			NSTimestamp dateFin = eoParametresFluxPaiement.dateFinApplication();
			if (dateFin == null) {
				return false;
			}
		}		
		return true;		
	}
	
	/**
	 * @return the nePasAutoriserBoutonModification
	 */
	public Boolean nePasAutoriserBoutonModification() {
		EOParametresFluxPaiement eoParametresFluxPaiement = getSelectedItem();
		if (eoParametresFluxPaiement != null) {
			return false;
		}		
		return true;		
	}	
	
	/**
	 * @return the nePasAutoriserBoutonDetail
	 */
	public Boolean nePasAutoriserBoutonDetail() {
		return nePasAutoriserBoutonModification();		
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_FLUX_PAIEMENT.getId();
	}	
	
	
	
}