package org.cocktail.peche.components.parametres.mep;

import java.text.SimpleDateFormat;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.Messages;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOTauxHoraire;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant de modifier ou d'ajouter les taux horaires
 * 
 * @author Laurent Pringot
 * 
 */
public class TauxHorForm extends PecheCreationForm<EOTauxHoraire> {
		
		
	private static final long serialVersionUID = -5210523160651079407L;

	private NSTimestamp dateDebutDernierTaux = null;
	
	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public TauxHorForm(WOContext context) {			
        super(context);
    }

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.TAUX_HORAIRE_FORM_TITRE_AJOUTER;
	}
		
	@Override
	protected String getFormTitreModificationKey() {
		return Messages.TAUX_HORAIRE_FORM_TITRE_MODIFIER;
	}

		
	@Override
	protected WOActionResults pageGestion() {
		TauxHorGestion page = (TauxHorGestion) pageWithName(TauxHorGestion.class.getName());
		return page;
	}

	public String nouveauTauxHoraireContainerId() {
		return getComponentId() + "_nouveauTauxHorContainerId";
	}
		
	@Override
	protected EOTauxHoraire creerNouvelObjet() {
		
			EOTauxHoraire eoTauxHoraire = EOTauxHoraire.creerEtInitialiser(edc());
			return eoTauxHoraire;				
	}
		
	
	protected void beforeSaveChanges(ModeEdition modeEdition) {
							
		EOTauxHoraire th = this.editedObject();
		
		PecheApplicationUser appUser = getPecheSession().getApplicationUser();
		EOPersonne personne = appUser.getPersonne();
		
		dateDebutDernierTaux = th.dateDebutDernierTaux();
		
		if (modeEdition == ModeEdition.CREATION) {
			th.majDonnesAuditCreation(personne);
		}
		
		if (modeEdition == ModeEdition.MODIFICATION) {
			th.majDonnesAuditModification(personne);				
		}			
	}
		
	@Override
	public WOActionResults enregistrerEtRetour() {
		
		EOTauxHoraire th = this.editedObject();
		
		if (getModeEdition() == ModeEdition.CREATION) {
			if (dateDebutDernierTaux == null) {
				dateDebutDernierTaux = th.dateDebutDernierTaux();					
			}
			if (dateDebutDernierTaux != null) {
				if (!(th.dateDebutApplication().after(dateDebutDernierTaux))) {
					SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy");						
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.PARAM_MEP_TH_DATE_NON_VALIDE + SDF.format(dateDebutDernierTaux));
					return null;
				}
			}
							
		}
	
		return super.enregistrerEtRetour();
	}

	/**
	 * @return the editiondateFin
	 */
	public String editionDateFin() {
		
		if (this.editedObject().dateFinApplication() == null) {
			return "false";
		}
		return "true";
	}

	/**
	 * @return the etatdatedebut
	 */
	public boolean etatdatedebut() {
		if (getModeEdition() == ModeEdition.CREATION) {
			return false;
		}
		
		return true;
	}	
	
	/**
	 * @return the etatdatefin
	 */
	public boolean etatdatefin() {
		return true;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_TAUX_HORAIRE.getId();
	}
}

