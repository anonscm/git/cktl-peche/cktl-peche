package org.cocktail.peche.components.parametres.duplication;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.OffreFormationCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity._EOActuelEnseignant;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public class Duplication extends CktlPecheBaseComponent {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = -3548022910179957212L;

	private Integer anneeCible;
	
	private EnseignantsStatutairesCtrl controller;
	
	private UECtrl controleurUe;
	
	private boolean uefDansEtabChecked;

	private boolean uefHorsEtabChecked;

	private boolean enseignantsStatutairesChecked;
	
	private OffreFormationCtrl controleurOffreFormation;
	
	public Duplication(WOContext context) {
        super(context);
        controller = new EnseignantsStatutairesCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(), getPecheSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
        controleurOffreFormation = new OffreFormationCtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        controleurUe = new UECtrl(edc(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
        determinerAnneeCible();
        enseignantsStatutairesChecked = true;        		
    }
	
	/**
	 * 
	 * @return liste enseignants
	 */
	public NSArray<EOActuelEnseignant> getListeEnseignantsStatutairesADupliquer() {
		
		NSArray<EOActuelEnseignant> listeEnseignantsStatutairesADupliquer = new NSMutableArray<EOActuelEnseignant>();
		
	
		EOQualifier qualifierListeEnseignants  = controller.getListeEnseignants(null, anneeCible, false, false, null);
		
		/* MIS EN COMMENTAIRES SUITE MISE EN PLACE PROCESS DESYNCHRONISE
		EOQualifier ficheServiceNonExistanteSurAnneeCibleQual = null;
		if (!EOPecheParametre.isFicheVoeuxUtilisation(edc())) {
			ficheServiceNonExistanteSurAnneeCibleQual = ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOService.ANNEE.key(), anneeCible), EOActuelEnseignant.LISTE_SERVICES.key()));
		} else {
			ficheServiceNonExistanteSurAnneeCibleQual = ERXQ.not(new ERXExistsQualifier(ERXQ.equals(EOFicheVoeux.ANNEE.key(), anneeCible), EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY));
		}
		EOQualifier qualifier = ERXQ.and(qualifierListeEnseignants, ficheServiceNonExistanteSurAnneeCibleQual);
		*/
		
		EOQualifier qualifier = ERXQ.and(qualifierListeEnseignants);
		
		ERXFetchSpecification<EOActuelEnseignant> fetchSpec = new ERXFetchSpecification<EOActuelEnseignant>(_EOActuelEnseignant.ENTITY_NAME, qualifier, EOActuelEnseignant.getTriParDefaut());
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(true);
	    NSArray<EOActuelEnseignant> listeEnseignantsStatutairesPotentielsTmp = fetchSpec.fetchObjects(edc());
		
	    for (EOActuelEnseignant enseignant : listeEnseignantsStatutairesPotentielsTmp) {
			
	    	EOService service = enseignant.getService(controller.getAnneeUniversitairePrecedente(anneeCible));
	    	
	    	if (service != null) {
		    	EODemande demande = controller.getDemande(service);
		    	if (EtapePeche.VALIDEE.getCodeEtape().equals(demande.toEtape().codeEtape()) 
		    			&& CircuitPeche.PECHE_STATUTAIRE.getCodeCircuit().equals(demande.toEtape().toCircuitValidation().codeCircuitValidation())) {
		    		listeEnseignantsStatutairesADupliquer.add(enseignant);
		    	}
	    	}
		}
	    
		return listeEnseignantsStatutairesADupliquer;
		
	}
	
	
	
	private void determinerAnneeCible() {
		anneeCible = controller.getAnneeUniversitaireSuivante(getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours());
	}
	
	
	/**
	 * @return menu pour la duplication des enseignants statutaires
	 */
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_DUPLICATION_STATUTAIRES.getId();
	}

	public String getAnneeCible() {
		return (String) listeAnneesUniversitaires().valueForKey(anneeCible.toString());
	}

	/**
	 * 
	 * @return action 
	 */
	public WOActionResults dupliquer() {
		
		
		if (uefDansEtabChecked()) {
		
			int nbDuplications = controleurUe.dupliquerUEFlottantesDansEtablissement(controleurOffreFormation.rechercherUeNonDiplomanteDansEtablissement(edc()), 
					anneeCible, personneConnecte());
			
			getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, String.format(Constante.DUPLICATION_UEF_DANS, String.valueOf(nbDuplications)));
			
		}
		
		if (uefHorsEtabChecked()) {
			
			int nbDuplications = controleurUe.dupliquerUEFlottantesHorsEtablissement(controleurOffreFormation.rechercherUeNonDiplomanteHorsEtablissement(edc()), anneeCible, personneConnecte());
			
			getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, String.format(Constante.DUPLICATION_UEF_HORS, String.valueOf(nbDuplications)));
			
		}
		
		if (enseignantsStatutairesChecked()) {
		
			int nbDuplications = controller.dupliquerEnseignants(getListeEnseignantsStatutairesADupliquer(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours(),
				anneeCible, personneConnecte());
			
			getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, String.format(Constante.DUPLICATION_FICHE, String.valueOf(nbDuplications)));
		
		}
		
		return doNothing();
	}

	/**
	 * @return the uefDansEtabChecked
	 */
	public boolean uefDansEtabChecked() {
		return uefDansEtabChecked;
	}

	/**
	 * @param uefDansEtabChecked the uefDansEtabChecked to set
	 */
	public void setUefDansEtabChecked(boolean uefDansEtabChecked) {
		this.uefDansEtabChecked = uefDansEtabChecked;
	}

	/**
	 * @return the uefHorsEtabChecked
	 */
	public boolean uefHorsEtabChecked() {
		return uefHorsEtabChecked;
	}

	/**
	 * @param uefHorsEtabChecked the uefHorsEtabChecked to set
	 */
	public void setUefHorsEtabChecked(boolean uefHorsEtabChecked) {
		this.uefHorsEtabChecked = uefHorsEtabChecked;
	}

	/**
	 * @return the enseignantsStatutairesChecked
	 */
	public boolean enseignantsStatutairesChecked() {
		return enseignantsStatutairesChecked;
	}

	/**
	 * @param enseignantsStatutairesChecked the enseignantsStatutairesChecked to set
	 */
	public void setEnseignantsStatutairesChecked(
			boolean enseignantsStatutairesChecked) {
		this.enseignantsStatutairesChecked = enseignantsStatutairesChecked;
	}
	
	
	
}