package org.cocktail.peche.components.parametres.reh;

import org.cocktail.peche.Messages;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheGestionComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Classe permettant la gestion des activités du Référentiel des Equivalences
 * Horaires
 *
 * @author Chama LAATIK
 *
 */
public class RehGestion extends PecheGestionComponent<EOReh, RehForm> {

	private static final long serialVersionUID = 9213617561115769708L;

	/**
	 * Constructeur.
	 *
	 * @param context
	 *            le WOContext.
	 */
	public RehGestion(WOContext context) {
		super(context);
	}
	
	@Override
	protected String getEntityName() {
		return EOReh.ENTITY_NAME;
	}

	@Override
	protected Class<?> getFormClass() {
		return RehForm.class;
	}

	@Override
	protected String getConfirmSupressMessageKey() {
		return Messages.REH_GESTION_ALERTE_CONFIRM_SUPPRESS;
	}
	
	@Override
	public WOActionResults deleteAction() {
		if (controleAffectationExistante(getSelectedItem())) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.REPARTITION_EXISTANTE_REH);
		}
		
		super.deleteAction();
		return null;
	}

	private boolean controleAffectationExistante(EOReh reh) {
		NSArray<EOServiceDetail> listeServiceDetail = EOServiceDetail.fetchEOServiceDetails(edc(), ERXQ.equals(EOServiceDetail.REH_KEY, reh), null);
		
		if (listeServiceDetail.size() > 0) {
			return true;
		}
		
		return false;
		
	}
	

	/**
	 * @return the nePasAutoriserBoutonSuppression
	 */
	public Boolean nePasAutoriserBoutonSuppression() {
		EOReh eoReh = getSelectedItem();
		if (eoReh != null) {
			return false;
		}		
		return true;		
	}
	
	/**
	 * @return the nePasAutoriserBoutonModification
	 */
	public Boolean nePasAutoriserBoutonModification() {
		EOReh eoReh = getSelectedItem();
		if (eoReh != null) {
			return false;
		}		
		return true;		
	}

	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_REH.getId();
	}	
	
	
}
