package org.cocktail.peche.components.parametres.reh;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.Messages;
import org.cocktail.peche.PecheApplicationUser;
import org.cocktail.peche.components.commun.ModeEdition;
import org.cocktail.peche.components.commun.PecheCreationForm;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe permettant de modifier ou d'ajouter les activités du Référentiel des
 * Equivalences Horaires
 * 
 * @author Chama LAATIK
 * 
 */
public class RehForm extends PecheCreationForm<EOReh> {

	private static final long serialVersionUID = 8091133571530959416L;

	/**
	 * Constructeur.
	 * 
	 * @param context
	 *            le WOContext.
	 */
	public RehForm(WOContext context) {
		super(context);
	}

	@Override
	protected String getFormTitreCreationKey() {
		return Messages.REH_FORM_TITRE_AJOUTER;
	}

	@Override
	protected String getFormTitreModificationKey() {
		return Messages.REH_FORM_TITRE_MODIFIER;
	}

	@Override
	protected EOReh creerNouvelObjet() {
		return EOReh.creerEtInitialiser(edc());
	}

	@Override
	protected WOActionResults pageGestion() {
		RehGestion page = (RehGestion) pageWithName(RehGestion.class.getName());
		return page;
	}

	/**
	 * @return l'identifiant du conteneur principal.
	 */
	public String nouveauRehContainerId() {
		return getComponentId() + "_nouveauRehContainerId";
	}

	/**
	 * Enregistre une activité REH.
	 * 
	 * @throws ValidationException
	 *             : exception au cours de l'enregistrement.
	 */
	protected void beforeSaveChanges(ModeEdition modeEdition) {
		NSTimestamp now = new NSTimestamp();
		EOReh o = this.editedObject();
		PecheApplicationUser appUser = getPecheSession().getApplicationUser();
		EOPersonne personne = appUser.getPersonne();
		
		OutilsValidation.verifierOrdreDates(this.editedObject().dateDebut(), this.editedObject().dateFin(), getPecheSession());
		
		if (modeEdition == ModeEdition.CREATION) {
			o.setPersonneCreation(personne);
			o.setDateCreation(now);
		}

		if (modeEdition == ModeEdition.MODIFICATION) {
			o.setPersonneModification(personne);
		}
		o.setDateModification(now);
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_REH.getId();
	}

}
