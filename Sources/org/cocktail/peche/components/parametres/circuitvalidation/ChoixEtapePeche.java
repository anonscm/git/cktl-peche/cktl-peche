package org.cocktail.peche.components.parametres.circuitvalidation;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation.ChoixEtapePage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

/**
 * Page "Choix d'une étape" permettant de changer l'étape d'arrivée d'un chemin.
 * 
 * @author Pascal MACOUIN
 */
public class ChoixEtapePeche extends CktlPecheTableComponent<EOEtape> implements ChoixEtapePage {
	
	/** Numéro de série. */
	private static final long serialVersionUID = 4666692343819926862L;
	/** 
	 * Page appelante de cette page.
	 * <p>
	 * C'est la page qui sera rappelée à la fermeture de celle-ci.
	 */
	private WOComponent pageAppelante;
	//private DefinitionCircuitsValidation pageAppelante;

	/** Le chemin à modifier. */
	private EOChemin chemin;
	
	private EOEditingContext editingContext;
	
	/**
	 * Constructeur
	 * 
	 * @param context Un contexte WO
	 */
    public ChoixEtapePeche(WOContext context) {
        super(context);
    }

   /* public void setPageAppelante(DefinitionCircuitsValidation pageAppelante) {
		this.pageAppelante = pageAppelante;
	}*/

    /**
     * Affecte les critères nécessaires à la fabrication du qualifier et fetch du display group.
     * 
     * @param chemin Le chemin à modifier
     */
/*	public void setCriteresQualifier(EOChemin chemin) {
    	this.chemin = chemin;

//    	EOCircuitValidation circuitValidation = chemin.etapeDepart().circuit();
//
//		ERXAndQualifier qualifier = ERXQ.equals(ERXQ.keyPath(EOEtape.CIRCUIT_KEY, EOCircuitValidation.APPLICATION_KEY, EOGdApplication.APP_STR_ID_KEY), circuitValidation.application().appStrId())
//				.and(EOEtape.CIRCUIT.dot(EOCircuitValidation.CODE).eq(circuitValidation.code()))
//				.and(EOEtape.CIRCUIT.dot(EOCircuitValidation.NUMERO_VERSION).eq(circuitValidation.numeroVersion()))
//				.and(EOEtape.CODE.isNot(chemin.etapeDepart().code()));
//		
//		if (chemin.etapeArrivee() != null) {
//			qualifier = qualifier.and(EOEtape.CODE.isNot(chemin.etapeArrivee().code()));
//		}
//		
//		fetchDisplay(qualifier, EOEtape.ENTITY_NAME);
		
    	NSArray<EOEtape> listeEtapes = chemin.toEtapeDepart().toCircuitValidation().etapesUtilisees();
    	
    	// On enlève l'étape d'arrivée et l'étape de départ
    	NSMutableArray<EOEtape> listeEtapesAffichees = new NSMutableArray<EOEtape>();
    	for (EOEtape uneEtape : listeEtapes) {
			if (uneEtape != chemin.toEtapeArrivee() && uneEtape != chemin.toEtapeDepart()) {
				listeEtapesAffichees.add(uneEtape);
			}
		}
    	
		setEtapes(listeEtapesAffichees);
    }*/

	/**
	 * Affecte les étapes au display group.
	 * 
	 * @param etapes Une liste d'étapes à affichées
	 */
/*	private void setEtapes(NSArray<EOEtape> etapes) {
		EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOEtape.class), edc());
		dataSource.setArray(etapes);

		ERXDisplayGroup<EOEtape> dg = this.getDisplayGroup();
		dg.setDataSource(dataSource);
		dg.fetch();
	}*/
	
	/**
	 * Annuler la sélection de l'étape et retourner à la page précédente.
	 * 
	 * @return La page précédente
	 */
/*	public WOActionResults annulerAction() {
		return pageAppelante;
	}*/

	/**
	 * Changer l'étape d'arrivée du chemin par l'étape sélectionné et retourner à la page précédente.
	 * 
	 * @return La page précédente
	 */
	/*public WOActionResults selectionnerAction() {
		// Si rien n'est sélectionné, on reste sur l'écran
		if (getSelectedObject() == null) {
			return null;
		}
		
		EOEtape etapeSelectionnee = getSelectedObject();
		
		// Recherche de l'étape dans l'ec d'origine
		for (EOEtape uneEtape : chemin.toEtapeDepart().toCircuitValidation().etapesUtilisees()) {
			if (uneEtape.codeEtape().equals(etapeSelectionnee.codeEtape())) {
				chemin.setToEtapeArriveeRelationship(uneEtape);
				break;
			}
		}
		
		return pageAppelante;
	}*/

	public WOComponent getPageAppelante() {
		return pageAppelante;
	}

	public void setPageAppelante(WOComponent pageAppelante) {
		this.pageAppelante = pageAppelante;
	}

	public EOChemin getChemin() {
		return chemin;
	}

	public void setChemin(EOChemin chemin) {
		this.chemin = chemin;
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_CIRCUIT_VALIDATION.getId();
	}


}