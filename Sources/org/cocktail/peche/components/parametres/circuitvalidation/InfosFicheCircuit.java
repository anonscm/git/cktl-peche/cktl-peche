package org.cocktail.peche.components.parametres.circuitvalidation;

import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.peche.entity.interfaces.IFiche;

public class InfosFicheCircuit {

	
	private EOCircuitValidation versionCircuit;
	private List<IFiche> listeFicheEtatInitial;
	private Integer nbFicheAutreEtape;
	
	public InfosFicheCircuit(EOCircuitValidation versionCircuit, List<IFiche> listeFicheEtatInitial, Integer nbFicheAutreEtape) {
		super();
		this.versionCircuit = versionCircuit;
		this.listeFicheEtatInitial = listeFicheEtatInitial;
		this.nbFicheAutreEtape = nbFicheAutreEtape;
	}
	
	public List<IFiche> getListeFicheEtatInitial() {
		return listeFicheEtatInitial;
	}
	public void setListeFicheEtatInitial(List<IFiche> listeFicheEtatInitial) {
		this.listeFicheEtatInitial = listeFicheEtatInitial;
	}
	public EOCircuitValidation getVersionCircuit() {
		return versionCircuit;
	}
	public void setVersionCircuit(EOCircuitValidation versionCircuit) {
		this.versionCircuit = versionCircuit;
	}
	
	public static Comparator<InfosFicheCircuit> comparateurNumeroVersionAsc = new Comparator<InfosFicheCircuit>() {
		public int compare(InfosFicheCircuit  profil1, InfosFicheCircuit  profil2) {
			return profil1.getVersionCircuit().numeroVersion().compareTo(profil2.getVersionCircuit().numeroVersion());
		}
	};

	public Integer getNbFicheAutreEtape() {
		return nbFicheAutreEtape;
	}

	public void setNbFicheAutreEtape(Integer nbFicheAutreEtape) {
		this.nbFicheAutreEtape = nbFicheAutreEtape;
	}
	
	
	
}
