package org.cocktail.peche.components.parametres.circuitvalidation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.Messages;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.interfaces.IFiche;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class ApplicationCircuitsValidation extends CktlPecheBaseComponent {
    
	
	private List<EOCircuitValidation> listeCircuitsValidation;
	private EOCircuitValidation currentCircuit;
	private EOCircuitValidation selectedCircuit;
	
	private List<InfosFicheCircuit> listeFicheVersionBean;
	private InfosFicheCircuit currentFicheVersionBean;
	
	public ApplicationCircuitsValidation(WOContext context) {
        super(context);
    }
	
	
	/**
	 * Retourne la liste des circuits de validation.
	 * 
	 * @return La liste des circuits de validation
	 */
	public List<EOCircuitValidation> listeCircuitsValidation() {
		if (listeCircuitsValidation == null) {
			listeCircuitsValidation = EOCircuitValidation.rechercherCircuitsValidation(edc(), Constante.APP_PECHE);

		}
		return listeCircuitsValidation;
	}


	@Override
	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_APPLI_CIRCUIT_VALIDATION.getId();
	}


	public EOCircuitValidation getCurrentCircuit() {
		return currentCircuit;
	}


	public EOCircuitValidation getSelectedCircuit() {
		return selectedCircuit;
	}


	public void setCurrentCircuit(EOCircuitValidation currentCircuit) {
		this.currentCircuit = currentCircuit;
	}


	public void setSelectedCircuit(EOCircuitValidation selectedCircuit) {
		this.selectedCircuit = selectedCircuit;
	}
	
	public String getListeFicheServiceId() {
		return "listeFicheServiceId";
	}
	
	private List<EOCircuitValidation> recupererToutesVersionsCircuit() {
		return EOCircuitValidation.rechercherCircuitsValidation(edc(), Constante.APP_PECHE, getSelectedCircuit().codeCircuitValidation());
	}
	
	private EOCircuitValidation recupererVersionActive() {
		return EOCircuitValidation.rechercherCircuitValidationUtilisable(edc(), Constante.APP_PECHE, getSelectedCircuit().codeCircuitValidation());
	}
	
	public List<InfosFicheCircuit> getListeFicheVersionBean() {
		
		listeFicheVersionBean = new ArrayList<InfosFicheCircuit>();
		
		if (getSelectedCircuit() != null) {
			
			List<IFiche> listeFiches = new ArrayList<IFiche>();
			
			
			List<EOCircuitValidation> circuits = recupererToutesVersionsCircuit();
			
			if (estCircuitFicheVoeuSelected()) {
				for (EOCircuitValidation versionCircuit : circuits) {
					List<IFiche> listeFichesVoeux = new ArrayList<IFiche>(EOFicheVoeux.fetchEOFicheVoeuxes(edc(), getQualifierFicheVoeuxEtatInitial(versionCircuit), null));
					listeFiches.addAll(listeFichesVoeux);
					Integer nbFichesPasEtatInitial = EOFicheVoeux.fetchEOFicheVoeuxes(edc(), getQualifierFicheVoeuxPasEtatInitial(versionCircuit), null).size();
					listeFicheVersionBean.add(new InfosFicheCircuit(versionCircuit,  listeFichesVoeux, nbFichesPasEtatInitial));
				}
			} else {
				for (EOCircuitValidation versionCircuit : circuits) {
					List<IFiche> listeFichesService = new ArrayList<IFiche>(EOService.fetchAllEOServicesDistinct(edc(), getQualifierFicheServiceEtatInitial(versionCircuit), null));
					listeFiches.addAll(listeFichesService);
					Integer nbFichesPasEtatInitial = EOService.fetchAllEOServicesDistinct(edc(), getQualifierFicheServicePasEtatInitial(versionCircuit), null).size();
					listeFicheVersionBean.add(new InfosFicheCircuit(versionCircuit,  listeFichesService, nbFichesPasEtatInitial));
				}
			}
			
		} 
		
		Collections.sort(listeFicheVersionBean, InfosFicheCircuit.comparateurNumeroVersionAsc);
		
		return listeFicheVersionBean;
	} 
	
	private EOQualifier getQualifierFicheVoeuxEtatInitial(EOCircuitValidation circuit) {
		return ERXQ.and(ERXQ.equals(EOFicheVoeux.TO_DEMANDE.dot(EODemande.TO_ETAPE).key(), circuit.etapeInitiale()),						
				ERXQ.equals(EOFicheVoeux.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
	}
	
	private EOQualifier getQualifierFicheVoeuxPasEtatInitial(EOCircuitValidation circuit) {
		return ERXQ.and(ERXQ.and(ERXQ.in(EOFicheVoeux.TO_DEMANDE.dot(EODemande.TO_ETAPE).key(), circuit.etapesUtilisees()),						
				ERXQ.equals(EOFicheVoeux.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()))
				, ERXQ.and(ERXQ.notEquals(EOFicheVoeux.TO_DEMANDE.dot(EODemande.TO_ETAPE).key(), circuit.etapeInitiale()),						
				ERXQ.equals(EOFicheVoeux.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())));
	}
	
	private EOQualifier getQualifierFicheServiceEtatInitial(EOCircuitValidation circuitValidation) {
		return ERXQ.and(ERXQ.equals(EOService.TO_LISTE_REPART_SERVICE.dot(
				EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE)).key(), circuitValidation.etapeInitiale()),						
				ERXQ.equals(EOService.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours()));
	}
	
	private EOQualifier getQualifierFicheServicePasEtatInitial(EOCircuitValidation circuitValidation) {
		return  ERXQ.and(ERXQ.and(ERXQ.in(EOService.TO_LISTE_REPART_SERVICE.dot(
				EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE)).key(), circuitValidation.etapesUtilisees()),						
				ERXQ.equals(EOService.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())),
						ERXQ.and(ERXQ.notEquals(EOService.TO_LISTE_REPART_SERVICE.dot(
				EORepartService.TO_DEMANDE.dot(EODemande.TO_ETAPE)).key(), circuitValidation.etapeInitiale()),						
				ERXQ.equals(EOService.ANNEE.key(), getPecheSession().getPecheParametres().getAnneeUniversitaireEnCours())));
	}
	
	
	public WOActionResults appliquerCircuit() {
		
		EOCircuitValidation circuitValidationActif = recupererVersionActive();
		
		int compteur = 0;
		
		for (InfosFicheCircuit element : listeFicheVersionBean) {
			if (element.getVersionCircuit().equals(circuitValidationActif)) {
				// version déjà active - inutile de faire l'application du circuit actif
				continue;
			} else {
				List<IFiche> listeFiches = element.getListeFicheEtatInitial();
				for (IFiche fiche : listeFiches) {
					fiche.reinitialiserDemandes(edc(), personneConnecte().persId(), circuitValidationActif);
					compteur++;
				}
			}
		}
		
		
		if (edc().hasChanges()) {
			edc().saveChanges();
		}
		
		getPecheSession().addSimpleMessage(TypeMessage.INFORMATION, String.format(message(Messages.APPLICATION_CIRCUIT_VALIDATION), String.valueOf(compteur)));
		
		return null;
	}
	
	public boolean boutonApplicationDisabled() {
		return getSelectedCircuit() == null;
	}
	
	private boolean estCircuitFicheVoeuSelected() {
		return CircuitPeche.PECHE_VOEUX.getCodeCircuit().equals(getSelectedCircuit().codeCircuitValidation());
	}


	public void setListeFicheVersionBean(
			List<InfosFicheCircuit> listeFicheVersionBean) {
		this.listeFicheVersionBean = listeFicheVersionBean;
	}


	public InfosFicheCircuit getCurrentFicheVersionBean() {
		return currentFicheVersionBean;
	}


	public void setCurrentFicheVersionBean(InfosFicheCircuit currentFicheVersionBean) {
		this.currentFicheVersionBean = currentFicheVersionBean;
	}
	
	
}