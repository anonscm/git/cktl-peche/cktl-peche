package org.cocktail.peche.components.parametres.circuitvalidation;

import org.cocktail.peche.components.commun.CktlPecheBaseComponent;
import org.cocktail.peche.components.commun.PecheMenuItem;

import com.webobjects.appserver.WOContext;

/**
 * Page 'Définition des circuits de validation'.
 * 
 * @author Pascal MACOUIN
 */
public class CircuitsValidationPeche extends CktlPecheBaseComponent {
	
	/** Numéro de série. */
	private static final long serialVersionUID = 4348417172448241913L;
	
	/**
	 * Constructeur.
	 * 
	 * @param context Le contexte WO
	 */
	public CircuitsValidationPeche(WOContext context) {
        super(context);      
    }

	public String getSelectedItemId() {
		return PecheMenuItem.PARAM_CIRCUIT_VALIDATION.getId();
	}
	

}