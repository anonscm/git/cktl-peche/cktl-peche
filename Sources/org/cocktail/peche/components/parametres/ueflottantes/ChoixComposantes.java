package org.cocktail.peche.components.parametres.ueflottantes;

import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.CktlPecheTableComponent;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.commun.PecheMenuItem;
import org.cocktail.peche.entity.EORepartUefStructure;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

/**
 * 
 * @author juliencallewaert
 *
 */
public class ChoixComposantes extends CktlPecheTableComponent<EORepartUefStructure> {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 7487728302118808829L;
	
	private EOStructure structureSelectionnee;
	
	private EOQualifier serviceQualifier;
	
	/**
	 * 
	 * @param context contexte
	 */
	public ChoixComposantes(WOContext context) {
        super(context);
        
		ERXFetchSpecification<EORepartUefStructure> fetchSpec = EORepartUefStructure.fetchSpec().sort(EORepartUefStructure.getTriParDefaut());
		fetchDisplay(fetchSpec);
		
    }
	
	public Integer getUtilisateurPersId() {
		return personneConnecte().persId();
	}

	public boolean getAllGroupsSelect() {
		return true;
	}
		
	public EOStructure getStructureSelectionnee() {
		return structureSelectionnee;
	}
	
	public boolean isAllExpended() {
		return true;
	}

	public void setStructureSelectionnee(EOStructure structureSelectionnee) {
		this.structureSelectionnee = structureSelectionnee;
	}
	
	/**
	 * 
	 * @return actionResults
	 */
	public WOActionResults ajouterRepartUefStructure() {
		if (!validationAjout()) {
			return doNothing();
		}
		
		EORepartUefStructure repartUefStructure = EORepartUefStructure.creerEtInitialiser(edc());
		repartUefStructure.majDonnesAuditCreation(getPecheSession().getApplicationUser().getPersonne());
		repartUefStructure.setToStructure(structureSelectionnee);
		edc().saveChanges();
		
		getDisplayGroup().fetch();
		
		return doNothing();
	}
	
	/**
	 * 
	 * @return actionResults
	 */
	public WOActionResults supprimerRepartUefStructure() {
		if (!validationSuppression()) {
			return doNothing();
		}
		edc().deleteObject(getSelectedObject());
		edc().saveChanges();
		
		setSelectedObject(null);
		getDisplayGroup().fetch();
		
		return doNothing();
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean validationAjout() {
		boolean validationOK = true;
		if (getStructureSelectionnee() == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.UEF_COMPOSANTE_AJOUT_SANS_SELECTION);
			validationOK = false;
		} else {
			NSArray<EORepartUefStructure> allEoRepartStructure = getDisplayGroup().allObjects();

			for (EORepartUefStructure repartUefStructure : allEoRepartStructure) {
				if (getStructureSelectionnee().equals(repartUefStructure.toStructure())) {
					getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.UEF_COMPOSANTE_AJOUT_DOUBLON);
					validationOK = false;
				}
			}
			
			if (getPecheSession().getComposantesScolarite().contains(getStructureSelectionnee())) {
				getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.UEF_COMPOSANTE_AJOUT_CS);
				validationOK = false;
			}
		}
		return validationOK;
	}
	
	/**
	 * 
	 * @return
	 */
	private boolean validationSuppression() {
		boolean validationOK = true;
		if (getSelectedObject() == null) {
			getPecheSession().addSimpleMessage(TypeMessage.ERREUR, Constante.UEF_COMPOSANTE_SUPPRESSION_SANS_SELECTION);
			validationOK = false;
		}
		return validationOK;
	}
	
	/**
	 * 
	 * @return
	 */
	public EOQualifier getServiceQualifier() {
		if (serviceQualifier == null) {
			serviceQualifier = ERXQ.and(EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES); 
		}
		return serviceQualifier;
	}
	
	
	/**
	 * 
	 * @return les structures exclus de l'arbre <=> 
	 */
	public NSArray<EOStructure> getStructuresExclus() {
		return getListHelper().bidouilleListeStructurePourWO(getPecheSession().getDepartements());
	}

	public String getSelectedItemId() {
		return PecheMenuItem.CHOIX_COMPOSANTES.getId();
	}
	
}