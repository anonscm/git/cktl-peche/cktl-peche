package org.cocktail.peche;

import java.lang.reflect.InvocationTargetException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlacces.server.handler.JarResourceRequestHandler;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxApplication;
import org.cocktail.fwkcktldroitsutils.common.metier.EOCompte;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;
import org.cocktail.fwkcktlwebapp.server.init.NSLegacyBundleMonkeyPatch;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.commun.Login;
import org.cocktail.peche.components.commun.Wrapper;
import org.cocktail.peche.components.enseignants.services.RunnableHeuresServices;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.serveur.Version;
import org.cocktail.peche.serveur.VersionMe;

import com.google.inject.Module;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOGeneralAdaptorException;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation._NSUtilities;
import com.woinject.WOInject;

import er.extensions.appserver.ERXMessageEncoding;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXProperties;

/**
 * Cette classe est le point d'entrée de l'application Peche.
 * @author yannick
 */
public class Application extends CocktailAjaxApplication {

	private static boolean isModeDebug;

	private static final String CONFIG_FILE_NAME = VersionMe.APPLICATIONINTERNALNAME + ".config";

	private static final String CONFIG_TABLE_NAME = "FwkCktlWebApp_GrhumParametres";

	private static final String[] CONFIG_MANDATORY_KEYS = {};

	private static final String[] CONFIG_OPTIONAL_KEYS = {};

	private static final String MAIN_MODEL_NAME = VersionMe.APPLICATIONINTERNALNAME;

	private int ajaxCktlFilterTimeout;
	private static final int VALEUR_DEFAUT_AJAX_FILTER_TIMEOUT = 1000;


	/**
	 * Fonction "main".
	 * @param argv les arguments de la ligne de commande.
	 */
	public static void main(String[] argv) {
		NSLegacyBundleMonkeyPatch.apply();
		WOInject.init("org.cocktail.peche.Application", argv);
	}

	//permet d'avoir le détail des requetes
	//static {
	//	SqlLogger.setupDelegate();
	//}

	private Version appVersion;

	/**
	 * Constructeur par défaut.
	 */
	public Application() {
		super();

		// Force l'utilisation des classes Peche
		fixClassLoading();

		setAllowsConcurrentRequestHandling(false);
		setDefaultRequestHandler(requestHandlerForKey(directActionRequestHandlerKey()));
		setPageRefreshOnBacktrackEnabled(true);
		WOMessage.setDefaultEncoding("UTF-8");
		WOMessage.setDefaultURLEncoding("UTF-8");
		ERXMessageEncoding.setDefaultEncoding("UTF8");
		ERXMessageEncoding.setDefaultEncodingForAllLanguages("UTF8");

		setAjaxCktlFilterTimeout(calculerCktlFilterTimeout());

		/* Remplacement du requestHandler pour les ressources statiques */
		registerRequestHandler(new JarResourceRequestHandler(), "_wr_");
	}

	@Override
	protected Module[] modules() {
		getModuleRegister().addModule(new PecheModule());
		return super.modules();
	}

	@Override
	public void appendToResponse(WOResponse aResponse, WOContext aContext) {
		super.appendToResponse(aResponse, aContext);
		//permet d'avoir le détail des requetes
		//System.err.println(">>> NB SQL : " + SqlLogger.sqlCount());
	}

	@Override
	public WOResponse handleException(Exception anException, WOContext context) {
		CktlLog.log(anException.getMessage());
		WOResponse response = null;
		if (context != null && context.hasSession()) {
			Session session = (Session) context.session();
			sendMessageErreur(anException, context, session);
			response = createResponseInContext(context);
			NSMutableDictionary formValues = new NSMutableDictionary();
			formValues.setObjectForKey(session.sessionID(), "wosid");
			String applicationExceptionUrl = context.directActionURLForActionNamed("applicationException", formValues, context.request().isSecure(), true);
			response.appendContentString("<script>document.location.href='" + applicationExceptionUrl + "';</script>");
			cleanInvalidEOFState(anException, context);
			return response;
		} else {
			return super.handleException(anException, context);
		}
	}

	private void sendMessageErreur(Exception anException, WOContext context, Session session) {
		try {
			CktlMailBus cmb = session.mailBus();
			String smtpServeur = config().stringForKey("GRHUM_HOST_MAIL");
			String destinataires = config().stringForKey("APP_ADMIN_MAIL");

			String expediteur = "peche@asso-cocktail.fr";

			String contenu = createMessageErreur(anException, context, session);
			session.setGeneralErrorMessage(contenu);

			if (cmb != null && smtpServeur != null && smtpServeur.equals("") == false && destinataires != null && destinataires.equals("") == false) {
				String objet = "[PECHE]:Exception:[";
				objet += VersionMe.txtAppliVersion() + "]";
				boolean retour = false;
				if (isModeDebug) {
					CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! MODE DEVELOPPEMENT : pas de mail !!!!!!!!!!!!!!!!");
					retour = false;
				} else {
					retour = cmb.sendMail(expediteur, destinataires, null, objet, contenu);
				}
				if (!retour) {
					CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
					CktlLog.log("\nMail:\n\n" + contenu);

				}
			} else {
				CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! IMPOSSIBLE d'ENVOYER le mail d'exception !!!!!!!!!!!!!!!!");
				CktlLog.log("Veuillez verifier que les parametres HOST_MAIL et ADMIN_MAIL sont bien renseignes");
				CktlLog.log("HOST_MAIL = " + smtpServeur);
				CktlLog.log("ADMIN_MAIL = " + destinataires);
				CktlLog.log("cmb = " + cmb);
				CktlLog.log("\n\n\n");
			}
		} catch (Exception e) {
			CktlLog.log("\n\n\n");
			CktlLog.log("!!!!!!!!!!!!!!!!!!!!!!!! Exception durant le traitement d'une autre exception !!!!!!!!!!!!!!!!");
			CktlLog.log("Message Exception dans exception: " + e.getMessage());
			CktlLog.log("Stack Exception dans exception: " + e.getStackTrace());
			super.handleException(e, context);
			CktlLog.log("\n");
			CktlLog.log("Message Exception originale: " + anException.getMessage());
			CktlLog.log("Stack Exception dans exception: " + anException.getStackTrace());
		}

	}

	private void cleanInvalidEOFState(Exception e, WOContext ctx) {
		if (e instanceof IllegalStateException || e instanceof EOGeneralAdaptorException) {
			ctx.session().defaultEditingContext().invalidateAllObjects();
		}
	}

	private String createMessageErreur(Exception anException, WOContext context, Session session) {
		String contenu;
		// Si c'est une erreur de config, on affiche pas tout le tsoin tsoin,
		// juste une info claire
		if (anException instanceof NSForwardException) {
			Throwable cause = ((NSForwardException) anException).originalException();
			contenu = cause != null ? cause.getLocalizedMessage() : null;
		} else {
			NSDictionary extraInfo = extraInformationForExceptionInContext(anException, context);
			contenu = "Date : " + DateCtrl.dateToString(DateCtrl.now(), "%d/%m/%Y %H:%M") + "\n";
			contenu += "OS: " + System.getProperty("os.name") + "\n";
			contenu += "Java vm version: " + System.getProperty("java.vm.version") + "\n";
			contenu += "WO version: " + ERXProperties.webObjectsVersion() + "\n\n";
			contenu += "User agent: " + context.request().headerForKey("user-agent") + "\n\n";
			
			if (session != null && session.getApplicationUser() != null) {
				contenu += "Utilisateur(Numero individu): " + session.getApplicationUser().getPersonne().getNomAndPrenom() + "("
						+ session.getApplicationUser().getNoIndividu() + ")" + "\n";
			}
			contenu += "\n\nException : " + "\n";
			if (anException instanceof InvocationTargetException) {
				contenu += getMessage(anException, extraInfo) + "\n";
				anException = (Exception) anException.getCause();
			}
			contenu += getMessage(anException, extraInfo) + "\n";
			contenu += "\n\n";
		}
		return contenu;
	}

	protected String getMessage(Throwable e, NSDictionary extraInfo) {
		String message = "";
		if (e != null) {
			message = stackTraceToString(e, false) + "\n\n";
			message += "Info extra :\n";
			if (extraInfo != null) {
				message += NSPropertyListSerialization.stringFromPropertyList(extraInfo) + "\n\n";
			}
		}
		return message;
	}

	/**
	 * permet de recuperer la trace d'une exception au format string message + trace
	 * @param e
	 * @return
	 */
	public static String stackTraceToString(Throwable e, boolean useHtml) {
		String tagCR = "\n";
		if (useHtml) {
			tagCR = "<br>";
		}
		String stackStr = e + tagCR + tagCR;
		StackTraceElement[] stack = e.getStackTrace();
		for (int i = 0; i < stack.length; i++) {
			stackStr += (stack[i]).toString() + tagCR;
		}
		return stackStr;
	}

	@Override
	public WOResponse handleSessionRestorationErrorInContext(WOContext context) {
		WOResponse response;
		response = createResponseInContext(context);
		String sessionExpiredUrl = context.directActionURLForActionNamed("sessionExpired", null);
		response.appendContentString("<script>document.location.href='" + sessionExpiredUrl + "';</script>");

		return response;
	}

	@Override
	public void initApplication() {
		System.out.println("Lancement de l'application serveur " + this.name() + " ...");
		super.initApplication();

		// Afficher les infos de connexion des modeles de donnees
		rawLogModelInfos();

		// Verifier la coherence des dictionnaires de connexion des modeles de
		// donnees
		/* boolean isInitialisationErreur = ! */checkModel();

		Application.isModeDebug = (Application.isDevelopmentModeSafe() || config().booleanForKey("MODE_DEBUG"));

		//Recuperer un grhum_createur
		String cptLogin = EOGrhumParametres.parametrePourCle(dataBus().editingContext(), EOGrhumParametres.PARAM_GRHUM_CREATEUR);
		if (cptLogin != null) {
			EOCompte cpt = EOCompte.compteForLogin(dataBus().editingContext(), cptLogin);
			if (cpt != null) {				
				Integer createurId = EOPersonne.fetchByKeyValue(dataBus().editingContext(), EOPersonne.PERS_ID_KEY, cpt.persId()).persId();
				ScheduledExecutorService execute = Executors.newSingleThreadScheduledExecutor();

				int fetchTimestampLag = application().config().intForKey("org.cocktail.peche.fetch.timestamp.lag");

				if (fetchTimestampLag > 0) {
					CktlLog.log("org.cocktail.peche.fetch.timestamp.lag:" + fetchTimestampLag);
					EOEditingContext.setDefaultFetchTimestampLag(fetchTimestampLag);			
				}

				int initialDelay = application().config().intForKey("org.cocktail.peche.async.heures.initial.delay");
				int period = application().config().intForKey("org.cocktail.peche.async.heures.period");

				if ((initialDelay > 0) && (period > 0)) {
					CktlLog.log("org.cocktail.peche.async.heures.initial.delay:" + initialDelay);
					CktlLog.log("org.cocktail.peche.async.heures.period:" + period);
					execute.scheduleAtFixedRate(
							new RunnableHeuresServices(createurId,
									new PecheParametres(dataBus().editingContext()).getAnneeUniversitaireEnCours()),
									initialDelay,
									period,
									TimeUnit.MILLISECONDS);
				}
			} else {
				CktlLog.log("ATTENTION : pas de lancement asynchrone possible car PARAM_GRHUM_CREATEUR invalide");
			}
		}
	}

	public static boolean isModeDebug() {
		return Application.isModeDebug;
	}

	@Override
	public void startRunning() {
		if (!checkPecheParams()) {
			System.exit(-1);
		}
		
		super.startRunning();
	}

	@Override
	public String configFileName() {
		return Application.CONFIG_FILE_NAME;
	}

	@Override
	public String configTableName() {
		return Application.CONFIG_TABLE_NAME;
	}

	@Override
	public String[] configMandatoryKeys() {
		return Application.CONFIG_MANDATORY_KEYS;
	}

	@Override
	public String[] configOptionalKeys() {
		return Application.CONFIG_OPTIONAL_KEYS;
	}

	@Override
	public String copyright() {
		return appVersion().copyright();
	}

	@Override
	public A_CktlVersion appCktlVersion() {
		return appVersion();
	}

	/**
	 * Descripteur de version de l'application.
	 * @return une instance de Version.
	 */
	public Version appVersion() {
		if (this.appVersion == null) {
			this.appVersion = new Version();
		}
		return this.appVersion;
	}

	@Override
	public String mainModelName() {
		return Application.MAIN_MODEL_NAME;
	}

	@Override
	public boolean acceptLoginName(String loginName) {
		return true;
	}

	public int getAjaxCktlFilterTimeout(){
		return ajaxCktlFilterTimeout;
	}

	public void setAjaxCktlFilterTimeout(int ajaxCktlFilterTimeout) {
		this.ajaxCktlFilterTimeout = ajaxCktlFilterTimeout;
	}

	/**
	 * @return Ids des connexion base de données
	 */
	public static String serverBDId() {
		NSMutableArray<String> serverDBIds = new NSMutableArray<String>();
		final NSMutableDictionary mdlsDico = EOModelCtrl.getModelsDico();
		final Enumeration mdls = mdlsDico.keyEnumerator();
		while (mdls.hasMoreElements()) {
			final String mdlName = (String) mdls.nextElement();
			String serverDBId = EOModelCtrl.bdConnexionServerId((EOModel) mdlsDico.objectForKey(mdlName));
			if (!serverDBIds.containsObject(serverDBId) && serverDBId != null) {
				serverDBIds.addObject(serverDBId);
			}
		}
		return serverDBIds.componentsJoinedByString(",");
	}

	private void fixClassLoading() {
		_NSUtilities.setClassForName(Login.class, Login.class.getSimpleName());
		_NSUtilities.setClassForName(Wrapper.class, Wrapper.class.getSimpleName());
	}

	/**
	 * 
	 * @return
	 */
	private int calculerCktlFilterTimeout() {
		if (config().intForKey("AJAX_CKTL_FILTER_TIMEOUT") != -1) {
			return config().intForKey("AJAX_CKTL_FILTER_TIMEOUT");
		} else {
			return VALEUR_DEFAUT_AJAX_FILTER_TIMEOUT;
		}
	}

	/**
	 * @return le chemin du dossier des jasper en local
	 */
	public static String pathJasperLocal() {
		return application().config().stringForKey("org.cocktail.peche.reports.local.location");
	}
	
	private final static String SB_SEPARATOR = ", ";
	
	public boolean checkPecheParams() {
		boolean hasMandatoryMissing = false;
		StringBuffer sbMandatoryMissing = new StringBuffer();
		Hashtable<String, String> hMandatoryFound = new Hashtable<String, String>();
		
		StringBuffer sb = new StringBuffer("Controle de la présence des valeurs dans " + pecheParametresTableName() +"\n" 
		+ "-------------------------------------------------------\n\n");
		
		if (listePecheParametre() != null && listePecheParametre().length > 0) {
			Integer anneeCourante = new PecheParametres(dataBus().editingContext()).getAnneeUniversitaireEnCours();
			for (String key : listePecheParametre()) {
				
				String keyToSearch = EOPecheParametre.getParamKeyAnnualise(key, anneeCourante);
				
				String value = valuePecheParametreEnBase(keyToSearch);
				if (value == null) {
					sbMandatoryMissing.append(keyToSearch);
					sbMandatoryMissing.append(SB_SEPARATOR);
					hasMandatoryMissing = true;
				} else {
					hMandatoryFound.put(keyToSearch, value);
				}
			}
			// enlever le dernier separateur et ajouter le message d'erreur
			if (sbMandatoryMissing.length() > 0) {
				int sbSize = sbMandatoryMissing.length();
				sbMandatoryMissing.replace(sbSize - SB_SEPARATOR.length(), sbSize, "");
				sbMandatoryMissing.insert(0, "  > Valeurs de "+ pecheParametresTableName() + " absents : ");
				sbMandatoryMissing.append(" - ERREUR");
			} else {
				sbMandatoryMissing.insert(0, "  > Toutes les valeurs de "+  pecheParametresTableName() + " sont présentes - OK");
			}		
			sb.append(sbMandatoryMissing).append("\n");
			sb.append(StringCtrl.getFormatted(hMandatoryFound));
		}
		CktlLog.rawLog(sb.toString());
		return !hasMandatoryMissing;
	}
	
	public String pecheParametresTableName() {
		return EOPecheParametre.ENTITY_NAME;
	}
	
	public String[] listePecheParametre() {
		return new String[] {
				EOPecheParametre.getParametreProfilAdministrateur(),
				EOPecheParametre.getParametreProfilEnseignant(),
				EOPecheParametre.getParametreTypePeriode(),
				EOPecheParametre.getParametreHCompMethodeDefaut(),
				EOPecheParametre.parametreFicheVoeuxExistence,
				EOPecheParametre.parametreReportBorneInferieur,
				EOPecheParametre.parametreReportBorneSuperieur,
				EOPecheParametre.parametreVacatairePrevisionnelExistence};
				
	}

	public String valuePecheParametreEnBase(String keyToSearch) {
		
		EOPecheParametre record = EOPecheParametre.fetchEOPecheParametre(dataBus().editingContext(), EOPecheParametre.KEY_KEY, keyToSearch);
		
		if (record != null) {
			return (String) record.value();
		}
		
		return null;
	}
}
