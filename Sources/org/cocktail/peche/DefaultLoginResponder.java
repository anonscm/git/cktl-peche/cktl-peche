package org.cocktail.peche;

import org.cocktail.fwkcktlwebapp.server.components.CktlLogin;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;

import com.webobjects.appserver.WOComponent;
import com.webobjects.foundation.NSDictionary;

/**
 * Gestion du processus d'authentification.
 * 
 * @author yannick
 * 
 */

public class DefaultLoginResponder implements CktlLoginResponder {

	/**
	 * 
	 */
	private final DirectAction directAction;
	private NSDictionary<String, String> actionParams;

	/**
	 * Constructeur.
	 * 
	 * @param actionParams
	 *            les paramètres de l'action.
	 * @param directAction
	 *            TODO
	 */
	public DefaultLoginResponder(DirectAction directAction,
			NSDictionary<String, String> actionParams) {
		this.directAction = directAction;
		this.actionParams = actionParams;
	}

	public NSDictionary<String, String> getActionParams() {
		return actionParams;
	}

	/**
	 * Méthode appelée par le framework lorsque l'utilisateur est authentifé
	 * correctement.
	 * 
	 * @param loginComponent
	 *            le composant contentant les informations de connexion.
	 * @return la page suivante à afficher.
	 */
	public WOComponent loginAccepted(CktlLogin loginComponent) {
		WOComponent nextPage = null;
		nextPage = DirectAction.cktlApp.pageWithName(this.directAction
				.getPagePrincipale().getName(), this.directAction.context());
		return nextPage;
	}

	/**
	 * 
	 * @param loginName le compte de l'utilisateur.
	 * @return true si l'utilisateur peut se connecter a l'application, false dans le cas contraire.
	 */
	public boolean acceptLoginName(String loginName) {
		return DirectAction.cktlApp.acceptLoginName(loginName);
	}

	/**
	 * @return true si l'on peut se connecter à l'application sans mot de passe, false dans le cas contraire.
	 */
	public boolean acceptEmptyPassword() {
		return DirectAction.cktlApp.config().booleanForKey(
				"ACCEPT_EMPTY_PASSWORD");
	}

	public String getRootPassword() {
		return DirectAction.cktlApp.getRootPassword();
	}
}