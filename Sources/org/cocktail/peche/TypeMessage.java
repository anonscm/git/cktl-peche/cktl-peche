package org.cocktail.peche;

/**
 * 
 * @author juliencallewaert
 *
 */
public enum TypeMessage {

	/** Titre du message "Attention". */
	ATTENTION ("Attention"),
	/** Titre du message "Erreur". */
	ERREUR ("Erreur"),
	/** Titre du message "Information". */
	INFORMATION ("Information");
	
	
	private String nomTypeMessage;
	
	
	TypeMessage(String typeMessage) {
		nomTypeMessage = typeMessage;
	}


	public String getNomTypeMessage() {
		return nomTypeMessage;
	}
	
	
	
}
