package org.cocktail.peche.commun;

/**
 * Les paramètres utilisateur de l'application Peche.
 * 
 * @author Chama LAATIK
 */
public interface IPecheParametres {

    /**
     * @return L'année universitaire en cours.
     */
    Integer getAnneeUniversitaireEnCours();
    
    String getAnneeUniversitaireEnCoursll();
    
	Integer getAnneeUniversitaire();
	
    /**
     * est-on en année d'exercice calquée sur l'année civile ?
     * false si universitaire, true si civile 
     * @return 
     */
    boolean isFormatAnneeExerciceAnneeCivile();
    
    
    /**
     * 
     * @return "U" si année universitaire, "C" si année civile
     */
    String getCodeFormatAnnee();
    
    void setAnnee(Integer annee);

}