package org.cocktail.peche.commun;


import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlscolpeda.serveur.FwkCktlScolPedaParamManager;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.entity.EOPeriode;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;


/**
 * Les paramètres utilisateur de l'application Peche.
 * 
 * @author Pascal
 */
public class PecheParametres implements IPecheParametres {

	
	private EOEditingContext editingContext;
	
	private Integer annee;
	private boolean anneeDejaRecherchee = false;
	private boolean formatAnneeExerciceAnneeCivile;
	private boolean formatAnneeExerciceAnneeCivileDejaRecherchee = false;
	
	/**
	 * 
	 * @param editingContext edc
	 */
	@Inject
	public PecheParametres(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */	
	public Integer getAnneeUniversitaireEnCours() {
		if (!anneeDejaRecherchee) {
			String anneeAsString = EOGrhumParametres.parametrePourCle(editingContext, EOGrhumParametres.PARAM_GRHUM_ANNEE_UNIVERSITAIRE);
			if (!StringCtrl.isEmpty(anneeAsString)) {
				annee = Integer.valueOf(anneeAsString);
				anneeDejaRecherchee = true;
			}
		}		
		return annee;
	}
	
	public String getAnneeUniversitaireEnCoursll() {
		Integer anneeUniv = getAnneeUniversitaireEnCours();

		if (isFormatAnneeExerciceAnneeCivile()) { 
			return anneeUniv.toString();
		} else {
			return anneeUniv + "/" + (anneeUniv + 1);
		}
	}
	

	public Integer getAnneeUniversitaire() {
			String anneeAsString = EOGrhumParametres.parametrePourCle(editingContext, EOGrhumParametres.PARAM_GRHUM_ANNEE_UNIVERSITAIRE);
			if (!StringCtrl.isEmpty(anneeAsString)) {
				return (Integer.valueOf(anneeAsString));
			} 
			return Integer.valueOf("2013");
	}
	
	
	public void setAnnee(Integer anneeUniversitaire) {
		this.annee = anneeUniversitaire;
		this.anneeDejaRecherchee = true;
	}

	/**
	 * {@inheritDoc}
	 */	
	public boolean isFormatAnneeExerciceAnneeCivile() {
		if (!formatAnneeExerciceAnneeCivileDejaRecherchee) {
			String parametreFormatAnnee = EOGrhumParametres.parametrePourCle(editingContext, FwkCktlScolPedaParamManager.FORMAT_ANNEE_EXERCICE_ANNEE_CIVILE);
			if (!StringCtrl.isEmpty(parametreFormatAnnee)) {
				formatAnneeExerciceAnneeCivile = StringCtrl.toBool(parametreFormatAnnee);
			} else {
				formatAnneeExerciceAnneeCivile = false;
			}
			formatAnneeExerciceAnneeCivileDejaRecherchee = true;
		}
		return formatAnneeExerciceAnneeCivile;
	}
	
	/**
	 * {@inheritDoc}
	 */	
	public String getCodeFormatAnnee() {
		if (isFormatAnneeExerciceAnneeCivile()) {
			return EOPeriode.TYPE_ANNEE_CIVILE;
		} else {
			return EOPeriode.TYPE_ANNEE_UNIVERSITAIRE;
		}		
	}
	
}
