package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.components.commun.Constante;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
/**
 * 
 *
 */
public class EORepartService extends _EORepartService {
	/**
	 * 
	 */
	private static final long serialVersionUID = -670082085282819090L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartService.class);
	public static int CommentairesMax = 1000;
	
	/**
	 * Crée et initialise une répartition service.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return une répartition service
	 */
	public static EORepartService creerEtInitialiser(EOEditingContext edc) {
		EORepartService repartService = new EORepartService();
		edc.insertObject(repartService);

		return repartService;
	}
	

	/**
	 * 
	 * @param edc contexte d'édition
	 * @param service service en cours
	 * @param structure structure du service
	 * @param personne 
	 * @param codeCircuit Utiliser Constante.CIRCUIT_VACATAIRE_A_DETERMINER si l'on veut faire en fonction du paramétre qui détermine 
	 * si les fiches prévisionnelles sont utilisées dans l'établissement
	 * @param annee année en cours 
	 * @return repartService
	 */
	public static EORepartService creerNouvelRepartService(EOEditingContext edc, EOService service, IStructure structure, EOPersonne personne, String codeCircuit, Integer annee) {
		
		EORepartService repartService = creerEtInitialiser(edc);
		
		repartService.setToService(service);
		repartService.setToStructure(structure);
		
		repartService.majDonnesAuditCreation(personne);
		
		if (Constante.CIRCUIT_VACATAIRE_A_DETERMINER.equals(codeCircuit)) {
			codeCircuit = getCodeCircuitVacataire(edc, annee);
		}
		
		if (codeCircuit != null) {
			attachNewDemande(edc, personne.persId(), codeCircuit, repartService);
		}
				
		return repartService;
	}

	/**
	 * 
	 * @param edc contexte d'édition
	 * @param service service en cours
	 * @param structure structure du service
	 * @param personne 
	 * @return repartService
	 */
	public static EORepartService creerNouvelRepartService(EOEditingContext edc, EOService service, IStructure structure, EOPersonne personne) {
		
		return creerNouvelRepartService(edc, service, structure, personne, null, null);
	}
	
	/**
	 * 
	 * @param edc contexte d'édition
	 * @param service
	 * @param structure
	 * @param codeCircuit
	 * @param personne
	 * @param annee
	 * @return EORepartService existant ou un nouveau
	 */
	public static EORepartService getOrCreateRepartService(EOEditingContext edc, EOService service, EOStructure structure, String codeCircuit, 
															EOPersonne personne, Integer annee) {
		EOQualifier qualifier = ERXQ.and(ERXQ.equals(EORepartService.TO_STRUCTURE_KEY, structure),
										ERXQ.equals(EORepartService.TO_SERVICE_KEY, service));
		
		EORepartService repartService = EORepartService.fetchEORepartService(edc, qualifier);
		
		if (repartService == null) {
			repartService = EORepartService.creerNouvelRepartService(edc, service, structure, personne, codeCircuit, annee);
		}
		
		return repartService;
	}
	

	public static void attachNewDemande(EOEditingContext edc, Integer persId, String codeCircuit, EORepartService repartService) {
		EODemande demande = EODemande.creerDemande(edc, Constante.APP_PECHE, codeCircuit, persId);
		repartService.setToDemande(demande);
	}
	
	/**
	 * 
	 * @param edc editing context
	 * @return code Circuit
	 */
	public static String getCodeCircuitVacataire(EOEditingContext edc, Integer annee) {
		// si l'on veut créer une fiche pour les vacataires, le paramétre récupéré permettra de savoir si ce sera prévisionnel ou non
		boolean isFicheVacatairePrevisionnelUtilisation = EOPecheParametre.isFicheVacatairePrevisionnelUtilisation(edc, annee);
		String codeCircuit;
		if (isFicheVacatairePrevisionnelUtilisation) {
			codeCircuit = CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit();
		} else {
			codeCircuit = CircuitPeche.PECHE_VACATAIRE.getCodeCircuit();
		}
		return codeCircuit;
	}
	
	
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);

		if (!StringCtrl.isEmpty(commentaire())) {
			setDateCommentaire(now);
		}
	}
	
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			NSTimestamp now = new NSTimestamp();
			setDateModification(now);
			setPersonneModificationRelationship(personne);
			
			if (hasKeyChangedFromCommittedSnapshot(EORepartService.COMMENTAIRE_KEY)) {
				setDateCommentaire(now);
			}
		}
	}
	
	/**
	 * 
	 * @param edc editingContext
	 */
	public void deleteAvecDemande(EOEditingContext edc) {
		this.toDemande().supprimerAvecHistorique();
		this.delete();
		edc.saveChanges();
	}
	
	public boolean isIncidentPresent() {
		return Constante.OUI.equals(temIncident());
	}
	
	public void setToStructure(IStructure value) {
	    super.setToStructure((EOStructure) value);
	}
	
	/**
	 * @param unService : le service
	 * @param uneStructure : la structure demandée
	 * @return le repartService qui concerne le service et la structure demandée
	 */
	public static EORepartService getRepartService(EOService unService, IStructure uneStructure) {
		EOQualifier qualifier = ERXQ.and(
									ERXQ.equals(EORepartService.TO_SERVICE_KEY, unService),
									ERXQ.equals(EORepartService.TO_STRUCTURE_KEY, uneStructure));

		return EORepartService.fetchEORepartService(unService.editingContext(), qualifier);
	}
	
}
