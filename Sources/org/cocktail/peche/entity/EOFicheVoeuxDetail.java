package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.entity.interfaces.HasComposantAP;
import org.cocktail.peche.entity.interfaces.IFicheDetail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe décrit le détail de la fiche de voeux de chaque enseignant
 *
 * @author Chama LAATIK
 */
public class EOFicheVoeuxDetail extends _EOFicheVoeuxDetail implements HasComposantAP, IFicheDetail {

	private static final long serialVersionUID = 7621206130322428841L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOFicheVoeuxDetail.class);

	/**
	 * Crée et initialise le détail de la fiche de voeux.
	 *
	 * @param edc : contexte d'édition.
	 * @return un élement de la fiche de voeux.
	 */
	public static EOFicheVoeuxDetail creerEtInitialiser(EOEditingContext edc) {
		EOFicheVoeuxDetail ficheVoeuxDetail = new EOFicheVoeuxDetail();
		edc.insertObject(ficheVoeuxDetail);

		return ficheVoeuxDetail;
	}

	@Override
	public void setComposantAP(EOAP value) {
		super.setComposantAP(value);
		super.setEtablissementExterne(null);
		super.setEnseignementExterne(null);
	}

	@Override
	public void setEtablissementExterne(String value) {
		super.setEtablissementExterne(value);
		super.setComposantAP(null);
	}

	/**
	 * Retourne vrai si au moins une des 2 entités est non nulles ==> sert lors
	 * de l'insertion d'un voeux
	 *
	 * @param composantAP : AP présent dans l'offre de formation
	 * @param etablissementExterne : Etablissment externe où l'enseignant interviendra
	 * @return vrai si au plus une des 2 entités est non nulles
	 */
	public boolean verifierEntiteNonNull(EOAP composantAP, String etablissementExterne) {
		return ((composantAP != null) || (etablissementExterne != null));
	}

	/**
	 * Mise à jour des données d'audit lorsqu'on est en création d'un enregistrement.
	 * 
	 * @param now Une date/heure
	 * @param personne La personne qui crée l'enregistrement
	 */
	public void majDonneesAuditCreation(NSTimestamp now, EOPersonne personne) {
		setPersonneCreation(personne);
		setDateCreation(now);
		setPersonneModification(personne);
		setDateModification(now);
		if (!StringCtrl.isEmpty(commentaire())) {
			setDateCommentaire(now);
		}
	}
	
	/**
	 * Mise à jour des données d'audit lorsqu'on est en modification d'un enregistrement.
	 * 
	 * @param now Une date/heure
	 * @param personne La personne qui modifie l'enregistrement
	 */
	public void majDonneesAuditModification(NSTimestamp now, EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setPersonneModification(personne);
			setDateModification(now);
			
			if (hasKeyChangedFromCommittedSnapshot(EOFicheVoeuxDetail.COMMENTAIRE_KEY)) {
				setDateCommentaire(now);
			}
		}
	}
	
	/**
	 * @return Le tri par défaut de la liste suivant le nom des enseignants génériques ou pas
	 */
	public static ERXSortOrderings getTriParDefaut() {
		return EOFicheVoeuxDetail.COMPOSANT_AP.dot(EOAP.TYPE_AP).dot(EOTypeAP.CODE_KEY).asc()
			.then(EOFicheVoeuxDetail.FICHE_VOEUX.dot(EOFicheVoeux.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE_KEY).asc());
	}

	/**
	 * @return le libellé d'affichage pour l'enseignement.
	 */
	public String affichageEnseignement() {
		if (composantAP() != null) {
			return ComposantAffichageHelper.getInstance().affichageEnseignementParent(composantAP());
		} else {
			return etablissementExterne();
		}
	}
	/**
	 * @return le libellé d'affichage pour le type d'AP.
	 */
	public String affichageType() {
		return ComposantAffichageHelper.getInstance().affichageType(composantAP());
	}
	
}
