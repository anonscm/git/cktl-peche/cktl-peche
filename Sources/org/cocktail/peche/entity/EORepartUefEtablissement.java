package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.joda.time.DateTime;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Cette classe représente la répartition entre les UE flottantes et un établissement externe où se 
 * déroulera cet enseignement
 * 
 * @author Chama LAATIK
 */
public class EORepartUefEtablissement extends _EORepartUefEtablissement {
	
	private static final long serialVersionUID = 8898948523187533598L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartUefEtablissement.class);
	
	/**
	 * Crée et initialise une répartition entre l'UE flottante et un établissement.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return une répartition.
	 */
	public static EORepartUefEtablissement creerEtInitialiser(EOEditingContext edc) {
		EORepartUefEtablissement repartition = new EORepartUefEtablissement();
		edc.insertObject(repartition);

		return repartition;
	}
	
	
	/**
	 * Permet de récupérer la répartition d'une UE en particulier
	 * @param edc : editing context
	 * @param composantUE : UE pour laquelle on recherche une répartition
	 * @return une répartition
	 */
	public static EORepartUefEtablissement rechercherEtablissementPourUE(EOEditingContext edc, IComposant composantUE) {
		EOQualifier qualifier = ERXQ.equals(COMPOSANT_UE_KEY, composantUE);
		
		return fetchEORepartUefEtablissement(edc, qualifier);
	}
	
	
	public EORepartUefEtablissement dupliquer(EOEditingContext edc, IComposant composantUE, EOPersonne personneConnecte) {
		EORepartUefEtablissement repartUefEtablissement = creerEtInitialiser(edc);
		repartUefEtablissement.setComposantUE((EOUE) composantUE);
		repartUefEtablissement.setDateCreation(new DateTime());
		repartUefEtablissement.setPersonneCreation(personneConnecte);
		repartUefEtablissement.setDateModification(new DateTime());
		repartUefEtablissement.setPersonneModification(personneConnecte);
		repartUefEtablissement.setEtablissementExterne(this.etablissementExterne());
		return repartUefEtablissement;
		
	}
	
	
}
