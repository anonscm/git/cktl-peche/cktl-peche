// DO NOT EDIT.  Make changes to EOFicheVoeux.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOFicheVoeux extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOFicheVoeux";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_COMMENTAIRE = new ERXKey<NSTimestamp>("dateCommentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> HEURES_SERVICE_DU = new ERXKey<java.math.BigDecimal>("heuresServiceDu");
  public static final ERXKey<java.math.BigDecimal> HEURES_SOUHAITEES = new ERXKey<java.math.BigDecimal>("heuresSouhaitees");
  public static final ERXKey<String> SUIVI = new ERXKey<String>("suivi");
  public static final ERXKey<String> TEMOIN_VALIDE = new ERXKey<String>("temoinValide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOActuelEnseignant> ENSEIGNANT = new ERXKey<org.cocktail.peche.entity.EOActuelEnseignant>("enseignant");
  public static final ERXKey<org.cocktail.peche.entity.EOFicheVoeuxDetail> LISTE_FICHE_VOEUX_DETAILS = new ERXKey<org.cocktail.peche.entity.EOFicheVoeuxDetail>("listeFicheVoeuxDetails");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemande");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_COMMENTAIRE_KEY = DATE_COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String HEURES_SERVICE_DU_KEY = HEURES_SERVICE_DU.key();
  public static final String HEURES_SOUHAITEES_KEY = HEURES_SOUHAITEES.key();
  public static final String SUIVI_KEY = SUIVI.key();
  public static final String TEMOIN_VALIDE_KEY = TEMOIN_VALIDE.key();
  // Relationships
  public static final String ENSEIGNANT_KEY = ENSEIGNANT.key();
  public static final String LISTE_FICHE_VOEUX_DETAILS_KEY = LISTE_FICHE_VOEUX_DETAILS.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_DEMANDE_KEY = TO_DEMANDE.key();

  private static Logger LOG = Logger.getLogger(_EOFicheVoeux.class);

  public EOFicheVoeux localInstanceIn(EOEditingContext editingContext) {
    EOFicheVoeux localInstance = (EOFicheVoeux)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOFicheVoeux.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.ANNEE_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOFicheVoeux.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCommentaire() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeux.DATE_COMMENTAIRE_KEY);
  }

  public void setDateCommentaire(NSTimestamp value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating dateCommentaire from " + dateCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.DATE_COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeux.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeux.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal heuresServiceDu() {
    return (java.math.BigDecimal) storedValueForKey(_EOFicheVoeux.HEURES_SERVICE_DU_KEY);
  }

  public void setHeuresServiceDu(java.math.BigDecimal value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating heuresServiceDu from " + heuresServiceDu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.HEURES_SERVICE_DU_KEY);
  }

  public java.math.BigDecimal heuresSouhaitees() {
    return (java.math.BigDecimal) storedValueForKey(_EOFicheVoeux.HEURES_SOUHAITEES_KEY);
  }

  public void setHeuresSouhaitees(java.math.BigDecimal value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating heuresSouhaitees from " + heuresSouhaitees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.HEURES_SOUHAITEES_KEY);
  }

  public String suivi() {
    return (String) storedValueForKey(_EOFicheVoeux.SUIVI_KEY);
  }

  public void setSuivi(String value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating suivi from " + suivi() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.SUIVI_KEY);
  }

  public String temoinValide() {
    return (String) storedValueForKey(_EOFicheVoeux.TEMOIN_VALIDE_KEY);
  }

  public void setTemoinValide(String value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
    	_EOFicheVoeux.LOG.debug( "updating temoinValide from " + temoinValide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeux.TEMOIN_VALIDE_KEY);
  }

  public org.cocktail.peche.entity.EOActuelEnseignant enseignant() {
    return (org.cocktail.peche.entity.EOActuelEnseignant)storedValueForKey(_EOFicheVoeux.ENSEIGNANT_KEY);
  }
  
  public void setEnseignant(org.cocktail.peche.entity.EOActuelEnseignant value) {
    takeStoredValueForKey(value, _EOFicheVoeux.ENSEIGNANT_KEY);
  }

  public void setEnseignantRelationship(org.cocktail.peche.entity.EOActuelEnseignant value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("updating enseignant from " + enseignant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setEnseignant(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOActuelEnseignant oldValue = enseignant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeux.ENSEIGNANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeux.ENSEIGNANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFicheVoeux.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFicheVoeux.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeux.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeux.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFicheVoeux.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFicheVoeux.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeux.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeux.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(_EOFicheVoeux.TO_DEMANDE_KEY);
  }
  
  public void setToDemande(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    takeStoredValueForKey(value, _EOFicheVoeux.TO_DEMANDE_KEY);
  }

  public void setToDemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("updating toDemande from " + toDemande() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDemande(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = toDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeux.TO_DEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeux.TO_DEMANDE_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail> listeFicheVoeuxDetails() {
    return (NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail>)storedValueForKey(_EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail> listeFicheVoeuxDetails(EOQualifier qualifier) {
    return listeFicheVoeuxDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail> listeFicheVoeuxDetails(EOQualifier qualifier, boolean fetch) {
    return listeFicheVoeuxDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail> listeFicheVoeuxDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOFicheVoeuxDetail.FICHE_VOEUX_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOFicheVoeuxDetail.fetchEOFicheVoeuxDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeFicheVoeuxDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFicheVoeuxDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeFicheVoeuxDetails(org.cocktail.peche.entity.EOFicheVoeuxDetail object) {
    includeObjectIntoPropertyWithKey(object, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
  }

  public void removeFromListeFicheVoeuxDetails(org.cocktail.peche.entity.EOFicheVoeuxDetail object) {
    excludeObjectFromPropertyWithKey(object, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
  }

  public void addToListeFicheVoeuxDetailsRelationship(org.cocktail.peche.entity.EOFicheVoeuxDetail object) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("adding " + object + " to listeFicheVoeuxDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeFicheVoeuxDetails(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
    }
  }

  public void removeFromListeFicheVoeuxDetailsRelationship(org.cocktail.peche.entity.EOFicheVoeuxDetail object) {
    if (_EOFicheVoeux.LOG.isDebugEnabled()) {
      _EOFicheVoeux.LOG.debug("removing " + object + " from listeFicheVoeuxDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeFicheVoeuxDetails(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
    }
  }

  public org.cocktail.peche.entity.EOFicheVoeuxDetail createListeFicheVoeuxDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOFicheVoeuxDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
    return (org.cocktail.peche.entity.EOFicheVoeuxDetail) eo;
  }

  public void deleteListeFicheVoeuxDetailsRelationship(org.cocktail.peche.entity.EOFicheVoeuxDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOFicheVoeux.LISTE_FICHE_VOEUX_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeFicheVoeuxDetailsRelationships() {
    Enumeration<org.cocktail.peche.entity.EOFicheVoeuxDetail> objects = listeFicheVoeuxDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeFicheVoeuxDetailsRelationship(objects.nextElement());
    }
  }


  public static EOFicheVoeux createEOFicheVoeux(EOEditingContext editingContext, Integer annee
, NSTimestamp dateCreation
, NSTimestamp dateModification
, String temoinValide
, org.cocktail.peche.entity.EOActuelEnseignant enseignant, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EOFicheVoeux eo = (EOFicheVoeux) EOUtilities.createAndInsertInstance(editingContext, _EOFicheVoeux.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setTemoinValide(temoinValide);
    eo.setEnseignantRelationship(enseignant);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOFicheVoeux> fetchSpec() {
    return new ERXFetchSpecification<EOFicheVoeux>(_EOFicheVoeux.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOFicheVoeux> fetchAllEOFicheVoeuxes(EOEditingContext editingContext) {
    return _EOFicheVoeux.fetchAllEOFicheVoeuxes(editingContext, null);
  }

  public static NSArray<EOFicheVoeux> fetchAllEOFicheVoeuxes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFicheVoeux.fetchEOFicheVoeuxes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFicheVoeux> fetchEOFicheVoeuxes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOFicheVoeux> fetchSpec = new ERXFetchSpecification<EOFicheVoeux>(_EOFicheVoeux.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFicheVoeux> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOFicheVoeux fetchEOFicheVoeux(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheVoeux.fetchEOFicheVoeux(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheVoeux fetchEOFicheVoeux(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFicheVoeux> eoObjects = _EOFicheVoeux.fetchEOFicheVoeuxes(editingContext, qualifier, null);
    EOFicheVoeux eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOFicheVoeux that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheVoeux fetchRequiredEOFicheVoeux(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheVoeux.fetchRequiredEOFicheVoeux(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheVoeux fetchRequiredEOFicheVoeux(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFicheVoeux eoObject = _EOFicheVoeux.fetchEOFicheVoeux(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOFicheVoeux that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheVoeux localInstanceIn(EOEditingContext editingContext, EOFicheVoeux eo) {
    EOFicheVoeux localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
