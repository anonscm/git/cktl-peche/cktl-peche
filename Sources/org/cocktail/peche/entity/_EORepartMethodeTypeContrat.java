// DO NOT EDIT.  Make changes to EORepartMethodeTypeContrat.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartMethodeTypeContrat extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartMethodeTypeContrat";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp> METHODE = new ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp>("methode");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("typeContratTravail");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  // Relationships
  public static final String METHODE_KEY = METHODE.key();
  public static final String TYPE_CONTRAT_TRAVAIL_KEY = TYPE_CONTRAT_TRAVAIL.key();

  private static Logger LOG = Logger.getLogger(_EORepartMethodeTypeContrat.class);

  public EORepartMethodeTypeContrat localInstanceIn(EOEditingContext editingContext) {
    EORepartMethodeTypeContrat localInstance = (EORepartMethodeTypeContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeTypeContrat.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartMethodeTypeContrat.LOG.isDebugEnabled()) {
    	_EORepartMethodeTypeContrat.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeTypeContrat.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeTypeContrat.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartMethodeTypeContrat.LOG.isDebugEnabled()) {
    	_EORepartMethodeTypeContrat.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeTypeContrat.DATE_MODIFICATION_KEY);
  }

  public org.cocktail.peche.entity.EOMethodesCalculHComp methode() {
    return (org.cocktail.peche.entity.EOMethodesCalculHComp)storedValueForKey(_EORepartMethodeTypeContrat.METHODE_KEY);
  }
  
  public void setMethode(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    takeStoredValueForKey(value, _EORepartMethodeTypeContrat.METHODE_KEY);
  }

  public void setMethodeRelationship(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    if (_EORepartMethodeTypeContrat.LOG.isDebugEnabled()) {
      _EORepartMethodeTypeContrat.LOG.debug("updating methode from " + methode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setMethode(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOMethodesCalculHComp oldValue = methode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeTypeContrat.METHODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeTypeContrat.METHODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey(_EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL_KEY);
  }
  
  public void setTypeContratTravail(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    takeStoredValueForKey(value, _EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EORepartMethodeTypeContrat.LOG.isDebugEnabled()) {
      _EORepartMethodeTypeContrat.LOG.debug("updating typeContratTravail from " + typeContratTravail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeContratTravail(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  

  public static EORepartMethodeTypeContrat createEORepartMethodeTypeContrat(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, org.cocktail.peche.entity.EOMethodesCalculHComp methode, org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail typeContratTravail) {
    EORepartMethodeTypeContrat eo = (EORepartMethodeTypeContrat) EOUtilities.createAndInsertInstance(editingContext, _EORepartMethodeTypeContrat.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
    eo.setMethodeRelationship(methode);
    eo.setTypeContratTravailRelationship(typeContratTravail);
    return eo;
  }

  public static ERXFetchSpecification<EORepartMethodeTypeContrat> fetchSpec() {
    return new ERXFetchSpecification<EORepartMethodeTypeContrat>(_EORepartMethodeTypeContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartMethodeTypeContrat> fetchAllEORepartMethodeTypeContrats(EOEditingContext editingContext) {
    return _EORepartMethodeTypeContrat.fetchAllEORepartMethodeTypeContrats(editingContext, null);
  }

  public static NSArray<EORepartMethodeTypeContrat> fetchAllEORepartMethodeTypeContrats(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrats(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartMethodeTypeContrat> fetchEORepartMethodeTypeContrats(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartMethodeTypeContrat> fetchSpec = new ERXFetchSpecification<EORepartMethodeTypeContrat>(_EORepartMethodeTypeContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartMethodeTypeContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartMethodeTypeContrat fetchEORepartMethodeTypeContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeTypeContrat fetchEORepartMethodeTypeContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartMethodeTypeContrat> eoObjects = _EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrats(editingContext, qualifier, null);
    EORepartMethodeTypeContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartMethodeTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeTypeContrat fetchRequiredEORepartMethodeTypeContrat(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeTypeContrat.fetchRequiredEORepartMethodeTypeContrat(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeTypeContrat fetchRequiredEORepartMethodeTypeContrat(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartMethodeTypeContrat eoObject = _EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrat(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartMethodeTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeTypeContrat localInstanceIn(EOEditingContext editingContext, EORepartMethodeTypeContrat eo) {
    EORepartMethodeTypeContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
