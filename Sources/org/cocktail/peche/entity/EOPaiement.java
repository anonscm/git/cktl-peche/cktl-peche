package org.cocktail.peche.entity;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.metier.miseenpaiement.GrouperMiseEnPaiementPourBordereau;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiement;
import org.cocktail.peche.metier.miseenpaiement.MiseEnPaiementDataBean;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente la table "PAIEMENT".
 */
public class EOPaiement extends _EOPaiement {

	/** Numéro de série. */
	private static final long serialVersionUID = 3368307003174559276L;
	private static Logger log = Logger.getLogger(EOPaiement.class);

	/**
	 * Créer et initialiser une instance paiement.
	 * 
	 * @param editingContext un editing context
	 * @param numeroPaiement un numéro de paiement
	 * @param moisPaiement le mois de paiement
	 * @param anneePaiement l'année de paiement
	 * @param personne la personne qui crée ce paiement
	 * @return le paiment créé
	 */
	public static EOPaiement creerEtInitialiser(EOEditingContext editingContext, int numeroPaiement, int moisPaiement, int anneePaiement, EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		EOPaiement paiement = EOPaiement.createEOPaiement(editingContext, anneePaiement, now, now, now, moisPaiement, numeroPaiement, Constante.NON, personne, personne);
		return paiement;
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(new NSTimestamp());
			setToPersonneModificationRelationship(personne);
		}
	}
	
	/**
	 * @return the anneeUniv : année universitaire
	 */
	public String anneeUniversitaire() {
		return this.anneePaiement() + "/" + (this.anneePaiement() + 1);
	}
	
	/**
	 * @return le libellé long du témoin : 'Oui' ou 'Non'
	 */
	public String temPayeLibelleLong() {
		if (Constante.OUI.equals(this.paye())) {
			return Constante.OUI_LONG;
		}
		
		return Constante.NON_LONG;
	}
	
	/**
	 * Le paiement concerne t-il des statutaires ? 
	 *  
	 */
	public boolean isPaiementForStatutaires(boolean isAnneeUniversitaireEnAnneeCivil) {
		List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(this, new GrouperMiseEnPaiementPourBordereau());
		Iterator<MiseEnPaiementDataBean> iterateur = listeMiseEnPaiement.iterator();

		// Si le 1er enseignant de la liste des mises en paiement est statutaire
		MiseEnPaiementDataBean miseEnPaiementDataBean = null;
		if (iterateur.hasNext()) {
			miseEnPaiementDataBean = iterateur.next();
			if (miseEnPaiementDataBean != null) {
				EOActuelEnseignant enseignant = miseEnPaiementDataBean.getService().enseignant();
				return (enseignant.isStatutaire(this.anneePaiement(), isAnneeUniversitaireEnAnneeCivil));
			}
		}
		return false;
	}
}