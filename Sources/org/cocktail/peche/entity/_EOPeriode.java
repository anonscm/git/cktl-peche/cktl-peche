// DO NOT EDIT.  Make changes to EOPeriode.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPeriode extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOPeriode";

  // Attribute Keys
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> NO_DERNIER_MOIS = new ERXKey<Integer>("noDernierMois");
  public static final ERXKey<Integer> NO_PREMIER_MOIS = new ERXKey<Integer>("noPremierMois");
  public static final ERXKey<Integer> NO_SEMESTRE = new ERXKey<Integer>("noSemestre");
  public static final ERXKey<Integer> ORDRE_AFFICHAGE = new ERXKey<Integer>("ordreAffichage");
  public static final ERXKey<Integer> ORDRE_PERIODE = new ERXKey<Integer>("ordrePeriode");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> TO_LISTE_SERVICE_DETAIL = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("toListeServiceDetail");

  // Attributes
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String NO_DERNIER_MOIS_KEY = NO_DERNIER_MOIS.key();
  public static final String NO_PREMIER_MOIS_KEY = NO_PREMIER_MOIS.key();
  public static final String NO_SEMESTRE_KEY = NO_SEMESTRE.key();
  public static final String ORDRE_AFFICHAGE_KEY = ORDRE_AFFICHAGE.key();
  public static final String ORDRE_PERIODE_KEY = ORDRE_PERIODE.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships
  public static final String TO_LISTE_SERVICE_DETAIL_KEY = TO_LISTE_SERVICE_DETAIL.key();

  private static Logger LOG = Logger.getLogger(_EOPeriode.class);

  public EOPeriode localInstanceIn(EOEditingContext editingContext) {
    EOPeriode localInstance = (EOPeriode)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String libelle() {
    return (String) storedValueForKey(_EOPeriode.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.LIBELLE_KEY);
  }

  public Integer noDernierMois() {
    return (Integer) storedValueForKey(_EOPeriode.NO_DERNIER_MOIS_KEY);
  }

  public void setNoDernierMois(Integer value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating noDernierMois from " + noDernierMois() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.NO_DERNIER_MOIS_KEY);
  }

  public Integer noPremierMois() {
    return (Integer) storedValueForKey(_EOPeriode.NO_PREMIER_MOIS_KEY);
  }

  public void setNoPremierMois(Integer value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating noPremierMois from " + noPremierMois() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.NO_PREMIER_MOIS_KEY);
  }

  public Integer noSemestre() {
    return (Integer) storedValueForKey(_EOPeriode.NO_SEMESTRE_KEY);
  }

  public void setNoSemestre(Integer value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating noSemestre from " + noSemestre() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.NO_SEMESTRE_KEY);
  }

  public Integer ordreAffichage() {
    return (Integer) storedValueForKey(_EOPeriode.ORDRE_AFFICHAGE_KEY);
  }

  public void setOrdreAffichage(Integer value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating ordreAffichage from " + ordreAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.ORDRE_AFFICHAGE_KEY);
  }

  public Integer ordrePeriode() {
    return (Integer) storedValueForKey(_EOPeriode.ORDRE_PERIODE_KEY);
  }

  public void setOrdrePeriode(Integer value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating ordrePeriode from " + ordrePeriode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.ORDRE_PERIODE_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOPeriode.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
    	_EOPeriode.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPeriode.TYPE_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail() {
    return (NSArray<org.cocktail.peche.entity.EOServiceDetail>)storedValueForKey(_EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier) {
    return toListeServiceDetail(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier, boolean fetch) {
    return toListeServiceDetail(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOServiceDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOServiceDetail.TO_PERIODE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOServiceDetail.fetchEOServiceDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeServiceDetail();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeServiceDetail(org.cocktail.peche.entity.EOServiceDetail object) {
    includeObjectIntoPropertyWithKey(object, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public void removeFromToListeServiceDetail(org.cocktail.peche.entity.EOServiceDetail object) {
    excludeObjectFromPropertyWithKey(object, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public void addToToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
      _EOPeriode.LOG.debug("adding " + object + " to toListeServiceDetail relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeServiceDetail(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
    }
  }

  public void removeFromToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOPeriode.LOG.isDebugEnabled()) {
      _EOPeriode.LOG.debug("removing " + object + " from toListeServiceDetail relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeServiceDetail(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
    }
  }

  public org.cocktail.peche.entity.EOServiceDetail createToListeServiceDetailRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOServiceDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
    return (org.cocktail.peche.entity.EOServiceDetail) eo;
  }

  public void deleteToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPeriode.TO_LISTE_SERVICE_DETAIL_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeServiceDetailRelationships() {
    Enumeration<org.cocktail.peche.entity.EOServiceDetail> objects = toListeServiceDetail().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeServiceDetailRelationship(objects.nextElement());
    }
  }


  public static EOPeriode createEOPeriode(EOEditingContext editingContext, String libelle
, Integer noDernierMois
, Integer noPremierMois
, Integer noSemestre
, Integer ordreAffichage
, Integer ordrePeriode
, String type
) {
    EOPeriode eo = (EOPeriode) EOUtilities.createAndInsertInstance(editingContext, _EOPeriode.ENTITY_NAME);    
		eo.setLibelle(libelle);
		eo.setNoDernierMois(noDernierMois);
		eo.setNoPremierMois(noPremierMois);
		eo.setNoSemestre(noSemestre);
		eo.setOrdreAffichage(ordreAffichage);
		eo.setOrdrePeriode(ordrePeriode);
		eo.setType(type);
    return eo;
  }

  public static ERXFetchSpecification<EOPeriode> fetchSpec() {
    return new ERXFetchSpecification<EOPeriode>(_EOPeriode.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPeriode> fetchAllEOPeriodes(EOEditingContext editingContext) {
    return _EOPeriode.fetchAllEOPeriodes(editingContext, null);
  }

  public static NSArray<EOPeriode> fetchAllEOPeriodes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPeriode.fetchEOPeriodes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPeriode> fetchEOPeriodes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPeriode> fetchSpec = new ERXFetchSpecification<EOPeriode>(_EOPeriode.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPeriode> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPeriode fetchEOPeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriode.fetchEOPeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriode fetchEOPeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPeriode> eoObjects = _EOPeriode.fetchEOPeriodes(editingContext, qualifier, null);
    EOPeriode eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriode fetchRequiredEOPeriode(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPeriode.fetchRequiredEOPeriode(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPeriode fetchRequiredEOPeriode(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPeriode eoObject = _EOPeriode.fetchEOPeriode(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOPeriode that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPeriode localInstanceIn(EOEditingContext editingContext, EOPeriode eo) {
    EOPeriode localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
