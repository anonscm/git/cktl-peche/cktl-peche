// DO NOT EDIT.  Make changes to EORepartUefDetail.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartUefDetail extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartUefDetail";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<org.joda.time.DateTime> DATE_CREATION = new ERXKey<org.joda.time.DateTime>("dateCreation");
  public static final ERXKey<org.joda.time.DateTime> DATE_MODIFICATION = new ERXKey<org.joda.time.DateTime>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant> COMPOSANT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant>("composant");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  // Relationships
  public static final String COMPOSANT_KEY = COMPOSANT.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EORepartUefDetail.class);

  public EORepartUefDetail localInstanceIn(EOEditingContext editingContext) {
    EORepartUefDetail localInstance = (EORepartUefDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EORepartUefDetail.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
    	_EORepartUefDetail.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefDetail.COMMENTAIRE_KEY);
  }

  public org.joda.time.DateTime dateCreation() {
    return (org.joda.time.DateTime) storedValueForKey(_EORepartUefDetail.DATE_CREATION_KEY);
  }

  public void setDateCreation(org.joda.time.DateTime value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
    	_EORepartUefDetail.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefDetail.DATE_CREATION_KEY);
  }

  public org.joda.time.DateTime dateModification() {
    return (org.joda.time.DateTime) storedValueForKey(_EORepartUefDetail.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(org.joda.time.DateTime value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
    	_EORepartUefDetail.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefDetail.DATE_MODIFICATION_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EORepartUefDetail.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
    	_EORepartUefDetail.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefDetail.ID_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant composant() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant)storedValueForKey(_EORepartUefDetail.COMPOSANT_KEY);
  }
  
  public void setComposant(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    takeStoredValueForKey(value, _EORepartUefDetail.COMPOSANT_KEY);
  }

  public void setComposantRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
      _EORepartUefDetail.LOG.debug("updating composant from " + composant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant oldValue = composant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefDetail.COMPOSANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefDetail.COMPOSANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefDetail.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefDetail.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
      _EORepartUefDetail.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefDetail.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefDetail.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefDetail.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefDetail.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefDetail.LOG.isDebugEnabled()) {
      _EORepartUefDetail.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefDetail.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefDetail.PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EORepartUefDetail createEORepartUefDetail(EOEditingContext editingContext, org.joda.time.DateTime dateCreation
, org.joda.time.DateTime dateModification
, Integer id
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EORepartUefDetail eo = (EORepartUefDetail) EOUtilities.createAndInsertInstance(editingContext, _EORepartUefDetail.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EORepartUefDetail> fetchSpec() {
    return new ERXFetchSpecification<EORepartUefDetail>(_EORepartUefDetail.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartUefDetail> fetchAllEORepartUefDetails(EOEditingContext editingContext) {
    return _EORepartUefDetail.fetchAllEORepartUefDetails(editingContext, null);
  }

  public static NSArray<EORepartUefDetail> fetchAllEORepartUefDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartUefDetail.fetchEORepartUefDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartUefDetail> fetchEORepartUefDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartUefDetail> fetchSpec = new ERXFetchSpecification<EORepartUefDetail>(_EORepartUefDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartUefDetail> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartUefDetail fetchEORepartUefDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefDetail.fetchEORepartUefDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefDetail fetchEORepartUefDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartUefDetail> eoObjects = _EORepartUefDetail.fetchEORepartUefDetails(editingContext, qualifier, null);
    EORepartUefDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartUefDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefDetail fetchRequiredEORepartUefDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefDetail.fetchRequiredEORepartUefDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefDetail fetchRequiredEORepartUefDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartUefDetail eoObject = _EORepartUefDetail.fetchEORepartUefDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartUefDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefDetail localInstanceIn(EOEditingContext editingContext, EORepartUefDetail eo) {
    EORepartUefDetail localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
