package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;
import er.extensions.qualifiers.ERXAndQualifier;

/**
 * Cette classe représente la table "PARAMETRE_FLUX_PAIEMENT".
 * 
 * @author Pascal MACOUIN
 */
public class EOParametresFluxPaiement extends _EOParametresFluxPaiement {
	/** Numéro de série. */
	private static final long serialVersionUID = 6264064152021049581L;
	@SuppressWarnings("unused")
	private static Logger log = Logger
			.getLogger(EOParametresFluxPaiement.class);

	public static final String TEMPLATE_NOM_FLUX_VALEUR_DEFAUT = "paiement_%1$d_%2$04d%3$02d";

	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de
	 * l'instance.
	 * 
	 * @param personne
	 *            la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);

		// en creation, il n'y a pas de date de fin
		setDateFinApplication(null);

		// on met à jour le précédent taux ( date fin à null) en mettant la date
		// de fin à la veille de la date de début d'application du nouveau taux
		ERXAndQualifier qualifier = ERXQ.and(EOParametresFluxPaiement.DATE_FIN_APPLICATION
				.isNull());

		NSArray<EOSortOrdering> sortOrderings = EOParametresFluxPaiement.DATE_DEBUT_APPLICATION.descs();
		
		NSArray<EOParametresFluxPaiement> listeParamFluxPaiement = EOParametresFluxPaiement
				.fetchEOParametresFluxPaiements(editingContext(), qualifier, sortOrderings);
		EOParametresFluxPaiement paramFluxAModifier = null;

		if (!listeParamFluxPaiement.isEmpty()) {
			paramFluxAModifier = listeParamFluxPaiement.get(0);
			NSTimestamp veille = dateDebutApplication()
					.timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);
			paramFluxAModifier.setDateFinApplication(veille);
		}

	}

	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de
	 * l'instance.
	 * 
	 * @param personne
	 *            la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(new NSTimestamp());
			setToPersonneModificationRelationship(personne);
		}
	}

	/**
	 * 
	 */
	public void majDonnesAuditSuppression() {

		// on met à jour le précédent taux le plus recent en mettant la date de
		// fin à null
		ERXAndQualifier qualifier = ERXQ
				.and(EOParametresFluxPaiement.DATE_FIN_APPLICATION.isNotNull())
				.and(EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT.is(toTypeFluxPaiement()));
				
		NSArray<EOParametresFluxPaiement> listeParametresFluxPaiement = EOParametresFluxPaiement
				.fetchEOParametresFluxPaiements(editingContext(), qualifier,
						EOParametresFluxPaiement.DATE_DEBUT_APPLICATION.descs());
		EOParametresFluxPaiement parametreFluxAModifier = null;

		if (!listeParametresFluxPaiement.isEmpty()) {
			parametreFluxAModifier = listeParametresFluxPaiement.get(0);
			parametreFluxAModifier.setDateFinApplication(null);
		}
	}

	/**
	 * @param datePaiement
	 *            : date du paiement
	 * @return qualifier pour déterminer le paramètre du flux de paiement en
	 *         fonction de la date de paiement
	 */
	public static EOQualifier qualifierPourDatePaiement(NSTimestamp datePaiement) {
		return ERXQ.and(ERXQ.lessThanOrEqualTo(
				EOParametresFluxPaiement.DATE_DEBUT_APPLICATION_KEY,
				datePaiement), ERXQ.or(ERXQ
				.isNull(EOParametresFluxPaiement.DATE_FIN_APPLICATION_KEY),
				ERXQ.greaterThanOrEqualTo(
						EOParametresFluxPaiement.DATE_FIN_APPLICATION_KEY,
						datePaiement)));
	}

	/**
	 * @param edc
	 *            : editingCOntext
	 * @param datePaiement
	 *            : date de paiement
	 * @return le paramètre de flux de paiement pour la date de paiement
	 */
	public static EOParametresFluxPaiement getParametresPourDatePaiement(
			EOEditingContext edc, NSTimestamp datePaiement) {
		return EOParametresFluxPaiement.fetchEOParametresFluxPaiement(edc,
				qualifierPourDatePaiement(datePaiement));
	}

	/**
	 * 
	 * @param editingContext
	 * @return
	 */
	public static EOParametresFluxPaiement creerEtInitialiser(
			EOEditingContext editingContext, EOTypeFluxPaiement typeFluxPaiement) {

		NSTimestamp now = new NSTimestamp();

		EOParametresFluxPaiement eoParametresFluxPaiement = EOParametresFluxPaiement
				.creerEtInitialiser(editingContext);
		eoParametresFluxPaiement
				.setTemplateNomFlux(TEMPLATE_NOM_FLUX_VALEUR_DEFAUT);
		eoParametresFluxPaiement.setDateDebutApplication(now);
		eoParametresFluxPaiement.setToTypeFluxPaiementRelationship(typeFluxPaiement);
		return eoParametresFluxPaiement;
	}
	
	/**
	 * Creer et initialise une instance de la classe.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return un élement du service.
	 */
	public static EOParametresFluxPaiement creerEtInitialiser(EOEditingContext edc) {
		EOParametresFluxPaiement parametresFluxPaiement = new EOParametresFluxPaiement();
		edc.insertObject(parametresFluxPaiement);
		return parametresFluxPaiement;
	}
	

	public NSTimestamp dateDebutDernierParametre(
			EOTypeFluxPaiement typeFluxPaiement) {
		NSTimestamp dateDebut = null;

		ERXAndQualifier qualifier = ERXQ.and(
				EOParametresFluxPaiement.DATE_FIN_APPLICATION.isNull());

		NSArray<EOParametresFluxPaiement> listeParametres = EOParametresFluxPaiement
				.fetchEOParametresFluxPaiements(editingContext(), qualifier,
						EOParametresFluxPaiement.DATE_DEBUT_APPLICATION.descs());
		EOParametresFluxPaiement parametreFluxPaiement = null;

		if (!listeParametres.isEmpty()) {
			parametreFluxPaiement = listeParametres.get(0);
			dateDebut = parametreFluxPaiement.dateDebutApplication();
		}

		return dateDebut;
	}

	/**
	 * @return Le tri par défaut de la liste
	 */
	public static ERXSortOrderings getTriParDefaut() {
		return EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT
				.dot(EOTypeFluxPaiement.LIBELLE_KEY).asc()
				.then(EOParametresFluxPaiement.DATE_DEBUT_APPLICATION.asc());
	}

	/**
	 * @return le type de flux est Papaye ?
	 */
	public boolean isFluxPapaye() {
		if (toTypeFluxPaiement().getClasseTypeFlux().toString().contains("Papaye")) {
			return true;
		}
		return false;
	}

}
