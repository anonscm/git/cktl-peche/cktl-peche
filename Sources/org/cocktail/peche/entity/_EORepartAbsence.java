// DO NOT EDIT.  Make changes to EORepartAbsence.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartAbsence extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartAbsence";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> NB_HEURES = new ERXKey<java.math.BigDecimal>("nbHeures");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.peche.entity.EOVueAbsences> TO_ABSENCE = new ERXKey<org.cocktail.peche.entity.EOVueAbsences>("toAbsence");
  public static final ERXKey<org.cocktail.peche.entity.EOService> TO_SERVICE = new ERXKey<org.cocktail.peche.entity.EOService>("toService");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String NB_HEURES_KEY = NB_HEURES.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_ABSENCE_KEY = TO_ABSENCE.key();
  public static final String TO_SERVICE_KEY = TO_SERVICE.key();

  private static Logger LOG = Logger.getLogger(_EORepartAbsence.class);

  public EORepartAbsence localInstanceIn(EOEditingContext editingContext) {
    EORepartAbsence localInstance = (EORepartAbsence)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartAbsence.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
    	_EORepartAbsence.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartAbsence.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartAbsence.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
    	_EORepartAbsence.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartAbsence.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal nbHeures() {
    return (java.math.BigDecimal) storedValueForKey(_EORepartAbsence.NB_HEURES_KEY);
  }

  public void setNbHeures(java.math.BigDecimal value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
    	_EORepartAbsence.LOG.debug( "updating nbHeures from " + nbHeures() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartAbsence.NB_HEURES_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartAbsence.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartAbsence.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
      _EORepartAbsence.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartAbsence.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartAbsence.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartAbsence.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartAbsence.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
      _EORepartAbsence.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartAbsence.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartAbsence.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOVueAbsences toAbsence() {
    return (org.cocktail.peche.entity.EOVueAbsences)storedValueForKey(_EORepartAbsence.TO_ABSENCE_KEY);
  }
  
  public void setToAbsence(org.cocktail.peche.entity.EOVueAbsences value) {
    takeStoredValueForKey(value, _EORepartAbsence.TO_ABSENCE_KEY);
  }

  public void setToAbsenceRelationship(org.cocktail.peche.entity.EOVueAbsences value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
      _EORepartAbsence.LOG.debug("updating toAbsence from " + toAbsence() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToAbsence(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOVueAbsences oldValue = toAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartAbsence.TO_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartAbsence.TO_ABSENCE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOService toService() {
    return (org.cocktail.peche.entity.EOService)storedValueForKey(_EORepartAbsence.TO_SERVICE_KEY);
  }
  
  public void setToService(org.cocktail.peche.entity.EOService value) {
    takeStoredValueForKey(value, _EORepartAbsence.TO_SERVICE_KEY);
  }

  public void setToServiceRelationship(org.cocktail.peche.entity.EOService value) {
    if (_EORepartAbsence.LOG.isDebugEnabled()) {
      _EORepartAbsence.LOG.debug("updating toService from " + toService() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOService oldValue = toService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartAbsence.TO_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartAbsence.TO_SERVICE_KEY);
    }
  }
  

  public static EORepartAbsence createEORepartAbsence(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, java.math.BigDecimal nbHeures
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.peche.entity.EOVueAbsences toAbsence, org.cocktail.peche.entity.EOService toService) {
    EORepartAbsence eo = (EORepartAbsence) EOUtilities.createAndInsertInstance(editingContext, _EORepartAbsence.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setNbHeures(nbHeures);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setToAbsenceRelationship(toAbsence);
    eo.setToServiceRelationship(toService);
    return eo;
  }

  public static ERXFetchSpecification<EORepartAbsence> fetchSpec() {
    return new ERXFetchSpecification<EORepartAbsence>(_EORepartAbsence.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartAbsence> fetchAllEORepartAbsences(EOEditingContext editingContext) {
    return _EORepartAbsence.fetchAllEORepartAbsences(editingContext, null);
  }

  public static NSArray<EORepartAbsence> fetchAllEORepartAbsences(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartAbsence.fetchEORepartAbsences(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartAbsence> fetchEORepartAbsences(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartAbsence> fetchSpec = new ERXFetchSpecification<EORepartAbsence>(_EORepartAbsence.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartAbsence> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartAbsence fetchEORepartAbsence(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAbsence.fetchEORepartAbsence(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAbsence fetchEORepartAbsence(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartAbsence> eoObjects = _EORepartAbsence.fetchEORepartAbsences(editingContext, qualifier, null);
    EORepartAbsence eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartAbsence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAbsence fetchRequiredEORepartAbsence(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartAbsence.fetchRequiredEORepartAbsence(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartAbsence fetchRequiredEORepartAbsence(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartAbsence eoObject = _EORepartAbsence.fetchEORepartAbsence(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartAbsence that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartAbsence localInstanceIn(EOEditingContext editingContext, EORepartAbsence eo) {
    EORepartAbsence localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
