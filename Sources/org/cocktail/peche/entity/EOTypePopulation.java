package org.cocktail.peche.entity;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.peche.entity.interfaces.ITypePopulation;
import org.joda.time.DateTime;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXQ;

public class EOTypePopulation extends _EOTypePopulation implements ITypePopulation{
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypePopulation.class);
	private static final long serialVersionUID = -1408357188070877855L;

	public String getCode() {
		return code();
	}

	public String getLibelle() {
		return libelle();
	}

	public DateTime getDateCreation() {
		return dateCreation();
	}

	public DateTime getDateModification() {
		return dateModification();
	}
	
	public static EOTypePopulation getTypePopulationTous(EOEditingContext ec) {
		return fetchEOTypePopulation(ec, ERXQ.equals(EOTypePopulation.CODE_KEY, TypePopulation.TOUS.getCode()));
	}
	
	public static EOTypePopulation getTypePopulationStat(EOEditingContext ec) {
		return fetchEOTypePopulation(ec, ERXQ.equals(EOTypePopulation.CODE_KEY, TypePopulation.STATUTAIRE.getCode()));
	}
	
	public static EOTypePopulation getTypePopulationVac(EOEditingContext ec) {
		return fetchEOTypePopulation(ec, ERXQ.equals(EOTypePopulation.CODE_KEY, TypePopulation.VACATAIRE.getCode()));
	}
	
	public static List<EOTypePopulation> getListeTypePopulation(EOEditingContext ec) {
		return fetchAllEOTypePopulations(ec);
	}
	
	public boolean estTypePopulationTous() {
		return this.code().equals(TypePopulation.TOUS.getCode());
	}
	
	public boolean estTypePopulationStatutaire() {
		return this.code().equals(TypePopulation.STATUTAIRE.getCode());
	}
	
	public boolean estTypePopulationVacataire() {
		return this.code().equals(TypePopulation.VACATAIRE.getCode());
	}
	
	
	public enum TypePopulation {

		TOUS ("TOUS"),
		STATUTAIRE ("STAT"),
		VACATAIRE ("VAC");
		
		private String code;

		private TypePopulation(String code) {
			this.code = code;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}
		
		
	}
	
}
