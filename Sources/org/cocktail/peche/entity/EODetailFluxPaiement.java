package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
/**
 * 
 * @author juliencallewaert
 *
 */
public class EODetailFluxPaiement extends _EODetailFluxPaiement {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2929325555957797640L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODetailFluxPaiement.class);
	
	
	/**
	 * 
	 * @param editingContext
	 * @param definitionFluxPaiement
	 * @param parametresFluxPaiement
	 * @return
	 */
	public static EODetailFluxPaiement creerEtInitialiser(EOEditingContext editingContext, EODefinitionFluxPaiement definitionFluxPaiement, EOParametresFluxPaiement parametresFluxPaiement) {
		
		EODetailFluxPaiement eoDetailFluxPaiement = EODetailFluxPaiement.creerEtInitialiser(editingContext);
		eoDetailFluxPaiement.setToDefinitionFluxPaiementRelationship(definitionFluxPaiement);
		eoDetailFluxPaiement.setToParametresFluxPaiementRelationship(parametresFluxPaiement);
		String valeurDefaut = definitionFluxPaiement.valeurDefaut();
		if (!StringCtrl.isEmpty(valeurDefaut)) {
			eoDetailFluxPaiement.setValeurChamp(valeurDefaut);
		}
		return eoDetailFluxPaiement;
	}
	
	
	/**
	 * Creer et initialise une instance de la classe.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return un élement du service.
	 */
	public static EODetailFluxPaiement creerEtInitialiser(EOEditingContext edc) {
		EODetailFluxPaiement detailsFluxPaiement = new EODetailFluxPaiement();
		edc.insertObject(detailsFluxPaiement);
		return detailsFluxPaiement;
	}
	
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);
	}
	
	
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(new NSTimestamp());
			setToPersonneModificationRelationship(personne);
		}
	}
	
}
