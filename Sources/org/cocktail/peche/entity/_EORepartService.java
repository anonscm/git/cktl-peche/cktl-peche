// DO NOT EDIT.  Make changes to EORepartService.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartService extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartService";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_COMMENTAIRE = new ERXKey<NSTimestamp>("dateCommentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> IND_VALIDATION_AUTO_UE_EC = new ERXKey<String>("indValidationAutoUeEc");
  public static final ERXKey<String> TEM_INCIDENT = new ERXKey<String>("temIncident");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemande");
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> TO_LISTE_SERVICE_DETAIL = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("toListeServiceDetail");
  public static final ERXKey<org.cocktail.peche.entity.EOService> TO_SERVICE = new ERXKey<org.cocktail.peche.entity.EOService>("toService");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_COMMENTAIRE_KEY = DATE_COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String IND_VALIDATION_AUTO_UE_EC_KEY = IND_VALIDATION_AUTO_UE_EC.key();
  public static final String TEM_INCIDENT_KEY = TEM_INCIDENT.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_DEMANDE_KEY = TO_DEMANDE.key();
  public static final String TO_LISTE_SERVICE_DETAIL_KEY = TO_LISTE_SERVICE_DETAIL.key();
  public static final String TO_SERVICE_KEY = TO_SERVICE.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();

  private static Logger LOG = Logger.getLogger(_EORepartService.class);

  public EORepartService localInstanceIn(EOEditingContext editingContext) {
    EORepartService localInstance = (EORepartService)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EORepartService.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCommentaire() {
    return (NSTimestamp) storedValueForKey(_EORepartService.DATE_COMMENTAIRE_KEY);
  }

  public void setDateCommentaire(NSTimestamp value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating dateCommentaire from " + dateCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.DATE_COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartService.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartService.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.DATE_MODIFICATION_KEY);
  }

  public String indValidationAutoUeEc() {
    return (String) storedValueForKey(_EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY);
  }

  public void setIndValidationAutoUeEc(String value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating indValidationAutoUeEc from " + indValidationAutoUeEc() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.IND_VALIDATION_AUTO_UE_EC_KEY);
  }

  public String temIncident() {
    return (String) storedValueForKey(_EORepartService.TEM_INCIDENT_KEY);
  }

  public void setTemIncident(String value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
    	_EORepartService.LOG.debug( "updating temIncident from " + temIncident() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartService.TEM_INCIDENT_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartService.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartService.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartService.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartService.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartService.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartService.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartService.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartService.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(_EORepartService.TO_DEMANDE_KEY);
  }
  
  public void setToDemande(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    takeStoredValueForKey(value, _EORepartService.TO_DEMANDE_KEY);
  }

  public void setToDemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("updating toDemande from " + toDemande() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDemande(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = toDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartService.TO_DEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartService.TO_DEMANDE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOService toService() {
    return (org.cocktail.peche.entity.EOService)storedValueForKey(_EORepartService.TO_SERVICE_KEY);
  }
  
  public void setToService(org.cocktail.peche.entity.EOService value) {
    takeStoredValueForKey(value, _EORepartService.TO_SERVICE_KEY);
  }

  public void setToServiceRelationship(org.cocktail.peche.entity.EOService value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("updating toService from " + toService() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOService oldValue = toService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartService.TO_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartService.TO_SERVICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EORepartService.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EORepartService.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartService.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartService.TO_STRUCTURE_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail() {
    return (NSArray<org.cocktail.peche.entity.EOServiceDetail>)storedValueForKey(_EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier) {
    return toListeServiceDetail(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier, boolean fetch) {
    return toListeServiceDetail(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> toListeServiceDetail(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOServiceDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOServiceDetail.TO_REPART_SERVICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOServiceDetail.fetchEOServiceDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeServiceDetail();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeServiceDetail(org.cocktail.peche.entity.EOServiceDetail object) {
    includeObjectIntoPropertyWithKey(object, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public void removeFromToListeServiceDetail(org.cocktail.peche.entity.EOServiceDetail object) {
    excludeObjectFromPropertyWithKey(object, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
  }

  public void addToToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("adding " + object + " to toListeServiceDetail relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeServiceDetail(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
    }
  }

  public void removeFromToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EORepartService.LOG.isDebugEnabled()) {
      _EORepartService.LOG.debug("removing " + object + " from toListeServiceDetail relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeServiceDetail(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
    }
  }

  public org.cocktail.peche.entity.EOServiceDetail createToListeServiceDetailRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOServiceDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
    return (org.cocktail.peche.entity.EOServiceDetail) eo;
  }

  public void deleteToListeServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EORepartService.TO_LISTE_SERVICE_DETAIL_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeServiceDetailRelationships() {
    Enumeration<org.cocktail.peche.entity.EOServiceDetail> objects = toListeServiceDetail().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeServiceDetailRelationship(objects.nextElement());
    }
  }


  public static EORepartService createEORepartService(EOEditingContext editingContext, NSTimestamp dateCreation
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.peche.entity.EOService toService) {
    EORepartService eo = (EORepartService) EOUtilities.createAndInsertInstance(editingContext, _EORepartService.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setToServiceRelationship(toService);
    return eo;
  }

  public static ERXFetchSpecification<EORepartService> fetchSpec() {
    return new ERXFetchSpecification<EORepartService>(_EORepartService.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartService> fetchAllEORepartServices(EOEditingContext editingContext) {
    return _EORepartService.fetchAllEORepartServices(editingContext, null);
  }

  public static NSArray<EORepartService> fetchAllEORepartServices(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartService.fetchEORepartServices(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartService> fetchEORepartServices(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartService> fetchSpec = new ERXFetchSpecification<EORepartService>(_EORepartService.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartService> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartService fetchEORepartService(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartService.fetchEORepartService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartService fetchEORepartService(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartService> eoObjects = _EORepartService.fetchEORepartServices(editingContext, qualifier, null);
    EORepartService eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartService fetchRequiredEORepartService(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartService.fetchRequiredEORepartService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartService fetchRequiredEORepartService(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartService eoObject = _EORepartService.fetchEORepartService(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartService localInstanceIn(EOEditingContext editingContext, EORepartService eo) {
    EORepartService localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
