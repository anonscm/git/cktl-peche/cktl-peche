package org.cocktail.peche.entity;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.interfaces.IEnseignant;
import org.cocktail.peche.entity.interfaces.IFiche;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXFetchSpecification;

/**
 * Cette classe représente la fiche de service de l'intervenant
 *
 * @author Chama LAATIK
 */
public class EOService extends _EOService implements IFiche {

	private static final long serialVersionUID = -5331229947690423996L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOService.class);
	public static int CommentairesMax = 1000;

	/**
	 * Crée et initialise un service.
	 *
	 * @param edc
	 *            : contexte d'edition.
	 * @return un service.
	 */
	public static EOService creerEtInitialiser(EOEditingContext edc) {
		EOService service = new EOService();
		edc.insertObject(service);
		return service;
	}
	
	/**
	 * Crée et initialise un service en créant un repartService.
	 *
	 * @param edc : contexte d'edition.
	 * @param personne : la personne qui crée l'instance
	 * @return un service.
	 */
	public static EOService creerEtInitialiserAvecRepart(EOEditingContext edc, EOPersonne personne, Integer annee) {
		
		return creerEtInitialiserAvecRepart(edc, null, personne, null, annee);
	}
	
	/**
	 * Crée et initialise un service en créant un repartService.
	 *
	 * @param edc : contexte d'edition.
	 * @param structure structure
	 * @param personne : la personne qui crée l'instance
	 * @param codeCircuit Utiliser Constante.CIRCUIT_VACATAIRE_A_DETERMINER si l'on veut faire en fonction du paramétre qui détermine 
	 * si les fiches prévisionnelles sont utilisées dans l'établissement 
	 * @param annee l'année en cours
	 * @return un service.
	 */
	public static EOService creerEtInitialiserAvecRepart(EOEditingContext edc, IStructure structure, EOPersonne personne, String codeCircuit, Integer annee) {
		EOService service = new EOService();
		EORepartService.creerNouvelRepartService(edc, service, structure, personne, codeCircuit, annee);
		edc.insertObject(service);

		return service;
	}
	
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);

		if (!StringCtrl.isEmpty(commentaire())) {
			setDateCommentaire(now);
		}
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			NSTimestamp now = new NSTimestamp();
			setDateModification(now);
			setPersonneModificationRelationship(personne);
			
			if (hasKeyChangedFromCommittedSnapshot(EOService.COMMENTAIRE_KEY)) {
				setDateCommentaire(now);
			}
		}
	}
	
	
	/**
	 * @return les heures à payer : 0 si null
	 */
	public BigDecimal getHeuresAPayer() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresAPayer());
	}
	
	/**
	 * @return les heures à reporter : 0 si null
	 */
	public BigDecimal getHeuresAReporterAttribuees() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresAReporterAttribuees());
	}
	
	/**
	 * @return les heures gratuites: 0 si null
	 */
	public BigDecimal getHeuresGratuites() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresGratuites());
	}
	
	/**
	 * @return les heures payees: 0 si null
	 */
	public BigDecimal getHeuresPayees() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresPayees());
	}
	
	/**
	 * @return les heures du service : 0 si null
	 */
	public BigDecimal getHeuresService() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresService());
	}
	
	/**
	 * @return les heures du service dû: 0 si null
	 */
	public BigDecimal getHeuresServiceDu() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresServiceDu());
	}
	
	/**
	 * @return les heures du service attribue: 0 si null
	 */
	public BigDecimal getHeuresServiceAttribue() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresServiceAttribue());
	}
	
	/**
	 * @return les heures du service realise: 0 si null
	 */
	public BigDecimal getHeuresServiceRealise() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresServiceRealise());
	}
	
	/**
	 * 
	 * @return liste serviceDetailReh
	 */
	public NSArray<EOServiceDetail> listeServiceDetailsReh() {
		return listeServiceDetails(EOServiceDetail.REH.isNotNull());
	}
	
	/**
	 * @param edc : editing Context
	 * @param qualifier : qualifier
	 * @param sorts : tri souhaité
	 * @return liste de services uniques (sans doublon)
	 */
	public static List<EOService> fetchAllEOServicesDistinct(EOEditingContext edc, EOQualifier qualifier, NSArray<EOSortOrdering> sorts) {
		return fetchAllEOServices(edc, qualifier, sorts, true);
	}

	/**
	 * @param edc : editing Context
	 * @param qualifier : qualifier
	 * @param sorts : tri souhaité
	 * @param distinct : Est-ce qu'on veut enlever les doublons?
	 * @return liste de services avec ou sans doublon en fonction du booléen distinct
	 */
	public static List<EOService> fetchAllEOServices(EOEditingContext edc, EOQualifier qualifier, NSArray<EOSortOrdering> sorts, boolean distinct) {
		ERXFetchSpecification<EOService> fetchSpec = EOService.fetchSpec().qualify(qualifier).sort(sorts);
		fetchSpec.setUsesDistinct(true);
		return fetchSpec.fetchObjects(edc);
	}
	
	/**
	 * Mise à jour des données du service DU pour un enseignant statutaire
	 * 
	 * @param personne la personne qui modifie le service Du
	 */
	public void majDonnesServiceDuStatutaire(EOEditingContext edc,EOPersonne personne,boolean isFormatAnneeExerciceAnneeCivile) {
		NSTimestamp now = new NSTimestamp();
		HceCtrl hceCtrl = new HceCtrl(edc);

		double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(this.enseignant(), this.annee(), isFormatAnneeExerciceAnneeCivile));
		setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
		setDateModification(now);
		setPersonneModificationRelationship(personne);
	}
	
	/**
	 * Mise à jour des données du service reparti pour un enseignant
	 * 
	 * @param personne la personne qui modifie le service
	 */
	public void majDonnesServiceReparti(EOEditingContext edc,EOPersonne personne,boolean isFormatAnneeExerciceAnneeCivile) {
		NSTimestamp now = new NSTimestamp();
		HceCtrl hceCtrl = new HceCtrl(edc);

		double serviceAttribue = hceCtrl.getServiceAttribue(this.enseignant(), this.annee(), isFormatAnneeExerciceAnneeCivile, this, getHeuresServiceDu().doubleValue());
		double serviceRealise = hceCtrl.getServiceRealise(this.enseignant(), this.annee(), isFormatAnneeExerciceAnneeCivile, this, getHeuresServiceDu().doubleValue());                     		

		setHeuresServiceAttribue(BigDecimal.valueOf(serviceAttribue));
		setHeuresServiceRealise(BigDecimal.valueOf(serviceRealise));
		
		setDateModification(now);
		setPersonneModificationRelationship(personne);
	}
	
	
	/**
	 * Ajout suivi :
	 * s'il existe dèjà, on l'ajoute au début avec un retour à la ligne
	 * attention, on tronque à 1000 carac.
	 * @param suivi le suivi à ajouter
	 */
	public void addSuivi(String suivi) {		
		StringBuffer ajoutSuivi = new StringBuffer();
		int longueurAjout = suivi.length();
		int longueurMax = 1000;
		
		if (longueurAjout > longueurMax) {
			setSuivi(suivi.substring(0, longueurMax).toString());
			return;
		}		
		if (suivi() != null) {
			int longueurActuelle = suivi().length();
			
			if ((longueurActuelle + longueurAjout) > longueurMax) {				
				ajoutSuivi.append(suivi).append("\n");
				ajoutSuivi.append(suivi().substring(0, longueurMax - longueurAjout));
				ajoutSuivi.append("...");
			} else {
				ajoutSuivi.append(suivi).append("\n");
				ajoutSuivi.append(suivi());
			}
		} else {
			ajoutSuivi.append(suivi);
		}
		
		setSuivi(ajoutSuivi.toString());
	}
	
	/**
	 * 
	 * @return true si un suivi est présent
	 */
	public boolean isSuiviPresent() {
		return (suivi() != null);
	}

	/**
	 * {@inheritDoc}
	 */
	public void reinitialiserDemandes(EOEditingContext edc, Integer persId, EOCircuitValidation circuitValidation) {
		
		List<EORepartService> listeRepartService = this.toListeRepartService();
		for (EORepartService eoRepartService : listeRepartService) {
			if (eoRepartService.toDemande().toEtape().isInitiale()) { 
				// on fait ce test car sur les vacataires on peut avoir des repartService avec une demande à l'état initial, et d'autres qui ne le sont pas
				
				gererReinitialisationDemande(edc, persId, circuitValidation, eoRepartService);
				
			}
		}
		
	}

	private void gererReinitialisationDemande(EOEditingContext edc,	Integer persId, EOCircuitValidation circuitValidation, EORepartService eoRepartService) {
		
		CircuitPeche circuit = CircuitPeche.getCircuit(eoRepartService.toDemande().toEtape().toCircuitValidation());
		
		if (circuit.isCircuitDefinitif()) {
			// Dans le cas où la demande est sur un circuit définitif, alors on supprime l'historique de la demande côté circuit définitif, et pas circuit prévisionnel
			// Et on repositionne le dernier historique du prévisionnel sur le circuit de validation passé en paramètre
			List<EOHistoriqueDemande> listeHistoriqueDemande = eoRepartService.toDemande().toHistoriqueDemandes();
			for (EOHistoriqueDemande historiqueDemande : listeHistoriqueDemande) {
				if (CircuitPeche.getCircuit(historiqueDemande.toEtapeArrivee().toCircuitValidation()).isCircuitDefinitif()) {
					historiqueDemande.delete();
				} else {
					if (CircuitPeche.getCircuit(historiqueDemande.toEtapeArrivee().toCircuitValidation()).isCircuitDefinitif()) {
						historiqueDemande.setToEtapeArriveeRelationship(circuitValidation.etapeInitiale());
					}
				}
			}
			
			eoRepartService.toDemande().changerCircuit(Constante.APP_PECHE, circuitValidation.codeCircuitValidation(), persId);
			
		} else {
			eoRepartService.toDemande().supprimerAvecHistorique();
			EORepartService.attachNewDemande(edc, persId, circuitValidation.codeCircuitValidation(), eoRepartService);
		}
	}
	
	public IEnseignant iEnseignant() {
		IEnseignant enseignant = null;
		
		if (isServiceGenerique()) {
			enseignant = toEnseignantGenerique();
		} else {
			enseignant = enseignant();
		}
		
		return enseignant;
	}
	
	public boolean isServiceGenerique() {
		return (toEnseignantGenerique() != null);
	}
}