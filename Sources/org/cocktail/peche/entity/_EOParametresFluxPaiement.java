// DO NOT EDIT.  Make changes to EOParametresFluxPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParametresFluxPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOParametresFluxPaiement";

  // Attribute Keys
  public static final ERXKey<String> ADMINISTRATION = new ERXKey<String>("administration");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_DEBUT_APPLICATION = new ERXKey<NSTimestamp>("dateDebutApplication");
  public static final ERXKey<NSTimestamp> DATE_FIN_APPLICATION = new ERXKey<NSTimestamp>("dateFinApplication");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> DEPARTEMENT = new ERXKey<String>("departement");
  public static final ERXKey<String> TEMPLATE_NOM_FLUX = new ERXKey<String>("templateNomFlux");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EODetailFluxPaiement> TO_LISTE_DETAIL_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EODetailFluxPaiement>("toListeDetailFluxPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");
  public static final ERXKey<org.cocktail.peche.entity.EOTypeFluxPaiement> TO_TYPE_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOTypeFluxPaiement>("toTypeFluxPaiement");

  // Attributes
  public static final String ADMINISTRATION_KEY = ADMINISTRATION.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_DEBUT_APPLICATION_KEY = DATE_DEBUT_APPLICATION.key();
  public static final String DATE_FIN_APPLICATION_KEY = DATE_FIN_APPLICATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DEPARTEMENT_KEY = DEPARTEMENT.key();
  public static final String TEMPLATE_NOM_FLUX_KEY = TEMPLATE_NOM_FLUX.key();
  // Relationships
  public static final String TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY = TO_LISTE_DETAIL_FLUX_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();
  public static final String TO_TYPE_FLUX_PAIEMENT_KEY = TO_TYPE_FLUX_PAIEMENT.key();

  private static Logger LOG = Logger.getLogger(_EOParametresFluxPaiement.class);

  public EOParametresFluxPaiement localInstanceIn(EOEditingContext editingContext) {
    EOParametresFluxPaiement localInstance = (EOParametresFluxPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String administration() {
    return (String) storedValueForKey(_EOParametresFluxPaiement.ADMINISTRATION_KEY);
  }

  public void setAdministration(String value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating administration from " + administration() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.ADMINISTRATION_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOParametresFluxPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateDebutApplication() {
    return (NSTimestamp) storedValueForKey(_EOParametresFluxPaiement.DATE_DEBUT_APPLICATION_KEY);
  }

  public void setDateDebutApplication(NSTimestamp value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating dateDebutApplication from " + dateDebutApplication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.DATE_DEBUT_APPLICATION_KEY);
  }

  public NSTimestamp dateFinApplication() {
    return (NSTimestamp) storedValueForKey(_EOParametresFluxPaiement.DATE_FIN_APPLICATION_KEY);
  }

  public void setDateFinApplication(NSTimestamp value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating dateFinApplication from " + dateFinApplication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.DATE_FIN_APPLICATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOParametresFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public String departement() {
    return (String) storedValueForKey(_EOParametresFluxPaiement.DEPARTEMENT_KEY);
  }

  public void setDepartement(String value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating departement from " + departement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.DEPARTEMENT_KEY);
  }

  public String templateNomFlux() {
    return (String) storedValueForKey(_EOParametresFluxPaiement.TEMPLATE_NOM_FLUX_KEY);
  }

  public void setTemplateNomFlux(String value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
    	_EOParametresFluxPaiement.LOG.debug( "updating templateNomFlux from " + templateNomFlux() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParametresFluxPaiement.TEMPLATE_NOM_FLUX_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOParametresFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOParametresFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
      _EOParametresFluxPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametresFluxPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametresFluxPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOParametresFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOParametresFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
      _EOParametresFluxPaiement.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametresFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametresFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOTypeFluxPaiement toTypeFluxPaiement() {
    return (org.cocktail.peche.entity.EOTypeFluxPaiement)storedValueForKey(_EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT_KEY);
  }
  
  public void setToTypeFluxPaiement(org.cocktail.peche.entity.EOTypeFluxPaiement value) {
    takeStoredValueForKey(value, _EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT_KEY);
  }

  public void setToTypeFluxPaiementRelationship(org.cocktail.peche.entity.EOTypeFluxPaiement value) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
      _EOParametresFluxPaiement.LOG.debug("updating toTypeFluxPaiement from " + toTypeFluxPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeFluxPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOTypeFluxPaiement oldValue = toTypeFluxPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParametresFluxPaiement.TO_TYPE_FLUX_PAIEMENT_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement() {
    return (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)storedValueForKey(_EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier) {
    return toListeDetailFluxPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeDetailFluxPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EODetailFluxPaiement.TO_PARAMETRES_FLUX_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EODetailFluxPaiement.fetchEODetailFluxPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeDetailFluxPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeDetailFluxPaiement(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public void removeFromToListeDetailFluxPaiement(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public void addToToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
      _EOParametresFluxPaiement.LOG.debug("adding " + object + " to toListeDetailFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeDetailFluxPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    if (_EOParametresFluxPaiement.LOG.isDebugEnabled()) {
      _EOParametresFluxPaiement.LOG.debug("removing " + object + " from toListeDetailFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeDetailFluxPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EODetailFluxPaiement createToListeDetailFluxPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EODetailFluxPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EODetailFluxPaiement) eo;
  }

  public void deleteToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOParametresFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeDetailFluxPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EODetailFluxPaiement> objects = toListeDetailFluxPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeDetailFluxPaiementRelationship(objects.nextElement());
    }
  }


  public static EOParametresFluxPaiement createEOParametresFluxPaiement(EOEditingContext editingContext, String administration
, NSTimestamp dateCreation
, NSTimestamp dateDebutApplication
, NSTimestamp dateModification
, String departement
, String templateNomFlux
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification, org.cocktail.peche.entity.EOTypeFluxPaiement toTypeFluxPaiement) {
    EOParametresFluxPaiement eo = (EOParametresFluxPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOParametresFluxPaiement.ENTITY_NAME);    
		eo.setAdministration(administration);
		eo.setDateCreation(dateCreation);
		eo.setDateDebutApplication(dateDebutApplication);
		eo.setDateModification(dateModification);
		eo.setDepartement(departement);
		eo.setTemplateNomFlux(templateNomFlux);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    eo.setToTypeFluxPaiementRelationship(toTypeFluxPaiement);
    return eo;
  }

  public static ERXFetchSpecification<EOParametresFluxPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOParametresFluxPaiement>(_EOParametresFluxPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParametresFluxPaiement> fetchAllEOParametresFluxPaiements(EOEditingContext editingContext) {
    return _EOParametresFluxPaiement.fetchAllEOParametresFluxPaiements(editingContext, null);
  }

  public static NSArray<EOParametresFluxPaiement> fetchAllEOParametresFluxPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParametresFluxPaiement.fetchEOParametresFluxPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParametresFluxPaiement> fetchEOParametresFluxPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParametresFluxPaiement> fetchSpec = new ERXFetchSpecification<EOParametresFluxPaiement>(_EOParametresFluxPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParametresFluxPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParametresFluxPaiement fetchEOParametresFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametresFluxPaiement.fetchEOParametresFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametresFluxPaiement fetchEOParametresFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParametresFluxPaiement> eoObjects = _EOParametresFluxPaiement.fetchEOParametresFluxPaiements(editingContext, qualifier, null);
    EOParametresFluxPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOParametresFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametresFluxPaiement fetchRequiredEOParametresFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParametresFluxPaiement.fetchRequiredEOParametresFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParametresFluxPaiement fetchRequiredEOParametresFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParametresFluxPaiement eoObject = _EOParametresFluxPaiement.fetchEOParametresFluxPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOParametresFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParametresFluxPaiement localInstanceIn(EOEditingContext editingContext, EOParametresFluxPaiement eo) {
    EOParametresFluxPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
