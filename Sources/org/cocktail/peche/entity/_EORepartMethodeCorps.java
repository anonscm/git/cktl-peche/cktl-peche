// DO NOT EDIT.  Make changes to EORepartMethodeCorps.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartMethodeCorps extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartMethodeCorps";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("corps");
  public static final ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp> METHODE = new ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp>("methode");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  // Relationships
  public static final String CORPS_KEY = CORPS.key();
  public static final String METHODE_KEY = METHODE.key();

  private static Logger LOG = Logger.getLogger(_EORepartMethodeCorps.class);

  public EORepartMethodeCorps localInstanceIn(EOEditingContext editingContext) {
    EORepartMethodeCorps localInstance = (EORepartMethodeCorps)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeCorps.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartMethodeCorps.LOG.isDebugEnabled()) {
    	_EORepartMethodeCorps.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeCorps.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeCorps.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartMethodeCorps.LOG.isDebugEnabled()) {
    	_EORepartMethodeCorps.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeCorps.DATE_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps corps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey(_EORepartMethodeCorps.CORPS_KEY);
  }
  
  public void setCorps(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    takeStoredValueForKey(value, _EORepartMethodeCorps.CORPS_KEY);
  }

  public void setCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EORepartMethodeCorps.LOG.isDebugEnabled()) {
      _EORepartMethodeCorps.LOG.debug("updating corps from " + corps() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setCorps(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = corps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeCorps.CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeCorps.CORPS_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOMethodesCalculHComp methode() {
    return (org.cocktail.peche.entity.EOMethodesCalculHComp)storedValueForKey(_EORepartMethodeCorps.METHODE_KEY);
  }
  
  public void setMethode(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    takeStoredValueForKey(value, _EORepartMethodeCorps.METHODE_KEY);
  }

  public void setMethodeRelationship(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    if (_EORepartMethodeCorps.LOG.isDebugEnabled()) {
      _EORepartMethodeCorps.LOG.debug("updating methode from " + methode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setMethode(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOMethodesCalculHComp oldValue = methode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeCorps.METHODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeCorps.METHODE_KEY);
    }
  }
  

  public static EORepartMethodeCorps createEORepartMethodeCorps(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, org.cocktail.fwkcktlpersonne.common.metier.EOCorps corps, org.cocktail.peche.entity.EOMethodesCalculHComp methode) {
    EORepartMethodeCorps eo = (EORepartMethodeCorps) EOUtilities.createAndInsertInstance(editingContext, _EORepartMethodeCorps.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
    eo.setCorpsRelationship(corps);
    eo.setMethodeRelationship(methode);
    return eo;
  }

  public static ERXFetchSpecification<EORepartMethodeCorps> fetchSpec() {
    return new ERXFetchSpecification<EORepartMethodeCorps>(_EORepartMethodeCorps.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartMethodeCorps> fetchAllEORepartMethodeCorpses(EOEditingContext editingContext) {
    return _EORepartMethodeCorps.fetchAllEORepartMethodeCorpses(editingContext, null);
  }

  public static NSArray<EORepartMethodeCorps> fetchAllEORepartMethodeCorpses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartMethodeCorps.fetchEORepartMethodeCorpses(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartMethodeCorps> fetchEORepartMethodeCorpses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartMethodeCorps> fetchSpec = new ERXFetchSpecification<EORepartMethodeCorps>(_EORepartMethodeCorps.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartMethodeCorps> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartMethodeCorps fetchEORepartMethodeCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeCorps.fetchEORepartMethodeCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeCorps fetchEORepartMethodeCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartMethodeCorps> eoObjects = _EORepartMethodeCorps.fetchEORepartMethodeCorpses(editingContext, qualifier, null);
    EORepartMethodeCorps eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartMethodeCorps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeCorps fetchRequiredEORepartMethodeCorps(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeCorps.fetchRequiredEORepartMethodeCorps(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeCorps fetchRequiredEORepartMethodeCorps(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartMethodeCorps eoObject = _EORepartMethodeCorps.fetchEORepartMethodeCorps(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartMethodeCorps that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeCorps localInstanceIn(EOEditingContext editingContext, EORepartMethodeCorps eo) {
    EORepartMethodeCorps localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
