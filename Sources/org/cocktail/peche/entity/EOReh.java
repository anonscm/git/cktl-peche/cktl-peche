package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Cette classe représente les activités du Référentiel des Equivalences Horaires
 *
 * @author Chama LAATIK
 */
public class EOReh extends _EOReh {

	private static final long serialVersionUID = 6817554449757943421L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOReh.class);

	/**
	 * @return Le libellé long si non <code>null</code> sinon la libellé court
	 */
	public String getLibelleLongCourt() {
		String libelle = libelleLong();
		
		if (libelle == null) {
			libelle = libelleCourt();
		}
		
		return libelle;
	}
	
	/**
	 * Crée et initialise une activité REH.
	 *
	 * @param edc : contexte d'edition.
	 * @return une activité REH.
	 */
	public static EOReh creerEtInitialiser(EOEditingContext edc) {
		EOReh reh = new EOReh();
		edc.insertObject(reh);
		return reh;
	}

	/**
	 * Duplique le REH
	 * @param source : activité REH
	 * @param edc : editingContext
	 * @return : activité REH
	 */
	public static EOReh dupliquer(EOReh source, EOEditingContext edc) {
		EOReh copie = EOReh.creerEtInitialiser(edc);
		copie.copier(source);
		return copie;
	}

	/**
	 * Crée un qualifier pour obtenir les REH existant uniquement sur l'année en cours
	 * @param anneeUniversitaire annee en cours
	 * @param isFormatAnneeExerciceAnneeCivile format année civile ?
	 * @return qualifier
	 */
	public static EOQualifier getQualifierPourListeRehVisibles(Integer anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		
		ERXKeyValueQualifier dDebQual = ERXQ.lessThanOrEqualTo(
				EOReh.DATE_DEBUT_KEY,
						OutilsValidation.getNSTFinAnneeUniversitaire(anneeUniversitaire, 
								isFormatAnneeExerciceAnneeCivile));

		ERXKeyValueQualifier dFinQual = ERXQ.greaterThanOrEqualTo(
				EOReh.DATE_FIN_KEY,
						OutilsValidation.getNSTDebutAnneeUniversitaire(anneeUniversitaire, 
								isFormatAnneeExerciceAnneeCivile));
		
		return ERXQ.and(dDebQual, dFinQual);
	}

	
	private void copier(EOReh source) {
		this.setCommentaire(source.commentaire());
		this.setDateDebut(source.dateDebut());
		this.setDateFin(source.dateFin());
		this.setDescription(source.description());
		this.setLibelleCourt(source.libelleCourt());
		this.setLibelleLong(source.libelleLong());
	}
}
