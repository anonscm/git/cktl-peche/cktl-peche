// DO NOT EDIT.  Make changes to EOPecheParametre.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPecheParametre extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOPecheParametre";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> KEY = new ERXKey<String>("key");
  public static final ERXKey<String> VALUE = new ERXKey<String>("value");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String KEY_KEY = KEY.key();
  public static final String VALUE_KEY = VALUE.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOPecheParametre.class);

  public EOPecheParametre localInstanceIn(EOEditingContext editingContext) {
    EOPecheParametre localInstance = (EOPecheParametre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOPecheParametre.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
    	_EOPecheParametre.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPecheParametre.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOPecheParametre.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
    	_EOPecheParametre.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPecheParametre.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOPecheParametre.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
    	_EOPecheParametre.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPecheParametre.DATE_MODIFICATION_KEY);
  }

  public String key() {
    return (String) storedValueForKey(_EOPecheParametre.KEY_KEY);
  }

  public void setKey(String value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
    	_EOPecheParametre.LOG.debug( "updating key from " + key() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPecheParametre.KEY_KEY);
  }

  public String value() {
    return (String) storedValueForKey(_EOPecheParametre.VALUE_KEY);
  }

  public void setValue(String value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
    	_EOPecheParametre.LOG.debug( "updating value from " + value() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPecheParametre.VALUE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOPecheParametre.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOPecheParametre.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
      _EOPecheParametre.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPecheParametre.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPecheParametre.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOPecheParametre.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOPecheParametre.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOPecheParametre.LOG.isDebugEnabled()) {
      _EOPecheParametre.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPecheParametre.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPecheParametre.PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EOPecheParametre createEOPecheParametre(EOEditingContext editingContext, String commentaire
, NSTimestamp dateCreation
, NSTimestamp dateModification
, String key
, String value
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification) {
    EOPecheParametre eo = (EOPecheParametre) EOUtilities.createAndInsertInstance(editingContext, _EOPecheParametre.ENTITY_NAME);    
		eo.setCommentaire(commentaire);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setKey(key);
		eo.setValue(value);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOPecheParametre> fetchSpec() {
    return new ERXFetchSpecification<EOPecheParametre>(_EOPecheParametre.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPecheParametre> fetchAllEOPecheParametres(EOEditingContext editingContext) {
    return _EOPecheParametre.fetchAllEOPecheParametres(editingContext, null);
  }

  public static NSArray<EOPecheParametre> fetchAllEOPecheParametres(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPecheParametre.fetchEOPecheParametres(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPecheParametre> fetchEOPecheParametres(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPecheParametre> fetchSpec = new ERXFetchSpecification<EOPecheParametre>(_EOPecheParametre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPecheParametre> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPecheParametre fetchEOPecheParametre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPecheParametre.fetchEOPecheParametre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPecheParametre fetchEOPecheParametre(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPecheParametre> eoObjects = _EOPecheParametre.fetchEOPecheParametres(editingContext, qualifier, null);
    EOPecheParametre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOPecheParametre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPecheParametre fetchRequiredEOPecheParametre(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPecheParametre.fetchRequiredEOPecheParametre(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPecheParametre fetchRequiredEOPecheParametre(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPecheParametre eoObject = _EOPecheParametre.fetchEOPecheParametre(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOPecheParametre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPecheParametre localInstanceIn(EOEditingContext editingContext, EOPecheParametre eo) {
    EOPecheParametre localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
