package org.cocktail.peche.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.GRHUtilities;
import org.cocktail.fwkcktlgrh.common.metier.EOCarriere;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.EOContratAvenant;
import org.cocktail.fwkcktlgrh.common.metier.EOElements;
import org.cocktail.fwkcktlgrh.common.metier.finder.VacatairesAffectationFinder;
import org.cocktail.fwkcktlgrh.common.metier.finder.VacatairesFinder;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacatairesAffectation;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrade;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.IEnseignantsCtrl;
import org.cocktail.peche.entity.interfaces.IEnseignant;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe représente un enseignant
 *
 * @author Chama LAATIK
 */
public class EOActuelEnseignant extends _EOActuelEnseignant implements IEnseignant {

	private static final long serialVersionUID = 3749103369903103431L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOActuelEnseignant.class);

	/** Pour filtrage mémoire uniquement. Pas de lien dans la base de données. */
	public static final ERXKey<EOStructure> COMPOSANTE = new ERXKey<EOStructure>("composante");
	/** Pour filtrage mémoire uniquement. Pas de lien dans la base de données. */
	public static final ERXKey<EOStructure> DEPARTEMENT_ENSEIGNEMENT = new ERXKey<EOStructure>("departementEnseignement");
	/** Pour filtrage mémoire uniquement. Pas de lien dans la base de données. */
	public static final ERXKey<EOCorps> CORPS = new ERXKey<EOCorps>("corps");
	/** Pour filtrage mémoire uniquement. Pas de lien dans la base de données. */
	public static final ERXKey<String> LIBELLECOURTCORPSOUCONTRAT = new ERXKey<String>("libelleCourtCorpsouContrat");
	
	
	public static final ERXKey<EOContrat> LISTE_CONTRATS = new ERXKey<EOContrat>("listeContrats");
	public static final String LISTE_CONTRATS_KEY = LISTE_CONTRATS.key();	  

	private boolean composanteDejaRecherche;
	private EOStructure composante;
	private boolean departementEnseignementDejaRecherche;
	private EOStructure departementEnseignement;

	// Données en cache
	private boolean carriereDejaRecherchee;
	private EOCarriere carriere;
	private boolean elementCarriereDejaRecherche;
	private EOElements elementCarriere;
	private boolean elementsCarriereDejaRecherche;
	private  NSArray<EOElements> elementsCarriere;
	private boolean corpsDejaRecherche;
	private EOCorps corps;
	private boolean listeContratsDejaRecherches;
	private NSArray<EOContrat> listeContrats;
	private boolean listeTypeContratDejaRecherches;
	private NSArray<EOTypeContratTravail> listeTypeContrat;
	private String etatFicheVoeux;
	private String etatFiche;

	private boolean listeVacationsDejaRecherches;
	private List<IVacataires> listeVacations = new ArrayList<IVacataires>();

	private boolean nbHeuresServiceStatutaireDejaRecherche;
	private double nbHeuresServiceStatutaire;

	private String libelleCourtCorpsouContrat = "";
	private boolean libelleCourtCorpsouContratDejaRecherche;

	private String libelleLongCorpsouContrat = "";
	private boolean libelleLongCorpsouContratDejaRecherche;
	
	private String libelleLongTypePopulation = "";
	private boolean libelleLongTypePopulationDejaRecherche;
	

	private Map<Integer, EOService> mapServicesCache = new HashMap<Integer, EOService>();
	private Map<Integer, Boolean> mapIsStatutairesCache = new HashMap<Integer, Boolean>();
	private Map<Integer, Boolean> mapIsTitulairesCache = new HashMap<Integer, Boolean>();
	private Map<Integer, EOStructure> mapStructureAffectationPrincipaleCache = new HashMap<Integer, EOStructure>();

	private int anneeDejaRecherchee = 0;

	/**
	 * efface le cache
	 */
	public void effacerCache() {
		carriereDejaRecherchee = false;
		elementCarriereDejaRecherche = false;
		elementsCarriereDejaRecherche = false;
		corpsDejaRecherche = false;
		listeContratsDejaRecherches = false;
		listeVacationsDejaRecherches = false;
		nbHeuresServiceStatutaireDejaRecherche = false;
		libelleCourtCorpsouContratDejaRecherche = false;
		libelleLongCorpsouContratDejaRecherche = false;
		libelleLongTypePopulationDejaRecherche = false;

	}

	/**
	 * @return La composante scolarité de l'enseignant (ou la composante si pas de composante scolarité)
	 */
	public EOStructure getComposante() {
		return composante;
	}

	public EOStructure getDepartementEnseignement() {
		return departementEnseignement;
	}

	public String getLibelleCourtCorpsouContrat() {
		return libelleCourtCorpsouContrat;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return composante d'affectation principale de l'enseignant
	 */
	public EOStructure getComposante(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (composanteDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!composanteDejaRecherche) {
			composanteDejaRecherche = true;
			anneeDejaRecherchee = annee;

			if (isStatutaire(annee, isFormatAnneeExerciceAnneeCivile)) {
				composante =  (EOStructure) new BasicPecheCtrl(editingContext(), annee, isFormatAnneeExerciceAnneeCivile).getComposanteTypeCS(getStructureAffectationPrincipale(annee, isFormatAnneeExerciceAnneeCivile));
				return composante;
			}

			IVacatairesAffectation vacatairesAffectation = getAffectationVacatairesPrincipale(annee, isFormatAnneeExerciceAnneeCivile);
			if (vacatairesAffectation != null 
					&& vacatairesAffectation.getToStructure() != null 
					&& EOStructureForGroupeSpec.isGroupeOfType((EOStructure) vacatairesAffectation.getToStructure(), EOTypeGroupe.TGRP_CODE_CS)) {
				composante = (EOStructure) vacatairesAffectation.getToStructure();
				return composante;
			} 

			composante = null;
		}
		return composante;
	}


	private IVacatairesAffectation getAffectationVacatairesPrincipale(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		List<IVacataires> vacations = getListeVacationsCourantesPourAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile);
		if (vacations.size() > 0) {
			//FIXME : utiliser le tem principal
			return VacatairesAffectationFinder.sharedInstance().findAffectationPrincipale(editingContext(), vacations.get(0));
		}
		return null;
	}

	private EOStructure getStructureAffectationPrincipale(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		EOStructure structureAffectationPrincipale = mapStructureAffectationPrincipaleCache.get(annee);
		if (structureAffectationPrincipale == null) {
			structureAffectationPrincipale = (EOStructure) new BasicPecheCtrl(editingContext(), annee, isFormatAnneeExerciceAnneeCivile).getStructureAffectationPrincipale(toIndividu());
			mapStructureAffectationPrincipaleCache.put(annee, structureAffectationPrincipale);
		} 
		return structureAffectationPrincipale;		
	}

	/**
	 * @return Le département d'enseignement de l'enseignant
	 */
	public EOStructure getDepartementEnseignement(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (!departementEnseignementDejaRecherche) {
			departementEnseignement = (EOStructure) new BasicPecheCtrl(editingContext(), annee, isFormatAnneeExerciceAnneeCivile).getDepartementEnseignement(getStructureAffectationPrincipale(annee, isFormatAnneeExerciceAnneeCivile));
			departementEnseignementDejaRecherche = true;			
		}
		return departementEnseignement;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return la structure d'affectation de l'enseignant
	 */
	public IStructure getStructureAffectation(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		IStructure structureAffectation = getDepartementEnseignement(annee, isFormatAnneeExerciceAnneeCivile);

		if (structureAffectation == null) {
			structureAffectation = getComposante(annee, isFormatAnneeExerciceAnneeCivile);
		}

		return structureAffectation;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return La carrière en cours pour l'enseignant ou <code>null</code> s'il n'en a pas
	 */
	public EOCarriere getCarriere(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (carriereDejaRecherchee && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!carriereDejaRecherchee) {
			carriere = EOCarriere.rechercherCarriereEnseignantSurPeriode(editingContext(), toIndividu(),
					OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));
			carriereDejaRecherchee = true;
			anneeDejaRecherchee = annee;
		}

		return carriere;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return L'élément de carrière en cours pour l'enseignant ou <code>null</code> s'il n'en a pas (ou pas de carrière)
	 */
	public EOElements getElementCarriere(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (elementCarriereDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}

		if (!elementCarriereDejaRecherche && getCarriere(annee, isFormatAnneeExerciceAnneeCivile) != null) {
			elementCarriere = EOElements.elementEnseignantsurPeriode(editingContext(), toIndividu(),
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));
			elementCarriereDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}

		return elementCarriere;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return La liste des éléments de carrière de l'enseignant
	 */
	public NSArray<EOElements> getElementsCarriere(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (elementsCarriereDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!elementsCarriereDejaRecherche && getCarriere(annee, isFormatAnneeExerciceAnneeCivile) != null) {
			elementsCarriere = EOElements.elementsEnseignantSurPeriode(editingContext(), toIndividu(),
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));
			elementsCarriereDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}

		return elementsCarriere;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return La liste de contrats de l'enseignant
	 */
	public NSArray<EOContrat> getListeContrats(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (listeContratsDejaRecherches && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!listeContratsDejaRecherches) {
			listeContrats = EOContrat.rechercherContratsEnseignantsPourIndividu(editingContext(), toIndividu(),
					OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));

			listeContratsDejaRecherches = true;
			anneeDejaRecherchee = annee;
		}

		return listeContrats;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return La liste des types de contrats de l'enseignant
	 */
	public NSArray<EOTypeContratTravail> getListeTypesContrat(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (listeTypeContratDejaRecherches && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!listeTypeContratDejaRecherches) {
			listeTypeContrat = new NSMutableArray<EOTypeContratTravail>();

			for (EOContrat contrat : getListeContrats(annee, isFormatAnneeExerciceAnneeCivile)) {
				listeTypeContrat.add(contrat.toTypeContratTravail());
			}
			listeTypeContratDejaRecherches = true;
			anneeDejaRecherchee = annee;
		}

		return listeTypeContrat;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return La liste des vacations de l'enseignant pour une année universitaire
	 */
	public List<IVacataires> getListeVacationsCourantesPourAnneeUniversitaire(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (listeVacationsDejaRecherches && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!listeVacationsDejaRecherches) {
			listeVacations = VacatairesFinder.sharedInstance().getListeVacationsTypeEnseignantPourIndividuEtPeriode(this.editingContext(), 
					toIndividu(), OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile), 
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));
			listeVacationsDejaRecherches = true;
			anneeDejaRecherchee = annee;
		}
		return listeVacations;
	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return Le corps actuel de l'enseignant
	 */
	public EOCorps getCorps(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (corpsDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!corpsDejaRecherche) {
			corps = null;

			EOElements element = getElementCarriere(annee, isFormatAnneeExerciceAnneeCivile);
			if (element != null) {
				corps = element.toCorps();
			} else {
				if (getListeVacationsCourantesPourAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile).size() > 0) {
					corps = getListeVacationsCourantesPourAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile).get(0).getToCorps();
				}
			}

			corpsDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}

		return corps;
	}

	public EOCorps getCorps() {
		return corps;
	}


	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return Le libelle court du corps actuel de l'enseignant ou
	 * le libelle court du type de contrat de travail
	 */
	public String getLibelleCourtCorpsouContrat(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (libelleCourtCorpsouContratDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!libelleCourtCorpsouContratDejaRecherche) {

			if (getCorps(annee, isFormatAnneeExerciceAnneeCivile) != null) {
				EOCorps corps = getCorps(annee, isFormatAnneeExerciceAnneeCivile);
				libelleCourtCorpsouContrat = corps.lcCorps() + " (" + corps.cCorps() + ")";
			} else {
				NSArray<EOContrat> contrats = getListeContrats(annee, isFormatAnneeExerciceAnneeCivile);
				if (contrats.size() > 0) {
					EOTypeContratTravail typeContratTravail = contrats.get(0).toTypeContratTravail();
					if (typeContratTravail != null) {
						libelleCourtCorpsouContrat = typeContratTravail.lcTypeContratTrav() + " (" + typeContratTravail.cTypeContratTrav() + ")";
					}
				} 
			}
			libelleCourtCorpsouContratDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}
		return libelleCourtCorpsouContrat;

	}

	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return Le libelle long du corps actuel de l'enseignant ou
	 * le libelle long du type de contrat de travail avec le code associé entre ()
	 */
	public String getLibelleLongCorpsouContrat(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (libelleLongCorpsouContratDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!libelleLongCorpsouContratDejaRecherche) {

			EOCorps corpsTemp = getCorps(annee, isFormatAnneeExerciceAnneeCivile);

			if (corpsTemp != null) {
				libelleLongCorpsouContrat = corpsTemp.llCorps() + " (" + corpsTemp.code() + ")";
			} else {
				NSArray<EOContrat> contrats = getListeContrats(annee, isFormatAnneeExerciceAnneeCivile);
				if (contrats.size() > 0) {
					EOTypeContratTravail typeContratTravail = contrats.get(0).toTypeContratTravail();
					if (typeContratTravail != null) {
						libelleLongCorpsouContrat = typeContratTravail.llTypeContratTrav() + " (" + typeContratTravail.cTypeContratTrav() + ")";
					}
				} 
			}
			libelleLongCorpsouContratDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}
		return libelleLongCorpsouContrat;

	}
	
	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return Le libelle long du type de population avec le code associé entre ()
	 */
	
	public String getLibelleLongTypePopulation(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (libelleLongTypePopulationDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!libelleLongTypePopulationDejaRecherche) {
			EOCorps corpsTemp = getCorps(annee, isFormatAnneeExerciceAnneeCivile);
			if (corpsTemp != null && corpsTemp.toTypePopulation() != null ) {
				libelleLongTypePopulation = corpsTemp.toTypePopulation().llTypePopulation()
						+ " (" + corpsTemp.toTypePopulation().code() + ")";
			}
			libelleLongTypePopulationDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}
		return libelleLongTypePopulation;
	}
	
	/**
	 * Récupère le service statutaire d'un enseignant.
	 *
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return le nombre d'heure dans le service statutaire de l'enseignant.
	 */
	public double getNbHeuresServiceStatutaire(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		if (nbHeuresServiceStatutaireDejaRecherche && anneeDejaRecherchee != annee) {
			effacerCache();
		}
		if (!nbHeuresServiceStatutaireDejaRecherche) {		
			nbHeuresServiceStatutaire = 0;

			if (getCarriere(annee, isFormatAnneeExerciceAnneeCivile) != null) {
				nbHeuresServiceStatutaire = getNbHeuresCarriere(annee, isFormatAnneeExerciceAnneeCivile);
			} else {
				nbHeuresServiceStatutaire = getNbHeuresAvenantContrat(annee, isFormatAnneeExerciceAnneeCivile);
			}

			nbHeuresServiceStatutaireDejaRecherche = true;
			anneeDejaRecherchee = annee;
		}

		return nbHeuresServiceStatutaire;
	}

	/**
	 * Renvoie le nombre d'heures lié à l'élément de carrière de l'enseignant
	 * @param enseignant : enseignant statutaire titulaire
	 * @return nombre d'heures
	 */
	private double getNbHeuresCarriere(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		NSArray<EOElements> listeElementsCarriere = getElementsCarriere(annee, isFormatAnneeExerciceAnneeCivile);

		double nbHeures = 0;
		double prorata = 1;

		for (EOElements element:listeElementsCarriere) {
			prorata = OutilsValidation.prorataDatesAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile,
					element.dEffetElement(), element.dFinElement());
			nbHeures += (element.toCorps().potentielBrut()) * prorata;
		}
		return nbHeures;
	}

	/**
	 * Renvoie le nombre d'heures de l'avenant du contrat de l'enseignant
	 * @param enseignant : enseignant statutaire non titulaire
	 * @return nombre d'heures
	 */
	private double getNbHeuresAvenantContrat(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		NSArray<EOContrat> contrats = getListeContrats(annee, isFormatAnneeExerciceAnneeCivile);
		double nbHeures = 0;

		for (EOContrat contrat : contrats) {
			NSArray<EOContratAvenant> avenants = contrat.avenantsSuivantPeriode(OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile),
					OutilsValidation.getNSTFinAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile));
			if (!NSArrayCtrl.isEmpty(avenants)) {
				for (EOContratAvenant avenant : avenants)  {					
					double prorata = 1;				
					if ((contrat.dFinAnticipee() !=  null) && contrat.dFinAnticipee().after(avenant.dDebAvenant()) && contrat.dFinAnticipee().before(avenant.dFinAvenant())) {
						// si fin anticipée, on proratise
						prorata = OutilsValidation.prorataDatesSiFinAnticipe(avenant.dDebAvenant(), avenant.dFinAvenant(), contrat.dFinAnticipee());
					} else {
						// si plusieurs avenants sur la même année universitaire, on proratise
						if (avenants.size() > 1) {						
							prorata = OutilsValidation.prorataDatesAnneeUniversitaire(annee, isFormatAnneeExerciceAnneeCivile, avenant.dDebAvenant(), avenant.dFinAvenant());
						}
					}
					// si l'avenant a une durée ET
					// ( le type de contrat de travail est enseignant
					// OU le grade du contrat est doctorant enseignant 6904
					// OU l'avenant grade corps puis type de population est enseignant )					
					if (avenant.ctraDuree() != null &&
							(Constante.OUI.equals(contrat.toTypeContratTravail().temEnseignant())
									|| EOGrade.CODE_GRADE_DOC_ENS.equals(avenant.toGrade().code())
									|| (avenant.toGrade().toCorps() != null
									&& avenant.toGrade().toCorps().toTypePopulation() != null
									&& Constante.OUI.equals(avenant.toGrade().toCorps().toTypePopulation().temEnseignant())))) {
						nbHeures += (prorata * avenant.ctraDuree());
					}
				}
			} 
		}
		return nbHeures;
	}

	public boolean isStatutaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		Boolean isStatutaire = mapIsStatutairesCache.get(annee);
		if (isStatutaire == null) {
			renseignerIsTitulaireIsStatutaire(annee, isAnneeUniversitaireEnAnneeCivil);
			isStatutaire = mapIsStatutairesCache.get(annee);
		}
		return isStatutaire;
	}

	public boolean isTitulaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		Boolean isTitulaire = mapIsTitulairesCache.get(annee);
		if (isTitulaire == null) {
			renseignerIsTitulaireIsStatutaire(annee, isAnneeUniversitaireEnAnneeCivil);
			isTitulaire = mapIsTitulairesCache.get(annee);
		}
		return isTitulaire;
	}

	private void renseignerIsTitulaireIsStatutaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		EOQualifier qualifier = ERXQ.and(EOActuelEnseignant.NO_DOSSIER_PERS.eq(this.noDossierPers()),
				qualifierPourPeriode(OutilsValidation.getNSTDebutAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil)
						, OutilsValidation.getNSTFinAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil)));
		NSArray<EOActuelEnseignant> listeEnseignants = EOActuelEnseignant.fetchEOActuelEnseignants(editingContext(), qualifier, null);
		
		//TODO: à revoir lorsque l'on traitera plusieurs types de statut par année universitaire
		// pour un même enseignant
		
		if (listeEnseignants != null && listeEnseignants.size() > 0) {
			boolean isTitulaire = (listeEnseignants.get(0).temStatutaire().equals(Constante.OUI)) && (listeEnseignants.get(0).temTitulaire().equals(Constante.OUI));
			boolean isStatutaire = (listeEnseignants.get(0).temStatutaire().equals(Constante.OUI));
			mapIsStatutairesCache.put(annee, isStatutaire);
			mapIsTitulairesCache.put(annee, isTitulaire);
		} else {
			mapIsStatutairesCache.put(annee, false);
			mapIsTitulairesCache.put(annee, false);
		}
	}

	/**
	 * @param edc : editing context
	 * @return la liste des enseignants vacataires
	 */
	public static NSArray<EOActuelEnseignant> getListeEnseignantsVacataires(EOEditingContext edc) {
		return fetchEOActuelEnseignants(edc, getQualifierEnseignantsVacataires(), getTriParDefaut());
	}

	/**
	 * @return Le tri par défaut de la liste
	 */
	public static ERXSortOrderings getTriParDefaut() {		
		return EOActuelEnseignant.TO_INDIVIDU.dot(EOIndividu.NOM_AFFICHAGE_KEY).asc().then(EOActuelEnseignant.TO_INDIVIDU.dot(EOIndividu.PRENOM_AFFICHAGE_KEY).asc());
	}

	/**
	 * @return Qualifier pour les enseignants vacataires
	 */
	public static EOQualifier getQualifierEnseignantsVacataires() {
		return EOActuelEnseignant.TEM_STATUTAIRE.eq(Constante.NON);
	}

	/**
	 * @return le statut de l'enseignant : Statutaire ou Vacataire
	 */
	public String getStatut(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		String statut = null;

		if (isStatutaire(annee, isAnneeUniversitaireEnAnneeCivil)) {
			statut = Constante.STATUTAIRE;
		} else {
			statut = Constante.VACATAIRE;
		}

		return statut;
	}


	/**
	 * @param annee : année en cours
	 * @param isFormatAnneeExerciceAnneeCivile : année civile?
	 * @return le libelle du moteur de calcul utilisé
	 */
	public String getMethodeCalcul(Integer annee, boolean isFormatAnneeExerciceAnneeCivile) {
		MoteurDeCalcul moteur = new MoteurDeCalcul(editingContext(), this, annee, isFormatAnneeExerciceAnneeCivile);
		return moteur.getLibelleMethodeDeCalcul();
	}

	/**
	 * Renvoie le service de l'enseignant pour une année donnée
	 * @param annee : année en cours
	 * @return le service 
	 */
	public EOService getService(Integer annee) {
		EOService service = mapServicesCache.get(annee);		
		if (service == null)  {
			EOQualifier qualifier = 
					ERXQ.and(
							ERXQ.equals(EOService.ANNEE_KEY, annee),
							ERXQ.equals(EOService.ENSEIGNANT_KEY, this)
							);
			service = EOService.fetchEOService(this.editingContext(), qualifier);
			mapServicesCache.put(annee, service);			
		}
		return service;
	}

	/**
	 * Renvoie la fiche de voeux de l'enseignant pour une année donnée
	 * @param annee : année en cours
	 * @return la fiche de voeux 
	 */
	public EOFicheVoeux getFicheVoeux(Integer annee) {
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOFicheVoeux.ANNEE_KEY, annee),
						ERXQ.equals(EOFicheVoeux.ENSEIGNANT_KEY, this)
						);

		return EOFicheVoeux.fetchEOFicheVoeux(this.editingContext(), qualifier);
	}

	public NSArray<EOContrat> getListeContrats() {
		return listeContrats;
	}

	/**
	 * Renvoie l'état de la fiche de voeux
	 * @param annee annee
	 * @return état de la fiche de voeux
	 */
	public EtapePeche getEtapePecheFicheVoeux(Integer annee) {

		EOFicheVoeux ficheVoeux = getFicheVoeux(annee);

		if (ficheVoeux != null) {
			EOEtape etape = ficheVoeux.toDemande().toEtape();
			return EtapePeche.getEtape(etape);
		}

		return null;
	}

	public String getEtatFicheVoeux() {
		return etatFicheVoeux;
	}

	public void setEtatFicheVoeux(String etatFicheVoeux) {
		this.etatFicheVoeux = etatFicheVoeux;
	}

	public String getEtatFiche() {
		return etatFiche;
	}

	public void setEtatFiche(String etatFiche) {
		this.etatFiche = etatFiche;
	}

	public EOQualifier qualifierPourPeriode(NSTimestamp debutPeriode, NSTimestamp finPeriode) {
		NSTimestamp fin = null;
		if (finPeriode != null) {
			fin = finPeriode;
		}
		return GRHUtilities.qualifierPourPeriode(EOActuelEnseignant.DATE_DEBUT_KEY, debutPeriode, EOActuelEnseignant.DATE_FIN_KEY, fin);
	}

	/**
	 * Raprochement d'un enseignant avec un enseignant générique.
	 * @return <code>true</code> si le rapprochement a été fait
	 */

	public boolean rapprocherEnseignant(EOService serviceEnseignantGenerique,IEnseignantsCtrl controleur, Integer annee, Session pecheSession, boolean isEnseignantStatutaire) {

		EOService serviceEnseignant = getService(annee);

		// Est-ce que la fiche de service de l'enseignant est à l'état initialisée (pas de fiche ou première étape du circuit prévisionnel) ?
		boolean isServiceEnseignantEtatInitialisee = true;
		boolean isServiceEnseignantAServiceAttribue = false;

		if (isEnseignantStatutaire && (serviceEnseignant != null)) {
			isServiceEnseignantEtatInitialisee = controleur.getDemande(serviceEnseignant).toEtape().isInitiale() 
					&& controleur.getDemande(serviceEnseignant).toEtape().toCircuitValidation().codeCircuitValidation().equals(CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit());
			isServiceEnseignantAServiceAttribue = !serviceEnseignant.listeServiceDetails().isEmpty();
		}

		if (!isEnseignantStatutaire && (serviceEnseignant != null)) {
			NSArray<EORepartService> listeRepartService = serviceEnseignant.toListeRepartService();
			for (EORepartService repartService : listeRepartService) {
				if (repartService.toListeServiceDetail() != null && repartService.toListeServiceDetail().size() > 0) {
					if (isServiceEnseignantEtatInitialisee) {
						isServiceEnseignantEtatInitialisee = controleur.getDemande(serviceEnseignant).toEtape().isInitiale() 
								&& controleur.getDemande(serviceEnseignant).toEtape().toCircuitValidation().codeCircuitValidation().equals(CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit());
					}
					if (!isServiceEnseignantAServiceAttribue) {
						isServiceEnseignantAServiceAttribue = !serviceEnseignant.listeServiceDetails().isEmpty();
					}

				}
			}
		}

		boolean isServiceEnseignantGeneriqueAServiceAttribue = false;

		if (serviceEnseignantGenerique != null) {
			isServiceEnseignantGeneriqueAServiceAttribue = !serviceEnseignantGenerique.listeServiceDetails().isEmpty();
		}

		if (!isServiceEnseignantEtatInitialisee) {
			pecheSession.addSimpleMessage(TypeMessage.ERREUR, "La fiche de service de l'enseignant à déjà été validée");
			return false;
		}

		//si l'enseignent générique à été créé a partir de grhum
		if (serviceEnseignantGenerique.toEnseignantGenerique().persID() != null) {
			if (serviceEnseignantGenerique.toEnseignantGenerique().persID().intValue() != toIndividu().persId().intValue()) {
				pecheSession.addSimpleMessage(TypeMessage.ERREUR, "L'enseignant et l'enseignant générique n'ont pas le même persID");
				return false;
			}
		}

		if (isServiceEnseignantAServiceAttribue && isServiceEnseignantGeneriqueAServiceAttribue) {
			pecheSession.addSimpleMessage(TypeMessage.ERREUR, "L'enseignant et l'enseignant générique ont tous les deux un service attribué. Le rapprochement ne peut pas être fait.");
			return false;
		}

		if (!isServiceEnseignantGeneriqueAServiceAttribue) {
			pecheSession.addSimpleMessage(TypeMessage.ERREUR, "L'enseignant générique n'a pas de service associé. Aucun service attribué n'a été copié.");
			return false;
		}


		EOPersonne personne = pecheSession.getApplicationUser().getPersonne();

		// On crée la fiche de service de l'enseignant si nécessaire
		if (serviceEnseignant == null) {
			serviceEnseignant = EOService.creerEtInitialiser(this.editingContext());
			serviceEnseignant.setEnseignant(this);
			serviceEnseignant.setAnnee(annee);
			serviceEnseignant.setTemoinEnseignantGenerique("N");
			serviceEnseignant.setTemoinValide("O");
			serviceEnseignant.majDonnesAuditCreation(personne);
		}

		if (isEnseignantStatutaire) {
			EORepartService repartServiceEnseignant = null;

			if (serviceEnseignant.toListeRepartService().size() == 0) {
				repartServiceEnseignant = EORepartService.creerNouvelRepartService(this.editingContext(), serviceEnseignant, null, personne, CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit(), pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
			} else {
				repartServiceEnseignant = serviceEnseignant.toListeRepartService().get(0);
			}

			// On copie les services attribuée dans la fiche de service de l'enseignant
			for (EOServiceDetail serviceDetail : serviceEnseignantGenerique.listeServiceDetails()) {			
				EOServiceDetail serviceDetailEnseignant = EOServiceDetail.dupliquer(serviceDetail, this.editingContext());
				serviceDetailEnseignant.setServiceRelationship(serviceEnseignant);
				serviceDetailEnseignant.majDonnesAuditCreation(personne);
				serviceDetailEnseignant.setToRepartService(repartServiceEnseignant);
			}
		} else {
			// VACATAIRES
			// on supprime les repartService de l'enseignant cible
			if (!isEnseignantStatutaire && (serviceEnseignant != null)) {
				NSArray<EORepartService> listeRepartService = serviceEnseignant.toListeRepartService();
				for (EORepartService repartService : listeRepartService) {
					if (repartService.toListeServiceDetail() != null) {
						repartService.delete();
					}
				}
			}

			// on ajoute ceux du générique
			for (EORepartService repartServiceEnseignantGenerique : serviceEnseignantGenerique.toListeRepartService()) {
				EORepartService repartServiceEnseignant = EORepartService.creerNouvelRepartService(this.editingContext(), serviceEnseignant, repartServiceEnseignantGenerique.toStructure(), personne, Constante.CIRCUIT_VACATAIRE_A_DETERMINER, pecheSession.getPecheParametres().getAnneeUniversitaireEnCours());
				// On copie les services attribuée dans la fiche de service de l'enseignant
				for (EOServiceDetail serviceDetail : repartServiceEnseignantGenerique.toListeServiceDetail()) {					
					EOServiceDetail serviceDetailEnseignant = EOServiceDetail.dupliquer(serviceDetail, this.editingContext());
					serviceDetailEnseignant.setServiceRelationship(serviceEnseignant);
					serviceDetailEnseignant.majDonnesAuditCreation(personne);
					serviceDetailEnseignant.setToRepartService(repartServiceEnseignant);
				}	

			}
		}


		serviceEnseignant.majDonnesServiceReparti(this.editingContext(), personne, pecheSession.getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		for (EOServiceDetail serviceDetail : serviceEnseignantGenerique.listeServiceDetails()) {
			serviceDetail.delete();
		}

		for (EORepartService repartService : serviceEnseignantGenerique.toListeRepartService()) {
			repartService.delete();
		}

		//On supprime l'enseignant générique
		EOEnseignantGenerique enseignantSelectionne = serviceEnseignantGenerique.toEnseignantGenerique();
		serviceEnseignantGenerique.setToEnseignantGenerique(null);
		serviceEnseignantGenerique.delete();  
		this.editingContext().saveChanges();
		enseignantSelectionne.delete();

		this.editingContext().saveChanges();

		pecheSession.addSimpleInfoMessage("Succès", "L'enseignant a été rapproché. L'enseignant générique a été supprimé.");

		return true;
	}
	
	/**
	 * @return la structure de l'enseignant statutaire
	 */
	private IStructure getStructureStatutaire() {
		IStructure structure = getDepartementEnseignement();
		
		if (structure == null) {
			structure = getComposante();
		}
		
		return structure;
	}

	/**
	 * {@inheritDoc}
	 */
	public IStructure getStructure(Integer annee, boolean isAnneeUnivEnAnneeCivil) {
		IStructure structure = null;

		if (isStatutaire(annee, isAnneeUnivEnAnneeCivil)) {
			structure = getStructureStatutaire();
		}
		
		//TODO : ajouter structure pour les vacataires quand affectation sera gérée
		return structure;
	}

}
