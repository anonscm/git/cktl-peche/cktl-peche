package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Cette classe représente la répartition croisée des enseignants d'un répartiteur
 * affectés dans une autre composante que celle d'affectation
 * 
 * @author Chama LAATIK
 */
public class EORepartEnseignant extends _EORepartEnseignant {
	
	private static final long serialVersionUID = 4516405222368634180L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartEnseignant.class);
	
	/**
	 * Crée et initialise une répartition croisée.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return une répartition croisée.
	 */
	public static EORepartEnseignant creerEtInitialiser(EOEditingContext edc) {
		EORepartEnseignant repartition = new EORepartEnseignant();
		edc.insertObject(repartition);

		return repartition;
	}
	
	/**
	 * Permet de récupérer la répartition d'un service detail en particulier
	 * @param edc : editing context
	 * @param serviceDetail : le détail du service pour lequel on recherche ue répartition
	 * @return une répartition
	 */
	public static EORepartEnseignant rechercherServiceDetail(EOEditingContext edc, EOServiceDetail serviceDetail) {
		EOQualifier qualifier = ERXQ.equals(SERVICE_DETAIL_KEY, serviceDetail);
		
		return fetchEORepartEnseignant(edc, qualifier);
	}
	
	/**
	 * Crée nouvelle répartition
	 * @param serviceDetail : détail du service
	 * @param repartiteurDemandeur : répartiteur qui fait la demande de l'enseignant
	 * @param composanteReferent : composante de l'enseignant
	 * @param personneConnecte : répartiteur connecté
	 * @param edc : editingContext
	 * @return nouvelle repartition
	 */
	public static EORepartEnseignant creerNouvelleRepartition(EOServiceDetail serviceDetail, EOIndividu repartiteurDemandeur, 
			IStructure composanteReferent, EOPersonne personneConnecte, EOEditingContext edc) {
		NSTimestamp now = new NSTimestamp();
		EORepartEnseignant repartition = EORepartEnseignant.creerEtInitialiser(edc);
		
		repartition.setPersonneCreation(personneConnecte);
		repartition.setDateCreation(now);
		repartition.setPersonneModification(personneConnecte);
		repartition.setDateModification(now);
		repartition.setRepartiteurDemandeur(repartiteurDemandeur);
		repartition.setComposanteReferente((EOStructure) composanteReferent);
		repartition.setServiceDetail(serviceDetail);
		return repartition;
	}
	
	/**
	 * Supprime une répartition si elle en existe une pour le détail de service recherché
	 * @param serviceDetail : détail de service
	 * @param edc : editingContext
	 */
	public static void supprimerRepartition(EOServiceDetail serviceDetail, EOEditingContext edc) {
		if ((serviceDetail != null) && (serviceDetail.etat() != null)) {
			EORepartEnseignant repartition = EORepartEnseignant.rechercherServiceDetail(edc, serviceDetail);
			if (repartition != null) {
				edc.deleteObject(repartition);
				edc.saveChanges();
			}
		}
	}
}
