package org.cocktail.peche.entity;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente la table "MISE_EN_PAIEMENT".
 * 
 * @author Pascal MACOUIN
 */
public class EOMiseEnPaiement extends _EOMiseEnPaiement {
	/** Numéro de série. */
	private static final long serialVersionUID = 3465607466460857345L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOMiseEnPaiement.class);

	/**
	 * Créer et initialiser une instance de mise en paiement.
	 * 
	 * @param editingContext un editing context
	 * @param service un service
	 * @param heuresAPayer les heures à payer
	 * @param heuresAPayerHetd les heures à payer (en HETD)
	 * @param tauxBrut le taux brut appliqué
	 * @param tauxCharge le taux chargé appliqué
	 * @param personne la personne qui crée cette instance
	 * @return l'instance de mise en paiement nouvellement créée
	 */
	public static EOMiseEnPaiement creerEtInitialiser(EOEditingContext editingContext, EOService service, BigDecimal heuresAPayer, BigDecimal heuresAPayerHetd, BigDecimal tauxBrut, BigDecimal tauxCharge, EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		EOMiseEnPaiement miseEnPaiement = EOMiseEnPaiement.createEOMiseEnPaiement(service.editingContext(), now, now, heuresAPayer, heuresAPayerHetd, tauxBrut, tauxCharge, personne, personne, service);
		return miseEnPaiement;
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);
	}

	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		setDateModification(new NSTimestamp());
		setToPersonneModificationRelationship(personne);
	}
}
