// DO NOT EDIT.  Make changes to EOFluxMiseEnPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOFluxMiseEnPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOFluxMiseEnPaiement";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> BRUT_PAYE = new ERXKey<java.math.BigDecimal>("brutPaye");
  public static final ERXKey<Integer> CLE_INSEE = new ERXKey<Integer>("cleInsee");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> HEURES_PAYEES = new ERXKey<java.math.BigDecimal>("heuresPayees");
  public static final ERXKey<String> MOIS_SUIVANTPOUR_TG = new ERXKey<String>("moisSuivantpourTG");
  public static final ERXKey<String> NOM = new ERXKey<String>("nom");
  public static final ERXKey<String> NUMERO_INSEE = new ERXKey<String>("numeroInsee");
  public static final ERXKey<String> NUMERO_PRISE_EN_CHARGE = new ERXKey<String>("numeroPriseEnCharge");
  public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
  public static final ERXKey<java.math.BigDecimal> TAUX_NON_CHARGE = new ERXKey<java.math.BigDecimal>("tauxNonCharge");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOPaiement>("toPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");
  public static final ERXKey<org.cocktail.peche.entity.EOService> TO_SERVICE = new ERXKey<org.cocktail.peche.entity.EOService>("toService");

  // Attributes
  public static final String BRUT_PAYE_KEY = BRUT_PAYE.key();
  public static final String CLE_INSEE_KEY = CLE_INSEE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String HEURES_PAYEES_KEY = HEURES_PAYEES.key();
  public static final String MOIS_SUIVANTPOUR_TG_KEY = MOIS_SUIVANTPOUR_TG.key();
  public static final String NOM_KEY = NOM.key();
  public static final String NUMERO_INSEE_KEY = NUMERO_INSEE.key();
  public static final String NUMERO_PRISE_EN_CHARGE_KEY = NUMERO_PRISE_EN_CHARGE.key();
  public static final String PRENOM_KEY = PRENOM.key();
  public static final String TAUX_NON_CHARGE_KEY = TAUX_NON_CHARGE.key();
  // Relationships
  public static final String TO_PAIEMENT_KEY = TO_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();
  public static final String TO_SERVICE_KEY = TO_SERVICE.key();

  private static Logger LOG = Logger.getLogger(_EOFluxMiseEnPaiement.class);

  public EOFluxMiseEnPaiement localInstanceIn(EOEditingContext editingContext) {
    EOFluxMiseEnPaiement localInstance = (EOFluxMiseEnPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal brutPaye() {
    return (java.math.BigDecimal) storedValueForKey(_EOFluxMiseEnPaiement.BRUT_PAYE_KEY);
  }

  public void setBrutPaye(java.math.BigDecimal value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating brutPaye from " + brutPaye() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.BRUT_PAYE_KEY);
  }

  public Integer cleInsee() {
    return (Integer) storedValueForKey(_EOFluxMiseEnPaiement.CLE_INSEE_KEY);
  }

  public void setCleInsee(Integer value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating cleInsee from " + cleInsee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.CLE_INSEE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOFluxMiseEnPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOFluxMiseEnPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal heuresPayees() {
    return (java.math.BigDecimal) storedValueForKey(_EOFluxMiseEnPaiement.HEURES_PAYEES_KEY);
  }

  public void setHeuresPayees(java.math.BigDecimal value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating heuresPayees from " + heuresPayees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.HEURES_PAYEES_KEY);
  }

  public String moisSuivantpourTG() {
    return (String) storedValueForKey(_EOFluxMiseEnPaiement.MOIS_SUIVANTPOUR_TG_KEY);
  }

  public void setMoisSuivantpourTG(String value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating moisSuivantpourTG from " + moisSuivantpourTG() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.MOIS_SUIVANTPOUR_TG_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(_EOFluxMiseEnPaiement.NOM_KEY);
  }

  public void setNom(String value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.NOM_KEY);
  }

  public String numeroInsee() {
    return (String) storedValueForKey(_EOFluxMiseEnPaiement.NUMERO_INSEE_KEY);
  }

  public void setNumeroInsee(String value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating numeroInsee from " + numeroInsee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.NUMERO_INSEE_KEY);
  }

  public String numeroPriseEnCharge() {
    return (String) storedValueForKey(_EOFluxMiseEnPaiement.NUMERO_PRISE_EN_CHARGE_KEY);
  }

  public void setNumeroPriseEnCharge(String value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating numeroPriseEnCharge from " + numeroPriseEnCharge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.NUMERO_PRISE_EN_CHARGE_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(_EOFluxMiseEnPaiement.PRENOM_KEY);
  }

  public void setPrenom(String value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.PRENOM_KEY);
  }

  public java.math.BigDecimal tauxNonCharge() {
    return (java.math.BigDecimal) storedValueForKey(_EOFluxMiseEnPaiement.TAUX_NON_CHARGE_KEY);
  }

  public void setTauxNonCharge(java.math.BigDecimal value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOFluxMiseEnPaiement.LOG.debug( "updating tauxNonCharge from " + tauxNonCharge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.TAUX_NON_CHARGE_KEY);
  }

  public org.cocktail.peche.entity.EOPaiement toPaiement() {
    return (org.cocktail.peche.entity.EOPaiement)storedValueForKey(_EOFluxMiseEnPaiement.TO_PAIEMENT_KEY);
  }
  
  public void setToPaiement(org.cocktail.peche.entity.EOPaiement value) {
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.TO_PAIEMENT_KEY);
  }

  public void setToPaiementRelationship(org.cocktail.peche.entity.EOPaiement value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOFluxMiseEnPaiement.LOG.debug("updating toPaiement from " + toPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOPaiement oldValue = toPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFluxMiseEnPaiement.TO_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFluxMiseEnPaiement.TO_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFluxMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOFluxMiseEnPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFluxMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFluxMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFluxMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOFluxMiseEnPaiement.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFluxMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFluxMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOService toService() {
    return (org.cocktail.peche.entity.EOService)storedValueForKey(_EOFluxMiseEnPaiement.TO_SERVICE_KEY);
  }
  
  public void setToService(org.cocktail.peche.entity.EOService value) {
    takeStoredValueForKey(value, _EOFluxMiseEnPaiement.TO_SERVICE_KEY);
  }

  public void setToServiceRelationship(org.cocktail.peche.entity.EOService value) {
    if (_EOFluxMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOFluxMiseEnPaiement.LOG.debug("updating toService from " + toService() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOService oldValue = toService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFluxMiseEnPaiement.TO_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFluxMiseEnPaiement.TO_SERVICE_KEY);
    }
  }
  

  public static EOFluxMiseEnPaiement createEOFluxMiseEnPaiement(EOEditingContext editingContext, java.math.BigDecimal brutPaye
, NSTimestamp dateCreation
, NSTimestamp dateModification
, java.math.BigDecimal heuresPayees
, String moisSuivantpourTG
, String nom
, String prenom
, java.math.BigDecimal tauxNonCharge
, org.cocktail.peche.entity.EOPaiement toPaiement, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification, org.cocktail.peche.entity.EOService toService) {
    EOFluxMiseEnPaiement eo = (EOFluxMiseEnPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOFluxMiseEnPaiement.ENTITY_NAME);    
		eo.setBrutPaye(brutPaye);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setHeuresPayees(heuresPayees);
		eo.setMoisSuivantpourTG(moisSuivantpourTG);
		eo.setNom(nom);
		eo.setPrenom(prenom);
		eo.setTauxNonCharge(tauxNonCharge);
    eo.setToPaiementRelationship(toPaiement);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    eo.setToServiceRelationship(toService);
    return eo;
  }

  public static ERXFetchSpecification<EOFluxMiseEnPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOFluxMiseEnPaiement>(_EOFluxMiseEnPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOFluxMiseEnPaiement> fetchAllEOFluxMiseEnPaiements(EOEditingContext editingContext) {
    return _EOFluxMiseEnPaiement.fetchAllEOFluxMiseEnPaiements(editingContext, null);
  }

  public static NSArray<EOFluxMiseEnPaiement> fetchAllEOFluxMiseEnPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFluxMiseEnPaiement.fetchEOFluxMiseEnPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFluxMiseEnPaiement> fetchEOFluxMiseEnPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOFluxMiseEnPaiement> fetchSpec = new ERXFetchSpecification<EOFluxMiseEnPaiement>(_EOFluxMiseEnPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFluxMiseEnPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOFluxMiseEnPaiement fetchEOFluxMiseEnPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFluxMiseEnPaiement.fetchEOFluxMiseEnPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFluxMiseEnPaiement fetchEOFluxMiseEnPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFluxMiseEnPaiement> eoObjects = _EOFluxMiseEnPaiement.fetchEOFluxMiseEnPaiements(editingContext, qualifier, null);
    EOFluxMiseEnPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOFluxMiseEnPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFluxMiseEnPaiement fetchRequiredEOFluxMiseEnPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFluxMiseEnPaiement.fetchRequiredEOFluxMiseEnPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFluxMiseEnPaiement fetchRequiredEOFluxMiseEnPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFluxMiseEnPaiement eoObject = _EOFluxMiseEnPaiement.fetchEOFluxMiseEnPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOFluxMiseEnPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFluxMiseEnPaiement localInstanceIn(EOEditingContext editingContext, EOFluxMiseEnPaiement eo) {
    EOFluxMiseEnPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
