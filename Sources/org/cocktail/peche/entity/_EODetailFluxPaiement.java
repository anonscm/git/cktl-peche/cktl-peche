// DO NOT EDIT.  Make changes to EODetailFluxPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODetailFluxPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EODetailFluxPaiement";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> VALEUR_CHAMP = new ERXKey<String>("valeurChamp");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EODefinitionFluxPaiement> TO_DEFINITION_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EODefinitionFluxPaiement>("toDefinitionFluxPaiement");
  public static final ERXKey<org.cocktail.peche.entity.EOParametresFluxPaiement> TO_PARAMETRES_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOParametresFluxPaiement>("toParametresFluxPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String VALEUR_CHAMP_KEY = VALEUR_CHAMP.key();
  // Relationships
  public static final String TO_DEFINITION_FLUX_PAIEMENT_KEY = TO_DEFINITION_FLUX_PAIEMENT.key();
  public static final String TO_PARAMETRES_FLUX_PAIEMENT_KEY = TO_PARAMETRES_FLUX_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EODetailFluxPaiement.class);

  public EODetailFluxPaiement localInstanceIn(EOEditingContext editingContext) {
    EODetailFluxPaiement localInstance = (EODetailFluxPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EODetailFluxPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
    	_EODetailFluxPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EODetailFluxPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EODetailFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
    	_EODetailFluxPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EODetailFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public String valeurChamp() {
    return (String) storedValueForKey(_EODetailFluxPaiement.VALEUR_CHAMP_KEY);
  }

  public void setValeurChamp(String value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
    	_EODetailFluxPaiement.LOG.debug( "updating valeurChamp from " + valeurChamp() + " to " + value);
    }
    takeStoredValueForKey(value, _EODetailFluxPaiement.VALEUR_CHAMP_KEY);
  }

  public org.cocktail.peche.entity.EODefinitionFluxPaiement toDefinitionFluxPaiement() {
    return (org.cocktail.peche.entity.EODefinitionFluxPaiement)storedValueForKey(_EODetailFluxPaiement.TO_DEFINITION_FLUX_PAIEMENT_KEY);
  }
  
  public void setToDefinitionFluxPaiement(org.cocktail.peche.entity.EODefinitionFluxPaiement value) {
    takeStoredValueForKey(value, _EODetailFluxPaiement.TO_DEFINITION_FLUX_PAIEMENT_KEY);
  }

  public void setToDefinitionFluxPaiementRelationship(org.cocktail.peche.entity.EODefinitionFluxPaiement value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
      _EODetailFluxPaiement.LOG.debug("updating toDefinitionFluxPaiement from " + toDefinitionFluxPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDefinitionFluxPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EODefinitionFluxPaiement oldValue = toDefinitionFluxPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODetailFluxPaiement.TO_DEFINITION_FLUX_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODetailFluxPaiement.TO_DEFINITION_FLUX_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOParametresFluxPaiement toParametresFluxPaiement() {
    return (org.cocktail.peche.entity.EOParametresFluxPaiement)storedValueForKey(_EODetailFluxPaiement.TO_PARAMETRES_FLUX_PAIEMENT_KEY);
  }
  
  public void setToParametresFluxPaiement(org.cocktail.peche.entity.EOParametresFluxPaiement value) {
    takeStoredValueForKey(value, _EODetailFluxPaiement.TO_PARAMETRES_FLUX_PAIEMENT_KEY);
  }

  public void setToParametresFluxPaiementRelationship(org.cocktail.peche.entity.EOParametresFluxPaiement value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
      _EODetailFluxPaiement.LOG.debug("updating toParametresFluxPaiement from " + toParametresFluxPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToParametresFluxPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOParametresFluxPaiement oldValue = toParametresFluxPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODetailFluxPaiement.TO_PARAMETRES_FLUX_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODetailFluxPaiement.TO_PARAMETRES_FLUX_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EODetailFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EODetailFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
      _EODetailFluxPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODetailFluxPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODetailFluxPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EODetailFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EODetailFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EODetailFluxPaiement.LOG.isDebugEnabled()) {
      _EODetailFluxPaiement.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODetailFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODetailFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EODetailFluxPaiement createEODetailFluxPaiement(EOEditingContext editingContext, NSTimestamp dateCreation
, org.cocktail.peche.entity.EODefinitionFluxPaiement toDefinitionFluxPaiement, org.cocktail.peche.entity.EOParametresFluxPaiement toParametresFluxPaiement, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation) {
    EODetailFluxPaiement eo = (EODetailFluxPaiement) EOUtilities.createAndInsertInstance(editingContext, _EODetailFluxPaiement.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
    eo.setToDefinitionFluxPaiementRelationship(toDefinitionFluxPaiement);
    eo.setToParametresFluxPaiementRelationship(toParametresFluxPaiement);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EODetailFluxPaiement> fetchSpec() {
    return new ERXFetchSpecification<EODetailFluxPaiement>(_EODetailFluxPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODetailFluxPaiement> fetchAllEODetailFluxPaiements(EOEditingContext editingContext) {
    return _EODetailFluxPaiement.fetchAllEODetailFluxPaiements(editingContext, null);
  }

  public static NSArray<EODetailFluxPaiement> fetchAllEODetailFluxPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODetailFluxPaiement.fetchEODetailFluxPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EODetailFluxPaiement> fetchEODetailFluxPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODetailFluxPaiement> fetchSpec = new ERXFetchSpecification<EODetailFluxPaiement>(_EODetailFluxPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODetailFluxPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODetailFluxPaiement fetchEODetailFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailFluxPaiement.fetchEODetailFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODetailFluxPaiement fetchEODetailFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODetailFluxPaiement> eoObjects = _EODetailFluxPaiement.fetchEODetailFluxPaiements(editingContext, qualifier, null);
    EODetailFluxPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EODetailFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODetailFluxPaiement fetchRequiredEODetailFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailFluxPaiement.fetchRequiredEODetailFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODetailFluxPaiement fetchRequiredEODetailFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EODetailFluxPaiement eoObject = _EODetailFluxPaiement.fetchEODetailFluxPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EODetailFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODetailFluxPaiement localInstanceIn(EOEditingContext editingContext, EODetailFluxPaiement eo) {
    EODetailFluxPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
