// DO NOT EDIT.  Make changes to EOService.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOService extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOService";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_COMMENTAIRE = new ERXKey<NSTimestamp>("dateCommentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_PAYER = new ERXKey<java.math.BigDecimal>("heuresAPayer");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_REPORTER_ATTRIBUEES = new ERXKey<java.math.BigDecimal>("heuresAReporterAttribuees");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_REPORTER_SOUHAITEES = new ERXKey<java.math.BigDecimal>("heuresAReporterSouhaitees");
  public static final ERXKey<java.math.BigDecimal> HEURES_GRATUITES = new ERXKey<java.math.BigDecimal>("heuresGratuites");
  public static final ERXKey<java.math.BigDecimal> HEURES_PAYEES = new ERXKey<java.math.BigDecimal>("heuresPayees");
  public static final ERXKey<java.math.BigDecimal> HEURES_SERVICE = new ERXKey<java.math.BigDecimal>("heuresService");
  public static final ERXKey<java.math.BigDecimal> HEURES_SERVICE_ATTRIBUE = new ERXKey<java.math.BigDecimal>("heuresServiceAttribue");
  public static final ERXKey<java.math.BigDecimal> HEURES_SERVICE_DU = new ERXKey<java.math.BigDecimal>("heuresServiceDu");
  public static final ERXKey<java.math.BigDecimal> HEURES_SERVICE_REALISE = new ERXKey<java.math.BigDecimal>("heuresServiceRealise");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> ID_ENSEIGNANT_GENERIQUE = new ERXKey<Integer>("idEnseignantGenerique");
  public static final ERXKey<String> SUIVI = new ERXKey<String>("suivi");
  public static final ERXKey<String> TEMOIN_ENSEIGNANT_GENERIQUE = new ERXKey<String>("temoinEnseignantGenerique");
  public static final ERXKey<String> TEMOIN_VALIDE = new ERXKey<String>("temoinValide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOActuelEnseignant> ENSEIGNANT = new ERXKey<org.cocktail.peche.entity.EOActuelEnseignant>("enseignant");
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> LISTE_SERVICE_DETAILS = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("listeServiceDetails");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TO_DEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("toDemande");
  public static final ERXKey<org.cocktail.peche.entity.EOEnseignantGenerique> TO_ENSEIGNANT_GENERIQUE = new ERXKey<org.cocktail.peche.entity.EOEnseignantGenerique>("toEnseignantGenerique");
  public static final ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement> TO_LISTE_MISES_EN_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement>("toListeMisesEnPaiement");
  public static final ERXKey<org.cocktail.peche.entity.EORepartService> TO_LISTE_REPART_SERVICE = new ERXKey<org.cocktail.peche.entity.EORepartService>("toListeRepartService");

  // Attributes
  public static final String ANNEE_KEY = ANNEE.key();
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_COMMENTAIRE_KEY = DATE_COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String HEURES_A_PAYER_KEY = HEURES_A_PAYER.key();
  public static final String HEURES_A_REPORTER_ATTRIBUEES_KEY = HEURES_A_REPORTER_ATTRIBUEES.key();
  public static final String HEURES_A_REPORTER_SOUHAITEES_KEY = HEURES_A_REPORTER_SOUHAITEES.key();
  public static final String HEURES_GRATUITES_KEY = HEURES_GRATUITES.key();
  public static final String HEURES_PAYEES_KEY = HEURES_PAYEES.key();
  public static final String HEURES_SERVICE_KEY = HEURES_SERVICE.key();
  public static final String HEURES_SERVICE_ATTRIBUE_KEY = HEURES_SERVICE_ATTRIBUE.key();
  public static final String HEURES_SERVICE_DU_KEY = HEURES_SERVICE_DU.key();
  public static final String HEURES_SERVICE_REALISE_KEY = HEURES_SERVICE_REALISE.key();
  public static final String ID_KEY = ID.key();
  public static final String ID_ENSEIGNANT_GENERIQUE_KEY = ID_ENSEIGNANT_GENERIQUE.key();
  public static final String SUIVI_KEY = SUIVI.key();
  public static final String TEMOIN_ENSEIGNANT_GENERIQUE_KEY = TEMOIN_ENSEIGNANT_GENERIQUE.key();
  public static final String TEMOIN_VALIDE_KEY = TEMOIN_VALIDE.key();
  // Relationships
  public static final String ENSEIGNANT_KEY = ENSEIGNANT.key();
  public static final String LISTE_SERVICE_DETAILS_KEY = LISTE_SERVICE_DETAILS.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_DEMANDE_KEY = TO_DEMANDE.key();
  public static final String TO_ENSEIGNANT_GENERIQUE_KEY = TO_ENSEIGNANT_GENERIQUE.key();
  public static final String TO_LISTE_MISES_EN_PAIEMENT_KEY = TO_LISTE_MISES_EN_PAIEMENT.key();
  public static final String TO_LISTE_REPART_SERVICE_KEY = TO_LISTE_REPART_SERVICE.key();

  private static Logger LOG = Logger.getLogger(_EOService.class);

  public EOService localInstanceIn(EOEditingContext editingContext) {
    EOService localInstance = (EOService)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer annee() {
    return (Integer) storedValueForKey(_EOService.ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating annee from " + annee() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.ANNEE_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOService.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCommentaire() {
    return (NSTimestamp) storedValueForKey(_EOService.DATE_COMMENTAIRE_KEY);
  }

  public void setDateCommentaire(NSTimestamp value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating dateCommentaire from " + dateCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.DATE_COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOService.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOService.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal heuresAPayer() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_A_PAYER_KEY);
  }

  public void setHeuresAPayer(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresAPayer from " + heuresAPayer() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_A_PAYER_KEY);
  }

  public java.math.BigDecimal heuresAReporterAttribuees() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_A_REPORTER_ATTRIBUEES_KEY);
  }

  public void setHeuresAReporterAttribuees(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresAReporterAttribuees from " + heuresAReporterAttribuees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_A_REPORTER_ATTRIBUEES_KEY);
  }

  public java.math.BigDecimal heuresAReporterSouhaitees() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_A_REPORTER_SOUHAITEES_KEY);
  }

  public void setHeuresAReporterSouhaitees(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresAReporterSouhaitees from " + heuresAReporterSouhaitees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_A_REPORTER_SOUHAITEES_KEY);
  }

  public java.math.BigDecimal heuresGratuites() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_GRATUITES_KEY);
  }

  public void setHeuresGratuites(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresGratuites from " + heuresGratuites() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_GRATUITES_KEY);
  }

  public java.math.BigDecimal heuresPayees() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_PAYEES_KEY);
  }

  public void setHeuresPayees(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresPayees from " + heuresPayees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_PAYEES_KEY);
  }

  public java.math.BigDecimal heuresService() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_SERVICE_KEY);
  }

  public void setHeuresService(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresService from " + heuresService() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_SERVICE_KEY);
  }

  public java.math.BigDecimal heuresServiceAttribue() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_SERVICE_ATTRIBUE_KEY);
  }

  public void setHeuresServiceAttribue(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresServiceAttribue from " + heuresServiceAttribue() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_SERVICE_ATTRIBUE_KEY);
  }

  public java.math.BigDecimal heuresServiceDu() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_SERVICE_DU_KEY);
  }

  public void setHeuresServiceDu(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresServiceDu from " + heuresServiceDu() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_SERVICE_DU_KEY);
  }

  public java.math.BigDecimal heuresServiceRealise() {
    return (java.math.BigDecimal) storedValueForKey(_EOService.HEURES_SERVICE_REALISE_KEY);
  }

  public void setHeuresServiceRealise(java.math.BigDecimal value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating heuresServiceRealise from " + heuresServiceRealise() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.HEURES_SERVICE_REALISE_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOService.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.ID_KEY);
  }

  public Integer idEnseignantGenerique() {
    return (Integer) storedValueForKey(_EOService.ID_ENSEIGNANT_GENERIQUE_KEY);
  }

  public void setIdEnseignantGenerique(Integer value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating idEnseignantGenerique from " + idEnseignantGenerique() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.ID_ENSEIGNANT_GENERIQUE_KEY);
  }

  public String suivi() {
    return (String) storedValueForKey(_EOService.SUIVI_KEY);
  }

  public void setSuivi(String value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating suivi from " + suivi() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.SUIVI_KEY);
  }

  public String temoinEnseignantGenerique() {
    return (String) storedValueForKey(_EOService.TEMOIN_ENSEIGNANT_GENERIQUE_KEY);
  }

  public void setTemoinEnseignantGenerique(String value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating temoinEnseignantGenerique from " + temoinEnseignantGenerique() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.TEMOIN_ENSEIGNANT_GENERIQUE_KEY);
  }

  public String temoinValide() {
    return (String) storedValueForKey(_EOService.TEMOIN_VALIDE_KEY);
  }

  public void setTemoinValide(String value) {
    if (_EOService.LOG.isDebugEnabled()) {
    	_EOService.LOG.debug( "updating temoinValide from " + temoinValide() + " to " + value);
    }
    takeStoredValueForKey(value, _EOService.TEMOIN_VALIDE_KEY);
  }

  public org.cocktail.peche.entity.EOActuelEnseignant enseignant() {
    return (org.cocktail.peche.entity.EOActuelEnseignant)storedValueForKey(_EOService.ENSEIGNANT_KEY);
  }
  
  public void setEnseignant(org.cocktail.peche.entity.EOActuelEnseignant value) {
    takeStoredValueForKey(value, _EOService.ENSEIGNANT_KEY);
  }

  public void setEnseignantRelationship(org.cocktail.peche.entity.EOActuelEnseignant value) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("updating enseignant from " + enseignant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setEnseignant(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOActuelEnseignant oldValue = enseignant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOService.ENSEIGNANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOService.ENSEIGNANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOService.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOService.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOService.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOService.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOService.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOService.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOService.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOService.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande toDemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(_EOService.TO_DEMANDE_KEY);
  }
  
  public void setToDemande(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    takeStoredValueForKey(value, _EOService.TO_DEMANDE_KEY);
  }

  public void setToDemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("updating toDemande from " + toDemande() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToDemande(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = toDemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOService.TO_DEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOService.TO_DEMANDE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOEnseignantGenerique toEnseignantGenerique() {
    return (org.cocktail.peche.entity.EOEnseignantGenerique)storedValueForKey(_EOService.TO_ENSEIGNANT_GENERIQUE_KEY);
  }
  
  public void setToEnseignantGenerique(org.cocktail.peche.entity.EOEnseignantGenerique value) {
    takeStoredValueForKey(value, _EOService.TO_ENSEIGNANT_GENERIQUE_KEY);
  }

  public void setToEnseignantGeneriqueRelationship(org.cocktail.peche.entity.EOEnseignantGenerique value) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("updating toEnseignantGenerique from " + toEnseignantGenerique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToEnseignantGenerique(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOEnseignantGenerique oldValue = toEnseignantGenerique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOService.TO_ENSEIGNANT_GENERIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOService.TO_ENSEIGNANT_GENERIQUE_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails() {
    return (NSArray<org.cocktail.peche.entity.EOServiceDetail>)storedValueForKey(_EOService.LISTE_SERVICE_DETAILS_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier) {
    return listeServiceDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier, boolean fetch) {
    return listeServiceDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOServiceDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOServiceDetail.SERVICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOServiceDetail.fetchEOServiceDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeServiceDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeServiceDetails(org.cocktail.peche.entity.EOServiceDetail object) {
    includeObjectIntoPropertyWithKey(object, _EOService.LISTE_SERVICE_DETAILS_KEY);
  }

  public void removeFromListeServiceDetails(org.cocktail.peche.entity.EOServiceDetail object) {
    excludeObjectFromPropertyWithKey(object, _EOService.LISTE_SERVICE_DETAILS_KEY);
  }

  public void addToListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("adding " + object + " to listeServiceDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeServiceDetails(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOService.LISTE_SERVICE_DETAILS_KEY);
    }
  }

  public void removeFromListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("removing " + object + " from listeServiceDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeServiceDetails(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.LISTE_SERVICE_DETAILS_KEY);
    }
  }

  public org.cocktail.peche.entity.EOServiceDetail createListeServiceDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOServiceDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOService.LISTE_SERVICE_DETAILS_KEY);
    return (org.cocktail.peche.entity.EOServiceDetail) eo;
  }

  public void deleteListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.LISTE_SERVICE_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeServiceDetailsRelationships() {
    Enumeration<org.cocktail.peche.entity.EOServiceDetail> objects = listeServiceDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeServiceDetailsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement() {
    return (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)storedValueForKey(_EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier) {
    return toListeMisesEnPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeMisesEnPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOMiseEnPaiement.TO_SERVICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeMisesEnPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void removeFromToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void addToToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("adding " + object + " to toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeMisesEnPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("removing " + object + " from toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeMisesEnPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EOMiseEnPaiement createToListeMisesEnPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOMiseEnPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EOMiseEnPaiement) eo;
  }

  public void deleteToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeMisesEnPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EOMiseEnPaiement> objects = toListeMisesEnPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeMisesEnPaiementRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.peche.entity.EORepartService> toListeRepartService() {
    return (NSArray<org.cocktail.peche.entity.EORepartService>)storedValueForKey(_EOService.TO_LISTE_REPART_SERVICE_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EORepartService> toListeRepartService(EOQualifier qualifier) {
    return toListeRepartService(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EORepartService> toListeRepartService(EOQualifier qualifier, boolean fetch) {
    return toListeRepartService(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EORepartService> toListeRepartService(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EORepartService> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EORepartService.TO_SERVICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EORepartService.fetchEORepartServices(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeRepartService();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartService>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartService>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeRepartService(org.cocktail.peche.entity.EORepartService object) {
    includeObjectIntoPropertyWithKey(object, _EOService.TO_LISTE_REPART_SERVICE_KEY);
  }

  public void removeFromToListeRepartService(org.cocktail.peche.entity.EORepartService object) {
    excludeObjectFromPropertyWithKey(object, _EOService.TO_LISTE_REPART_SERVICE_KEY);
  }

  public void addToToListeRepartServiceRelationship(org.cocktail.peche.entity.EORepartService object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("adding " + object + " to toListeRepartService relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeRepartService(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_REPART_SERVICE_KEY);
    }
  }

  public void removeFromToListeRepartServiceRelationship(org.cocktail.peche.entity.EORepartService object) {
    if (_EOService.LOG.isDebugEnabled()) {
      _EOService.LOG.debug("removing " + object + " from toListeRepartService relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeRepartService(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_REPART_SERVICE_KEY);
    }
  }

  public org.cocktail.peche.entity.EORepartService createToListeRepartServiceRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EORepartService.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOService.TO_LISTE_REPART_SERVICE_KEY);
    return (org.cocktail.peche.entity.EORepartService) eo;
  }

  public void deleteToListeRepartServiceRelationship(org.cocktail.peche.entity.EORepartService object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOService.TO_LISTE_REPART_SERVICE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeRepartServiceRelationships() {
    Enumeration<org.cocktail.peche.entity.EORepartService> objects = toListeRepartService().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeRepartServiceRelationship(objects.nextElement());
    }
  }


  public static EOService createEOService(EOEditingContext editingContext, Integer annee
, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer id
, String temoinEnseignantGenerique
, String temoinValide
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EOService eo = (EOService) EOUtilities.createAndInsertInstance(editingContext, _EOService.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
		eo.setTemoinEnseignantGenerique(temoinEnseignantGenerique);
		eo.setTemoinValide(temoinValide);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOService> fetchSpec() {
    return new ERXFetchSpecification<EOService>(_EOService.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOService> fetchAllEOServices(EOEditingContext editingContext) {
    return _EOService.fetchAllEOServices(editingContext, null);
  }

  public static NSArray<EOService> fetchAllEOServices(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOService.fetchEOServices(editingContext, null, sortOrderings);
  }

  public static NSArray<EOService> fetchEOServices(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOService> fetchSpec = new ERXFetchSpecification<EOService>(_EOService.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOService> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOService fetchEOService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOService.fetchEOService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOService fetchEOService(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOService> eoObjects = _EOService.fetchEOServices(editingContext, qualifier, null);
    EOService eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOService fetchRequiredEOService(EOEditingContext editingContext, String keyName, Object value) {
    return _EOService.fetchRequiredEOService(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOService fetchRequiredEOService(EOEditingContext editingContext, EOQualifier qualifier) {
    EOService eoObject = _EOService.fetchEOService(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOService that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOService localInstanceIn(EOEditingContext editingContext, EOService eo) {
    EOService localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
