// DO NOT EDIT.  Make changes to EOReh.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOReh extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOReh";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
  public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> DESCRIPTION = new ERXKey<String>("description");
  public static final ERXKey<String> LIBELLE_COURT = new ERXKey<String>("libelleCourt");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> LISTE_SERVICE_DETAILS = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("listeServiceDetails");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_DEBUT_KEY = DATE_DEBUT.key();
  public static final String DATE_FIN_KEY = DATE_FIN.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DESCRIPTION_KEY = DESCRIPTION.key();
  public static final String LIBELLE_COURT_KEY = LIBELLE_COURT.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  // Relationships
  public static final String LISTE_SERVICE_DETAILS_KEY = LISTE_SERVICE_DETAILS.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOReh.class);

  public EOReh localInstanceIn(EOEditingContext editingContext) {
    EOReh localInstance = (EOReh)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOReh.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOReh.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.DATE_CREATION_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(_EOReh.DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(_EOReh.DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.DATE_FIN_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOReh.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.DATE_MODIFICATION_KEY);
  }

  public String description() {
    return (String) storedValueForKey(_EOReh.DESCRIPTION_KEY);
  }

  public void setDescription(String value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating description from " + description() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.DESCRIPTION_KEY);
  }

  public String libelleCourt() {
    return (String) storedValueForKey(_EOReh.LIBELLE_COURT_KEY);
  }

  public void setLibelleCourt(String value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating libelleCourt from " + libelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.LIBELLE_COURT_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(_EOReh.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (_EOReh.LOG.isDebugEnabled()) {
    	_EOReh.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, _EOReh.LIBELLE_LONG_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOReh.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOReh.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOReh.LOG.isDebugEnabled()) {
      _EOReh.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOReh.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOReh.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOReh.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOReh.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOReh.LOG.isDebugEnabled()) {
      _EOReh.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOReh.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOReh.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails() {
    return (NSArray<org.cocktail.peche.entity.EOServiceDetail>)storedValueForKey(_EOReh.LISTE_SERVICE_DETAILS_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier) {
    return listeServiceDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier, boolean fetch) {
    return listeServiceDetails(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOServiceDetail> listeServiceDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOServiceDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOServiceDetail.REH_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOServiceDetail.fetchEOServiceDetails(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeServiceDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOServiceDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeServiceDetails(org.cocktail.peche.entity.EOServiceDetail object) {
    includeObjectIntoPropertyWithKey(object, _EOReh.LISTE_SERVICE_DETAILS_KEY);
  }

  public void removeFromListeServiceDetails(org.cocktail.peche.entity.EOServiceDetail object) {
    excludeObjectFromPropertyWithKey(object, _EOReh.LISTE_SERVICE_DETAILS_KEY);
  }

  public void addToListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOReh.LOG.isDebugEnabled()) {
      _EOReh.LOG.debug("adding " + object + " to listeServiceDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeServiceDetails(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOReh.LISTE_SERVICE_DETAILS_KEY);
    }
  }

  public void removeFromListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    if (_EOReh.LOG.isDebugEnabled()) {
      _EOReh.LOG.debug("removing " + object + " from listeServiceDetails relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeServiceDetails(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOReh.LISTE_SERVICE_DETAILS_KEY);
    }
  }

  public org.cocktail.peche.entity.EOServiceDetail createListeServiceDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOServiceDetail.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOReh.LISTE_SERVICE_DETAILS_KEY);
    return (org.cocktail.peche.entity.EOServiceDetail) eo;
  }

  public void deleteListeServiceDetailsRelationship(org.cocktail.peche.entity.EOServiceDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOReh.LISTE_SERVICE_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeServiceDetailsRelationships() {
    Enumeration<org.cocktail.peche.entity.EOServiceDetail> objects = listeServiceDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeServiceDetailsRelationship(objects.nextElement());
    }
  }


  public static EOReh createEOReh(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, String description
, String libelleCourt
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EOReh eo = (EOReh) EOUtilities.createAndInsertInstance(editingContext, _EOReh.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setDescription(description);
		eo.setLibelleCourt(libelleCourt);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOReh> fetchSpec() {
    return new ERXFetchSpecification<EOReh>(_EOReh.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOReh> fetchAllEORehs(EOEditingContext editingContext) {
    return _EOReh.fetchAllEORehs(editingContext, null);
  }

  public static NSArray<EOReh> fetchAllEORehs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOReh.fetchEORehs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOReh> fetchEORehs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOReh> fetchSpec = new ERXFetchSpecification<EOReh>(_EOReh.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOReh> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOReh fetchEOReh(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReh.fetchEOReh(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReh fetchEOReh(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOReh> eoObjects = _EOReh.fetchEORehs(editingContext, qualifier, null);
    EOReh eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOReh that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReh fetchRequiredEOReh(EOEditingContext editingContext, String keyName, Object value) {
    return _EOReh.fetchRequiredEOReh(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOReh fetchRequiredEOReh(EOEditingContext editingContext, EOQualifier qualifier) {
    EOReh eoObject = _EOReh.fetchEOReh(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOReh that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOReh localInstanceIn(EOEditingContext editingContext, EOReh eo) {
    EOReh localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
