// DO NOT EDIT.  Make changes to EOTauxHoraire.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTauxHoraire extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOTauxHoraire";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_DEBUT_APPLICATION = new ERXKey<NSTimestamp>("dateDebutApplication");
  public static final ERXKey<NSTimestamp> DATE_FIN_APPLICATION = new ERXKey<NSTimestamp>("dateFinApplication");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> TAUX_BRUT = new ERXKey<java.math.BigDecimal>("tauxBrut");
  public static final ERXKey<java.math.BigDecimal> TAUX_CHARGE_FONCTIONNAIRE = new ERXKey<java.math.BigDecimal>("tauxChargeFonctionnaire");
  public static final ERXKey<java.math.BigDecimal> TAUX_CHARGE_NON_FONCTIONNAIRE = new ERXKey<java.math.BigDecimal>("tauxChargeNonFonctionnaire");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_DEBUT_APPLICATION_KEY = DATE_DEBUT_APPLICATION.key();
  public static final String DATE_FIN_APPLICATION_KEY = DATE_FIN_APPLICATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String TAUX_BRUT_KEY = TAUX_BRUT.key();
  public static final String TAUX_CHARGE_FONCTIONNAIRE_KEY = TAUX_CHARGE_FONCTIONNAIRE.key();
  public static final String TAUX_CHARGE_NON_FONCTIONNAIRE_KEY = TAUX_CHARGE_NON_FONCTIONNAIRE.key();
  // Relationships
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOTauxHoraire.class);

  public EOTauxHoraire localInstanceIn(EOEditingContext editingContext) {
    EOTauxHoraire localInstance = (EOTauxHoraire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOTauxHoraire.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.DATE_CREATION_KEY);
  }

  public NSTimestamp dateDebutApplication() {
    return (NSTimestamp) storedValueForKey(_EOTauxHoraire.DATE_DEBUT_APPLICATION_KEY);
  }

  public void setDateDebutApplication(NSTimestamp value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating dateDebutApplication from " + dateDebutApplication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.DATE_DEBUT_APPLICATION_KEY);
  }

  public NSTimestamp dateFinApplication() {
    return (NSTimestamp) storedValueForKey(_EOTauxHoraire.DATE_FIN_APPLICATION_KEY);
  }

  public void setDateFinApplication(NSTimestamp value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating dateFinApplication from " + dateFinApplication() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.DATE_FIN_APPLICATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOTauxHoraire.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal tauxBrut() {
    return (java.math.BigDecimal) storedValueForKey(_EOTauxHoraire.TAUX_BRUT_KEY);
  }

  public void setTauxBrut(java.math.BigDecimal value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating tauxBrut from " + tauxBrut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.TAUX_BRUT_KEY);
  }

  public java.math.BigDecimal tauxChargeFonctionnaire() {
    return (java.math.BigDecimal) storedValueForKey(_EOTauxHoraire.TAUX_CHARGE_FONCTIONNAIRE_KEY);
  }

  public void setTauxChargeFonctionnaire(java.math.BigDecimal value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating tauxChargeFonctionnaire from " + tauxChargeFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.TAUX_CHARGE_FONCTIONNAIRE_KEY);
  }

  public java.math.BigDecimal tauxChargeNonFonctionnaire() {
    return (java.math.BigDecimal) storedValueForKey(_EOTauxHoraire.TAUX_CHARGE_NON_FONCTIONNAIRE_KEY);
  }

  public void setTauxChargeNonFonctionnaire(java.math.BigDecimal value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
    	_EOTauxHoraire.LOG.debug( "updating tauxChargeNonFonctionnaire from " + tauxChargeNonFonctionnaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTauxHoraire.TAUX_CHARGE_NON_FONCTIONNAIRE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOTauxHoraire.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOTauxHoraire.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
      _EOTauxHoraire.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOTauxHoraire.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOTauxHoraire.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOTauxHoraire.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOTauxHoraire.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOTauxHoraire.LOG.isDebugEnabled()) {
      _EOTauxHoraire.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOTauxHoraire.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOTauxHoraire.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EOTauxHoraire createEOTauxHoraire(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateDebutApplication
, NSTimestamp dateModification
, java.math.BigDecimal tauxBrut
, java.math.BigDecimal tauxChargeFonctionnaire
, java.math.BigDecimal tauxChargeNonFonctionnaire
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification) {
    EOTauxHoraire eo = (EOTauxHoraire) EOUtilities.createAndInsertInstance(editingContext, _EOTauxHoraire.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateDebutApplication(dateDebutApplication);
		eo.setDateModification(dateModification);
		eo.setTauxBrut(tauxBrut);
		eo.setTauxChargeFonctionnaire(tauxChargeFonctionnaire);
		eo.setTauxChargeNonFonctionnaire(tauxChargeNonFonctionnaire);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOTauxHoraire> fetchSpec() {
    return new ERXFetchSpecification<EOTauxHoraire>(_EOTauxHoraire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTauxHoraire> fetchAllEOTauxHoraires(EOEditingContext editingContext) {
    return _EOTauxHoraire.fetchAllEOTauxHoraires(editingContext, null);
  }

  public static NSArray<EOTauxHoraire> fetchAllEOTauxHoraires(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTauxHoraire.fetchEOTauxHoraires(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTauxHoraire> fetchEOTauxHoraires(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTauxHoraire> fetchSpec = new ERXFetchSpecification<EOTauxHoraire>(_EOTauxHoraire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTauxHoraire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTauxHoraire fetchEOTauxHoraire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTauxHoraire.fetchEOTauxHoraire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTauxHoraire fetchEOTauxHoraire(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTauxHoraire> eoObjects = _EOTauxHoraire.fetchEOTauxHoraires(editingContext, qualifier, null);
    EOTauxHoraire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOTauxHoraire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTauxHoraire fetchRequiredEOTauxHoraire(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTauxHoraire.fetchRequiredEOTauxHoraire(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTauxHoraire fetchRequiredEOTauxHoraire(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTauxHoraire eoObject = _EOTauxHoraire.fetchEOTauxHoraire(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOTauxHoraire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTauxHoraire localInstanceIn(EOEditingContext editingContext, EOTauxHoraire eo) {
    EOTauxHoraire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
