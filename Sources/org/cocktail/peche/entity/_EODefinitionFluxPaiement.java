// DO NOT EDIT.  Make changes to EODefinitionFluxPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODefinitionFluxPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EODefinitionFluxPaiement";

  // Attribute Keys
  public static final ERXKey<String> CODE_CHAMP = new ERXKey<String>("codeChamp");
  public static final ERXKey<String> COMMENTAIRE_CHAMP = new ERXKey<String>("commentaireChamp");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID_PERSONNE_CREATION = new ERXKey<Integer>("idPersonneCreation");
  public static final ERXKey<Integer> ID_PERSONNE_MODIFICATION = new ERXKey<Integer>("idPersonneModification");
  public static final ERXKey<String> LIBELLE_CHAMP = new ERXKey<String>("libelleChamp");
  public static final ERXKey<Integer> LONGUEUR_CHAMP = new ERXKey<Integer>("longueurChamp");
  public static final ERXKey<Integer> TYPE_FLUX_PAIEMENT = new ERXKey<Integer>("typeFluxPaiement");
  public static final ERXKey<String> VALEUR_DEFAUT = new ERXKey<String>("valeurDefaut");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EODetailFluxPaiement> TO_LISTE_DETAIL_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EODetailFluxPaiement>("toListeDetailFluxPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");

  // Attributes
  public static final String CODE_CHAMP_KEY = CODE_CHAMP.key();
  public static final String COMMENTAIRE_CHAMP_KEY = COMMENTAIRE_CHAMP.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_PERSONNE_CREATION_KEY = ID_PERSONNE_CREATION.key();
  public static final String ID_PERSONNE_MODIFICATION_KEY = ID_PERSONNE_MODIFICATION.key();
  public static final String LIBELLE_CHAMP_KEY = LIBELLE_CHAMP.key();
  public static final String LONGUEUR_CHAMP_KEY = LONGUEUR_CHAMP.key();
  public static final String TYPE_FLUX_PAIEMENT_KEY = TYPE_FLUX_PAIEMENT.key();
  public static final String VALEUR_DEFAUT_KEY = VALEUR_DEFAUT.key();
  // Relationships
  public static final String TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY = TO_LISTE_DETAIL_FLUX_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EODefinitionFluxPaiement.class);

  public EODefinitionFluxPaiement localInstanceIn(EOEditingContext editingContext) {
    EODefinitionFluxPaiement localInstance = (EODefinitionFluxPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeChamp() {
    return (String) storedValueForKey(_EODefinitionFluxPaiement.CODE_CHAMP_KEY);
  }

  public void setCodeChamp(String value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating codeChamp from " + codeChamp() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.CODE_CHAMP_KEY);
  }

  public String commentaireChamp() {
    return (String) storedValueForKey(_EODefinitionFluxPaiement.COMMENTAIRE_CHAMP_KEY);
  }

  public void setCommentaireChamp(String value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating commentaireChamp from " + commentaireChamp() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.COMMENTAIRE_CHAMP_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EODefinitionFluxPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EODefinitionFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public Integer idPersonneCreation() {
    return (Integer) storedValueForKey(_EODefinitionFluxPaiement.ID_PERSONNE_CREATION_KEY);
  }

  public void setIdPersonneCreation(Integer value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating idPersonneCreation from " + idPersonneCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.ID_PERSONNE_CREATION_KEY);
  }

  public Integer idPersonneModification() {
    return (Integer) storedValueForKey(_EODefinitionFluxPaiement.ID_PERSONNE_MODIFICATION_KEY);
  }

  public void setIdPersonneModification(Integer value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating idPersonneModification from " + idPersonneModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.ID_PERSONNE_MODIFICATION_KEY);
  }

  public String libelleChamp() {
    return (String) storedValueForKey(_EODefinitionFluxPaiement.LIBELLE_CHAMP_KEY);
  }

  public void setLibelleChamp(String value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating libelleChamp from " + libelleChamp() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.LIBELLE_CHAMP_KEY);
  }

  public Integer longueurChamp() {
    return (Integer) storedValueForKey(_EODefinitionFluxPaiement.LONGUEUR_CHAMP_KEY);
  }

  public void setLongueurChamp(Integer value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating longueurChamp from " + longueurChamp() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.LONGUEUR_CHAMP_KEY);
  }

  public Integer typeFluxPaiement() {
    return (Integer) storedValueForKey(_EODefinitionFluxPaiement.TYPE_FLUX_PAIEMENT_KEY);
  }

  public void setTypeFluxPaiement(Integer value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating typeFluxPaiement from " + typeFluxPaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.TYPE_FLUX_PAIEMENT_KEY);
  }

  public String valeurDefaut() {
    return (String) storedValueForKey(_EODefinitionFluxPaiement.VALEUR_DEFAUT_KEY);
  }

  public void setValeurDefaut(String value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
    	_EODefinitionFluxPaiement.LOG.debug( "updating valeurDefaut from " + valeurDefaut() + " to " + value);
    }
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.VALEUR_DEFAUT_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EODefinitionFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EODefinitionFluxPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
      _EODefinitionFluxPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODefinitionFluxPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODefinitionFluxPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement() {
    return (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)storedValueForKey(_EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier) {
    return toListeDetailFluxPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeDetailFluxPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> toListeDetailFluxPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EODetailFluxPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EODetailFluxPaiement.TO_DEFINITION_FLUX_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EODetailFluxPaiement.fetchEODetailFluxPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeDetailFluxPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EODetailFluxPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeDetailFluxPaiement(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public void removeFromToListeDetailFluxPaiement(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
  }

  public void addToToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
      _EODefinitionFluxPaiement.LOG.debug("adding " + object + " to toListeDetailFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeDetailFluxPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
      _EODefinitionFluxPaiement.LOG.debug("removing " + object + " from toListeDetailFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeDetailFluxPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EODetailFluxPaiement createToListeDetailFluxPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EODetailFluxPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EODetailFluxPaiement) eo;
  }

  public void deleteToListeDetailFluxPaiementRelationship(org.cocktail.peche.entity.EODetailFluxPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_LISTE_DETAIL_FLUX_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeDetailFluxPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EODetailFluxPaiement> objects = toListeDetailFluxPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeDetailFluxPaiementRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> toPersonneModification() {
    return (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>)storedValueForKey(_EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> toPersonneModification(EOQualifier qualifier) {
    return toPersonneModification(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> toPersonneModification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> results;
      results = toPersonneModification();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne object) {
    includeObjectIntoPropertyWithKey(object, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void removeFromToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne object) {
    excludeObjectFromPropertyWithKey(object, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void addToToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne object) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
      _EODefinitionFluxPaiement.LOG.debug("adding " + object + " to toPersonneModification relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToPersonneModification(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }

  public void removeFromToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne object) {
    if (_EODefinitionFluxPaiement.LOG.isDebugEnabled()) {
      _EODefinitionFluxPaiement.LOG.debug("removing " + object + " from toPersonneModification relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToPersonneModification(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne createToPersonneModificationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne) eo;
  }

  public void deleteToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EODefinitionFluxPaiement.TO_PERSONNE_MODIFICATION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersonneModificationRelationships() {
    Enumeration<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> objects = toPersonneModification().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersonneModificationRelationship(objects.nextElement());
    }
  }


  public static EODefinitionFluxPaiement createEODefinitionFluxPaiement(EOEditingContext editingContext, String codeChamp
, String commentaireChamp
, NSTimestamp dateCreation
, Integer idPersonneCreation
, String libelleChamp
, Integer longueurChamp
, Integer typeFluxPaiement
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation) {
    EODefinitionFluxPaiement eo = (EODefinitionFluxPaiement) EOUtilities.createAndInsertInstance(editingContext, _EODefinitionFluxPaiement.ENTITY_NAME);    
		eo.setCodeChamp(codeChamp);
		eo.setCommentaireChamp(commentaireChamp);
		eo.setDateCreation(dateCreation);
		eo.setIdPersonneCreation(idPersonneCreation);
		eo.setLibelleChamp(libelleChamp);
		eo.setLongueurChamp(longueurChamp);
		eo.setTypeFluxPaiement(typeFluxPaiement);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EODefinitionFluxPaiement> fetchSpec() {
    return new ERXFetchSpecification<EODefinitionFluxPaiement>(_EODefinitionFluxPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODefinitionFluxPaiement> fetchAllEODefinitionFluxPaiements(EOEditingContext editingContext) {
    return _EODefinitionFluxPaiement.fetchAllEODefinitionFluxPaiements(editingContext, null);
  }

  public static NSArray<EODefinitionFluxPaiement> fetchAllEODefinitionFluxPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODefinitionFluxPaiement.fetchEODefinitionFluxPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EODefinitionFluxPaiement> fetchEODefinitionFluxPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODefinitionFluxPaiement> fetchSpec = new ERXFetchSpecification<EODefinitionFluxPaiement>(_EODefinitionFluxPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODefinitionFluxPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODefinitionFluxPaiement fetchEODefinitionFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODefinitionFluxPaiement.fetchEODefinitionFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODefinitionFluxPaiement fetchEODefinitionFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODefinitionFluxPaiement> eoObjects = _EODefinitionFluxPaiement.fetchEODefinitionFluxPaiements(editingContext, qualifier, null);
    EODefinitionFluxPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EODefinitionFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODefinitionFluxPaiement fetchRequiredEODefinitionFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EODefinitionFluxPaiement.fetchRequiredEODefinitionFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODefinitionFluxPaiement fetchRequiredEODefinitionFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EODefinitionFluxPaiement eoObject = _EODefinitionFluxPaiement.fetchEODefinitionFluxPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EODefinitionFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODefinitionFluxPaiement localInstanceIn(EOEditingContext editingContext, EODefinitionFluxPaiement eo) {
    EODefinitionFluxPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
