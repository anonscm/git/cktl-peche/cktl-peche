// DO NOT EDIT.  Make changes to EOServiceDetail.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOServiceDetail extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOServiceDetail";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_COMMENTAIRE = new ERXKey<NSTimestamp>("dateCommentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> ETAT = new ERXKey<String>("etat");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_PAYER = new ERXKey<java.math.BigDecimal>("heuresAPayer");
  public static final ERXKey<java.math.BigDecimal> HEURES_PAYEES = new ERXKey<java.math.BigDecimal>("heuresPayees");
  public static final ERXKey<Double> HEURES_PREVUES = new ERXKey<Double>("heuresPrevues");
  public static final ERXKey<Double> HEURES_REALISEES = new ERXKey<Double>("heuresRealisees");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> COMPOSANT_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("composantAP");
  public static final ERXKey<org.cocktail.peche.entity.EORepartEnseignant> LISTE_REPART_ENSEIGNANTS = new ERXKey<org.cocktail.peche.entity.EORepartEnseignant>("listeRepartEnseignants");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.peche.entity.EOReh> REH = new ERXKey<org.cocktail.peche.entity.EOReh>("reh");
  public static final ERXKey<org.cocktail.peche.entity.EOService> SERVICE = new ERXKey<org.cocktail.peche.entity.EOService>("service");
  public static final ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement> TO_LISTE_MISES_EN_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement>("toListeMisesEnPaiement");
  public static final ERXKey<org.cocktail.peche.entity.EOPeriode> TO_PERIODE = new ERXKey<org.cocktail.peche.entity.EOPeriode>("toPeriode");
  public static final ERXKey<org.cocktail.peche.entity.EORepartService> TO_REPART_SERVICE = new ERXKey<org.cocktail.peche.entity.EORepartService>("toRepartService");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_COMMENTAIRE_KEY = DATE_COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ETAT_KEY = ETAT.key();
  public static final String HEURES_A_PAYER_KEY = HEURES_A_PAYER.key();
  public static final String HEURES_PAYEES_KEY = HEURES_PAYEES.key();
  public static final String HEURES_PREVUES_KEY = HEURES_PREVUES.key();
  public static final String HEURES_REALISEES_KEY = HEURES_REALISEES.key();
  // Relationships
  public static final String COMPOSANT_AP_KEY = COMPOSANT_AP.key();
  public static final String LISTE_REPART_ENSEIGNANTS_KEY = LISTE_REPART_ENSEIGNANTS.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String REH_KEY = REH.key();
  public static final String SERVICE_KEY = SERVICE.key();
  public static final String TO_LISTE_MISES_EN_PAIEMENT_KEY = TO_LISTE_MISES_EN_PAIEMENT.key();
  public static final String TO_PERIODE_KEY = TO_PERIODE.key();
  public static final String TO_REPART_SERVICE_KEY = TO_REPART_SERVICE.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();

  private static Logger LOG = Logger.getLogger(_EOServiceDetail.class);

  public EOServiceDetail localInstanceIn(EOEditingContext editingContext) {
    EOServiceDetail localInstance = (EOServiceDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOServiceDetail.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCommentaire() {
    return (NSTimestamp) storedValueForKey(_EOServiceDetail.DATE_COMMENTAIRE_KEY);
  }

  public void setDateCommentaire(NSTimestamp value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating dateCommentaire from " + dateCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.DATE_COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOServiceDetail.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOServiceDetail.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.DATE_MODIFICATION_KEY);
  }

  public String etat() {
    return (String) storedValueForKey(_EOServiceDetail.ETAT_KEY);
  }

  public void setEtat(String value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating etat from " + etat() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.ETAT_KEY);
  }

  public java.math.BigDecimal heuresAPayer() {
    return (java.math.BigDecimal) storedValueForKey(_EOServiceDetail.HEURES_A_PAYER_KEY);
  }

  public void setHeuresAPayer(java.math.BigDecimal value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating heuresAPayer from " + heuresAPayer() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.HEURES_A_PAYER_KEY);
  }

  public java.math.BigDecimal heuresPayees() {
    return (java.math.BigDecimal) storedValueForKey(_EOServiceDetail.HEURES_PAYEES_KEY);
  }

  public void setHeuresPayees(java.math.BigDecimal value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating heuresPayees from " + heuresPayees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.HEURES_PAYEES_KEY);
  }

  public Double heuresPrevues() {
    return (Double) storedValueForKey(_EOServiceDetail.HEURES_PREVUES_KEY);
  }

  public void setHeuresPrevues(Double value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating heuresPrevues from " + heuresPrevues() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.HEURES_PREVUES_KEY);
  }

  public Double heuresRealisees() {
    return (Double) storedValueForKey(_EOServiceDetail.HEURES_REALISEES_KEY);
  }

  public void setHeuresRealisees(Double value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
    	_EOServiceDetail.LOG.debug( "updating heuresRealisees from " + heuresRealisees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOServiceDetail.HEURES_REALISEES_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP composantAP() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP)storedValueForKey(_EOServiceDetail.COMPOSANT_AP_KEY);
  }
  
  public void setComposantAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    takeStoredValueForKey(value, _EOServiceDetail.COMPOSANT_AP_KEY);
  }

  public void setComposantAPRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating composantAP from " + composantAP() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposantAP(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP oldValue = composantAP();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.COMPOSANT_AP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.COMPOSANT_AP_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOServiceDetail.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOServiceDetail.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOServiceDetail.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOServiceDetail.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOReh reh() {
    return (org.cocktail.peche.entity.EOReh)storedValueForKey(_EOServiceDetail.REH_KEY);
  }
  
  public void setReh(org.cocktail.peche.entity.EOReh value) {
    takeStoredValueForKey(value, _EOServiceDetail.REH_KEY);
  }

  public void setRehRelationship(org.cocktail.peche.entity.EOReh value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating reh from " + reh() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setReh(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOReh oldValue = reh();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.REH_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.REH_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOService service() {
    return (org.cocktail.peche.entity.EOService)storedValueForKey(_EOServiceDetail.SERVICE_KEY);
  }
  
  public void setService(org.cocktail.peche.entity.EOService value) {
    takeStoredValueForKey(value, _EOServiceDetail.SERVICE_KEY);
  }

  public void setServiceRelationship(org.cocktail.peche.entity.EOService value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating service from " + service() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOService oldValue = service();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.SERVICE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOPeriode toPeriode() {
    return (org.cocktail.peche.entity.EOPeriode)storedValueForKey(_EOServiceDetail.TO_PERIODE_KEY);
  }
  
  public void setToPeriode(org.cocktail.peche.entity.EOPeriode value) {
    takeStoredValueForKey(value, _EOServiceDetail.TO_PERIODE_KEY);
  }

  public void setToPeriodeRelationship(org.cocktail.peche.entity.EOPeriode value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating toPeriode from " + toPeriode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPeriode(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOPeriode oldValue = toPeriode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.TO_PERIODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.TO_PERIODE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EORepartService toRepartService() {
    return (org.cocktail.peche.entity.EORepartService)storedValueForKey(_EOServiceDetail.TO_REPART_SERVICE_KEY);
  }
  
  public void setToRepartService(org.cocktail.peche.entity.EORepartService value) {
    takeStoredValueForKey(value, _EOServiceDetail.TO_REPART_SERVICE_KEY);
  }

  public void setToRepartServiceRelationship(org.cocktail.peche.entity.EORepartService value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating toRepartService from " + toRepartService() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToRepartService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EORepartService oldValue = toRepartService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.TO_REPART_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.TO_REPART_SERVICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EOServiceDetail.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EOServiceDetail.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOServiceDetail.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOServiceDetail.TO_STRUCTURE_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EORepartEnseignant> listeRepartEnseignants() {
    return (NSArray<org.cocktail.peche.entity.EORepartEnseignant>)storedValueForKey(_EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EORepartEnseignant> listeRepartEnseignants(EOQualifier qualifier) {
    return listeRepartEnseignants(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EORepartEnseignant> listeRepartEnseignants(EOQualifier qualifier, boolean fetch) {
    return listeRepartEnseignants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EORepartEnseignant> listeRepartEnseignants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EORepartEnseignant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EORepartEnseignant.SERVICE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EORepartEnseignant.fetchEORepartEnseignants(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeRepartEnseignants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartEnseignant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartEnseignant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeRepartEnseignants(org.cocktail.peche.entity.EORepartEnseignant object) {
    includeObjectIntoPropertyWithKey(object, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
  }

  public void removeFromListeRepartEnseignants(org.cocktail.peche.entity.EORepartEnseignant object) {
    excludeObjectFromPropertyWithKey(object, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
  }

  public void addToListeRepartEnseignantsRelationship(org.cocktail.peche.entity.EORepartEnseignant object) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("adding " + object + " to listeRepartEnseignants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeRepartEnseignants(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
    }
  }

  public void removeFromListeRepartEnseignantsRelationship(org.cocktail.peche.entity.EORepartEnseignant object) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("removing " + object + " from listeRepartEnseignants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeRepartEnseignants(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
    }
  }

  public org.cocktail.peche.entity.EORepartEnseignant createListeRepartEnseignantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EORepartEnseignant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
    return (org.cocktail.peche.entity.EORepartEnseignant) eo;
  }

  public void deleteListeRepartEnseignantsRelationship(org.cocktail.peche.entity.EORepartEnseignant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOServiceDetail.LISTE_REPART_ENSEIGNANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeRepartEnseignantsRelationships() {
    Enumeration<org.cocktail.peche.entity.EORepartEnseignant> objects = listeRepartEnseignants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeRepartEnseignantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement() {
    return (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)storedValueForKey(_EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier) {
    return toListeMisesEnPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeMisesEnPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeMisesEnPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void removeFromToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void addToToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("adding " + object + " to toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeMisesEnPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOServiceDetail.LOG.isDebugEnabled()) {
      _EOServiceDetail.LOG.debug("removing " + object + " from toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeMisesEnPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EOMiseEnPaiement createToListeMisesEnPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOMiseEnPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EOMiseEnPaiement) eo;
  }

  public void deleteToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOServiceDetail.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeMisesEnPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EOMiseEnPaiement> objects = toListeMisesEnPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeMisesEnPaiementRelationship(objects.nextElement());
    }
  }


  public static EOServiceDetail createEOServiceDetail(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Double heuresPrevues
, Double heuresRealisees
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.peche.entity.EOService service) {
    EOServiceDetail eo = (EOServiceDetail) EOUtilities.createAndInsertInstance(editingContext, _EOServiceDetail.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setHeuresPrevues(heuresPrevues);
		eo.setHeuresRealisees(heuresRealisees);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setServiceRelationship(service);
    return eo;
  }

  public static ERXFetchSpecification<EOServiceDetail> fetchSpec() {
    return new ERXFetchSpecification<EOServiceDetail>(_EOServiceDetail.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOServiceDetail> fetchAllEOServiceDetails(EOEditingContext editingContext) {
    return _EOServiceDetail.fetchAllEOServiceDetails(editingContext, null);
  }

  public static NSArray<EOServiceDetail> fetchAllEOServiceDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOServiceDetail.fetchEOServiceDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOServiceDetail> fetchEOServiceDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOServiceDetail> fetchSpec = new ERXFetchSpecification<EOServiceDetail>(_EOServiceDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOServiceDetail> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOServiceDetail fetchEOServiceDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOServiceDetail.fetchEOServiceDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOServiceDetail fetchEOServiceDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOServiceDetail> eoObjects = _EOServiceDetail.fetchEOServiceDetails(editingContext, qualifier, null);
    EOServiceDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOServiceDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOServiceDetail fetchRequiredEOServiceDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOServiceDetail.fetchRequiredEOServiceDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOServiceDetail fetchRequiredEOServiceDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOServiceDetail eoObject = _EOServiceDetail.fetchEOServiceDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOServiceDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOServiceDetail localInstanceIn(EOEditingContext editingContext, EOServiceDetail eo) {
    EOServiceDetail localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
