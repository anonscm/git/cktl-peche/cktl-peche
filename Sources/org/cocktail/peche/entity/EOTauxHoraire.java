package org.cocktail.peche.entity;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXAndQualifier;

/**
 * Cette classe représente la table "TAUX_HORAIRE".
 * 
 * @author Pascal MACOUIN
 */
public class EOTauxHoraire extends _EOTauxHoraire {
	/** Numéro de série. */
	private static final long serialVersionUID = -6393133946683863665L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTauxHoraire.class);
	
	/**
	 * Créer et initialiser une instance de taux horaire.
	 * 
	 * @param editingContext un editing context
	 * @param dateDebut la date de début d'application du taux horaire (en UTC)
	 * @param tauxBrut le taux brut
	 * @param tauxChargeFonctionnaire le taux chargé pour les fonctionnaires
	 * @param tauxChargeNonFonctionnaire le taux chargé pour les non fonctionnaires
	 * @param personne la personne qui crée cette instance
	 * @return l'instance de mise en paiement nouvellement créée
	 */
	public static EOTauxHoraire creerEtInitialiser(EOEditingContext editingContext, NSTimestamp dateDebut, BigDecimal tauxBrut, BigDecimal tauxChargeFonctionnaire, BigDecimal tauxChargeNonFonctionnaire, EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		EOTauxHoraire tauxHoraire = EOTauxHoraire.createEOTauxHoraire(editingContext, now, dateDebut, now, tauxBrut, tauxChargeFonctionnaire, tauxChargeNonFonctionnaire, personne,personne);
		return tauxHoraire;
	}
	
	
	public static EOTauxHoraire creerEtInitialiser(EOEditingContext editingContext) {
		NSTimestamp lendemain = null;
		EOTauxHoraire eoTauxHoraire = new EOTauxHoraire();
		
		ERXAndQualifier qualifier = ERXQ.and(EOTauxHoraire.DATE_FIN_APPLICATION.isNull());
		
		NSArray<EOTauxHoraire> listeTauxHoraire = EOTauxHoraire.fetchEOTauxHoraires(editingContext, qualifier, EOTauxHoraire.DATE_DEBUT_APPLICATION.descs());
		EOTauxHoraire ancienTauxHoraire = null;
		
		if (!listeTauxHoraire.isEmpty()) {
			ancienTauxHoraire = listeTauxHoraire.get(0);			
			lendemain = ancienTauxHoraire.dateDebutApplication().timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
			eoTauxHoraire.setDateDebutApplication(lendemain);			
		}
		
		return eoTauxHoraire;
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);
		
		// en creation, il n'y a pas de date de fin 
		setDateFinApplication(null);
		
		// on met à jour le précédent taux ( date fin à null) en mettant la date de fin à la veille de la date de début d'application du nouveau taux
		ERXAndQualifier qualifier = ERXQ.and(EOTauxHoraire.DATE_FIN_APPLICATION.isNull());
		
		NSArray<EOTauxHoraire> listeTauxHoraire = EOTauxHoraire.fetchEOTauxHoraires(editingContext(), qualifier, EOTauxHoraire.DATE_DEBUT_APPLICATION.ascs());
		EOTauxHoraire tauxHoraireAModifier = null;
		
		if (!listeTauxHoraire.isEmpty()) {
			tauxHoraireAModifier = listeTauxHoraire.get(0);					
			NSTimestamp veille = dateDebutApplication().timestampByAddingGregorianUnits(0, 0, -1, 0, 0, 0);
			tauxHoraireAModifier.setDateFinApplication(veille);
		}
	}
	
	public void majDonnesAuditSuppression() {		
		
		// on met à jour le précédent taux le plus recent en mettant la date de fin à null
		ERXAndQualifier qualifier = ERXQ.and(EOTauxHoraire.DATE_FIN_APPLICATION.isNotNull());
		
		NSArray<EOTauxHoraire> listeTauxHoraire = EOTauxHoraire.fetchEOTauxHoraires(editingContext(), qualifier, EOTauxHoraire.DATE_DEBUT_APPLICATION.descs());
		EOTauxHoraire tauxHoraireAModifier = null;
		
		if (!listeTauxHoraire.isEmpty()) {
			tauxHoraireAModifier = listeTauxHoraire.get(0);					
			tauxHoraireAModifier.setDateFinApplication(null);
		}
	}
	
	public NSTimestamp dateDebutDernierTaux() {
		NSTimestamp dateDebut = null;
		
		ERXAndQualifier qualifier = ERXQ.and(EOTauxHoraire.DATE_FIN_APPLICATION.isNull());
		
		NSArray<EOTauxHoraire> listeTauxHoraire = EOTauxHoraire.fetchEOTauxHoraires(editingContext(), qualifier, EOTauxHoraire.DATE_DEBUT_APPLICATION.descs());
		EOTauxHoraire tauxHoraire = null;
		
		if (!listeTauxHoraire.isEmpty()) {
			tauxHoraire = listeTauxHoraire.get(0);					
			dateDebut= tauxHoraire.dateDebutApplication();			
		}
		
		return dateDebut;		
	}
	
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		setDateModification(new NSTimestamp());
		setToPersonneModificationRelationship(personne);
	}
}
