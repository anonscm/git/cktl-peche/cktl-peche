// DO NOT EDIT.  Make changes to EOActuelEnseignant.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOActuelEnseignant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOActuelEnseignant";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
  public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
  public static final ERXKey<Integer> NO_DOSSIER_PERS = new ERXKey<Integer>("noDossierPers");
  public static final ERXKey<String> TEM_STATUTAIRE = new ERXKey<String>("temStatutaire");
  public static final ERXKey<String> TEM_TITULAIRE = new ERXKey<String>("temTitulaire");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOFicheVoeux> LISTE_FICHES_VOEUX = new ERXKey<org.cocktail.peche.entity.EOFicheVoeux>("listeFichesVoeux");
  public static final ERXKey<org.cocktail.peche.entity.EOService> LISTE_SERVICES = new ERXKey<org.cocktail.peche.entity.EOService>("listeServices");
  public static final ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> TO_AFFECTATION = new ERXKey<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>("toAffectation");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");

  // Attributes
  public static final String DATE_DEBUT_KEY = DATE_DEBUT.key();
  public static final String DATE_FIN_KEY = DATE_FIN.key();
  public static final String NO_DOSSIER_PERS_KEY = NO_DOSSIER_PERS.key();
  public static final String TEM_STATUTAIRE_KEY = TEM_STATUTAIRE.key();
  public static final String TEM_TITULAIRE_KEY = TEM_TITULAIRE.key();
  // Relationships
  public static final String LISTE_FICHES_VOEUX_KEY = LISTE_FICHES_VOEUX.key();
  public static final String LISTE_SERVICES_KEY = LISTE_SERVICES.key();
  public static final String TO_AFFECTATION_KEY = TO_AFFECTATION.key();
  public static final String TO_INDIVIDU_KEY = TO_INDIVIDU.key();

  private static Logger LOG = Logger.getLogger(_EOActuelEnseignant.class);

  public EOActuelEnseignant localInstanceIn(EOEditingContext editingContext) {
    EOActuelEnseignant localInstance = (EOActuelEnseignant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(_EOActuelEnseignant.DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
    	_EOActuelEnseignant.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActuelEnseignant.DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(_EOActuelEnseignant.DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
    	_EOActuelEnseignant.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActuelEnseignant.DATE_FIN_KEY);
  }

  public Integer noDossierPers() {
    return (Integer) storedValueForKey(_EOActuelEnseignant.NO_DOSSIER_PERS_KEY);
  }

  public void setNoDossierPers(Integer value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
    	_EOActuelEnseignant.LOG.debug( "updating noDossierPers from " + noDossierPers() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActuelEnseignant.NO_DOSSIER_PERS_KEY);
  }

  public String temStatutaire() {
    return (String) storedValueForKey(_EOActuelEnseignant.TEM_STATUTAIRE_KEY);
  }

  public void setTemStatutaire(String value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
    	_EOActuelEnseignant.LOG.debug( "updating temStatutaire from " + temStatutaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActuelEnseignant.TEM_STATUTAIRE_KEY);
  }

  public String temTitulaire() {
    return (String) storedValueForKey(_EOActuelEnseignant.TEM_TITULAIRE_KEY);
  }

  public void setTemTitulaire(String value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
    	_EOActuelEnseignant.LOG.debug( "updating temTitulaire from " + temTitulaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOActuelEnseignant.TEM_TITULAIRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(_EOActuelEnseignant.TO_INDIVIDU_KEY);
  }
  
  public void setToIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, _EOActuelEnseignant.TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToIndividu(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOActuelEnseignant.TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOActuelEnseignant.TO_INDIVIDU_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOFicheVoeux> listeFichesVoeux() {
    return (NSArray<org.cocktail.peche.entity.EOFicheVoeux>)storedValueForKey(_EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeux> listeFichesVoeux(EOQualifier qualifier) {
    return listeFichesVoeux(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeux> listeFichesVoeux(EOQualifier qualifier, boolean fetch) {
    return listeFichesVoeux(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOFicheVoeux> listeFichesVoeux(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOFicheVoeux> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOFicheVoeux.ENSEIGNANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOFicheVoeux.fetchEOFicheVoeuxes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeFichesVoeux();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFicheVoeux>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFicheVoeux>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeFichesVoeux(org.cocktail.peche.entity.EOFicheVoeux object) {
    includeObjectIntoPropertyWithKey(object, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
  }

  public void removeFromListeFichesVoeux(org.cocktail.peche.entity.EOFicheVoeux object) {
    excludeObjectFromPropertyWithKey(object, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
  }

  public void addToListeFichesVoeuxRelationship(org.cocktail.peche.entity.EOFicheVoeux object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("adding " + object + " to listeFichesVoeux relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeFichesVoeux(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
    }
  }

  public void removeFromListeFichesVoeuxRelationship(org.cocktail.peche.entity.EOFicheVoeux object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("removing " + object + " from listeFichesVoeux relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeFichesVoeux(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
    }
  }

  public org.cocktail.peche.entity.EOFicheVoeux createListeFichesVoeuxRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOFicheVoeux.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
    return (org.cocktail.peche.entity.EOFicheVoeux) eo;
  }

  public void deleteListeFichesVoeuxRelationship(org.cocktail.peche.entity.EOFicheVoeux object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_FICHES_VOEUX_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeFichesVoeuxRelationships() {
    Enumeration<org.cocktail.peche.entity.EOFicheVoeux> objects = listeFichesVoeux().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeFichesVoeuxRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.peche.entity.EOService> listeServices() {
    return (NSArray<org.cocktail.peche.entity.EOService>)storedValueForKey(_EOActuelEnseignant.LISTE_SERVICES_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOService> listeServices(EOQualifier qualifier) {
    return listeServices(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOService> listeServices(EOQualifier qualifier, boolean fetch) {
    return listeServices(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOService> listeServices(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOService> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOService.ENSEIGNANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOService.fetchEOServices(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = listeServices();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOService>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOService>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToListeServices(org.cocktail.peche.entity.EOService object) {
    includeObjectIntoPropertyWithKey(object, _EOActuelEnseignant.LISTE_SERVICES_KEY);
  }

  public void removeFromListeServices(org.cocktail.peche.entity.EOService object) {
    excludeObjectFromPropertyWithKey(object, _EOActuelEnseignant.LISTE_SERVICES_KEY);
  }

  public void addToListeServicesRelationship(org.cocktail.peche.entity.EOService object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("adding " + object + " to listeServices relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToListeServices(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_SERVICES_KEY);
    }
  }

  public void removeFromListeServicesRelationship(org.cocktail.peche.entity.EOService object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("removing " + object + " from listeServices relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromListeServices(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_SERVICES_KEY);
    }
  }

  public org.cocktail.peche.entity.EOService createListeServicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOService.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOActuelEnseignant.LISTE_SERVICES_KEY);
    return (org.cocktail.peche.entity.EOService) eo;
  }

  public void deleteListeServicesRelationship(org.cocktail.peche.entity.EOService object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.LISTE_SERVICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllListeServicesRelationships() {
    Enumeration<org.cocktail.peche.entity.EOService> objects = listeServices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteListeServicesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> toAffectation() {
    return (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>)storedValueForKey(_EOActuelEnseignant.TO_AFFECTATION_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> toAffectation(EOQualifier qualifier) {
    return toAffectation(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> toAffectation(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> results;
      results = toAffectation();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgrh.common.metier.EOAffectation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToAffectation(org.cocktail.fwkcktlgrh.common.metier.EOAffectation object) {
    includeObjectIntoPropertyWithKey(object, _EOActuelEnseignant.TO_AFFECTATION_KEY);
  }

  public void removeFromToAffectation(org.cocktail.fwkcktlgrh.common.metier.EOAffectation object) {
    excludeObjectFromPropertyWithKey(object, _EOActuelEnseignant.TO_AFFECTATION_KEY);
  }

  public void addToToAffectationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectation object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("adding " + object + " to toAffectation relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToAffectation(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.TO_AFFECTATION_KEY);
    }
  }

  public void removeFromToAffectationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectation object) {
    if (_EOActuelEnseignant.LOG.isDebugEnabled()) {
      _EOActuelEnseignant.LOG.debug("removing " + object + " from toAffectation relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToAffectation(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.TO_AFFECTATION_KEY);
    }
  }

  public org.cocktail.fwkcktlgrh.common.metier.EOAffectation createToAffectationRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgrh.common.metier.EOAffectation.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOActuelEnseignant.TO_AFFECTATION_KEY);
    return (org.cocktail.fwkcktlgrh.common.metier.EOAffectation) eo;
  }

  public void deleteToAffectationRelationship(org.cocktail.fwkcktlgrh.common.metier.EOAffectation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOActuelEnseignant.TO_AFFECTATION_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToAffectationRelationships() {
    Enumeration<org.cocktail.fwkcktlgrh.common.metier.EOAffectation> objects = toAffectation().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToAffectationRelationship(objects.nextElement());
    }
  }


  public static EOActuelEnseignant createEOActuelEnseignant(EOEditingContext editingContext, NSTimestamp dateDebut
, Integer noDossierPers
, String temStatutaire
, String temTitulaire
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu) {
    EOActuelEnseignant eo = (EOActuelEnseignant) EOUtilities.createAndInsertInstance(editingContext, _EOActuelEnseignant.ENTITY_NAME);    
		eo.setDateDebut(dateDebut);
		eo.setNoDossierPers(noDossierPers);
		eo.setTemStatutaire(temStatutaire);
		eo.setTemTitulaire(temTitulaire);
    eo.setToIndividuRelationship(toIndividu);
    return eo;
  }

  public static ERXFetchSpecification<EOActuelEnseignant> fetchSpec() {
    return new ERXFetchSpecification<EOActuelEnseignant>(_EOActuelEnseignant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOActuelEnseignant> fetchAllEOActuelEnseignants(EOEditingContext editingContext) {
    return _EOActuelEnseignant.fetchAllEOActuelEnseignants(editingContext, null);
  }

  public static NSArray<EOActuelEnseignant> fetchAllEOActuelEnseignants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOActuelEnseignant.fetchEOActuelEnseignants(editingContext, null, sortOrderings);
  }

  public static NSArray<EOActuelEnseignant> fetchEOActuelEnseignants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOActuelEnseignant> fetchSpec = new ERXFetchSpecification<EOActuelEnseignant>(_EOActuelEnseignant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOActuelEnseignant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOActuelEnseignant fetchEOActuelEnseignant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOActuelEnseignant.fetchEOActuelEnseignant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOActuelEnseignant fetchEOActuelEnseignant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOActuelEnseignant> eoObjects = _EOActuelEnseignant.fetchEOActuelEnseignants(editingContext, qualifier, null);
    EOActuelEnseignant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOActuelEnseignant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOActuelEnseignant fetchRequiredEOActuelEnseignant(EOEditingContext editingContext, String keyName, Object value) {
    return _EOActuelEnseignant.fetchRequiredEOActuelEnseignant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOActuelEnseignant fetchRequiredEOActuelEnseignant(EOEditingContext editingContext, EOQualifier qualifier) {
    EOActuelEnseignant eoObject = _EOActuelEnseignant.fetchEOActuelEnseignant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOActuelEnseignant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOActuelEnseignant localInstanceIn(EOEditingContext editingContext, EOActuelEnseignant eo) {
    EOActuelEnseignant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
