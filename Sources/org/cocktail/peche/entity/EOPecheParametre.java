package org.cocktail.peche.entity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.Constante;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Classe permettant de récupérer les paramètres de l'application PECHE
 */
public class EOPecheParametre extends _EOPecheParametre {

	private static final long serialVersionUID = -4021061609759556018L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPecheParametre.class);
	
	private static String parametreTypePeriode = "periode.choix.type";
	
	public static String parametreReportBorneInferieur = "report.borne.inferieur";
	public static String parametreReportBorneSuperieur = "report.borne.superieur";
	public static String parametreVacatairePrevisionnelExistence = "fiche.previsionnel.vacataire.utilisation";
	public static String parametreFicheVoeuxExistence = "fiche.voeux.utilisation";
	
	private static String parametreProfilAdministrateur = "perimetreDonnees.idProfil.admin";
	private static String parametreProfilEnseignant = "perimetreDonnees.idProfil.enseignant";
	private static String parametreHCompMethodeDefaut = "hcomp.methode.defaut";

	private static boolean isTypePeriodeDejaCalculee = false;
	private static String typePeriode;
	
	
	private static Map<String, Object> mapParamAnnualise = new HashMap<String, Object>();
	
	private static boolean isProfilAdministrateurDejaCalcule;
	private static int idProfilAdministrateur;
	private static boolean isProfilEnseignantDejaCalcule;
	private static int idProfilEnseignant;
	private static boolean isListeProfilsExclusDejaCalculee;
	private static NSArray<Integer> listeProfilsExclus;
		
	
	private static List<String> parametreAnnualises = Arrays.asList(parametreFicheVoeuxExistence, 
																	parametreVacatairePrevisionnelExistence,
																	parametreReportBorneInferieur, 
																	parametreReportBorneSuperieur);
	
	/**
	 * @param edc editingContext
	 * @return le type de période choisi : 'S' pour les semestres sinon 'M' pour les mois
	 */
	public static String getChoixTypePeriode(EOEditingContext edc) {
		if (!isTypePeriodeDejaCalculee) {

			EOPecheParametre paramChoixTypePeriode = recupererParametre(edc, getParametreTypePeriode());
			
			typePeriode = EOPeriode.TYPE_MOIS;
			
			if (paramChoixTypePeriode != null) {
				if (paramChoixTypePeriode.value().trim().equals(EOPeriode.TYPE_SEMESTRE)) {
					typePeriode = EOPeriode.TYPE_SEMESTRE;
				}
			}
			isTypePeriodeDejaCalculee = true;		
		}
		
		return typePeriode;
	}
	
	/**
	 * @param edc :editingContext
	 * @param annee : annee
	 * @return Nombre d'heures du report de la borne inférieure
	 */
	public static int getNbHeureReportBorneInferieur(EOEditingContext edc, Integer annee) {
		
		String paramKeyReportBorneInferieur = getParametreReportBorneInferieur(annee);
		
		if (!mapParamAnnualise.containsKey(paramKeyReportBorneInferieur)) {

			EOPecheParametre paramReportInferieur = recupererParametre(edc, paramKeyReportBorneInferieur);
			
			
			if (paramReportInferieur != null) {
				
				int nbHeuresReportBorneInferieur = StringCtrl.toInt(paramReportInferieur.value(), 0);
				mapParamAnnualise.put(paramKeyReportBorneInferieur, nbHeuresReportBorneInferieur);
			}
			
		}
		
		return (Integer) mapParamAnnualise.get(paramKeyReportBorneInferieur);
	}
	
	/**
	 * @param edc :editingContext
	 * @param annee : annee
	 * @return Nombre d'heures du report de la borne supérieure
	 */
	public static int getNbHeureReportBorneSuperieur(EOEditingContext edc, Integer annee) {
		
		String paramKeyReportBorneSuperieur = getParametreReportBorneSuperieur(annee);
		
		if (!mapParamAnnualise.containsKey(paramKeyReportBorneSuperieur)) {

			EOPecheParametre paramReportSuperieur = recupererParametre(edc, paramKeyReportBorneSuperieur);
			
			
			if (paramReportSuperieur != null) {
				
				int nbHeuresReportBorneSuperieur = StringCtrl.toInt(paramReportSuperieur.value(), 0);
				mapParamAnnualise.put(paramKeyReportBorneSuperieur, nbHeuresReportBorneSuperieur);
			}
			
		}
		
		return (Integer) mapParamAnnualise.get(paramKeyReportBorneSuperieur);
	}
	
	/**
	 * @param edc editingContext
	 * @param annee année
	 * @return true si les fiches de voeux sont utilisés par l'établissement
	 */
	public static boolean isFicheVoeuxUtilisation(EOEditingContext edc, Integer annee) {
		
		String paramKeyFicheVoeuxExistence = getParametreFicheVoeuxExistence(annee);
		
		if (!mapParamAnnualise.containsKey(paramKeyFicheVoeuxExistence)) {
		
			EOPecheParametre paramFicheVoeuxExistence = recupererParametre(edc, paramKeyFicheVoeuxExistence);
			
			boolean utilisationFicheVoeux = false;
			
			if (paramFicheVoeuxExistence != null) {
				if (Constante.OUI.equals(paramFicheVoeuxExistence.value().trim())) {
					utilisationFicheVoeux = true;
				}
				mapParamAnnualise.put(paramKeyFicheVoeuxExistence, utilisationFicheVoeux);
			}
		
		}
		
		return  (Boolean) mapParamAnnualise.get(paramKeyFicheVoeuxExistence);
	}
	
	/**
	 * @param edc editingContext
	 * @param annee année
	 * @return true si les fiches de service prévisionnel sont utilisés pour les vacataires
	 */
	public static boolean isFicheVacatairePrevisionnelUtilisation(EOEditingContext edc, Integer annee) {
		
		String paramKeyVacatairePrevisionnel = getParametreVacatairePrevisionnelExistence(annee);
		
		if (!mapParamAnnualise.containsKey(paramKeyVacatairePrevisionnel)) {
			
			EOPecheParametre paramFichePrevisionnelVacataire = recupererParametre(edc, paramKeyVacatairePrevisionnel);
			
			boolean utilisationFichePrevisionnelVacataire = false;
			
			if (paramFichePrevisionnelVacataire != null) {
				if (Constante.OUI.equals(paramFichePrevisionnelVacataire.value().trim())) {
					utilisationFichePrevisionnelVacataire = true;
				}
				mapParamAnnualise.put(paramKeyVacatairePrevisionnel, utilisationFichePrevisionnelVacataire);
			}
			
		}
		
		return (Boolean) mapParamAnnualise.get(paramKeyVacatairePrevisionnel);
	}

	private static EOPecheParametre recupererParametre(EOEditingContext edc, String paramKey) {
		EOPecheParametre parametre = EOPecheParametre.fetchEOPecheParametre(edc, EOPecheParametre.KEY_KEY, paramKey);
		
		if (parametre == null && isParamKeyAnnualise(paramKey)) {
			parametre = creerParametreAnnualise(edc, paramKey);
			edc.saveChanges();
		}
		
		return parametre;
	}
	
	/**
	 * Indique si le paramètre (avec .annee) est un paramètre annualisé
	 * 
	 * @param paramKey
	 * @return true/false
	 */
	private static boolean isParamKeyAnnualise(String paramKey) {
		
		String paramKeyAnnualise = getParamKeyAnnualiseSansAnnee(paramKey);
		
		return isAnnualise(paramKeyAnnualise);
		
	}
	
	
	/**
	 * Indique si le paramètre est un paramètre annualisé
	 * 
	 * @param paramKey
	 * @return true/false
	 */
	public static boolean isAnnualise(String paramKey) {
		
		boolean isAnnualise = false;
		
		if (parametreAnnualises.contains(paramKey)) {
			isAnnualise = true;
		}

		return isAnnualise;
	}
	
	
	/**
	 * Crée un paramètre annualisé 
	 * On recherche le dernier présent en base (par rapport au paramKey) pour y injecter les mêmes valeurs
	 * @param edc un editing context
	 * @param paramKey clé du paramètre
	 * @return {@link EOPecheParametre}
	 */
	private static EOPecheParametre creerParametreAnnualise(EOEditingContext edc, String paramKey) {
		
		String paramKeySansAnnee = getParamKeyAnnualiseSansAnnee(paramKey);
		String anneeParamKey = getAnneeParamKeyAnnualise(paramKey);
		
		List<EOPecheParametre> parametres = EOPecheParametre.fetchEOPecheParametres(edc, ERXQ.startsWith(EOPecheParametre.KEY_KEY, paramKeySansAnnee), CktlSort.newSort(EOPecheParametre.KEY_KEY));
		
		EOPecheParametre dernierParametre = (EOPecheParametre) CollectionUtils.get(parametres, parametres.size() - 1);
		
		String nouveauCommentaire = StringUtils.replace(dernierParametre.commentaire(), getAnneeParamKeyAnnualise(dernierParametre.key()), anneeParamKey);
		
		EOPecheParametre nouveauParametre = createEOPecheParametre(edc, nouveauCommentaire, DateCtrl.now(), DateCtrl.now(), paramKey, dernierParametre.value(), dernierParametre.personneCreation(), dernierParametre.personneModification());
		
		return nouveauParametre;
		
	}
	
	private static String getParamKeyAnnualiseSansAnnee(String paramKey) {
		return paramKey.substring(0, paramKey.lastIndexOf("."));
	}
	
	private static String getAnneeParamKeyAnnualise(String paramKey) {
		return paramKey.substring(paramKey.lastIndexOf(".") + 1, paramKey.length());
	}
	
	/**
	 * @param edc editingContext
	 * @return l'identifiant du profil administrateur
	 */
	public static int getIdProfilAdministrateur(EOEditingContext edc) {
		if (!isProfilAdministrateurDejaCalcule) {
			EOPecheParametre paramProfilAdministrateur = recupererParametre(edc, getParametreProfilAdministrateur());
			
			if (paramProfilAdministrateur != null) {
				idProfilAdministrateur = StringCtrl.toInt(paramProfilAdministrateur.value(), 0);
			}
			isProfilAdministrateurDejaCalcule = true;	
		}
		
		return idProfilAdministrateur;
	}
	
	/**
	 * @param edc editingContext
	 * @return l'identifiant du profil enseignant
	 */
	public static int getIdProfilEnseignant(EOEditingContext edc) {
		if (!isProfilEnseignantDejaCalcule) {
			EOPecheParametre paramProfilEnseignant = recupererParametre(edc, getParametreProfilEnseignant());
			
			if (paramProfilEnseignant != null) {
				idProfilEnseignant = StringCtrl.toInt(paramProfilEnseignant.value(), 0);
			}
			isProfilEnseignantDejaCalcule = true;	
		}
		
		return idProfilEnseignant;
	}
	
	/**
	 * @param edc : editingContext
	 * @return la liste des profils exclus
	 */
	public static NSArray<Integer> getListeIdProfilExclus(EOEditingContext edc) {
		if (!isListeProfilsExclusDejaCalculee) {
			listeProfilsExclus = new NSMutableArray<Integer>();
			listeProfilsExclus.add(EOPecheParametre.getIdProfilAdministrateur(edc));
			listeProfilsExclus.add(EOPecheParametre.getIdProfilEnseignant(edc));
			
			isListeProfilsExclusDejaCalculee = true;
		}
		return listeProfilsExclus;
	}
	
	public static String getParametreTypePeriode() {
		return parametreTypePeriode;
	}

	public static String getParametreReportBorneInferieur(Integer annee) {
		return getParamKeyAnnualise(parametreReportBorneInferieur, annee);
	}

	public static String getParametreReportBorneSuperieur(Integer annee) {
		return getParamKeyAnnualise(parametreReportBorneSuperieur, annee);
	}

	public static String getParametreVacatairePrevisionnelExistence(Integer annee) {
		return getParamKeyAnnualise(parametreVacatairePrevisionnelExistence, annee);
	}

	public static String getParametreFicheVoeuxExistence(Integer annee) {
		return getParamKeyAnnualise(parametreFicheVoeuxExistence, annee);
	}

	public static String getParamKeyAnnualise(String parametre, Integer annee) {
		if (isAnnualise(parametre)) {
			return parametre + "." + annee;
		} else {
			return parametre;
		}
		
	}

	public static String getParametreProfilAdministrateur() {
		return parametreProfilAdministrateur;
	}

	public static String getParametreProfilEnseignant() {
		return parametreProfilEnseignant;
	}

	public static NSArray<Integer> getListeProfilsExclus() {
		return listeProfilsExclus;
	}

	public static String getParametreHCompMethodeDefaut() {
		return parametreHCompMethodeDefaut;
	}
	
	
}
