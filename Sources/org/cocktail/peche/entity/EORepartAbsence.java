package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Classe de gestion des répartitions des heures d'absences pour les enseignants
 * 
 * @author Chama LAATIK
 */
public class EORepartAbsence extends _EORepartAbsence {
	
	private static final long serialVersionUID = -4042721360974523971L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartAbsence.class);
	
	/**
	 * Crée et initialise une nouvelle répartition des heures d'absence
	 *
	 * @param edc : contexte d'edition.
	 * @param uneAbsence : l'absence
	 * @param service : service de l'enseignant
	 * @param personne : personne connecté
	 *
	 * @return la répartition d'absence.
	 */
	public static EORepartAbsence creerEtInitialiser(EOEditingContext edc, EOVueAbsences uneAbsence, EOService service, EOPersonne personne) {
		EORepartAbsence uneRepartAbsence = new EORepartAbsence();
		edc.insertObject(uneRepartAbsence);
		
		uneRepartAbsence.setToAbsence(uneAbsence);
		uneRepartAbsence.setToService(service);
		uneRepartAbsence.setDateCreation(new NSTimestamp());
		uneRepartAbsence.setDateModification(new NSTimestamp());
		uneRepartAbsence.setPersonneCreationRelationship(personne);
		uneRepartAbsence.setPersonneModificationRelationship(personne);
		
		return uneRepartAbsence;
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(new NSTimestamp());
			setPersonneModificationRelationship(personne);
		}
	}
}
