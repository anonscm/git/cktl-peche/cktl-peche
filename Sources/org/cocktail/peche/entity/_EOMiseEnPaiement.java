// DO NOT EDIT.  Make changes to EOMiseEnPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOMiseEnPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOMiseEnPaiement";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_PAYER = new ERXKey<java.math.BigDecimal>("heuresAPayer");
  public static final ERXKey<java.math.BigDecimal> HEURES_A_PAYER_HETD = new ERXKey<java.math.BigDecimal>("heuresAPayerHetd");
  public static final ERXKey<java.math.BigDecimal> HEURES_PAYEES = new ERXKey<java.math.BigDecimal>("heuresPayees");
  public static final ERXKey<java.math.BigDecimal> HEURES_PAYEES_HETD = new ERXKey<java.math.BigDecimal>("heuresPayeesHetd");
  public static final ERXKey<java.math.BigDecimal> TAUX_BRUT = new ERXKey<java.math.BigDecimal>("tauxBrut");
  public static final ERXKey<java.math.BigDecimal> TAUX_CHARGE = new ERXKey<java.math.BigDecimal>("tauxCharge");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOPaiement> TO_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOPaiement>("toPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");
  public static final ERXKey<org.cocktail.peche.entity.EOService> TO_SERVICE = new ERXKey<org.cocktail.peche.entity.EOService>("toService");
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> TO_SERVICE_DETAIL = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("toServiceDetail");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String HEURES_A_PAYER_KEY = HEURES_A_PAYER.key();
  public static final String HEURES_A_PAYER_HETD_KEY = HEURES_A_PAYER_HETD.key();
  public static final String HEURES_PAYEES_KEY = HEURES_PAYEES.key();
  public static final String HEURES_PAYEES_HETD_KEY = HEURES_PAYEES_HETD.key();
  public static final String TAUX_BRUT_KEY = TAUX_BRUT.key();
  public static final String TAUX_CHARGE_KEY = TAUX_CHARGE.key();
  // Relationships
  public static final String TO_PAIEMENT_KEY = TO_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();
  public static final String TO_SERVICE_KEY = TO_SERVICE.key();
  public static final String TO_SERVICE_DETAIL_KEY = TO_SERVICE_DETAIL.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();

  private static Logger LOG = Logger.getLogger(_EOMiseEnPaiement.class);

  public EOMiseEnPaiement localInstanceIn(EOEditingContext editingContext) {
    EOMiseEnPaiement localInstance = (EOMiseEnPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOMiseEnPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOMiseEnPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal heuresAPayer() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.HEURES_A_PAYER_KEY);
  }

  public void setHeuresAPayer(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating heuresAPayer from " + heuresAPayer() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.HEURES_A_PAYER_KEY);
  }

  public java.math.BigDecimal heuresAPayerHetd() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.HEURES_A_PAYER_HETD_KEY);
  }

  public void setHeuresAPayerHetd(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating heuresAPayerHetd from " + heuresAPayerHetd() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.HEURES_A_PAYER_HETD_KEY);
  }

  public java.math.BigDecimal heuresPayees() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.HEURES_PAYEES_KEY);
  }

  public void setHeuresPayees(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating heuresPayees from " + heuresPayees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.HEURES_PAYEES_KEY);
  }

  public java.math.BigDecimal heuresPayeesHetd() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.HEURES_PAYEES_HETD_KEY);
  }

  public void setHeuresPayeesHetd(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating heuresPayeesHetd from " + heuresPayeesHetd() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.HEURES_PAYEES_HETD_KEY);
  }

  public java.math.BigDecimal tauxBrut() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.TAUX_BRUT_KEY);
  }

  public void setTauxBrut(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating tauxBrut from " + tauxBrut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.TAUX_BRUT_KEY);
  }

  public java.math.BigDecimal tauxCharge() {
    return (java.math.BigDecimal) storedValueForKey(_EOMiseEnPaiement.TAUX_CHARGE_KEY);
  }

  public void setTauxCharge(java.math.BigDecimal value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
    	_EOMiseEnPaiement.LOG.debug( "updating tauxCharge from " + tauxCharge() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMiseEnPaiement.TAUX_CHARGE_KEY);
  }

  public org.cocktail.peche.entity.EOPaiement toPaiement() {
    return (org.cocktail.peche.entity.EOPaiement)storedValueForKey(_EOMiseEnPaiement.TO_PAIEMENT_KEY);
  }
  
  public void setToPaiement(org.cocktail.peche.entity.EOPaiement value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_PAIEMENT_KEY);
  }

  public void setToPaiementRelationship(org.cocktail.peche.entity.EOPaiement value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toPaiement from " + toPaiement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPaiement(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOPaiement oldValue = toPaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOService toService() {
    return (org.cocktail.peche.entity.EOService)storedValueForKey(_EOMiseEnPaiement.TO_SERVICE_KEY);
  }
  
  public void setToService(org.cocktail.peche.entity.EOService value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_SERVICE_KEY);
  }

  public void setToServiceRelationship(org.cocktail.peche.entity.EOService value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toService from " + toService() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToService(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOService oldValue = toService();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_SERVICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_SERVICE_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOServiceDetail toServiceDetail() {
    return (org.cocktail.peche.entity.EOServiceDetail)storedValueForKey(_EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY);
  }
  
  public void setToServiceDetail(org.cocktail.peche.entity.EOServiceDetail value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY);
  }

  public void setToServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toServiceDetail from " + toServiceDetail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToServiceDetail(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOServiceDetail oldValue = toServiceDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_SERVICE_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EOMiseEnPaiement.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EOMiseEnPaiement.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOMiseEnPaiement.LOG.isDebugEnabled()) {
      _EOMiseEnPaiement.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMiseEnPaiement.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMiseEnPaiement.TO_STRUCTURE_KEY);
    }
  }
  

  public static EOMiseEnPaiement createEOMiseEnPaiement(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, java.math.BigDecimal heuresAPayer
, java.math.BigDecimal heuresAPayerHetd
, java.math.BigDecimal tauxBrut
, java.math.BigDecimal tauxCharge
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification, org.cocktail.peche.entity.EOService toService) {
    EOMiseEnPaiement eo = (EOMiseEnPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOMiseEnPaiement.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setHeuresAPayer(heuresAPayer);
		eo.setHeuresAPayerHetd(heuresAPayerHetd);
		eo.setTauxBrut(tauxBrut);
		eo.setTauxCharge(tauxCharge);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    eo.setToServiceRelationship(toService);
    return eo;
  }

  public static ERXFetchSpecification<EOMiseEnPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOMiseEnPaiement>(_EOMiseEnPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOMiseEnPaiement> fetchAllEOMiseEnPaiements(EOEditingContext editingContext) {
    return _EOMiseEnPaiement.fetchAllEOMiseEnPaiements(editingContext, null);
  }

  public static NSArray<EOMiseEnPaiement> fetchAllEOMiseEnPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMiseEnPaiement> fetchEOMiseEnPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOMiseEnPaiement> fetchSpec = new ERXFetchSpecification<EOMiseEnPaiement>(_EOMiseEnPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMiseEnPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOMiseEnPaiement fetchEOMiseEnPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMiseEnPaiement.fetchEOMiseEnPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMiseEnPaiement fetchEOMiseEnPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMiseEnPaiement> eoObjects = _EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext, qualifier, null);
    EOMiseEnPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOMiseEnPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMiseEnPaiement fetchRequiredEOMiseEnPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMiseEnPaiement.fetchRequiredEOMiseEnPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMiseEnPaiement fetchRequiredEOMiseEnPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMiseEnPaiement eoObject = _EOMiseEnPaiement.fetchEOMiseEnPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOMiseEnPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMiseEnPaiement localInstanceIn(EOEditingContext editingContext, EOMiseEnPaiement eo) {
    EOMiseEnPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
