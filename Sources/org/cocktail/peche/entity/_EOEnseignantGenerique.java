// DO NOT EDIT.  Make changes to EOEnseignantGenerique.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOEnseignantGenerique extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOEnseignantGenerique";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Double> HEURES_PREVUES = new ERXKey<Double>("heuresPrevues");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> NOM = new ERXKey<String>("nom");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persID");
  public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
  public static final ERXKey<String> TEMOIN_STATUTAIRE = new ERXKey<String>("temoinStatutaire");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String HEURES_PREVUES_KEY = HEURES_PREVUES.key();
  public static final String ID_KEY = ID.key();
  public static final String NOM_KEY = NOM.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PRENOM_KEY = PRENOM.key();
  public static final String TEMOIN_STATUTAIRE_KEY = TEMOIN_STATUTAIRE.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();

  private static Logger LOG = Logger.getLogger(_EOEnseignantGenerique.class);

  public EOEnseignantGenerique localInstanceIn(EOEditingContext editingContext) {
    EOEnseignantGenerique localInstance = (EOEnseignantGenerique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOEnseignantGenerique.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOEnseignantGenerique.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.DATE_MODIFICATION_KEY);
  }

  public Double heuresPrevues() {
    return (Double) storedValueForKey(_EOEnseignantGenerique.HEURES_PREVUES_KEY);
  }

  public void setHeuresPrevues(Double value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating heuresPrevues from " + heuresPrevues() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.HEURES_PREVUES_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOEnseignantGenerique.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.ID_KEY);
  }

  public String nom() {
    return (String) storedValueForKey(_EOEnseignantGenerique.NOM_KEY);
  }

  public void setNom(String value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating nom from " + nom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.NOM_KEY);
  }

  public Integer persID() {
    return (Integer) storedValueForKey(_EOEnseignantGenerique.PERS_ID_KEY);
  }

  public void setPersID(Integer value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating persID from " + persID() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.PERS_ID_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(_EOEnseignantGenerique.PRENOM_KEY);
  }

  public void setPrenom(String value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating prenom from " + prenom() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.PRENOM_KEY);
  }

  public String temoinStatutaire() {
    return (String) storedValueForKey(_EOEnseignantGenerique.TEMOIN_STATUTAIRE_KEY);
  }

  public void setTemoinStatutaire(String value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
    	_EOEnseignantGenerique.LOG.debug( "updating temoinStatutaire from " + temoinStatutaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOEnseignantGenerique.TEMOIN_STATUTAIRE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOEnseignantGenerique.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOEnseignantGenerique.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
      _EOEnseignantGenerique.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEnseignantGenerique.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEnseignantGenerique.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOEnseignantGenerique.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOEnseignantGenerique.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
      _EOEnseignantGenerique.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEnseignantGenerique.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEnseignantGenerique.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EOEnseignantGenerique.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EOEnseignantGenerique.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOEnseignantGenerique.LOG.isDebugEnabled()) {
      _EOEnseignantGenerique.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOEnseignantGenerique.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOEnseignantGenerique.TO_STRUCTURE_KEY);
    }
  }
  

  public static EOEnseignantGenerique createEOEnseignantGenerique(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Double heuresPrevues
, Integer id
, String nom
, String prenom
, String temoinStatutaire
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification) {
    EOEnseignantGenerique eo = (EOEnseignantGenerique) EOUtilities.createAndInsertInstance(editingContext, _EOEnseignantGenerique.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setHeuresPrevues(heuresPrevues);
		eo.setId(id);
		eo.setNom(nom);
		eo.setPrenom(prenom);
		eo.setTemoinStatutaire(temoinStatutaire);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOEnseignantGenerique> fetchSpec() {
    return new ERXFetchSpecification<EOEnseignantGenerique>(_EOEnseignantGenerique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOEnseignantGenerique> fetchAllEOEnseignantGeneriques(EOEditingContext editingContext) {
    return _EOEnseignantGenerique.fetchAllEOEnseignantGeneriques(editingContext, null);
  }

  public static NSArray<EOEnseignantGenerique> fetchAllEOEnseignantGeneriques(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOEnseignantGenerique.fetchEOEnseignantGeneriques(editingContext, null, sortOrderings);
  }

  public static NSArray<EOEnseignantGenerique> fetchEOEnseignantGeneriques(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOEnseignantGenerique> fetchSpec = new ERXFetchSpecification<EOEnseignantGenerique>(_EOEnseignantGenerique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOEnseignantGenerique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOEnseignantGenerique fetchEOEnseignantGenerique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnseignantGenerique.fetchEOEnseignantGenerique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnseignantGenerique fetchEOEnseignantGenerique(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOEnseignantGenerique> eoObjects = _EOEnseignantGenerique.fetchEOEnseignantGeneriques(editingContext, qualifier, null);
    EOEnseignantGenerique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOEnseignantGenerique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnseignantGenerique fetchRequiredEOEnseignantGenerique(EOEditingContext editingContext, String keyName, Object value) {
    return _EOEnseignantGenerique.fetchRequiredEOEnseignantGenerique(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOEnseignantGenerique fetchRequiredEOEnseignantGenerique(EOEditingContext editingContext, EOQualifier qualifier) {
    EOEnseignantGenerique eoObject = _EOEnseignantGenerique.fetchEOEnseignantGenerique(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOEnseignantGenerique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOEnseignantGenerique localInstanceIn(EOEditingContext editingContext, EOEnseignantGenerique eo) {
    EOEnseignantGenerique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
