package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.interfaces.IDroit;
import org.cocktail.peche.entity.interfaces.ITypePopulation;
import org.joda.time.DateTime;

import com.webobjects.eocontrol.EOEditingContext;

public class EODroit extends _EODroit implements IDroit {
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EODroit.class);
	private static final long serialVersionUID = -2971903123930450586L;

	public static EODroit creerEtInitialiser(EOEditingContext ec, EOIndividu utilisateur, EOGdProfil profil, 
											 EOStructure cibleEnseignants, EOStructure cibleEnseignements,
											 boolean temoinEnseignantEnfant, boolean temoinEnseignementEnfant) {
		EODroit droit = new EODroit();
		droit.setToIndividuRelationship(utilisateur);
		droit.setToProfilRelationship(profil);
		droit.setToStructureEnseignantRelationship(cibleEnseignants);
		droit.setToStructureEnseignementRelationship(cibleEnseignements);
		droit.setTemoinEnseignantEnfant(temoinEnseignantEnfant);
		droit.setTemoinEnseignementEnfant(temoinEnseignementEnfant);
		
		return droit;
	}
	
	public static EODroit creerEtInitialiser(EOEditingContext ec) {
		EODroit droit = new EODroit();
		
		return droit;
	}
	
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		DateTime now = DateTime.now();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);
	}
	
	
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(DateTime.now());
			setPersonneModificationRelationship(personne);
		}
	}

	public boolean isTemoinEnseignantEnfant() {
		return temoinEnseignantEnfant() != null && Constante.OUI.equals(temoinEnseignantEnfant());
	}

	
	public void setTemoinEnseignantEnfant(boolean value) {
		if (value) {
			setTemoinEnseignantEnfant(Constante.OUI);
		} else {
			setTemoinEnseignantEnfant(Constante.NON);
		}
	}

	
	
	public boolean isTemoinEnseignementEnfant() {
		return temoinEnseignementEnfant() != null && Constante.OUI.equals(temoinEnseignementEnfant());
	}

	public void setTemoinEnseignementEnfant(boolean value) {
		if (value) {
			setTemoinEnseignementEnfant(Constante.OUI);
		} else {
			setTemoinEnseignementEnfant(Constante.NON);
		}
	}

	public IIndividu getToIndividu() {
		return toIndividu();
	}

	public void setToIndividu(IIndividu value) {
		super.setToIndividu((EOIndividu) value);
	}

	public EOGdProfil getToProfil() {
		return toProfil();
	}

	public IStructure getToStructureEnseignement() {
		return toStructureEnseignement();
	}

	public void setToStructureEnseignement(IStructure value) {
		super.setToStructureEnseignement((EOStructure) value);
	}

	public IStructure getToStructureEnseignant() {
		return toStructureEnseignant();
	}

	public void setToStructureEnseignant(IStructure value) {
		super.setToStructureEnseignant((EOStructure) value);
	}

	public ITypePopulation getToTypePopulation() {
		return toTypePopulation();
	}

	public void setToTypePopulation(ITypePopulation value) {
		super.setToTypePopulation((EOTypePopulation) value);
	}

	public EOPersonne getPersonneCreation() {
		return (EOPersonne) personneCreation();
	}

	public void setPersonneCreation(IPersonne value) {
		super.setPersonneCreation((EOPersonne) value);
	}

	public EOPersonne getPersonneModification() {
		return (EOPersonne) personneModification();
	}

	public void setPersonneModification(IPersonne value) {
		super.setPersonneModification((EOPersonne) value);
	}

	public DateTime getDateCreation() {
		return dateCreation();
	}

	public DateTime getDateModification() {
		return dateModification();
	}
}
