// DO NOT EDIT.  Make changes to EOParamPopHetd.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOParamPopHetd extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOParamPopHetd";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
  public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> DENOMINATEUR_HORS_SERVICE = new ERXKey<Integer>("denominateurHorsService");
  public static final ERXKey<Integer> DENOMINATEUR_SERVICE = new ERXKey<Integer>("denominateurService");
  public static final ERXKey<Integer> NUMERATEUR_HORS_SERVICE = new ERXKey<Integer>("numerateurHorsService");
  public static final ERXKey<Integer> NUMERATEUR_SERVICE = new ERXKey<Integer>("numerateurService");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps> CORPS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCorps>("corps");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> T_CONTRAT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("tContrat");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP> TYPE_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP>("typeAP");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail> TYPE_CONTRAT_TRAVAIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail>("typeContratTravail");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation> TYPE_POPULATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation>("typePopulation");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_DEBUT_KEY = DATE_DEBUT.key();
  public static final String DATE_FIN_KEY = DATE_FIN.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DENOMINATEUR_HORS_SERVICE_KEY = DENOMINATEUR_HORS_SERVICE.key();
  public static final String DENOMINATEUR_SERVICE_KEY = DENOMINATEUR_SERVICE.key();
  public static final String NUMERATEUR_HORS_SERVICE_KEY = NUMERATEUR_HORS_SERVICE.key();
  public static final String NUMERATEUR_SERVICE_KEY = NUMERATEUR_SERVICE.key();
  // Relationships
  public static final String CORPS_KEY = CORPS.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String T_CONTRAT_KEY = T_CONTRAT.key();
  public static final String TYPE_AP_KEY = TYPE_AP.key();
  public static final String TYPE_CONTRAT_TRAVAIL_KEY = TYPE_CONTRAT_TRAVAIL.key();
  public static final String TYPE_POPULATION_KEY = TYPE_POPULATION.key();

  private static Logger LOG = Logger.getLogger(_EOParamPopHetd.class);

  public EOParamPopHetd localInstanceIn(EOEditingContext editingContext) {
    EOParamPopHetd localInstance = (EOParamPopHetd)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOParamPopHetd.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DATE_CREATION_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(_EOParamPopHetd.DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(_EOParamPopHetd.DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DATE_FIN_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOParamPopHetd.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DATE_MODIFICATION_KEY);
  }

  public Integer denominateurHorsService() {
    return (Integer) storedValueForKey(_EOParamPopHetd.DENOMINATEUR_HORS_SERVICE_KEY);
  }

  public void setDenominateurHorsService(Integer value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating denominateurHorsService from " + denominateurHorsService() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DENOMINATEUR_HORS_SERVICE_KEY);
  }

  public Integer denominateurService() {
    return (Integer) storedValueForKey(_EOParamPopHetd.DENOMINATEUR_SERVICE_KEY);
  }

  public void setDenominateurService(Integer value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating denominateurService from " + denominateurService() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.DENOMINATEUR_SERVICE_KEY);
  }

  public Integer numerateurHorsService() {
    return (Integer) storedValueForKey(_EOParamPopHetd.NUMERATEUR_HORS_SERVICE_KEY);
  }

  public void setNumerateurHorsService(Integer value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating numerateurHorsService from " + numerateurHorsService() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.NUMERATEUR_HORS_SERVICE_KEY);
  }

  public Integer numerateurService() {
    return (Integer) storedValueForKey(_EOParamPopHetd.NUMERATEUR_SERVICE_KEY);
  }

  public void setNumerateurService(Integer value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
    	_EOParamPopHetd.LOG.debug( "updating numerateurService from " + numerateurService() + " to " + value);
    }
    takeStoredValueForKey(value, _EOParamPopHetd.NUMERATEUR_SERVICE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCorps corps() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCorps)storedValueForKey(_EOParamPopHetd.CORPS_KEY);
  }
  
  public void setCorps(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    takeStoredValueForKey(value, _EOParamPopHetd.CORPS_KEY);
  }

  public void setCorpsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCorps value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating corps from " + corps() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setCorps(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCorps oldValue = corps();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.CORPS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.CORPS_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOParamPopHetd.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOParamPopHetd.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOParamPopHetd.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOParamPopHetd.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail tContrat() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey(_EOParamPopHetd.T_CONTRAT_KEY);
  }
  
  public void setTContrat(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    takeStoredValueForKey(value, _EOParamPopHetd.T_CONTRAT_KEY);
  }

  public void setTContratRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating tContrat from " + tContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTContrat(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = tContrat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.T_CONTRAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.T_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP typeAP() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP)storedValueForKey(_EOParamPopHetd.TYPE_AP_KEY);
  }
  
  public void setTypeAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP value) {
    takeStoredValueForKey(value, _EOParamPopHetd.TYPE_AP_KEY);
  }

  public void setTypeAPRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating typeAP from " + typeAP() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeAP(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP oldValue = typeAP();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.TYPE_AP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.TYPE_AP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail typeContratTravail() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail)storedValueForKey(_EOParamPopHetd.TYPE_CONTRAT_TRAVAIL_KEY);
  }
  
  public void setTypeContratTravail(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    takeStoredValueForKey(value, _EOParamPopHetd.TYPE_CONTRAT_TRAVAIL_KEY);
  }

  public void setTypeContratTravailRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating typeContratTravail from " + typeContratTravail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypeContratTravail(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail oldValue = typeContratTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.TYPE_CONTRAT_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.TYPE_CONTRAT_TRAVAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation typePopulation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation)storedValueForKey(_EOParamPopHetd.TYPE_POPULATION_KEY);
  }
  
  public void setTypePopulation(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    takeStoredValueForKey(value, _EOParamPopHetd.TYPE_POPULATION_KEY);
  }

  public void setTypePopulationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    if (_EOParamPopHetd.LOG.isDebugEnabled()) {
      _EOParamPopHetd.LOG.debug("updating typePopulation from " + typePopulation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypePopulation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation oldValue = typePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOParamPopHetd.TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOParamPopHetd.TYPE_POPULATION_KEY);
    }
  }
  

  public static EOParamPopHetd createEOParamPopHetd(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateDebut
, NSTimestamp dateModification
, Integer denominateurHorsService
, Integer denominateurService
, Integer numerateurHorsService
, Integer numerateurService
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP typeAP) {
    EOParamPopHetd eo = (EOParamPopHetd) EOUtilities.createAndInsertInstance(editingContext, _EOParamPopHetd.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateDebut(dateDebut);
		eo.setDateModification(dateModification);
		eo.setDenominateurHorsService(denominateurHorsService);
		eo.setDenominateurService(denominateurService);
		eo.setNumerateurHorsService(numerateurHorsService);
		eo.setNumerateurService(numerateurService);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    eo.setTypeAPRelationship(typeAP);
    return eo;
  }

  public static ERXFetchSpecification<EOParamPopHetd> fetchSpec() {
    return new ERXFetchSpecification<EOParamPopHetd>(_EOParamPopHetd.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOParamPopHetd> fetchAllEOParamPopHetds(EOEditingContext editingContext) {
    return _EOParamPopHetd.fetchAllEOParamPopHetds(editingContext, null);
  }

  public static NSArray<EOParamPopHetd> fetchAllEOParamPopHetds(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOParamPopHetd.fetchEOParamPopHetds(editingContext, null, sortOrderings);
  }

  public static NSArray<EOParamPopHetd> fetchEOParamPopHetds(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOParamPopHetd> fetchSpec = new ERXFetchSpecification<EOParamPopHetd>(_EOParamPopHetd.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOParamPopHetd> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOParamPopHetd fetchEOParamPopHetd(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamPopHetd.fetchEOParamPopHetd(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamPopHetd fetchEOParamPopHetd(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOParamPopHetd> eoObjects = _EOParamPopHetd.fetchEOParamPopHetds(editingContext, qualifier, null);
    EOParamPopHetd eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOParamPopHetd that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamPopHetd fetchRequiredEOParamPopHetd(EOEditingContext editingContext, String keyName, Object value) {
    return _EOParamPopHetd.fetchRequiredEOParamPopHetd(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOParamPopHetd fetchRequiredEOParamPopHetd(EOEditingContext editingContext, EOQualifier qualifier) {
    EOParamPopHetd eoObject = _EOParamPopHetd.fetchEOParamPopHetd(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOParamPopHetd that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOParamPopHetd localInstanceIn(EOEditingContext editingContext, EOParamPopHetd eo) {
    EOParamPopHetd localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
