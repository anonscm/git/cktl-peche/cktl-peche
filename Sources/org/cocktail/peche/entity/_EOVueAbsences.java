// DO NOT EDIT.  Make changes to EOVueAbsences.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOVueAbsences extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOVueAbsences";

  // Attribute Keys
  public static final ERXKey<String> CODE_ABSENCE = new ERXKey<String>("codeAbsence");
  public static final ERXKey<NSTimestamp> DATE_DEBUT = new ERXKey<NSTimestamp>("dateDebut");
  public static final ERXKey<NSTimestamp> DATE_FIN = new ERXKey<NSTimestamp>("dateFin");
  public static final ERXKey<Integer> ID_ABSENCE = new ERXKey<Integer>("idAbsence");
  public static final ERXKey<Integer> QUOTITE = new ERXKey<Integer>("quotite");
  public static final ERXKey<String> TYPE = new ERXKey<String>("type");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
  public static final ERXKey<org.cocktail.peche.entity.EORepartAbsence> TO_LISTE_REPART_ABSENCES = new ERXKey<org.cocktail.peche.entity.EORepartAbsence>("toListeRepartAbsences");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence> TO_TYPE_ABSENCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence>("toTypeAbsence");

  // Attributes
  public static final String CODE_ABSENCE_KEY = CODE_ABSENCE.key();
  public static final String DATE_DEBUT_KEY = DATE_DEBUT.key();
  public static final String DATE_FIN_KEY = DATE_FIN.key();
  public static final String ID_ABSENCE_KEY = ID_ABSENCE.key();
  public static final String QUOTITE_KEY = QUOTITE.key();
  public static final String TYPE_KEY = TYPE.key();
  // Relationships
  public static final String TO_INDIVIDU_KEY = TO_INDIVIDU.key();
  public static final String TO_LISTE_REPART_ABSENCES_KEY = TO_LISTE_REPART_ABSENCES.key();
  public static final String TO_TYPE_ABSENCE_KEY = TO_TYPE_ABSENCE.key();

  private static Logger LOG = Logger.getLogger(_EOVueAbsences.class);

  public EOVueAbsences localInstanceIn(EOEditingContext editingContext) {
    EOVueAbsences localInstance = (EOVueAbsences)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeAbsence() {
    return (String) storedValueForKey(_EOVueAbsences.CODE_ABSENCE_KEY);
  }

  public void setCodeAbsence(String value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating codeAbsence from " + codeAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.CODE_ABSENCE_KEY);
  }

  public NSTimestamp dateDebut() {
    return (NSTimestamp) storedValueForKey(_EOVueAbsences.DATE_DEBUT_KEY);
  }

  public void setDateDebut(NSTimestamp value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating dateDebut from " + dateDebut() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.DATE_DEBUT_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(_EOVueAbsences.DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating dateFin from " + dateFin() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.DATE_FIN_KEY);
  }

  public Integer idAbsence() {
    return (Integer) storedValueForKey(_EOVueAbsences.ID_ABSENCE_KEY);
  }

  public void setIdAbsence(Integer value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating idAbsence from " + idAbsence() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.ID_ABSENCE_KEY);
  }

  public Integer quotite() {
    return (Integer) storedValueForKey(_EOVueAbsences.QUOTITE_KEY);
  }

  public void setQuotite(Integer value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating quotite from " + quotite() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.QUOTITE_KEY);
  }

  public String type() {
    return (String) storedValueForKey(_EOVueAbsences.TYPE_KEY);
  }

  public void setType(String value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
    	_EOVueAbsences.LOG.debug( "updating type from " + type() + " to " + value);
    }
    takeStoredValueForKey(value, _EOVueAbsences.TYPE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(_EOVueAbsences.TO_INDIVIDU_KEY);
  }
  
  public void setToIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, _EOVueAbsences.TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
      _EOVueAbsences.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToIndividu(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOVueAbsences.TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOVueAbsences.TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence)storedValueForKey(_EOVueAbsences.TO_TYPE_ABSENCE_KEY);
  }
  
  public void setToTypeAbsence(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    takeStoredValueForKey(value, _EOVueAbsences.TO_TYPE_ABSENCE_KEY);
  }

  public void setToTypeAbsenceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence value) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
      _EOVueAbsences.LOG.debug("updating toTypeAbsence from " + toTypeAbsence() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypeAbsence(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence oldValue = toTypeAbsence();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOVueAbsences.TO_TYPE_ABSENCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOVueAbsences.TO_TYPE_ABSENCE_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EORepartAbsence> toListeRepartAbsences() {
    return (NSArray<org.cocktail.peche.entity.EORepartAbsence>)storedValueForKey(_EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EORepartAbsence> toListeRepartAbsences(EOQualifier qualifier) {
    return toListeRepartAbsences(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EORepartAbsence> toListeRepartAbsences(EOQualifier qualifier, boolean fetch) {
    return toListeRepartAbsences(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EORepartAbsence> toListeRepartAbsences(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EORepartAbsence> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EORepartAbsence.TO_ABSENCE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EORepartAbsence.fetchEORepartAbsences(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeRepartAbsences();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartAbsence>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EORepartAbsence>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeRepartAbsences(org.cocktail.peche.entity.EORepartAbsence object) {
    includeObjectIntoPropertyWithKey(object, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
  }

  public void removeFromToListeRepartAbsences(org.cocktail.peche.entity.EORepartAbsence object) {
    excludeObjectFromPropertyWithKey(object, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
  }

  public void addToToListeRepartAbsencesRelationship(org.cocktail.peche.entity.EORepartAbsence object) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
      _EOVueAbsences.LOG.debug("adding " + object + " to toListeRepartAbsences relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeRepartAbsences(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
    }
  }

  public void removeFromToListeRepartAbsencesRelationship(org.cocktail.peche.entity.EORepartAbsence object) {
    if (_EOVueAbsences.LOG.isDebugEnabled()) {
      _EOVueAbsences.LOG.debug("removing " + object + " from toListeRepartAbsences relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeRepartAbsences(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
    }
  }

  public org.cocktail.peche.entity.EORepartAbsence createToListeRepartAbsencesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EORepartAbsence.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
    return (org.cocktail.peche.entity.EORepartAbsence) eo;
  }

  public void deleteToListeRepartAbsencesRelationship(org.cocktail.peche.entity.EORepartAbsence object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOVueAbsences.TO_LISTE_REPART_ABSENCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeRepartAbsencesRelationships() {
    Enumeration<org.cocktail.peche.entity.EORepartAbsence> objects = toListeRepartAbsences().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeRepartAbsencesRelationship(objects.nextElement());
    }
  }


  public static EOVueAbsences createEOVueAbsences(EOEditingContext editingContext, String codeAbsence
, NSTimestamp dateDebut
, NSTimestamp dateFin
, Integer idAbsence
, String type
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence toTypeAbsence) {
    EOVueAbsences eo = (EOVueAbsences) EOUtilities.createAndInsertInstance(editingContext, _EOVueAbsences.ENTITY_NAME);    
		eo.setCodeAbsence(codeAbsence);
		eo.setDateDebut(dateDebut);
		eo.setDateFin(dateFin);
		eo.setIdAbsence(idAbsence);
		eo.setType(type);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToTypeAbsenceRelationship(toTypeAbsence);
    return eo;
  }

  public static ERXFetchSpecification<EOVueAbsences> fetchSpec() {
    return new ERXFetchSpecification<EOVueAbsences>(_EOVueAbsences.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOVueAbsences> fetchAllEOVueAbsenceses(EOEditingContext editingContext) {
    return _EOVueAbsences.fetchAllEOVueAbsenceses(editingContext, null);
  }

  public static NSArray<EOVueAbsences> fetchAllEOVueAbsenceses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVueAbsences.fetchEOVueAbsenceses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVueAbsences> fetchEOVueAbsenceses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOVueAbsences> fetchSpec = new ERXFetchSpecification<EOVueAbsences>(_EOVueAbsences.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVueAbsences> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOVueAbsences fetchEOVueAbsences(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVueAbsences.fetchEOVueAbsences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVueAbsences fetchEOVueAbsences(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVueAbsences> eoObjects = _EOVueAbsences.fetchEOVueAbsenceses(editingContext, qualifier, null);
    EOVueAbsences eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOVueAbsences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVueAbsences fetchRequiredEOVueAbsences(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVueAbsences.fetchRequiredEOVueAbsences(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVueAbsences fetchRequiredEOVueAbsences(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVueAbsences eoObject = _EOVueAbsences.fetchEOVueAbsences(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOVueAbsences that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVueAbsences localInstanceIn(EOEditingContext editingContext, EOVueAbsences eo) {
    EOVueAbsences localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
