package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Représente le détail d'une UE (commentaire, etc...).
 * @author Equipe GRH
 */
public class EORepartUefDetail extends _EORepartUefDetail {

	private static final long serialVersionUID = -5182102830117173781L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartUefDetail.class);

	/**
	 * @param edc l'editing context
	 * @param ue l'UE dont on recherche le détail
	 * @return le détail de l'UE
	 */
	public static EORepartUefDetail rechercherDetailForUE(EOEditingContext edc, EOUE ue){
		EORepartUefDetail result = EORepartUefDetail.fetchEORepartUefDetail(edc, EORepartUefDetail.COMPOSANT_KEY, ue);
		return result;
	}
	
	/**
	 * Crée et initialise l'association entre l'UE flottante et le détail de l'UE.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return une répartition.
	 */
	public static EORepartUefDetail creerEtInitialiser(EOEditingContext edc) {
		EORepartUefDetail repartition = new EORepartUefDetail();
		edc.insertObject(repartition);
		return repartition;
	}
	
}
