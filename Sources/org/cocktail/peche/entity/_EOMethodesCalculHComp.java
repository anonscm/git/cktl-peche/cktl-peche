// DO NOT EDIT.  Make changes to EOMethodesCalculHComp.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOMethodesCalculHComp extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOMethodesCalculHComp";

  // Attribute Keys
  public static final ERXKey<String> CLASSE_METHODE = new ERXKey<String>("classeMethode");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> ID_PERSONNE_CREATION = new ERXKey<Integer>("idPersonneCreation");
  public static final ERXKey<Integer> ID_PERSONNE_MODIFICATION = new ERXKey<Integer>("idPersonneModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String CLASSE_METHODE_KEY = CLASSE_METHODE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String ID_PERSONNE_CREATION_KEY = ID_PERSONNE_CREATION.key();
  public static final String ID_PERSONNE_MODIFICATION_KEY = ID_PERSONNE_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOMethodesCalculHComp.class);

  public EOMethodesCalculHComp localInstanceIn(EOEditingContext editingContext) {
    EOMethodesCalculHComp localInstance = (EOMethodesCalculHComp)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String classeMethode() {
    return (String) storedValueForKey(_EOMethodesCalculHComp.CLASSE_METHODE_KEY);
  }

  public void setClasseMethode(String value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating classeMethode from " + classeMethode() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.CLASSE_METHODE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOMethodesCalculHComp.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOMethodesCalculHComp.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.DATE_MODIFICATION_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOMethodesCalculHComp.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.ID_KEY);
  }

  public Integer idPersonneCreation() {
    return (Integer) storedValueForKey(_EOMethodesCalculHComp.ID_PERSONNE_CREATION_KEY);
  }

  public void setIdPersonneCreation(Integer value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating idPersonneCreation from " + idPersonneCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.ID_PERSONNE_CREATION_KEY);
  }

  public Integer idPersonneModification() {
    return (Integer) storedValueForKey(_EOMethodesCalculHComp.ID_PERSONNE_MODIFICATION_KEY);
  }

  public void setIdPersonneModification(Integer value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating idPersonneModification from " + idPersonneModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.ID_PERSONNE_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOMethodesCalculHComp.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
    	_EOMethodesCalculHComp.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOMethodesCalculHComp.LIBELLE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOMethodesCalculHComp.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOMethodesCalculHComp.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
      _EOMethodesCalculHComp.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMethodesCalculHComp.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMethodesCalculHComp.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOMethodesCalculHComp.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOMethodesCalculHComp.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOMethodesCalculHComp.LOG.isDebugEnabled()) {
      _EOMethodesCalculHComp.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOMethodesCalculHComp.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOMethodesCalculHComp.PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EOMethodesCalculHComp createEOMethodesCalculHComp(EOEditingContext editingContext, String classeMethode
, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer id
, Integer idPersonneCreation
, Integer idPersonneModification
, String libelle
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification) {
    EOMethodesCalculHComp eo = (EOMethodesCalculHComp) EOUtilities.createAndInsertInstance(editingContext, _EOMethodesCalculHComp.ENTITY_NAME);    
		eo.setClasseMethode(classeMethode);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
		eo.setIdPersonneCreation(idPersonneCreation);
		eo.setIdPersonneModification(idPersonneModification);
		eo.setLibelle(libelle);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOMethodesCalculHComp> fetchSpec() {
    return new ERXFetchSpecification<EOMethodesCalculHComp>(_EOMethodesCalculHComp.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOMethodesCalculHComp> fetchAllEOMethodesCalculHComps(EOEditingContext editingContext) {
    return _EOMethodesCalculHComp.fetchAllEOMethodesCalculHComps(editingContext, null);
  }

  public static NSArray<EOMethodesCalculHComp> fetchAllEOMethodesCalculHComps(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOMethodesCalculHComp.fetchEOMethodesCalculHComps(editingContext, null, sortOrderings);
  }

  public static NSArray<EOMethodesCalculHComp> fetchEOMethodesCalculHComps(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOMethodesCalculHComp> fetchSpec = new ERXFetchSpecification<EOMethodesCalculHComp>(_EOMethodesCalculHComp.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOMethodesCalculHComp> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOMethodesCalculHComp fetchEOMethodesCalculHComp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMethodesCalculHComp.fetchEOMethodesCalculHComp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMethodesCalculHComp fetchEOMethodesCalculHComp(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOMethodesCalculHComp> eoObjects = _EOMethodesCalculHComp.fetchEOMethodesCalculHComps(editingContext, qualifier, null);
    EOMethodesCalculHComp eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOMethodesCalculHComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMethodesCalculHComp fetchRequiredEOMethodesCalculHComp(EOEditingContext editingContext, String keyName, Object value) {
    return _EOMethodesCalculHComp.fetchRequiredEOMethodesCalculHComp(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOMethodesCalculHComp fetchRequiredEOMethodesCalculHComp(EOEditingContext editingContext, EOQualifier qualifier) {
    EOMethodesCalculHComp eoObject = _EOMethodesCalculHComp.fetchEOMethodesCalculHComp(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOMethodesCalculHComp that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOMethodesCalculHComp localInstanceIn(EOEditingContext editingContext, EOMethodesCalculHComp eo) {
    EOMethodesCalculHComp localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
