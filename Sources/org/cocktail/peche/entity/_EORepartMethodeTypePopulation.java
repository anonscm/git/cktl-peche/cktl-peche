// DO NOT EDIT.  Make changes to EORepartMethodeTypePopulation.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartMethodeTypePopulation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartMethodeTypePopulation";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp> METHODE = new ERXKey<org.cocktail.peche.entity.EOMethodesCalculHComp>("methode");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation> TYPE_POPULATION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation>("typePopulation");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  // Relationships
  public static final String METHODE_KEY = METHODE.key();
  public static final String TYPE_POPULATION_KEY = TYPE_POPULATION.key();

  private static Logger LOG = Logger.getLogger(_EORepartMethodeTypePopulation.class);

  public EORepartMethodeTypePopulation localInstanceIn(EOEditingContext editingContext) {
    EORepartMethodeTypePopulation localInstance = (EORepartMethodeTypePopulation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeTypePopulation.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartMethodeTypePopulation.LOG.isDebugEnabled()) {
    	_EORepartMethodeTypePopulation.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeTypePopulation.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartMethodeTypePopulation.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartMethodeTypePopulation.LOG.isDebugEnabled()) {
    	_EORepartMethodeTypePopulation.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartMethodeTypePopulation.DATE_MODIFICATION_KEY);
  }

  public org.cocktail.peche.entity.EOMethodesCalculHComp methode() {
    return (org.cocktail.peche.entity.EOMethodesCalculHComp)storedValueForKey(_EORepartMethodeTypePopulation.METHODE_KEY);
  }
  
  public void setMethode(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    takeStoredValueForKey(value, _EORepartMethodeTypePopulation.METHODE_KEY);
  }

  public void setMethodeRelationship(org.cocktail.peche.entity.EOMethodesCalculHComp value) {
    if (_EORepartMethodeTypePopulation.LOG.isDebugEnabled()) {
      _EORepartMethodeTypePopulation.LOG.debug("updating methode from " + methode() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setMethode(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOMethodesCalculHComp oldValue = methode();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeTypePopulation.METHODE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeTypePopulation.METHODE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation typePopulation() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation)storedValueForKey(_EORepartMethodeTypePopulation.TYPE_POPULATION_KEY);
  }
  
  public void setTypePopulation(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    takeStoredValueForKey(value, _EORepartMethodeTypePopulation.TYPE_POPULATION_KEY);
  }

  public void setTypePopulationRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation value) {
    if (_EORepartMethodeTypePopulation.LOG.isDebugEnabled()) {
      _EORepartMethodeTypePopulation.LOG.debug("updating typePopulation from " + typePopulation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setTypePopulation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation oldValue = typePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartMethodeTypePopulation.TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartMethodeTypePopulation.TYPE_POPULATION_KEY);
    }
  }
  

  public static EORepartMethodeTypePopulation createEORepartMethodeTypePopulation(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, org.cocktail.peche.entity.EOMethodesCalculHComp methode, org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation typePopulation) {
    EORepartMethodeTypePopulation eo = (EORepartMethodeTypePopulation) EOUtilities.createAndInsertInstance(editingContext, _EORepartMethodeTypePopulation.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
    eo.setMethodeRelationship(methode);
    eo.setTypePopulationRelationship(typePopulation);
    return eo;
  }

  public static ERXFetchSpecification<EORepartMethodeTypePopulation> fetchSpec() {
    return new ERXFetchSpecification<EORepartMethodeTypePopulation>(_EORepartMethodeTypePopulation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartMethodeTypePopulation> fetchAllEORepartMethodeTypePopulations(EOEditingContext editingContext) {
    return _EORepartMethodeTypePopulation.fetchAllEORepartMethodeTypePopulations(editingContext, null);
  }

  public static NSArray<EORepartMethodeTypePopulation> fetchAllEORepartMethodeTypePopulations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulations(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartMethodeTypePopulation> fetchEORepartMethodeTypePopulations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartMethodeTypePopulation> fetchSpec = new ERXFetchSpecification<EORepartMethodeTypePopulation>(_EORepartMethodeTypePopulation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartMethodeTypePopulation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartMethodeTypePopulation fetchEORepartMethodeTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeTypePopulation fetchEORepartMethodeTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartMethodeTypePopulation> eoObjects = _EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulations(editingContext, qualifier, null);
    EORepartMethodeTypePopulation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartMethodeTypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeTypePopulation fetchRequiredEORepartMethodeTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartMethodeTypePopulation.fetchRequiredEORepartMethodeTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartMethodeTypePopulation fetchRequiredEORepartMethodeTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartMethodeTypePopulation eoObject = _EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartMethodeTypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartMethodeTypePopulation localInstanceIn(EOEditingContext editingContext, EORepartMethodeTypePopulation eo) {
    EORepartMethodeTypePopulation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
