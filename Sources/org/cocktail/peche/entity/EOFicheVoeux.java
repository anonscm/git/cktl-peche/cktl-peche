package org.cocktail.peche.entity;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.interfaces.IFiche;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente la fiche de voeux de l'enseignant
 *
 * @author Chama LAATIK
 */
public class EOFicheVoeux extends _EOFicheVoeux implements IFiche {

	private static final long serialVersionUID = 7084485538516813993L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOFicheVoeux.class);

	/**
	 * Crée et initialise une fiche de voeux sur le circuit de demande par défaut.
	 *
	 * @param edc : contexte d'edition.
	 * @param persId : personne connecté
	 *
	 * @return une fiche de voeux.
	 */
	public static EOFicheVoeux creerEtInitialiser(EOEditingContext edc, Integer persId) {
		EOFicheVoeux ficheVoeux = new EOFicheVoeux();
		edc.insertObject(ficheVoeux);

		attachNewDemande(edc, persId, ficheVoeux);

		return ficheVoeux;
	}


	private static void attachNewDemande(EOEditingContext edc, Integer persId,
			EOFicheVoeux ficheVoeux) {
		EODemande demande = EODemande.creerDemande(edc, Constante.APP_PECHE, CircuitPeche.PECHE_VOEUX.getCodeCircuit(), persId);
		ficheVoeux.setToDemande(demande);
	}

	
	/**
	 * Mise à jour des données d'audit lorsqu'on est en création d'un enregistrement.
	 * 
	 * @param now Une date/heure
	 * @param personne La personne qui crée l'enregistrement
	 */
	public void majDonneesAuditCreation(NSTimestamp now, EOPersonne personne) {
		setPersonneCreation(personne);
		setDateCreation(now);
		setPersonneModification(personne);
		setDateModification(now);
		if (!StringCtrl.isEmpty(commentaire())) {
			setDateCommentaire(now);
		}
	}
	
	/**
	 * Mise à jour des données.
	 * 
	 * @param now Une date/heure
	 * @param personne La personne qui modifie l'enregistrement
	 */
	
	public void majDonnees(EOEditingContext edc,NSTimestamp now, EOPersonne personne, Integer annee, boolean isFormatAnneeExerciceAnneeCivile)	{
	
		HceCtrl hceCtrl = new HceCtrl(edc);		
		double serviceDu = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceDuStatutaire(this.enseignant(), annee, isFormatAnneeExerciceAnneeCivile));
		double serviceSouhaitees = HceCtrl.arrondirAuCentieme(hceCtrl.getServiceSouhaite(this.enseignant(), annee, isFormatAnneeExerciceAnneeCivile, this, serviceDu));

		setHeuresServiceDu(BigDecimal.valueOf(serviceDu));
		setHeuresSouhaitees(BigDecimal.valueOf(serviceSouhaitees));
		majDonneesAuditCreation(now, personne);
	}
	
	/**
	 * @return les heures du service dû: 0 si null
	 */
	public BigDecimal getHeuresServiceDu() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresServiceDu());
	}
	
	/**
	 * @return les heures du service souhaite : 0 si null
	 */
	public BigDecimal getHeuresServiceSouhaite() {
		return OutilsValidation.retournerBigDecimalNonNull(super.heuresSouhaitees());
	}
	
	/**
	 * Ajout suivi :
	 * s'il existe dèjà, on l'ajoute au début avec un retour à la ligne
	 * attention, on tronque à 1000 carac.
	 * @param suivi le suivi à ajouter
	 */
	public void addSuivi(String suivi) {		
		StringBuffer ajoutSuivi = new StringBuffer();
		int longueurAjout = suivi.length();
		int longueurMax = 1000;
		
		if (longueurAjout > longueurMax) {
			setSuivi(suivi.substring(0, longueurMax).toString());
			return;
		}		
		if (suivi() != null) {
			int longueurActuelle = suivi().length();
			
			if ((longueurActuelle + longueurAjout) > longueurMax) {				
				ajoutSuivi.append(suivi).append("\n");
				ajoutSuivi.append(suivi().substring(0, longueurMax - longueurAjout));
				ajoutSuivi.append("...");
			} else {
				ajoutSuivi.append(suivi).append("\n");
				ajoutSuivi.append(suivi());
			}
		} else {
			ajoutSuivi.append(suivi);
		}
		
		setSuivi(ajoutSuivi.toString());
	}
	
	/**
	 * 
	 * @return true si un suivi est présent
	 */
	public boolean isSuiviPresent() {
		return (suivi() != null);
	}

	/**
	 * {@inheritDoc}
	 */
	public void reinitialiserDemandes(EOEditingContext edc, Integer persId, EOCircuitValidation circuitValidation) {
		this.toDemande().supprimerAvecHistorique();
		attachNewDemande(edc, persId, this);
	}
}
