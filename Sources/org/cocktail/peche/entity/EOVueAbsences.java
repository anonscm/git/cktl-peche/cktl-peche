package org.cocktail.peche.entity;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAbsence;
import org.cocktail.peche.components.commun.Constante;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Cette classe représente la vue des absences gérées dans PECHE
 * 
 * @author Chama LAATIK
 */
public class EOVueAbsences extends _EOVueAbsences {
	
	private static final long serialVersionUID = -5211339476065900792L;
	
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOVueAbsences.class);
	
	
	/**
	 * Renvoie la liste des absences validées d'un enseignant pendant l'année universitaire 
	 * filtrées selon certains types d'absence (minoration, modalité de service et position)
	 * 
	 * @param ec : editing context
	 * @param individu : enseignant 
	 * @param annee : année universitaire
	 * @return liste des absences
	 */
	public static NSArray<EOVueAbsences> rechercheAbsenceCouranteAnneeUniversitairePourEnseignant(EOEditingContext ec, EOIndividu individu, 
			int annee, boolean isFormatAnneeExerciceAnneeCivile) {	
		return fetchEOVueAbsenceses(ec, qualifierAbsenceCouranteAnneeUniversitaire(individu, annee, isFormatAnneeExerciceAnneeCivile), DATE_DEBUT.ascs());
	}
	
	/**
	 * Qualifier pour avoir les absences valides d'un individu pendant l'année universitaire
	 * @param individu : enseignant
	 * @param annee : année universitaire
	 * @return qualifier
	 */
	public static EOQualifier qualifierAbsenceCouranteAnneeUniversitaire(EOIndividu individu, int anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		NSTimestamp debut = null;
		NSTimestamp fin = null;
		EOQualifier qualifier = null;

		SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
		try {
			if (isFormatAnneeExerciceAnneeCivile) {
				debut = new NSTimestamp(DATE_FORMAT.parse(String.format("01/01/%d", anneeUniversitaire)));
				fin = new NSTimestamp(DATE_FORMAT.parse(String.format("31/12/%d", anneeUniversitaire)));
			} else 	{
				debut = new NSTimestamp(DATE_FORMAT.parse(String.format("01/09/%d", anneeUniversitaire)));
				fin = new NSTimestamp(DATE_FORMAT.parse(String.format("31/08/%d", anneeUniversitaire + 1)));

			}
		} catch (ParseException e) {
			// non traité
		}

		if (debut != null && fin != null) {
			qualifier =	ERXQ.and(
					ERXQ.equals(ERXQ.keyPath(EOVueAbsences.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY), individu.noIndividu()), 
					EOVueAbsences.DATE_DEBUT.before(fin),
					EOVueAbsences.DATE_FIN.after(debut));

		}
		return qualifier;
	}

	/**
	 * Renvoie la liste des modalités de service d'un enseignant pendant l'année universitaire 
	 * 
	 * @param ec : editing context
	 * @param individu : enseignant 
	 * @param annee : année universitaire
	 * @return liste des modalités de service
	 */
	public static NSArray<EOVueAbsences> rechercherModalitesCourantesAnneeUniversitaire(EOEditingContext ec, EOIndividu individu, int annee, boolean isFormatAnneeExerciceAnneeCivile) {
		EOQualifier qualifier = 
				ERXQ.and(
						qualifierAbsenceCouranteAnneeUniversitaire(individu, annee, isFormatAnneeExerciceAnneeCivile),						
						qualifierPourModalite()
						);
		return fetchEOVueAbsenceses(ec, qualifier, DATE_DEBUT.ascs());
	}
	
	/**
	 * @return qualifier pour récupérer les modalités de service 
	 */
	public static EOQualifier qualifierPourModalite() {
		return ERXQ.equals(EOVueAbsences.TYPE_KEY, Constante.MODALITE);
	}
	
	/**
	 * @param service : service concerné par la répartition des heures d'absence
	 * @return une répartition des heures d'absence
	 */
	public EORepartAbsence getRepartAbsence(EOService service) {
		if ((toListeRepartAbsences(qualifierServicePourRepartition(service)) != null) 
				&& (toListeRepartAbsences(qualifierServicePourRepartition(service)).size() > 0)) {
			return toListeRepartAbsences(qualifierServicePourRepartition(service)).get(0);
		}
		
		return null;
	}
	
	/**
	 * Retourne qualifier pour la répartition des heures d'absence pour le service concerné
	 * @param service : service concerné par la répartition des heures d'absence
	 * @return qualifier
	 */
	private EOQualifier qualifierServicePourRepartition(EOService service) {
		return ERXQ.equals(EORepartAbsence.TO_SERVICE_KEY, service);
	}
	
	/**
	 * @param service : le service de l'enseignant
	 * @return le nombre d'heures d'absence
	 */
	public BigDecimal getNbHeuresAbsence(EOService service) {
		if (getRepartAbsence(service) != null) {
			return getRepartAbsence(service).nbHeures();
		}
		return null;
	}
	
	/**
	 * Est-ce que l'absence est une modalité de service?
	 * @return Vrai/Faux
	 */
	public boolean isModalite() {
		return Constante.MODALITE.equals(this.type());
	}
	
	/**
	 * Est-ce que la modalité est de type CRCT?
	 * @return Vrai/Faux
	 */
	public boolean isTypeCRCT() {
		return EOTypeAbsence.TYPE_CRCT.equals(this.toTypeAbsence().cTypeAbsence());
	}
	
	/**
	 * Est-ce que la modalité est de type Delegation ?
	 * @return Vrai/Faux
	 */
	public boolean isTypeDelegation() {
		return EOTypeAbsence.TYPE_DELEGATION.equals(this.toTypeAbsence().cTypeAbsence());
	}
	
	
	/**
	 * Est-ce que l'absence est une minoration de service?
	 * @return Vrai/Faux
	 */
	public boolean isMinoration() {
		return Constante.MINORATION.equals(this.type());
	}
	
}
