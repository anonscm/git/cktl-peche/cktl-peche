package org.cocktail.peche.entity;

import java.math.BigDecimal;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente la table "FLUX_MISE_EN_PAIEMENT".
 * 
 * @author Pascal MACOUIN
 */
public class EOFluxMiseEnPaiement extends _EOFluxMiseEnPaiement {
	/** Numéro de série. */
	private static final long serialVersionUID = 7965563401597828614L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOFluxMiseEnPaiement.class);
	
	/**
	 * Créer et initialiser une instance de flux de mise en paiement.
	 * 
	 * @param editingContext un editing context
	 * @param paiement le paiement
	 * @param service le service
	 * @param nom le nom de la personne payée
	 * @param prenom le prénom de la personne payée
	 * @param heuresPayees le nombre d'heures payées
	 * @param tauxNonCharge le taux horaire non chargé
	 * @param brutPaye le brut payé
	 * @parma moisSuivantPourTG mois suivant pour la paiement TG ? Oui ou Non
	 * @param personne la personne qui crée ce paiement
	 * @return le flux de mise en paiment
	 */
	public static EOFluxMiseEnPaiement creerEtInitialiser(EOEditingContext editingContext, EOPaiement paiement, EOService service, String nom, String prenom, BigDecimal heuresPayees, BigDecimal tauxNonCharge, BigDecimal brutPaye, String moisSuivantPourTG, EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		EOFluxMiseEnPaiement fluxMiseEnPaiement = EOFluxMiseEnPaiement.createEOFluxMiseEnPaiement(editingContext, brutPaye, now, now, heuresPayees, moisSuivantPourTG, nom, prenom, tauxNonCharge, paiement, personne, personne, service);
		return fluxMiseEnPaiement;
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setToPersonneCreationRelationship(personne);
		setToPersonneModificationRelationship(personne);
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			setDateModification(new NSTimestamp());
			setToPersonneModificationRelationship(personne);
		}
	}
	
	/**
	 * Mise à jour du npc -> si null, alors 00
	 * @param fluxMiseEnPaiement
	 * @param personnel
	 */
	public void setNumeroPriseEnCharge(EOFluxMiseEnPaiement fluxMiseEnPaiement, EOPersonnel personnel) {
		String npc = personnel.npc();
		if (StringUtils.isEmpty(npc)) {
			npc = "00";
		}
		fluxMiseEnPaiement.setNumeroPriseEnCharge(npc);
	}
	
}
