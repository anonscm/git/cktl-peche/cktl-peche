// DO NOT EDIT.  Make changes to EOFicheVoeuxDetail.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOFicheVoeuxDetail extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOFicheVoeuxDetail";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> DATE_COMMENTAIRE = new ERXKey<NSTimestamp>("dateCommentaire");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> ENSEIGNEMENT_EXTERNE = new ERXKey<String>("enseignementExterne");
  public static final ERXKey<String> ETABLISSEMENT_EXTERNE = new ERXKey<String>("etablissementExterne");
  public static final ERXKey<Double> HEURES_SOUHAITEES = new ERXKey<Double>("heuresSouhaitees");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP> COMPOSANT_AP = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP>("composantAP");
  public static final ERXKey<org.cocktail.peche.entity.EOFicheVoeux> FICHE_VOEUX = new ERXKey<org.cocktail.peche.entity.EOFicheVoeux>("ficheVoeux");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String DATE_COMMENTAIRE_KEY = DATE_COMMENTAIRE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ENSEIGNEMENT_EXTERNE_KEY = ENSEIGNEMENT_EXTERNE.key();
  public static final String ETABLISSEMENT_EXTERNE_KEY = ETABLISSEMENT_EXTERNE.key();
  public static final String HEURES_SOUHAITEES_KEY = HEURES_SOUHAITEES.key();
  // Relationships
  public static final String COMPOSANT_AP_KEY = COMPOSANT_AP.key();
  public static final String FICHE_VOEUX_KEY = FICHE_VOEUX.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOFicheVoeuxDetail.class);

  public EOFicheVoeuxDetail localInstanceIn(EOEditingContext editingContext) {
    EOFicheVoeuxDetail localInstance = (EOFicheVoeuxDetail)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(_EOFicheVoeuxDetail.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCommentaire() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeuxDetail.DATE_COMMENTAIRE_KEY);
  }

  public void setDateCommentaire(NSTimestamp value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating dateCommentaire from " + dateCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.DATE_COMMENTAIRE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeuxDetail.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOFicheVoeuxDetail.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.DATE_MODIFICATION_KEY);
  }

  public String enseignementExterne() {
    return (String) storedValueForKey(_EOFicheVoeuxDetail.ENSEIGNEMENT_EXTERNE_KEY);
  }

  public void setEnseignementExterne(String value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating enseignementExterne from " + enseignementExterne() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.ENSEIGNEMENT_EXTERNE_KEY);
  }

  public String etablissementExterne() {
    return (String) storedValueForKey(_EOFicheVoeuxDetail.ETABLISSEMENT_EXTERNE_KEY);
  }

  public void setEtablissementExterne(String value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating etablissementExterne from " + etablissementExterne() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.ETABLISSEMENT_EXTERNE_KEY);
  }

  public Double heuresSouhaitees() {
    return (Double) storedValueForKey(_EOFicheVoeuxDetail.HEURES_SOUHAITEES_KEY);
  }

  public void setHeuresSouhaitees(Double value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
    	_EOFicheVoeuxDetail.LOG.debug( "updating heuresSouhaitees from " + heuresSouhaitees() + " to " + value);
    }
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.HEURES_SOUHAITEES_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP composantAP() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP)storedValueForKey(_EOFicheVoeuxDetail.COMPOSANT_AP_KEY);
  }
  
  public void setComposantAP(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.COMPOSANT_AP_KEY);
  }

  public void setComposantAPRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
      _EOFicheVoeuxDetail.LOG.debug("updating composantAP from " + composantAP() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposantAP(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP oldValue = composantAP();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeuxDetail.COMPOSANT_AP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeuxDetail.COMPOSANT_AP_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOFicheVoeux ficheVoeux() {
    return (org.cocktail.peche.entity.EOFicheVoeux)storedValueForKey(_EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
  }
  
  public void setFicheVoeux(org.cocktail.peche.entity.EOFicheVoeux value) {
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
  }

  public void setFicheVoeuxRelationship(org.cocktail.peche.entity.EOFicheVoeux value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
      _EOFicheVoeuxDetail.LOG.debug("updating ficheVoeux from " + ficheVoeux() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setFicheVoeux(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOFicheVoeux oldValue = ficheVoeux();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeuxDetail.FICHE_VOEUX_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFicheVoeuxDetail.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
      _EOFicheVoeuxDetail.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeuxDetail.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeuxDetail.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOFicheVoeuxDetail.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOFicheVoeuxDetail.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOFicheVoeuxDetail.LOG.isDebugEnabled()) {
      _EOFicheVoeuxDetail.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOFicheVoeuxDetail.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOFicheVoeuxDetail.PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EOFicheVoeuxDetail createEOFicheVoeuxDetail(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, Double heuresSouhaitees
, org.cocktail.peche.entity.EOFicheVoeux ficheVoeux, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EOFicheVoeuxDetail eo = (EOFicheVoeuxDetail) EOUtilities.createAndInsertInstance(editingContext, _EOFicheVoeuxDetail.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setHeuresSouhaitees(heuresSouhaitees);
    eo.setFicheVoeuxRelationship(ficheVoeux);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EOFicheVoeuxDetail> fetchSpec() {
    return new ERXFetchSpecification<EOFicheVoeuxDetail>(_EOFicheVoeuxDetail.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOFicheVoeuxDetail> fetchAllEOFicheVoeuxDetails(EOEditingContext editingContext) {
    return _EOFicheVoeuxDetail.fetchAllEOFicheVoeuxDetails(editingContext, null);
  }

  public static NSArray<EOFicheVoeuxDetail> fetchAllEOFicheVoeuxDetails(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOFicheVoeuxDetail.fetchEOFicheVoeuxDetails(editingContext, null, sortOrderings);
  }

  public static NSArray<EOFicheVoeuxDetail> fetchEOFicheVoeuxDetails(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOFicheVoeuxDetail> fetchSpec = new ERXFetchSpecification<EOFicheVoeuxDetail>(_EOFicheVoeuxDetail.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOFicheVoeuxDetail> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOFicheVoeuxDetail fetchEOFicheVoeuxDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheVoeuxDetail.fetchEOFicheVoeuxDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheVoeuxDetail fetchEOFicheVoeuxDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOFicheVoeuxDetail> eoObjects = _EOFicheVoeuxDetail.fetchEOFicheVoeuxDetails(editingContext, qualifier, null);
    EOFicheVoeuxDetail eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOFicheVoeuxDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheVoeuxDetail fetchRequiredEOFicheVoeuxDetail(EOEditingContext editingContext, String keyName, Object value) {
    return _EOFicheVoeuxDetail.fetchRequiredEOFicheVoeuxDetail(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOFicheVoeuxDetail fetchRequiredEOFicheVoeuxDetail(EOEditingContext editingContext, EOQualifier qualifier) {
    EOFicheVoeuxDetail eoObject = _EOFicheVoeuxDetail.fetchEOFicheVoeuxDetail(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOFicheVoeuxDetail that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOFicheVoeuxDetail localInstanceIn(EOEditingContext editingContext, EOFicheVoeuxDetail eo) {
    EOFicheVoeuxDetail localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
