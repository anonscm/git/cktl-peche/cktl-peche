// DO NOT EDIT.  Make changes to EOTypeFluxPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypeFluxPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOTypeFluxPaiement";

  // Attribute Keys
  public static final ERXKey<String> CLASSE_TYPE_FLUX = new ERXKey<String>("classeTypeFlux");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<Integer> ID_PERSONNE_CREATION = new ERXKey<Integer>("idPersonneCreation");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.peche.entity.EODefinitionFluxPaiement> TO_LISTE_DEFINITION_FLUX_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EODefinitionFluxPaiement>("toListeDefinitionFluxPaiement");

  // Attributes
  public static final String CLASSE_TYPE_FLUX_KEY = CLASSE_TYPE_FLUX.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String ID_PERSONNE_CREATION_KEY = ID_PERSONNE_CREATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY = TO_LISTE_DEFINITION_FLUX_PAIEMENT.key();

  private static Logger LOG = Logger.getLogger(_EOTypeFluxPaiement.class);

  public EOTypeFluxPaiement localInstanceIn(EOEditingContext editingContext) {
    EOTypeFluxPaiement localInstance = (EOTypeFluxPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String classeTypeFlux() {
    return (String) storedValueForKey(_EOTypeFluxPaiement.CLASSE_TYPE_FLUX_KEY);
  }

  public void setClasseTypeFlux(String value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating classeTypeFlux from " + classeTypeFlux() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.CLASSE_TYPE_FLUX_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOTypeFluxPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOTypeFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.DATE_MODIFICATION_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EOTypeFluxPaiement.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.ID_KEY);
  }

  public Integer idPersonneCreation() {
    return (Integer) storedValueForKey(_EOTypeFluxPaiement.ID_PERSONNE_CREATION_KEY);
  }

  public void setIdPersonneCreation(Integer value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating idPersonneCreation from " + idPersonneCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.ID_PERSONNE_CREATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypeFluxPaiement.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
    	_EOTypeFluxPaiement.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypeFluxPaiement.LIBELLE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOTypeFluxPaiement.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOTypeFluxPaiement.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
      _EOTypeFluxPaiement.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOTypeFluxPaiement.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOTypeFluxPaiement.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOTypeFluxPaiement.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOTypeFluxPaiement.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
      _EOTypeFluxPaiement.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOTypeFluxPaiement.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOTypeFluxPaiement.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement> toListeDefinitionFluxPaiement() {
    return (NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement>)storedValueForKey(_EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement> toListeDefinitionFluxPaiement(EOQualifier qualifier) {
    return toListeDefinitionFluxPaiement(qualifier, null);
  }

  public NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement> toListeDefinitionFluxPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement> results;
      results = toListeDefinitionFluxPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EODefinitionFluxPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToToListeDefinitionFluxPaiement(org.cocktail.peche.entity.EODefinitionFluxPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
  }

  public void removeFromToListeDefinitionFluxPaiement(org.cocktail.peche.entity.EODefinitionFluxPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
  }

  public void addToToListeDefinitionFluxPaiementRelationship(org.cocktail.peche.entity.EODefinitionFluxPaiement object) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
      _EOTypeFluxPaiement.LOG.debug("adding " + object + " to toListeDefinitionFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeDefinitionFluxPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeDefinitionFluxPaiementRelationship(org.cocktail.peche.entity.EODefinitionFluxPaiement object) {
    if (_EOTypeFluxPaiement.LOG.isDebugEnabled()) {
      _EOTypeFluxPaiement.LOG.debug("removing " + object + " from toListeDefinitionFluxPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeDefinitionFluxPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EODefinitionFluxPaiement createToListeDefinitionFluxPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EODefinitionFluxPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EODefinitionFluxPaiement) eo;
  }

  public void deleteToListeDefinitionFluxPaiementRelationship(org.cocktail.peche.entity.EODefinitionFluxPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOTypeFluxPaiement.TO_LISTE_DEFINITION_FLUX_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeDefinitionFluxPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EODefinitionFluxPaiement> objects = toListeDefinitionFluxPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeDefinitionFluxPaiementRelationship(objects.nextElement());
    }
  }


  public static EOTypeFluxPaiement createEOTypeFluxPaiement(EOEditingContext editingContext, String classeTypeFlux
, NSTimestamp dateCreation
, NSTimestamp dateModification
, Integer id
, Integer idPersonneCreation
, String libelle
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification) {
    EOTypeFluxPaiement eo = (EOTypeFluxPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOTypeFluxPaiement.ENTITY_NAME);    
		eo.setClasseTypeFlux(classeTypeFlux);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
		eo.setIdPersonneCreation(idPersonneCreation);
		eo.setLibelle(libelle);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOTypeFluxPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOTypeFluxPaiement>(_EOTypeFluxPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypeFluxPaiement> fetchAllEOTypeFluxPaiements(EOEditingContext editingContext) {
    return _EOTypeFluxPaiement.fetchAllEOTypeFluxPaiements(editingContext, null);
  }

  public static NSArray<EOTypeFluxPaiement> fetchAllEOTypeFluxPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeFluxPaiement.fetchEOTypeFluxPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeFluxPaiement> fetchEOTypeFluxPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypeFluxPaiement> fetchSpec = new ERXFetchSpecification<EOTypeFluxPaiement>(_EOTypeFluxPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeFluxPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypeFluxPaiement fetchEOTypeFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFluxPaiement.fetchEOTypeFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFluxPaiement fetchEOTypeFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeFluxPaiement> eoObjects = _EOTypeFluxPaiement.fetchEOTypeFluxPaiements(editingContext, qualifier, null);
    EOTypeFluxPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOTypeFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFluxPaiement fetchRequiredEOTypeFluxPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeFluxPaiement.fetchRequiredEOTypeFluxPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeFluxPaiement fetchRequiredEOTypeFluxPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeFluxPaiement eoObject = _EOTypeFluxPaiement.fetchEOTypeFluxPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOTypeFluxPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeFluxPaiement localInstanceIn(EOEditingContext editingContext, EOTypeFluxPaiement eo) {
    EOTypeFluxPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
