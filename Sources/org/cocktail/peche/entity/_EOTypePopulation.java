// DO NOT EDIT.  Make changes to EOTypePopulation.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOTypePopulation extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOTypePopulation";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<org.joda.time.DateTime> DATE_CREATION = new ERXKey<org.joda.time.DateTime>("dateCreation");
  public static final ERXKey<org.joda.time.DateTime> DATE_MODIFICATION = new ERXKey<org.joda.time.DateTime>("dateModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(_EOTypePopulation.class);

  public EOTypePopulation localInstanceIn(EOEditingContext editingContext) {
    EOTypePopulation localInstance = (EOTypePopulation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(_EOTypePopulation.CODE_KEY);
  }

  public void setCode(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePopulation.CODE_KEY);
  }

  public org.joda.time.DateTime dateCreation() {
    return (org.joda.time.DateTime) storedValueForKey(_EOTypePopulation.DATE_CREATION_KEY);
  }

  public void setDateCreation(org.joda.time.DateTime value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePopulation.DATE_CREATION_KEY);
  }

  public org.joda.time.DateTime dateModification() {
    return (org.joda.time.DateTime) storedValueForKey(_EOTypePopulation.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(org.joda.time.DateTime value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePopulation.DATE_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOTypePopulation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOTypePopulation.LOG.isDebugEnabled()) {
    	_EOTypePopulation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOTypePopulation.LIBELLE_KEY);
  }


  public static EOTypePopulation createEOTypePopulation(EOEditingContext editingContext, String code
, org.joda.time.DateTime dateCreation
, org.joda.time.DateTime dateModification
, String libelle
) {
    EOTypePopulation eo = (EOTypePopulation) EOUtilities.createAndInsertInstance(editingContext, _EOTypePopulation.ENTITY_NAME);    
		eo.setCode(code);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setLibelle(libelle);
    return eo;
  }

  public static ERXFetchSpecification<EOTypePopulation> fetchSpec() {
    return new ERXFetchSpecification<EOTypePopulation>(_EOTypePopulation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOTypePopulation> fetchAllEOTypePopulations(EOEditingContext editingContext) {
    return _EOTypePopulation.fetchAllEOTypePopulations(editingContext, null);
  }

  public static NSArray<EOTypePopulation> fetchAllEOTypePopulations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypePopulation.fetchEOTypePopulations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypePopulation> fetchEOTypePopulations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOTypePopulation> fetchSpec = new ERXFetchSpecification<EOTypePopulation>(_EOTypePopulation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypePopulation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOTypePopulation fetchEOTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePopulation.fetchEOTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePopulation fetchEOTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypePopulation> eoObjects = _EOTypePopulation.fetchEOTypePopulations(editingContext, qualifier, null);
    EOTypePopulation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOTypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePopulation fetchRequiredEOTypePopulation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypePopulation.fetchRequiredEOTypePopulation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypePopulation fetchRequiredEOTypePopulation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypePopulation eoObject = _EOTypePopulation.fetchEOTypePopulation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOTypePopulation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypePopulation localInstanceIn(EOEditingContext editingContext, EOTypePopulation eo) {
    EOTypePopulation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
