// DO NOT EDIT.  Make changes to EORepartUefEtablissement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartUefEtablissement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartUefEtablissement";

  // Attribute Keys
  public static final ERXKey<org.joda.time.DateTime> DATE_CREATION = new ERXKey<org.joda.time.DateTime>("dateCreation");
  public static final ERXKey<org.joda.time.DateTime> DATE_MODIFICATION = new ERXKey<org.joda.time.DateTime>("dateModification");
  public static final ERXKey<String> ETABLISSEMENT_EXTERNE = new ERXKey<String>("etablissementExterne");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE> COMPOSANT_UE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE>("composantUE");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ETABLISSEMENT_EXTERNE_KEY = ETABLISSEMENT_EXTERNE.key();
  // Relationships
  public static final String COMPOSANT_UE_KEY = COMPOSANT_UE.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EORepartUefEtablissement.class);

  public EORepartUefEtablissement localInstanceIn(EOEditingContext editingContext) {
    EORepartUefEtablissement localInstance = (EORepartUefEtablissement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.joda.time.DateTime dateCreation() {
    return (org.joda.time.DateTime) storedValueForKey(_EORepartUefEtablissement.DATE_CREATION_KEY);
  }

  public void setDateCreation(org.joda.time.DateTime value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
    	_EORepartUefEtablissement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefEtablissement.DATE_CREATION_KEY);
  }

  public org.joda.time.DateTime dateModification() {
    return (org.joda.time.DateTime) storedValueForKey(_EORepartUefEtablissement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(org.joda.time.DateTime value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
    	_EORepartUefEtablissement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefEtablissement.DATE_MODIFICATION_KEY);
  }

  public String etablissementExterne() {
    return (String) storedValueForKey(_EORepartUefEtablissement.ETABLISSEMENT_EXTERNE_KEY);
  }

  public void setEtablissementExterne(String value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
    	_EORepartUefEtablissement.LOG.debug( "updating etablissementExterne from " + etablissementExterne() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefEtablissement.ETABLISSEMENT_EXTERNE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE composantUE() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE)storedValueForKey(_EORepartUefEtablissement.COMPOSANT_UE_KEY);
  }
  
  public void setComposantUE(org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE value) {
    takeStoredValueForKey(value, _EORepartUefEtablissement.COMPOSANT_UE_KEY);
  }

  public void setComposantUERelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
      _EORepartUefEtablissement.LOG.debug("updating composantUE from " + composantUE() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposantUE(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE oldValue = composantUE();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefEtablissement.COMPOSANT_UE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefEtablissement.COMPOSANT_UE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefEtablissement.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefEtablissement.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
      _EORepartUefEtablissement.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefEtablissement.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefEtablissement.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefEtablissement.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefEtablissement.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefEtablissement.LOG.isDebugEnabled()) {
      _EORepartUefEtablissement.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefEtablissement.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefEtablissement.PERSONNE_MODIFICATION_KEY);
    }
  }
  

  public static EORepartUefEtablissement createEORepartUefEtablissement(EOEditingContext editingContext, org.joda.time.DateTime dateCreation
, org.joda.time.DateTime dateModification
, String etablissementExterne
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE composantUE, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation) {
    EORepartUefEtablissement eo = (EORepartUefEtablissement) EOUtilities.createAndInsertInstance(editingContext, _EORepartUefEtablissement.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setEtablissementExterne(etablissementExterne);
    eo.setComposantUERelationship(composantUE);
    eo.setPersonneCreationRelationship(personneCreation);
    return eo;
  }

  public static ERXFetchSpecification<EORepartUefEtablissement> fetchSpec() {
    return new ERXFetchSpecification<EORepartUefEtablissement>(_EORepartUefEtablissement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartUefEtablissement> fetchAllEORepartUefEtablissements(EOEditingContext editingContext) {
    return _EORepartUefEtablissement.fetchAllEORepartUefEtablissements(editingContext, null);
  }

  public static NSArray<EORepartUefEtablissement> fetchAllEORepartUefEtablissements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartUefEtablissement.fetchEORepartUefEtablissements(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartUefEtablissement> fetchEORepartUefEtablissements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartUefEtablissement> fetchSpec = new ERXFetchSpecification<EORepartUefEtablissement>(_EORepartUefEtablissement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartUefEtablissement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartUefEtablissement fetchEORepartUefEtablissement(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefEtablissement.fetchEORepartUefEtablissement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefEtablissement fetchEORepartUefEtablissement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartUefEtablissement> eoObjects = _EORepartUefEtablissement.fetchEORepartUefEtablissements(editingContext, qualifier, null);
    EORepartUefEtablissement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartUefEtablissement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefEtablissement fetchRequiredEORepartUefEtablissement(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefEtablissement.fetchRequiredEORepartUefEtablissement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefEtablissement fetchRequiredEORepartUefEtablissement(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartUefEtablissement eoObject = _EORepartUefEtablissement.fetchEORepartUefEtablissement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartUefEtablissement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefEtablissement localInstanceIn(EOEditingContext editingContext, EORepartUefEtablissement eo) {
    EORepartUefEtablissement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
