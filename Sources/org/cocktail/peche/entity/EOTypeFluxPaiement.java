package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.peche.metier.miseenpaiement.IFormatFluxPaiement;

/**
 * Cette classe représente un type de flux de paiement. 
 * 
 * @author Pascal MACOUIN
 */

public class EOTypeFluxPaiement extends _EOTypeFluxPaiement {
	/** Numéro de série. */
	private static final long serialVersionUID = 8926605226489993194L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOTypeFluxPaiement.class);
	
	/**
	 * Retourne la classe représentée par {@link #classeTypeFlux()}.
	 * 
	 * @return La classe
	 */
	public Class<? extends IFormatFluxPaiement> getClasseTypeFlux() {
		Class<? extends IFormatFluxPaiement> formatFluxPaiement;
		
		try {
			formatFluxPaiement = (Class<? extends IFormatFluxPaiement>) Class.forName(classeTypeFlux());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Classe " + classeTypeFlux() + " non trouvée", e);
		}
		
		return formatFluxPaiement;
	}
}
