// DO NOT EDIT.  Make changes to EORepartEnseignant.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartEnseignant extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartEnseignant";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> COMPOSANTE_REFERENTE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("composanteReferente");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> REPARTITEUR_DEMANDEUR = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("repartiteurDemandeur");
  public static final ERXKey<org.cocktail.peche.entity.EOServiceDetail> SERVICE_DETAIL = new ERXKey<org.cocktail.peche.entity.EOServiceDetail>("serviceDetail");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  // Relationships
  public static final String COMPOSANTE_REFERENTE_KEY = COMPOSANTE_REFERENTE.key();
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String REPARTITEUR_DEMANDEUR_KEY = REPARTITEUR_DEMANDEUR.key();
  public static final String SERVICE_DETAIL_KEY = SERVICE_DETAIL.key();

  private static Logger LOG = Logger.getLogger(_EORepartEnseignant.class);

  public EORepartEnseignant localInstanceIn(EOEditingContext editingContext) {
    EORepartEnseignant localInstance = (EORepartEnseignant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartEnseignant.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
    	_EORepartEnseignant.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartEnseignant.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartEnseignant.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
    	_EORepartEnseignant.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartEnseignant.DATE_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure composanteReferente() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EORepartEnseignant.COMPOSANTE_REFERENTE_KEY);
  }
  
  public void setComposanteReferente(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EORepartEnseignant.COMPOSANTE_REFERENTE_KEY);
  }

  public void setComposanteReferenteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
      _EORepartEnseignant.LOG.debug("updating composanteReferente from " + composanteReferente() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setComposanteReferente(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = composanteReferente();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartEnseignant.COMPOSANTE_REFERENTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartEnseignant.COMPOSANTE_REFERENTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartEnseignant.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartEnseignant.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
      _EORepartEnseignant.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartEnseignant.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartEnseignant.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartEnseignant.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartEnseignant.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
      _EORepartEnseignant.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartEnseignant.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartEnseignant.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu repartiteurDemandeur() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(_EORepartEnseignant.REPARTITEUR_DEMANDEUR_KEY);
  }
  
  public void setRepartiteurDemandeur(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, _EORepartEnseignant.REPARTITEUR_DEMANDEUR_KEY);
  }

  public void setRepartiteurDemandeurRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
      _EORepartEnseignant.LOG.debug("updating repartiteurDemandeur from " + repartiteurDemandeur() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setRepartiteurDemandeur(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = repartiteurDemandeur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartEnseignant.REPARTITEUR_DEMANDEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartEnseignant.REPARTITEUR_DEMANDEUR_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOServiceDetail serviceDetail() {
    return (org.cocktail.peche.entity.EOServiceDetail)storedValueForKey(_EORepartEnseignant.SERVICE_DETAIL_KEY);
  }
  
  public void setServiceDetail(org.cocktail.peche.entity.EOServiceDetail value) {
    takeStoredValueForKey(value, _EORepartEnseignant.SERVICE_DETAIL_KEY);
  }

  public void setServiceDetailRelationship(org.cocktail.peche.entity.EOServiceDetail value) {
    if (_EORepartEnseignant.LOG.isDebugEnabled()) {
      _EORepartEnseignant.LOG.debug("updating serviceDetail from " + serviceDetail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setServiceDetail(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOServiceDetail oldValue = serviceDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartEnseignant.SERVICE_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartEnseignant.SERVICE_DETAIL_KEY);
    }
  }
  

  public static EORepartEnseignant createEORepartEnseignant(EOEditingContext editingContext, NSTimestamp dateCreation
, NSTimestamp dateModification
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure composanteReferente, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu repartiteurDemandeur, org.cocktail.peche.entity.EOServiceDetail serviceDetail) {
    EORepartEnseignant eo = (EORepartEnseignant) EOUtilities.createAndInsertInstance(editingContext, _EORepartEnseignant.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
    eo.setComposanteReferenteRelationship(composanteReferente);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setRepartiteurDemandeurRelationship(repartiteurDemandeur);
    eo.setServiceDetailRelationship(serviceDetail);
    return eo;
  }

  public static ERXFetchSpecification<EORepartEnseignant> fetchSpec() {
    return new ERXFetchSpecification<EORepartEnseignant>(_EORepartEnseignant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartEnseignant> fetchAllEORepartEnseignants(EOEditingContext editingContext) {
    return _EORepartEnseignant.fetchAllEORepartEnseignants(editingContext, null);
  }

  public static NSArray<EORepartEnseignant> fetchAllEORepartEnseignants(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartEnseignant.fetchEORepartEnseignants(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartEnseignant> fetchEORepartEnseignants(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartEnseignant> fetchSpec = new ERXFetchSpecification<EORepartEnseignant>(_EORepartEnseignant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartEnseignant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartEnseignant fetchEORepartEnseignant(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartEnseignant.fetchEORepartEnseignant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartEnseignant fetchEORepartEnseignant(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartEnseignant> eoObjects = _EORepartEnseignant.fetchEORepartEnseignants(editingContext, qualifier, null);
    EORepartEnseignant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartEnseignant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartEnseignant fetchRequiredEORepartEnseignant(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartEnseignant.fetchRequiredEORepartEnseignant(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartEnseignant fetchRequiredEORepartEnseignant(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartEnseignant eoObject = _EORepartEnseignant.fetchEORepartEnseignant(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartEnseignant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartEnseignant localInstanceIn(EOEditingContext editingContext, EORepartEnseignant eo) {
    EORepartEnseignant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
