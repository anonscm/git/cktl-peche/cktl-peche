package org.cocktail.peche.entity;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.interfaces.IEnseignant;
import org.cocktail.peche.outils.ListHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Classe représentant un enseignant générique
 * 
 * @author Etienne RAGONNEAU
 * @author Chama LAATIK
 */
public class EOEnseignantGenerique extends _EOEnseignantGenerique implements IEnseignant {

	private static final long serialVersionUID = -9187789042923279497L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOEnseignantGenerique.class);

	/**
	 * Crée et initialise un enseignant générique.
	 *
	 * @param edc : editing context.
	 * @return un enseignant générique.
	 */
	public static EOEnseignantGenerique creerEtInitialiser(EOEditingContext edc) {
		EOEnseignantGenerique eo = new EOEnseignantGenerique();
		eo.setHeuresPrevues(Double.valueOf(0.0));
		edc.insertObject(eo);

		return eo;
	}
	
	/**
	 * Crée et initialise un enseignant générique avec nom et prénom.
	 *
	 * @param edc : editing context.
	 * @param nom : nom de l'enseignant générique
	 * @param prenom : prenom de l'enseignant générique
	 * @return un enseignant générique.
	 */
	public static EOEnseignantGenerique creerEtInitialiser(EOEditingContext edc, String nom, String prenom) {
		EOEnseignantGenerique eo = new EOEnseignantGenerique();
		eo.setNom(nom);
		eo.setPrenom(prenom);
		eo.setHeuresPrevues(Double.valueOf(0.0));
		edc.insertObject(eo);

		return eo;
	}
	
	/**
	 * On recherche les enseignants génériques qui ont un service sur l'année universitaire n 
	 *  et qui ont une structure nulle ou appartiennent au périmètre de données de l'utilisateur
	 * @param edc : editing context
	 * @return la liste des enseignants génériques valides
	 */
	public static List<EOEnseignantGenerique> getListeEnseignantGenerique(EOEditingContext edc,Integer anneeUniversitaire, List<IStructure> listeStructures, boolean isPourVacataire) {
		EOQualifier qualifierService = null;
		
		if (isPourVacataire) {
			qualifierService = qualifierPourVacataires(anneeUniversitaire);
		} else {
			qualifierService = qualifierPourStatutaires(anneeUniversitaire, listeStructures);
		}
		
		NSArray<EOService> services = EOService.fetchEOServices(edc, qualifierService, null);
		
		NSArray<Integer> listeId = new NSMutableArray<Integer>();
		for (EOService service : services) {
			listeId.add(service.idEnseignantGenerique());	
		}

		return fetchEOEnseignantGeneriques(edc, ERXQ.in(EOEnseignantGenerique.ID_KEY, listeId), getTriParDefaut());
	}
	
	/**
	 * @param anneeUniversitaire : année universitaire
	 * @return qualifier pour récupérer les génériques vacataires
	 */
	private static EOQualifier qualifierPourVacataires(Integer anneeUniversitaire) {
		return ERXQ.and(
				EOService.TO_ENSEIGNANT_GENERIQUE.isNotNull(),
				EOService.ANNEE.eq(anneeUniversitaire),
				EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TEMOIN_STATUTAIRE).eq(Constante.NON),
				EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TO_STRUCTURE_KEY).isNull()
				);
	}
	
	/**
	 * @param anneeUniversitaire : année universitaire
	 * @param listeStructures : liste des structures
	 * @return qualifier pour récupérer les génériques statutaires
	 */
	private static EOQualifier qualifierPourStatutaires(Integer anneeUniversitaire, List<IStructure> listeStructures) {
		return ERXQ.and(
				EOService.TO_ENSEIGNANT_GENERIQUE.isNotNull(),
				EOService.ANNEE.eq(anneeUniversitaire),
				EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TEMOIN_STATUTAIRE).eq(Constante.OUI),
				ERXQ.or(
						EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TO_STRUCTURE_KEY).isNull(),
						EOService.TO_ENSEIGNANT_GENERIQUE.dot(EOEnseignantGenerique.TO_STRUCTURE).in(getListHelper().bidouilleListeStructurePourWO(listeStructures)))
				);
	}
	
	/**
	 * @return Le tri par défaut de la liste
	 */
	public static ERXSortOrderings getTriParDefaut() {
		return EOEnseignantGenerique.NOM.asc().then(EOEnseignantGenerique.PRENOM.asc());
	}
	
	/**
	 * @return le statut de l'enseignant : Générique
	 */
	public String getStatut() {
		if (isEnseignantStatutaire()) {
			return Constante.GENERIQUE_STAT;
		} else {
			return Constante.GENERIQUE_VACAT;
		}
	}

	/**
	 * @param anneeUniversitaire année universitaire
	 * @return le service de cet enseignant pour cette année universitaire
	 */
	public EOService getService(Integer anneeUniversitaire) {
		EOQualifier qualifier = EOService.TO_ENSEIGNANT_GENERIQUE.eq(this).and(EOService.ANNEE.eq(anneeUniversitaire));
		EOService service = EOService.fetchEOService(editingContext(), qualifier);
		
		return service;
	}
	
	public boolean isEnseignantStatutaire() {
		return temoinStatutaire() != null && Constante.OUI.equals(temoinStatutaire());
	}

	/**
	 * @param value : valeur à mettre à jour
	 */
	public void setEnseignantStatutaire(boolean value) {
		if (value) {
			setTemoinStatutaire(Constante.OUI);
		} else {
			setTemoinStatutaire(Constante.NON);
		}
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			NSTimestamp now = new NSTimestamp();
			setDateModification(now);
			setPersonneModificationRelationship(personne);
		}
	}
	
	public static ListHelper getListHelper() {
		return new ListHelper();
	}

	/**
	 * {@inheritDoc}
	 */
	public IStructure getStructure(Integer annee, boolean isAnneeUnivEnAnneeCivil) {
		return this.toStructure();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isStatutaire(Integer annee, boolean isAnneeUnivEnAnneeCivil) {
		return isEnseignantStatutaire();
	}
	
}
