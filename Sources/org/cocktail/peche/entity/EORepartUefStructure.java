package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXSortOrdering;

/**
 * 
 *
 */
public class EORepartUefStructure extends _EORepartUefStructure {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8295620024670750219L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EORepartUefStructure.class);
	
	/**
	 * Crée et initialise une répartition entre l'UE flottante et un établissement.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return une répartition.
	 */
	public static EORepartUefStructure creerEtInitialiser(EOEditingContext edc) {
		EORepartUefStructure repartUef = new EORepartUefStructure();
		edc.insertObject(repartUef);
		
		return repartUef;
	}
	
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);
	}
	
	/**
	 * 
	 * @return ErxSortOrdering
	 */
	public static ERXSortOrdering getTriParDefaut() {
		return EORepartUefStructure.TO_STRUCTURE.dot(EOStructure.LL_STRUCTURE).asc();
	}
}
