package org.cocktail.peche.entity;

import org.apache.log4j.Logger;

/**
 * Classe des périodes utilisées dans PECHE : mois ou semestre
 */
public class EOPeriode extends _EOPeriode {

	private static final long serialVersionUID = -4090515763195541178L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPeriode.class);
	
	public static final String TYPE_MOIS = "M";
	public static final String TYPE_SEMESTRE = "S";
	
	public static final String TYPE_ANNEE_CIVILE = "C";
	public static final String TYPE_ANNEE_UNIVERSITAIRE = "U";
	
	
	/**
	 * Retourne le nombre de mois dans la période.
	 * 
	 * @return le nombre de mois dans la période
	 */
	public int nombreMois() {
		int moisFin = noDernierMois();
		
		// La période est à cheval sur une année
		if (moisFin < noPremierMois()) {
			moisFin += 12;
		}
		
		return moisFin - noPremierMois() + 1;
	}

}
