// DO NOT EDIT.  Make changes to EOPaiement.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EOPaiement extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EOPaiement";

  // Attribute Keys
  public static final ERXKey<Integer> ANNEE_PAIEMENT = new ERXKey<Integer>("anneePaiement");
  public static final ERXKey<NSTimestamp> DATE_ARRETE = new ERXKey<NSTimestamp>("dateArrete");
  public static final ERXKey<NSTimestamp> DATE_BORDEREAU = new ERXKey<NSTimestamp>("dateBordereau");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_FLUX = new ERXKey<NSTimestamp>("dateFlux");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<NSTimestamp> DATE_PAIEMENT = new ERXKey<NSTimestamp>("datePaiement");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> MOIS_PAIEMENT = new ERXKey<Integer>("moisPaiement");
  public static final ERXKey<Integer> NUMERO_PAIEMENT = new ERXKey<Integer>("numeroPaiement");
  public static final ERXKey<String> PAYE = new ERXKey<String>("paye");
  // Relationship Keys
  public static final ERXKey<org.cocktail.peche.entity.EOFluxMiseEnPaiement> TO_LISTE_FLUX_MISE_EN_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOFluxMiseEnPaiement>("toListeFluxMiseEnPaiement");
  public static final ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement> TO_LISTE_MISES_EN_PAIEMENT = new ERXKey<org.cocktail.peche.entity.EOMiseEnPaiement>("toListeMisesEnPaiement");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonneModification");

  // Attributes
  public static final String ANNEE_PAIEMENT_KEY = ANNEE_PAIEMENT.key();
  public static final String DATE_ARRETE_KEY = DATE_ARRETE.key();
  public static final String DATE_BORDEREAU_KEY = DATE_BORDEREAU.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_FLUX_KEY = DATE_FLUX.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DATE_PAIEMENT_KEY = DATE_PAIEMENT.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MOIS_PAIEMENT_KEY = MOIS_PAIEMENT.key();
  public static final String NUMERO_PAIEMENT_KEY = NUMERO_PAIEMENT.key();
  public static final String PAYE_KEY = PAYE.key();
  // Relationships
  public static final String TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY = TO_LISTE_FLUX_MISE_EN_PAIEMENT.key();
  public static final String TO_LISTE_MISES_EN_PAIEMENT_KEY = TO_LISTE_MISES_EN_PAIEMENT.key();
  public static final String TO_PERSONNE_CREATION_KEY = TO_PERSONNE_CREATION.key();
  public static final String TO_PERSONNE_MODIFICATION_KEY = TO_PERSONNE_MODIFICATION.key();

  private static Logger LOG = Logger.getLogger(_EOPaiement.class);

  public EOPaiement localInstanceIn(EOEditingContext editingContext) {
    EOPaiement localInstance = (EOPaiement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer anneePaiement() {
    return (Integer) storedValueForKey(_EOPaiement.ANNEE_PAIEMENT_KEY);
  }

  public void setAnneePaiement(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating anneePaiement from " + anneePaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.ANNEE_PAIEMENT_KEY);
  }

  public NSTimestamp dateArrete() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_ARRETE_KEY);
  }

  public void setDateArrete(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dateArrete from " + dateArrete() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_ARRETE_KEY);
  }

  public NSTimestamp dateBordereau() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_BORDEREAU_KEY);
  }

  public void setDateBordereau(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dateBordereau from " + dateBordereau() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_BORDEREAU_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_CREATION_KEY);
  }

  public NSTimestamp dateFlux() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_FLUX_KEY);
  }

  public void setDateFlux(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dateFlux from " + dateFlux() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_FLUX_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_MODIFICATION_KEY);
  }

  public NSTimestamp datePaiement() {
    return (NSTimestamp) storedValueForKey(_EOPaiement.DATE_PAIEMENT_KEY);
  }

  public void setDatePaiement(NSTimestamp value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating datePaiement from " + datePaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.DATE_PAIEMENT_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(_EOPaiement.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.LIBELLE_KEY);
  }

  public Integer moisPaiement() {
    return (Integer) storedValueForKey(_EOPaiement.MOIS_PAIEMENT_KEY);
  }

  public void setMoisPaiement(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating moisPaiement from " + moisPaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.MOIS_PAIEMENT_KEY);
  }

  public Integer numeroPaiement() {
    return (Integer) storedValueForKey(_EOPaiement.NUMERO_PAIEMENT_KEY);
  }

  public void setNumeroPaiement(Integer value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating numeroPaiement from " + numeroPaiement() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.NUMERO_PAIEMENT_KEY);
  }

  public String paye() {
    return (String) storedValueForKey(_EOPaiement.PAYE_KEY);
  }

  public void setPaye(String value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
    	_EOPaiement.LOG.debug( "updating paye from " + paye() + " to " + value);
    }
    takeStoredValueForKey(value, _EOPaiement.PAYE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOPaiement.TO_PERSONNE_CREATION_KEY);
  }
  
  public void setToPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOPaiement.TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("updating toPersonneCreation from " + toPersonneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiement.TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiement.TO_PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EOPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }
  
  public void setToPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EOPaiement.TO_PERSONNE_MODIFICATION_KEY);
  }

  public void setToPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("updating toPersonneModification from " + toPersonneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EOPaiement.TO_PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EOPaiement.TO_PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement> toListeFluxMiseEnPaiement() {
    return (NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement>)storedValueForKey(_EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement> toListeFluxMiseEnPaiement(EOQualifier qualifier) {
    return toListeFluxMiseEnPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement> toListeFluxMiseEnPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeFluxMiseEnPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement> toListeFluxMiseEnPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOFluxMiseEnPaiement.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOFluxMiseEnPaiement.fetchEOFluxMiseEnPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeFluxMiseEnPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOFluxMiseEnPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeFluxMiseEnPaiement(org.cocktail.peche.entity.EOFluxMiseEnPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
  }

  public void removeFromToListeFluxMiseEnPaiement(org.cocktail.peche.entity.EOFluxMiseEnPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
  }

  public void addToToListeFluxMiseEnPaiementRelationship(org.cocktail.peche.entity.EOFluxMiseEnPaiement object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("adding " + object + " to toListeFluxMiseEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeFluxMiseEnPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeFluxMiseEnPaiementRelationship(org.cocktail.peche.entity.EOFluxMiseEnPaiement object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("removing " + object + " from toListeFluxMiseEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeFluxMiseEnPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EOFluxMiseEnPaiement createToListeFluxMiseEnPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOFluxMiseEnPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EOFluxMiseEnPaiement) eo;
  }

  public void deleteToListeFluxMiseEnPaiementRelationship(org.cocktail.peche.entity.EOFluxMiseEnPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_FLUX_MISE_EN_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeFluxMiseEnPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EOFluxMiseEnPaiement> objects = toListeFluxMiseEnPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeFluxMiseEnPaiementRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement() {
    return (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)storedValueForKey(_EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier) {
    return toListeMisesEnPaiement(qualifier, null, false);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, boolean fetch) {
    return toListeMisesEnPaiement(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> toListeMisesEnPaiement(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.peche.entity.EOMiseEnPaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.peche.entity.EOMiseEnPaiement.TO_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.peche.entity.EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toListeMisesEnPaiement();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.peche.entity.EOMiseEnPaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    includeObjectIntoPropertyWithKey(object, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void removeFromToListeMisesEnPaiement(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    excludeObjectFromPropertyWithKey(object, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
  }

  public void addToToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("adding " + object + " to toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	addToToListeMisesEnPaiement(object);
    }
    else {
    	addObjectToBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public void removeFromToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    if (_EOPaiement.LOG.isDebugEnabled()) {
      _EOPaiement.LOG.debug("removing " + object + " from toListeMisesEnPaiement relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	removeFromToListeMisesEnPaiement(object);
    }
    else {
    	removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    }
  }

  public org.cocktail.peche.entity.EOMiseEnPaiement createToListeMisesEnPaiementRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.peche.entity.EOMiseEnPaiement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    return (org.cocktail.peche.entity.EOMiseEnPaiement) eo;
  }

  public void deleteToListeMisesEnPaiementRelationship(org.cocktail.peche.entity.EOMiseEnPaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, _EOPaiement.TO_LISTE_MISES_EN_PAIEMENT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToListeMisesEnPaiementRelationships() {
    Enumeration<org.cocktail.peche.entity.EOMiseEnPaiement> objects = toListeMisesEnPaiement().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToListeMisesEnPaiementRelationship(objects.nextElement());
    }
  }


  public static EOPaiement createEOPaiement(EOEditingContext editingContext, Integer anneePaiement
, NSTimestamp dateCreation
, NSTimestamp dateModification
, NSTimestamp datePaiement
, Integer moisPaiement
, Integer numeroPaiement
, String paye
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonneModification) {
    EOPaiement eo = (EOPaiement) EOUtilities.createAndInsertInstance(editingContext, _EOPaiement.ENTITY_NAME);    
		eo.setAnneePaiement(anneePaiement);
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setDatePaiement(datePaiement);
		eo.setMoisPaiement(moisPaiement);
		eo.setNumeroPaiement(numeroPaiement);
		eo.setPaye(paye);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPersonneModificationRelationship(toPersonneModification);
    return eo;
  }

  public static ERXFetchSpecification<EOPaiement> fetchSpec() {
    return new ERXFetchSpecification<EOPaiement>(_EOPaiement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EOPaiement> fetchAllEOPaiements(EOEditingContext editingContext) {
    return _EOPaiement.fetchAllEOPaiements(editingContext, null);
  }

  public static NSArray<EOPaiement> fetchAllEOPaiements(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPaiement.fetchEOPaiements(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPaiement> fetchEOPaiements(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EOPaiement> fetchSpec = new ERXFetchSpecification<EOPaiement>(_EOPaiement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPaiement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EOPaiement fetchEOPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiement.fetchEOPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiement fetchEOPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPaiement> eoObjects = _EOPaiement.fetchEOPaiements(editingContext, qualifier, null);
    EOPaiement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EOPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiement fetchRequiredEOPaiement(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPaiement.fetchRequiredEOPaiement(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPaiement fetchRequiredEOPaiement(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPaiement eoObject = _EOPaiement.fetchEOPaiement(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EOPaiement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPaiement localInstanceIn(EOEditingContext editingContext, EOPaiement eo) {
    EOPaiement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
