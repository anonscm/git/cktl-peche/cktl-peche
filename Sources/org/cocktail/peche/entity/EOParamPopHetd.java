package org.cocktail.peche.entity;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe qui définit les heures en équivalent TD des AP(CM, TD,...) en fonction de la population
 * @author Equipe PECHE
 */
public class EOParamPopHetd extends _EOParamPopHetd {

	private static final long serialVersionUID = -104815281917200361L;

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOParamPopHetd.class);

	/**
	 * Crée et initialise une correspondance Type AP/Population.
	 *
	 * @param edc : contexte d'edition.
	 * @return EOParamPopHetd.
	 */
	public static EOParamPopHetd creerEtInitialiser(EOEditingContext edc) {
		EOParamPopHetd paramPopHetd = new EOParamPopHetd();
		edc.insertObject(paramPopHetd);
		return paramPopHetd;
	}

	/**
	 * Duplique la correspondance entre la population et les HETD
	 * @param source : EOParamPopHetd
	 * @param edc : editingContext
	 * @return : EOParamPopHetd
	 */
	public static EOParamPopHetd dupliquer(EOParamPopHetd source, EOEditingContext edc) {
		EOParamPopHetd copie = creerEtInitialiser(edc);
		copie.remplir(source.localInstanceIn(edc));
		return copie;
	}

	private void remplir(EOParamPopHetd source) {
		EOCorps corps = source.corps();
		if (corps != null) {
			this.setCorps(source.corps());
		} else {
			this.setTypeContratTravail(source.typeContratTravail());
		}
		this.setDateDebut(source.dateDebut());
		this.setDateFin(source.dateFin());
		this.setDenominateurHorsService(source.denominateurHorsService());
		this.setDenominateurService(source.denominateurService());
		this.setNumerateurHorsService(source.numerateurHorsService());
		this.setNumerateurService(source.numerateurService());
		this.setTypeAP(source.typeAP());
	}

	@Override
	public void setCorps(EOCorps value) {
		super.setCorps(value);
		super.setTypeContratTravail(null);
	}
	
	
	@Override
	public void setTypeContratTravail(EOTypeContratTravail value) {
		super.setTypeContratTravail(value);
		super.setCorps(null);
	}

	/**
	 * Renvoie libelle court du corps ou du contrat selectionné
	 * @return libelle court
	 */
	public String libelleCourtCorpsOuContrat() {
		if (typeContratTravail() != null) {
			return typeContratTravail().lcTypeContratTrav();
		} else {
			return corps().lcCorps();
		}
	}
}
