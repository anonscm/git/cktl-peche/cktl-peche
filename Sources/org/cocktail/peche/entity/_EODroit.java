// DO NOT EDIT.  Make changes to EODroit.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EODroit extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EODroit";

  // Attribute Keys
  public static final ERXKey<org.joda.time.DateTime> DATE_CREATION = new ERXKey<org.joda.time.DateTime>("dateCreation");
  public static final ERXKey<org.joda.time.DateTime> DATE_MODIFICATION = new ERXKey<org.joda.time.DateTime>("dateModification");
  public static final ERXKey<Integer> ID = new ERXKey<Integer>("id");
  public static final ERXKey<String> TEMOIN_ENSEIGNANT_ENFANT = new ERXKey<String>("temoinEnseignantEnfant");
  public static final ERXKey<String> TEMOIN_ENSEIGNEMENT_ENFANT = new ERXKey<String>("temoinEnseignementEnfant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividu");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil> TO_PROFIL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil>("toProfil");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_ENSEIGNANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureEnseignant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE_ENSEIGNEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructureEnseignement");
  public static final ERXKey<org.cocktail.peche.entity.EOTypePopulation> TO_TYPE_POPULATION = new ERXKey<org.cocktail.peche.entity.EOTypePopulation>("toTypePopulation");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String ID_KEY = ID.key();
  public static final String TEMOIN_ENSEIGNANT_ENFANT_KEY = TEMOIN_ENSEIGNANT_ENFANT.key();
  public static final String TEMOIN_ENSEIGNEMENT_ENFANT_KEY = TEMOIN_ENSEIGNEMENT_ENFANT.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_INDIVIDU_KEY = TO_INDIVIDU.key();
  public static final String TO_PROFIL_KEY = TO_PROFIL.key();
  public static final String TO_STRUCTURE_ENSEIGNANT_KEY = TO_STRUCTURE_ENSEIGNANT.key();
  public static final String TO_STRUCTURE_ENSEIGNEMENT_KEY = TO_STRUCTURE_ENSEIGNEMENT.key();
  public static final String TO_TYPE_POPULATION_KEY = TO_TYPE_POPULATION.key();

  private static Logger LOG = Logger.getLogger(_EODroit.class);

  public EODroit localInstanceIn(EOEditingContext editingContext) {
    EODroit localInstance = (EODroit)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.joda.time.DateTime dateCreation() {
    return (org.joda.time.DateTime) storedValueForKey(_EODroit.DATE_CREATION_KEY);
  }

  public void setDateCreation(org.joda.time.DateTime value) {
    if (_EODroit.LOG.isDebugEnabled()) {
    	_EODroit.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EODroit.DATE_CREATION_KEY);
  }

  public org.joda.time.DateTime dateModification() {
    return (org.joda.time.DateTime) storedValueForKey(_EODroit.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(org.joda.time.DateTime value) {
    if (_EODroit.LOG.isDebugEnabled()) {
    	_EODroit.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EODroit.DATE_MODIFICATION_KEY);
  }

  public Integer id() {
    return (Integer) storedValueForKey(_EODroit.ID_KEY);
  }

  public void setId(Integer value) {
    if (_EODroit.LOG.isDebugEnabled()) {
    	_EODroit.LOG.debug( "updating id from " + id() + " to " + value);
    }
    takeStoredValueForKey(value, _EODroit.ID_KEY);
  }

  public String temoinEnseignantEnfant() {
    return (String) storedValueForKey(_EODroit.TEMOIN_ENSEIGNANT_ENFANT_KEY);
  }

  public void setTemoinEnseignantEnfant(String value) {
    if (_EODroit.LOG.isDebugEnabled()) {
    	_EODroit.LOG.debug( "updating temoinEnseignantEnfant from " + temoinEnseignantEnfant() + " to " + value);
    }
    takeStoredValueForKey(value, _EODroit.TEMOIN_ENSEIGNANT_ENFANT_KEY);
  }

  public String temoinEnseignementEnfant() {
    return (String) storedValueForKey(_EODroit.TEMOIN_ENSEIGNEMENT_ENFANT_KEY);
  }

  public void setTemoinEnseignementEnfant(String value) {
    if (_EODroit.LOG.isDebugEnabled()) {
    	_EODroit.LOG.debug( "updating temoinEnseignementEnfant from " + temoinEnseignementEnfant() + " to " + value);
    }
    takeStoredValueForKey(value, _EODroit.TEMOIN_ENSEIGNEMENT_ENFANT_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EODroit.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EODroit.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EODroit.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EODroit.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(_EODroit.TO_INDIVIDU_KEY);
  }
  
  public void setToIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, _EODroit.TO_INDIVIDU_KEY);
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToIndividu(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.TO_INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.TO_INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toProfil() {
    return (org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil)storedValueForKey(_EODroit.TO_PROFIL_KEY);
  }
  
  public void setToProfil(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil value) {
    takeStoredValueForKey(value, _EODroit.TO_PROFIL_KEY);
  }

  public void setToProfilRelationship(org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating toProfil from " + toProfil() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToProfil(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil oldValue = toProfil();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.TO_PROFIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.TO_PROFIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEnseignant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EODroit.TO_STRUCTURE_ENSEIGNANT_KEY);
  }
  
  public void setToStructureEnseignant(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EODroit.TO_STRUCTURE_ENSEIGNANT_KEY);
  }

  public void setToStructureEnseignantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating toStructureEnseignant from " + toStructureEnseignant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructureEnseignant(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureEnseignant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.TO_STRUCTURE_ENSEIGNANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.TO_STRUCTURE_ENSEIGNANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEnseignement() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EODroit.TO_STRUCTURE_ENSEIGNEMENT_KEY);
  }
  
  public void setToStructureEnseignement(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EODroit.TO_STRUCTURE_ENSEIGNEMENT_KEY);
  }

  public void setToStructureEnseignementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating toStructureEnseignement from " + toStructureEnseignement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructureEnseignement(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructureEnseignement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.TO_STRUCTURE_ENSEIGNEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.TO_STRUCTURE_ENSEIGNEMENT_KEY);
    }
  }
  
  public org.cocktail.peche.entity.EOTypePopulation toTypePopulation() {
    return (org.cocktail.peche.entity.EOTypePopulation)storedValueForKey(_EODroit.TO_TYPE_POPULATION_KEY);
  }
  
  public void setToTypePopulation(org.cocktail.peche.entity.EOTypePopulation value) {
    takeStoredValueForKey(value, _EODroit.TO_TYPE_POPULATION_KEY);
  }

  public void setToTypePopulationRelationship(org.cocktail.peche.entity.EOTypePopulation value) {
    if (_EODroit.LOG.isDebugEnabled()) {
      _EODroit.LOG.debug("updating toTypePopulation from " + toTypePopulation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToTypePopulation(value);
    }
    else if (value == null) {
    	org.cocktail.peche.entity.EOTypePopulation oldValue = toTypePopulation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EODroit.TO_TYPE_POPULATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EODroit.TO_TYPE_POPULATION_KEY);
    }
  }
  

  public static EODroit createEODroit(EOEditingContext editingContext, org.joda.time.DateTime dateCreation
, org.joda.time.DateTime dateModification
, Integer id
, String temoinEnseignantEnfant
, String temoinEnseignementEnfant
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil toProfil, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEnseignant, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructureEnseignement, org.cocktail.peche.entity.EOTypePopulation toTypePopulation) {
    EODroit eo = (EODroit) EOUtilities.createAndInsertInstance(editingContext, _EODroit.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setDateModification(dateModification);
		eo.setId(id);
		eo.setTemoinEnseignantEnfant(temoinEnseignantEnfant);
		eo.setTemoinEnseignementEnfant(temoinEnseignementEnfant);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setPersonneModificationRelationship(personneModification);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToProfilRelationship(toProfil);
    eo.setToStructureEnseignantRelationship(toStructureEnseignant);
    eo.setToStructureEnseignementRelationship(toStructureEnseignement);
    eo.setToTypePopulationRelationship(toTypePopulation);
    return eo;
  }

  public static ERXFetchSpecification<EODroit> fetchSpec() {
    return new ERXFetchSpecification<EODroit>(_EODroit.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EODroit> fetchAllEODroits(EOEditingContext editingContext) {
    return _EODroit.fetchAllEODroits(editingContext, null);
  }

  public static NSArray<EODroit> fetchAllEODroits(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODroit.fetchEODroits(editingContext, null, sortOrderings);
  }

  public static NSArray<EODroit> fetchEODroits(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EODroit> fetchSpec = new ERXFetchSpecification<EODroit>(_EODroit.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODroit> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EODroit fetchEODroit(EOEditingContext editingContext, String keyName, Object value) {
    return _EODroit.fetchEODroit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODroit fetchEODroit(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODroit> eoObjects = _EODroit.fetchEODroits(editingContext, qualifier, null);
    EODroit eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EODroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODroit fetchRequiredEODroit(EOEditingContext editingContext, String keyName, Object value) {
    return _EODroit.fetchRequiredEODroit(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODroit fetchRequiredEODroit(EOEditingContext editingContext, EOQualifier qualifier) {
    EODroit eoObject = _EODroit.fetchEODroit(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EODroit that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODroit localInstanceIn(EOEditingContext editingContext, EODroit eo) {
    EODroit localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
