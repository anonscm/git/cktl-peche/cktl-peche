// DO NOT EDIT.  Make changes to EORepartUefStructure.java instead.
package org.cocktail.peche.entity;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;

@SuppressWarnings("all")
public abstract class _EORepartUefStructure extends  ERXGenericRecord {
  public static final String ENTITY_NAME = "EORepartUefStructure";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> PERSONNE_MODIFICATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("personneModification");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> TO_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("toStructure");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  // Relationships
  public static final String PERSONNE_CREATION_KEY = PERSONNE_CREATION.key();
  public static final String PERSONNE_MODIFICATION_KEY = PERSONNE_MODIFICATION.key();
  public static final String TO_STRUCTURE_KEY = TO_STRUCTURE.key();

  private static Logger LOG = Logger.getLogger(_EORepartUefStructure.class);

  public EORepartUefStructure localInstanceIn(EOEditingContext editingContext) {
    EORepartUefStructure localInstance = (EORepartUefStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(_EORepartUefStructure.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (_EORepartUefStructure.LOG.isDebugEnabled()) {
    	_EORepartUefStructure.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefStructure.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(_EORepartUefStructure.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (_EORepartUefStructure.LOG.isDebugEnabled()) {
    	_EORepartUefStructure.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, _EORepartUefStructure.DATE_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefStructure.PERSONNE_CREATION_KEY);
  }
  
  public void setPersonneCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefStructure.PERSONNE_CREATION_KEY);
  }

  public void setPersonneCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefStructure.LOG.isDebugEnabled()) {
      _EORepartUefStructure.LOG.debug("updating personneCreation from " + personneCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneCreation(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefStructure.PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefStructure.PERSONNE_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneModification() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(_EORepartUefStructure.PERSONNE_MODIFICATION_KEY);
  }
  
  public void setPersonneModification(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    takeStoredValueForKey(value, _EORepartUefStructure.PERSONNE_MODIFICATION_KEY);
  }

  public void setPersonneModificationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (_EORepartUefStructure.LOG.isDebugEnabled()) {
      _EORepartUefStructure.LOG.debug("updating personneModification from " + personneModification() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setPersonneModification(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = personneModification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefStructure.PERSONNE_MODIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefStructure.PERSONNE_MODIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(_EORepartUefStructure.TO_STRUCTURE_KEY);
  }
  
  public void setToStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, _EORepartUefStructure.TO_STRUCTURE_KEY);
  }

  public void setToStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EORepartUefStructure.LOG.isDebugEnabled()) {
      _EORepartUefStructure.LOG.debug("updating toStructure from " + toStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
    	setToStructure(value);
    }
    else if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toStructure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, _EORepartUefStructure.TO_STRUCTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, _EORepartUefStructure.TO_STRUCTURE_KEY);
    }
  }
  

  public static EORepartUefStructure createEORepartUefStructure(EOEditingContext editingContext, NSTimestamp dateCreation
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne personneCreation, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toStructure) {
    EORepartUefStructure eo = (EORepartUefStructure) EOUtilities.createAndInsertInstance(editingContext, _EORepartUefStructure.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
    eo.setPersonneCreationRelationship(personneCreation);
    eo.setToStructureRelationship(toStructure);
    return eo;
  }

  public static ERXFetchSpecification<EORepartUefStructure> fetchSpec() {
    return new ERXFetchSpecification<EORepartUefStructure>(_EORepartUefStructure.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<EORepartUefStructure> fetchAllEORepartUefStructures(EOEditingContext editingContext) {
    return _EORepartUefStructure.fetchAllEORepartUefStructures(editingContext, null);
  }

  public static NSArray<EORepartUefStructure> fetchAllEORepartUefStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartUefStructure.fetchEORepartUefStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartUefStructure> fetchEORepartUefStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<EORepartUefStructure> fetchSpec = new ERXFetchSpecification<EORepartUefStructure>(_EORepartUefStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartUefStructure> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static EORepartUefStructure fetchEORepartUefStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefStructure.fetchEORepartUefStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefStructure fetchEORepartUefStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartUefStructure> eoObjects = _EORepartUefStructure.fetchEORepartUefStructures(editingContext, qualifier, null);
    EORepartUefStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one EORepartUefStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefStructure fetchRequiredEORepartUefStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartUefStructure.fetchRequiredEORepartUefStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartUefStructure fetchRequiredEORepartUefStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartUefStructure eoObject = _EORepartUefStructure.fetchEORepartUefStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no EORepartUefStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartUefStructure localInstanceIn(EOEditingContext editingContext, EORepartUefStructure eo) {
    EORepartUefStructure localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
