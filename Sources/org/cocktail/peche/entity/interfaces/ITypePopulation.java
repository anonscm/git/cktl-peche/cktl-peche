package org.cocktail.peche.entity.interfaces;

import org.joda.time.DateTime;

/**
 * Contrat d'un type de population
 *
 * @author Chama LAATIK
 */
public interface ITypePopulation {
	
	/**
	 * @return le code
	 */
	String getCode();
	
	/**
	 * @param value : code du type de population
	 */
	void setCode(String value);
	
	/**
	 * @return le libellé
	 */
	String getLibelle();
	
	/**
	 * @param value : libelle du type de population
	 */
	void setLibelle(String value);
	
	/**
	 * @return date de création
	 */
	DateTime getDateCreation();
	
	/**
	 * @param value : date de création
	 */
	void setDateCreation(DateTime dateCreation);

	/**
	 * @return date de modification
	 */
	DateTime getDateModification();

	/**
	 * @param value : date de modification
	 */
	void setDateModification(DateTime value);
}
