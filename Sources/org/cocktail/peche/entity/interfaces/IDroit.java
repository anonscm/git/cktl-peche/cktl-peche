package org.cocktail.peche.entity.interfaces;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.joda.time.DateTime;

/**
 * Contrat d'un droit dans Peche
 *
 * @author Chama LAATIK
 */
public interface IDroit {

	boolean isTemoinEnseignantEnfant();
	
	void setTemoinEnseignantEnfant(boolean value);
	
	boolean isTemoinEnseignementEnfant();
	
	void setTemoinEnseignementEnfant(boolean value);
	
	IIndividu getToIndividu();
	
	void setToIndividu(IIndividu value);
	
	EOGdProfil getToProfil();
	
	void setToProfil(EOGdProfil value);
	
	IStructure getToStructureEnseignement();
	
	void setToStructureEnseignement(IStructure value);
	
	IStructure getToStructureEnseignant();
	
	void setToStructureEnseignant(IStructure value);
	
	ITypePopulation getToTypePopulation();
	
	void setToTypePopulation(ITypePopulation value);	

	/**
	 * @return personne qui crée l'enregistrement
	 */
	EOPersonne getPersonneCreation();

	void setPersonneCreation(EOPersonne value);

	/**
	 * @return personne qui modifie l'enregistrement
	 */
	EOPersonne getPersonneModification();

	void setPersonneModification(EOPersonne value);
	
	/**
	 * @return date de création
	 */
	DateTime getDateCreation();
	
	/**
	 * @param value : date de création
	 */
	void setDateCreation(DateTime dateCreation);

	/**
	 * @return date de modification
	 */
	DateTime getDateModification();

	/**
	 * @param value : date de modification
	 */
	void setDateModification(DateTime value);
	
}
