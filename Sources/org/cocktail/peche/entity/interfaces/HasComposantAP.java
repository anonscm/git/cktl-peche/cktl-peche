package org.cocktail.peche.entity.interfaces;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;

/**
 * Interface commune aux éléments qui ont un composant AP.
 * @author Equipe GRH
 *
 */
public interface HasComposantAP {

	/**
	 * @return la composante AP.
	 */
	IAP composantAP();
	
}
