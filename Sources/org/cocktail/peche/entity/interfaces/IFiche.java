package org.cocktail.peche.entity.interfaces;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;

import com.webobjects.eocontrol.EOEditingContext;

public interface IFiche {

	
	/**
	 * Réinitialise la ou les demandes liées à la fiche
	 * Le circuit de validation passé en paramètre est celui sur lequel les demandes doivent être créées
	 * @param edc editing context
	 * @param persId persId
	 * @param circuitValidation un circuit de validation Peche
	 */
	void reinitialiserDemandes(EOEditingContext edc, Integer persId, EOCircuitValidation circuitValidation);
	
	
}
