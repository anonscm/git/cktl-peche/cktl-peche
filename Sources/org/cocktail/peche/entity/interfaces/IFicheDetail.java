package org.cocktail.peche.entity.interfaces;


/**
 * 
 * @author Equipe GRH
 */
public interface IFicheDetail {

	/**
	 * @return le libelle d'affichage de l'enseignement.
	 */
	public String affichageEnseignement();
	
	
	/**
	 * @return le libelle d'affichage du type.
	 */
	public String affichageType();
	
}
