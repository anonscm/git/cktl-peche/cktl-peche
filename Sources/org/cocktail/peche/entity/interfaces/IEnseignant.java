package org.cocktail.peche.entity.interfaces;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

/**
 * Contrat d'un enseignant
 *
 * @author Chama LAATIK
 */
public interface IEnseignant {
	
	/**
	 * @return la structure de l'enseignant
	 */
	IStructure getStructure(Integer annee, boolean isAnneeUnivEnAnneeCivil);
	/**
	 * 
	 * @param annee : année universitaire
	 * @param isAnneeUnivEnAnneeCivil : Est-ce que l'année universitaire est en année civil?
	 * @return Vrai si l'enseignant est statutaire
	 */
	boolean isStatutaire(Integer annee, boolean isAnneeUnivEnAnneeCivil);
}
