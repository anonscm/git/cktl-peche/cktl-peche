package org.cocktail.peche.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.entity.interfaces.HasComposantAP;
import org.cocktail.peche.entity.interfaces.IFicheDetail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

/**
 * Cette classe décrit le détail du service de chaque intervenant
 * 
 * @author Chama LAATIK
 */
public class EOServiceDetail extends _EOServiceDetail implements HasComposantAP, IFicheDetail {

	private static final long serialVersionUID = -9103209375505582802L;
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOServiceDetail.class);
	
	/**
	 * Creer et initialise le détail du service.
	 * 
	 * @param edc
	 *            : contexte d'edition.
	 * @return un élement du service.
	 */
	public static EOServiceDetail creerEtInitialiser(EOEditingContext edc) {
		EOServiceDetail serviceDetail = new EOServiceDetail();
		serviceDetail.setHeuresPrevues(0.0);
		serviceDetail.setHeuresRealisees(0.0);
		edc.insertObject(serviceDetail);

		return serviceDetail;
	}
	
	@Override
	public void setHeuresAPayer(BigDecimal value) {
		if (value != null) {
			// On force 2 après la virgule
			value = value.setScale(2, RoundingMode.HALF_UP);
		}

		super.setHeuresAPayer(value);
	}

	@Override
	public void setHeuresPayees(BigDecimal value) {
		if (value != null) {
			// On force 2 après la virgule
			value = value.setScale(2, RoundingMode.HALF_UP);
		}

		super.setHeuresPayees(value);
	}

	@Override
	public void setReh(EOReh value) {
		super.setReh(value);
		super.setComposantAP(null);
	}

	@Override
	public void setComposantAP(EOAP value) {
		super.setComposantAP(value);
		super.setReh(null);
	}
	
	/**
	 * Retourne vrai si au moins une des 2 entités est non nulles ==> sert lors
	 * de l'insertion d'un service
	 * 
	 * @param reh : Référentiel des des Equivalences Horaires
	 * @param composantAP : AP présent dans l'offre de formation
	 * @return vrai si au moins une des 2 entités est non nulles
	 */
	public boolean verifierEntiteNonNull(EOReh reh, EOAP composantAP) {
		return (reh != null) || (composantAP != null);
	}

	private void copier(EOServiceDetail source) {
		if (source.reh() != null) {
			this.setRehRelationship(source.reh());
		} else {
			this.setComposantAPRelationship(source.composantAP());
		}
		
		this.setHeuresPrevues(source.heuresPrevues());
		this.setHeuresRealisees(source.heuresRealisees());
		this.setHeuresAPayer(source.heuresAPayer());
		this.setHeuresPayees(source.heuresPayees());
		this.setCommentaire(source.commentaire());		
		this.setEtat(source.etat());
		
		this.setDateCommentaire(source.dateCommentaire());
		this.setDateCreation(source.dateCreation());
		this.setPersonneCreationRelationship(source.personneCreation());
		this.setDateModification(source.dateModification());
		this.setPersonneModificationRelationship(source.personneModification());
		this.setToStructure(source.toStructure());
		this.setToRepartService(source.toRepartService());
	}
	
	/**
	 * duplique un service détail existant 
	 * @param source : le service détail à dupliquer
	 * @param edc : contexte d'édition.
	 * @return : le nouveau service détail
	 */
	public static EOServiceDetail dupliquer(EOServiceDetail source, EOEditingContext edc) {
		EOServiceDetail copie = EOServiceDetail.creerEtInitialiser(edc);
		copie.copier(source);
		return copie;
	}
	
	/**
	 * /**
	 * Méthode pour l'affichage de l'AP et de son parent direct
	 * @param composantAP : AP présent dan sl'offre de formation
	 * @return : affichage de ce type ==> Parent : AP
	 */
	public String affichageAPEtParent(EOAP composantAP) {
		return ComposantAffichageHelper.getInstance().affichageAPEtParent(composantAP);
	}

	/**
	 * @return Le tri par défaut de la liste suivant le nom des enseignants génériques ou pas
	 */
	//FIXME : tri par actuel enseignant puis par enseignant générique
	// ==> voir comment avoir tri par enseignant
	public static ERXSortOrderings getTriParDefaut() {
		return EOServiceDetail.COMPOSANT_AP.dot(EOAP.TYPE_AP).dot(EOTypeAP.CODE_KEY).asc()
			.then(EOServiceDetail.SERVICE.dot(EOService.TO_ENSEIGNANT_GENERIQUE).dot(EOEnseignantGenerique.NOM_KEY).asc())
			.then(EOServiceDetail.SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE_KEY).asc());
	}
	
	/**
	 * Mise à jour des données d'audit dans le cas où on est en création de l'instance.
	 * 
	 * @param personne la personne qui crée l'instance
	 */
	public void majDonnesAuditCreation(EOPersonne personne) {
		NSTimestamp now = new NSTimestamp();
		setDateCreation(now);
		setDateModification(now);
		setPersonneCreationRelationship(personne);
		setPersonneModificationRelationship(personne);
		
		if (!StringCtrl.isEmpty(commentaire())) {
			setDateCommentaire(now);
		}
	}
	
	/**
	 * Mise à jour les données d'audit dans le cas où on modifie des données de l'instance.
	 * 
	 * @param personne la personne qui modifie l'instance
	 */
	public void majDonnesAuditModification(EOPersonne personne) {
		if (!changesFromCommittedSnapshot().isEmpty()) {
			NSTimestamp now = new NSTimestamp();
			setDateModification(now);
			setPersonneModificationRelationship(personne);
			
			if (hasKeyChangedFromCommittedSnapshot(EOServiceDetail.COMMENTAIRE_KEY)) {
				setDateCommentaire(now);
			}
		}
	}
	
	
	 public void setToStructureRelationship(IStructure value) {
		 super.setToStructureRelationship((EOStructure) value);
	 }
		   
	 
	 /**
	  * @return le libellé d'affichage de l'enseignement.
	  */
	 public String affichageEnseignement() {
		 return ComposantAffichageHelper.getInstance().affichageEnseignementParent(composantAP());
	 }

	 /**
	  * @return le libellé d'affichage du type.
	  */
	public String affichageType() {
		return ComposantAffichageHelper.getInstance().affichageType(composantAP());
	}
	
}
