package org.cocktail.peche;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.PersonneDelegate;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebAction;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlLoginResponder;
import org.cocktail.peche.components.Accueil;
import org.cocktail.peche.components.Timeout;
import org.cocktail.peche.components.commun.ErreurCAS;
import org.cocktail.peche.components.commun.Login;
import org.cocktail.peche.outils.PecheLogger;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WORequest;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEC;

/**
 * Cette classe traite toutes les requêtes destinées à l'application Peche.
 * 
 * @author yannick
 * 
 */
public class DirectAction extends CktlWebAction {

	private static final PecheLogger LOGGER = PecheLogger
			.getLogger(DirectAction.class);

	/**
	 * Constructeur.
	 * 
	 * @param request
	 *            la requête en provenance du navigateur.
	 */
	public DirectAction(WORequest request) {
		super(request);
	}

	protected Session laSession() {
		Session laSession = (Session) existingSession();
		if (laSession == null) {
			laSession = (Session) session();
		}
		return laSession;
	}

	@Override
	public WOActionResults defaultAction() {
		if (useCasService()) {
			return loginCASPage();
		} else {
			return loginNoCasPage(null);
		}
	}

	@Override
	public WOActionResults loginCasSuccessPage(String netid,
			@SuppressWarnings("rawtypes") NSDictionary actionParams) {
		return loginCasSuccessPage(netid);
	}

	/**
	 * Traitement de l'authentification CAS réussie.
	 * 
	 * @param netid
	 *            l'identifiant de l'utilisateur connecté.
	 * @return l'action qui mène à la page principale de l'application.
	 */
	public WOActionResults loginCasSuccessPage(String netid) {
		try {
			LOGGER.debug("loginCasSuccessPage : %s", netid);
			ERXEC edc = (ERXEC) laSession().defaultEditingContext();
			IPersonne pers = PersonneDelegate.fetchPersonneForLogin(edc, netid);
			if (pers == null) {
				throw new Exception(
						"Impossible de recuperer un objet personne associe au login "
								+ netid);
			}
			LOGGER.debug("loginCasSuccessPage : %s %s", pers.persLibelle(),
					pers.persLc());

			if (!initGestionDroits(laSession(), pers.persId())) {
				ErreurCAS page = (ErreurCAS) pageWithName(ErreurCAS.class.getName());

				if (session() != null) {
					session().terminate();
				}
				
				return page;
			}
			PecheApplicationUser appUser = new PecheApplicationUser(edc, Integer.valueOf(pers.persId().intValue()),laSession().getPecheParametres().getAnneeUniversitaireEnCours(),laSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			laSession().setApplicationUser(appUser);
			WOActionResults nextPage = pageWithName(getPagePrincipale().getName());
			return nextPage;
		} catch (Exception e) {
			e.printStackTrace();
			return getErrorPage(e.getMessage());
		}
	}

	private WOComponent getErrorPage(String errorMessage) {
		CktlAlertPage page = (CktlAlertPage) cktlApp.pageWithName(
				"CktlAlertPage", context());
		page.showMessage(null, cktlApp.name() + " : ERREUR", errorMessage,
				null, null, null, CktlAlertPage.ERROR, null);
		return page;
	}

	public WOActionResults applicationExceptionAction() {
		Accueil nextPage = (Accueil)pageWithName(Accueil.class.getName());		
		nextPage.setIsOpenFenetreException(true);		
		return nextPage;
	}
	
	public WOActionResults sessionExpiredAction() {
		Timeout nextPage = (Timeout)pageWithName(Timeout.class.getName());
		return nextPage;
	}
	
	@Override
	public WOActionResults loginCasFailurePage(String errorMessage,
			String errorCode) {
		LOGGER.debug("loginCasFailurePage : %s (%s)", errorMessage,
				errorMessage);
		StringBuffer msg = new StringBuffer();
		msg.append("Une erreur s'est produite lors de l'authentification de l'uilisateur&nbsp;:<br><br>");
		if (errorMessage != null) {
			msg.append("&nbsp;:<br><br>").append(errorMessage);
		}
		return getErrorPage(msg.toString());
	}

	WOActionResults loginCASPage() {
		String url = DirectAction.getLoginActionURL(this.context(), false, null, true, null);
		WORedirect page = (WORedirect) pageWithName(WORedirect.class.getName());
		page.setUrl(url);
		return page;
//		return loginNoCasPage(null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public WOActionResults loginNoCasPage(
			@SuppressWarnings("rawtypes") NSDictionary actionParams) {
		Login page = (Login) pageWithName(Login.class);
		page.registerLoginResponder(getNewLoginResponder(actionParams));
		return page;
	}

	private DefaultLoginResponder getNewLoginResponder(
			NSDictionary<String, String> actionParams) {
		return new DefaultLoginResponder(this, actionParams);
	}


	
	/**
	 * Validation du couple identifiant/mot de passe lorsque l'on n'utilise pas
	 * le service CAS.
	 * 
	 * @return l'action qui même à la page principale de l'application.
	 */
	public WOActionResults validerLoginAction() {
		WOActionResults page = null;
		WORequest request = context().request();
		String login = StringCtrl.normalize((String) request
				.formValueForKey("identifiant"));
		String password = StringCtrl.normalize((String) request
				.formValueForKey("mot_de_passe"));
		String messageErreur = null;
		Session session = (Session) context().session();

		CktlLoginResponder loginResponder = getNewLoginResponder(null);
		CktlUserInfo loggedUserInfo = new CktlUserInfoDB(cktlApp.dataBus());

		if (login.length() == 0) {
			messageErreur = "Vous devez renseigner l'identifiant.";
		} else if (!loginResponder.acceptLoginName(login)) {
			messageErreur = "Vous n'êtes pas autorisé(e) à utiliser cette application";
		} else {
			if (password == null) {
				password = "";
			}
			loggedUserInfo.setRootPass(loginResponder.getRootPassword());
			loggedUserInfo.setAcceptEmptyPass(loginResponder
					.acceptEmptyPassword());
			loggedUserInfo.compteForLogin(login, password, true);
			if (loggedUserInfo.errorCode() != CktlUserInfo.ERROR_NONE) {
				if (loggedUserInfo.errorMessage() != null) {
					messageErreur = loggedUserInfo.errorMessage();
				}
				LOGGER.error(loggedUserInfo.errorMessage());
			}
		}

		if (messageErreur == null) {
			session.setConnectedUserInfo(loggedUserInfo);
			String erreur = session.setConnectedUser(loggedUserInfo.login());
			if (erreur != null) {
				messageErreur = erreur;
			} else {
				if (!initGestionDroits(session, loggedUserInfo.persId())) {
					messageErreur = "Vous n'avez pas le droit d'accéder à cette application.";
				}
			}
		}

		if (messageErreur != null) {
			page = (Login) pageWithName(Login.class.getName());
			((Login) page).setMessageErreur(messageErreur);
			return page;
		}

		return loginResponder.loginAccepted(null);
	}

	private boolean initGestionDroits(Session session, Number persId) {
		/* Initalisation de la gestion des droits par profil */
		PecheAutorisationsCache pecheAutorisationCache = new PecheAutorisationsCache(persId.intValue(), laSession().defaultEditingContext());
		laSession().setPecheAutorisationCache(pecheAutorisationCache);

		/* Enregistrement en session de l'utilisateur identifié */
		PecheApplicationUser appUser = new PecheApplicationUser(laSession().defaultEditingContext(), Integer.valueOf(persId.intValue()), 
				laSession().getPecheParametres().getAnneeUniversitaireEnCours(),laSession().getPecheParametres().isFormatAnneeExerciceAnneeCivile());
		laSession().setApplicationUser(appUser);

		LOGGER.debug("setting edc = %s", session.defaultEditingContext()
				.toString());

		return pecheAutorisationCache.hasDroitAccesPeche();
	}

	Class<? extends WOActionResults> getPagePrincipale() {
		return Accueil.class;
	}

}
