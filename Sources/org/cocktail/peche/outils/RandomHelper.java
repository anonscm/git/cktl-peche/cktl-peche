package org.cocktail.peche.outils;

import java.util.UUID;


/**
 * Classe utilitaire pour la génération d'éléments aléatoires.
 */
public class RandomHelper {

	private static RandomHelper INSTANCE;
	
	/**
	 * @return l'instance du singleton.
	 */
	public static RandomHelper getInstance(){
		if(INSTANCE == null){
			INSTANCE = new RandomHelper();
		}
		return INSTANCE;
	}
	
	/**
	 * @return une chaine de caractère randomisée.
	 */
	public String getRandomString(){
		return UUID.randomUUID().toString();
	}

	/**
	 * @return une chaine de caractère randomisée.
	 */
	public String getRandomStringWithPrefix(String prefix){
		return prefix + UUID.randomUUID().toString();
	}
	
	
}
