package org.cocktail.peche.outils;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Permet de gérer des listes {@link NSArray} à la place de {@link List} quand c'est necessaire
 * ( exemple dans les qualifiers)
 * 
 * @author Chama LAATIK
 *
 */
public class ListHelper {
	
	private NSArray<EOStructure> listeWO;
	
	/**
	 * Constructeur.
	 * 
	 * @param editingContext Un editing context
	 */
	public ListHelper() {
		listeWO = new NSMutableArray<EOStructure>();
	}
	
	/**
	 * Permet de créer une liste NSArray<EOStructure> à partir d'une liste List<IStructure>
	 * 	afin de l'utiliser notamment pour les qualifier qui n'acceptent pas de List<IStructure>
	 * @param listeStructures : liste des structures
	 * @return une liste NSArray de structures
	 */
	public NSArray<EOStructure> bidouilleListeStructurePourWO(List<IStructure> listeStructures) {
		if (listeWO.isEmpty()) {
			if (!CollectionUtils.isEmpty(listeStructures)) {
				for (IStructure uneStructure : listeStructures) {
					listeWO.add((EOStructure) uneStructure);
				}
			}
		}
		return listeWO;
	}
	
}
