package org.cocktail.peche.outils;

import org.apache.log4j.spi.LoggingEvent;

import er.extensions.logging.ERXPatternLayout;

/**
 * Pour débugger. Prototype à utiliser avec des pincettes car il peut provoquer
 * l'implosion du soleil et la destruction de toutes vies dans l'univers s'il
 * est mal utilisé.
 * 
 * @author Equipe PECHE.
 * 
 */
public class CktlPechePatternLayout extends ERXPatternLayout {

	@Override
	public String format(LoggingEvent event) {
		if ("NSLog".equals(event.getLoggerName())) {
			System.err.println(event.getLocationInformation().fullInfo);
			StackTraceElement[] stackTrace = Thread.currentThread()
					.getStackTrace();
			System.err.println(stackTrace == null);
			for (StackTraceElement element : stackTrace) {
				System.err.println(element.toString());
			}
		}
		return event.getMessage().toString();
	}

}
