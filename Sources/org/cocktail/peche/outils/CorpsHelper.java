package org.cocktail.peche.outils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlgrh.common.MangueParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.cocktail.fwkcktlwebapp.common.util.CocktailConstantes;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * Classe utilitaire pour afficher une liste de corps et mémoriser la sélection.
 * 
 * @author Equipe Peche.
 * 
 */
public class CorpsHelper {

	private NSArray<EOCorps> listeCorps;
	private NSArray<EOTypeContratTravail> listeTypeContratsTravail;
	private NSArray<EOTypePopulation> listeTypePopulation;
	
	private EOCorps corps;
	private EOTypePopulation typePopulation;
	private EOTypeContratTravail contratTravail;
	private EOCorps selectionCorps;
	private EOTypeContratTravail selectionContratTravail;
	private EOTypePopulation selectionPopulation;
	

	/**
	 * Constructeur.
	 * @param edc le contexte d'édition de WebObjec.
	 */
	public CorpsHelper(EOEditingContext edc) {
		this.setListeCorps(EOCorps.fetchAll(edc, EOCorps.LL_CORPS.ascs()));
	}

	/**
	 * Constructeur.
	 * @param edc le contexte d'édition de WebObjec.
	 * @param anneeUniversitaire l'année universitaire en cours
	 */
	
	public CorpsHelper(EOEditingContext edc, String anneeUniversitaire) {
		// initialisation de la liste des corps enseignants non fermés depuis le 02/01 de l'année universitaire en cours

		listeCorps = new NSMutableArray<EOCorps>();	
		setListeTypePopulation(new NSMutableArray<EOTypePopulation>());
		
		java.text.SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		NSTimestamp dateReference = null;
		
		try {
			dateReference = new NSTimestamp(simpleDateFormat.parse(new StringBuffer("02/01/").append(anneeUniversitaire).toString()));
		} catch (ParseException e1) {
			// exception non traitée
		}
		NSArray<EOTypePopulation> typesPopulationEnseignant = EOTypePopulation.fetchAll(edc, EOTypePopulation.TEM_ENSEIGNANT.eq(CocktailConstantes.VRAI), EOTypePopulation.LL_TYPE_POPULATION.ascs());
		
		listeTypePopulation.addAll(typesPopulationEnseignant);
		
		for (int i = 0; i < typesPopulationEnseignant.count(); i++) {
			listeCorps.addAll(EOCorps.rechercherCorpsPourTypePopulation(edc, typesPopulationEnseignant.get(i), dateReference));
		}
		
		NSComparator nsComparatorLLCorps = new NSComparator() {		
			public int compare(Object corps0, Object corps1) {
				return ((EOCorps) corps0).llCorps().toUpperCase().compareTo(((EOCorps) corps1).llCorps().toUpperCase());
			}
		};
	
		try {
			listeCorps = listeCorps.sortedArrayUsingComparator(nsComparatorLLCorps);
		} catch (ComparisonException e) {
			// exception non traitée
		}
		
		//On ajoute que les types de contrat ouverts, visibles et avec le témoin Enseignement
		EOQualifier qualTypeContratTravail = (EOTypeContratTravail.D_FIN_VAL.isNull()
				.or(EOTypeContratTravail.D_FIN_VAL.after(dateReference)))
				.and(EOTypeContratTravail.TEM_VISIBLE.eq(CocktailConstantes.VRAI))
				.and(EOTypeContratTravail.TEM_ENSEIGNEMENT.eq(CocktailConstantes.VRAI));
		
		if (!MangueParametres.isGestionHu(edc)) {			
			qualTypeContratTravail = ERXQ.and(qualTypeContratTravail, EOTypeContratTravail.TEM_AH_CU_AO.eq(CocktailConstantes.FAUX));
		}
	
		listeTypeContratsTravail = new NSMutableArray<EOTypeContratTravail>();
		listeTypeContratsTravail = EOTypeContratTravail.fetchAll(edc, 
				qualTypeContratTravail,
				EOTypeContratTravail.LC_TYPE_CONTRAT_TRAV.ascs());
	}

	public CorpsHelper(EOEditingContext edc, String anneeUniversitaire, boolean uniquementPourStatutaire, boolean uniquementPourVacataire) {
		// initialisation de la liste des corps enseignants non fermés depuis le 02/01 de l'année universitaire en cours
		// et respectant les "uniquement"

		listeCorps = new NSMutableArray<EOCorps>();	
		setListeTypePopulation(new NSMutableArray<EOTypePopulation>());
		
		java.text.SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		NSTimestamp dateReference = null;
		
		try {
			dateReference = new NSTimestamp(simpleDateFormat.parse(new StringBuffer("02/01/").append(anneeUniversitaire).toString()));
		} catch (ParseException e1) {
			// exception non traitée
		}
		NSArray<EOTypePopulation> typesPopulationEnseignant = EOTypePopulation.fetchAll(edc, EOTypePopulation.TEM_ENSEIGNANT.eq(CocktailConstantes.VRAI), EOTypePopulation.LL_TYPE_POPULATION.ascs());
		listeTypePopulation.addAll(typesPopulationEnseignant);
		
		EOQualifier qualCorps = null;
		
		if (uniquementPourStatutaire) {
			qualCorps = ERXQ.and(EOCorps.TO_TYPE_POPULATION.in(typesPopulationEnseignant),
					ERXQ.or(EOCorps.D_OUVERTURE_CORPS.isNull()
							, EOCorps.D_OUVERTURE_CORPS.before(dateReference)),
							ERXQ.or(EOCorps.D_FERMETURE_CORPS.isNull()
									, EOCorps.D_FERMETURE_CORPS.after(dateReference)),
									EOCorps.C_CORPS.ne("758"),
									EOCorps.C_CORPS.ne("759"));
									
		} else {
			qualCorps = ERXQ.or(EOCorps.C_CORPS.eq("758"),
					EOCorps.C_CORPS.eq("759"));
		}
		
		listeCorps.addAll(EOCorps.rechercherCorpsAvecCriteres(edc, qualCorps));				
			
		NSComparator nsComparatorLLCorps = new NSComparator() {		
			public int compare(Object corps0, Object corps1) {
				return ((EOCorps) corps0).llCorps().toUpperCase().compareTo(((EOCorps) corps1).llCorps().toUpperCase());
			}
		};
	
		try {
			listeCorps = listeCorps.sortedArrayUsingComparator(nsComparatorLLCorps);
		} catch (ComparisonException e) {
			// exception non traitée
		}
		
		//On ajoute que les types de contrat ouverts, visibles et avec le témoin Enseignement
		EOQualifier qualTypeContratTravail = (EOTypeContratTravail.D_FIN_VAL.isNull()
				.or(EOTypeContratTravail.D_FIN_VAL.after(dateReference)))
				.and(EOTypeContratTravail.TEM_VISIBLE.eq(CocktailConstantes.VRAI))
				.and(EOTypeContratTravail.TEM_ENSEIGNEMENT.eq(CocktailConstantes.VRAI));
		
		if (!MangueParametres.isGestionHu(edc)) {			
			qualTypeContratTravail = ERXQ.and(qualTypeContratTravail, EOTypeContratTravail.TEM_AH_CU_AO.eq(CocktailConstantes.FAUX));
		}
	
		if (uniquementPourStatutaire) {
			qualTypeContratTravail = ERXQ.and(qualTypeContratTravail, EOTypeContratTravail.TEM_VACATAIRE.eq(CocktailConstantes.FAUX));						
		} else  {
			qualTypeContratTravail = ERXQ.and(qualTypeContratTravail, EOTypeContratTravail.TEM_VACATAIRE.eq(CocktailConstantes.VRAI));
		}
		
		listeTypeContratsTravail = new NSMutableArray<EOTypeContratTravail>();
		listeTypeContratsTravail = EOTypeContratTravail.fetchAll(edc, 
				qualTypeContratTravail,
				EOTypeContratTravail.LC_TYPE_CONTRAT_TRAV.ascs());
	}
	
	/**
	 * @return the listeCorps
	 */
	public NSArray<EOCorps> getListeCorps() {
		return listeCorps;
	}

	/**
	 * @param listeCorps the listeCorps to set
	 */
	public void setListeCorps(NSArray<EOCorps> listeCorps) {
		this.listeCorps = listeCorps;
	}

	/**
	 * @return the corps
	 */
	public EOCorps getCorps() {
		return corps;
	}

	/**
	 * @param corps the corps to set
	 */
	public void setCorps(EOCorps corps) {
		this.corps = corps;
	}

	/**
	 * @return the selection
	 */
	public EOCorps getSelectionCorps() {
		return selectionCorps;
	}
	
	/**
	 * @param selection the selection to set
	 */
	public void setSelectionCorps(EOCorps selection) {
		this.selectionCorps = selection;
	}

	public NSArray<EOTypeContratTravail> getListeContratsTravail() {
		return listeTypeContratsTravail;
	}

	public void setListeContratsTravail(NSArray<EOTypeContratTravail> listeContratsTravail) {
		this.listeTypeContratsTravail = listeContratsTravail;
	}

	public NSArray<EOTypePopulation> getListeTypePopulation() {
		return listeTypePopulation;
	}

	public void setListeTypePopulation(NSArray<EOTypePopulation> listeTypePopulation) {
		this.listeTypePopulation = listeTypePopulation;
	}

	public EOTypePopulation getTypePopulation() {
		return typePopulation;
	}

	public void setTypePopulation(EOTypePopulation typePopulation) {
		this.typePopulation = typePopulation;
	}

	public EOTypeContratTravail getContratTravail() {
		return contratTravail;
	}

	public void setContratTravail(EOTypeContratTravail contratTravail) {
		this.contratTravail = contratTravail;
	}

	public EOTypeContratTravail getSelectionContratTravail() {
		return selectionContratTravail;
	}

	public void setSelectionContratTravail(EOTypeContratTravail selectionContratTravail) {
		this.selectionContratTravail = selectionContratTravail;
	}

	public EOTypePopulation getSelectionPopulation() {
		return selectionPopulation;
	}

	public void setSelectionPopulation(EOTypePopulation selectionPopulation) {
		this.selectionPopulation = selectionPopulation;
	}

	public String getSelection() {
		if (getContratTravail() != null) {
			return getContratTravail().llTypeContratTrav();
		}
		if (getCorps() != null) {
			return getCorps().llCorps();
		}
		if (getTypePopulation() != null) {
			return getTypePopulation().llTypePopulation();
		}
		return "*";
	}
	public String displayContratTravail() {
		return getContratTravail().lcTypeContratTrav() + " (" + getContratTravail().cTypeContratTrav() + ")";			
	}
	
	public String displayCorps() {
		return getCorps().llCorps() + " (" + getCorps().cCorps() + ")";			
	}
	
	public String displayTypePopulation() {
		return getTypePopulation().lcTypePopulation() + " (" + getTypePopulation().cTypePopulation() + ")";			
	}
}
