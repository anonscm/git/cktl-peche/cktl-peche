package org.cocktail.peche.outils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.TreeMap;

public class GenereMessages {

	private static final String sources = "Sources";

	private static final String packageBase = "org.cocktail.peche";

	private static final String localizable = "Resources/French.lproj/Localizable.strings";

	public static void main(String[] args) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader(localizable));

		String line;

		File dir = new File(sources + "/" + packageBase.replace(".", "/"));
		File file = new File(dir, "Messages.java");
		PrintWriter writer = new PrintWriter(file);

		Map<String, String> keys = new TreeMap<String, String>();

		while ((line = reader.readLine()) != null) {
			line = line.trim();
			if (line.startsWith("//") || line.startsWith("/*"))
				continue;
			String s = line;
			if (s.endsWith("}"))
				s = s.substring(0, s.length() - 1);
			if (s.startsWith("{"))
				s = s.substring(1);
			if (s.length() > 0) {
				String[] parts = s.split("=");
				String key = parts[0].trim();
				String value = parts[1].trim().substring(1);
				value = value.substring(0, value.length() - 2);
				String field = key.replace(".", "_");
				String newField = "";
				for (int i = 0; i < field.length(); i++) {
					String c = field.substring(i, i + 1);
					if (Character.isUpperCase(field.charAt(i))) {
						newField += "_";
					}
					newField += Character.toUpperCase(field.charAt(i));
				}
				if (newField.startsWith("_")) {
					newField = newField.substring(1);
				}
				newField = newField.replaceAll("__", "_");
				keys.put("\tpublic static final String " + newField + " = \"" + key + "\";", "\t// " + value);
			}
			if (line.endsWith("}"))
				break;
		}

		writer.println("package " + packageBase + ";");
		writer.println();
		writer.println();
		writer.println("/**");
		writer.println(" * Cette classe est générée à partir du fichier Localizable.string.");
		writer.println(" * Si le fichier source est modifié, lancer l'outils GenereMessage.java.");
		writer.println(" */");
		writer.println("public final class Messages {");
		writer.println();
		writer.println("\tprivate Messages() {");
		writer.println("\t}");
		writer.println();
		for (String key : keys.keySet()) {
			writer.println(keys.get(key));
			writer.println(key);
			writer.println();
		}
		writer.println("}");

		reader.close();
		writer.flush();
		writer.close();
	}
}
