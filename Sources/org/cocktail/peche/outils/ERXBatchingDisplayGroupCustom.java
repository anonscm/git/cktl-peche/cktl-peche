package org.cocktail.peche.outils;

import com.webobjects.foundation.NSArray;

import er.extensions.batching.ERXBatchingDisplayGroup;

public class ERXBatchingDisplayGroupCustom<T> extends ERXBatchingDisplayGroup<T> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7511717005179776625L;

	public void setDisplayedObjects(NSArray<T> displayedObjects) {
		_displayedObjects = displayedObjects;
	}
	
	
}
