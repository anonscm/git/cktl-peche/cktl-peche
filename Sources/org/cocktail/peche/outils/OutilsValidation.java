package org.cocktail.peche.outils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.peche.Session;
import org.cocktail.peche.TypeMessage;

import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Classe de validation des méthodes utilitaires
 *
 * @author Chama LAATIK
 */
public class OutilsValidation {
	
	/** Logger. */
	private static Logger LOG = Logger.getLogger(OutilsValidation.class);

	static final String TOUTES_LES_DATES_DOIVENT_ETRE_RENSEIGNEES = "Toutes les dates doivent être renseignées";
	static final String LA_DATE_DE_FIN_EST_LA_MEME_QUE_LA_DATE_DE_DEBUT = "La date de fin est la même que la date de début";
	static final String LA_DATE_DE_DEBUT_EST_APRES_LA_DATE_DE_FIN = "La date de début est après la date de fin..!";

	/**
	 * Vérifie la cohérence des dates
	 * @param dateDebut : date du début
	 * @param dateFin : date de fin
	 * @param session : la session
	 */
	public static void verifierOrdreDates(NSTimestamp dateDebut, NSTimestamp dateFin, Session session) {
		if ((dateDebut != null) && (dateFin != null)) {
			if (DateCtrl.isAfter(dateDebut, dateFin)) {
				throw new ValidationException(LA_DATE_DE_DEBUT_EST_APRES_LA_DATE_DE_FIN);
			}
			
			if (DateCtrl.isSameDay(dateDebut, dateFin)) {
				session.addSimpleMessage(TypeMessage.ATTENTION, LA_DATE_DE_FIN_EST_LA_MEME_QUE_LA_DATE_DE_DEBUT);
			}
		} else {
			throw new ValidationException(TOUTES_LES_DATES_DOIVENT_ETRE_RENSEIGNEES);
		}
	}
	
	/**
	 * Formatteur à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public static NSNumberFormatter getApp2DecimalesFormatter() {
		NSNumberFormatter app2DecimalesFormatter = new NSNumberFormatter();
		app2DecimalesFormatter.setDecimalSeparator(","); 
		app2DecimalesFormatter.setThousandSeparator(" ");

		app2DecimalesFormatter.setHasThousandSeparators(true);
		//permet de fixer 2 décimales après la virgule et de ne pas arrondir les nombres négatifs
		app2DecimalesFormatter.setPattern("#,##0.00;#,##0.00");
		
		return app2DecimalesFormatter;
		
	}

	/**
	 * Formatteur à deux decimales : utiliser pour les données numériques.
	 * @return nombre formatté
	 */
	public static NSNumberFormatter getApp3DecimalesFormatter() {
		NSNumberFormatter app3DecimalesFormatter = new NSNumberFormatter();
		app3DecimalesFormatter.setDecimalSeparator(","); 
		app3DecimalesFormatter.setThousandSeparator(" ");

		app3DecimalesFormatter.setHasThousandSeparators(true);
		//permet de fixer 3 décimales après la virgule et de ne pas arrondir les nombres négatifs
		app3DecimalesFormatter.setPattern("#,##0.00#;#,##0.00#");
		
		return app3DecimalesFormatter;
		
	}
	
	/**
	 * NumberFormat à trois decimales : utiliser pour les données numériques.
	 * @return le format 
	 */
	public static String getApp3DecimalesNumberFormat() {
		return "#,##0.00#;#,##0.00#";		
	}
	
	/**
	 * Formatteur à deux decimales : utiliser pour les données numériques qui pourraient être négatives.
	 * @return nombre formatté
	 */
	public static NSNumberFormatter getApp2DecimalesFormatterNegatif() {
		NSNumberFormatter app2DecimalesFormatter = new NSNumberFormatter();
		app2DecimalesFormatter.setDecimalSeparator(","); 
		app2DecimalesFormatter.setThousandSeparator(" ");

		app2DecimalesFormatter.setHasThousandSeparators(true);
		app2DecimalesFormatter.setPattern("#,##0.00;0,00;-#,##0.00");
		
		return app2DecimalesFormatter;
	}
	
	/**
	 * Formatteur à trois decimales : utiliser pour les données numériques qui pourraient être négatives.
	 * @return nombre formatté
	 */
	public static NSNumberFormatter getApp3DecimalesFormatterNegatif() {
		NSNumberFormatter app3DecimalesFormatter = new NSNumberFormatter();
		app3DecimalesFormatter.setDecimalSeparator(","); 
		app3DecimalesFormatter.setThousandSeparator(" ");

		app3DecimalesFormatter.setHasThousandSeparators(true);
		app3DecimalesFormatter.setPattern("#,##0.00#;0,000;-#,##0.00#");
		
		return app3DecimalesFormatter;
	}
	/**
	 * Formateur nombre a 5 chiffres
	 * @return the numeroACinqChiffres
	 */
	public static NSNumberFormatter getNumeroACinqChiffresFormatter() {
		NSNumberFormatter formatter = new NSNumberFormatter();
		formatter.setPattern("00000");		
		return formatter;
	}

	/**
	 * Ne retourne jamais <code>null</code>.
	 * @param uneValeur une valeur
	 * @return la valeur ou "" si une valeur est null
	 */
	public static String retournerNonNull(String uneValeur) {
		if (uneValeur == null) {
			return "";
		}
		
		return uneValeur;
	}
	
	/**
	 * Formatte un entier avec le formater passé en paramètre.
	 * <p>
	 * Ne retourne jamais <code>null</code> et jamais un {@link NullPointerException}.
	 * 
	 * @param unEntier un entier
	 * @param unFormater un formater
	 * @return l'entier formaté ou "" si unEntier était <code>null</code>
	 */
	public static String formatter(Integer unEntier, NumberFormat unFormater) {
		if (unEntier == null) {
			return "";
		}		
		return unFormater.format(unEntier);
	}
	
	/**
	 * Retourne la valeur d'un <code>BigDecimal</code> (0 si <code>null</code>).
	 * 
	 * @param unBigDecimal un BigDecimal
	 * @return lui même ou 0
	 */
	public static BigDecimal retournerBigDecimalNonNull(BigDecimal unBigDecimal) {
		if (unBigDecimal == null) {
			return BigDecimal.ZERO;
		}
		
		return unBigDecimal;
	}
	
	/**
	 * Retourne la date de début de l'année universitaire
	 * 	=> Si sur année civile : 01/01/annee
	 * 		Sinon = 01/09/annee
	 * @param annee : l'année
	 * @param isAnneeUniversitaireEnAnneeCivil : année civile ou non
	 * @return la date de début de l'année universitaire
	 */
	public static GregorianCalendar getDebutAnneeUniversitaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		if (annee == null) {
			GregorianCalendar c = (GregorianCalendar) Calendar.getInstance();
			annee = c.get(GregorianCalendar.YEAR);
		}
		
		int mois = 8;
		
		if (isAnneeUniversitaireEnAnneeCivil) {
			mois = 0;
		}
		
		return new GregorianCalendar(annee, mois, 1, 0, 0, 0);
	}
	
	public static NSTimestamp getNSTDebutAnneeUniversitaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		//
		if (isAnneeUniversitaireEnAnneeCivil) {
			return DateCtrl.stringToDate("01/01/" + annee.intValue());
		}
		
		return DateCtrl.stringToDate("01/09/" + annee.intValue());
	}
	
	
	public static NSTimestamp getNSTFinAnneeUniversitaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		
		if (isAnneeUniversitaireEnAnneeCivil) {
			return DateCtrl.stringToDate("31/12/" + annee.intValue());
		}
		
		return DateCtrl.stringToDate("31/08/" + (annee.intValue() + 1));
	}

	/**
	 * Retourne la date de fin de l'année universitaire
	 * 	=> Si sur année civile : 31/12/annee
	 * 		Sinon = 31/08/(annee+1)
	 * @param annee : l'année
	 * @param isAnneeUniversitaireEnAnneeCivil : année civile ou non
	 * @return la date de fin de l'année universitaire
	 */
	public static GregorianCalendar getFinAnneeUniversitaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil) {
		
		if (annee == null) {
			GregorianCalendar c = (GregorianCalendar) Calendar.getInstance();
			annee = c.get(GregorianCalendar.YEAR);
		}
		
		int mois = 12;
		
		if (!isAnneeUniversitaireEnAnneeCivil) {
			mois = 7;
			annee = annee + 1;
		}
		
		return new GregorianCalendar(annee, mois, 31, 24, 0, 0);
	}
	
	/**
	 * Retourne le prorata du nombre de jours entre dateDebut et dateFin par rapport à l'année universitaire
	 * @param annee : l'année Universitaire
	 * @param isAnneeUniversitaireEnAnneeCivil : année civile ou non
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @return le prorata des a date de fin de l'année universitaire
	 */
	public static double prorataDatesAnneeUniversitaire(Integer annee, boolean isAnneeUniversitaireEnAnneeCivil, NSTimestamp dateDebut, NSTimestamp dateFin) {

		int nbJours;
		
		GregorianCalendar debutAnneeUniv = getDebutAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil);
		GregorianCalendar finAnneeUniv = getFinAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil);

		int nbJoursUniv = DateCtrl.nbJoursEntreDates(OutilsValidation.getDebutAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil), 
				OutilsValidation.getFinAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil));

		GregorianCalendar debutTemp = new GregorianCalendar();
		debutTemp.setTimeInMillis(dateDebut.getTime());

		GregorianCalendar debut = new GregorianCalendar(debutTemp.get(Calendar.YEAR),
				debutTemp.get(Calendar.MONTH), debutTemp.get(Calendar.DAY_OF_MONTH), 0, 0, 0);

		if (dateFin == null) {
			if (debut.compareTo(debutAnneeUniv) <= 0) {
				// début avant début année univ et pas de fin donc 100%
				return 1;
			} else {
				// début après début année univ et pas de fin donc prorata début avec fin année univ
				nbJours = DateCtrl.nbJoursEntreDates(debut, OutilsValidation.getFinAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil));
				return (double) nbJours / nbJoursUniv;
			}    			 
		} else {
			GregorianCalendar finTemp = new GregorianCalendar();
			finTemp.setTimeInMillis(dateFin.getTime());

			GregorianCalendar fin = new GregorianCalendar(finTemp.get(Calendar.YEAR),
					finTemp.get(Calendar.MONTH), finTemp.get(Calendar.DAY_OF_MONTH), 24, 0, 0);

			if (debut.compareTo(debutAnneeUniv) <= 0) {
				// début avant début année univ
				if (fin.compareTo(finAnneeUniv) > 0) {
					// fin après fin année univ donc 100%
					return 1;
				} else {
					// fin avant fin année univ donc prorata début univ avec fin
					nbJours = DateCtrl.nbJoursEntreDates(OutilsValidation.getDebutAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil), fin);
					return (double) nbJours / nbJoursUniv;    				 
				}
			} else {
				// début après début année univ
				if (fin.compareTo(finAnneeUniv) > 0) {
					// fin après fin année univ donc prorata début avec fin univ
					nbJours = DateCtrl.nbJoursEntreDates(debut, OutilsValidation.getFinAnneeUniversitaire(annee, isAnneeUniversitaireEnAnneeCivil));
					return (double) nbJours / nbJoursUniv;    				 
				} else {
					// fin avant fin année univ donc prorata début avec fin
					nbJours = DateCtrl.nbJoursEntreDates(debut, fin);
					return (double) nbJours / nbJoursUniv;    				 
				}
			}
		}    	 
	}

	/**
	 * Retourne le prorata du nombre de jours entre dateDebut et dateFin si dateFin anticipe
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @param dateFinAnticipe
	 * @return le prorata des a date de fin de l'année universitaire
	 */
	public static double prorataDatesSiFinAnticipe(NSTimestamp dateDebut, NSTimestamp dateFin, NSTimestamp dateFinAnticipe) {

		GregorianCalendar debutContratTemp = new GregorianCalendar();
		debutContratTemp.setTimeInMillis(dateDebut.getTime());
		
		GregorianCalendar debut = new GregorianCalendar(debutContratTemp.get(Calendar.YEAR),
				debutContratTemp.get(Calendar.MONTH), debutContratTemp.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
		
		GregorianCalendar finContratTemp = new GregorianCalendar();
		finContratTemp.setTimeInMillis(dateFin.getTime());
		
		GregorianCalendar fin = new GregorianCalendar(finContratTemp.get(Calendar.YEAR),
				finContratTemp.get(Calendar.MONTH), finContratTemp.get(Calendar.DAY_OF_MONTH), 24, 0, 0);
		
		int nbJoursContrat = DateCtrl.nbJoursEntreDates(debut, fin);
		
		GregorianCalendar finContratAnticipeTemp = new GregorianCalendar();
		finContratAnticipeTemp.setTimeInMillis(dateFinAnticipe.getTime());
		
		GregorianCalendar finAnticipe = new GregorianCalendar(finContratAnticipeTemp.get(Calendar.YEAR),
				finContratAnticipeTemp.get(Calendar.MONTH), finContratAnticipeTemp.get(Calendar.DAY_OF_MONTH), 24, 0, 0);
		
		int nbJoursAnticipeFinContrat = DateCtrl.nbJoursEntreDates(debut, finAnticipe);
		return (double) nbJoursAnticipeFinContrat / nbJoursContrat;    				 
		    	 
	}

	/**
	 * @param date1 : date 1 {@link GregorianCalendar}
	 * @param date2 : date 2 {@link GregorianCalendar}
	 * @return la date supérieure entre les 2 dates à comparer
	 */
	public static GregorianCalendar getDateSuperieur(GregorianCalendar date1, GregorianCalendar date2) {
		if (DateCtrl.isAfter(date1, date2)) {
			return date1;
		} else {
			return date2;
		}
	}
	
	/**
	 * @param date1 : date 1 {@link GregorianCalendar}
	 * @param date2 : date 2 {@link GregorianCalendar}
	 * @return la date inférieure entre les 2 dates à comparer
	 */
	public static GregorianCalendar getDateInferieur(GregorianCalendar date1, GregorianCalendar date2) {
		if (DateCtrl.isBefore(date1, date2)) {
			return date1;
		} else {
			return date2;
		}
	}
	
	/**
	 * Renvoie les log pour une date 
	 * @param uneDate : la date
	 * @param titre : titre du log
	 */
	public static void getLogPourGregorianDate(GregorianCalendar uneDate, String titre) {
		LOG.debug(titre + " est : " + uneDate.get(GregorianCalendar.DAY_OF_MONTH) + "/"
				+ (uneDate.get(GregorianCalendar.MONTH) + 1) + "/" 
				+ uneDate.get(GregorianCalendar.YEAR) + " "
				+ uneDate.get(GregorianCalendar.HOUR_OF_DAY) + ":"
				+ uneDate.get(GregorianCalendar.MINUTE) + ":"
				+ uneDate.get(GregorianCalendar.SECOND));
	}
}
