package org.cocktail.peche.outils;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

/**
 * Classe utilitaire pour encapsuler les accès aux loggers
 * 
 * @author yannick
 * 
 */
public final class PecheLogger {

	private static final Map<Class<?>, PecheLogger> LOGGERS = new HashMap<Class<?>, PecheLogger>();

	/**
	 * Retourne une instance de PecheLog. Si une instance est déjà associée à la
	 * classe passée en paramètre, elle est retournée. Dans le cas contraire,
	 * une nouvelle instance est crée, et mise en cache.
	 * 
	 * @param clazz
	 *            la classe à associer avec le logger.
	 * @return une instance de PecheLog associée à la classe passée en
	 *         paramètre.
	 */
	public static PecheLogger getLogger(Class<?> clazz) {
		PecheLogger logger = LOGGERS.get(clazz);
		if (logger == null) {
			logger = new PecheLogger(clazz);
			LOGGERS.put(clazz, logger);
		}

		return logger;
	}

	private Logger logger;

	private PecheLogger(Class<?> clazz) {
		this.logger = Logger.getLogger(clazz);
	}

	/**
	 * Affiche les informations passées en paramètres si le logger est configuré
	 * en mode DEBUG.
	 * 
	 * @param format
	 *            La chaine de formattage.
	 * @param args
	 *            Les arguments référencés par la chaine de formattage.
	 * @see String#format(String, Object...)
	 */
	public void debug(String format, Object... args) {
		if (this.logger.isDebugEnabled()) {
			this.logger.debug(String.format("[%s] %s",
					DateCtrl.currentDateTimeString(),
					String.format(format, args)));
		}
	}

	/**
	 * Affiche les information passées en paramètres.
	 * 
	 * @param format
	 *            La chaine de formattage.
	 * @param args
	 *            Les arguments référencés par la chaine de formattage.
	 */
	public void error(String format, Object... args) {
		this.logger.error(String.format(format, args));
	}
}
