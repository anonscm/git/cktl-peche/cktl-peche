package org.cocktail.peche.outils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;
import org.cocktail.peche.Application;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe utilitaire pour gestion des logos d'etablisements dans les éditions
 * 
 * @author yannick
 * 
 */
public class LogoEdition {
	/** Logger. */
	private static Logger log = Logger.getLogger(LogoEdition.class);

	public static String cheminLogoEtablissement(EOEditingContext editingContext) {

		String cheminDir = SystemCtrl.tempDir();

		if (cheminDir != null)	{
			if (!(cheminDir.substring(cheminDir.length() - 1).equals(File.separator))) {
				cheminDir += File.separator;
			}

			String filePath = cheminDir + "Logo.jpg";
			if (!pathExiste(filePath)) {
				if (EOGrhumParametres.parametrePourCle(editingContext, "URL_LOGO") != null) {
					String urlLogo = EOGrhumParametres.parametrePourCle(editingContext, "URL_LOGO");
					try {
						saveImage(urlLogo, filePath);
					} catch (Exception e) {
						log.error("erreur logo:"+e.getMessage());
						return null;
					}
				}
			}

			return filePath;
		}
		return null;
	}


	public static void saveImage(String imageUrl, String destinationFile) throws Exception {
		URL url = null;
		InputStream is = null;

		if  (imageUrl.startsWith("http")) {
			url = new URL(imageUrl);
			log.debug("url logo:" + url.getPath());
		} else {
			// url locale relative
			log.debug("url directConnectURL pour logo:" + Application.erxApplication().getDirectConnectURL());			
			URL aURL = new URL(Application.erxApplication().getDirectConnectURL());
			log.debug("url logo:" + aURL.getProtocol() + "://" + aURL.getHost() + imageUrl);			
			url =  new URL(aURL.getProtocol() + "://" + aURL.getHost() + imageUrl);
		}


		/*
		URL aURL = new URL(Application.erxApplication().getDirectConnectURL());

		 System.out.println("protocol = " + aURL.getProtocol());
	        System.out.println("authority = " + aURL.getAuthority());
	        System.out.println("host = " + aURL.getHost());
	        System.out.println("port = " + aURL.getPort());
	        System.out.println("path = " + aURL.getPath());
	        System.out.println("query = " + aURL.getQuery());
	        System.out.println("filename = " + aURL.getFile());
	        System.out.println("ref = " + aURL.getRef());

		System.out.println(Application.erxApplication().getCGIAdaptorURL());
		System.out.println(Application.erxApplication().getWebserverConnectURL());
		System.out.println(Application.erxApplication().getDirectConnectURL());
		System.out.println(Application.erxApplication().getHostAddress());
		System.out.println(Application.erxApplication().getPort());
		System.out.println(Application.erxApplication().getHost());
		System.out.println(Application.erxApplication().getDefaultAdaptor());
		System.out.println(Application.erxApplication().getServletConnectURL());
		System.out.println(Application.erxApplication().getWebserverConnectURL());
		 */

		is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	}

	private static boolean pathExiste(String unPath) {
		if (unPath == null || unPath.length() == 0) {
			return false;
		}
		File file = new File(unPath);
		return file.exists();
	}

}
