package org.cocktail.peche;

import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.IServiceConstructionArbitrageAP;
import org.cocktail.peche.components.enseignants.ficheVoeux.services.ServiceConstructionArbitrageAPImpl;
import org.cocktail.peche.components.enseignants.services.IServiceOngletEnseignant;
import org.cocktail.peche.components.enseignants.services.ServiceOngletEnseignantImpl;
import org.cocktail.peche.services.IPeriodeService;
import org.cocktail.peche.services.PeriodeServiceImpl;

import com.google.inject.AbstractModule;
import com.woinject.WOScopes;

/**
 * Cette classe permet de lier les classes injectées
 */
public class PecheModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(IPecheParametres.class).to(PecheParametres.class).in(WOScopes.SESSION);
        bind(IPeriodeService.class).to(PeriodeServiceImpl.class).in(WOScopes.SESSION);
        bind(IServiceOngletEnseignant.class).to(ServiceOngletEnseignantImpl.class).in(WOScopes.SESSION);
        bind(IServiceConstructionArbitrageAP.class).to(ServiceConstructionArbitrageAPImpl.class).in(WOScopes.SESSION);
    }

}
