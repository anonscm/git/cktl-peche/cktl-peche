package org.cocktail.peche;


/**
 * Cette classe est générée à partir du fichier Localizable.string.
 * Si le fichier source est modifié, lancer l'outils GenereMessage.java.
 */
public final class Messages {

	private Messages() {
	}

	// Ajouter
	public static final String APPLICATION_BOUTON_AJOUTER = "application.boutonAjouter";

	// Annuler
	public static final String APPLICATION_BOUTON_ANNULER = "application.boutonAnnuler";

	// Détail
	public static final String APPLICATION_BOUTON_DETAIL_U_E = "application.boutonDetailUE";

	// Enregistrer
	public static final String APPLICATION_BOUTON_ENREGISTRER = "application.boutonEnregistrer";

	// Fiche de service
	public static final String APPLICATION_BOUTON_FICHE_SERVICE = "application.boutonFicheService";
	
	// Fiche de voeux
	public static final String APPLICATION_BOUTON_FICHE_VOEUX = "application.boutonFicheVoeux";

	// Modifier
	public static final String APPLICATION_BOUTON_MODIFIER = "application.boutonModifier";

	// Supprimer
	public static final String APPLICATION_BOUTON_SUPPRIMER = "application.boutonSupprimer";

	// Valider
	public static final String APPLICATION_BOUTON_VALIDER = "application.boutonValider";

	// Valider la fiche
	public static final String APPLICATION_BOUTON_VALIDER_FICHE = "application.boutonValiderFiche";

	// Paramètres
	public static final String APPLICATION_ONGLET_PARAMETRES = "application.ongletParametres";

	// HETD
	public static final String APPLICATION_ONGLET_PARAMETRES_SUB_MENU_H_E_T_D = "application.ongletParametres.subMenuHETD";

	// REH
	public static final String APPLICATION_ONGLET_PARAMETRES_SUB_MENU_R_E_H = "application.ongletParametres.subMenuREH";

	// Service
	public static final String APPLICATION_ONGLET_SERVICE = "application.ongletService";

	// Initialisation
	public static final String APPLICATION_ONGLET_SERVICE_SUB_MENU_I_N_I = "application.ongletService.subMenuINI";

	// Prévisionnel
	public static final String APPLICATION_ONGLET_SERVICE_SUB_MENU_P_R_E = "application.ongletService.subMenuPRE";

	// Retour au répartiteur
	public static final String APPLICATION_SERVICE_BOUTON_RETOUR_REPARTITEUR = "application.service.boutonRetourRepartiteur";

	// PECHE
	public static final String APPLICATION_TITRE = "application.titre";

	// Programme d'Etablissement des Charges d'Enseignements
	public static final String APPLICATION_TITRE_LONG = "application.titreLong";

	// Création d'un enseignant Générique statutaire
	public static final String ENSEIGNANT_GENERIQUE_FORM_CREATION_STATUTAIRE = "EnseignantGenerique.Form.creation.statutaire";
	// Création d'un enseignant Générique vacataire
	public static final String ENSEIGNANT_GENERIQUE_FORM_CREATION_VACATAIRE = "EnseignantGenerique.Form.creation.vacataire";
	
	// Modification d'un enseignant générique statutaire
	public static final String ENSEIGNANT_GENERIQUE_FORM_MODIFICATION_STATUTAIRE = "EnseignantGenerique.Form.modification.statutaire";
	// Modification d'un enseignant générique vacataire
	public static final String ENSEIGNANT_GENERIQUE_FORM_MODIFICATION_VACATAIRE = "EnseignantGenerique.Form.modification.vacataire";
	
	// Êtes-vous certain de vouloir supprimer cet enseignant générique ?
	public static final String ENSEIGNANT_GENERIQUE_SUPPRIMER = "EnseignantGenerique.supprimer";
	
	// Il existe des répartitions en cours pour cet enseignant générique. Êtes-vous certain de vouloir le supprimer ?
	public static final String ENSEIGNANT_GENERIQUE_REPARTITION_SUPPRIMER = "EnseignantGenerique.supprimerRepartitionExistante";

	// Enseignants statutaires
	public static final String ENSEIGNANT_STATUTAIRE_TITRE = "EnseignantStatutaire.titre";

	// Enseignement
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_A_P = "FicheServiceStatutaire.TableViews.ColonneAP";

	// Etat
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_ETAT = "FicheServiceStatutaire.TableViews.ColonneEtat";

	// Heures présentielles prévues
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_H_E_T_D = "FicheServiceStatutaire.TableViews.ColonneHETD";
	
	// Heures présentielles réalisées
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_H_REALISEES = "FicheServiceStatutaire.TableViews.ColonneHeuresRealisees";
		
	// Heures présentielles
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_H_A_PAYER = "FicheServiceStatutaire.TableViews.ColonneHeuresAPayer";
		
		// Heures présentielles
		public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_H_PAYEES_HETD = "FicheServiceStatutaire.TableViews.ColonneHeuresPayeesHETD";

	// Référentiel des Equivalences Horaires
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_R_E_H = "FicheServiceStatutaire.TableViews.ColonneREH";

	// Sem.
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_SEMESTRE = "FicheServiceStatutaire.TableViews.ColonneSemestre";

	// Type
	public static final String FICHE_SERVICE_STATUTAIRE_TABLE_VIEWS_COLONNE_TYPE = "FicheServiceStatutaire.TableViews.ColonneType";

	// Fiche individuelle de service
	public static final String FICHE_SERVICE_TITRE = "FicheService.titre";

	// Erreur - mode d'édition non défini
	public static final String FORMS_MODE_EDITION_UNKNOWN = "forms.modeEdition.unknown";

	// Détail de l'enseignement
	public static final String OFFRE_FORMATION_DETAIL_TITRE = "OffreFormationDetail.titre";

	// Attribués
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_ATTRIBUES = "OffreFormation.TableViews.ColonneAttribues";

	// Charge
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_CHARGE = "OffreFormation.TableViews.ColonneCharge";

	// Code
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_CODE = "OffreFormation.TableViews.ColonneCode";

	// Composante
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_COMPOSANTE = "OffreFormation.TableViews.ColonneComposante";

	// Delta
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_DELTA = "OffreFormation.TableViews.ColonneDelta";

	// Dept
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_DEPARTEMENT = "OffreFormation.TableViews.ColonneDepartement";

	// Libellé
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_LIBELLE = "OffreFormation.TableViews.ColonneLibelle";

	// S. Calend
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_SEMESTRE_CALEND = "OffreFormation.TableViews.ColonneSemestreCalend";

	// S. Etudiant
	public static final String OFFRE_FORMATION_TABLE_VIEWS_COLONNE_SEMESTRE_ETUD = "OffreFormation.TableViews.ColonneSemestreEtud";

	// Offre de formation
	public static final String OFFRE_FORMATION_TITRE = "OffreFormation.titre";

	// Êtes-vous sur de vouloir supprimer ce paramètre ?
	public static final String PARAMS_POP_HETD_GESTION_ALERTE_CONFIRM_SUPPRESS = "ParamsPopHetdGestion.alerte.confirmSuppress";

	// Type d'horaire
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_CODE_TYPE_HORAIRE = "ParamsPopHetdGestion.etiquette.codeTypeHoraire";

	// Corps
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_CORPS = "ParamsPopHetdGestion.etiquette.corps";

	// Corps/Type Contrat
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_CORPS_OU_CONTRAT = "ParamsPopHetdGestion.etiquette.corpsOuContrat";

	// Date de début
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_DATE_DEBUT = "ParamsPopHetdGestion.etiquette.dateDebut";

	// Date de fin
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_DATE_FIN = "ParamsPopHetdGestion.etiquette.dateFin";

	// Dénominateur hors service
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_DENOMINATEUR_HORS_SERVICE = "ParamsPopHetdGestion.etiquette.denominateurHorsService";

	// Dénominateur service
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_DENOMINATEUR_SERVICE = "ParamsPopHetdGestion.etiquette.denominateurService";

	// Numérateur hors service
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_NUMERATEUR_HORS_SERVICE = "ParamsPopHetdGestion.etiquette.numerateurHorsService";

	// Numérateur service
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_NUMERATEUR_SERVICE = "ParamsPopHetdGestion.etiquette.numerateurService";

	// Type de contrat
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_TYPE_CONTRAT = "ParamsPopHetdGestion.etiquette.typeContrat";

	// Type de population
	public static final String PARAMS_POP_HETD_GESTION_ETIQUETTE_TYPE_POPULATION = "ParamsPopHetdGestion.etiquette.typePopulation";

	// Liste des paramètres des populations
	public static final String PARAMS_POP_HETD_GESTION_LISTE = "ParamsPopHetdGestion.liste";

	// Ajouter un nouveau type de population
	public static final String PARAM_POP_HETD_FORM_TITRE_AJOUTER = "ParamPopHetdForm.titre.ajouter";

	// Modifier un type de population
	public static final String PARAM_POP_HETD_FORM_TITRE_MODIFIER = "ParamPopHetdForm.titre.modifier";

	// Commentaire
	public static final String REH_FORM_COMMENTAIRE = "RehForm.commentaire";

	// Début
	public static final String REH_FORM_DATE_DEBUT = "RehForm.dateDebut";

	// Fin
	public static final String REH_FORM_DATE_FIN = "RehForm.dateFin";

	// Description
	public static final String REH_FORM_DESCRIPTION = "RehForm.description";

	// Libellé court
	public static final String REH_FORM_LIBELLE_COURT = "RehForm.libelleCourt";

	// Libellé long
	public static final String REH_FORM_LIBELLE_LONG = "RehForm.libelleLong";

	// Ajouter une nouvelle activité REH
	public static final String REH_FORM_TITRE_AJOUTER = "RehForm.titre.ajouter";

	// Modifier une activité REH existante
	public static final String REH_FORM_TITRE_MODIFIER = "RehForm.titre.modifier";

	// Êtes-vous sûr de vouloir supprimer cette activité REH ?
	public static final String REH_GESTION_ALERTE_CONFIRM_SUPPRESS = "RehGestion.alerte.confirmSuppress";

		// Liste des activités du Référentiel des Equivalences Horaires
	public static final String REH_GESTION_LISTE = "RehGestion.liste";

	// Commentaire
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_COMMENTAIRE = "RehGestion.TableViews.ColonneCommentaire";

	// Date du début
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_DATE_DEBUT = "RehGestion.TableViews.ColonneDateDebut";

	// Date de fin
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_DATE_FIN = "RehGestion.TableViews.ColonneDateFin";

	// Description
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_DESCRIPTION = "RehGestion.TableViews.ColonneDescription";

	// Libellé court
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_LIBELLE_COURT = "RehGestion.TableViews.ColonneLibelleCourt";

	// Libellé long
	public static final String REH_GESTION_TABLE_VIEWS_COLONNE_LIBELLE_LONG = "RehGestion.TableViews.ColonneLibelleLong";

	// Êtes-vous sur de vouloir supprimer cette affectation ?
	public static final String SERVICE_ALERTE_CONFIRM_SUPPRESS = "Service.alerte.confirmSuppress";

	// Commentaire
	public static final String SERVICE_FORM_COMMENTAIRE = "ServiceForm.commentaire";

	// Element de la maquette
	public static final String SERVICE_FORM_ELEMENT_MAQUETTE = "ServiceForm.elementMaquette";

	// Intervenant
	public static final String SERVICE_FORM_ENSEIGNANT = "ServiceForm.enseignant";

	// Heures prévues
	public static final String SERVICE_FORM_HEURES_PREVUES = "ServiceForm.heuresPrevues";

	// Heures réalisées
	public static final String SERVICE_FORM_HEURES_REALISEES = "ServiceForm.heuresRealisees";
	
	// Heures à payer
	public static final String SERVICE_FORM_HEURES_A_PAYER = "ServiceForm.heuresAPayer";

	// REH
	public static final String SERVICE_FORM_REH = "ServiceForm.reh";

	// Ajouter une nouvelle affectation
	public static final String SERVICE_FORM_TITRE_AJOUTER = "ServiceForm.titre.ajouter";

	// Modifier une affectation existante
	public static final String SERVICE_FORM_TITRE_MODIFIER = "ServiceForm.titre.modifier";

	// Ajouter une nouvelle réalisation
	public static final String SERVICEREALISE_FORM_TITRE_AJOUTER = "ServiceRealiseForm.titre.ajouter";

	// Modifier une réalisation existante
	public static final String SERVICEREALISE_FORM_TITRE_MODIFIER = "ServiceRealiseForm.titre.modifier";

	// Initialisation du service des enseignants
	public static final String SERVICE_INIT_LISTE = "ServiceInit.liste";

	// Liste du service prévisionnel des enseignants
	public static final String SERVICE_PREV_LISTE = "ServicePrev.liste";

	// Intervenant
	public static final String SERVICE_TABLE_VIEWS_COLONNE_ENSEIGNANT = "Service.TableViews.ColonneEnseignant";

	// Heures prévues
	public static final String SERVICE_TABLE_VIEWS_COLONNE_HEURES_PREVUES = "Service.TableViews.ColonneHeuresPrevues";

	// Heures réalisées
	public static final String SERVICE_TABLE_VIEWS_COLONNE_HEURES_REALISEES = "Service.TableViews.ColonneHeuresRealisees";

	// REH
	public static final String SERVICE_TABLE_VIEWS_COLONNE_REH = "Service.TableViews.ColonneReh";

	// REH/Unité d'enseignement
	public static final String SERVICE_TABLE_VIEWS_COLONNE_REH_ELEMENT_MAQUETTE = "Service.TableViews.ColonneRehElementMaquette";

	// Modifier le nombre de groupe de l'AP
	public static final String SURCHARGE_A_P_FORM_TITRE_MODIFIER = "SurchargeAPForm.titre.modifier";

	public static final String TYPES_AP_GESTION_ALERTE_CONFIRM_SUPPRESS = "TypesAPGestion.alerte.confirmSuppress";

	public static final String TYPES_AP_GESTION_LISTE = "TypesAPGestion.liste";

	public static final String TYPES_AP_GESTION_TABLE_VIEWS_COLONNE_CODE = "TypesAPGestion.TableViews.ColonneCode";

	// Libellé court
	public static final String TYPES_AP_GESTION_TABLE_VIEWS_COLONNE_LIBELLE_COURT = "TypesAPGestion.TableViews.ColonneLibelleCourt";

	// Libellé long
	public static final String TYPES_AP_GESTION_TABLE_VIEWS_COLONNE_LIBELLE_LONG = "TypesAPGestion.TableViews.ColonneLibelleLong";

	// Code (sur 2 caractères)
	public static final String TYPE_AP_FORM_ETIQUETTE_CODE_TYPE_AP = "TypeAPForm.etiquette.codeTypeAP";

	// Libellé court
	public static final String TYPE_AP_FORM_ETIQUETTE_LIBELLE_COURT = "TypeAPForm.etiquette.libelleCourt";

	// Libellé long
	public static final String TYPE_AP_FORM_ETIQUETTE_LIBELLE_LONG = "TypeAPForm.etiquette.libelleLong";

	// Ajouter un nouveau type d'horaire
	public static final String TYPE_AP_FORM_TITRE_AJOUTER = "TypeAPForm.titre.ajouter";

	// Modifier un type d'horaire
	public static final String TYPE_AP_FORM_TITRE_MODIFIER = "TypeAPForm.titre.modifier";

	// Ajouter un nouveau taux horaire
	public static final String TAUX_HORAIRE_FORM_TITRE_AJOUTER = "TauxHorForm.titre.ajouter";

	// Modifier un taux d'horaire
	public static final String TAUX_HORAIRE_FORM_TITRE_MODIFIER = "TauxHorForm.titre.modifier";
	
	// Êtes-vous sûr de vouloir supprimer cette activité REH ?
	public static final String TAUX_HORAIRE_CONFIRM_SUPPRESS = "TauxHor.alerte.confirmSuppress";
	
	// Enseignement si connu
	public static final String VOEUX_FORM_ENSEIGNEMENT_EXTERNE = "VoeuxForm.enseignementExterne";

	// Etablissement externe
	public static final String VOEUX_FORM_ETABLISSEMENT_EXTERNE = "VoeuxForm.etablissementExterne";

	// Ajouter un nouveau souhait
	public static final String VOEUX_FORM_TITRE_AJOUTER = "VoeuxForm.titre.ajouter";

	

	// Modifier un souhait existant
	public static final String VOEUX_FORM_TITRE_MODIFIER = "VoeuxForm.titre.modifier";

	// Association COCKTAIL - 37-41, rue Guibal - Pôle Média - BP 20038 - Marseille Innovation - MARSEILLE Cedex 03
	public static final String WRAPPER_COPYRIGHT_ADRESS = "Wrapper.copyrightAdress";

	// Utilisateur non identifié
	public static final String WRAPPER_NOM_UTILISATEUR_VIDE = "Wrapper.nomUtilisateurVide";
	
	// Modifier le nombre d'heures de l'absence de l'intervenant
	public static final String ABSENCE_FORM_TITRE_MODIFIER = "AbsenceForm.titre.modifier";
	
	//Libellé Oui/Non
	public static final String OUI_LIBELLE = "Oui.Libelle";
	public static final String NON_LIBELLE = "Non.Libelle";
	
	// Êtes-vous sur de vouloir supprimer cette UE ?
	public static final String UE_FLOTTANTE_ALERTE_CONFIRM_SUPPRESS = "UEFlottante.alerte.confirmSuppress";
	
	// Ajouter un nouveau paiement
	public static final String PAIEMENT_TITRE_AJOUTER = "MEPForm.ajouter.titre";
	
	//Libellé enregistré
	public static final String DETAIL_MEP_MESSAGE_LIBELLE_SAUVEGARDE_TITRE = "DetailMEP.message.libelle.sauvegarde.titre";
	public static final String DETAIL_MEP_MESSAGE_LIBELLE_SAUVEGARDE_DETAIL = "DetailMEP.message.libelle.sauvegarde.detail";
	
	public static final String DETAIL_MEP_CONFIRM_ANNULATION_PAIEMENT = "DetailMEP.boutons.annulerMEP.confirmationAction";
	public static final String DETAIL_MEP_CONFIRM_GENERATION_FLUX = "DetailMEP.boutons.genererFluxPaiement.confirmationAction";
	
	/*** Controle ***/
	//Formulaire de répartition de service
	public static final String CONTROLE_HEURES_A_PAYER_INTERDIT_SI_PERIODE_NULL = "Formulaire.controle.heuresAPayerInterditSiPeriodeNull";

	// Surcharge du nombre de groupes et du nombre d'heures
	public static final String SURCHARGE_AP_FORM_MESSAGE_NOMBRE_GROUPES_DOIT_ETRE_ENTIER = "SurchargeAPForm.message.nombreGroupesDoitEtreEntier";
	public static final String SURCHARGE_AP_FORM_MESSAGE_NOMBRE_HEURES_DOIT_ETRE_NOMBRE = "SurchargeAPForm.message.nombreHeuresDoitEtreNombre";
	public static final String SURCHARGE_AP_FORM_MESSAGE_NOMBRE_HEURES_DEUX_DECIMALES_MAXIMUM = "SurchargeAPForm.message.nombreHeuresDeuxDecimalesMaximum";
	
	// Êtes-vous sûr de vouloir supprimer cette activité REH ?
	public static final String PARAMETRES_FLUX_PAIEMENT_CONFIRM_SUPPRESS = "FluxPaieGestion.alerte.confirmSuppress";
	
	// Ajouter un nouveau flux de paiement
	public static final String FLUX_PAIEMENT_FORM_TITRE_AJOUTER = "FluxPaieForm.titre.ajouter";

	// Modifier un flux de paiement
	public static final String FLUX_PAIEMENT_FORM_TITRE_MODIFIER = "FluxPaieForm.titre.modifier";
	
	public static final String FLUX_PAIEMENT_LONGUEUR_SAISIE_EXCESSIVE = "FluxPaieForm.controle.longueurValeurSaisieExcessive";
	public static final String FLUX_PAIEMENT_CHAMP_OBLIGATOIRE = "FluxPaieForm.controle.saisieChampRequise";
	
	public static final String FLUX_PAIEMENT_MESSAGE_SUPPRESSION_TITRE = "FluxPaieGestion.message.suppression.titre";
	public static final String FLUX_PAIEMENT_MESSAGE_SUPPRESSION_DETAIL = "FluxPaieGestion.message.suppression.detail";
	
	public static final String FLUX_PAIEMENT_MESSAGE_AJOUT_TITRE = "FluxPaieForm.message.ajout.titre";
	public static final String FLUX_PAIEMENT_MESSAGE_AJOUT_DETAIL = "FluxPaieForm.message.ajout.detail";
	public static final String FLUX_PAIEMENT_MESSAGE_MODIFICATION_DETAIL = "FluxPaieForm.message.modification.detail";
	
	//Êtes-vous sûr de vouloir supprimer ce périmètre de données ?
	public static final String GESTION_DROITS_SUPPRIMER = "gestionDroits.alerte.confirmSuppress";
	public static final String SAISIE_DROITS_ERR_UTILISATEUR = "DroitsForm.message.selectionUtilisateur";
	public static final String SAISIE_DROITS_ERR_PROFIL = "DroitsForm.message.selectionProfil";
	public static final String SAISIE_DROITS_ERR_CIBLE_ENSEIGNANT = "DroitsForm.message.selectionCibleEnseignant";
	public static final String SAISIE_DROITS_ERR_CIBLE_ENSEIGNEMENT = "DroitsForm.message.selectionCibleEnseignement";
	public static final String SAISIE_DROITS_ERR_INDIVIDU_PLUSIEURS_PROFIL = "DroitsForm.message.individuPlusieursProfils";
	
	
	public static final String ENREGISTRER_ET_POURSUIVRE = "forms.message.enregistrerEtPoursuivre";
	
	public static final String DROITS_FORM_TITRE_AJOUTER = "DroitsForm.message.ajout.titre";
	public static final String DROITS_FORM_TITRE_MODIFIER = "DroitsForm.message.modifier.titre";
	
	public static final String APPLICATION_CIRCUIT_VALIDATION = "ApplicationCircuitValidation.message.appliquer";
	
	
}
