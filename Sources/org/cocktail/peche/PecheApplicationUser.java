package org.cocktail.peche;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Création d'une instance d'un utilisateur à partir d'un persId 
 * 
 * @author Chama LAATIK
 *
 */
public class PecheApplicationUser extends ApplicationUser {
	private EOIndividu individu;
	private boolean enseignantDejaRecherche;
	private EOActuelEnseignant enseignant;
	
	private Integer anneeUniversitaire;
	private boolean isAnneeUniversitaireEnAnneeCivil;

	/**
	 * Constructeur
	 * 
	 * @param ec : editingContext
	 * @param persId : persId de la personne qui se connecte
	 * @param annee : année universitaire
	 * @param isAnneeUnivEnAnneeCivil : année universitaire en année civile
	 */
	public PecheApplicationUser(EOEditingContext ec, Integer persId, Integer annee, boolean isAnneeUnivEnAnneeCivil) {
		super(ec, persId);
		anneeUniversitaire = annee;
		isAnneeUniversitaireEnAnneeCivil = isAnneeUnivEnAnneeCivil;
		enseignantDejaRecherche = false;
	}

	/**
	 * Crée une instance d'un utilisateur à partir d'un persId en filtrant sur le nom 
	 * d'une application présente dans GRHUM.GD_APPLICATION
	 * @param ec : editingContext
	 * @param nomAppli : nom de l'application
	 * @param persId : persId de la personne qui se connecte
	 */
	public PecheApplicationUser(EOEditingContext ec, String nomAppli, Integer persId) {
		super(ec, nomAppli, persId);
	}

	/**
	 * @return L'individu connecté
	 */
	public EOIndividu getIndividu() {
		if (individu == null) {
			individu = EOIndividu.individuWithPersId(getEditingContext(), getPersId());
		}
		return individu;
	}
		


	/**
	 * Renvoie l'enseignant de l'utilisateur connecté (si l'utilisateur connecté est aussi un enseignant).
	 * 
	 * @return L'enseignant ou <code>null</code> si l'utilisateur n'est pas enseignant
	 */
	public EOActuelEnseignant getEnseignant() {
		if (!enseignantDejaRecherche) {
			Integer noIndividu = getIndividu().noIndividu();
			NSArray<EOActuelEnseignant> enseignants = EOActuelEnseignant.fetchEOActuelEnseignants(
					getEditingContext(), new EOKeyValueQualifier(EOActuelEnseignant.NO_DOSSIER_PERS_KEY, EOQualifier.QualifierOperatorEqual, noIndividu), null);
			if (enseignants.size() > 0) {
				enseignant = enseignants.get(0);
			}
			enseignantDejaRecherche = true;
		}

		return enseignant;
	}

	/**
	 * @return <code>true</code> si l'utilisateur connecté est aussi un enseignant
	 */
	public boolean isEnseignant() {
		return getEnseignant() != null;
	}

	/**
	 * Est-ce que l'utilisateur est un enseignant statutaire ?
	 * 
	 * @return <code>true</code> si oui
	 */
	public boolean isEnseignantStatutaire() {
		EOActuelEnseignant enseignant1 = getEnseignant();

		if (enseignant1 != null) {
			return enseignant1.isStatutaire(anneeUniversitaire, isAnneeUniversitaireEnAnneeCivil);
		}
		return false;
	}

	/**
	 * Est-ce que l'utilisateur est un enseignant vacataire ?
	 * 
	 * @return <code>true</code> si oui
	 */
	public boolean isEnseignantVacataire() {
		EOActuelEnseignant enseignant1 = getEnseignant();

		if (enseignant1 != null) {
			return !enseignant1.isStatutaire(anneeUniversitaire, isAnneeUniversitaireEnAnneeCivil);
		}
		return false;
	}

	/**
	 * @param service service en cours
	 * @return est-ce que la fiche de service que nous consultons est celle de l'utilisateur connecté
	 */
	public boolean isFicheEnseignantConnecte(EOService service) {
		if (service.enseignant() == null) {
			return false;
		}
		return getIndividu().equals(service.enseignant().toIndividu());
	}
	
}
