package org.cocktail.peche.services;

import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.entity.EOPeriode;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * 
 * @author juliencallewaert
 *
 */
public class PeriodeServiceImpl implements IPeriodeService {

	@Inject
	private IPecheParametres pecheParametres;
	
	/**
	 * {@inheritDoc}
	 */
	public EOPeriode rechercherPremierSemestre(EOEditingContext editingContext) {
		return EOPeriode.fetchEOPeriode(editingContext, EOPeriode.TYPE.eq(EOPeriode.TYPE_SEMESTRE + getPecheParametres().getCodeFormatAnnee()).and(EOPeriode.NO_SEMESTRE.eq(1)));
	}

	/**
	 * {@inheritDoc}
	 */
	public EOPeriode rechercherSecondSemestre(EOEditingContext editingContext) {
		return EOPeriode.fetchEOPeriode(editingContext, EOPeriode.TYPE.eq(EOPeriode.TYPE_SEMESTRE + getPecheParametres().getCodeFormatAnnee()).and(EOPeriode.NO_SEMESTRE.eq(2)));
	}
	
	/**
	 * {@inheritDoc}
	 */
	public EOPeriode rechercherPeriode(EOEditingContext edc, String typePeriode, int ordrePeriode) {
		EOQualifier qualifier = EOPeriode.TYPE.eq(typePeriode + getPecheParametres().getCodeFormatAnnee()).and(EOPeriode.ORDRE_PERIODE.eq(ordrePeriode));
		
		return EOPeriode.fetchEOPeriode(edc, qualifier);
	}
	
	

	public IPecheParametres getPecheParametres() {
		return pecheParametres;
	}

	public void setPecheParametres(IPecheParametres pecheParametres) {
		this.pecheParametres = pecheParametres;
	}
	
}
