package org.cocktail.peche.services;

import org.cocktail.peche.entity.EOPeriode;

import com.webobjects.eocontrol.EOEditingContext;

public interface IPeriodeService {

	
	
	/**
	 * Recherche le premier semestre.
	 * 
	 * @param editingContext un editing context
	 * @return le premier semestre
	 */
	EOPeriode rechercherPremierSemestre(EOEditingContext editingContext);
	

	/**
	 * Recherche le second semestre.
	 * 
	 * @param editingContext un editing context
	 * @return le second semestre
	 */
	EOPeriode rechercherSecondSemestre(EOEditingContext editingContext);
	
	
	/**
	 * Rechercher une période par sont type et son ordre.
	 * 
	 * @param edc un editing context
	 * @param typePeriode un type de période (voir les constantes)
	 * @param ordrePeriode un ordre de période
	 * @return la période trouvée (ou <code>null</code> si non trouvée)
	 */
	EOPeriode rechercherPeriode(EOEditingContext edc, String typePeriode, int ordrePeriode);
	
	
}
