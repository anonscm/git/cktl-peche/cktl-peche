package org.cocktail.peche;

import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktlpeche.droits.Fonction;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.tri.StructureTri;
import org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique.StructureRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique.StructureRepositoryEOF;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.commun.OngletComposante;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

/**
 * 
 * @author yannick
 *
 */
public class Session extends CocktailAjaxSession {

	/** Numéro de série. */
	private static final long serialVersionUID = -1289666401471310028L;
	/** logging. */
	public static final Logger LOG = Logger.getLogger(Session.class);

	private String notificationContainerId = null;
	private PecheApplicationUser applicationUser;
	private PecheAutorisationsCache pecheAutorisationCache = null;
	private OngletComposante ongletComposanteSelectionne;
	/** Les paramètres de l'application Peche. */
	private IPecheParametres pecheParametres = new PecheParametres(defaultEditingContext());

	private String generalErrorMessage;

	public static final String BOUTONRETOUR = "boutonRetour";
	private StructureRepository structureRepository;
	private StructureTri structureTri;
	/**
	 * Constructeur par défaut.
	 */
	public Session() {
	}

	public PecheApplicationUser getApplicationUser() {
		return applicationUser;
	}
	public void setApplicationUser(PecheApplicationUser applicationUser) {
		this.applicationUser = applicationUser;		
	}

	/**
	 * @return the pecheAutorisationCache
	 */
	public PecheAutorisationsCache getPecheAutorisationCache() {
		return pecheAutorisationCache;
	}

	/**
	 * @param pecheAutorisationCache the pecheAutorisationCache to set
	 */
	public void setPecheAutorisationCache(PecheAutorisationsCache pecheAutorisationCache) {
		this.pecheAutorisationCache = pecheAutorisationCache;
	}

	/**
	 * @return Les paramètres de l'application Peche
	 */
	public IPecheParametres getPecheParametres() {
		return pecheParametres;
	}

	/**
	 * @return Tous les départements de l'établissement
	 */
	public List<IStructure> getDepartements() {
		return getStructureRepository().getDepartementsEnseignements();
	}

	/**
	 * @return Toutes les composantes scolarité de l'établissement.
	 */
	public List<IStructure> getComposantesScolarite() {
		getStructureTri().trierParLibelleCourt(getStructureRepository().getComposantesScolarites());
		return getStructureRepository().getComposantesScolarites();
	}

	/**
	 * @return Toutes les structures CS et DE de l'établissement.
	 */
	public List<IStructure> getStructures() {
		return getStructureRepository().getStructuresEnseignement();
	}
	
	/**
	 * @param composanteScolarite Une composante scolarité
	 * @return La liste des structures filles de type "CS" ou "DE" de cette composante scolarité
	 */
	public List<IStructure> getStructures(IStructure composanteScolarite) {
		if (composanteScolarite != null) {
			return getStructureRepository().getStructuresEnseignement(composanteScolarite);
		}
		return null;
	}

	/**
	 * @return un identifiant unique pour les container Ajax.
	 */
	public String notificationContainerId() {
		if (notificationContainerId == null) {
			notificationContainerId = "cktl_" + UUID.randomUUID().toString().replaceAll("-", "_") + "_notificationContainer";
		}
		return notificationContainerId;
	}

	/**
	 * @return l'action à faire lorsque l'on souhaite quitter l'application.
	 */
	public WOActionResults onQuitter() {
		return goToMainSite();
	}

	/**
	 * "Rollback".
	 */
	public void resetDefaultEditingContext() {
		EOEditingContext defaultEditingContext = defaultEditingContext();

		if (defaultEditingContext != null && defaultEditingContext.hasChanges()) {
			LOG.warn("L'editingContext par défaut contenait des changements non sauvegardés. Un revert() a été fait.");

			defaultEditingContext().revert();
		}

		generalErrorMessage = null;

	}

	public NSDictionary exceptionInfos() {
		return null;
	}
	/**
	 * retour sur la page précedente.
	 * 
	 * @return La page appelante
	 */
	public WOActionResults pagePrecedente() {
		WOComponent pageRetour = (WOComponent) this.objectForKey(Session.BOUTONRETOUR);
		if (pageRetour != null) {
			return pageRetour;
		}
		return null;
	}

	public String getGeneralErrorMessage() {
		return generalErrorMessage;
	}

	public void setGeneralErrorMessage(String generalErrorMessage) {
		this.generalErrorMessage = generalErrorMessage;
	}

	/**
	 * Sur la page des vacataires, on peut sélectionner une composante par défaut
	 * Conservée en mémoire
	 * @return ongletComposante
	 */
	public OngletComposante getOngletComposanteSelectionne() {
		return ongletComposanteSelectionne;
	}

	/**
	 * 
	 * @param ongletComposanteSelectionne onglet
	 */
	public void setOngletComposanteSelectionne(
			OngletComposante ongletComposanteSelectionne) {
		this.ongletComposanteSelectionne = ongletComposanteSelectionne;
	}

	/**
	 * Affichage dans le debug de la liste des autorisations
	 */
	public void autorisationDebugList() {
		if (pecheAutorisationCache != null) {
			for (Fonction tmp :Fonction.values()) {			
				if (pecheAutorisationCache.hasDroitUtilisationOnFonction(tmp.getIdFonction())) {
					CktlLog.log("autorisation " + tmp + " : UTILISATION");					
				} else if (pecheAutorisationCache.hasDroitConnaissanceOnFonction(tmp.getIdFonction())) {
					CktlLog.log("autorisation " + tmp + " : CONNAISSANCE");					
				}
			}		
		}
	}

	/**
	 * Ajouter un message d'erreur ou d'info selon le type de message 
	 * @param typeMessage type du message
	 * @param details le message
	 */
	public void addSimpleMessage(TypeMessage typeMessage, String details) {
		if (TypeMessage.ATTENTION.equals(typeMessage) || TypeMessage.INFORMATION.equals(typeMessage)) {
			addSimpleInfoMessage(typeMessage.getNomTypeMessage(), details);
		} else if (TypeMessage.ERREUR.equals(typeMessage)) {
			addSimpleErrorMessage(typeMessage.getNomTypeMessage(), details);
		}
	}

	/**
	 * Ajouter un message d'erreur ou d'info selon le type de message 
	 * @param typeMessage type du message
	 * @param e exception
	 */
	public void addSimpleMessage(TypeMessage typeMessage, Exception e) {
		addSimpleErrorMessage(typeMessage.getNomTypeMessage(), e);
	}

	private StructureRepository getStructureRepository() {
		if (structureRepository == null) {
			structureRepository = new StructureRepositoryEOF(defaultEditingContext());
		}
		
		return structureRepository;
	}
	
	private StructureTri getStructureTri() {
		if (structureTri == null) {
			structureTri = new StructureTri();
		}
		
		return structureTri;
	}

}
