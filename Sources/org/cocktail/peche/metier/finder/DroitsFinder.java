package org.cocktail.peche.metier.finder;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.filtre.GdProfilFiltre;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EODroit;
import org.cocktail.peche.entity.EOPecheParametre;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class DroitsFinder {
	
	
	/** L'instance singleton de cette classe */
	private static DroitsFinder sharedInstance;

	private DroitsFinder() {
		// Pas d'instanciation possible
	}

	/**
	 * Permet d'accéder à cette classe sans l'instancier à partir d'une autre classe
	 * 
	 * Ex : DroitsFinder.sharedInstance().methode
	 * 
	 * @return une instance de la classe
	 */
	public static DroitsFinder sharedInstance() {
		if (sharedInstance == null) {
			sharedInstance = new DroitsFinder();
		}
		return sharedInstance;
	}
	
	/**
	 * @param edc : edintingContext
	 * @return la liste des droits dans Peche en enlevant les profils exclus
	 */
	public NSArray<EODroit> getListeDroits(EOEditingContext edc) {
		return EODroit.fetchEODroits(edc, ERXQ.notIn(EODroit.TO_PROFIL.dot(EOGdProfil.PR_ID_KEY).key(), 
				(NSArray<Integer>) EOPecheParametre.getListeIdProfilExclus(edc)), null);
	}
	
	/**
	 * 
	 * @param edc un editing context
	 * @return la liste dse profils visibles dans l'ajout des droits
	 */
	public List<EOGdProfil> getListeProfilVisible(EOEditingContext edc) {
		GdProfilFiltre profilFiltre = new GdProfilFiltre();
		List<EOGdProfil> profils = DroitsHelper.profilsForApplication(edc, Constante.APP_PECHE);
		return profilFiltre.filtrerParIds(profils, (List<Integer>) EOPecheParametre.getListeIdProfilExclus(edc));
	}
	

	/**
	 * 
	 * @param edc un editing context
	 * @param individu  
	 * @return la liste dse profils visibles dans l'ajout des droits
	 */
	public NSArray<EODroit> getListeDroitsUtilisateur(EOEditingContext edc, EOIndividu individu) {
		return EODroit.fetchEODroits(edc, ERXQ.equals(EODroit.TO_INDIVIDU_KEY, individu), null);
	}
	
	
}
