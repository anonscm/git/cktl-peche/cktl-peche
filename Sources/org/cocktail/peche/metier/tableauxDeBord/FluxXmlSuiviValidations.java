package org.cocktail.peche.metier.tableauxDeBord;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.components.controlers.DemandeCtrl;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Classe permettant de générer un flux XML nécessaire à l'export du suivi des validations.
 */
public class FluxXmlSuiviValidations {
	
	/** logger. */
	private static Logger log = Logger.getLogger(FluxXmlSuiviValidations.class);
	
	private EOEditingContext edc;
	private EnseignantsStatutairesCtrl enseignantsStatutairesCtrl;
	private EnseignantsVacatairesCtrl enseignantsVacatairesCtrl;
	private DemandeCtrl demandeCtrl;
	private HceCtrl hceCtrl;
	
	
	/**
	 * Constructeur.
	 * @param editingContext : l'editingContext
	 */
	public FluxXmlSuiviValidations(EOEditingContext editingContext) {
		edc = editingContext;
		demandeCtrl = new DemandeCtrl(edc);
		hceCtrl = new HceCtrl(editingContext);		
	}

	/**
	 * Retourne le flux XML nécessaire à l'export du tableau de bord sous format excel.
	 * 
	 * @param listeRepartitions : liste des répartitions à exporter
	 * @param criteresRecherche : les criteres de recherche
	 * @return le flux XML
	 */
	public String getFlux(List<EORepartService> listeRepartitions, CriteresRechercheService criteresRecherche) {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);
		BasicPecheCtrl basicPecheCtrl = new BasicPecheCtrl(edc,criteresRecherche.getAnneeUniversitaire(),
				criteresRecherche.isFormatAnneeExerciceAnneeCivile());

		try {
			w.setEscapeSpecChars(true);

			w.startDocument();
			w.startElement("suivi_validations");
			
			w.writeElement("annee_universitaire", StringCtrl.checkString(displayAnneeUniversitaire(criteresRecherche.getAnneeUniversitaire(),
					criteresRecherche.isFormatAnneeExerciceAnneeCivile())));
			w.writeElement("is_statutaire", StringCtrl.checkString(criteresRecherche.isPopulationStatutaireString()));
			w.writeElement("type_fiche", StringCtrl.checkString(criteresRecherche.getTypeFiche()));
			w.writeElement("date_extraction", DateCtrl.dateToString(DateCtrl.now(), "dd/MM/yyyy"));
			
			w.writeElement("composantes_recherchees", StringCtrl.checkString(displayComposantesRecherchees(criteresRecherche.getListeComposantes())));
			w.writeElement("etats_recherchees", StringCtrl.checkString(displayEtatsDossierRecherches(criteresRecherche.getListeEtapesCircuitValidation())));
			
			
			if (criteresRecherche.isPopulationStatutaire()) {
				w.writeElement("population", StringCtrl.checkString("statutaires"));
			} else {
				w.writeElement("population", StringCtrl.checkString("vacataires"));
			}
			
			for (EORepartService uneRepartition : listeRepartitions) {
				w.startElement("enseignant");
				
				w.writeElement("nom", StringCtrl.checkString(uneRepartition.toService().enseignant().toIndividu().nomAffichage().toUpperCase()));
				w.writeElement("prenom", StringCtrl.checkString(uneRepartition.toService().enseignant().toIndividu().prenomAffichage()));
				
				if (criteresRecherche.isPopulationStatutaire()) {
					w.writeElement("composante", StringCtrl.checkString(uneRepartition.toService().enseignant().getComposante(
							criteresRecherche.getAnneeUniversitaire(), criteresRecherche.isFormatAnneeExerciceAnneeCivile()).lcStructure()));
					EOStructure dept = uneRepartition.toService().enseignant().getDepartementEnseignement(criteresRecherche.getAnneeUniversitaire(), criteresRecherche.isFormatAnneeExerciceAnneeCivile());
					if (dept != null) {
						w.writeElement("departement", StringCtrl.checkString(dept.lcStructure()));
					}
				} else {
					if (basicPecheCtrl.isStructureDeTypeGroupe(uneRepartition.toStructure(), EOTypeGroupe.TGRP_CODE_CS)) {
						w.writeElement("composante", StringCtrl.checkString(uneRepartition.toStructure().lcStructure()));										
					} else if (basicPecheCtrl.isStructureDeTypeGroupe(uneRepartition.toStructure(), EOTypeGroupe.TGRP_CODE_DE)) {
						EOStructure parent = uneRepartition.toStructure().toStructurePere();
						if (parent != null && basicPecheCtrl.isStructureDeTypeGroupe(parent, EOTypeGroupe.TGRP_CODE_CS)) {
							w.writeElement("composante", StringCtrl.checkString(parent.lcStructure()));
							w.writeElement("departement", StringCtrl.checkString(uneRepartition.toStructure().lcStructure()));									
						}
					}					
				}				
				w.writeElement("etat", StringCtrl.checkString(demandeCtrl.etatEtape(uneRepartition.toDemande())));
				
				Map<String, EOHistoriqueDemande> mapVisas = demandeCtrl.getMapVisasHistorique(uneRepartition.toDemande());
				
				if (mapVisas.get(uneRepartition.toDemande().getToEtape().codeEtape()) != null) {
					w.writeElement("date", DateCtrl.dateToString(mapVisas.get(uneRepartition.toDemande()
							.getToEtape().codeEtape()).dateModification(), "dd/MM/yyyy"));
				} 
				
				if (criteresRecherche.isPopulationStatutaire()) {
					getBalisesStatutaireDetailHeures(w, uneRepartition, criteresRecherche);
				} else {
					getBalisesVacataireDetailHeures(w, uneRepartition, criteresRecherche);
				}
				
				w.endElement();
			}
			
			w.endElement();
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}
		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");
		return sw.toString();
	}
	
	/**
	 * Ecrit les balises du détail des heures d'un statutaire
	 * @param xmlWriter : le flux xml
	 * @param uneRepartition : la répartition
	 * @param criteresRecherche : les criteres de recherche
	 */
	public void getBalisesStatutaireDetailHeures(CktlXMLWriter xmlWriter, EORepartService uneRepartition, CriteresRechercheService criteresRecherche) {
		DecimalFormat df = new DecimalFormat("0.00#");
		enseignantsStatutairesCtrl = new EnseignantsStatutairesCtrl(getEdc(), criteresRecherche.getAnneeUniversitaire(), 
				criteresRecherche.isFormatAnneeExerciceAnneeCivile());

		try {
			//Service
			xmlWriter.writeElement("service_statutaire", df.format(uneRepartition.toService().getHeuresService()));
			xmlWriter.writeElement("modalite_service", df.format(hceCtrl.getTotalNbHeuresModaliteService(
					uneRepartition.toService().enseignant(), criteresRecherche.getAnneeUniversitaire(), criteresRecherche.isFormatAnneeExerciceAnneeCivile())));
			xmlWriter.writeElement("decharges", df.format(hceCtrl.getTotalNbHeuresDecharges(
					uneRepartition.toService().enseignant().toIndividu(),criteresRecherche.getAnneeUniversitaire(), criteresRecherche.isFormatAnneeExerciceAnneeCivile())));
			xmlWriter.writeElement("minoration_service", df.format(hceCtrl.getTotalNbHeuresMinorations(
					uneRepartition.toService().enseignant(), criteresRecherche.getAnneeUniversitaire(), criteresRecherche.isFormatAnneeExerciceAnneeCivile())));
			xmlWriter.writeElement("service_du", df.format(enseignantsStatutairesCtrl.getServiceDu(
					uneRepartition.toService().enseignant(), criteresRecherche.getAnneeUniversitaire())));
	
			//Service et hcomp prévionnels et définitifs
			xmlWriter.writeElement("service_prevu", df.format(enseignantsStatutairesCtrl.getServiceAttribue(
					uneRepartition.toService().enseignant())));
			xmlWriter.writeElement("hcomp_prevu", df.format(enseignantsStatutairesCtrl.getHcompPrev(
					uneRepartition.toService().enseignant().getService(criteresRecherche.getAnneeUniversitaire()))));
			xmlWriter.writeElement("service_realise", df.format(enseignantsStatutairesCtrl.getServiceRealise(
					uneRepartition.toService().enseignant())));
			xmlWriter.writeElement("hcomp_realise", df.format(enseignantsStatutairesCtrl.getHcompRealise(
					uneRepartition.toService().enseignant().getService(criteresRecherche.getAnneeUniversitaire()))));
	
			//Détail des heures complémentaires
			xmlWriter.writeElement("heures_payees", df.format(OutilsValidation.retournerBigDecimalNonNull(
					uneRepartition.toService().heuresPayees())));
			xmlWriter.writeElement("heures_a_payer", df.format(OutilsValidation.retournerBigDecimalNonNull(
					uneRepartition.toService().heuresAPayer())));
			xmlWriter.writeElement("heures_a_reporter", df.format(OutilsValidation.retournerBigDecimalNonNull(
					uneRepartition.toService().heuresAReporterAttribuees())));
			xmlWriter.writeElement("heures_gratuites", df.format(OutilsValidation.retournerBigDecimalNonNull(
					uneRepartition.toService().heuresGratuites())));
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}
	}
	
	/**
	 * Ecrit les balises du détail des heures d'une répartition d'un vacataire
	 * @param xmlWriter : le flux XML
	 * @param uneRepartition : la répartition
	 * @param criteresRecherche : les critères de recherche
	 */
	public void getBalisesVacataireDetailHeures(CktlXMLWriter xmlWriter, EORepartService uneRepartition, CriteresRechercheService criteresRecherche) {
		DecimalFormat df = new DecimalFormat("0.00#");
		enseignantsVacatairesCtrl = new EnseignantsVacatairesCtrl(getEdc(), criteresRecherche.getAnneeUniversitaire(), 
				criteresRecherche.isFormatAnneeExerciceAnneeCivile());
		
		BigDecimal totalHeuresPrevuesHetdStructure = BigDecimal.ZERO;
		BigDecimal totalHeuresRealiseesHetdStructure = BigDecimal.ZERO;
		BigDecimal totalHeuresAPayerHetdStructure = BigDecimal.ZERO;
		BigDecimal totalHeuresPayeesHetdStructure = BigDecimal.ZERO;
		BigDecimal totalDeltaPayeesEtRealiseesHetdStructure = BigDecimal.ZERO;
		
		try {	
			for (IStructure uneComposante : criteresRecherche.getListeComposantes()) {
				if (uneComposante.equals(uneRepartition.toStructure())) {
					for (EOServiceDetail unServiceDetail : uneRepartition.toListeServiceDetail()) {
						if (unServiceDetail.composantAP() != null) {
							totalHeuresPrevuesHetdStructure = totalHeuresPrevuesHetdStructure.add(enseignantsVacatairesCtrl.getHeuresPresentielEnHETDParAP(
									uneRepartition.toService(), unServiceDetail.composantAP(), unServiceDetail.heuresPrevues()));
							totalHeuresRealiseesHetdStructure = totalHeuresRealiseesHetdStructure.add(enseignantsVacatairesCtrl.getHeuresPresentielEnHETDParAP(
									uneRepartition.toService(), unServiceDetail.composantAP(), unServiceDetail.heuresRealisees()));
							
							if (unServiceDetail.heuresAPayer() != null) {
								totalHeuresAPayerHetdStructure = totalHeuresAPayerHetdStructure.add(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(
										uneRepartition.toService(), unServiceDetail.composantAP(), unServiceDetail.heuresAPayer()));
							}
							
							if (unServiceDetail.heuresPayees() != null) {
								totalHeuresPayeesHetdStructure = totalHeuresPayeesHetdStructure.add(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(
										uneRepartition.toService(), unServiceDetail.composantAP(), unServiceDetail.heuresPayees()));
							}
							
							totalDeltaPayeesEtRealiseesHetdStructure = totalDeltaPayeesEtRealiseesHetdStructure.add(enseignantsVacatairesCtrl.getDeltaHeuresPayeEtRealiseHETD(
									uneRepartition.toService(), unServiceDetail.composantAP(), unServiceDetail.heuresPayees(), unServiceDetail.heuresRealisees()));
							
						} else {
							//Cas du REH
							totalHeuresPrevuesHetdStructure = totalHeuresPrevuesHetdStructure.add(new BigDecimal(unServiceDetail.heuresPrevues()));
							totalHeuresRealiseesHetdStructure = totalHeuresRealiseesHetdStructure.add(new BigDecimal(unServiceDetail.heuresRealisees()));
							
							if (unServiceDetail.heuresAPayer() != null) {
								totalHeuresAPayerHetdStructure = totalHeuresAPayerHetdStructure.add(unServiceDetail.heuresAPayer());
							}
							
							if (unServiceDetail.heuresPayees() != null) {
								totalHeuresPayeesHetdStructure = totalHeuresPayeesHetdStructure.add(unServiceDetail.heuresPayees());
							}
							
							totalDeltaPayeesEtRealiseesHetdStructure = totalDeltaPayeesEtRealiseesHetdStructure.add(enseignantsVacatairesCtrl.
									getDeltaHeuresPayeEtRealisePresentiel(unServiceDetail.heuresPayees(), unServiceDetail.heuresRealisees()));
						}
					}
				
					xmlWriter.writeElement("heures_prevues", df.format(OutilsValidation.retournerBigDecimalNonNull(
							totalHeuresPrevuesHetdStructure)));
					xmlWriter.writeElement("heures_realisees", df.format(OutilsValidation.retournerBigDecimalNonNull(
							totalHeuresRealiseesHetdStructure)));
					
					xmlWriter.writeElement("heures_a_payer", df.format(OutilsValidation.retournerBigDecimalNonNull(
							totalHeuresAPayerHetdStructure)));
					xmlWriter.writeElement("heures_payees", df.format(OutilsValidation.retournerBigDecimalNonNull(
							totalHeuresPayeesHetdStructure)));
					xmlWriter.writeElement("delta_heures", df.format(OutilsValidation.retournerBigDecimalNonNull(
							totalDeltaPayeesEtRealiseesHetdStructure)));
				
				}
			}
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}
	}
	
	/**
	 * @param listeComposantes : la liste des composantes recherchées
	 * @return Affichage de la liste des composantes recherchées
	 */
	public String displayComposantesRecherchees(List<IStructure> listeComposantes) {
		StringBuffer listeComposante = new StringBuffer();
		
		for (IStructure uneComposante : listeComposantes) {
			if (uneComposante == listeComposantes.get(0)) {
				listeComposante.append(uneComposante.lcStructure());
			} else {
				listeComposante.append(", ").append(uneComposante.lcStructure());
			}
		}
		
		return listeComposante.toString();
	}
	
	/**
	 * @param listeEtapes : la liste des étapes recherchées
	 * @return Affichage de la liste des étapes recherchées
	 */
	public String displayEtatsDossierRecherches(List<EtapePeche> listeEtapes) {
		StringBuffer listeEtape = new StringBuffer();
		for (EtapePeche uneEtape : listeEtapes) {
			if (uneEtape == listeEtapes.get(0)) {
				listeEtape.append(uneEtape.getEtatDemande());
			} else {
				listeEtape.append(", ").append(uneEtape.getEtatDemande());
			}
		}
		
		return listeEtape.toString();
	}
	
	/**
	 * @param anneeUniversitaire : l'année universitaire
	 * @param isFormatAnneeExerciceAnneeCivile : est-ce que l'année univeritaire est en année civile?
	 * @return Affichage de l'année universitaire en fonction si l'année et civile ou non
	 */
	public String displayAnneeUniversitaire(int anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		String affichageAnnee = null;
		if (isFormatAnneeExerciceAnneeCivile) {
			affichageAnnee = new Integer(anneeUniversitaire).toString();
		} else {
			affichageAnnee = new Integer(anneeUniversitaire).toString() + "/" + new Integer(anneeUniversitaire + 1).toString();
		}
		
		return affichageAnnee;
	}

	public EOEditingContext getEdc() {
		return edc;
	}

	public void setEdc(EOEditingContext edc) {
		this.edc = edc;
	}
}
