package org.cocktail.peche.metier.tableauxDeBord;

import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EORepartService;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Cette classe représente les critères de recherche des fiches de service à exporter.
 * 
 * @author Chama LAATIK
 */
public class CriteresRechercheService {

	/** Quel est la population recherchée : statutaire ou vacataire? */
	private String population;

	/** Filtre sur l'année universitaire (obligatoire). */
	private int anneeUniversitaire;

	private boolean isFormatAnneeExerciceAnneeCivile;

	/** Filtre sur les composantes (facultatif). */
	private List<IStructure> listeComposantes;

	private IStructure structure;

	/**
	 * Quel est le type de la fiche de service recherché: prévisionnel ou
	 * définitif?
	 */
	private String typeFiche;

	/** Filtre sur les étapes du circuit de validation */
	private List<EtapePeche> listeEtapesCircuitValidation;

	/** Logger. */
	private static Logger log = Logger.getLogger(CriteresRechercheService.class);


	/**
	 * Constructeur
	 * @param isFormatAnneeExerciceAnneeCivile : année universitaire en civile?
	 */
	public CriteresRechercheService(boolean isFormatAnneeExerciceAnneeCivile) {
		this.isFormatAnneeExerciceAnneeCivile = isFormatAnneeExerciceAnneeCivile;
	}

	/**
	 * Retourne <code>true</code> si les critères non codable en SQL sont satisfaits.
	 * 
	 * @param edc : editingContext
	 * @param uneRepartitionService : la répartition du service
	 * @return <code>true</code> si les critères non codable en SQL sont satisfaits
	 */
	public boolean isCriteresAuxiliairesSatisfaits(EOEditingContext edc, EORepartService uneRepartitionService) {
		if (uneRepartitionService.toDemande() != null) {
			log.debug("  > étape à traiter : " + uneRepartitionService.toDemande().getToEtape().codeEtape());

			// On vérifie que l'étape de la demande de la fiche de service correspond à une des étapes recherchées
			if ((getListeEtapesCircuitValidation() != null) && getListeEtapesCircuitValidation().toString().contains(uneRepartitionService.toDemande().getToEtape().codeEtape())) {
				log.debug("   > Etape OK ");

				// On vérifie le circuit de validation de la fiche de service en fonction du type de fiche recherché
				if (getListeCircuitsValidation(edc, getLibelleCircuitValidation(isPopulationStatutaire(), isTypeFichePrevisionnel()))
						.contains(uneRepartitionService.toDemande().getToEtape().toCircuitValidation())) {
					log.debug("   > Circuit OK");

					if (isComposanteOK(uneRepartitionService, isPopulationStatutaire())) {
						return true;
					}

				} else {
					log.debug("   > Circuit KO");
				}
			} else {
				log.debug("   > Etape KO ");
			}
		}

		log.debug("  > Ne répond pas aux critères");
		return false;
	}

	/**
	 * Est-ce que la composante ou le département de l'enseignant est présente dans la liste des composantes/départements recherchés ?
	 * @param uneRepart : répartition des demandes du service
	 * @param isPopulationStatutaire : enseignant statutaire
	 * @return Vrai/Faux
	 */
	public boolean isComposanteOK(EORepartService uneRepart, boolean isPopulationStatutaire) {
		boolean isComposanteOK = false;

		if (isPopulationStatutaire) {
			log.debug("   > Composante de l'enseignant statutaire : " + uneRepart.toService().enseignant().getComposante(getAnneeUniversitaire(),
					isFormatAnneeExerciceAnneeCivile()));
			//Si la liste des composantes est nulle, on fait une recherche sur toutes les composantes
			//Sinon, on vérifie que la composante de l'enseignant est présente dans la liste
			EOStructure composante = uneRepart.toService().enseignant().getComposante(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());
			EOStructure dept = uneRepart.toService().enseignant().getDepartementEnseignement(getAnneeUniversitaire(), isFormatAnneeExerciceAnneeCivile());

			if ((getListeComposantes() != null 
					&& getListeComposantes().size() > 0) 
					&& composante != null
					&& (getListeComposantes().contains(composante))
					|| (getListeComposantes() == null)) {
				log.debug("    > Composante OK");
				isComposanteOK = true;
			}

			if (!isComposanteOK 
					&& getListeComposantes() != null 
					&& getListeComposantes().size() > 0
					&& dept != null 
					&& getListeComposantes().contains(dept) ) {
				log.debug("    > Departement OK");
				isComposanteOK = true;				
			}
		} else {
			if (!NSArrayCtrl.isEmpty(uneRepart.toListeServiceDetail()) || !uneRepart.toDemande().estSurEtapeInitiale()) {	
				log.debug("   > Composante de l'enseignant vacataire : " + uneRepart.toStructure());		
				//Si la liste des composantes est nulle, on fait une recherche sur toutes les composantes
				//Sinon, on vérifie que la composante de l'enseignant est présente dans la liste
				if ((uneRepart.toStructure() != null) 
						&& (getListeComposantes() != null)
						&& (getListeComposantes().size() > 0)
						&& (getListeComposantes().contains(uneRepart.toStructure()))
						|| (getListeComposantes() == null)) {
					log.debug("    > Structure OK");
					isComposanteOK = true;
				}
			} else {
				log.debug("    > Composante KO");
			}
			
		}

		return isComposanteOK;
	}

	/**
	 * @param isPopulationStatutaire : Est-ce qu'on traite des statutaires?
	 * @param isTypeFichePrevisionnel : Est-ce que le type de la fiche de service est prévisionnel?
	 * @return Le libellé du circuit de validation en fonction de la population (statutaires ou vacataires) 
	 * et du type de la fiche de service (présionnel ou définitif)
	 */
	private CircuitPeche getLibelleCircuitValidation(boolean isPopulationStatutaire, boolean isTypeFichePrevisionnel) {
		CircuitPeche libelleCircuitPeche = null;

		if (isPopulationStatutaire) {
			if (isTypeFichePrevisionnel) {
				libelleCircuitPeche = CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL;
			} else {
				libelleCircuitPeche = CircuitPeche.PECHE_STATUTAIRE;
			}
		} else {
			if (isTypeFichePrevisionnel) {
				libelleCircuitPeche = CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL;
			} else {
				libelleCircuitPeche = CircuitPeche.PECHE_VACATAIRE;
			}
		}

		return libelleCircuitPeche;
	}

	/**
	 * @param edc : editingContext
	 * @param leCircuit : le code du circuit de validation pour l'application PECHE
	 * @return la liste des circuits de validation concernés
	 */
	private List<EOCircuitValidation> getListeCircuitsValidation(EOEditingContext edc, CircuitPeche leCircuit) {
		return EOCircuitValidation.rechercherCircuitsValidation(edc, Constante.APP_PECHE, leCircuit.getCodeCircuit());
	}

	/**
	 * Retourne Oui si filtrage statutaire, sinon Non
	 * @return Oui/Non
	 */
	public String isPopulationStatutaireString() {
		if (Constante.STATUTAIRE.equals(getPopulation())) {
			return Constante.OUI;
		}
		return Constante.NON;
	}

	/**
	 * Retourne Vrai si filtrage statutaire
	 * @return Vrai/Faux
	 */
	public boolean isPopulationStatutaire() {
		if (Constante.STATUTAIRE.equals(getPopulation())) {
			return true;
		}
		return false;
	}

	/**
	 * Retourne Vrai si filtrage sur le type prévisionnel
	 * @return Vrai/Faux
	 */
	public boolean isTypeFichePrevisionnel() {
		if (Constante.PREVISIONNEL.equals(getTypeFiche())) {
			return true;
		}
		return false;
	}

	public String getPopulation() {
		return population;
	}

	public void setPopulation(String population) {
		this.population = population;
	}

	public int getAnneeUniversitaire() {
		return anneeUniversitaire;
	}

	public void setAnneeUniversitaire(int anneeUniversitaire) {
		this.anneeUniversitaire = anneeUniversitaire;
	}

	public List<IStructure> getListeComposantes() {
		return listeComposantes;
	}

	public void setListeComposantes(List<IStructure> listeComposantes) {
		this.listeComposantes = listeComposantes;
	}

	public String getTypeFiche() {
		return typeFiche;
	}

	public void setTypeFiche(String typeFiche) {
		this.typeFiche = typeFiche;
	}

	public List<EtapePeche> getListeEtapesCircuitValidation() {
		return listeEtapesCircuitValidation;
	}

	public void setListeEtapesCircuitValidation(List<EtapePeche> listeEtapesCircuitValidation) {
		this.listeEtapesCircuitValidation = listeEtapesCircuitValidation;
	}

	public boolean isFormatAnneeExerciceAnneeCivile() {
		return isFormatAnneeExerciceAnneeCivile;
	}

	public IStructure getStructure() {
		return structure;
	}

	public void setStructure(IStructure structure) {
		this.structure = structure;
	}

}
