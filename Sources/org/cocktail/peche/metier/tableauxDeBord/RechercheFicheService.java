package org.cocktail.peche.metier.tableauxDeBord;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

/**
 * Cette classe permet de gérer la recherche des fiches de services à exporter pour les tableaux de bord. 
 * 
 * @author Chama LAATIK
 */
public final class RechercheFicheService {

	/** L'instance singleton de cette classe */
	private static RechercheFicheService instance;
	
	/** Logger. */
	private static Logger log = Logger.getLogger(RechercheFicheService.class);

	private int TAILLE_LISTE_RECHERCHE = 50;

	private RechercheFicheService() {
		// Pas d'instanciation possible
	}

	/**
	 * Retourne l'instance singleton de cette classe.
	 * 
	 * @return le singleton de cette classe
	 */
	public static RechercheFicheService getInstance() {
		if (instance == null) {
			instance = new RechercheFicheService();
		}
		return instance;
	}
	
	/**
	 * Recherche la liste des fiches de service à exporter
	 * @param ec : editingContext
	 * @param criteresRechercheService : les critères de recherche
	 * @param isRechercheLimite : est-ce qu'on limite la recherche aux 50 premiers?
	 * @return la liste des répartitions des fiches de service
	 */
	public List<EORepartService> rechercherListeFichesServices(EOEditingContext ec, CriteresRechercheService criteresRechercheService, 
			boolean isRechercheLimite) {
		NSArray<EOSortOrdering> tri = EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE).ascInsensitives()
				.then(EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.PRENOM_AFFICHAGE).asc())
				.then(EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU).asc());
		
		EOQualifier qualifier = 
				ERXQ.and(
						ERXQ.equals(EOService.ANNEE_KEY, criteresRechercheService.getAnneeUniversitaire()),
						ERXQ.equals(EOService.ENSEIGNANT.dot(EOActuelEnseignant.TEM_STATUTAIRE_KEY).key(), criteresRechercheService.isPopulationStatutaireString())
						);
		
		List<EOService> listeFichesService = EOService.fetchAllEOServicesDistinct(ec, qualifier, tri);
		
		log.debug("Le nombre de fiche de service à traiter est : " + listeFichesService.size());
		
		List<EORepartService> listeRepartServicesATraiter = new ArrayList<EORepartService>();
		
		//On parcoure la liste des fiches de service
		for (EOService unService : listeFichesService) {
			log.debug("*******************");
			log.debug(" > L'enseignant traité : " + unService.enseignant().toIndividu().getNomPrenomAffichage());
			
			//On parcoure toutes les répartitions de la fiche de service
			for (EORepartService uneRepart : unService.toListeRepartService()) {
				//on limite la recherche
				if (isRechercheLimite) {
					// Si la liste n'a pas encore atteint la taille maximale et que les critères
					// sont satisfaits => on ajoute dans la liste des répartitions à afficher
					if (listeRepartServicesATraiter.size() < TAILLE_LISTE_RECHERCHE) {
						if (criteresRechercheService.isCriteresAuxiliairesSatisfaits(ec, uneRepart)) {
							log.debug("  > Ajout dans la liste des fiches de services à exporter");
							listeRepartServicesATraiter.add(uneRepart);
						} 
					} else {
						break;
					}
				} else {
					//On exporte la liste de toutes les répartitions qui répondent aux critères
					if (criteresRechercheService.isCriteresAuxiliairesSatisfaits(ec, uneRepart)) {
						log.debug("  > Ajout dans la liste des fiches de services à exporter");
						listeRepartServicesATraiter.add(uneRepart);
					} 
				}
			}
		}
		
		return listeRepartServicesATraiter;
	}
	
}
