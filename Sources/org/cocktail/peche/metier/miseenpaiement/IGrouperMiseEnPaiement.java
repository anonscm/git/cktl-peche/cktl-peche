package org.cocktail.peche.metier.miseenpaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Interface nécessaire pour gérer des ruptures.
 * 
 * @param <T> le type sur lequelle on fait la rupture
 */
public interface IGrouperMiseEnPaiement<T> {

	/**
	 * Retourne le tri permettant de gérer la rupture.
	 * <p>
	 * Les enregistrements doivent être ordonnés par les colonnes utilisées par la rupture.
	 * <p>
	 * La colonne taux brut doit toujours être la dernière colonne de tri et doit obligatoirement être présente.
	 * 
	 * @return le tri permettant de gérer la rupture
	 */
	NSArray<EOSortOrdering> getTri();
	
	/**
	 * Est-ce qu'on est en rupture ?
	 * 
	 * @param precedent l'occurence précédente
	 * @param enCours l'occurence en cours
	 * @return <code>true</code> si on est en rupture
	 */
	boolean isRupture(T precedent, T enCours);
}
