package org.cocktail.peche.metier.miseenpaiement;

import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EOService;

public class MiseEnPaiementDataBean {
	private EOService service;
	private EOStructure structure;
	private EOPeriode periode;
	private BigDecimal heuresAPayer = BigDecimal.ZERO;
	private BigDecimal heuresAPayerHetd = BigDecimal.ZERO;
	private BigDecimal heuresPayees = BigDecimal.ZERO;
	private BigDecimal heuresPayeesHetd = BigDecimal.ZERO;
	private BigDecimal tauxBrut;
	private BigDecimal tauxCharge;	
	private BigDecimal montantAPayerBrut = BigDecimal.ZERO;
	private BigDecimal montantPayesBrut = BigDecimal.ZERO;
	private BigDecimal montantAPayerCharge = BigDecimal.ZERO;
	private BigDecimal montantPayesCharge = BigDecimal.ZERO;
	
	
	private boolean serviceValeurInconsistante;
	private boolean structureValeurInconsistante;
	private boolean periodeValeurInconsistante;
	private boolean tauxBrutValeurInconsistante;
	private boolean tauxChargeValeurInconsistante;
	
	
	public EOService getService() {
		if (serviceValeurInconsistante) {
			return null;
		}

		return service;
	}
	
	public void setService(EOService service) {
		this.service = service;
		serviceValeurInconsistante = false;
	}
	
	public EOStructure getStructure() {
		if (structureValeurInconsistante) {
			return null;
		}
		
		return structure;
	}
	
	public void setStructure(EOStructure structure) {
		this.structure = structure;
		structureValeurInconsistante = false;
	}
	
	public EOPeriode getPeriode() {
		if (periodeValeurInconsistante) {
			return null;
		}
		
		return periode;
	}
	
	public void setPeriode(EOPeriode periode) {
		this.periode = periode;
		periodeValeurInconsistante = false;
	}
	
	public BigDecimal getHeuresAPayer() {
		return heuresAPayer;
	}
	
	public void setHeuresAPayer(BigDecimal heuresAPayer) {
		this.heuresAPayer = heuresAPayer;
	}
	
	public BigDecimal getHeuresAPayerHetd() {
		return heuresAPayerHetd;
	}
	
	public void setHeuresAPayerHetd(BigDecimal heuresAPayerHetd) {
		this.heuresAPayerHetd = heuresAPayerHetd;
	}
	
	public BigDecimal getHeuresPayees() {
		return heuresPayees;
	}
	
	public void setHeuresPayees(BigDecimal heuresPayees) {
		this.heuresPayees = heuresPayees;
	}
	
	public BigDecimal getHeuresPayeesHetd() {
		return heuresPayeesHetd;
	}
	
	public void setHeuresPayeesHetd(BigDecimal heuresPayeesHetd) {
		this.heuresPayeesHetd = heuresPayeesHetd;
	}
	
	public BigDecimal getTauxBrut() {
		if (tauxBrutValeurInconsistante) {
			return null;
		}
		
		return tauxBrut;
	}
	
	public void setTauxBrut(BigDecimal tauxBrut) {
		this.tauxBrut = tauxBrut;
		tauxBrutValeurInconsistante = false;
	}

	public BigDecimal getTauxCharge() {
		if (tauxChargeValeurInconsistante) {
			return null;
		}
		
		return tauxCharge;
	}
	
	public void setTauxCharge(BigDecimal tauxCharge) {
		this.tauxCharge = tauxCharge;
		tauxChargeValeurInconsistante = false;
	}
	
	public BigDecimal getMontantAPayerBrut() {
		return montantAPayerBrut;
	}

	public void setMontantAPayerBrut(BigDecimal montantAPayer) {
		this.montantAPayerBrut = montantAPayer;
	}

	public BigDecimal getMontantPayesBrut() {
		return montantPayesBrut;
	}

	public void setMontantPayesBrut(BigDecimal montantPayes) {
		this.montantPayesBrut = montantPayes;
	}

	public BigDecimal getMontantAPayerCharge() {
		return montantAPayerCharge;
	}

	public void setMontantAPayerCharge(BigDecimal montantAPayer) {
		this.montantAPayerCharge = montantAPayer;
	}

	public BigDecimal getMontantPayesCharge() {
		return montantPayesCharge;
	}

	public void setMontantPayesCharge(BigDecimal montantPayes) {
		this.montantPayesCharge = montantPayes;
	}
	public void cumulService(EOService service) {
		if (!serviceValeurInconsistante) {
			if (this.service == null) {
				this.service = service;
			} else if (service == null || !this.service.primaryKey().equals(service.primaryKey())) {
				serviceValeurInconsistante = true;
			}
		}
	}
	
	public void cumulStructure(EOStructure structure) {
		if (!structureValeurInconsistante) {
			if (this.structure == null) {
				this.structure = structure;
			} else if (structure == null || !this.structure.primaryKey().equals(structure.primaryKey())) {
				structureValeurInconsistante = true;
			}
		}
	}
	
	public void cumulPeriode(EOPeriode periode) {
		if (!periodeValeurInconsistante) {
			if (this.periode == null) {
				this.periode = periode;
			} else if (periode == null || !this.periode.primaryKey().equals(periode.primaryKey())) {
				periodeValeurInconsistante = true;
			}
		}
	}
	
	public void cumulHeuresAPayer(BigDecimal heuresAPayer) {
		setHeuresAPayer(getHeuresAPayer().add(heuresAPayer));
	}
	
	public void cumulheuresAPayerHetd(BigDecimal heuresAPayerHetd) {
		setHeuresAPayerHetd(getHeuresAPayerHetd().add(heuresAPayerHetd));
	}
	
	public void cumulheuresPayees(BigDecimal heuresPayees) {
		setHeuresPayees(getHeuresPayees().add(heuresPayees));
	}
	
	public void cumulheuresPayeesHetd(BigDecimal heuresPayeesHetd) {
		setHeuresPayeesHetd(getHeuresPayeesHetd().add(heuresPayeesHetd));
	}
	
	public void cumultauxBrut(BigDecimal tauxBrut) {
		if (!tauxBrutValeurInconsistante) {
			if (this.tauxBrut == null) {
				this.tauxBrut = tauxBrut;
			} else if (tauxBrut == null || this.tauxBrut.compareTo(tauxBrut) != 0) {
				tauxBrutValeurInconsistante = true;
			}
		}
	}

	public void cumultauxCharge(BigDecimal tauxCharge) {
		if (!tauxChargeValeurInconsistante) {
			if (this.tauxCharge == null) {
				this.tauxCharge = tauxCharge;
			} else if (tauxCharge == null || this.tauxCharge.compareTo(tauxCharge) != 0) {
				tauxChargeValeurInconsistante = true;
			}
		}
	}

	
	public void cumulMontantAPayerBrut(BigDecimal montantAPayer) {
		setMontantAPayerBrut(getMontantAPayerBrut().add(montantAPayer));
	}
	
	public void cumulMontantPayesBrut(BigDecimal montantPayes) {
		setMontantPayesBrut(getMontantPayesBrut().add(montantPayes));
	}
	
	public void cumulMontantAPayerCharge(BigDecimal montantAPayer) {
		setMontantAPayerCharge(getMontantAPayerCharge().add(montantAPayer));
	}
	
	public void cumulMontantPayesCharge(BigDecimal montantPayes) {
		setMontantPayesCharge(getMontantPayesCharge().add(montantPayes));
	}
	
}
