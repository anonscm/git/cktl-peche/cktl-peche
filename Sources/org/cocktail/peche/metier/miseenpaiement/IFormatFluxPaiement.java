package org.cocktail.peche.metier.miseenpaiement;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Cette interface impose les méthodes minimum pour implémenter la génération d'un flux de paiement.
 *  
 * @author Pascal MACOUIN
 */
public interface IFormatFluxPaiement {
	
	/**
	 * Paramètres nécessaires à la mise en paiement.
	 * 
	 * @param parametresFluxPaiement les paramètres nécessaires à la mise en paiement
	 */
	void setParametresFluxPaiement(EOParametresFluxPaiement parametresFluxPaiement);
	
	/**
	 * Tri sur la table {@link EOFluxMiseEnPaiement} pour le tri du flux de sortie.
	 * 
	 * @return le tri pour le flux de sortie
	 */
	NSArray<EOSortOrdering> getTri();
	
	/**
	 * Retourne la ligne d'en-tête du flux (la première ligne).
	 * Peut retourner <code>null</code> s'il n'y a pas de ligne d'entête dans le flux.
	 * 
	 * @return la ligne d'entête du flux
	 */
	String getLigneEntete();
	
	/**
	 * Retourne une ligne détail d'un paiement pour une personne.
	 * Peut retourner <code>null</code> s'il n'y a pas de ligne détail généré pour ce flux de mise en paiement.
	 * 
	 * @param fluxMiseEnPaiement Les données du flux de mise en paiement
	 * @return une ligne détail d'un paiement pour une personne
	 * @throws Exception 
	 */
	String getLigneDetail(EOFluxMiseEnPaiement fluxMiseEnPaiement, boolean estCotisationASolidarite) throws Exception;
	
	/**
	 * Retourne le nom du flux formaté en fonction du {@link EOParametresFluxPaiement#templateNomFlux() template}.
	 * 
	 * @param paiement le paiement
	 * @return le nom du flux formaté en fonction du {@link EOParametresFluxPaiement#templateNomFlux() template}
	 */
	String getNomFlux(EOPaiement paiement);
	
	/**
	 * Extension du nom du flux pour le format.
	 * 
	 * @return extension du nom du flux
	 */
	String getExtensionNomFlux();
	
	/**
	 * le flux est au format Excel
	 * 
	 * @return true si format excel
	 */	
	boolean isFormatExcel();
	
	/**
	 * Retourne le flux formaté pour Excel .
	 * 
	 * @param paiement le paiement
	 * @paral estCotisationASolidarite indicateur pour code cotisation
	 * @return le flux formaté pour Excel
	 */	
	HSSFWorkbook getFluxFormatExcel(EOPaiement paiement, boolean estCotisationASolidarite);
		
}
