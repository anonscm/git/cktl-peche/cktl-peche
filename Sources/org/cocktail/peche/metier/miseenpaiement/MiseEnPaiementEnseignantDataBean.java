package org.cocktail.peche.metier.miseenpaiement;

import java.math.BigDecimal;
import java.util.Comparator;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOMiseEnPaiement;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/**
 * Cette classe représente les mise en paiement pour un enseignant et une structure (celle qui paye).
 */
public class MiseEnPaiementEnseignantDataBean implements NSKeyValueCoding {
	/** Est-ce que cette mise en paiement trouvée doit être mise ne paiement ? */
	private boolean bonPourPaiement = true;
	/** L'enseignant */
	private EOActuelEnseignant enseignant;
	/** La structure qui paye */
	private EOStructure structurePayante;
	/** La liste des mises en paiement */
	private NSArray<EOMiseEnPaiement> listeMiseEnPaiement = new NSMutableArray<EOMiseEnPaiement>();;
	
	public boolean isBonPourPaiement() {
		return bonPourPaiement;
	}

	public void setBonPourPaiement(boolean bonPourPaiement) {
		this.bonPourPaiement = bonPourPaiement;
	}

	public EOActuelEnseignant getEnseignant() {
		return enseignant;
	}
	
	public void setEnseignant(EOActuelEnseignant enseignant) {
		this.enseignant = enseignant;
	}
	
	public EOStructure getStructurePayante() {
		return structurePayante;
	}

	public void setStructurePayante(EOStructure structurePayante) {
		this.structurePayante = structurePayante;
	}

	public NSArray<EOMiseEnPaiement> getListeMiseEnPaiement() {
		return listeMiseEnPaiement;
	}
	
	public void setListeMiseEnPaiement(NSArray<EOMiseEnPaiement> listeMiseEnPaiement) {
		this.listeMiseEnPaiement = listeMiseEnPaiement;
	}
	
	/**
	 * Retourne le nombre d'heures à payer (le total des à payer des mises en paiement).
	 * @return le nombre d'heures à payer
	 */
	public BigDecimal getTotalNombreHeuresAPayer() {
		BigDecimal totalNombreHeuresAPayer = BigDecimal.ZERO;
		
		for (EOMiseEnPaiement miseEnPaiement : getListeMiseEnPaiement()) {
			totalNombreHeuresAPayer = totalNombreHeuresAPayer.add(miseEnPaiement.heuresAPayer());
		}
		
		return totalNombreHeuresAPayer;
	}

	/**
	 * Retourne le nombre d'heures à payer (le total des à payer des mises en paiement).
	 * @return le nombre d'heures à payer
	 */
	public BigDecimal getTotalNombreHeuresAPayerHetd() {
		BigDecimal totalNombreHeuresAPayerHetd = BigDecimal.ZERO;
		
		for (EOMiseEnPaiement miseEnPaiement : getListeMiseEnPaiement()) {
			totalNombreHeuresAPayerHetd = totalNombreHeuresAPayerHetd.add(miseEnPaiement.heuresAPayerHetd());
		}
		
		return totalNombreHeuresAPayerHetd;
	}
	
	/**
	 * Retourne <code>true</code> si la mise en paiement peut être associée à ce data bean (même enseignant et même structure).
	 * @param miseEnPaiement une mise en paiement
	 * @return <code>true</code> si la mise en paiement peut être associée à ce data bean
	 */
	public boolean isMemeDataBean(EOMiseEnPaiement miseEnPaiement) {
		EOActuelEnseignant enseignant2 = miseEnPaiement.toService().enseignant();
		String cStructurePayante = "";
		
		if (structurePayante != null) {
			cStructurePayante = structurePayante.cStructure();
		}
		
		String cStructureMiseEnPaiement = "";
		
		if (miseEnPaiement.toStructure() != null) {
			cStructureMiseEnPaiement = miseEnPaiement.toStructure().cStructure();
		}
		
		if (enseignant.noDossierPers().equals(enseignant2.noDossierPers())
				&& cStructurePayante.equals(cStructureMiseEnPaiement)) {
			return true;
		}
		
		return false;
	}

	public void takeValueForKey(Object value, String key) {		
		NSKeyValueCoding.DefaultImplementation.takeValueForKey(this, value, key);		
	}

	public Object valueForKey(String key) {
		return NSKeyValueCoding.DefaultImplementation.valueForKey(this, key);
	}
	
	/**
	 * Comparator pour le tri des {@link MiseEnPaiementEnseignantDataBean} par nom affichage, prénom affichage et libellé court de la structure qui paye.
	 */
	public static class TriParEnseignantStructure implements Comparator<MiseEnPaiementEnseignantDataBean> {
		/**
		 * {@inheritDoc}
		 */
		public int compare(MiseEnPaiementEnseignantDataBean o1, MiseEnPaiementEnseignantDataBean o2) {
			int resultat = o1.enseignant.toIndividu().nomAffichage().compareToIgnoreCase(o2.enseignant.toIndividu().nomAffichage());
			
			if (resultat == 0) {
				resultat = o1.enseignant.toIndividu().prenomAffichage().compareTo(o2.enseignant.toIndividu().prenomAffichage());
			}

			if (resultat == 0) {
				if (o1.structurePayante == null && o2.structurePayante == null) {
					resultat = 0;
				} else if (o1.structurePayante == null && o2.structurePayante != null) {
					resultat = -1;
				} else if (o1.structurePayante != null && o2.structurePayante == null) {
					resultat = +1;
				} else {
					resultat = o1.structurePayante.lcStructure().compareTo(o2.structurePayante.lcStructure());
				}
			}
			
			return resultat;
		}
	}
}
