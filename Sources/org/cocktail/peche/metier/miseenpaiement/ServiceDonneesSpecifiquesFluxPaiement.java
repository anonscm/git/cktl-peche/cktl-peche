package org.cocktail.peche.metier.miseenpaiement;

import org.cocktail.peche.entity.EODefinitionFluxPaiement;
import org.cocktail.peche.entity.EODetailFluxPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

import com.webobjects.foundation.NSArray;

/**
 * 
 * @author juliencallewaert
 *
 */
public class ServiceDonneesSpecifiquesFluxPaiement {

	/**
	 * Méthode recherchant la valeur d'un champ dans EODetailFluxPaiement selon le 
	 * code défini dans EODefinitionFluxPaiement 
	 * @param parametresFluxPaiement
	 * @param codeChamp
	 * @return
	 */
	public String recupererValeurSelonCodeChamp(EOParametresFluxPaiement parametresFluxPaiement, String codeChamp) {
		
		NSArray<EODetailFluxPaiement> listeDetailFluxPaiement = parametresFluxPaiement.toListeDetailFluxPaiement();
		if (listeDetailFluxPaiement != null) {
			for (EODetailFluxPaiement detailFluxPaiement : listeDetailFluxPaiement) {
				EODefinitionFluxPaiement definitionFluxPaiement = detailFluxPaiement.toDefinitionFluxPaiement();
				if (codeChamp.equals(definitionFluxPaiement.codeChamp())) {
					return detailFluxPaiement.valeurChamp();
				}			
			}
		}
		return null;		
	}
	
	
}
