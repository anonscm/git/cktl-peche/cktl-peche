package org.cocktail.peche.metier.miseenpaiement;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeCode;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeContrat;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeFonction;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeMois;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeParamPerso;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePeriode;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayePerso;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeRubrique;
import org.cocktail.fwkcktlgrh.common.metier.paye.EOPayeStatut;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.peche.Application;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe formate le flux de mise en paiement envoyé directement à Papaye.
 * 
 * @author Laurent PRINGOT
 */
public class FormatFluxPaiementPapaye extends FormatFluxPaiementSansEntete {

	@Inject
	private IPecheParametres pecheParametres;

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOSortOrdering> getTri() {
		return EOFluxMiseEnPaiement.NUMERO_INSEE.ascs();
	}	

	/**
	 * {@inheritDoc}
	 */
	public String getExtensionNomFlux() {
		return ".papaye";
	}

	/**
	 * {@inheritDoc}
	 * @throws Exception 
	 */
	@Override
	public String getLigneDetail(EOFluxMiseEnPaiement fluxMiseEnPaiement, boolean estCotisationASolidarite) throws Exception {		

		pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);

		EOEditingContext edc = fluxMiseEnPaiement.toService().editingContext();
		EOActuelEnseignant enseignant = fluxMiseEnPaiement.toService().enseignant();

		Calendar dateDebut = Calendar.getInstance();
		Calendar dateFin = Calendar.getInstance();

		if (Constante.OUI.equals(fluxMiseEnPaiement.moisSuivantpourTG())) {
			dateDebut.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
			dateDebut.set(Calendar.DAY_OF_MONTH, 1); //on va au 1er jour du mois
			dateDebut.add(Calendar.MONTH, 1); // on ajoute 1 mois		
			dateFin.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
			dateFin.set(Calendar.DAY_OF_MONTH, 1); //on va au 1er jour du mois
			dateFin.add(Calendar.MONTH, 2); // on ajoute 2 mois
			dateFin.add(Calendar.DAY_OF_MONTH, -1); //on enlève un jour
		} else {
			dateDebut.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
			dateDebut.set(Calendar.DAY_OF_MONTH, 1); //on va au 1er jour du mois
			dateFin.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
			dateFin.set(Calendar.DAY_OF_MONTH, 1); //on va au 1er jour du mois
			dateFin.add(Calendar.MONTH, 1); // on ajoute 1 mois
			dateFin.add(Calendar.DAY_OF_MONTH, -1); //on enlève un jour
		}

		NSTimestamp contrat_dateDebut = new NSTimestamp(dateDebut.getTime());
		NSTimestamp contrat_dateFin = new NSTimestamp(dateFin.getTime());

		String Statut_psta_ordre;
		String rubrique_prub_ordre_ContribSolidarite;
		String rubrique_prub_ordre_HCompl;
		String codeNbHeures;
		String codeTauxHoraire;
		String fonction;
		String secteur;
	//	String codeTypeContratTravail;


		// récupération de toutes les variables

		EOStructure composante = enseignant.getComposante(fluxMiseEnPaiement.toPaiement().anneePaiement(),
				pecheParametres.isFormatAnneeExerciceAnneeCivile());


		if (enseignant.isStatutaire(fluxMiseEnPaiement.toPaiement().anneePaiement(),
				pecheParametres.isFormatAnneeExerciceAnneeCivile())) {

			//contrat_NbHeuresContrat = new BigDecimal(enseignant.getNbHeuresServiceStatutaire(fluxMiseEnPaiement.toPaiement().anneePaiement(), pecheParametres.isFormatAnneeExerciceAnneeCivile()));

			if (enseignant.isTitulaire(fluxMiseEnPaiement.toPaiement().anneePaiement(),
					pecheParametres.isFormatAnneeExerciceAnneeCivile())) {
				Statut_psta_ordre = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PSTA_ORDRE_STAT.getCodeChamp());				
				rubrique_prub_ordre_ContribSolidarite = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_CONTRIBSOLI_STAT.getCodeChamp());
				rubrique_prub_ordre_HCompl = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_HCOMPL_STAT.getCodeChamp());
				codeNbHeures = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_NBHEURES_STAT.getCodeChamp());
				codeTauxHoraire = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_TXHOR_STAT.getCodeChamp());
				fonction = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PFCT_ORDRE_STAT.getCodeChamp());
			//	codeTypeContratTravail = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PCT_CONTRAT_TRAV_STAT.getCodeChamp());
			} else {
				Statut_psta_ordre = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PSTA_ORDRE_CONT.getCodeChamp());
				rubrique_prub_ordre_ContribSolidarite = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_CONTRIBSOLI_CONT.getCodeChamp());
				rubrique_prub_ordre_HCompl = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_HCOMPL_CONT.getCodeChamp());
				codeNbHeures = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_NBHEURES_CONT.getCodeChamp());
				codeTauxHoraire = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_TXHOR_CONT.getCodeChamp());
				fonction = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PFCT_ORDRE_CONT.getCodeChamp());
			//	codeTypeContratTravail = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PCT_CONTRAT_TRAV_CONT.getCodeChamp());
			}
		} else {

			//	EnseignantsVacatairesCtrl enseignantsVacatairesCtrl = new EnseignantsVacatairesCtrl(edc, fluxMiseEnPaiement.toPaiement().anneePaiement(), pecheParametres.isFormatAnneeExerciceAnneeCivile());

			//contrat_NbHeuresContrat = new BigDecimal(enseignantsVacatairesCtrl.getTotalNbHeuresVacation(enseignant));

			List<IVacataires> listeVacations = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(fluxMiseEnPaiement.toPaiement().anneePaiement(),
					pecheParametres.isFormatAnneeExerciceAnneeCivile());
			// nous prenons les dates du 1er contrat de vacation
			// sur l'annnée universitaire
			if (listeVacations.size() > 0) {				
				contrat_dateDebut = listeVacations.get(0).getDateDebut();
				contrat_dateFin = listeVacations.get(0).getDateFin();
			}

			Statut_psta_ordre = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PSTA_ORDRE_VACAT.getCodeChamp());
			rubrique_prub_ordre_ContribSolidarite = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_CONTRIBSOLI_VACAT.getCodeChamp());
			rubrique_prub_ordre_HCompl = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PRUB_ORDRE_HCOMPL_VACAT.getCodeChamp());
			codeNbHeures = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_NBHEURES_VACAT.getCodeChamp());
			codeTauxHoraire = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_CODE_TXHOR_VACAT.getCodeChamp());
			fonction = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PFCT_ORDRE_VACAT.getCodeChamp());
			// codeTypeContratTravail = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PCT_CONTRAT_TRAV_VACAT.getCodeChamp());			
		}


		secteur = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.PAPAYE_PSEC_ORDRE.getCodeChamp());
		CktlLog.log("Papaye: secteur:" + secteur);
		// MOIS

		EOPayeMois payeMois = EOPayeMois.fetch(edc, EOPayeMois.MOIS_CODE.eq(Integer.valueOf(getDatePaye(fluxMiseEnPaiement))));

		if (payeMois == null) {
			CktlLog.log("Papaye: Impossible de recuperer le mois de paye");
			throw new IllegalArgumentException("Impossible de recuperer le mois de paye");			
		}
		CktlLog.log("Papaye: payeMois:" + payeMois.moisLibelle());

		// CONTRAT
		EOPayeContrat payeContratExistant = null;
		String contrat_temAnnulation = "N";
		String contrat_temPaiementPonctuel = "O";
		String contrat_temPayeLocale = "O";
		String contrat_temPayeUtile = "O";
		String contrat_temPreContrat = "N";
		String contrat_temTempsPlein = "N";
		BigDecimal contrat_NbHeuresContrat = null;		
		BigDecimal contrat_NbJoursContrat = null;
		String contrat_CEchelon = null;
		String contrat_ClassificationEmploi = null;
		String contrat_CodeStatutCategoriel = null;
		String contrat_IndiceContrat = null;
		String contrat_IndiceOrigine = null;

		//PERIODE
		EOPayePeriode periodeExistante = null;
		String periode_temComplet = "O";
		String periode_temPositive = "O";
		String periode_temPremierePaye = "N";
		String periode_temValide = "O";

		// si contrat existant le même mois, alors on reprend le contrat
		// et la période
		EOQualifier qualifierContrat = EOPayeContrat.INDIVIDU.eq(enseignant.toIndividu()).and(
				EOPayeContrat.PERIODES.dot(EOPayePeriode.MOIS_KEY).eq(payeMois));
		NSArray<EOPayeContrat> payeContrats = EOPayeContrat.fetch(edc, qualifierContrat, null);
		CktlLog.log("Papaye: payeContrats trouvés :" + payeContrats.size());
		if (payeContrats.size() > 0) {
			CktlLog.log("Papaye: payeContrats réutilisation des données");
			payeContratExistant = payeContrats.get(0);
			NSArray<EOPayePeriode> periodes = payeContrats.get(0).periodes();
			for (EOPayePeriode periode : periodes) {
				if (periode.moisTraitement() == payeMois) {
					periodeExistante = periode;										
				}
			}			
		}

	/*	EOTypeContratTravail typeContratTravail = EOTypeContratTravail.fetchByKeyValue(edc, EOTypeContratTravail.C_TYPE_CONTRAT_TRAV_KEY, codeTypeContratTravail);

		if (typeContratTravail == null) {
			CktlLog.log("Papaye: Impossible de recuperer le type de contrat suivant le code " + codeTypeContratTravail);
			throw new IllegalArgumentException("Impossible de recuperer le type de contrat suivant le code " + codeTypeContratTravail);			
		}

		CktlLog.log("Papaye: typeContratTravail:" + typeContratTravail.cTypeContratTrav());
*/
		EOPayeContrat payeContrat = null;
		if (payeContratExistant == null) {

			/*payeContrat = EOPayeContrat.create(edc,
					new NSTimestamp(), contrat_dateDebut, new NSTimestamp(),
					contrat_temAnnulation, contrat_temPaiementPonctuel, contrat_temPayeLocale, contrat_temPayeUtile, contrat_temPreContrat, contrat_temTempsPlein,
					enseignant.toIndividu(), typeContratTravail);*/

			payeContrat = EOPayeContrat.create(edc,
					new NSTimestamp(), contrat_dateDebut, new NSTimestamp(),
					contrat_temAnnulation, contrat_temPaiementPonctuel, contrat_temPayeLocale, contrat_temPayeUtile, contrat_temPreContrat, contrat_temTempsPlein,
					enseignant.toIndividu());

			
			if (payeContrat == null) {
				CktlLog.log("Papaye: création impossible de payeContrat");
				throw new Exception("création impossible de payeContrat");			
			}

			payeContrat.setModeCalcul("D");
			payeContrat.setNbHeuresContrat(contrat_NbHeuresContrat);
			payeContrat.setNbJoursContrat(contrat_NbJoursContrat);
			payeContrat.setDFinContratTrav(contrat_dateFin);
			payeContrat.setCEchelon(contrat_CEchelon);
			payeContrat.setClassificationEmploi(contrat_ClassificationEmploi);
			payeContrat.setCodeStatutCategoriel(contrat_CodeStatutCategoriel);
			payeContrat.setIndiceContrat(contrat_IndiceContrat);
			payeContrat.setIndiceOrigine(contrat_IndiceOrigine);
			

			EOPayeFonction payeFonction = EOPayeFonction.fetch(edc, EOPayeFonction.FONC_ORDRE.eq(Integer.valueOf(fonction)));

			if (payeFonction == null) {
				CktlLog.log("Papaye: Impossible de recuperer la fonction suivant le code " + fonction);
				throw new IllegalArgumentException("Impossible de recuperer la fonction suivant le code " + fonction);			
			}

			payeContrat.setFonctionRelationship(payeFonction);

			EOStructure etablissement = EOStructure.rechercherEtablissement(edc);
			CktlLog.log("Papaye: etablissement:" + etablissement);
			
			if (composante == null && etablissement != null) {
				payeContrat.setStructureRelationship(etablissement);				
			}
			
			if (etablissement != null) {
				payeContrat.setStructureSiretRelationship(etablissement);
				if ((etablissement.temSectorise() != null) 
						&& "O".equals(etablissement.temSectorise()) 
						&& (secteur != null)) {
					payeContrat.setPsecOrdre(Integer.valueOf(secteur));
				}
			} else {
				payeContrat.setPsecOrdre(null);
			}

			if (estCotisationASolidarite) {
				payeContrat.setTemCotiseSolidarite("O");
			} else {
				payeContrat.setTemCotiseSolidarite("N");
			}
		} else {
			payeContrat = payeContratExistant;
		}
		CktlLog.log("Papaye: payeContrat:" + payeContrat);

		// STATUT
		if (payeContratExistant == null) {
			// si contrat existant : on ne touche pas au statut
			NSArray<EOPayeStatut> statuts = EOPayeStatut.fetchAll(edc);
			EOPayeStatut statutRemun = null;

			for (EOPayeStatut statut : statuts) {
				if (statut.primaryKey().equals(Statut_psta_ordre)) {				
					statutRemun = statut;
				}			
			}
			if (statutRemun == null) {
				CktlLog.log("Papaye: Impossible de recuperer le statut de paye pour la valeur " + Statut_psta_ordre);
				throw new IllegalArgumentException("Impossible de recuperer le statut de paye pour la valeur " + Statut_psta_ordre);			
			}

			CktlLog.log("Papaye: statut:" + statutRemun.pstaLibelle());

			payeContrat.setStatutRelationship(statutRemun);
		}

		if (periodeExistante == null) {
			EOPayePeriode payePeriode = EOPayePeriode.create(edc, new BigDecimal(0), new BigDecimal(30), periode_temComplet, periode_temPositive, periode_temPremierePaye, periode_temValide, payeContrat, payeMois, payeMois);
			
			if (payePeriode == null) {
				CktlLog.log("Papaye: création impossible de payePeriode");
				throw new Exception("création impossible de payePeriode");			
			}
			payeContrat.addToPeriodesRelationship(payePeriode);

			CktlLog.log("Papaye: payePeriode:" + payePeriode);

		} else {
			payeContrat.addToPeriodesRelationship(periodeExistante);
		}

		// recherches rubriques

		NSArray<EOPayeRubrique> rubriques = EOPayeRubrique.fetchAll(edc);
		EOPayeRubrique rubContribSolidarite = null;
		EOPayeRubrique rubHCompl = null;

		for (EOPayeRubrique rubrique : rubriques) {
			if (rubrique.primaryKey().equals(rubrique_prub_ordre_ContribSolidarite)) {
				rubContribSolidarite = rubrique;
			}
			if (rubrique.primaryKey().equals(rubrique_prub_ordre_HCompl)) {
				rubHCompl = rubrique;
			}
		}

		if (rubContribSolidarite == null) {
			CktlLog.log("Papaye: Impossible de recuperer la rubrique Contribution Solidarité pour la valeur " + rubrique_prub_ordre_ContribSolidarite);
			throw new IllegalArgumentException("Impossible de recuperer la rubrique Contribution Solidarité pour la valeur " + rubrique_prub_ordre_ContribSolidarite);			
		}

		if (rubHCompl == null) {
			CktlLog.log("Papaye: Impossible de recuperer la rubrique Heures Complémentaires pour la valeur " + rubrique_prub_ordre_HCompl);
			throw new IllegalArgumentException("Impossible de recuperer la rubrique Heures Complémentaires pour la valeur " + rubrique_prub_ordre_HCompl);			
		}
		CktlLog.log("Papaye: rubrique ContribSolidarite:" + rubContribSolidarite.prubLibelle() + "(" + rubContribSolidarite.primaryKey() + ")");
		CktlLog.log("Papaye: rubrique HCompl:" + rubHCompl.prubLibelle() + "(" + rubHCompl.primaryKey() + ")");

		// PERSO SOLIDARITE
		String perso_temPermanent = "N";
		String perso_temValide = "O";
		EOPayePerso payeContriSolidarite = null;
		if (payeContratExistant == null) {
			// si contrat existant : on ne touche pas à la rubrique contrib solidarite

			if (estCotisationASolidarite) {			 
				payeContriSolidarite = EOPayePerso.create(edc, rubContribSolidarite.prubLibelleImp(), perso_temPermanent, perso_temValide, payeContrat, rubContribSolidarite);
				if (payeContriSolidarite == null) {
					CktlLog.log("Papaye: création impossible de payePerso Contribution Solidarite");
					throw new Exception("création impossible de payePerso Contribution Solidarite");			
				}
				CktlLog.log("Papaye: ajout payePerso contriSolidarite:" + payeContriSolidarite);			
			}
		}

		// PERSO HEURES 		
		EOPayePerso payeHCompl = EOPayePerso.create(edc, rubHCompl.prubLibelleImp(), perso_temPermanent, perso_temValide, payeContrat, rubHCompl);
		if (payeHCompl == null) {
			CktlLog.log("Papaye: création impossible de payePerso Heures Complémentaires");
			throw new Exception("création impossible de payePerso Heures Complémentaires");			
		}
		CktlLog.log("Papaye: ajout payePerso Hcompl:" + payeHCompl);

		// PARAM_PERSO NB HEURES + TAUX HORAIRE
		String paramPerso_temValide = "O";
		EOPayeCode codeNbH = EOPayeCode.fetch(edc, EOPayeCode.PCOD_CODE.eq(codeNbHeures));
		if (codeNbH == null) {
			CktlLog.log("Papaye: Impossible de recuperer le code Paye NbHeures pour la valeur " + codeNbHeures);
			throw new IllegalArgumentException("Impossible de recuperer le code Paye NbHeures pour la valeur " + codeNbHeures);			
		}
		CktlLog.log("Papaye: payeCode NbHeures:" + codeNbH);

		EOPayeParamPerso payeParamPersoHeures = EOPayeParamPerso.create(edc, codeNbH.pcodLibelle(),
				Integer.valueOf(getDatePaye(fluxMiseEnPaiement)), Integer.valueOf("300000"), fluxMiseEnPaiement.heuresPayees().toString(), paramPerso_temValide, codeNbH, payeHCompl);
		if (payeParamPersoHeures == null) {
			CktlLog.log("Papaye: création impossible de payeParamPerso Heures");
			throw new Exception("création impossible de payeParamPerso Heures");			
		}
		payeParamPersoHeures.setCodeRelationship(codeNbH);		
		CktlLog.log("Papaye: ajout payeParamPerso nbHeures:" + payeParamPersoHeures);

		EOPayeCode codeTauxH = EOPayeCode.fetch(edc, EOPayeCode.PCOD_CODE.eq(codeTauxHoraire));
		if (codeTauxH == null) {
			CktlLog.log("Papaye: Impossible de recuperer le code Paye TauxHoraire pour la valeur " + codeTauxHoraire);
			throw new IllegalArgumentException("Impossible de recuperer le code Paye TauxHoraire pour la valeur " + codeTauxHoraire);			
		}
		CktlLog.log("Papaye: payeCode tauxHoraire:" + codeTauxH);

		EOPayeParamPerso payeParamPersoTaux = EOPayeParamPerso.create(edc, codeTauxH.pcodLibelle(),
				Integer.valueOf(getDatePaye(fluxMiseEnPaiement)), Integer.valueOf("300000"), fluxMiseEnPaiement.tauxNonCharge().toString(), "O", codeTauxH, payeHCompl);
		if (payeParamPersoTaux == null) {
			CktlLog.log("Papaye: création impossible de payeParamPerso Taux");
			throw new Exception("création impossible de payeParamPerso Taux");			
		}
		payeParamPersoTaux.setCodeRelationship(codeTauxH);
		CktlLog.log("Papaye: ajout payeParamPerso tauxHoraire:" + payeParamPersoTaux);

		return "";
	}

	public boolean isFormatExcel() {
		return false;
	}

	public HSSFWorkbook getFluxFormatExcel(EOPaiement paiement,
			boolean estCotisationASolidarite) {
		// TODO Auto-generated method stub
		return null;
	}
}
