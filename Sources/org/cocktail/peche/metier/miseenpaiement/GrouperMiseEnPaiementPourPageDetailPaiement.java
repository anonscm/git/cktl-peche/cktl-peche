package org.cocktail.peche.metier.miseenpaiement;

import org.cocktail.peche.entity.EOMiseEnPaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Groupage des mises en paiement pour la liste des enseignants de la page détail d'un paiement.
 */
public class GrouperMiseEnPaiementPourPageDetailPaiement implements IGrouperMiseEnPaiement<EOMiseEnPaiement> {
	
	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOSortOrdering> getTri() {
		return null;
		/*return EOMiseEnPaiement.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE).ascInsensitive()
				.then(EOMiseEnPaiement.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.PRENOM_AFFICHAGE).asc())
				.then(EOMiseEnPaiement.TO_SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU).asc())
				.then(EOMiseEnPaiement.TO_SERVICE.dot(EOService.ID).asc())
				.then(EOMiseEnPaiement.TO_STRUCTURE.dot(EOStructure.C_STRUCTURE).asc())
				.then(EOMiseEnPaiement.TAUX_BRUT.asc());*/
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isRupture(EOMiseEnPaiement miseEnPaiementPrecedente, EOMiseEnPaiement miseEnPaiementEnCours) {
		return !miseEnPaiementPrecedente.toService().primaryKey().equals(miseEnPaiementEnCours.toService().primaryKey())
				|| !miseEnPaiementPrecedente.toStructure().primaryKey().equals(miseEnPaiementEnCours.toStructure().primaryKey());
	}

}
