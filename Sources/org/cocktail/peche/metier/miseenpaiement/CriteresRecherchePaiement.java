package org.cocktail.peche.metier.miseenpaiement;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.peche.components.controlers.BasicPecheCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EORepartService;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Cette classe représente les critères de recherche des heures à payer.
 * 
 * @author Pascal MACOUIN
 */
public class CriteresRecherchePaiement {
	/** Faut-il rechercher les enseignants statutaires ? */
	private boolean enseignantsStatutaire;
	/** Faut-il rechercher les enseignants vacataires ? */
	private boolean enseignantsVacataire;
	/** Filtre sur ces structures (facultatif). */
	private List<IStructure> listeStructure;
	private Set<String> setCodeStructure = new HashSet<String>();
	/** Filtre sur l'année universitaire (obligatoire). */
	private int anneeUniversitaire;
	/** Filtre sur la période (obligatoire). */
	private EOPeriode periode;
	/** Filtre sur le corps (facultatif). */
	private EOCorps corps;
	/** Filtre sur le type de contrat (facultatif). */
	private EOTypeContratTravail typeContrat;
	
	public CriteresRecherchePaiement()	{
	}
	
	public boolean isEnseignantsStatutaire() {
		return enseignantsStatutaire;
	}

	public void setEnseignantsStatutaire(boolean enseignantsStatutaire) {
		this.enseignantsStatutaire = enseignantsStatutaire;
	}

	public boolean isEnseignantsVacataire() {
		return enseignantsVacataire;
	}

	public void setEnseignantsVacataire(boolean enseignantsVacataire) {
		this.enseignantsVacataire = enseignantsVacataire;
	}

	/**
	 * Retourne la valeur pour le filtrage SQL (vacataire/statutaire).
	 * @return "O" si filtrage statutaire, "N" si filtrage vacataire, <code>null</code> si pas de filtrage
	 */
	public String getValeurFiltreTemoinStatutaire() {
		if (enseignantsStatutaire == enseignantsVacataire) {
			return null;
		} else if (enseignantsStatutaire) {
			return "O";
		} else {
			return "N";
		}
	}
	
	public List<IStructure> getListeStructure() {
		return listeStructure;
	}

	/**
	 * Affecte la liste des structures à filtrer.
	 * @param listeStructure une liste de structures
	 */
	public void setListeStructure(List<IStructure> listeStructure) {
		this.listeStructure = listeStructure;
		
		setCodeStructure.clear();
		
		if (CollectionUtils.isNotEmpty(listeStructure)) {
			for (IStructure structure : listeStructure) {
				setCodeStructure.add(structure.cStructure());
			}
		}
	}

	public int getAnneeUniversitaire() {
		return anneeUniversitaire;
	}

	public void setAnneeUniversitaire(int anneeUniversitaire) {
		this.anneeUniversitaire = anneeUniversitaire;
	}

	public EOPeriode getPeriode() {
		return periode;
	}

	public void setPeriode(EOPeriode periode) {
		this.periode = periode;
	}

	public EOCorps getCorps() {
		return corps;
	}

	public void setCorps(EOCorps corps) {
		this.corps = corps;
	}

	public EOTypeContratTravail getTypeContrat() {
		return typeContrat;
	}

	public void setTypeContrat(EOTypeContratTravail typeContrat) {
		this.typeContrat = typeContrat;
	}

	/**
	 * Retourne <code>true</code> si les critères non codable en SQL sont satisfaits.
	 * 
	 * @param serviceDetail un service détail
	 * @param structureComposant la structure du composant
	 * @return <code>true</code> si les critères non codable en SQL sont satisfaits
	 */
	public boolean isCriteresAuxiliairesSatisfaits(EOEditingContext ec, EOServiceDetail serviceDetail, EOStructure structureComposant) {
		boolean criteresAuxiliairesSatisfaits = isCriteresAuxiliairesSatisfaits(ec, serviceDetail.toRepartService());
		// Critère structure
		if (criteresAuxiliairesSatisfaits && getListeStructure() != null) {
			if (structureComposant == null) {
				criteresAuxiliairesSatisfaits = false;
			} else if (!setCodeStructure.contains(structureComposant.cStructure())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}
		
		return criteresAuxiliairesSatisfaits;
	}
	
	/**
	 * Retourne <code>true</code> si les critères non codable en SQL sont satisfaits.
	 * 
	 * @param service : le service 
	 * @return <code>true</code> si les critères non codable en SQL sont satisfaits
	 */
	public boolean isCriteresAuxiliairesSatisfaits(EOEditingContext ec, EOService service) {
		boolean criteresAuxiliairesSatisfaits = true;
		
		EOActuelEnseignant enseignant = service.enseignant();
		
		BasicPecheCtrl controller = new BasicPecheCtrl(ec);
		
		// La fiche de service doit être sur le circuit définitif et sur l'étape "Validée"
		CircuitPeche circuit = CircuitPeche.getCircuit(controller.getDemande(service).toEtape().toCircuitValidation());
		EtapePeche etape = EtapePeche.getEtape(controller.getDemande(service).toEtape());
		
		if (circuit == null || !circuit.isCircuitDefinitif()
				|| etape == null || !etape.getCodeEtape().equals(EtapePeche.VALIDEE.getCodeEtape())) {
			criteresAuxiliairesSatisfaits = false;
		}
		
		// Critère corps
		if (criteresAuxiliairesSatisfaits && getCorps() != null) {
			EOCorps corpsEnseignant = enseignant.getCorps(service.annee(), controller.isFormatAnneeExerciceAnneeCivile());
			
			if (corpsEnseignant == null) {
				criteresAuxiliairesSatisfaits = false;
			} else if (!getCorps().code().equals(corpsEnseignant.code())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}

		// Critères type de contrat
		if (criteresAuxiliairesSatisfaits && getTypeContrat() != null) {
			if (!enseignant.getListeTypesContrat(service.annee(), controller.isFormatAnneeExerciceAnneeCivile()).contains(getTypeContrat())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}
		
		return criteresAuxiliairesSatisfaits;
	}	
	
	public boolean isCriteresAuxiliairesSatisfaits(EOEditingContext ec, EORepartService repartService) {
		boolean criteresAuxiliairesSatisfaits = true;
		
		EOActuelEnseignant enseignant = repartService.toService().enseignant();
		
		BasicPecheCtrl controller = new BasicPecheCtrl(ec);
		
		// La fiche de service doit être sur le circuit définitif et sur l'étape "Validée"
		CircuitPeche circuit = CircuitPeche.getCircuit(repartService.toDemande().toEtape().toCircuitValidation());
		EtapePeche etape = EtapePeche.getEtape(repartService.toDemande().toEtape());
		
		if (circuit == null || !circuit.isCircuitDefinitif()
				|| etape == null || !etape.getCodeEtape().equals(EtapePeche.VALIDEE.getCodeEtape())) {
			criteresAuxiliairesSatisfaits = false;
		}
		
		// Critère corps
		if (criteresAuxiliairesSatisfaits && getCorps() != null) {
			EOCorps corpsEnseignant = enseignant.getCorps(repartService.toService().annee(), controller.isFormatAnneeExerciceAnneeCivile());
			
			if (corpsEnseignant == null) {
				criteresAuxiliairesSatisfaits = false;
			} else if (!getCorps().code().equals(corpsEnseignant.code())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}

		// Critères type de contrat pour vacataires
		if (criteresAuxiliairesSatisfaits && getTypeContrat() != null && isEnseignantsVacataire()) {
			criteresAuxiliairesSatisfaits = false;
			List<IVacataires> liste = enseignant.getListeVacationsCourantesPourAnneeUniversitaire(getAnneeUniversitaire(), controller.isFormatAnneeExerciceAnneeCivile());
			if (!liste.isEmpty()) {
				// On prend la 1ère vacation
				// TODO:ATTENTION A REVOIR lors de la mise en place de la gestion des multi-contrats
				EOTypeContratTravail typeContratTravail = liste.get(0).getToTypeContratTravail();
				if (typeContratTravail != null 
						&& typeContratTravail.cTypeContratTrav().equals(getTypeContrat().cTypeContratTrav())) {
					criteresAuxiliairesSatisfaits = true;
				}				
			}
		}

		// Critères type de contrat pour statutaires
		if (criteresAuxiliairesSatisfaits && getTypeContrat() != null && isEnseignantsStatutaire()) {
			if (!enseignant.getListeTypesContrat(repartService.toService().annee(), controller.isFormatAnneeExerciceAnneeCivile()).contains(getTypeContrat())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}
		
		return criteresAuxiliairesSatisfaits;
	}	
	/**
	 * Retourne <code>true</code> si les critères non codable en SQL sont satisfaits.
	 * 
	 * @param service un service 
	 * @param structureAffectation la structure de l'enseignant
	 * @return <code>true</code> si les critères non codable en SQL sont satisfaits
	 */
	public boolean isCriteresAuxiliairesSatisfaits(EOEditingContext ec, EOService service, EOStructure structureAffectation) {
		boolean criteresAuxiliairesSatisfaits = isCriteresAuxiliairesSatisfaits(ec, service);
						
		// Critère structure
		if (criteresAuxiliairesSatisfaits && getListeStructure() != null) {
			if (structureAffectation == null) {
				criteresAuxiliairesSatisfaits = false;
			} else if (!setCodeStructure.contains(structureAffectation.cStructure())) {
				criteresAuxiliairesSatisfaits = false;
			}
		}
		
		return criteresAuxiliairesSatisfaits;
	}

}
