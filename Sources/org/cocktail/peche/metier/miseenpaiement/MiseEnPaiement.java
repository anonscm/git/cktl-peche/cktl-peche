package org.cocktail.peche.metier.miseenpaiement;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonnel;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.peche.Application;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.ComposantCtrl;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.entity.EOTauxHoraire;
import org.cocktail.peche.metier.hce.MoteurDeCalcul;
import org.cocktail.peche.services.IPeriodeService;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXAndQualifier;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * Cette classe permet de gérer toute la mise en paiement. C'est un singleton.
 * <p>
 * Une fois les heures à payer saisies sur les fiches de service
 * (au niveau de {@link EOService} pour les HComp ou au niveau de {@link EOServiceDetail} pour les AP),
 * la mise en paiement se déroule comme suit :
 * <ol>
 * <li>On met en paiement des fiches de service à payer ({@link #mettreEnPaiement(EOService, EOPersonne)}).
 *     Cette mise en paiement peut être étalée dans le temps.
 *     On peut mettre en paiement une fiche déjà en paiement, cela annule automatiquement la mise en paiement précédente
 *     pour en générer une nouvelle qui cumule les deux mises en paiement ;</li>
 * <li>On peut annuler la mise en paiement à tort d'une fiche de service ({@link #annulerMiseEnPaiementEnAttente(EOService)}) ;</li>
 * <li>On genère un paiement ({@link #genererPaiement(EOEditingContext, int, int, int, EOPersonne)}).
 *     Cette génération fige les mises en paiement qui étaient en attente.
 *     Les mises en paiement suivantes feront l'objet d'un autre paiement.
 *     Les paiements de ce paiement ne peuvent plus être annulé avec la méthode ({@link #annulerMiseEnPaiementEnAttente(EOService)}) ;</li>
 * <li>On peut invalider un paiement (le supprimer complètement) ({@link #invaliderPaiement(EOPaiement, EOPersonne)}).
 *     Les paiements de ce paiement sont remis en attente ;</li>
 * <li>On peut enlever une fiche de service des paiements d'un paiement ({@link #annulerMiseEnPaiementduPaiement(EOPaiement, EOService)}) ;</li>
 * <li>On valide le paiement ({@link #validerPaiement(EOPaiement, EOPersonne)}).
 *     Une fois fait, ce paiement est considéré comme payé et ne peut plus être modifié ni supprimé ;</li>
 * <li>On génère un flux de données pour intégration dans un outil de paye (Winpaie ou Girafe) pour un paiement ({@link #genererFluxPaiement(EOPaiement, PecheParametresMiseEnPaiement, Class, EOPersonne)}).
 *     La génération d'un flux valide automatiquement le paiement s'il ne l'était pas.</li>
 * </ol>
 * 
 * La mise en paiement idéale se déroule donc ainsi :
 * <ol>
 * <li>{@link #mettreEnPaiement(EOService, EOPersonne)} sur chaque fiche de service à payer.
 *     Les heures marquées à payer sont mises en paiement en attente ;</li>
 * <li>{@link #genererPaiement(EOEditingContext, int, int, int, EOPersonne)} pour générer un paiement pour toutes les mises en paiement en attente ;</li>
 * <li>{@link #validerPaiement(EOPaiement, EOPersonne)} une fois le paiement vérifié et validé par un responsable ;</li>
 * <li>{@link #genererFluxPaiement(EOPaiement, PecheParametresMiseEnPaiement, Class, EOPersonne)} pour l'intégration dans un outil de paie.</li>
 * </ol>
 * 
 * @author Pascal MACOUIN
 */
public class MiseEnPaiement {
	/** Logger. */
	private static Logger log = Logger.getLogger(MiseEnPaiement.class);

	/** L'instance singleton de cette classe */
	private static MiseEnPaiement instance = new MiseEnPaiement();

	@Inject
	private IPeriodeService periodeService;

	@Inject
	private IPecheParametres pecheParametres;

	private MiseEnPaiement() {
		// Pas d'instanciation possible
	}

	/**
	 * Retourne l'instance singleton de cette classe.
	 * 
	 * @return le singleton de cette classe
	 */
	public static MiseEnPaiement getInstance() {
		if (instance.getPecheParametres() == null) {
			if (Application.application() != null) {
				instance.setPecheParametres(Application.application().injector().getInstance(IPecheParametres.class));
			}
		}
		if (instance.getPeriodeService() == null) {
			if (Application.application() != null) {
				instance.setPeriodeService(Application.application().injector().getInstance(IPeriodeService.class));
			}
		}
		return instance;
	}

	/**
	 * Mettre en paiement les heures à payer de cette fiche de service.
	 * 
	 * @param service une fiche de service
	 * @param personne personne qui fait la demande de mise en paiement
	 * @return <code>true</code> s'il y a des heures à payées qui ont été mises en paiement, <code>false</code> si rien n'était à payer
	 */
	public boolean mettreEnPaiement(EOService service, EOPersonne personne) {
		boolean miseEnPaiementEffectuee = false;

		// Pas de mise en paiement pour les génériques
		if (service.toEnseignantGenerique() != null) {
			throw new IllegalArgumentException("Pas de mise en paiement possible pour les fiches de service d'un enseignant générique");
		}

		boolean isFonctionnaire = false;

		if (service.enseignant() != null) {
			List<IVacataires> listeVacations = service.enseignant().getListeVacationsCourantesPourAnneeUniversitaire(service.annee(),getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			if (listeVacations.size() > 0) {
				isFonctionnaire = listeVacations.get(0).isTitulaire();
			}
		}
		// On supprime la mise en paiement en cours sur cette fiche
		annulerMiseEnPaiementEnAttente(service);

		// On met en paiement les heures à payer (s'il y a zéro saisie, on paye zéro, seul les heures à null ne sont pas prises en comptent)
		// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		// Pour le service (les hcomp)
		if (service.heuresAPayer() != null) {
			BigDecimal totalAPayer = calculerResteAPayer(service, null);
			if (totalAPayer != null) {
				EOPeriode periode = getPeriodeService().rechercherSecondSemestre(service.editingContext());				
				EOMiseEnPaiement miseEnPaiement = creerMiseEnPaiement(service, null, rechercherTauxBrut(service.annee(), periode), rechercherTauxCharge(service.annee(), periode, isFonctionnaire), personne);
				if (miseEnPaiement != null) {
					miseEnPaiementEffectuee = true;
				}
			}
		}

		// Pour chaque détail
		for (EOServiceDetail detail : service.listeServiceDetails()) {
			if (detail.heuresAPayer() != null) {
				EOMiseEnPaiement miseEnPaiement = creerMiseEnPaiement(service, detail, rechercherTauxBrut(service.annee(), detail.toPeriode()), rechercherTauxCharge(service.annee(), detail.toPeriode(), isFonctionnaire), personne);

				if (miseEnPaiement != null) {
					miseEnPaiementEffectuee = true;
				}
			}
		}

		return miseEnPaiementEffectuee;
	}

	private EOMiseEnPaiement creerMiseEnPaiement(EOService service, EOServiceDetail detail, BigDecimal tauxBrut, BigDecimal tauxCharge, EOPersonne personne) {
		EOMiseEnPaiement miseEnPaiement = null;
		BigDecimal totalAPayer = calculerResteAPayer(service, detail);
		BigDecimal totalAPayerHetd = null;

		if (totalAPayer != null) {
			// On teste la présence du composant AP pour le cas où d'un service Détail de type REH pour les vacataires
			// Dans ce cas, le totalAPayer hetd est égal au totalAPayer
			if (detail != null && detail.composantAP() != null) {
				// Si on est au niveau du service détail, les heures sont en présentielles, on les convertit en HETD pour le paiement.
				//				MoteurDeCalcul moteurDeCalcul = new MoteurDeCalcul(detail.editingContext(), null);
				MoteurDeCalcul moteurDeCalcul = new MoteurDeCalcul(detail.editingContext(), service.enseignant(), service.annee(), getPecheParametres().isFormatAnneeExerciceAnneeCivile());
				totalAPayerHetd = moteurDeCalcul.convertirEnHetdEnService(detail.composantAP(), totalAPayer);
			} else {
				// Les heures au niveau du service sont déjà en HETD
				totalAPayerHetd = totalAPayer;
			}

			miseEnPaiement = EOMiseEnPaiement.creerEtInitialiser(service.editingContext(), service, totalAPayer, totalAPayerHetd, tauxBrut, tauxCharge, personne);

			if (detail != null) {
				miseEnPaiement.setToServiceDetailRelationship(detail);
			}
		}

		return miseEnPaiement;
	}

	/**
	 * Calcule le nombre d'heures en attente de paiement.
	 * <p>
	 * Si on modifie le "à payer" d'un service (ou d'un service détail), on ne peut pas saisir moins (c'est un plancher).
	 * 
	 * @param service la fiche de service
	 * @param serviceDetail le service détail de cette fiche de service
	 * @return le nombre d'heures en attente de paiement (mises en paiement et non payées)
	 */
	public BigDecimal calculerAPayerEnCours(EOService service, EOServiceDetail serviceDetail) {
		BigDecimal aPayerEnCours = BigDecimal.ZERO;

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement;

		ERXKeyValueQualifier qualifierPaiementNull = ERXQ.isNull(EOMiseEnPaiement.TO_PAIEMENT_KEY);
		ERXKeyValueQualifier qualifierPaiementNonPaye = EOMiseEnPaiement.TO_PAIEMENT.dot(EOPaiement.PAYE).eq(Constante.NON);

		if (serviceDetail == null) {
			listeMisesEnPaiement = service.toListeMisesEnPaiement(qualifierPaiementNull.or(qualifierPaiementNonPaye).and(EOMiseEnPaiement.TO_SERVICE_DETAIL.isNull()));
		} else {
			listeMisesEnPaiement = serviceDetail.toListeMisesEnPaiement(qualifierPaiementNull.or(qualifierPaiementNonPaye));
		}

		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			aPayerEnCours = aPayerEnCours.add(miseEnPaiement.heuresAPayer());
		}

		return aPayerEnCours;
	}

	/**
	 * Calcule le nombre d'heures restant à payer.
	 * <p>
	 * Si on demande à payer 10 et qu'il y a 3 en attente de paiement, retournera 7.
	 * <p>
	 * Peut retourner <code>null</code> s'il n'y a rien à payer et zéro s'il y a zéro heures à payer demandé.
	 * 
	 * @param service la fiche de service
	 * @param serviceDetail le service détail de cette fiche de service
	 * @return le nombre d'heures restant à payer
	 */
	public BigDecimal calculerResteAPayer(EOService service, EOServiceDetail serviceDetail) {
		BigDecimal heuresAPayer;
		if (serviceDetail == null) {
			heuresAPayer = service.heuresAPayer();
		} else {
			heuresAPayer = serviceDetail.heuresAPayer();
		}

		if (heuresAPayer == null) {
			return null; // Rien à payer
		} else if (heuresAPayer.compareTo(BigDecimal.ZERO) == 0) {
			return BigDecimal.ZERO; // On paye zéro car explicitement demandé
		}

		BigDecimal aPayerEnCours = calculerAPayerEnCours(service, serviceDetail);
		BigDecimal resteAPayer = heuresAPayer.subtract(aPayerEnCours);
		// Pas de négatif (normalement cas impossible)
		if (resteAPayer.compareTo(BigDecimal.ZERO) < 0) {
			resteAPayer = BigDecimal.ZERO;
		}

		// On ne paye pas zéro
		if (resteAPayer.compareTo(BigDecimal.ZERO) == 0) {
			resteAPayer = null;
		}

		log.debug("    Le reste à payer est : " + resteAPayer);
		return resteAPayer;
	}

	/**
	 * Annuler la mise en paiement en attente de cette fiche de service.
	 * <p>
	 * @param service une fiche de service
	 */
	public void annulerMiseEnPaiementEnAttente(EOService service) {
		// TODO Prévoir un système qui conserve qui a fait l'annulation de la mise en paiement ?
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = service.toListeMisesEnPaiement(ERXQ.isNull(EOMiseEnPaiement.TO_PAIEMENT_KEY));
		log.debug("Le nombre de mise en paiement en attente à annuler : " + listeMisesEnPaiement.size());

		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			miseEnPaiement.setToServiceRelationship(null);
			miseEnPaiement.setToServiceDetailRelationship(null);
			service.editingContext().deleteObject(miseEnPaiement);
		}
	}

	/**
	 * Annuler la mise en paiement du paiement de cette fiche de service.
	 * <p>
	 * Le paiement ne doit pas déjà être payé.
	 * 
	 * @param paiement le paiement
	 * @param service une fiche de service
	 */
	public void annulerMiseEnPaiementduPaiement(EOPaiement paiement, EOService service) {
		if (Constante.OUI.equals(paiement.paye())) {
			throw new IllegalArgumentException("Le paiement n° " + paiement.numeroPaiement() + " a été payé. On ne peut pas l'invalider.");
		}

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = service.toListeMisesEnPaiement(ERXQ.equals(EOMiseEnPaiement.TO_PAIEMENT_KEY, paiement));

		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			miseEnPaiement.setToPaiementRelationship(null);
			miseEnPaiement.setToServiceRelationship(null);
			miseEnPaiement.setToServiceDetailRelationship(null);
			service.editingContext().deleteObject(miseEnPaiement);
		}

		log.debug("Le paiement n°" + paiement.numeroPaiement() + " a été invalidé");

		// Si le paiement n'est plus attaché à aucune mise en paiement, on l'annule
		if (paiement.toListeMisesEnPaiement().size() == 0) {
			log.debug("Le paiement n°" + paiement.numeroPaiement() + " a été supprimé");
			paiement.editingContext().deleteObject(paiement);
		}
	}

	/**
	 * Recherche les heures à payer en fonction de critères de recherche.
	 * 
	 * @param ec un editingContext
	 * @param criteresRecherchePaiement les critères de recherche
	 * @param personne la personne qui fait la demande de mise en paiement
	 * @return une liste d'enseignant/structure contenant elle même une liste de mise en paiement pour cet enseignant et cette structure
	 */
	public NSMutableArray<MiseEnPaiementEnseignantDataBean> rechercherHeuresAPayer(EOEditingContext ec, CriteresRecherchePaiement criteresRecherchePaiement, EOPersonne personne) {
		Map<String, MiseEnPaiementEnseignantDataBean> mapEnseignants = new HashMap<String, MiseEnPaiementEnseignantDataBean>();

		// Rechercher des services à payer (des heures complémentaires) pour les statutaires
		if (Constante.OUI.equals(criteresRecherchePaiement.getValeurFiltreTemoinStatutaire())) {
			mapEnseignants = rechercherHeuresAPayerStatutaires(ec, criteresRecherchePaiement, personne);
		} else {
			//recherche des services détails pour les vacataires
			mapEnseignants = rechercherHeuresAPayerVacataires(ec, criteresRecherchePaiement, personne);
		}

		List<MiseEnPaiementEnseignantDataBean> listeHeuresAPayer = new ArrayList<MiseEnPaiementEnseignantDataBean>(mapEnseignants.values());
		Collections.sort(listeHeuresAPayer, new MiseEnPaiementEnseignantDataBean.TriParEnseignantStructure());

		return new NSMutableArray<MiseEnPaiementEnseignantDataBean>(listeHeuresAPayer);
	}

	/**
	 * Recherche les heures à payer pour les statutaires
	 * @param ec un editingContext
	 * @param criteresRecherchePaiement les critères de recherche
	 * @param personne la personne qui fait la demande de mise en paiement
	 * @return une liste d'enseignant/structure contenant elle même une liste de mise en paiement pour cet enseignant et cette structure
	 */
	public Map<String, MiseEnPaiementEnseignantDataBean> rechercherHeuresAPayerStatutaires(EOEditingContext ec, 
			CriteresRecherchePaiement criteresRecherchePaiement, EOPersonne personne) {

		Map<String, MiseEnPaiementEnseignantDataBean> mapEnseignants = new HashMap<String, MiseEnPaiementEnseignantDataBean>();
		MiseEnPaiementEnseignantDataBean enseignantDataBean = null;

		EOQualifier qualifier = 
				ERXQ.and(
						EOService.HEURES_A_PAYER.isNotNull(), 
						EOService.ANNEE.eq(criteresRecherchePaiement.getAnneeUniversitaire()),
						EOService.ENSEIGNANT.dot(EOActuelEnseignant.TEM_STATUTAIRE_KEY).eq(criteresRecherchePaiement.getValeurFiltreTemoinStatutaire())
						);

		NSArray<EOSortOrdering> tri = EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE).ascInsensitives()
				.then(EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.PRENOM_AFFICHAGE).asc())
				.then(EOService.ENSEIGNANT.dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU).asc());

		NSArray<EOService> listeServices = EOService.fetchEOServices(ec, qualifier, tri);
		log.debug("Le nombre de services des statutaires à traiter est : " + listeServices.size());

		for (EOService unService : listeServices) {
			log.debug("  L'enseignant traité : " + unService.enseignant().toIndividu().getNomPrenomAffichage());
			log.debug("    Les heures à payer : " + unService.heuresAPayer());

			//Structure d'affectation
			EOStructure structureAffectation = unService.enseignant().getComposante(unService.annee(),getPecheParametres().isFormatAnneeExerciceAnneeCivile());

			// Filtres auxiliaires (non codable en SQL)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			boolean ligneServiceATraiter = criteresRecherchePaiement.isCriteresAuxiliairesSatisfaits(ec, unService, structureAffectation);
			log.debug("    Est-ce que les critères auxiliaires ont été satisfait? : " + ligneServiceATraiter);
			log.debug("    Est-ce que le statutaire est titulaire? : " + unService.enseignant().isTitulaire(unService.annee(), getPecheParametres().isFormatAnneeExerciceAnneeCivile()));

			if (ligneServiceATraiter) {
				BigDecimal tauxBrut = rechercherTauxBrut(unService.annee(), getPeriodeService().rechercherSecondSemestre(ec));
				BigDecimal tauxCharge = rechercherTauxCharge(unService.annee(), getPeriodeService().rechercherSecondSemestre(ec), unService.enseignant().isTitulaire(unService.annee(), getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
				log.debug("    Le taux chargé est de : " + tauxCharge);

				// On crée un objet mise en paiement pour chaque service 
				EOMiseEnPaiement miseEnPaiement = creerMiseEnPaiement(unService, null, tauxBrut, tauxCharge, personne);

				if (miseEnPaiement != null) {
					log.debug("    On crée une mise en paiement ");
					miseEnPaiement.setToStructureRelationship(structureAffectation);
					enseignantDataBean = recupererEnseignantDataBean(mapEnseignants, enseignantDataBean, miseEnPaiement);
					enseignantDataBean.getListeMiseEnPaiement().add(miseEnPaiement);
				}
			}
		}

		return mapEnseignants;
	}

	/**
	 * Recherche les heures à payer pour les vacataires
	 * @param ec un editingContext
	 * @param criteresRecherchePaiement les critères de recherche
	 * @param personne la personne qui fait la demande de mise en paiement
	 * @return une liste d'enseignant/structure contenant elle même une liste de mise en paiement pour cet enseignant et cette structure
	 */
	public Map<String, MiseEnPaiementEnseignantDataBean> rechercherHeuresAPayerVacataires(EOEditingContext ec, CriteresRecherchePaiement criteresRecherchePaiement, EOPersonne personne) {
		Map<String, MiseEnPaiementEnseignantDataBean> mapEnseignants = new HashMap<String, MiseEnPaiementEnseignantDataBean>();
		MiseEnPaiementEnseignantDataBean enseignantDataBean = null;

		// Recherche des service détail à payer
		// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		ERXAndQualifier qualifierService = EOServiceDetail.HEURES_A_PAYER.isNotNull()
				.and(EOServiceDetail.SERVICE.dot(EOService.ANNEE).eq(criteresRecherchePaiement.getAnneeUniversitaire()))
				.and(EOServiceDetail.TO_PERIODE.dot(EOPeriode.ORDRE_PERIODE).lessThanOrEqualTo(criteresRecherchePaiement.getPeriode().ordrePeriode()))
				.and(EOServiceDetail.COMPOSANT_AP.isNotNull());



		ERXAndQualifier qualifierReh = EOServiceDetail.HEURES_A_PAYER.isNotNull()
				.and(EOServiceDetail.SERVICE.dot(EOService.ANNEE).eq(criteresRecherchePaiement.getAnneeUniversitaire()))
				.and(EOServiceDetail.TO_PERIODE.dot(EOPeriode.ORDRE_PERIODE).lessThanOrEqualTo(criteresRecherchePaiement.getPeriode().ordrePeriode()))
				.and(EOServiceDetail.REH.isNotNull()).and(EOServiceDetail.COMPOSANT_AP.isNull());


		if (criteresRecherchePaiement.getValeurFiltreTemoinStatutaire() != null) {

			EOQualifier qualifierStatutaire = (EOServiceDetail.SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TEM_STATUTAIRE_KEY).eq(criteresRecherchePaiement.getValeurFiltreTemoinStatutaire()));
			qualifierService = ERXQ.and(qualifierService,qualifierStatutaire);
			qualifierReh = ERXQ.and(qualifierReh,qualifierStatutaire);

		}

		NSArray<EOSortOrdering> tri = EOServiceDetail.SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NOM_AFFICHAGE).ascInsensitives()
				.then(EOServiceDetail.SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.PRENOM_AFFICHAGE).asc())
				.then(EOServiceDetail.SERVICE.dot(EOService.ENSEIGNANT).dot(EOActuelEnseignant.TO_INDIVIDU).dot(EOIndividu.NO_INDIVIDU).asc());

		NSArray<EOServiceDetail> listeServiceDetailSer = EOServiceDetail.fetchEOServiceDetails(ec, qualifierService, null);
		NSArray<EOServiceDetail> listeServiceDetailReh = EOServiceDetail.fetchEOServiceDetails(ec, qualifierReh, null);

		NSMutableArray<EOServiceDetail> listeServiceDetail = new NSMutableArray<EOServiceDetail>(listeServiceDetailSer);
		listeServiceDetail.addAll(listeServiceDetailReh);

		EOSortOrdering.sortedArrayUsingKeyOrderArray(listeServiceDetail, tri);

		log.debug("Le nombre de détails de service des vacataires à traiter est : " + listeServiceDetail.size());

		for (EOServiceDetail serviceDetail : listeServiceDetail) {
			log.debug("  Le service détail traité est  : " + serviceDetail.primaryKey());
			log.debug("    Les heures à payer : " + serviceDetail.heuresAPayer());

			EOStructure structureComposant = determinerStructurePayante(serviceDetail);
			
			EOStructure structureRepartService = serviceDetail.toRepartService().toStructure();
			// cas particulier : l'AP n'a pas de structure			
			if (structureComposant == null) {
				structureComposant = structureRepartService;
			}
			
			// Filtres auxiliaires (non codable en SQL)
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			boolean ligneServiceDetailATraiter = criteresRecherchePaiement.isCriteresAuxiliairesSatisfaits(ec, serviceDetail, structureComposant);

			if (ligneServiceDetailATraiter) {
				BigDecimal tauxBrut = rechercherTauxBrut(serviceDetail.service().annee(), serviceDetail.toPeriode());

				boolean isFonctionnaire = false;

				if (serviceDetail.service().enseignant() != null) {
					List<IVacataires> listeVacations = serviceDetail.service().enseignant().getListeVacationsCourantesPourAnneeUniversitaire(serviceDetail.service().annee(),getPecheParametres().isFormatAnneeExerciceAnneeCivile());
					if (listeVacations.size() > 0) {
						isFonctionnaire = listeVacations.get(0).isTitulaire();
					}
				}

				log.debug("    Est-ce que le vacataire est fonctionnaire? : " + isFonctionnaire);
				BigDecimal tauxCharge = rechercherTauxCharge(serviceDetail.service().annee(), serviceDetail.toPeriode(), isFonctionnaire);
				log.debug("    Le taux chargé est de : " + tauxCharge);

				// On crée un objet mise en paiement pour chaque service détail
				EOMiseEnPaiement miseEnPaiement = creerMiseEnPaiement(serviceDetail.service(), serviceDetail, tauxBrut, tauxCharge, personne);

				if (miseEnPaiement != null) {
					log.debug("    On crée une mise en paiement");					
					miseEnPaiement.setToStructureRelationship(structureComposant);
					enseignantDataBean = recupererEnseignantDataBean(mapEnseignants, enseignantDataBean, miseEnPaiement);
					enseignantDataBean.getListeMiseEnPaiement().add(miseEnPaiement);
				}
			}
		}

		return mapEnseignants;
	}

	/**
	 * Retourne un {@link EOStructure} correspondant au service Detail
	 * @param serviceDetail le serviceDetail sur lequel on itère
	 * @return
	 */
	private EOStructure determinerStructurePayante(EOServiceDetail serviceDetail) {

		EOStructure structurePayante = null;

		if (serviceDetail.composantAP() != null) {
			// cas d'une répartition service
			ComposantCtrl composantCtrl = new ComposantCtrl();
			structurePayante = composantCtrl.getStructure(serviceDetail.composantAP());
		} else if (serviceDetail.reh() != null && serviceDetail.toStructure() != null) {
			// Cas d'une répartition REH
			structurePayante = serviceDetail.toStructure();
		}
		return structurePayante;

	}

	/**
	 * Retourne un {@link MiseEnPaiementEnseignantDataBean} correspondant à l'enseignant/structure.
	 * 
	 * @param mapEnseignants la map des bean enseignants/structure. Cette map est alimenté automatiquement si besoin.
	 * @param enseignantDataBean Le bean enseignant/structure en cours. <code>null</code> si rien en cours (la première fois).
	 * @param miseEnPaiement La mise en paiement à affecter à ce bean enseignant/structure
	 * @return un bean enseignantStructure
	 */
	private MiseEnPaiementEnseignantDataBean recupererEnseignantDataBean(Map<String, MiseEnPaiementEnseignantDataBean> mapEnseignants, MiseEnPaiementEnseignantDataBean enseignantDataBean, EOMiseEnPaiement miseEnPaiement) {
		if (enseignantDataBean == null || !enseignantDataBean.isMemeDataBean(miseEnPaiement)) {
			// Changement d'enseignant et/ou de structure de paiement
			String cle = miseEnPaiement.toService().enseignant().noDossierPers().toString();

			if (miseEnPaiement.toStructure() != null) {
				cle += "/" + miseEnPaiement.toStructure().cStructure();
			}

			enseignantDataBean = mapEnseignants.get(cle);

			if (enseignantDataBean == null) {
				enseignantDataBean = new MiseEnPaiementEnseignantDataBean();
				enseignantDataBean.setEnseignant(miseEnPaiement.toService().enseignant());
				enseignantDataBean.setStructurePayante(miseEnPaiement.toStructure());
				mapEnseignants.put(cle, enseignantDataBean);
			}
		}

		return enseignantDataBean;
	}

	/**
	 * Générer un numéro de paiement.
	 * @param paiement un paiement
	 * @return un numéro de paiement
	 */
	public Integer genererNumeroPaiement(EOPaiement paiement) {
		Integer numeroPaiement;

		if (paiement.primaryKey() != null) {
			numeroPaiement = Integer.valueOf(paiement.primaryKey());
		} else {
			numeroPaiement = Integer.valueOf(paiement.primaryKeyInTransaction());
		}

		return numeroPaiement;
	}

	/**
	 * Génère un paiement.
	 * 
	 * @param editingContext un editingContext
	 * @param anneeUniversitaire L'année universitaire
	 * @param periode la période
	 * @param listeMiseEnPaiementEnseignantDataBean la liste des bean enseignant/structure à mettre en paiement (si l'indicateur {@link MiseEnPaiementEnseignantDataBean#isBonPourPaiement()} est à <code>true</code>).
	 * @param personne La personne qui fait la demande de mise en paiement
	 * @return Le paiement qui à été généré
	 */
	public EOPaiement genererPaiement(EOEditingContext editingContext, int anneeUniversitaire, EOPeriode periode, NSArray<MiseEnPaiementEnseignantDataBean> listeMiseEnPaiementEnseignantDataBean, EOPersonne personne) {
		EOPaiement paiement = EOPaiement.creerEtInitialiser(editingContext, 0, 0, anneeUniversitaire, personne);
		paiement.setNumeroPaiement(genererNumeroPaiement(paiement));
		if (periode != null) {
			paiement.setMoisPaiement(periode.ordrePeriode());
		}
		paiement.setAnneePaiement(anneeUniversitaire);
		paiement.setLibelle("Paiement n° " + paiement.numeroPaiement() + " du " + DateCtrl.dateToString(paiement.dateCreation()));

		for (MiseEnPaiementEnseignantDataBean miseEnPaiementEnseignantDataBean : listeMiseEnPaiementEnseignantDataBean) {
			if (miseEnPaiementEnseignantDataBean.isBonPourPaiement()) {
				for (EOMiseEnPaiement miseEnPaiement : miseEnPaiementEnseignantDataBean.getListeMiseEnPaiement()) {
					miseEnPaiement.setToPaiementRelationship(paiement);
					miseEnPaiement.majDonnesAuditModification(personne);
				}
			} else {
				for (EOMiseEnPaiement miseEnPaiement : miseEnPaiementEnseignantDataBean.getListeMiseEnPaiement()) {
					miseEnPaiement.editingContext().deleteObject(miseEnPaiement);
				}
			}
		}

		return paiement;
	}

	/**
	 * Génération du paiement et association des mises en paiement en attente à celui-ci.
	 * 
	 * @param editingContext un editing context
	 * @param numeroPaiement le numéro de paiement à générer
	 * @param moisPaiement le mois de paiement de ce paiement
	 * @param anneePaiement l'année de paiement de ce paiement
	 * @param personne la personne qui demande ce paiement
	 * @return le paiement généré
	 */
	public EOPaiement genererPaiement(EOEditingContext editingContext, int numeroPaiement, int moisPaiement, int anneePaiement, EOPersonne personne) {
		EOPaiement paiement = EOPaiement.creerEtInitialiser(editingContext, numeroPaiement, moisPaiement, anneePaiement, personne);

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = EOMiseEnPaiement.fetchEOMiseEnPaiements(editingContext, ERXQ.isNull(EOMiseEnPaiement.TO_PAIEMENT_KEY), null);
		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			miseEnPaiement.setToPaiementRelationship(paiement);
			miseEnPaiement.majDonnesAuditModification(personne);
		}

		return paiement;
	}

	/**
	 * Invalider le paiement en supprimant le paiement et en rendant disponible les mise en paiement.
	 * 
	 * @param paiement le paiement à invalider
	 * @param personne la personne qui demande l'invalidation de ce paiement
	 */
	public void invaliderPaiement(EOPaiement paiement, EOPersonne personne) {
		if (Constante.OUI.equals(paiement.paye())) {
			throw new IllegalArgumentException("Le paiement n° " + paiement.numeroPaiement() + " a été payé. On ne peut pas l'invalider.");
		}

		// TODO Prévoir un système qui conserve qui a invalidé le paiement (réponse "non" pour les établissements) ?
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = paiement.toListeMisesEnPaiement().immutableClone();
		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			miseEnPaiement.setToPaiementRelationship(null);
			miseEnPaiement.editingContext().deleteObject(miseEnPaiement);
		}

		paiement.editingContext().deleteObject(paiement);
	}

	/**
	 * Valider le paiement.
	 * <p>
	 * Le paiement va être fait. Une fois valider, il est figé et plus modifiable.
	 * <p>
	 * Toutes les heures qui sont à payer pour ce paiement sont basculées (ajoutées) dans les colonnes "PAYEES"
	 * et déduites (soustraites) des colonnes "A_PAYER".
	 * 
	 * @param paiement le paiement à valider
	 * @param personne la personne qui demande la validation
	 */
	public void validerPaiement(EOPaiement paiement, EOPersonne personne) {
		if (Constante.OUI.equals(paiement.paye())) {
			throw new IllegalArgumentException("Le paiement n° " + paiement.numeroPaiement() + " a déjà été payé. On ne peut pas le valider à nouveau.");
		}

		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = paiement.toListeMisesEnPaiement();
		for (EOMiseEnPaiement miseEnPaiement : listeMisesEnPaiement) {
			miseEnPaiement.setHeuresPayees(miseEnPaiement.heuresAPayer());
			miseEnPaiement.setHeuresPayeesHetd(miseEnPaiement.heuresAPayerHetd());
			miseEnPaiement.majDonnesAuditModification(personne);

			if (miseEnPaiement.toServiceDetail() != null) {
				EOServiceDetail detail = miseEnPaiement.toServiceDetail();
				detail.setHeuresPayees(retournerNonNull(detail.heuresPayees()).add(miseEnPaiement.heuresPayees()));

				BigDecimal heuresAPayerRestantes = retournerNonNull(detail.heuresAPayer()).subtract(miseEnPaiement.heuresPayees());
				if (heuresAPayerRestantes.compareTo(BigDecimal.ZERO) == 0) {
					heuresAPayerRestantes = null;
				}
				detail.setHeuresAPayer(heuresAPayerRestantes);
				detail.majDonnesAuditModification(personne);
			} else {
				EOService service = miseEnPaiement.toService();
				service.setHeuresPayees(retournerNonNull(service.heuresPayees()).add(miseEnPaiement.heuresPayees()));

				BigDecimal heuresAPayerRestantes = service.heuresAPayer().subtract(miseEnPaiement.heuresPayees());
				if (heuresAPayerRestantes.compareTo(BigDecimal.ZERO) == 0) {
					heuresAPayerRestantes = null;
				}
				service.setHeuresAPayer(heuresAPayerRestantes);
				service.majDonnesAuditModification(personne);
			}
		}

		paiement.setPaye(Constante.OUI);
		paiement.majDonnesAuditModification(personne);
	}

	/**
	 * Génération des données utiles pour les flux de paiement pour un paiement.
	 * <p>
	 * Si le paiement n'est pas validé, il est validé.
	 * <p>
	 * Utilise le type de flux par défaut indiqué dans les paramètres de génération du flux.
	 * 
	 * @param paiement le paiement à payer
	 * @param moisSuivantPourTG si true, ajoute 1 mois au mois de paiement
	 * @param parametresFluxPaiement les paramètres nécessaires à la mise en paiement
	 * @param personne la personne qui génère le flux
	 * @return le flux de mise en paiement généré
	 * @throws Exception 
	 */
	public FluxPaiement genererFluxPaiement(EOPaiement paiement, String moisSuivantPourTG, boolean estCotisationASolidarite, EOParametresFluxPaiement parametresFluxPaiement, EOPersonne personne) throws Exception {
		return genererFluxPaiement(paiement, moisSuivantPourTG, estCotisationASolidarite, parametresFluxPaiement, parametresFluxPaiement.toTypeFluxPaiement().getClasseTypeFlux(), personne);
	}

	/**
	 * Génération des données utiles pour les flux de paiement pour un paiement.
	 * <p>
	 * Si le paiement n'est pas validé, il est validé.
	 * 
	 * @param paiement le paiement à payer
	 * @param moisSuivantPourTG si true, ajoute 1 mois au mois de paiement
	 * @param estCotisationASolidarite les enseignants sont-ils soumis à la cotisation avec solidarité ?
	 * @param parametresFluxPaiement les paramètres nécessaires à la mise en paiement
	 * @param formatFluxPaiement classe représentant le format du flux de paiement à générer
	 * @param personne la personne qui génère le flux
	 * @return le flux de mise en paiement généré
	 * @throws Exception 
	 */
	public FluxPaiement genererFluxPaiement(EOPaiement paiement, String moisSuivantPourTG, boolean estCotisationASolidarite, EOParametresFluxPaiement parametresFluxPaiement, Class<? extends IFormatFluxPaiement> formatFluxPaiement, EOPersonne personne) throws Exception {
		if (Constante.NON.equals(paiement.paye())) {
			validerPaiement(paiement, personne);
		}

		paiement.setDateFlux(new NSTimestamp());

		// Si pas déjà des données on génère, sinon on les met à jour
		if (paiement.toListeFluxMiseEnPaiement().isEmpty()) {
			genererDonneesFluxPaiement(paiement, moisSuivantPourTG, personne);
		} else {
			majDonneesFluxPaiement(paiement, personne);
		}

		// Génération du flux
		FluxPaiement fluxPaiement = new FluxPaiement(formatFluxPaiement, parametresFluxPaiement);
		if (!fluxPaiement.isFormatExcel()) {
			fluxPaiement.genererFlux(paiement, estCotisationASolidarite);
		} else {
			fluxPaiement.genererFluxFormatExcel(paiement, estCotisationASolidarite);
		}

		return fluxPaiement;
	}

	/**
	 * Retourne les mise en paiement d'un paiement en les groupants et en cumulant les heures.
	 * 
	 * @param paiement le paiement
	 * @param groupage un objet définissant le groupage
	 * @return une liste de mise en paiement
	 */
	public List<MiseEnPaiementDataBean> listerMiseEnPaiementGrouperPar(EOPaiement paiement, IGrouperMiseEnPaiement<EOMiseEnPaiement> groupage) {

		List<MiseEnPaiementDataBean> listeMiseEnPaiementDataBean = new ArrayList<MiseEnPaiementDataBean>();


		// Création des données pour le flux de paiement
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = paiement.toListeMisesEnPaiement(null, groupage.getTri(), true);

		if (NSArrayCtrl.isEmpty(groupage.getTri())) {
			try {
				listeMisesEnPaiement = listeMisesEnPaiement.sortedArrayUsingComparator(new NSComparator() {
					@Override
					public int compare(Object miseEnPaiementA, Object miseEnPaiementB) {
						int result = 0;

						EnseignantsVacatairesCtrl controleurVacataires = new EnseignantsVacatairesCtrl(((EOMiseEnPaiement) miseEnPaiementA).editingContext(), getPecheParametres().getAnneeUniversitaireEnCours(),
								getPecheParametres().isFormatAnneeExerciceAnneeCivile());

						if (((EOMiseEnPaiement) miseEnPaiementA).toStructure() != null && ((EOMiseEnPaiement) miseEnPaiementB).toStructure() != null) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).toStructure().cStructure().compareTo(((EOMiseEnPaiement) miseEnPaiementB).toStructure().cStructure());
						}
						if (result == 0) {
							result = controleurVacataires.getLibelleTypeVacationPourXML(((EOMiseEnPaiement) miseEnPaiementA).toService().enseignant())
									.compareToIgnoreCase(controleurVacataires.getLibelleTypeVacationPourXML(((EOMiseEnPaiement) miseEnPaiementB).toService().enseignant()));							
						}
						if (result == 0) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).toService().enseignant().toIndividu().nomAffichage()
									.compareToIgnoreCase(((EOMiseEnPaiement) miseEnPaiementB).toService().enseignant().toIndividu().nomAffichage());							
						}
						if (result == 0) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).toService().enseignant().toIndividu().prenom()
									.compareToIgnoreCase(((EOMiseEnPaiement) miseEnPaiementB).toService().enseignant().toIndividu().prenom());							
						}
						if (result == 0) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).toService().enseignant().toIndividu().noIndividu()
									.compareTo(((EOMiseEnPaiement) miseEnPaiementB).toService().enseignant().toIndividu().noIndividu());							
						}
						if (result == 0) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).toService().id()
									.compareTo(((EOMiseEnPaiement) miseEnPaiementB).toService().id());							
						}
						if (result == 0) {
							result = ((EOMiseEnPaiement) miseEnPaiementA).tauxBrut()
									.compareTo(((EOMiseEnPaiement) miseEnPaiementB).tauxBrut());							
						}
						return result;
					}
				});
			} catch (ComparisonException e) {
				log.error("erreur comparaison:" + e);
			}
		}
		Iterator<EOMiseEnPaiement> iterateurListeMisesEnPaiement = listeMisesEnPaiement.iterator();

		EOMiseEnPaiement miseEnPaiement = null;
		EOMiseEnPaiement miseEnPaiementPrecedente = null;

		// Lecture du premier
		if (iterateurListeMisesEnPaiement.hasNext()) {
			miseEnPaiement = iterateurListeMisesEnPaiement.next();
		}

		while (miseEnPaiement != null) {
			MiseEnPaiementDataBean miseEnPaiementDataBean = new MiseEnPaiementDataBean();
			BigDecimal totalheuresAPayerHetdPourCeTaux = BigDecimal.ZERO;
			BigDecimal totalheuresPayeesHetdPourCeTaux = BigDecimal.ZERO;

			// On regroupe et on cumul
			do {
				miseEnPaiementDataBean.cumulHeuresAPayer(miseEnPaiement.heuresAPayer());
				miseEnPaiementDataBean.cumulheuresAPayerHetd(miseEnPaiement.heuresAPayerHetd());
				totalheuresAPayerHetdPourCeTaux = totalheuresAPayerHetdPourCeTaux.add(miseEnPaiement.heuresAPayerHetd());

				if (miseEnPaiement.heuresPayees() != null) {
					miseEnPaiementDataBean.cumulheuresPayees(miseEnPaiement.heuresPayees());
				}
				if (miseEnPaiement.heuresPayeesHetd() != null) {
					miseEnPaiementDataBean.cumulheuresPayeesHetd(miseEnPaiement.heuresPayeesHetd());
					totalheuresPayeesHetdPourCeTaux.add(miseEnPaiement.heuresPayeesHetd());
				}
				miseEnPaiementDataBean.cumulService(miseEnPaiement.toService());
				miseEnPaiementDataBean.cumultauxBrut(miseEnPaiement.tauxBrut());
				miseEnPaiementDataBean.cumultauxCharge(miseEnPaiement.tauxCharge());

				if (miseEnPaiement.toServiceDetail() != null) {
					miseEnPaiementDataBean.cumulPeriode(miseEnPaiement.toServiceDetail().toPeriode());
				}

				miseEnPaiementDataBean.cumulStructure(miseEnPaiement.toStructure());

				// Gestion de la rupture
				miseEnPaiementPrecedente = miseEnPaiement;

				if (iterateurListeMisesEnPaiement.hasNext()) {
					miseEnPaiement = iterateurListeMisesEnPaiement.next();
				} else {
					miseEnPaiement = null;
				}

				// Si le taux brut change, on calcul les montants
				if (miseEnPaiement == null || miseEnPaiementPrecedente.tauxBrut().compareTo(miseEnPaiement.tauxBrut()) != 0 
						|| groupage.isRupture(miseEnPaiementPrecedente, miseEnPaiement)) {
					miseEnPaiementDataBean.cumulMontantAPayerBrut(totalheuresAPayerHetdPourCeTaux.multiply(miseEnPaiementPrecedente.tauxBrut()).setScale(2, RoundingMode.HALF_UP));
					miseEnPaiementDataBean.cumulMontantPayesBrut(totalheuresPayeesHetdPourCeTaux.multiply(miseEnPaiementPrecedente.tauxBrut()).setScale(2, RoundingMode.HALF_UP));
					miseEnPaiementDataBean.cumulMontantAPayerCharge(totalheuresAPayerHetdPourCeTaux.multiply(miseEnPaiementPrecedente.tauxCharge()).setScale(2, RoundingMode.HALF_UP));
					miseEnPaiementDataBean.cumulMontantPayesCharge(totalheuresPayeesHetdPourCeTaux.multiply(miseEnPaiementPrecedente.tauxCharge()).setScale(2, RoundingMode.HALF_UP));					
				}


			} while (miseEnPaiement != null && !groupage.isRupture(miseEnPaiementPrecedente, miseEnPaiement));

			listeMiseEnPaiementDataBean.add(miseEnPaiementDataBean);
		}

		return listeMiseEnPaiementDataBean;
	}

	/**
	 * Générer les données pour le flux de paiement.
	 * 
	 * @param paiement le paiment
	 * @param personne la personne qui génère les données
	 */
	private void genererDonneesFluxPaiement(EOPaiement paiement, String moisSuivantPourTG, EOPersonne personne) {
		// Création des données pour le flux de paiement
		NSArray<EOMiseEnPaiement> listeMisesEnPaiement = paiement.toListeMisesEnPaiement(null, EOMiseEnPaiement.TO_SERVICE.dot(EOService.ID).asc().then(EOMiseEnPaiement.TAUX_BRUT.asc()), false);
		Iterator<EOMiseEnPaiement> iterateurListeMisesEnPaiement = listeMisesEnPaiement.iterator();

		EOMiseEnPaiement miseEnPaiement = null;
		EOMiseEnPaiement miseEnPaiementPrecedent = null;

		// Lecture du premier
		if (iterateurListeMisesEnPaiement.hasNext()) {
			miseEnPaiement = iterateurListeMisesEnPaiement.next();
		}

		while (miseEnPaiement != null) {
			BigDecimal totalHeuresPayesHetd = BigDecimal.ZERO;

			// On cumul par fiche de service (donc par personne) et par taux brut
			do {
				totalHeuresPayesHetd = totalHeuresPayesHetd.add(retournerNonNull(miseEnPaiement.heuresPayeesHetd()));

				miseEnPaiementPrecedent = miseEnPaiement;

				if (iterateurListeMisesEnPaiement.hasNext()) {
					miseEnPaiement = iterateurListeMisesEnPaiement.next();
				} else {
					miseEnPaiement = null;
				}
			} while (miseEnPaiement != null
					&& miseEnPaiementPrecedent.toService() == miseEnPaiement.toService()
					&& miseEnPaiementPrecedent.tauxBrut().compareTo(miseEnPaiement.tauxBrut()) == 0);

			// Création des données du flux pour cette personne
			EOService service = miseEnPaiementPrecedent.toService();
			EOIndividu individu = service.enseignant().toIndividu();
			EOPersonnel personnel = individu.toPersonnel();

			String nom = individu.nomAffichage().toUpperCase();
			String prenom = individu.prenomAffichage();

			BigDecimal brutPaye = totalHeuresPayesHetd.multiply(miseEnPaiementPrecedent.tauxBrut());
			brutPaye = brutPaye.setScale(2, RoundingMode.HALF_UP);

			EOFluxMiseEnPaiement fluxMiseEnPaiement = EOFluxMiseEnPaiement.creerEtInitialiser(paiement.editingContext(), paiement, service, nom, prenom, 
					totalHeuresPayesHetd, miseEnPaiementPrecedent.tauxBrut(), brutPaye, moisSuivantPourTG, personne);
			fluxMiseEnPaiement.setNumeroInsee(individu.indNoInsee());
			fluxMiseEnPaiement.setCleInsee(individu.indCleInsee());
			fluxMiseEnPaiement.setNumeroPriseEnCharge(fluxMiseEnPaiement, personnel);			
		}
	}

	/**
	 * Mettre à jour certaines données du flux de paiement.
	 * 
	 * @param paiement le paiement
	 * @param personne la personne qui met à jour les données
	 */
	private void majDonneesFluxPaiement(EOPaiement paiement, EOPersonne personne) {
		for (EOFluxMiseEnPaiement fluxMiseEnPaiement : paiement.toListeFluxMiseEnPaiement()) {
			EOIndividu individu = fluxMiseEnPaiement.toService().enseignant().toIndividu();
			EOPersonnel personnel = individu.toPersonnel();

			fluxMiseEnPaiement.setNom(individu.nomAffichage().toUpperCase());
			fluxMiseEnPaiement.setPrenom(individu.prenomAffichage());
			fluxMiseEnPaiement.setNumeroInsee(individu.indNoInsee());
			fluxMiseEnPaiement.setCleInsee(individu.indCleInsee());
			fluxMiseEnPaiement.setNumeroPriseEnCharge(fluxMiseEnPaiement, personnel);

			fluxMiseEnPaiement.majDonnesAuditModification(personne);
		}
	}

	/**
	 * Calcule et retourne le taux brut (le taux non chargé) pour la période du paiement.
	 * 
	 * @param paiement le paiement
	 * @return le taux brut (le taux non chargé)
	 */
	private BigDecimal rechercherTauxBrut(int anneeUniversitaire, EOPeriode periode) {
		EOTauxHoraire tauxHoraire = rechercherTauxHoraire(anneeUniversitaire, periode);

		if (tauxHoraire == null) {
			return BigDecimal.ZERO;
		}

		return tauxHoraire.tauxBrut();
	}

	/**
	 * Calcule et retourne le taux chargé pour la période du paiement.
	 * 
	 * @param paiement le paiement
	 * @return le taux chargé
	 */
	private BigDecimal rechercherTauxCharge(int anneeUniversitaire, EOPeriode periode, boolean isFonctionnaire) {
		EOTauxHoraire tauxHoraire = rechercherTauxHoraire(anneeUniversitaire, periode);

		if (tauxHoraire == null) {
			return BigDecimal.ZERO;
		}
		if (isFonctionnaire) {
			return tauxHoraire.tauxChargeFonctionnaire();
		} else {
			return tauxHoraire.tauxChargeNonFonctionnaire();
		}
	}
	/**
	 * Rechercher le taux horaire actif pour la période du paiement.
	 * 
	 * @param paiement le paiement
	 * @return le taux horaire
	 */
	private EOTauxHoraire rechercherTauxHoraire(int anneeUniversitaire, EOPeriode periode) {
		int anneeCivile = calculerAnneeCivile(anneeUniversitaire, periode);

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.clear();
		calendar.set(anneeCivile, periode.noDernierMois(), 1);

		NSTimestamp dateDebutPeriodeSuivante = new NSTimestamp(calendar.getTime());

		calendar.add(Calendar.MONTH, -periode.nombreMois());
		NSTimestamp dateDebutPeriode = new NSTimestamp(calendar.getTime());

		ERXAndQualifier qualifier = EOTauxHoraire.DATE_DEBUT_APPLICATION.lessThan(dateDebutPeriodeSuivante)
				.and(EOTauxHoraire.DATE_FIN_APPLICATION.isNull()
						.or(EOTauxHoraire.DATE_FIN_APPLICATION.greaterThanOrEqualTo(dateDebutPeriode)));

		NSArray<EOTauxHoraire> listeTauxHoraire = EOTauxHoraire.fetchEOTauxHoraires(periode.editingContext(), qualifier, EOTauxHoraire.DATE_FIN_APPLICATION.descs());

		EOTauxHoraire tauxHoraire = null;

		if (!listeTauxHoraire.isEmpty()) {
			tauxHoraire = listeTauxHoraire.get(0);
		}

		return tauxHoraire;
	}

	/**
	 * Retourne l'année civile en fonction de l'année universitaire et de la période.
	 * 
	 * @param anneeUniversitaire l'année universitaire
	 * @param periode la période
	 * @return l'année civile
	 */
	private int calculerAnneeCivile(int anneeUniversitaire, EOPeriode periode) {

		boolean estAnneeCivile = getPecheParametres().isFormatAnneeExerciceAnneeCivile();
		if (!estAnneeCivile) {
			EOPeriode periodePremierSemestre = getPeriodeService().rechercherPremierSemestre(periode.editingContext());

			int moisDepartAnneeUniversitaire = periodePremierSemestre.noPremierMois();

			if (periode.noDernierMois() < moisDepartAnneeUniversitaire) {
				return anneeUniversitaire + 1;
			}
		}

		return anneeUniversitaire;
	}

	/**
	 * Retourne la valeur d'un <code>BigDecimal</code> (0 si <code>null</code>).
	 * 
	 * @param unBigDecimal un BigDecimal
	 * @return lui même ou 0
	 */
	private BigDecimal retournerNonNull(BigDecimal unBigDecimal) {
		if (unBigDecimal == null) {
			return BigDecimal.ZERO;
		}

		return unBigDecimal;
	}

	public IPeriodeService getPeriodeService() {
		return periodeService;
	}

	public void setPeriodeService(IPeriodeService periodeService) {
		this.periodeService = periodeService;
	}

	public IPecheParametres getPecheParametres() {
		return pecheParametres;
	}

	public void setPecheParametres(IPecheParametres pecheParametres) {
		this.pecheParametres = pecheParametres;
	}
}
