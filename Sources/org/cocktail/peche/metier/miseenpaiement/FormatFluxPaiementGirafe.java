package org.cocktail.peche.metier.miseenpaiement;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Cette classe formate le flux de mise en paiement au format Girafe.
 * 
 * @author Pascal MACOUIN
 */
public class FormatFluxPaiementGirafe extends FormatFluxPaiementSansEntete {

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOSortOrdering> getTri() {
		return EOFluxMiseEnPaiement.NUMERO_INSEE.ascs();
	}	
	
	/**
	 * {@inheritDoc}
	 */
	public String getExtensionNomFlux() {
		return ".ds2";
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getLigneDetail(EOFluxMiseEnPaiement fluxMiseEnPaiement, boolean estCotisationASolidarite) {
		
		StringBuilder ligneDetail = new StringBuilder(getDatePaye(fluxMiseEnPaiement));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_CODE_CHAINE_PAYE.getCodeChamp()));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_CORR.getCodeChamp()));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_NUMERO_REMISE.getCodeChamp()));
		ligneDetail.append(String.format("%5s", ""));
		ligneDetail.append("20");
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_MIN.getCodeChamp()));
		ligneDetail.append(getNumeroInsee(fluxMiseEnPaiement));
		ligneDetail.append(getCleInsee(fluxMiseEnPaiement));
		ligneDetail.append(getNumeroPriseEnCharge(fluxMiseEnPaiement));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_ORIG.getCodeChamp()));
		ligneDetail.append(getCodeCotisationSolidarite(estCotisationASolidarite));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_SENS.getCodeChamp()));
		ligneDetail.append(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_MTT.getCodeChamp()));
		ligneDetail.append(String.format("%4s", ""));
		ligneDetail.append(getMontantBrutPaye(fluxMiseEnPaiement));
		ligneDetail.append(String.format("%8s", ""));
		ligneDetail.append(getLibelleComplementaire(fluxMiseEnPaiement));
		ligneDetail.append(String.format("%10s", ""));
		ligneDetail.append(getParametresFluxPaiement().administration());
		ligneDetail.append(getParametresFluxPaiement().departement());
		ligneDetail.append(String.format("%1s", ""));
		ligneDetail.append(getParametresFluxPaiement().administration());
		ligneDetail.append(getParametresFluxPaiement().departement());
		ligneDetail.append(String.format("%4s", ""));
		
		return ligneDetail.toString();
	}

	/**
	 * Retourne le libellé complémentaire pouvant apparaître sur le bulletin de paie.
	 * Doit retourner 25 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return le libellé complémentaire
	 */
	private String getLibelleComplementaire(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		return String.format("%-25.25s", String.format(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_TPL_LIB_COMP.getCodeChamp()), 
				fluxMiseEnPaiement.heuresPayees(), fluxMiseEnPaiement.tauxNonCharge(), fluxMiseEnPaiement.brutPaye()));
	}
	
	/**
	 * 
	 * @param estCotisationASolidarite
	 * @return
	 */
	private String getCodeCotisationSolidarite(boolean estCotisationASolidarite) {
		String codeCotisation = "";
		if (estCotisationASolidarite) {
			codeCotisation = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_IR_SOLIDARITE.getCodeChamp());
		} else {
			codeCotisation = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.GIRAFE_IR_SANS_SOLIDARITE.getCodeChamp());
		}
		return codeCotisation;
	}

	public boolean isFormatExcel() {
		return false;
	}

	public HSSFWorkbook getFluxFormatExcel(EOPaiement paiement,
			boolean estCotisationASolidarite) {
		// TODO Auto-generated method stub
		return null;
	}

}
