package org.cocktail.peche.metier.miseenpaiement;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

import com.webobjects.foundation.NSArray;

/**
 * Cette classe représente un flux de paiement.
 * 
 * @author Pascal MACOUIN
 */
public class FluxPaiement {
	/** Logger. */
	private static Logger log = Logger.getLogger(FluxPaiement.class);

	/** Objet permettant de formatter le flux de paiement. */
	private IFormatFluxPaiement formatFluxPaiement;
	/** Paramètres nécessaires à la mise en paiement. */
	private EOParametresFluxPaiement parametresFluxPaiement;
	/** Nom du flux de mise en paiement. */
	private String nom;
	/** Données du flux de mise en paiement. */
	private StringBuilder flux;
	
	/** Données du flux de mise en paiement. */
	private HSSFWorkbook fluxFormatExcel;
	
	private boolean formatExcel;
	
	
	/**
	 * Constructeur.
	 * 
	 * @param formatFluxPaiement le format du flux à générer
	 * @param parametresFluxPaiement les paramètres nécessaires à la mise en paiement
	 */
	public FluxPaiement(Class<? extends IFormatFluxPaiement> formatFluxPaiement, EOParametresFluxPaiement parametresFluxPaiement) {
		try {
			this.formatFluxPaiement = formatFluxPaiement.newInstance();
		} catch (Exception e) {
			throw new RuntimeException("Impossible d'instancier la classe " + formatFluxPaiement, e);
		}
		this.formatFluxPaiement.setParametresFluxPaiement(parametresFluxPaiement);
		this.parametresFluxPaiement = parametresFluxPaiement;
		this.formatExcel = this.formatFluxPaiement.isFormatExcel();
		}
	
	/**
	 * Générer le flux de mise en paiement.
	 * 
	 * @param paiement le paiement
	 * @throws Exception 
	 */
	public void genererFlux(EOPaiement paiement, boolean estCotisationASolidarite) throws Exception {
		// Nom du flux
		nom = formatFluxPaiement.getNomFlux(paiement);
		
		// En-tête du flux
		String entete = formatFluxPaiement.getLigneEntete();
		if (entete != null) {
			flux = new StringBuilder(entete);
		} else {
			flux = new StringBuilder();
		}
		
		NSArray<EOFluxMiseEnPaiement> listFluxMiseEnPaiement = paiement.toListeFluxMiseEnPaiement(null, formatFluxPaiement.getTri(), false);
		
		for (EOFluxMiseEnPaiement fluxMiseEnPaiement : listFluxMiseEnPaiement) {
			appendPaiement(fluxMiseEnPaiement, estCotisationASolidarite);
		}
	}
	
	/**
	 * Générer le flux de mise en paiement.
	 * 
	 * @param paiement le paiement
	 * @throws Exception 
	 */
		public void genererFluxFormatExcel(EOPaiement paiement, boolean estCotisationASolidarite) throws Exception {
		// Nom du flux
		nom = formatFluxPaiement.getNomFlux(paiement);
		
		fluxFormatExcel = formatFluxPaiement.getFluxFormatExcel(paiement, estCotisationASolidarite);
		
	}
	
	
	/**
	 * Ajoute un paiement dans le flux.
	 * 
	 * @param fluxMiseEnPaiement les données du flux de paiement à ajouter
	 * @throws Exception 
	 */
	private void appendPaiement(EOFluxMiseEnPaiement fluxMiseEnPaiement, boolean estCotisationASolidarite) throws Exception {
		// On ne sort pas les paiements à zéro
		if (fluxMiseEnPaiement.brutPaye().compareTo(BigDecimal.ZERO) == 0) {
			log.info("La personne " + fluxMiseEnPaiement.nom() + " " + fluxMiseEnPaiement.prenom() + " (insee : " + fluxMiseEnPaiement.numeroInsee() + ") a un brut à payer à zéro."
					+ " Flux de mise en paiement id " + fluxMiseEnPaiement.primaryKey() + " n'a pas été pris en compte dans ce flux.");
			return;
		}
		
		String detail = formatFluxPaiement.getLigneDetail(fluxMiseEnPaiement, estCotisationASolidarite);
		
		if (detail != null) {
			if (flux.length() > 0) {
				flux.append("\n");
			}
			flux.append(detail);
		}
	}
	
	/**
	 * retourne les paramètres nécessaires à la mise en paiement.
	 * 
	 * @return les paramètres nécessaires à la mise en paiement
	 */
	public EOParametresFluxPaiement getParametresFluxPaiement() {
		return parametresFluxPaiement;
	}

	/**
	 * retourne le flux généré.
	 * 
	 * @return le flux généré (jamais <code>null</code>)
	 */
	public String getFlux() {
		if (flux == null) {
			return "";
		}
		
		return flux.toString();
	}

	/**
	 * Retourne le nom du flux généré.
	 * 
	 * @return le nom du flux généré
	 */
	public String getNom() {
		return nom;
	}

	public boolean isFormatExcel() {
		return formatExcel;
	}

	public void setFormatExcel(boolean formatExcel) {
		this.formatExcel = formatExcel;
	}

	public HSSFWorkbook getFluxFormatExcel() {
		return fluxFormatExcel;
	}

	public void setFluxFormatExcel(HSSFWorkbook fluxFormatExcel) {
		this.fluxFormatExcel = fluxFormatExcel;
	}
}
