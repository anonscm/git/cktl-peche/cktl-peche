package org.cocktail.peche.metier.miseenpaiement;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.peche.Application;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.outils.LogoEdition;
import org.cocktail.peche.outils.OutilsValidation;
import org.cocktail.peche.services.IPeriodeService;

import com.google.inject.Inject;
import com.webobjects.foundation.NSArray;

/**
 * Classe permettant de générer un flux XML nécessaire à l'édition d'un bordereau de paiement.
 */
public class FluxXmlBordereauPaiement {

	/** logger. */
	private static Logger log = Logger.getLogger(FluxXmlBordereauPaiement.class);

	private NumberFormat formatCleInsee;
	private NumberFormat formatMois;
	private EnseignantsVacatairesCtrl enseignantsVacatairesCtrl;
	private boolean hasStatutaires;
	private boolean hasVacataires;
	
	@Inject
	private IPeriodeService periodeService;
	
	@Inject
	private IPecheParametres pecheParametres;

	/**
	 * Constructeur.
	 */
	public FluxXmlBordereauPaiement() {
		formatCleInsee = NumberFormat.getIntegerInstance();
		formatCleInsee.setMinimumIntegerDigits(2);
		formatMois = NumberFormat.getIntegerInstance();
		formatMois.setMinimumIntegerDigits(2);

		periodeService = Application.application().injector().getInstance(IPeriodeService.class);
		pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);
		
	}

	/**
	 * Retourne le flux XML nécessaire à l'édition d'un bordereau de paiement.
	 * 
	 * @param paiement un paiement
	 * @return le flux XML
	 */
	public String getFlux(EOPaiement paiement) {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			// Xml des structures
			String fluxNiveauxStructures = getFluxNiveauxStructures(paiement);

			w.setEscapeSpecChars(true);

			w.startDocument();
			w.startElement("bordereau_paiement");

			w.startElement("paiement");
			w.writeElement("numero_paiement", StringCtrl.checkString(paiement.numeroPaiement().toString()));

			if (hasStatutaires == hasVacataires) {
				ecrireBaliseStatut(w, "", "");
			} else if (hasStatutaires) {
				ecrireBaliseStatut(w, "S", "Statutaire");
			} else if (hasVacataires) {
				ecrireBaliseStatut(w, "V", "Vacataire");
			}

			String anneeUniversitaire = !getPecheParametres().isFormatAnneeExerciceAnneeCivile() ? paiement.anneePaiement() + "/" + (paiement.anneePaiement() + 1) : String.valueOf(paiement.anneePaiement());
			
			w.writeElement("annee_universitaire", anneeUniversitaire);

			w.startElement("periode_paiement");
			EOPeriode periode = getPeriodeService().rechercherPeriode(paiement.editingContext(), EOPecheParametre.getChoixTypePeriode(paiement.editingContext()) + getPecheParametres().getCodeFormatAnnee(), paiement.moisPaiement());
			
			if (periode != null) {
				w.writeElement("numero_dernier_mois_periode", StringCtrl.checkString(OutilsValidation.formatter(periode.noDernierMois(), formatMois)));
				w.writeElement("libelle", StringCtrl.checkString(periode.libelle()));
			} else {
				w.writeElement("numero_dernier_mois_periode", "");
				w.writeElement("libelle", "");
			}
			w.endElement();

			w.writeElement("libelle", StringCtrl.checkString(paiement.libelle()));
			w.writeElement("etablissement", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY)));

			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT") != null) {
				w.writeElement("president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT")));
			}
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT") != null) {
				w.writeElement("signature_president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT")));
			}
			String cLogo = LogoEdition.cheminLogoEtablissement(paiement.editingContext());
			
			if (cLogo != null) {
				w.writeElement("logo_etablissement", StringCtrl.checkString(cLogo));
			}

			w.writeElement("date_paiement", StringCtrl.checkString(DateCtrl.dateToString(paiement.datePaiement())));
			w.writeElement("date_bordereau", StringCtrl.checkString(DateCtrl.dateToString(paiement.dateBordereau(), "dd/MM/yyyy")));
			w.writeElement("date_arrete", StringCtrl.checkString(DateCtrl.dateToString(paiement.dateArrete())));
			w.writeElement("date_flux", StringCtrl.checkString(DateCtrl.dateToString(paiement.dateFlux())));
			w.endElement();

			w.writeString(fluxNiveauxStructures);

			w.endElement();
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}

		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");
		
		return sw.toString();
	}

	/**
	 * Génération du XML au niveaux des structures.
	 * 
	 * @param paiement un paiement
	 * @return Le XML structure
	 * @throws IOException
	 */
	private String getFluxNiveauxStructures(EOPaiement paiement) throws IOException {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		w.setEscapeSpecChars(true);
		
		DecimalFormat df = new DecimalFormat("0.00#");
		
		hasStatutaires = false;
		hasVacataires = false;

		enseignantsVacatairesCtrl = new EnseignantsVacatairesCtrl(null,paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(paiement, new GrouperMiseEnPaiementPourBordereau());
		Iterator<MiseEnPaiementDataBean> iterateur = listeMiseEnPaiement.iterator();

		// Première lecture
		MiseEnPaiementDataBean miseEnPaiementDataBean = null;
		if (iterateur.hasNext()) {
			miseEnPaiementDataBean = iterateur.next();
		}

		BigDecimal totalHeuresAPayerHetdStructure = BigDecimal.ZERO;
		BigDecimal totalMontantAPayerBrutStructure = BigDecimal.ZERO;
		BigDecimal totalMontantAPayerChargeStructure = BigDecimal.ZERO;
		BigDecimal soustotalMontantAPayerBrutTypeContrat = BigDecimal.ZERO;
		BigDecimal soustotalMontantAPayerChargeTypeContrat = BigDecimal.ZERO;
		BigDecimal totalGeneralMontantAPayerBrut = BigDecimal.ZERO;
		BigDecimal totalGeneralMontantAPayerCharge = BigDecimal.ZERO;		

		String codeTypeContratTravailenCours;

		while (miseEnPaiementDataBean != null) {
			EOStructure structure = miseEnPaiementDataBean.getStructure();
			w.startElement("structure");
			w.writeElement("code", StringCtrl.checkString(structure.cStructure()));
			w.writeElement("libelle_court", StringCtrl.checkString(structure.lcStructure()));
			w.writeElement("libelle_long", StringCtrl.checkString(structure.llStructure()));

			totalHeuresAPayerHetdStructure = BigDecimal.ZERO;
			totalMontantAPayerBrutStructure = BigDecimal.ZERO;
			totalMontantAPayerChargeStructure = BigDecimal.ZERO;

			do {

				EOActuelEnseignant enseignant = miseEnPaiementDataBean.getService().enseignant();
				EOIndividu individu = enseignant.toIndividu();
				
				String codeTypeContratTravail = "";
				String libelleCourtTypeContratTravail = "";
				String libelleLongTypeContratTravail = "";
				
				if (enseignant.isStatutaire(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
					// utilisation du contrat pour définir Titulaire ou Non
					if (enseignant.isTitulaire(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
						codeTypeContratTravail = "Titulaires";
						libelleCourtTypeContratTravail = "Titulaires";
						libelleLongTypeContratTravail = "Titulaires";
					} else {
						codeTypeContratTravail = "Non Titulaires";
						libelleCourtTypeContratTravail = "Non Titulaires";
						libelleLongTypeContratTravail = "Non Titulaires";
					}
				} else {
					// Libellé vacation
					codeTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
					libelleCourtTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
					libelleLongTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
				}

				codeTypeContratTravailenCours = codeTypeContratTravail;

				w.startElement("type_contrat");
				w.writeElement("code", StringCtrl.checkString(codeTypeContratTravail));
				w.writeElement("libelle_court", StringCtrl.checkString(libelleCourtTypeContratTravail));
				w.writeElement("libelle_long", StringCtrl.checkString(libelleLongTypeContratTravail));

				// si non statutaire, calcul sous-totaux par type de contrat
				// si titulaire, calcul sous-totaux par structure
				//if (enseignant.isStatutaire()) {
					soustotalMontantAPayerBrutTypeContrat = BigDecimal.ZERO;
					soustotalMontantAPayerChargeTypeContrat = BigDecimal.ZERO;
				//}
				
				do {
					// si non statutaire, calcul sous-totaux par type de contrat
					// si titulaire, calcul sous-totaux par structure
				//	if (!enseignant.isStatutaire()) {
				//		soustotalMontantAPayerBrutTypeContrat = BigDecimal.ZERO;
				//		soustotalMontantAPayerChargeTypeContrat = BigDecimal.ZERO;
				//	}
				
					w.startElement("enseignant");
					w.writeElement("nom", StringCtrl.checkString(individu.nomAffichage().toUpperCase()));
					w.writeElement("prenom", StringCtrl.checkString(individu.prenomAffichage()));
					w.writeElement("numero_insee", StringCtrl.checkString(individu.indNoInsee()));
					w.writeElement("cle_insee", StringCtrl.checkString(OutilsValidation.formatter(individu.indCleInsee(), formatCleInsee)));
					w.writeElement("numero_prise_en_charge", StringCtrl.checkString(individu.toPersonnel().npc()));

					if (enseignant.isStatutaire(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
						ecrireBaliseStatut(w, "S", "Statutaire");
						hasStatutaires = true;
					} else {
						ecrireBaliseStatut(w, "V", "Vacataire");
						hasVacataires = true;
						w.writeElement("type_vacation", StringCtrl.checkString(enseignantsVacatairesCtrl.getLibelleTypeAssocieVacation(enseignant)));
					}

					EOCorps corps = enseignant.getCorps(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile());
					if (corps == null) {
						w.startElement("corps");
						w.writeElement("code", "");
						w.writeElement("libelle_court", "");
						w.writeElement("libelle_long", "");
						w.endElement();
						NSArray<EOContrat> contrats = enseignant.getListeContrats(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile());
						if (contrats.size() > 0) {
							EOTypeContratTravail typeContratTravail = contrats.get(0).toTypeContratTravail();
							if (typeContratTravail != null) {								
								w.startElement("contrat");						
								w.writeElement("code", typeContratTravail.cTypeContratTrav()); 
								w.writeElement("libelle_court", typeContratTravail.lcTypeContratTrav());
								w.writeElement("libelle_long", typeContratTravail.llTypeContratTrav());
								w.endElement();
							}
						}						
					} else {
						w.startElement("corps");
						w.writeElement("code", StringCtrl.checkString(corps.cCorps()));
						w.writeElement("libelle_court", StringCtrl.checkString(corps.lcCorps()));
						w.writeElement("libelle_long", StringCtrl.checkString(corps.llCorps()));
						w.endElement();
					}

					// Profession
					IVacataires vacation = enseignantsVacatairesCtrl.getContratVacataires(enseignant);

					String libelle = "";
					String detail = "";

					if (vacation != null && vacation.getToProfession() != null) {
						libelle = vacation.getToProfession().proLibelle();
						detail = vacation.getToProfession().proDetails();
					}

					w.startElement("profession");
					w.writeElement("libelle", StringCtrl.checkString(libelle));				
					w.writeElement("detail", StringCtrl.checkString(detail));
					w.endElement();

					if (miseEnPaiementDataBean.getTauxBrut() != null) {
						w.writeElement("taux_brut", miseEnPaiementDataBean.getTauxBrut().toString());
					}

					if (miseEnPaiementDataBean.getTauxCharge() != null) {
						w.writeElement("taux_charge", miseEnPaiementDataBean.getTauxCharge().toString());
					}

					w.writeElement("heures_a_payer", df.format(miseEnPaiementDataBean.getHeuresAPayer()));
					w.writeElement("heures_a_payer_hetd", df.format(miseEnPaiementDataBean.getHeuresAPayerHetd()));
					
					w.writeElement("montant_a_payer_brut", miseEnPaiementDataBean.getMontantAPayerBrut().toString());
					w.writeElement("montant_a_payer_charge", miseEnPaiementDataBean.getMontantAPayerCharge().toString());

					if (miseEnPaiementDataBean.getHeuresAPayerHetd() != null) {
						totalHeuresAPayerHetdStructure = totalHeuresAPayerHetdStructure.add(miseEnPaiementDataBean.getHeuresAPayerHetd());
					}
					if (miseEnPaiementDataBean.getMontantAPayerBrut() != null) {
						totalMontantAPayerBrutStructure = totalMontantAPayerBrutStructure.add(miseEnPaiementDataBean.getMontantAPayerBrut());
					}
					if (miseEnPaiementDataBean.getMontantAPayerCharge() != null) {
						totalMontantAPayerChargeStructure = totalMontantAPayerChargeStructure.add(miseEnPaiementDataBean.getMontantAPayerCharge());
					}
					if (miseEnPaiementDataBean.getMontantAPayerBrut() != null) {						
						soustotalMontantAPayerBrutTypeContrat = soustotalMontantAPayerBrutTypeContrat.add(miseEnPaiementDataBean.getMontantAPayerBrut());
					}
					if (miseEnPaiementDataBean.getMontantAPayerCharge() != null) {
						soustotalMontantAPayerChargeTypeContrat = soustotalMontantAPayerChargeTypeContrat.add(miseEnPaiementDataBean.getMontantAPayerCharge());
					}

					w.endElement(); // Fin enseignant

					if (iterateur.hasNext()) {
						miseEnPaiementDataBean = iterateur.next();


						enseignant = miseEnPaiementDataBean.getService().enseignant();
						individu = enseignant.toIndividu();

						if (enseignant.isStatutaire(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
							// utilisation du contrat pour définir Titulaire ou Non
							if (enseignant.isTitulaire(paiement.anneePaiement(), getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
								codeTypeContratTravail = "Titulaires";
								libelleCourtTypeContratTravail = "Titulaires";
								libelleLongTypeContratTravail = "Titulaires";
							} else {
								codeTypeContratTravail = "Non Titulaires";
								libelleCourtTypeContratTravail = "Non Titulaires";
								libelleLongTypeContratTravail = "Non Titulaires";
							}
						} else {
							// libellé vacation
							codeTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
							libelleCourtTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
							libelleLongTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(enseignant);
						}
					} else {
						miseEnPaiementDataBean = null;
					}
				} while (miseEnPaiementDataBean != null 
						&& structure == miseEnPaiementDataBean.getStructure()
						&& codeTypeContratTravailenCours.equals(codeTypeContratTravail));

				w.writeElement("soustotal_type_contrat_montant_a_payer_brut", soustotalMontantAPayerBrutTypeContrat.toString());
				w.writeElement("soustotal_type_contrat_montant_a_payer_charge", soustotalMontantAPayerChargeTypeContrat.toString());

				w.endElement(); // fin type_contrat			

			} while (miseEnPaiementDataBean != null && structure == miseEnPaiementDataBean.getStructure());


			w.writeElement("total_montant_a_payer_brut", totalMontantAPayerBrutStructure.toString());
			w.writeElement("total_montant_a_payer_charge", totalMontantAPayerChargeStructure.toString());
			w.writeElement("total_heures_a_payer_hetd", df.format(totalHeuresAPayerHetdStructure));

			totalGeneralMontantAPayerBrut = totalGeneralMontantAPayerBrut.add(totalMontantAPayerBrutStructure);
			totalGeneralMontantAPayerCharge = totalGeneralMontantAPayerCharge.add(totalMontantAPayerChargeStructure);	

			w.endElement(); // Fin structure
		}

		w.writeElement("total_general_montant_a_payer_brut", totalGeneralMontantAPayerBrut.toString());
		w.writeElement("total_general_montant_a_payer_charge", totalGeneralMontantAPayerCharge.toString());

		w.close();
		return sw.toString();
	}

	/**
	 * Ecrire le groupe balise statut.
	 * @param w un XML writer
	 * @param code un code
	 * @param libelle un libelle
	 * @throws IOException 
	 */
	private void ecrireBaliseStatut(CktlXMLWriter w, String code, String libelle) throws IOException {
		w.startElement("statut");
		w.writeElement("code", code);
		w.writeElement("libelle", StringCtrl.checkString(libelle));
		w.endElement();
	}

	public IPeriodeService getPeriodeService() {
		return periodeService;
	}

	public void setPeriodeService(IPeriodeService periodeService) {
		this.periodeService = periodeService;
	}

	public IPecheParametres getPecheParametres() {
		return pecheParametres;
	}

	public void setPecheParametres(IPecheParametres pecheParametres) {
		this.pecheParametres = pecheParametres;
	}
	
}
