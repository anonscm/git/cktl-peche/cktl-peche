package org.cocktail.peche.metier.miseenpaiement;

import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.Application;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.EnseignantsStatutairesCtrl;
import org.cocktail.peche.components.controlers.HceCtrl;
import org.cocktail.peche.components.controlers.UECtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOReh;
import org.cocktail.peche.entity.EORepartUefEtablissement;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.LogoEdition;
import org.cocktail.peche.outils.OutilsValidation;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

/**
 * Classe permettant de générer un flux XML nécessaire à l'édition d'un justificatif de paiement.
 */
public class FluxXmlJustificatifPaiementStatutaires {

	/** logger. */
	private static Logger log = Logger.getLogger(FluxXmlJustificatifPaiementStatutaires.class);

	private EOEditingContext context;
	private NumberFormat formatCleInsee;
	private NumberFormat formatMois;
	private EnseignantsStatutairesCtrl enseignantsStatutairesCtrl;
	private UECtrl uectrl;
	private Map<String, EOHistoriqueDemande> mapVisas = new HashMap<String, EOHistoriqueDemande>();
	private HceCtrl hceCtrl;
	@Inject
	private IPecheParametres pecheParametres;
	
	private Integer anneeConcernee;
	/**
	 * Constructeur.
	 * @param editingContext l'editingContext
	 */
	public FluxXmlJustificatifPaiementStatutaires(EOEditingContext editingContext) {
		context = editingContext;
		formatCleInsee = NumberFormat.getIntegerInstance();
		formatCleInsee.setMinimumIntegerDigits(2);
		formatMois = NumberFormat.getIntegerInstance();
		formatMois.setMinimumIntegerDigits(2);
		pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);
		anneeConcernee = getPecheParametres().getAnneeUniversitaire();
		uectrl = new UECtrl(editingContext, anneeConcernee);
		hceCtrl = new HceCtrl(editingContext);
	}

	/**
	 * Retourne le flux XML nécessaire à l'édition d'un justificatif de paiement.
	 * 
	 * @param paiement un paiement
	 * @return le flux XML
	 */
	public String getFlux(EOPaiement paiement) {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			
			List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(paiement, new GrouperMiseEnPaiementPourBordereau());
			// pour initialiser l'année concernée , nous prenons le 1er élément mis en paiement 
			if ((listeMiseEnPaiement.size() > 0) && (listeMiseEnPaiement.get(0) != null) && (listeMiseEnPaiement.get(0).getService()) != null) {			
				anneeConcernee = listeMiseEnPaiement.get(0).getService().annee();
			}
			
			// Xml des structures
			String fluxNiveauxStructures = getFluxNiveauxEnseignantsStatutaires(paiement);

			w.setEscapeSpecChars(true);

			w.startDocument();
			w.startElement("justificatif_paiement");

			w.startElement("paiement");
			w.writeElement("numero_paiement", paiement.numeroPaiement().toString());

			ecrireBaliseStatut(w, "S", "Statutaire");
			
			if (getPecheParametres().isFormatAnneeExerciceAnneeCivile()) {
				w.writeElement("annee_universitaire", String.valueOf(anneeConcernee));
			} else {
				w.writeElement("annee_universitaire", anneeConcernee + "/" + (anneeConcernee + 1));
			}				

			w.writeElement("libelle", StringCtrl.checkString(paiement.libelle()));
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY) != null) {
				w.writeElement("etablissement", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY)));
			}
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT") != null) {
				w.writeElement("president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT")));
			}
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT") != null) {
				w.writeElement("signature_president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT")));
			}

			String cLogo = LogoEdition.cheminLogoEtablissement(paiement.editingContext());
			if (cLogo != null) {
				w.writeElement("logo_etablissement", cLogo);
			}

			w.writeElement("date_paiement", DateCtrl.dateToString(paiement.datePaiement()));
			w.writeElement("date_bordereau", DateCtrl.dateToString(paiement.dateBordereau(), "dd/MM/yyyy"));
			w.writeElement("date_arrete", DateCtrl.dateToString(paiement.dateArrete()));
			w.writeElement("date_flux", DateCtrl.dateToString(paiement.dateFlux()));
			w.endElement();

			w.writeString(fluxNiveauxStructures);

			w.endElement();
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}

		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");
		
		return sw.toString();
	}


	/**
	 * Génération du XML au niveau des enseignants statutaires.
	 * 
	 * @param paiement un paiement
	 * @return Le XML structure
	 * @throws IOException
	 */
	private String getFluxNiveauxEnseignantsStatutaires(EOPaiement paiement) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		DecimalFormat df = new DecimalFormat("0.00#");
				
		w.setEscapeSpecChars(true);

		List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(paiement, new GrouperMiseEnPaiementPourBordereau());
		List<EOActuelEnseignant> listeEnseignants = new ArrayList<EOActuelEnseignant>();

		enseignantsStatutairesCtrl = new EnseignantsStatutairesCtrl(context, anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile());

		for (int i = 0; i < listeMiseEnPaiement.size(); i++) {
			if (listeMiseEnPaiement.get(i) != null) {
				EOService eoservice = listeMiseEnPaiement.get(i).getService();
				if (eoservice != null)	{
					if (!listeEnseignants.contains(eoservice.enseignant())) {
						listeEnseignants.add(eoservice.enseignant());			
					}
				}
			}
		}
		
		//Qualifier pour ne pas prendre en compte les répartitions croisées en attente et refusées dans l'affichage des enseignements
		EOQualifier qualifier = ERXQ.and(
				ERXQ.notEquals(EOServiceDetail.ETAT_KEY, Constante.ATTENTE),
				ERXQ.notEquals(EOServiceDetail.ETAT_KEY, Constante.REFUSE)
			);

		int indexEnseignant;

		for (indexEnseignant = 0; indexEnseignant < listeEnseignants.size(); indexEnseignant++)
		{
			EOActuelEnseignant lenseignant = listeEnseignants.get(indexEnseignant);
			EOIndividu individu = lenseignant.toIndividu();

			w.startElement("enseignant");
			w.writeElement("nom", StringCtrl.checkString(individu.nomAffichage().toUpperCase()));
			w.writeElement("prenom", StringCtrl.checkString(individu.prenomAffichage()));
			w.writeElement("numero_insee", StringCtrl.checkString(individu.indNoInsee()));
			w.writeElement("cle_insee", OutilsValidation.formatter(individu.indCleInsee(), formatCleInsee));
			w.writeElement("numero_prise_en_charge", OutilsValidation.retournerNonNull(individu.toPersonnel().npc()));
			if (individu.getDtNaissance() != null) {
				w.writeElement("date_naissance", formatter.format(individu.getDtNaissance()));
			}
			w.writeElement("corps", StringCtrl.checkString(enseignantsStatutairesCtrl.getLibelleLongCorps(lenseignant)));
			NSArray<EOContrat> contrats = lenseignant.getListeContrats(anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			if (contrats.size() > 0) {
				EOTypeContratTravail typeContratTravail = contrats.get(0).toTypeContratTravail();
				if (typeContratTravail != null) {
					w.writeElement("contrat", StringCtrl.checkString(typeContratTravail.llTypeContratTrav()));
				}
			}
			
			w.writeElement("statut", StringCtrl.checkString(enseignantsStatutairesCtrl.getStatut(lenseignant)));
	
			
			if (enseignantsStatutairesCtrl.getComposante(lenseignant) != null) {
				w.writeElement("composante_directe", StringCtrl.checkString(enseignantsStatutairesCtrl.getComposante(lenseignant).llStructure()));
			} else {
				w.writeElement("composante_directe", "");
			}

			w.writeElement("service_statutaire", df.format(lenseignant.getService(anneeConcernee).getHeuresService()));
			w.writeElement("report_annee_anterieure", df.format(-enseignantsStatutairesCtrl.getReportAnneeAnterieure(lenseignant, enseignantsStatutairesCtrl.getAnneeUniversitairePrecedente(anneeConcernee))));			
			w.writeElement("modalite_service", df.format(-hceCtrl.getTotalNbHeuresModaliteService(lenseignant, anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())));
			w.writeElement("decharges_enregistrees", df.format(-hceCtrl.getTotalNbHeuresDecharges(individu, anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())));
			w.writeElement("minoration_service", df.format(-hceCtrl.getTotalNbHeuresMinorations(lenseignant, anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())));
			w.writeElement("service_du", df.format(enseignantsStatutairesCtrl.getServiceDu(lenseignant, anneeConcernee)));

			w.writeElement("service_realise", df.format(enseignantsStatutairesCtrl.getServiceRealise(lenseignant)));

			w.writeElement("hcomp_realise", df.format(enseignantsStatutairesCtrl.getHcompRealise(lenseignant.getService(anneeConcernee))));

			if (lenseignant.getService(anneeConcernee).getHeuresGratuites().doubleValue() > 0) {			
				w.writeElement("heures_gratuites", df.format(lenseignant.getService(anneeConcernee).getHeuresGratuites()));
			}

			if (lenseignant.getService(anneeConcernee).getHeuresAReporterAttribuees().doubleValue() > 0) {
				w.writeElement("heures_a_reporter", df.format(lenseignant.getService(anneeConcernee).getHeuresAReporterAttribuees()));
			}

			if (lenseignant.getService(anneeConcernee).getHeuresAPayer().doubleValue() > 0) {
				w.writeElement("heures_a_payer", df.format(lenseignant.getService(anneeConcernee).getHeuresAPayer()));
			}

			if (lenseignant.getService(anneeConcernee).getHeuresPayees().doubleValue() > 0) {
				w.writeElement("heures_payees", df.format(lenseignant.getService(anneeConcernee).getHeuresPayees()));
			}
			EODemande demande = enseignantsStatutairesCtrl.getDemande(lenseignant.getService(anneeConcernee));
			NSArray<EOCircuitValidation > listeCircuits = demande.rechercherCircuitsEmpruntes(true);
			if (listeCircuits.size() > 0) {
				for (EOCircuitValidation circuitValidation : listeCircuits) {
					w.startElement("circuit");
					w.writeElement("libelle", StringCtrl.checkString(getLibelleFicheTypeCircuit(circuitValidation)));
					NSMutableArray<EOEtape> etapesUtiliseesTemp = circuitValidation.etapesUtiliseesSuivantOrdreChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), TypeCheminPeche.REFUSER.getCodeTypeChemin()).mutableClone();
					for (int iEtape = 0; iEtape < etapesUtiliseesTemp.size(); iEtape++) {
						EOEtape etape = etapesUtiliseesTemp.get(iEtape);
						if (etape.isFinale()) {
							etapesUtiliseesTemp.remove(etape);
						}
					}
					initVisa(demande, circuitValidation);
					for (EOEtape etape : etapesUtiliseesTemp) {
						w.startElement("etape");
						w.writeElement("libelle_visa", StringCtrl.checkString(EtapePeche.getEtape(etape).getLibelleVisa()));
						if (mapVisas.get(etape.codeEtape()) != null) {
							w.writeElement("date_visa", formatter.format(mapVisas.get(etape.codeEtape()).dateModification()));
						} else {
							w.writeElement("date_visa", "");
						}
						w.endElement();
					}
					w.endElement();
				}
			}
			if (lenseignant.isStatutaire(anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
				ecrireBaliseStatut(w, "S", "Statutaire");

			} else {
				ecrireBaliseStatut(w, "V", "Vacataire");

			}

			MiseEnPaiementDataBean miseEnPaiementDataBean = null;
			Double totalGeneral = 0.0;
			
			for (int i = 0; i < listeMiseEnPaiement.size(); i++) {
				if (listeMiseEnPaiement.get(i) != null) {
					miseEnPaiementDataBean = listeMiseEnPaiement.get(i);
					EOActuelEnseignant enseignant = miseEnPaiementDataBean.getService().enseignant();
					if (enseignant.equals(listeEnseignants.get(indexEnseignant))) {

						Double totalGeneralHeuresOffreFormation = 0.0;
						EOStructure structureAffectation = miseEnPaiementDataBean.getStructure();
						EOService service = miseEnPaiementDataBean.getService();
						//On ne prend pas en compte les répartitions croisées en attente et refusées dans l'affichage des enseignements
						NSArray<EOServiceDetail> listeServiceDetail = service.listeServiceDetails(qualifier);

						// ENSEIGNEMENTS AFFECTATION
						w.startElement("offre");
						w.writeElement("libelle", "Offre de formation");
						boolean premiereStructure = true;
						Double totalHeuresRealiseesStructure = 0.0;

						for (int j = 0; j < listeServiceDetail.size(); j++) {
							EOServiceDetail serviceDetail = listeServiceDetail.get(j);
							EOAP eoAP = serviceDetail.composantAP();
							List<IComposant> parents = null;
							if (eoAP != null) {
								parents = eoAP.parents();
							}
							IComposant composant = null;
							if (parents != null) {
								composant = parents.get(0);
							}
							List<IStructure> structures = null;
							if (composant != null) {
								structures = (List<IStructure>) composant.structures();
							}
							EOStructure lieuParentAP = null;
							if (structures != null && !structures.isEmpty()) {
								lieuParentAP = (EOStructure)((List<IStructure>) composant.structures()).get(0);
							}
							if (lieuParentAP != null && lieuParentAP == structureAffectation) {
								if (premiereStructure) {
									w.startElement("structure");
									w.writeElement("code", StringCtrl.checkString(lieuParentAP.cStructure()));
									w.writeElement("libelle_court", StringCtrl.checkString(lieuParentAP.lcStructure()));
									w.writeElement("libelle_long", StringCtrl.checkString("Composante d'affectation : " + lieuParentAP.llStructure()));
									premiereStructure = false;
									totalHeuresRealiseesStructure = 0.0;
								}
								w.startElement("enseignement");
								w.writeElement("libelle", ComposantAffichageHelper.getInstance().affichageEnseignement(composant));							 
								if (serviceDetail.composantAP() != null && serviceDetail.composantAP().typeAP() != null) {
									w.writeElement("type", StringCtrl.checkString(serviceDetail.composantAP().typeAP().code()));
								}
								if (serviceDetail.heuresRealisees() != null) {								
									w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
									totalHeuresRealiseesStructure = totalHeuresRealiseesStructure + serviceDetail.heuresRealisees();
								}
								w.endElement();
							}
						}
						if (totalHeuresRealiseesStructure.intValue() != 0) {						
							w.writeElement("total_heures_realisees", df.format(totalHeuresRealiseesStructure));
							totalGeneralHeuresOffreFormation = totalGeneralHeuresOffreFormation + totalHeuresRealiseesStructure;
							w.endElement(); // Fin structure
						}

						// ENSEIGNEMENTS AUTRES AFFECTATIONS

						premiereStructure = true;
						totalHeuresRealiseesStructure = 0.0;

						for (int j = 0; j < listeServiceDetail.size(); j++) {
							EOServiceDetail serviceDetail = listeServiceDetail.get(j);
							EOAP eoAP = serviceDetail.composantAP();
							List<IComposant> parents = null;
							if (eoAP != null) {
								parents = eoAP.parents();
							}
							IComposant composant = null;
							if (parents != null) {
								composant = parents.get(0);
							}
							List<IStructure> structures = null;
							if (composant != null) {
								structures = (List<IStructure>) composant.structures();
							}
							EOStructure lieuParentAP = null;
							if (structures != null && !structures.isEmpty()) {
								lieuParentAP = (EOStructure)(composant.structures().get(0));
							}

							if (lieuParentAP != null && lieuParentAP != structureAffectation) {
								if (premiereStructure) {
									w.startElement("structure");
									w.writeElement("code", StringCtrl.checkString(lieuParentAP.cStructure()));
									w.writeElement("libelle_court", StringCtrl.checkString(lieuParentAP.lcStructure()));
									w.writeElement("libelle_long", StringCtrl.checkString(lieuParentAP.llStructure()));
									premiereStructure = false;
									totalHeuresRealiseesStructure = 0.0;
								}
								w.startElement("enseignement");
								if (composant != null) {
									w.writeElement("libelle", ComposantAffichageHelper.getInstance().affichageEnseignement(composant));
								}
								if (serviceDetail.composantAP() != null && serviceDetail.composantAP().typeAP() != null) {
									w.writeElement("type", StringCtrl.checkString(serviceDetail.composantAP().typeAP().code()));
								}
								if (serviceDetail.heuresRealisees() != null) {
									w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
									totalHeuresRealiseesStructure = totalHeuresRealiseesStructure + serviceDetail.heuresRealisees();
								}
								w.endElement();
							}
							
							if (lieuParentAP == null && composant != null) {
								if (premiereStructure) {
									w.startElement("structure");
									w.writeElement("code", "UNDEF");
									w.writeElement("libelle_court", "Composante non définie");
									w.writeElement("libelle_long", "Composante non définie");
									premiereStructure = false;
									totalHeuresRealiseesStructure = 0.0;
								}
								w.startElement("enseignement");
								w.writeElement("libelle", ComposantAffichageHelper.getInstance().affichageEnseignement(composant));
								if (serviceDetail.composantAP() != null && serviceDetail.composantAP().typeAP() != null) {
									w.writeElement("type", StringCtrl.checkString(serviceDetail.composantAP().typeAP().code()));
								}
								if (serviceDetail.heuresRealisees() != null) {
									w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
									totalHeuresRealiseesStructure = totalHeuresRealiseesStructure + serviceDetail.heuresRealisees();
								}
								w.endElement();
							}
						}
						if (totalHeuresRealiseesStructure.intValue() != 0) {						
							w.writeElement("total_heures_realisees", df.format(totalHeuresRealiseesStructure));
							totalGeneralHeuresOffreFormation = totalGeneralHeuresOffreFormation + totalHeuresRealiseesStructure;
							w.endElement(); // Fin structure
						}
						
						w.writeElement("total_offre_heures_realisees", df.format(totalGeneralHeuresOffreFormation));
						totalGeneral = totalGeneral + totalGeneralHeuresOffreFormation;
						totalGeneralHeuresOffreFormation = 0.0;
						w.endElement(); // offre de formation

						// ENSEIGNEMENTS REH
						
						w.startElement("offre");
						w.writeElement("libelle", "REH hors offre de formation");
						
						premiereStructure = true;
						totalHeuresRealiseesStructure = 0.0;

						for (int j = 0; j < listeServiceDetail.size(); j++) {
							EOServiceDetail serviceDetail = listeServiceDetail.get(j);
							EOReh eoReh = serviceDetail.reh();

							if (eoReh != null) {
								if (premiereStructure) {
									w.startElement("structure");
									w.writeElement("code", "REH");
									w.writeElement("libelle_court", "REH");
									w.writeElement("libelle_long", "REH hors offre de formation");
									premiereStructure = false;
									totalHeuresRealiseesStructure = 0.0;
								}
								w.startElement("enseignement");
								w.writeElement("libelle", StringCtrl.checkString(eoReh.libelleCourt()));
								if(serviceDetail.toStructure() != null){
									w.writeElement("composante", StringCtrl.checkString(serviceDetail.toStructure().lcStructure()));
								}
								//w.writeElement("type", " ");
								if (serviceDetail.heuresRealisees() != null) {
									w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
									totalHeuresRealiseesStructure = totalHeuresRealiseesStructure + serviceDetail.heuresRealisees();
								}
								w.endElement();
							}
						}
						if (totalHeuresRealiseesStructure.intValue() != 0) {						
							w.writeElement("total_heures_realisees", df.format(totalHeuresRealiseesStructure));
							totalGeneralHeuresOffreFormation = totalGeneralHeuresOffreFormation + totalHeuresRealiseesStructure;
							w.endElement(); // Fin structure
						}
						
						w.writeElement("total_offre_heures_realisees", df.format(totalGeneralHeuresOffreFormation));
						totalGeneral = totalGeneral + totalGeneralHeuresOffreFormation;
						totalGeneralHeuresOffreFormation = 0.0;
						w.endElement(); // offre de REH hors offre de formation
						
						// ENSEIGNEMENTS UE FLOTTANTES HORS ETABLISSEMENT
						w.startElement("offre");
						w.writeElement("libelle", "UE flottantes hors établissement");
						
						premiereStructure = true;
						totalHeuresRealiseesStructure = 0.0;

						for (int j = 0; j < listeServiceDetail.size(); j++) {
							EOServiceDetail serviceDetail = listeServiceDetail.get(j);
							EOAP eoAP = serviceDetail.composantAP();
							List<IComposant> parents = null;
							if (eoAP != null) {
								parents = eoAP.parents();
							}
							IComposant composant = null;
							if (parents != null) {
								composant = parents.get(0);
							}

							EORepartUefEtablissement eoRepartUefEtablissement = null;

							if (composant != null) {
								eoRepartUefEtablissement = uectrl.getRepartition(composant);
							}

							if (eoRepartUefEtablissement != null) {
								if (premiereStructure) {
									w.startElement("structure");
									w.writeElement("code", eoRepartUefEtablissement.etablissementExterne());
									w.writeElement("libelle_court", eoRepartUefEtablissement.etablissementExterne());
									w.writeElement("libelle_long", eoRepartUefEtablissement.etablissementExterne());
									premiereStructure = false;
									totalHeuresRealiseesStructure = 0.0;
								}
								w.startElement("enseignement");
								if (composant != null) {
									w.writeElement("libelle", ComposantAffichageHelper.getInstance().affichageEnseignement(composant));
									}
								if (serviceDetail.composantAP() != null && serviceDetail.composantAP().typeAP() != null) {
									w.writeElement("type", serviceDetail.composantAP().typeAP().code());
								}
								if (serviceDetail.heuresRealisees() != null) {
									//EOCorps corps = enseignant.getCorps(anneeConcernee,getPecheParametres().isFormatAnneeExerciceAnneeCivile());
									w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
									totalHeuresRealiseesStructure = totalHeuresRealiseesStructure + serviceDetail.heuresRealisees();
								}
								w.endElement();
							}
						}
						if (totalHeuresRealiseesStructure.intValue() != 0) {						
							w.writeElement("total_heures_realisees", df.format(totalHeuresRealiseesStructure));
							totalGeneralHeuresOffreFormation = totalGeneralHeuresOffreFormation + totalHeuresRealiseesStructure;
							w.endElement(); // Fin structure
						}
						
						w.writeElement("total_offre_heures_realisees", df.format(totalGeneralHeuresOffreFormation));
						totalGeneral = totalGeneral + totalGeneralHeuresOffreFormation;
						totalGeneralHeuresOffreFormation = 0.0;
						w.endElement(); // offre de UE flottantes hors établissement						
					}
				}
			} 
			w.writeElement("total_heures_enseignement", df.format(totalGeneral));
			w.endElement(); // fin enseignant
		}

		w.close();
		return sw.toString();
	}

	/**
	 * Retourne le libellé du type de la fiche déterminé en fonction du circuit emprunté.
	 * 
	 * @param circuit le circuit
	 * @return Un libellé du type de la fiche
	 */
	public String getLibelleFicheTypeCircuit(EOCircuitValidation circuit) {
		if (CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit().equals(circuit.codeCircuitValidation())
				|| CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(circuit.codeCircuitValidation())) {
			return "Fiche prévisionnelle";
		} else {
			return "Fiche définitive";
		}
	}

	/**
	 * On récupère les visa des chemins "VALIDER".
	 * @param demande la demande
	 * @param circuit le circuit
	 */
	public void initVisa(EODemande demande, EOCircuitValidation circuit) {

		mapVisas.clear();

		if (demande != null) {
			NSArray<EOHistoriqueDemande> listeHistoriqueDemande = 
					demande.historiqueChronologique(EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(circuit.codeCircuitValidation()));

			for (EOHistoriqueDemande historiqueDemande : listeHistoriqueDemande) {
				EOEtape etapeDepart = historiqueDemande.toEtapeDepart();

				if (etapeDepart != null) {
					EOChemin cheminValider = etapeDepart.chemin(TypeCheminPeche.VALIDER.getCodeTypeChemin());
					if (cheminValider != null
							&& cheminValider.toEtapeArrivee() == historiqueDemande.toEtapeArrivee()) {
						mapVisas.put(etapeDepart.codeEtape(), historiqueDemande);
					}
				}
			}
		}
	}

	/**
	 * Ecrire le groupe balise statut.
	 * @param w un XML writer
	 * @param code un code
	 * @param libelle un libelle
	 * @throws IOException 
	 */
	private void ecrireBaliseStatut(CktlXMLWriter w, String code, String libelle) throws IOException {
		w.startElement("statut");
		w.writeElement("code", code);
		w.writeElement("libelle", StringCtrl.checkString(libelle));
		w.endElement();
	}

	private IPecheParametres getPecheParametres() {
		return pecheParametres;
	}
	
}