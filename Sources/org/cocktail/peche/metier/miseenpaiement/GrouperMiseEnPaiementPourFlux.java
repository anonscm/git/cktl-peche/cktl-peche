package org.cocktail.peche.metier.miseenpaiement;

import org.cocktail.peche.entity.EOMiseEnPaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Groupage des mises en paiement pour la génération du flux de paiement.
 */
public class GrouperMiseEnPaiementPourFlux implements IGrouperMiseEnPaiement<EOMiseEnPaiement> {

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOSortOrdering> getTri() {
		return EOMiseEnPaiement.TO_SERVICE.asc().then(EOMiseEnPaiement.TAUX_BRUT.asc());
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isRupture(EOMiseEnPaiement miseEnPaiementPrecedente, EOMiseEnPaiement miseEnPaiementEnCours) {
		return miseEnPaiementPrecedente.toService() != miseEnPaiementEnCours.toService()
				|| miseEnPaiementPrecedente.tauxBrut().compareTo(miseEnPaiementEnCours.tauxBrut()) != 0;
	}

}
