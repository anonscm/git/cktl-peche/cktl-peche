package org.cocktail.peche.metier.miseenpaiement;

import java.util.Calendar;

import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOParametresFluxPaiement;

/**
 * Cette classe représente une implémentation partielle par défaut.
 * Cette implémentation ne génère pas d'en-tête ({@link #getLigneEntete()} retourne <code>null</code>).
 * 
 * @author Pascal MACOUIN
 */
public abstract class FormatFluxPaiementSansEntete implements IFormatFluxPaiement {

	/** Paramètres nécessaires à la mise en paiement. */
	private EOParametresFluxPaiement parametresFluxPaiement;

	
	private ServiceDonneesSpecifiquesFluxPaiement serviceDonneesSpecifiquesFluxPaiement = new ServiceDonneesSpecifiquesFluxPaiement(); 
	
	
	public ServiceDonneesSpecifiquesFluxPaiement getServiceDonneesSpecifiquesFluxPaiement() {
		return serviceDonneesSpecifiquesFluxPaiement;
	}

	public void setServiceDonneesSpecifiquesFluxPaiement(
			ServiceDonneesSpecifiquesFluxPaiement serviceDonneesSpecifiquesFluxPaiement) {
		this.serviceDonneesSpecifiquesFluxPaiement = serviceDonneesSpecifiquesFluxPaiement;
	}

	/**
	 * Retourne les paramètres nécessaires à la mise en paiement.
	 * 
	 * @return les paramètres nécessaires à la mise en paiement
	 */
	public EOParametresFluxPaiement getParametresFluxPaiement() {
		return parametresFluxPaiement;
	}

	/**
	 * Affecte les paramètres nécessaires à la mise en paiement.
	 * 
	 * @param parametresFluxPaiement les paramètres nécessaires à la mise en paiement
	 */
	public void setParametresFluxPaiement(EOParametresFluxPaiement parametresFluxPaiement) {
		this.parametresFluxPaiement = parametresFluxPaiement;
	}

	/**
	 * Pas d'en-tête pour ces flux.
	 * 
	 * @return <code>null</code>
	 */
	public String getLigneEntete() {
		return null;
	}

	/**
	 * {@inheritDoc}
	 * @throws Exception 
	 */
	public abstract String getLigneDetail(EOFluxMiseEnPaiement fluxMiseEnPaiement, boolean estCotisationASolidarite) throws Exception;

	/**
	 * {@inheritDoc}
	 */
	public String getNomFlux(EOPaiement paiement) {
		Calendar date = Calendar.getInstance();
		date.setTime(paiement.datePaiement());

		return String.format(getParametresFluxPaiement().templateNomFlux(), paiement.numeroPaiement(), 
				date.get(Calendar.YEAR), date.get(Calendar.MONTH) + 1, date.get(Calendar.DAY_OF_MONTH)) + getExtensionNomFlux();
	}

	/**
	 * Retourne la date de paiement (AAAAMM). Doit retourner 6 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return la date de paiement (AAAAMM)
	 */
	public String getDatePaye(EOFluxMiseEnPaiement fluxMiseEnPaiement) {

		String annee,mois;
		Calendar date = Calendar.getInstance();

		if (Constante.OUI.equals(fluxMiseEnPaiement.moisSuivantpourTG())) {
			date.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
			date.set(Calendar.DAY_OF_MONTH, 1); //on va au 1er jour du mois
			date.add(Calendar.MONTH,1); // on ajoute 1 mois
		}
		else {
			date.setTime(fluxMiseEnPaiement.toPaiement().datePaiement());
		}

		annee = String.format("%04d", date.get(Calendar.YEAR));
		mois = String.format("%02d", date.get(Calendar.MONTH) + 1);

		return annee + mois;
	}

	/**
	 * Retourne le numéro INSEE. Doit retourner 13 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return le numéro INSEE
	 */
	public String getNumeroInsee(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		String numeroInsee = fluxMiseEnPaiement.numeroInsee();

		if (numeroInsee == null) {
			numeroInsee = "0000000000000";
		}

		return String.format("%13s", numeroInsee);
	}

	/**
	 * Retourne la clé INSEE. Doit retourner 2 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return la clé INSEE
	 */
	public String getCleInsee(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		Integer cleInsee = fluxMiseEnPaiement.cleInsee();
		String cleInseeChaine; 

		if (cleInsee == null) {
			cleInseeChaine = "00";
		} else {
			cleInseeChaine = String.format("%02d", cleInsee);
		}

		return cleInseeChaine;
	}

	/**
	 * Retourne le Numéro de Prise en Charge (NPC). Doit retourner 2 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return le Numéro de Prise en Charge (NPC)
	 */
	public String getNumeroPriseEnCharge(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		String npc = fluxMiseEnPaiement.numeroPriseEnCharge();

		if (npc == null) {
			npc = "00";
		}

		return npc;
	}

	/**
	 * Retourne le montant brut à payer (00000V00). Doit retourner 7 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return le montant brut à payer (00000V00)
	 */
	public String getMontantBrutPaye(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		return String.format("%07d", fluxMiseEnPaiement.brutPaye().movePointRight(2).intValue());
	}
}
