package org.cocktail.peche.metier.miseenpaiement;

/**
 * Liste des codes champ dynamiques présents dans la table DEFINITION_FLUX_PAIEMENT 
 * @author juliencallewaert
 *
 */
public enum FluxPaiementChampSpecifique {
	
	GIRAFE_CODE_CHAINE_PAYE ("GirafeCodeChainePaye"),
	GIRAFE_CORR("GirafeCorr"),
	GIRAFE_MIN("GirafeMin"),
	GIRAFE_ORIG("GirafeOrig"),
	GIRAFE_SENS("GirafeSens"),
	GIRAFE_MTT("GirafeCodeMttPreCalcule"),
	GIRAFE_TPL_LIB_COMP("GirafeTplLibComplementaire"),
	GIRAFE_IR_SOLIDARITE("GirafeCodeIrCotisation"),
	GIRAFE_IR_SANS_SOLIDARITE("GirafeCodeIrNonCotisation"),
	GIRAFE_NUMERO_REMISE("GirafeNumeroRemise"),
	WINPAIE_TPL_NOM_PRENOM("WinPaieTplNomPrenom"),
	WINPAIE_OBJET("WinPaieObjet"),
	WINPAIE_IR_SOLIDARITE("WinPaieCodeIrSolidarite"),
	WINPAIE_IR_SANS_SOLIDARITE("WinPaieCodeIrNonSolidarite"),
	PAPAYE_PSTA_ORDRE_STAT("PapayePstaOrdreStat"),
	PAPAYE_PSTA_ORDRE_CONT("PapayePstaOrdreCont"),
	PAPAYE_PSTA_ORDRE_VACAT("PapayePstaOrdreVacat"),
	PAPAYE_PRUB_ORDRE_CONTRIBSOLI_STAT("PapayePrubCotSStat"),
	PAPAYE_PRUB_ORDRE_CONTRIBSOLI_CONT("PapayePrubCotSCont"),
	PAPAYE_PRUB_ORDRE_CONTRIBSOLI_VACAT("PapayePrubCotSVacat"),
	PAPAYE_PRUB_ORDRE_HCOMPL_STAT("PapayePrubHComplStat"),
	PAPAYE_PRUB_ORDRE_HCOMPL_CONT("PapayePrubHComplCont"),
	PAPAYE_PRUB_ORDRE_HCOMPL_VACAT("PapayePrubHComplVacat"),
	PAPAYE_CODE_NBHEURES_STAT("PapayeCodeNbHeuresStat"),
	PAPAYE_CODE_NBHEURES_CONT("PapayeCodeNbHeuresCont"),
	PAPAYE_CODE_NBHEURES_VACAT("PapayeCodeNbHeuresVacat"),
	PAPAYE_CODE_TXHOR_STAT("PapayeCodeTxHorStat"),
	PAPAYE_CODE_TXHOR_CONT("PapayeCodeTxHorCont"),
	PAPAYE_CODE_TXHOR_VACAT("PapayeCodeTxHorVacat"),
	PAPAYE_PFCT_ORDRE_STAT("PapayePfctOrdreStat"),
	PAPAYE_PFCT_ORDRE_CONT("PapayePfctOrdreCont"),
	PAPAYE_PFCT_ORDRE_VACAT("PapayePfctOrdreVacat"),
	PAPAYE_PCT_CONTRAT_TRAV_STAT("PapayePctContratTravStat"),
	PAPAYE_PCT_CONTRAT_TRAV_CONT("PapayePctContratTravCont"),
	PAPAYE_PCT_CONTRAT_TRAV_VACAT("PapayePctContratTravVacat"),	
	PAPAYE_PSEC_ORDRE("PapayePsecOrdre");	
	
	private String codeChamp;
	
	/**
	 * Constructeur.
	 */
	FluxPaiementChampSpecifique(String codeChamp) {
		this.codeChamp = codeChamp;
	}

	public String getCodeChamp() {
		return codeChamp;
	}
	
	
	
}
