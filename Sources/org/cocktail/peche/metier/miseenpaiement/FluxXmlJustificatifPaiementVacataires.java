package org.cocktail.peche.metier.miseenpaiement;

import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgrh.common.metier.interfaces.IVacataires;
import org.cocktail.fwkcktlpeche.circuitvalidation.CircuitPeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOHistoriqueDemande;
import org.cocktail.peche.Application;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.components.commun.ComposantAffichageHelper;
import org.cocktail.peche.components.controlers.EnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EOPeriode;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.LogoEdition;
import org.cocktail.peche.outils.OutilsValidation;
import org.cocktail.peche.services.IPeriodeService;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe permettant de générer un flux XML nécessaire à l'édition d'un justificatif de paiement.
 */
public class FluxXmlJustificatifPaiementVacataires {

	/** logger. */
	private static Logger log = Logger.getLogger(FluxXmlJustificatifPaiementVacataires.class);

	private NumberFormat formatCleInsee;
	private NumberFormat formatMois;
	private EnseignantsVacatairesCtrl enseignantsVacatairesCtrl;
	private boolean hasStatutaires;
	private boolean hasVacataires;
	private Map<String, EOHistoriqueDemande> mapVisas = new HashMap<String, EOHistoriqueDemande>();

	private EOEditingContext editingContext;

	@Inject
	private IPeriodeService periodeService;

	@Inject
	private IPecheParametres pecheParametres;
	
	private Integer anneeConcernee;

	/**
	 * Constructeur.
	 * @param edc l'editingContext
	 */
	public FluxXmlJustificatifPaiementVacataires(EOEditingContext edc) {
		formatCleInsee = NumberFormat.getIntegerInstance();
		formatCleInsee.setMinimumIntegerDigits(2);
		formatMois = NumberFormat.getIntegerInstance();
		formatMois.setMinimumIntegerDigits(2);

		periodeService = Application.application().injector().getInstance(IPeriodeService.class);
		pecheParametres = Application.application().injector().getInstance(IPecheParametres.class);
		editingContext = edc;
		
		anneeConcernee = getPecheParametres().getAnneeUniversitaire();
	}

	/**
	 * Retourne le flux XML nécessaire à l'édition d'un justificatif de paiement.
	 * 
	 * @param paiement un paiement
	 * @return le flux XML
	 */
	public String getFlux(EOPaiement paiement) {
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);

		try {
			
			List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(paiement, new GrouperMiseEnPaiementPourBordereau());
			// pour initialiser l'année concernée , nous prenons le 1er élément mis en paiement 
			if ((listeMiseEnPaiement.size() > 0) && (listeMiseEnPaiement.get(0) != null) && (listeMiseEnPaiement.get(0).getService()) != null) {			
				anneeConcernee = listeMiseEnPaiement.get(0).getService().annee();
			}
			
			// Xml des structures
			String fluxNiveauxStructures = getFluxNiveauxEnseignantsVacataires(paiement);

			w.setEscapeSpecChars(true);

			w.startDocument();
			w.startElement("justificatif_paiement");

			w.startElement("paiement");
			w.writeElement("numero_paiement", paiement.numeroPaiement().toString());

			if (hasStatutaires == hasVacataires) {
				ecrireBaliseStatut(w, "", "");
			} else if (hasStatutaires) {
				ecrireBaliseStatut(w, "S", "Statutaire");
			} else if (hasVacataires) {
				ecrireBaliseStatut(w, "V", "Vacataire");
			}

			if (getPecheParametres().isFormatAnneeExerciceAnneeCivile()) {
				w.writeElement("annee_universitaire", String.valueOf(anneeConcernee));
			} else {
				w.writeElement("annee_universitaire", anneeConcernee + "/" + (anneeConcernee + 1));
			}	

			w.startElement("periode_paiement");

			EOPeriode periode = getPeriodeService().rechercherPeriode(paiement.editingContext(), EOPecheParametre.getChoixTypePeriode(paiement.editingContext()) + getPecheParametres().getCodeFormatAnnee(), paiement.moisPaiement());
			if (periode != null) {
				w.writeElement("numero_dernier_mois_periode", OutilsValidation.formatter(periode.noDernierMois(), formatMois));
				w.writeElement("libelle", StringCtrl.checkString(periode.libelle()));
			} else {
				w.writeElement("numero_dernier_mois_periode", "");
				w.writeElement("libelle", "");
			}
			w.endElement();

			w.writeElement("libelle", StringCtrl.checkString(paiement.libelle()));
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY) != null) {
				w.writeElement("etablissement", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), EOGrhumParametres.PARAM_GRHUM_ETAB_KEY)));
			}
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT") != null) {
				w.writeElement("president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "GRHUM_PRESIDENT")));
			}
			if (EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT") != null) {
				w.writeElement("signature_president", StringCtrl.checkString(EOGrhumParametres.parametrePourCle(paiement.editingContext(), "SIGNATURE_PRESIDENT")));
			}

			String cLogo = LogoEdition.cheminLogoEtablissement(paiement.editingContext());
			if (cLogo != null) {
				w.writeElement("logo_etablissement", cLogo);
			}

			w.writeElement("date_paiement", DateCtrl.dateToString(paiement.datePaiement()));
			w.writeElement("date_bordereau", DateCtrl.dateToString(paiement.dateBordereau(), "dd/MM/yyyy"));
			w.writeElement("date_arrete", DateCtrl.dateToString(paiement.dateArrete()));
			w.writeElement("date_flux", DateCtrl.dateToString(paiement.dateFlux()));
			w.endElement();

			w.writeString(fluxNiveauxStructures);

			w.endElement();
			w.endDocument();
			w.close();
		} catch (IOException e) {
			// Impossible car on écrit en mémoire
			log.error("Impossible de générer le flux XML", e);
		}

		log.debug("*** Flux XML:" + sw.toString() + "*** Fin Flux XML");
		return sw.toString();
	}


	/**
	 * Génération du XML au niveaux des enseignants vacataires.
	 * 
	 * @param paiement un paiement
	 * @return Le XML structure
	 * @throws IOException
	 */
	private String getFluxNiveauxEnseignantsVacataires(EOPaiement paiement) throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		StringWriter sw = new StringWriter();
		CktlXMLWriter w = new CktlXMLWriter(sw);
		w.setEscapeSpecChars(true);
		
		DecimalFormat df = new DecimalFormat("0.00#");
		
		hasStatutaires = false;
		hasVacataires = false;

		List<MiseEnPaiementDataBean> listeMiseEnPaiement = MiseEnPaiement.getInstance().listerMiseEnPaiementGrouperPar(paiement, new GrouperMiseEnPaiementPourBordereau());
		List<EOActuelEnseignant> listeEnseignants = new ArrayList<EOActuelEnseignant>();

		enseignantsVacatairesCtrl = new EnseignantsVacatairesCtrl(editingContext, anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile());


		for (int i = 0; i < listeMiseEnPaiement.size(); i++) {
			if (listeMiseEnPaiement.get(i) != null) {
				EOService eoservice = listeMiseEnPaiement.get(i).getService();
				if (eoservice != null)	{
					if (!listeEnseignants.contains(eoservice.enseignant())) {
						listeEnseignants.add(eoservice.enseignant());			
					}
				}
			}
		}


		int indexEnseignant;

		for (indexEnseignant = 0; indexEnseignant < listeEnseignants.size(); indexEnseignant++)
		{
			EOActuelEnseignant lenseignant = listeEnseignants.get(indexEnseignant);
			EOIndividu individu = lenseignant.toIndividu();

			w.startElement("enseignant");
			w.writeElement("nom", StringCtrl.checkString(individu.nomAffichage().toUpperCase()));
			w.writeElement("prenom", StringCtrl.checkString(individu.prenomAffichage()));
			w.writeElement("numero_insee", StringCtrl.checkString(individu.indNoInsee()));
			w.writeElement("cle_insee", OutilsValidation.formatter(individu.indCleInsee(), formatCleInsee));
			w.writeElement("numero_prise_en_charge", OutilsValidation.retournerNonNull(individu.toPersonnel().npc()));
			w.writeElement("date_naissance", formatter.format(individu.getDtNaissance()));

			String codeTypeContratTravail = "";
			String libelleCourtTypeContratTravail = "";
			String libelleLongTypeContratTravail = "";

			if (lenseignant.isStatutaire(anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
				// Sans objet pour les statutaires
			} else {
				// Libellé  vacation
				libelleLongTypeContratTravail = enseignantsVacatairesCtrl.getLibelleTypeVacationPourXML(lenseignant);

				w.startElement("type_contrat");
				w.writeElement("code", StringCtrl.checkString(codeTypeContratTravail));
				w.writeElement("libelle_court", StringCtrl.checkString(libelleCourtTypeContratTravail));
				w.writeElement("libelle_long", StringCtrl.checkString(libelleLongTypeContratTravail));
				w.endElement();
				w.writeElement("type_vacation", enseignantsVacatairesCtrl.getLibelleTypeAssocieVacation(lenseignant));		
				
				List<IVacataires> listeVacations = enseignantsVacatairesCtrl.getListeContratsSurAnneeUniversitaire(lenseignant);

				StringBuffer datesContrats = new StringBuffer();
				String dateDebut, dateFin, date;

				for (IVacataires vacation : listeVacations) {
					dateDebut = formatter.format(vacation.getDateDebut().getTime());
					dateFin = formatter.format(vacation.getDateFin().getTime());
					date = dateDebut + " au " + dateFin + " ";
					datesContrats.append(date);
				}
				w.writeElement("dates_contrats", datesContrats.toString());
			}

			double heures_contrat = enseignantsVacatairesCtrl.getTotalNbHeuresVacation(lenseignant);
			double heures_attribuees = enseignantsVacatairesCtrl.getServiceAttribue(lenseignant);
			double heures_realisees = enseignantsVacatairesCtrl.getServiceRealise(lenseignant);
			BigDecimal heures_deja_payees = enseignantsVacatairesCtrl.getServiceTotalHeuresAPayerHETD(lenseignant, false);
			BigDecimal heures_a_payer = enseignantsVacatairesCtrl.getServiceTotalHeuresAPayerHETD(lenseignant, true);

			w.writeElement("heures_contrat", String.valueOf(heures_contrat));
			w.writeElement("heures_attribuees",	String.valueOf(heures_attribuees));
			w.writeElement("delta_previsionnel", String.valueOf(heures_contrat - heures_attribuees));
			w.writeElement("heures_realisees", String.valueOf(heures_realisees));
			w.writeElement("delta_realise", String.valueOf(heures_contrat - heures_realisees));
			w.writeElement("heures_deja_payees", String.valueOf(heures_deja_payees));
			w.writeElement("heures_a_payer", String.valueOf(heures_a_payer));


			EODemande demande = enseignantsVacatairesCtrl.getDemande(lenseignant.getService(anneeConcernee));
			NSArray<EOCircuitValidation > listeCircuits = demande.rechercherCircuitsEmpruntes(true);
			if (listeCircuits.size() > 0) {
				for (EOCircuitValidation circuitValidation : listeCircuits) {
					w.startElement("circuit");
					w.writeElement("libelle", StringCtrl.checkString(getLibelleFicheTypeCircuit(circuitValidation)));
					NSMutableArray<EOEtape> etapesUtiliseesTemp = circuitValidation.etapesUtiliseesSuivantOrdreChemin(TypeCheminPeche.VALIDER.getCodeTypeChemin(), TypeCheminPeche.REFUSER.getCodeTypeChemin()).mutableClone();
					for (int iEtape = 0; iEtape < etapesUtiliseesTemp.size(); iEtape++) {
						EOEtape etape = etapesUtiliseesTemp.get(iEtape);
						if (etape.isFinale()) {
							etapesUtiliseesTemp.remove(etape);
						}
					}
					initVisa(demande, circuitValidation);
					for (EOEtape etape : etapesUtiliseesTemp) {
						w.startElement("etape");
						w.writeElement("libelle_visa", StringCtrl.checkString(EtapePeche.getEtape(etape).getLibelleVisa()));
						if (mapVisas.get(etape.codeEtape()) != null) {
							w.writeElement("date_visa", formatter.format(mapVisas.get(etape.codeEtape()).dateModification()));
						} else {
							w.writeElement("date_visa", "");
						}
						w.endElement();
					}
					w.endElement();
				}
			}
			if (lenseignant.isStatutaire(anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile())) {
				ecrireBaliseStatut(w, "S", "Statutaire");
				hasStatutaires = true;
			} else {
				ecrireBaliseStatut(w, "V", "Vacataire");
				hasVacataires = true;
			}

			EOCorps corps = lenseignant.getCorps(anneeConcernee, getPecheParametres().isFormatAnneeExerciceAnneeCivile());
			w.startElement("corps");
			if (corps == null) {
				w.writeElement("code", "");
				w.writeElement("libelle_court", "");
				w.writeElement("libelle_long", "");
			} else {
				w.writeElement("code", StringCtrl.checkString(corps.cCorps()));
				w.writeElement("libelle_court", StringCtrl.checkString(corps.lcCorps()));
				w.writeElement("libelle_long", StringCtrl.checkString(corps.llCorps()));
			}
			w.endElement();

			Iterator<MiseEnPaiementDataBean> iterateur = listeMiseEnPaiement.iterator();

			// Première lecture
			MiseEnPaiementDataBean miseEnPaiementDataBean = null;
			if (iterateur.hasNext()) {
				miseEnPaiementDataBean = iterateur.next();
			}

			BigDecimal totalHeuresAPayerHetdStructure = BigDecimal.ZERO;
			BigDecimal totalHeuresPayeesHetdStructure = BigDecimal.ZERO;

			while (miseEnPaiementDataBean != null) {
				EOActuelEnseignant enseignant = miseEnPaiementDataBean.getService().enseignant();
				if (enseignant.equals(listeEnseignants.get(indexEnseignant))) {					

					totalHeuresAPayerHetdStructure = BigDecimal.ZERO;
					totalHeuresPayeesHetdStructure = BigDecimal.ZERO;

					EOStructure structure = miseEnPaiementDataBean.getStructure();
					boolean tableauRepartitionExiste = false;
					while (miseEnPaiementDataBean != null && structure == miseEnPaiementDataBean.getStructure()) {
						EOService service = miseEnPaiementDataBean.getService();
						List<EOServiceDetail> listeServiceDetailDansPaiement = new ArrayList<EOServiceDetail>();

						//On ne récupère que les service Détail du paiement
						for (EOServiceDetail unServiceDetail : service.listeServiceDetails()) {
							for (EOMiseEnPaiement uneMep : paiement.toListeMisesEnPaiement()) {
								if (unServiceDetail == uneMep.toServiceDetail()) {
									listeServiceDetailDansPaiement.add(unServiceDetail);
								}
							}
						}
						// tri selon periode puis typeAP (sauf REH à la fin)
						Collections.sort(listeServiceDetailDansPaiement, new Comparator<EOServiceDetail>() {
							public int compare(EOServiceDetail a, EOServiceDetail b) {
								if ((a.toPeriode() == null) || (b.toPeriode() == null)) {
									return 0;
								}
								if (a.toPeriode().ordrePeriode() == b.toPeriode().ordrePeriode()) {
									if (a.reh() != null) {
										return 1;
									} 									
									if (b.reh() != null) {
										return -1;
									}									
									if ((a.composantAP() != null && a.composantAP().typeAP() != null)
											&& (b.composantAP() != null && b.composantAP().typeAP() != null)) 	{
										return a.composantAP().typeAP().code().compareTo(b.composantAP().typeAP().code());
									}
									return 0;
								}
								if (a.toPeriode().ordrePeriode() > b.toPeriode().ordrePeriode()) {
									return 1;
								} else {
									return -1;
								}
							}
						});

						// premiere boucle sur la liste pour les répartitions service
						for (int i = 0; i < listeServiceDetailDansPaiement.size(); i++) {
							EOServiceDetail serviceDetail = listeServiceDetailDansPaiement.get(i);
							if (serviceDetail.service().enseignant().equals(listeEnseignants.get(indexEnseignant)))	{

								if (!tableauRepartitionExiste) {
									w.startElement("structure"); // début structure
									w.writeElement("code", StringCtrl.checkString(structure.cStructure()));
									w.writeElement("libelle_court", StringCtrl.checkString(structure.lcStructure()));
									w.writeElement("libelle_long", StringCtrl.checkString(structure.llStructure()));
									tableauRepartitionExiste = true;
								}

								if (serviceDetail.composantAP() != null
										&& serviceDetail.composantAP().parents() != null) {

									IComposant composant = serviceDetail.composantAP().parents().get(0);

									if (composant.structures() != null 
											&& !composant.structures().isEmpty()) {
										EOStructure lieuParentAP = (EOStructure)(composant.structures().get(0));
										// Cas d'une répartition de service	
										if (lieuParentAP.equals(structure) &&  ( (serviceDetail.heuresAPayer() != null) || (serviceDetail.heuresPayees() != null))) {

											w.startElement("enseignement");
											w.writeElement("libelle", ComposantAffichageHelper.getInstance().affichageEnseignement(composant));							 
											if (serviceDetail.composantAP() != null && serviceDetail.composantAP().typeAP() != null) {
												w.writeElement("type", StringCtrl.checkString(serviceDetail.composantAP().typeAP().code()));
											}

											if (serviceDetail.heuresAPayer() != null) {						
												w.writeElement("heuresAPayer", df.format(serviceDetail.heuresAPayer()));
											}

											if (serviceDetail.heuresPayees() != null) {
												w.writeElement("heuresPayees", df.format(serviceDetail.heuresPayees()));
											}

											if (serviceDetail.heuresPrevues() != null) {
												w.writeElement("heuresPrevues", df.format(serviceDetail.heuresPrevues()));
											}
											if (serviceDetail.heuresRealisees() != null) {
												w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
											}

											if (serviceDetail.heuresAPayer() != null) {												
												w.writeElement("heuresAPayerHETD",df.format(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(service, serviceDetail.composantAP(),serviceDetail.heuresAPayer())));
												totalHeuresAPayerHetdStructure = totalHeuresAPayerHetdStructure.add(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(service,serviceDetail.composantAP(),serviceDetail.heuresAPayer()));
											}											

											if (serviceDetail.heuresPayees() != null) {												
												w.writeElement("heuresPayeesHETD",df.format(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(service, serviceDetail.composantAP(),serviceDetail.heuresPayees())));
												totalHeuresPayeesHetdStructure = totalHeuresPayeesHetdStructure.add(enseignantsVacatairesCtrl.getServiceHeuresHETDParAP(service,serviceDetail.composantAP(),serviceDetail.heuresPayees()));
											}											

											if (serviceDetail.toPeriode() != null) {
												w.writeElement("periode", StringCtrl.checkString(serviceDetail.toPeriode().libelle()));	
											}
											w.endElement();
										}
									}
								} else {

									// Cas d'une répartition REH
									if (serviceDetail.reh() != null && serviceDetail.toStructure() != null
											&& structure.equals(serviceDetail.toStructure())) {

										w.startElement("enseignement");
										w.writeElement("libelle", serviceDetail.reh().libelleCourt());
										w.writeElement("type", "REH");

										if (serviceDetail.heuresAPayer() != null) {						
											w.writeElement("heuresAPayer", df.format(serviceDetail.heuresAPayer()));
										}

										if (serviceDetail.heuresPayees() != null) {
											w.writeElement("heuresPayees", df.format(serviceDetail.heuresPayees()));
										}

										if (serviceDetail.heuresPrevues() != null) {
											w.writeElement("heuresPrevues", df.format(serviceDetail.heuresPrevues()));
										}
										if (serviceDetail.heuresRealisees() != null) {
											w.writeElement("heuresRealisees", df.format(serviceDetail.heuresRealisees()));
										}

										if (serviceDetail.heuresAPayer() != null) {												
											w.writeElement("heuresAPayerHETD", df.format(serviceDetail.heuresAPayer()));
											totalHeuresAPayerHetdStructure = totalHeuresAPayerHetdStructure.add(serviceDetail.heuresAPayer());
										}											

										if (serviceDetail.heuresPayees() != null) {												
											w.writeElement("heuresPayeesHETD", df.format(serviceDetail.heuresPayees()));
											totalHeuresPayeesHetdStructure = totalHeuresPayeesHetdStructure.add(serviceDetail.heuresPayees());
										}											

										if (serviceDetail.toPeriode() != null) {
											w.writeElement("periode", StringCtrl.checkString(serviceDetail.toPeriode().libelle()));	
										}
										w.endElement();

									}

								}
							}
						}

						if (totalHeuresAPayerHetdStructure.intValue() != 0) {						
							w.writeElement("total_heures_a_payer_hetd", df.format(totalHeuresAPayerHetdStructure));
						}
						if (totalHeuresPayeesHetdStructure.intValue() != 0) {
							w.writeElement("total_heures_payees_hetd", df.format(totalHeuresPayeesHetdStructure));
						}
						if (iterateur.hasNext()) {
							miseEnPaiementDataBean = iterateur.next();				
						} else {
							miseEnPaiementDataBean = null;
						}

					}

					if (tableauRepartitionExiste) {
						w.endElement(); // Fin structure
					}
				} else {
					if (iterateur.hasNext()) {
						miseEnPaiementDataBean = iterateur.next();				
					} else {
						miseEnPaiementDataBean = null;
					}
				}
			} 
			w.endElement(); // fin enseignant
		}

		w.close();
		return sw.toString();
	}

	/**
	 * Retourne le libellé du type de la fiche déterminé en fonction du circuit emprunté.
	 * 
	 * @param circuit le circuit
	 * @return Un libellé du type de la fiche
	 */
	public String getLibelleFicheTypeCircuit(EOCircuitValidation circuit) {
		if (CircuitPeche.PECHE_STATUTAIRE_PREVISIONNEL.getCodeCircuit().equals(circuit.codeCircuitValidation())
				|| CircuitPeche.PECHE_VACATAIRE_PREVISIONNEL.getCodeCircuit().equals(circuit.codeCircuitValidation())) {
			return "Fiche prévisionnelle";
		} else {
			return "Fiche définitive";
		}
	}

	/**
	 * On récupère les visa des chemins "VALIDER".
	 * @param demande la demande
	 * @param circuit le circuit
	 */
	public void initVisa(EODemande demande, EOCircuitValidation circuit) {

		mapVisas.clear();

		if (demande != null) {
			NSArray<EOHistoriqueDemande> listeHistoriqueDemande = 
					demande.historiqueChronologique(EOHistoriqueDemande.TO_ETAPE_ARRIVEE.dot(EOEtape.TO_CIRCUIT_VALIDATION).dot(EOCircuitValidation.CODE_CIRCUIT_VALIDATION).eq(circuit.codeCircuitValidation()));

			for (EOHistoriqueDemande historiqueDemande : listeHistoriqueDemande) {
				EOEtape etapeDepart = historiqueDemande.toEtapeDepart();

				if (etapeDepart != null) {
					EOChemin cheminValider = etapeDepart.chemin(TypeCheminPeche.VALIDER.getCodeTypeChemin());
					if (cheminValider != null
							&& cheminValider.toEtapeArrivee() == historiqueDemande.toEtapeArrivee()) {
						mapVisas.put(etapeDepart.codeEtape(), historiqueDemande);
					}
				}
			}
		}
	}

	/**
	 * Ecrire le groupe balise statut.
	 * @param w un XML writer
	 * @param code un code
	 * @param libelle un libelle
	 * @throws IOException 
	 */
	private void ecrireBaliseStatut(CktlXMLWriter w, String code, String libelle) throws IOException {
		w.startElement("statut");
		w.writeElement("code", code);
		w.writeElement("libelle", StringCtrl.checkString(libelle));
		w.endElement();
	}

	public IPeriodeService getPeriodeService() {
		return periodeService;
	}

	private IPecheParametres getPecheParametres() {
		return pecheParametres;
	}

}
