package org.cocktail.peche.metier.miseenpaiement;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.peche.entity.EOFluxMiseEnPaiement;
import org.cocktail.peche.entity.EOPaiement;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

/**
 * Cette classe formate le flux de mise en paiement au format Winpaie.
 * 
 * @author Pascal MACOUIN
 * 			Laurent PRINGOT
 */
public class FormatFluxPaiementWinpaie extends FormatFluxPaiementSansEntete {

	/**
	 * {@inheritDoc}
	 */
	public NSArray<EOSortOrdering> getTri() {
		return EOFluxMiseEnPaiement.NOM.asc().then(EOFluxMiseEnPaiement.PRENOM.asc());
	}	

	/**
	 * {@inheritDoc}
	 */
	public String getExtensionNomFlux() {
		return ".xls";
	}

	/**
	 * Retourne la concaténation du nom/prénom. Doit retourner 30 caractères.
	 * 
	 * @param fluxMiseEnPaiement le flux de mise en paiement
	 * @return la concaténation du nom/prénom
	 */
	private String getNomPrenom(EOFluxMiseEnPaiement fluxMiseEnPaiement) {
		return String.format("%-30.30s", String.format(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), 
				FluxPaiementChampSpecifique.WINPAIE_TPL_NOM_PRENOM.getCodeChamp()), fluxMiseEnPaiement.nom(), fluxMiseEnPaiement.prenom()));
	}


	/**
	 * 
	 * @param estCotisationASolidarite
	 * @return
	 */
	private String getCodeCotisationSolidarite(boolean estCotisationASolidarite) {
		String codeCotisation = "";
		if (estCotisationASolidarite) {
			codeCotisation = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.WINPAIE_IR_SOLIDARITE.getCodeChamp());
		} else {
			codeCotisation = getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.WINPAIE_IR_SANS_SOLIDARITE.getCodeChamp());
		}
		return codeCotisation;
	}

	public boolean isFormatExcel() {
		return true;
	}


	public HSSFWorkbook getFluxFormatExcel(EOPaiement paiement, boolean estCotisationASolidarite)
	{

		int colonne;
		HSSFWorkbook wb = new HSSFWorkbook();
		try {

			HSSFSheet sheet = wb.createSheet();
			sheet.setDefaultColumnWidth(20);

			HSSFRow ligneEntete = sheet.createRow((short) 0);
			// Creation des colonnes
			colonne = 0;
			ligneEntete.createCell(colonne++).setCellValue("Insee");
			ligneEntete.createCell(colonne++).setCellValue("Nom");
			ligneEntete.createCell(colonne++).setCellValue("Carte");
			ligneEntete.createCell(colonne++).setCellValue("Code Origine");
			ligneEntete.createCell(colonne++).setCellValue("Retenue");
			ligneEntete.createCell(colonne++).setCellValue("Sens");
			ligneEntete.createCell(colonne++).setCellValue("MC");
			ligneEntete.createCell(colonne++).setCellValue("NBU");
			ligneEntete.createCell(colonne++).setCellValue("Montant");
			ligneEntete.createCell(colonne++).setCellValue("Libelle");

			short noLigne = 1;

			NSArray<EOFluxMiseEnPaiement> listFluxMiseEnPaiement = paiement.toListeFluxMiseEnPaiement(null, getTri(), false);

			for (EOFluxMiseEnPaiement fluxMiseEnPaiement : listFluxMiseEnPaiement) {

				HSSFRow ligne = sheet.createRow(noLigne);
				colonne = 0;
				ligne.createCell(colonne++).setCellValue(getNumeroInsee(fluxMiseEnPaiement)
						+ getCleInsee(fluxMiseEnPaiement)
						+ getNumeroPriseEnCharge(fluxMiseEnPaiement).substring(0, 1));
				ligne.createCell(colonne++).setCellValue(getNomPrenom(fluxMiseEnPaiement));
				ligne.createCell(colonne++).setCellValue("20");
				ligne.createCell(colonne++).setCellValue("0");
				ligne.createCell(colonne++).setCellValue(getCodeCotisationSolidarite(estCotisationASolidarite));
				ligne.createCell(colonne++).setCellValue("0");
				ligne.createCell(colonne++).setCellValue("B");
				ligne.createCell(colonne++).setCellValue(String.format("%.2f", fluxMiseEnPaiement.heuresPayees()));
				ligne.createCell(colonne++).setCellValue(String.format("%.2f", fluxMiseEnPaiement.tauxNonCharge()));
				ligne.createCell(colonne++).setCellValue(getServiceDonneesSpecifiquesFluxPaiement().recupererValeurSelonCodeChamp(getParametresFluxPaiement(), FluxPaiementChampSpecifique.WINPAIE_OBJET.getCodeChamp())
						+ fluxMiseEnPaiement.heuresPayees());
				fluxMiseEnPaiement.heuresPayees();
				noLigne++;			
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return wb;
	}

	@Override
	public String getLigneDetail(EOFluxMiseEnPaiement fluxMiseEnPaiement,
			boolean estCotisationASolidarite) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
