package org.cocktail.peche.metier.hce;

import java.util.HashMap;
import java.util.Map;

/**
 * Cette classe contient les données nécessaires aux calculs du service HETD et des heures complémentaires.
 * 
 * @author Equipe PECHE.
 */
public class DonneesDuCalcul {
	/** Total général des heures "présentielles" (sans application d'aucun coefficient). */
	private double totalPresentiel;
	/** Total général des heures en equilavent TD (les coefficients utilisés sont ceux "dans le service"). */
	private double totalHetd;
	/** Total général des heures (en equilavent TD) pour les REH uniquement. */
	private double totalPourReh;

	/** Total général des heures "présentielles" pour un type d'AP (sans appliquer de coefficient). */
	private Map<String, Double> mapTotalPresentiel;
	/** Total général des heures en equilavent TD pour un type d'AP (les coefficients utilisés sont ceux "dans le service"). */
	private Map<String, Double> mapTotalHetd;

	/**
	 * Initialisation (remise à zéro des données).
	 */
	public void init() {
		this.totalPresentiel = 0.0d;
		this.totalHetd = 0.0d;
		this.totalPourReh = 0.0d;
		this.mapTotalPresentiel = new HashMap<String, Double>();
		this.mapTotalHetd = new HashMap<String, Double>();
	}

	/**
	 * Retourne le total général des heures "présentielles" (sans application d'aucun coefficient).
	 * @return Le total général des heures "présentielles" (sans application d'aucun coefficient)
	 */
	public double getTotalPresentiel() {
		return totalPresentiel;
	}

	/**
	 * Retourne le total général des heures en equilavent TD (les coefficients utilisés sont ceux "dans le service").
	 * @return Le total général des heures en equilavent TD
	 */
	public double getTotalHetd() {
		return totalHetd;
	}

	/**
	 * Retourne le total général des heures (en equilavent TD) pour les REH uniquement.
	 * @return Le total général des heures (en equilavent TD) pour les REH uniquement
	 */
	public double getTotalPourReh() {
		return totalPourReh;
	}

	/**
	 * Retourne le total général des heures "présentielles" par type d'AP (sans application d'aucun coefficient).
	 * @return Le total général des heures "présentielles" par type d'AP
	 */
	public Map<String, Double> getMapTotalPresentiel() {
		return mapTotalPresentiel;
	}
	
	/**
	 * Retourne le total général des heures en equilavent TD par type d'AP (les coefficients utilisés sont ceux "dans le service").
	 * @return Le total général des heures en equilavent TD par type d'AP
	 */
	public Map<String, Double> getMapTotalHetd() {
		return mapTotalHetd;
	}
	
	/**
	 * Ajout des heures au total général REH et aux totaux généraux.
	 * @param heuresPourReh Un nombre d'heure
	 */
	public void addHeuresPourReh(double heuresPourReh) {
		totalPresentiel += heuresPourReh;
		totalHetd += heuresPourReh;
		totalPourReh += heuresPourReh;
	}
	
	/**
	 * Ajout des heures HETD au total général par type d'AP.
	 * @param codeTypeAp Un code de type d'AP
	 * @param heuresPresentiellesPourAp Un nombre d'heure "présentielles" pour ce type d'AP (sans application d'aucun coefficient)
	 * @param hetdPourAp Un nombre d'heure en equilavent TD pour ce type d'AP (les coefficients à utiliser sont ceux "dans le service")
	 */
	public void addHetdPourAp(String codeTypeAp, double heuresPresentiellesPourAp, double hetdPourAp) {
		totalPresentiel += heuresPresentiellesPourAp;
		totalHetd += hetdPourAp;

		Double totalPresentielPourAp = getMapTotalPresentiel().get(codeTypeAp);
		
		if (totalPresentielPourAp == null) {
			totalPresentielPourAp = Double.valueOf(heuresPresentiellesPourAp);
		} else {
			totalPresentielPourAp = Double.valueOf(totalPresentielPourAp.doubleValue() + heuresPresentiellesPourAp);
		}
		
		getMapTotalPresentiel().put(codeTypeAp, totalPresentielPourAp);

		Double totalHetdPourAp = getMapTotalHetd().get(codeTypeAp);
		
		if (totalHetdPourAp == null) {
			totalHetdPourAp = Double.valueOf(hetdPourAp);
		} else {
			totalHetdPourAp = Double.valueOf(totalHetdPourAp.doubleValue() + hetdPourAp);
		}
		
		getMapTotalHetd().put(codeTypeAp, totalHetdPourAp);
	}
}
