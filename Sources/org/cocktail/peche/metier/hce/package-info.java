/**
 * Package contenant les différentes méthode de calcul du service HETD et des heures complémentaires des enseignants.
 * <p>
 * Ce package fourni différentes méthodes de calcul pré-définies, comme :
 * <ul>
 * <li>une méthode par proratisation (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalculParProratisation})</li>
 * <li>une méthode par proratisation sur les heures présentielles (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalculParProratisationSurPresentiel})</li>
 * <li>une méthode par priorité CM/TD/TP (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteCmTdTp})</li>
 * <li>une méthode par priorité TP/TD/CM (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteTpTdCm})</li>
 * </ul>
 * <p>
 * Il fourni également les éléments de bases pour la création d'autres méthode de calcul, comme :
 * <ul>
 * <li>une interface qui doit être implémenté obligatoirement par chaque méthode de calcul (classe {@link org.cocktail.peche.metier.hce.IMethodeDeCalcul})</li>
 * <li>une implémentation minimale de cette interface (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalcul})</li>
 * <li>une implémentation minimale pour les méthode par priorité (classe {@link org.cocktail.peche.metier.hce.MethodeDeCalculParPriorite})</li>
 * </ul>
 * <p>
 * Les méthodes de calcul doivent être déclarées dans la table <code>GRH_PECHE.METHODES_CALCUL_HCOMP</code> (pour pouvoir être utilisées dans <code>Peche</code>) où :
 * <ul>
 * <li><code>METHODE_ID</code> est la clé pimaire de la table;</li>
 * <li><code>LL_METHODE</code> est le libellé long de la méthode de calcul qui apparaîtra à l'écran;</li>
 * <li><code>CLASSE_METHODE</code> est le nom qualifier de la classe en charge du calcul (classe implémentant {@link org.cocktail.peche.metier.hce.IMethodeDeCalcul}).</li>
 * </ul>
 * <p>
 * Les méthodes ainsi déclarées peut être paramétrées (via l'écran "Param. HETD" dédié de l'application Peche) et utilisées dans Peche.
 */
package org.cocktail.peche.metier.hce;