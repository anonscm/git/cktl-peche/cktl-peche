package org.cocktail.peche.metier.hce;

import java.util.Map;


/**
 * Méthode de calcul des heures complémentaires par proratisation.
 * <p>
 * C'est la méthode choisie, entre autre, par Bordeaux Ségalen.
 * <ul>
 * <li>On commence par faire le total des heures en appliquant les coefficients "en service".</li>
 * <li>On calcule un pourcentage pour chaque type d'AP par rapport à ce total.</li>
 * <li>On applique chaque pourcentage aux heures au delas du service.</li>
 * <li>On réintroduit les coefficients "hors service".</li>
 * </ul>
 * <p>
 * Exemple pour :
 * <pre>
 *  56h CM =  84 HETD
 *  56h TD =  56 HETD
 * 100h TP = 100 HETD
 * 
 *  => 84 + 56 + 100 = 240 HETD de service prévisionnel.
 * </pre>
 * <p>
 * Avec service statutaire de 192h : 240 - 192 =  48 HCE
 * <p>
 * On recalcul la part des heures des différentes AP :
 * <ul>
 * <li>CM =  84 * 100 / 240 = 35%</li>
 * <li>TD =  56 * 100 / 240 = 23.3%</li>
 * <li>TP = 100 * 100 / 240 = 41.7%</li>
 * </ul>
 * 
 * On applique ces parts sur les HCE :
 * <ul>
 * <li>CM = 48 * 35.0% = 16.8</li>
 * <li>TD = 48 * 23.3% = 11.2</li>
 * <li>TP = 48 * 41.7% = 20.0</li> 
 * </ul>
 * <p>
 * On réintroduit les coefficients "hors service", par exemple 1TP = 2/3TD
 * <p>
 * 16.8 + 11.2 + (20.0 * 2/3) = 41.3 heures complémentaires.
 * 
 * @author Equipe PECHE.
 *
 */
public class MethodeDeCalculParProratisation extends MethodeDeCalcul {

	/**
	 * {@inheritDoc}
	 */
	public double hcomp(DonneesDuCalcul donneesDuCalcul) {
		if (donneesDuCalcul.getTotalHetd() <= getServiceDu()) {
			return 0;
		}
		
		double totalHetdHorsReh = donneesDuCalcul.getTotalHetd() - donneesDuCalcul.getTotalPourReh();
		double hcomp = 0.0d;

		// Les REH ne comptent pas dans la proratisation
		if (totalHetdHorsReh < getServiceDu()) {
			hcomp = -(getServiceDu() - totalHetdHorsReh);
		} else {
			double delta = totalHetdHorsReh - getServiceDu();
	
			if (delta != 0.0 && totalHetdHorsReh != 0.0) {
				for (Map.Entry<String, Double> entree : donneesDuCalcul.getMapTotalHetd().entrySet()) {
					Fraction coeffHorsService = getMapCoeffHorsService().get(entree.getKey());
					Fraction coeffService = getMapCoeffService().get(entree.getKey());
					Double totalAP = entree.getValue();
					double hcompAP = coeffHorsService.times(coeffService.inverse()).times(delta * totalAP / totalHetdHorsReh);
					hcomp += hcompAP;
				}
			}
		}
		
		// Les REH
		hcomp += donneesDuCalcul.getTotalPourReh();
		
		return hcomp;
	}
}
