package org.cocktail.peche.metier.hce;

import java.util.List;
import java.util.Map;

/**
 * Cette classe abstraite permet de créer simplement des méthodes de calcul par priorité de type d'AP.
 * <p>
 * Les différents éléments du service sont traité par priorité :
 * <ol>
 * <li>En premier lieu les AP de types retournés par {@link #getOrdreTypeApLesPremiers()} dans l'ordre de cette liste;</li>
 * <li>Ensuite les AP qui ne sont pas dans la liste {@link #getOrdreTypeApLesPremiers()} ni dans la liste {@link #getOrdreTypeApLesDerniers()} (pas d'ordre);</li>
 * <li>Ensuite les AP de type retournés par {@link #getOrdreTypeApLesDerniers()} dans l'ordre de cette liste;</li>
 * <li>Enfin les REH.</li>
 * </ol>
 * 
 * @author Equipe PECHE.
 *
 */
public abstract class MethodeDeCalculParPriorite extends MethodeDeCalcul {

	/**
	 * Retourne une liste de code de type d'AP spécifiant l'ordre de priorité des AP à prendre en compte en premiers.
	 * @return une liste de code de type d'AP spécifiant l'ordre de priorité des AP à prendre en compte en premiers
	 */
	public abstract List<String> getOrdreTypeApLesPremiers();
	
	/**
	 * Retourne une liste de code de type d'AP spécifiant l'ordre de priorité des AP à prendre en compte en derniers.
	 * @return une liste de code de type d'AP spécifiant l'ordre de priorité des AP à prendre en compte en derniers
	 */
	public abstract List<String> getOrdreTypeApLesDerniers();

	/**
	 * {@inheritDoc}
	 */
	public double hcomp(DonneesDuCalcul donneesDuCalcul) {
		if (donneesDuCalcul.getTotalHetd() <= getServiceDu()) {
			return 0;
		}
		
		Contexte contexteCalcul = new Contexte();

		// Prise en compte des AP selon l'ordre spécifié
		// Les premiers
		for (String codeTypeAp : getOrdreTypeApLesPremiers()) {
			Double totalHetdPourAp = donneesDuCalcul.getMapTotalHetd().get(codeTypeAp);
			
			if (totalHetdPourAp != null) {
				calculerHcomp(contexteCalcul, codeTypeAp, totalHetdPourAp);
			}
		}
		
		// Ajout des autres types d'AP
		for (Map.Entry<String, Double> entree : donneesDuCalcul.getMapTotalHetd().entrySet()) {
			if (!getOrdreTypeApLesPremiers().contains(entree.getKey())
					&& !getOrdreTypeApLesDerniers().contains(entree.getKey())) {
				calculerHcomp(contexteCalcul, entree.getKey(), entree.getValue());
			}
		}
		
		// Les derniers
		for (String codeTypeAp : getOrdreTypeApLesDerniers()) {
			Double totalHetdPourAp = donneesDuCalcul.getMapTotalHetd().get(codeTypeAp);
			
			if (totalHetdPourAp != null) {
				calculerHcomp(contexteCalcul, codeTypeAp, totalHetdPourAp);
			}
		}
		
		// Les REH
		calculerHcomp(contexteCalcul, null, donneesDuCalcul.getTotalPourReh());
		
		return contexteCalcul.hcomp;
	}

	/**
	 * Calcul des heures complémentaires et du service pour ce type d'AP.
	 * 
	 * @param contexteCalcul Le contexte de la méthode de calcul (pour les calculs intermédiaires)
	 * @param codeTypeAp Le code du type d'AP (null si REH)
	 * @param totalHetd Le total HETD pour ce type d'AP ou les REH
	 */
	private void calculerHcomp(Contexte contexteCalcul, String codeTypeAp, Double totalHetd) {
		double serviceRestant = getServiceDu() - contexteCalcul.service;
		
		if (totalHetd.doubleValue() < serviceRestant) {
			contexteCalcul.service += totalHetd.doubleValue();
		} else {
			if (codeTypeAp != null) {
				// AP
				Fraction coeffService = getMapCoeffService().get(codeTypeAp);
				Fraction coeffHorsService = getMapCoeffHorsService().get(codeTypeAp);
				
				contexteCalcul.hcomp += coeffHorsService.times(coeffService.inverse()).times(totalHetd.doubleValue() - serviceRestant);
			} else {
				// REH
				contexteCalcul.hcomp += totalHetd.doubleValue() - serviceRestant;
			}
			
			contexteCalcul.service = getServiceDu();
		}
	}
	
	/**
	 * Pour le jour où la méthode de calcul sera un singleton.
	 */
	private class Contexte {
		private double service = 0.0d;
		private double hcomp = 0.0d;
	}
}
