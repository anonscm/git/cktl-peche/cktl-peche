package org.cocktail.peche.metier.hce;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlgrh.common.metier.EOContrat;
import org.cocktail.fwkcktlpersonne.common.metier.EOCorps;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeContratTravail;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypePopulation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.parametres.hetd.Coefficients;
import org.cocktail.peche.entity.EOActuelEnseignant;
import org.cocktail.peche.entity.EOFicheVoeux;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOMethodesCalculHComp;
import org.cocktail.peche.entity.EOParamPopHetd;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EORepartMethodeCorps;
import org.cocktail.peche.entity.EORepartMethodeTypeContrat;
import org.cocktail.peche.entity.EORepartMethodeTypePopulation;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOServiceDetail;
import org.cocktail.peche.outils.OutilsValidation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Cette classe représente le moteur de calcul des heures complémentaires d'enseignement.
 * <p>
 * Le moteur de calcul utilise une méthode de calcul.
 * 
 * @author Equipe PECHE.
 */
public class MoteurDeCalcul {

	private static final IMethodeDeCalcul DUMMY_METHOD = new IMethodeDeCalcul() {

		private double servicePrevu;
		private double serviceRealise;
		
		
		
		public void traiterDetailService(EOServiceDetail detail, Fraction coeffService, Fraction coeffHorsService) {
			this.servicePrevu += detail.heuresPrevues();
			this.serviceRealise += detail.heuresRealisees();
		}

		public double serviceHETDPrevu() {
			return this.servicePrevu;
		}

		public double serviceHETDRealise() {
			return this.serviceRealise;
		}

		public void init(double serviceDu) {
			this.servicePrevu = 0;
			this.serviceRealise = 0;
		}

		public double hcompPrevu() {
			return 0;
		}

		public double hcompRealise() {
			return 0;
		}
		
		public void traiterDetailService(EOFicheVoeuxDetail detail, Fraction coeffService, Fraction coeffHorsService) {
			this.servicePrevu += detail.heuresSouhaitees();
		}
	};

	private static final Coefficients COEFFICIENTS_REH = new Coefficients(null, Fraction.UN, Fraction.UN);
	private static final double CENT = 100.0d;
	private static final double MILLE = 1000.0d;
	

	private static final String TYPE_POPULATION_CONTRAT = "Contrat";
	private static final String TYPE_POPULATION_CORPS = "Corp";
	private static final String TYPE_POPULATION_POPULATION = "Population";
	private static final String TYPE_POPULATION_TOUS = "*";
	
	private EOEditingContext ec;
	private EOParamPopHetd nullParam;

	private IMethodeDeCalcul methodeDeCalcul;
	private String libelleMethodeDeCalcul;
	
	private String typePopulation = "";
	private EOCorps corps;
	private EOTypePopulation population;
	private EOTypeContratTravail typeContrat;
	
	
	public MoteurDeCalcul(EOEditingContext ec) {
		this.ec = ec;
		this.nullParam = new EOParamPopHetd();
		this.nullParam.setNumerateurService(1);
		this.nullParam.setNumerateurHorsService(0);
		this.nullParam.setDenominateurService(1);
		this.nullParam.setDenominateurHorsService(1);

		typePopulation = "*";
		
		
		this.corps = null;
		this.population = null;
		this.typeContrat = null;
		this.libelleMethodeDeCalcul = "";
		this.methodeDeCalcul = DUMMY_METHOD;	
	}
	
	/**
	 * ATTENTION, UNIQUEMENT pour les TESTS UNITAIRES
	 * 
	 */
	public MoteurDeCalcul(EOEditingContext ec, EOCorps corps, int anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		this.ec = ec;
		this.nullParam = new EOParamPopHetd();
		this.nullParam.setNumerateurService(1);
		this.nullParam.setNumerateurHorsService(0);
		this.nullParam.setDenominateurService(1);
		this.nullParam.setDenominateurHorsService(1);
		
		this.corps = corps;
		this.population = null;
		this.typeContrat = null;
		
		EOQualifier qCorp = EORepartMethodeCorps.CORPS.eq(this.corps);
		EORepartMethodeCorps repartMethodeCorps = EORepartMethodeCorps.fetchEORepartMethodeCorps(ec, qCorp);

		
		String className = "";
		EOPecheParametre paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(ec, EOPecheParametre.KEY.eq("hcomp.methode.defaut"));
		if (paramMethodeParDefaut != null) {
			EOMethodesCalculHComp methodeCalcul = EOMethodesCalculHComp.fetchRequiredEOMethodesCalculHComp(ec, EOMethodesCalculHComp.ID.eq(Integer.valueOf(paramMethodeParDefaut.value())));
			className = methodeCalcul.classeMethode();
			typePopulation = TYPE_POPULATION_TOUS;
		}
		
		if (repartMethodeCorps != null) {
			//corp
			EOMethodesCalculHComp methodeDeCalculHComp = repartMethodeCorps.methode();
			className = methodeDeCalculHComp.classeMethode();
			typePopulation = TYPE_POPULATION_CORPS;
		} else {
			paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(ec, EOPecheParametre.KEY.eq("hcomp.methode.defaut"));
			if (paramMethodeParDefaut != null) {
				EOMethodesCalculHComp methodeCalcul = EOMethodesCalculHComp.fetchRequiredEOMethodesCalculHComp(ec, EOMethodesCalculHComp.ID.eq(Integer.valueOf(paramMethodeParDefaut.value())));
				className = methodeCalcul.classeMethode();
				typePopulation = TYPE_POPULATION_TOUS;
			}
		}
		IMethodeDeCalcul methode = null;
		if (className != null) {
			try {
				methode = (IMethodeDeCalcul) Class.forName(className).newInstance();
			} catch (Exception e) {
				methode = null;
			}
		}
		if (methode == null) {
			methode = DUMMY_METHOD;
		}
		this.methodeDeCalcul = methode;
		
	}
	
	public MoteurDeCalcul(EOEditingContext ec, EOActuelEnseignant enseignant,int anneeUniversitaire, boolean isFormatAnneeExerciceAnneeCivile) {
		this.ec = ec;
		this.nullParam = new EOParamPopHetd();
		this.nullParam.setNumerateurService(1);
		this.nullParam.setNumerateurHorsService(0);
		this.nullParam.setDenominateurService(1);
		this.nullParam.setDenominateurHorsService(1);
		
		corps = enseignant.getCorps(anneeUniversitaire, isFormatAnneeExerciceAnneeCivile);
		if (corps != null && corps.toTypePopulation() != null) {
			population = corps.toTypePopulation();
		} else {
			population = null;
		}
		
		EOContrat contrat = null;
		
		EOQualifier qualContrat = EOContrat.qualifierPourPeriode(enseignant.toIndividu(),
				OutilsValidation.getNSTDebutAnneeUniversitaire(anneeUniversitaire, isFormatAnneeExerciceAnneeCivile), 
				OutilsValidation.getNSTFinAnneeUniversitaire(anneeUniversitaire, isFormatAnneeExerciceAnneeCivile));

		NSArray<EOContrat> contrats = EOContrat.fetchAll(ec, qualContrat, EOContrat.D_DEB_CONTRAT_TRAV.ascs());
		if (!NSArrayCtrl.isEmpty(contrats)) {
			contrat = contrats.lastObject();
		}

		if (contrat != null && contrat.toTypeContratTravail() != null) {
			typeContrat = contrat.toTypeContratTravail();
		} else {
			typeContrat = null;
		}
		
		EOQualifier qCorp = EORepartMethodeCorps.CORPS.eq(corps);
		EORepartMethodeCorps repartMethodeCorps = EORepartMethodeCorps.fetchEORepartMethodeCorps(ec, qCorp);
		
		EOQualifier qPopulation = EORepartMethodeTypePopulation.TYPE_POPULATION.eq(population);
		EORepartMethodeTypePopulation repartMethodePopulation = EORepartMethodeTypePopulation.fetchEORepartMethodeTypePopulation(ec, qPopulation);
		
		EOQualifier qContrat = EORepartMethodeTypeContrat.TYPE_CONTRAT_TRAVAIL.eq(typeContrat);
		EORepartMethodeTypeContrat repartMethodeContrat = EORepartMethodeTypeContrat.fetchEORepartMethodeTypeContrat(ec, qContrat);

		String className = "";
		String libelleclassName = "";
	
		EOPecheParametre paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(ec, EOPecheParametre.KEY.eq("hcomp.methode.defaut"));
		if (paramMethodeParDefaut != null) {
			EOMethodesCalculHComp methodeCalcul = EOMethodesCalculHComp.fetchRequiredEOMethodesCalculHComp(ec, EOMethodesCalculHComp.ID.eq(Integer.valueOf(paramMethodeParDefaut.value())));
			className = methodeCalcul.classeMethode();
			libelleclassName = methodeCalcul.libelle();
			typePopulation = TYPE_POPULATION_TOUS;
		}

		if (repartMethodeContrat != null && repartMethodeCorps == null && repartMethodePopulation == null && typeContrat != null) {
			//contrat
			EOMethodesCalculHComp methodeDeCalculHComp = repartMethodeContrat.methode();
			className = methodeDeCalculHComp.classeMethode();
			libelleclassName = methodeDeCalculHComp.libelle();
			typePopulation = TYPE_POPULATION_CONTRAT;
		} else if (repartMethodeContrat == null && repartMethodeCorps != null) {
				//corp
				EOMethodesCalculHComp methodeDeCalculHComp = repartMethodeCorps.methode();
				className = methodeDeCalculHComp.classeMethode();
				libelleclassName = methodeDeCalculHComp.libelle();
				typePopulation = TYPE_POPULATION_CORPS;
			} else if (repartMethodeContrat == null && repartMethodeCorps == null && repartMethodePopulation != null) {
					//population
					EOMethodesCalculHComp methodeDeCalculHComp = repartMethodePopulation.methode();
					className = methodeDeCalculHComp.classeMethode();
					libelleclassName = methodeDeCalculHComp.libelle();
					typePopulation = TYPE_POPULATION_POPULATION;
				} else {
					//default
					paramMethodeParDefaut = EOPecheParametre.fetchEOPecheParametre(ec, EOPecheParametre.KEY.eq("hcomp.methode.defaut"));
					if (paramMethodeParDefaut != null) {
						EOMethodesCalculHComp methodeCalcul = EOMethodesCalculHComp.fetchRequiredEOMethodesCalculHComp(ec, EOMethodesCalculHComp.ID.eq(Integer.valueOf(paramMethodeParDefaut.value())));
						className = methodeCalcul.classeMethode();
						libelleclassName = methodeCalcul.libelle();
						typePopulation = TYPE_POPULATION_TOUS;
					}
				}
		
		IMethodeDeCalcul methode = null;
		if (className != null) {
			try {
				methode = (IMethodeDeCalcul) Class.forName(className).newInstance();
				this.libelleMethodeDeCalcul = libelleclassName;
			} catch (Exception e) {
				methode = null;
				this.libelleMethodeDeCalcul = "";
			}
		}
		if (methode == null) {
			methode = DUMMY_METHOD;
			this.libelleMethodeDeCalcul = "";
		}
		this.methodeDeCalcul = methode;	
	}
	
	/**
	 * Retourne la méthode de calcul qui sera utilisée par le moteur.
	 * @return la méthode de calcul
	 */
	public IMethodeDeCalcul getMethode() {
		return this.methodeDeCalcul;
	}

	/**
	 * Change la méthode de calcul qui sera utilisée par le moteur.
	 * @param methodeDeCalcul une nouvelle méthode de calcul
	 */
	public void remplacerMethode(IMethodeDeCalcul methodeDeCalcul) {
		this.methodeDeCalcul = methodeDeCalcul;
	}

	/**
	 * Calcul le service attribué en HETD en prenant en compte les coefficients dans et en dehors du service statutaire. 
	 * @param corps : Le corps de l'enseignant
	 * @param serviceDu : service dû
	 * @param listeServiceDetail : liste de serviceDetail sur lequel faire le calcul
	 */
	private void calculerServiceHETD(double serviceDu, NSArray<EOServiceDetail> listeServiceDetail) {
		methodeDeCalcul.init(serviceDu);
		
		for (EOServiceDetail detail : listeServiceDetail) {
			traiterCalculServiceHETDParServiceDetail(detail);
		}
	}
	
	/**
	 * 
	 * @param detail
	 */
	private void traiterCalculServiceHETDParServiceDetail(EOServiceDetail detail) {
		Map<String, Coefficients> mapCoefficients = new HashMap<String, Coefficients>();

			Coefficients coefficients;
			EOAP ap = detail.composantAP();
			coefficients = COEFFICIENTS_REH;
			if (ap == null) {
				// REH
				coefficients = COEFFICIENTS_REH;
				methodeDeCalcul.traiterDetailService(detail, coefficients.getEnService(), coefficients.getHorsService());
			} else {
				// AP
				//Pour les répartitions croisées, on ne prend pas en compte les répartitions refusées et en attente
				if (!(Constante.REFUSE.equals(detail.etat()) || Constante.ATTENTE.equals(detail.etat()))) {
					coefficients = mapCoefficients.get(ap.typeAP().code());
					
					if (coefficients == null) {
						coefficients = rechercherCoefficientsDefault(ap);
						if (typePopulation.equals(TYPE_POPULATION_CONTRAT) && typeContrat != null) {							
							coefficients = rechercherCoefficients(ap, typeContrat);
						}
						if (typePopulation.equals(TYPE_POPULATION_CORPS)  && corps != null) {
							coefficients = rechercherCoefficients(ap, corps);
						}
						if (typePopulation.equals(TYPE_POPULATION_POPULATION)  && population != null) {
							coefficients = rechercherCoefficients(ap, population);
						}
						
						mapCoefficients.put(ap.typeAP().code(), coefficients);
					}
					methodeDeCalcul.traiterDetailService(detail, coefficients.getEnService(), coefficients.getHorsService());
				}
			}
	}
	
	/**
	 * Calcul le service souhaité en HETD en prenant en compte les coefficients dans et en dehors du service statutaire. 
	 * @param corps : Le corps de l'enseignant
	 * @param serviceDu : service dû
	 * @param voeux : service souhaité
	 */
	private void calculerServiceHETD(double serviceDu, EOFicheVoeux voeux) {
		methodeDeCalcul.init(serviceDu);
		Map<String, Coefficients> mapCoefficients = new HashMap<String, Coefficients>();
		for (EOFicheVoeuxDetail detail : voeux.listeFicheVoeuxDetails()) {
			Coefficients coefficients = null;
			EOAP ap = detail.composantAP();
			if (ap != null) {
				coefficients = mapCoefficients.get(ap.typeAP().code());
				
				if (coefficients == null) {
					if (typePopulation.equals(TYPE_POPULATION_CONTRAT) && typeContrat != null) {
						coefficients = rechercherCoefficients(ap, typeContrat);
					}
					if (typePopulation.equals(TYPE_POPULATION_CORPS) && corps != null) {
						coefficients = rechercherCoefficients(ap, corps);
					}
					if (typePopulation.equals(TYPE_POPULATION_POPULATION) && population != null) {
						coefficients = rechercherCoefficients(ap, population);
					}
					if (typePopulation.equals(TYPE_POPULATION_TOUS)) {
						coefficients = rechercherCoefficientsDefault(ap);
					}
					mapCoefficients.put(ap.typeAP().code(), coefficients);
				}
				methodeDeCalcul.traiterDetailService(detail, coefficients.getEnService(), coefficients.getHorsService());
			}
		}
	}
	
	
	
	/**
	 * Recherche les coefficient paramétré pour ce type de population (ou par défaut)
	 * @param ap l'AP
	 * @param population la population
	 * @return Les coefficients pour cette AP
	 */
	private Coefficients rechercherCoefficients(EOAP ap, EOTypePopulation population) {
		NSTimestamp now = new NSTimestamp();
		
		EOQualifier qualifier = EOParamPopHetd.TYPE_POPULATION.eq(population).and(
				EOParamPopHetd.TYPE_AP
						.eq(ap.typeAP())
						.and(EOParamPopHetd.DATE_DEBUT.before(now))
						.and(EOParamPopHetd.DATE_FIN.isNull().or(
								EOParamPopHetd.DATE_FIN.after(now))));
		EOParamPopHetd param = EOParamPopHetd.fetchEOParamPopHetd(ec,
				qualifier);
		
		
		// Lecture des coefficients par défaut (corps = null)
		if (param == null) {
			qualifier = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_POPULATION.isNull()).
					and(EOParamPopHetd.T_CONTRAT.isNull())
					.and(
					EOParamPopHetd.TYPE_AP
							.eq(ap.typeAP())
							.and(EOParamPopHetd.DATE_DEBUT.before(now))
							.and(EOParamPopHetd.DATE_FIN.isNull().or(
									EOParamPopHetd.DATE_FIN.after(now))));
			param = EOParamPopHetd.fetchEOParamPopHetd(ec, qualifier);
		}
		
		Fraction coeffService;
		Fraction coeffHorsService;
		
		// Récupération des coefficients
		if (param != null) {
			coeffService = new Fraction(param.numerateurService(), param.denominateurService());
			coeffHorsService = new Fraction(param.numerateurHorsService(), param.denominateurHorsService());
		} else {
			coeffService = Coefficients.getCoeffServiceParDefaut(ap.typeAP());
			coeffHorsService = Coefficients.getCoeffHorsServiceParDefaut(ap.typeAP());
		}
		
		return new Coefficients(ap.typeAP(), coeffService, coeffHorsService);
	}
	

	private Coefficients rechercherCoefficientsDefault(EOAP ap) {
		NSTimestamp now = new NSTimestamp();
		
		EOParamPopHetd param = null;		
		// Lecture des coefficients par défaut (corps = null)
		if (param == null) {
			EOQualifier qualifier = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_POPULATION.isNull()).
					and(EOParamPopHetd.T_CONTRAT.isNull())
					.and(
					EOParamPopHetd.TYPE_AP
							.eq(ap.typeAP())
							.and(EOParamPopHetd.DATE_DEBUT.before(now))
							.and(EOParamPopHetd.DATE_FIN.isNull().or(
									EOParamPopHetd.DATE_FIN.after(now))));
			param = EOParamPopHetd.fetchEOParamPopHetd(ec, qualifier);
		}
		
		Fraction coeffService;
		Fraction coeffHorsService;
		
		// Récupération des coefficients
		if (param != null) {
			coeffService = new Fraction(param.numerateurService(), param.denominateurService());
			coeffHorsService = new Fraction(param.numerateurHorsService(), param.denominateurHorsService());
		} else {
			coeffService = Coefficients.getCoeffServiceParDefaut(ap.typeAP());
			coeffHorsService = Coefficients.getCoeffHorsServiceParDefaut(ap.typeAP());
		}
		
		return new Coefficients(ap.typeAP(), coeffService, coeffHorsService);
	}
	
	/**
	 * Recherche les coefficient paramétré pour ce type de contrat (ou par défaut)
	 * @param ap l'AP
	 * @param contrat le contrat
	 * @return Les coefficients pour cette AP
	 */
	private Coefficients rechercherCoefficients(EOAP ap, EOTypeContratTravail contrat) {
		NSTimestamp now = new NSTimestamp();
		
		EOQualifier qualifier = EOParamPopHetd.T_CONTRAT.eq(contrat).and(
				EOParamPopHetd.TYPE_AP
						.eq(ap.typeAP())
						.and(EOParamPopHetd.DATE_DEBUT.before(now))
						.and(EOParamPopHetd.DATE_FIN.isNull().or(
								EOParamPopHetd.DATE_FIN.after(now))));
		EOParamPopHetd param = EOParamPopHetd.fetchEOParamPopHetd(ec,
				qualifier);
		
		// Lecture des coefficients par défaut (corps = null)
		if (param == null) {
			qualifier = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_POPULATION.isNull()).
					and(EOParamPopHetd.T_CONTRAT.isNull())
					.and(
					EOParamPopHetd.TYPE_AP
							.eq(ap.typeAP())
							.and(EOParamPopHetd.DATE_DEBUT.before(now))
							.and(EOParamPopHetd.DATE_FIN.isNull().or(
									EOParamPopHetd.DATE_FIN.after(now))));
			param = EOParamPopHetd.fetchEOParamPopHetd(ec, qualifier);
		}
		
		Fraction coeffService;
		Fraction coeffHorsService;
		
		// Récupération des coefficients
		if (param != null) {
			coeffService = new Fraction(param.numerateurService(), param.denominateurService());
			coeffHorsService = new Fraction(param.numerateurHorsService(), param.denominateurHorsService());
		} else {
			coeffService = Coefficients.getCoeffServiceParDefaut(ap.typeAP());
			coeffHorsService = Coefficients.getCoeffHorsServiceParDefaut(ap.typeAP());
		}
		
		return new Coefficients(ap.typeAP(), coeffService, coeffHorsService);
	}

	/**
	 * Recherche les coefficient paramétré pour ce corps (ou par défaut).
	 * @param ap L'AP
	 * @param corps Le corps de l'enseignant
	 * @return Les coefficients pour cette AP et pour ce corps
	 */
	private Coefficients rechercherCoefficients(EOAP ap, EOCorps corps) {
		NSTimestamp now = new NSTimestamp();
		
		EOQualifier qualifier = EOParamPopHetd.CORPS.eq(corps).and(
				EOParamPopHetd.TYPE_AP
						.eq(ap.typeAP())
						.and(EOParamPopHetd.DATE_DEBUT.before(now))
						.and(EOParamPopHetd.DATE_FIN.isNull().or(
								EOParamPopHetd.DATE_FIN.after(now))));
		EOParamPopHetd param = EOParamPopHetd.fetchEOParamPopHetd(ec,
				qualifier);
		
		// Lecture des coefficients par défaut
		if (param == null) {
			qualifier = EOParamPopHetd.CORPS.isNull().and(EOParamPopHetd.TYPE_POPULATION.isNull()).
					and(EOParamPopHetd.T_CONTRAT.isNull())
					.and(
					EOParamPopHetd.TYPE_AP
							.eq(ap.typeAP())
							.and(EOParamPopHetd.DATE_DEBUT.before(now))
							.and(EOParamPopHetd.DATE_FIN.isNull().or(
									EOParamPopHetd.DATE_FIN.after(now))));
			param = EOParamPopHetd.fetchEOParamPopHetd(ec, qualifier);
		}
		
		Fraction coeffService;
		Fraction coeffHorsService;
		
		// Récupération des coefficients
		if (param != null) {
			coeffService = new Fraction(param.numerateurService(), param.denominateurService());
			coeffHorsService = new Fraction(param.numerateurHorsService(), param.denominateurHorsService());
		} else {
			coeffService = Coefficients.getCoeffServiceParDefaut(ap.typeAP());
			coeffHorsService = Coefficients.getCoeffHorsServiceParDefaut(ap.typeAP());
		}
		
		return new Coefficients(ap.typeAP(), coeffService, coeffHorsService);
	}
	
	/**
	 * Retourne le nombre d'heures complémentaires prévu en équivalent TD.
	 * @param serviceDu le service dû
	 * @param service le service attribué à l'enseignant, en heures présentielles
	 * @return le nombre d'heures complémentaires prévu en équivalent TD
	 */
	public double getHeuresComplementairesPrevues(double serviceDu, EOService service) {

		if (service == null) {
			throw new NullPointerException("Le service ne peut pas être null");
		}

		calculerServiceHETD(serviceDu, service.listeServiceDetails());

		return arrondirAuMillieme(methodeDeCalcul.hcompPrevu());
	}

	/**
	 * Retourne le nombre d'heures complémentaires réalisé en équivalent TD.
	 * @param serviceDu le service dû
	 * @param service le service attribué à l'enseignant, en heures présentielles
	 * @return le nombre d'heures complémentaires réalisé en équivalent TD
	 */
	public double getHeuresComplementairesRealisees(double serviceDu, EOService service) {

		if (service == null) {
			throw new NullPointerException("Le service ne peut pas être null");
		}

		calculerServiceHETD(serviceDu, service.listeServiceDetails());

		return arrondirAuMillieme(methodeDeCalcul.hcompRealise());
	}

	/**
	 * Retourne le service attribué à l'enseignant, en heures équivalent TD.
	 * @param serviceDu le service dû par l'enseignant
	 * @param listeServiceDetail liste restreinte ou non de détails sur laquelle effectuer le calcul
	 * @return le service attribué à l'enseignant, en heures équivalent TD
	 */
	public double getServiceHETDPrevu(double serviceDu, NSArray<EOServiceDetail> listeServiceDetail) {

		calculerServiceHETD(serviceDu, listeServiceDetail);

		return arrondirAuMillieme(methodeDeCalcul.serviceHETDPrevu());
	}
	
	/**
	 * Retourne le service réalisé par l'enseignant, en heures équivalent TD.
	 * @param serviceDu le service dû par l'enseignant
	 * @param listeServiceDetail liste restreinte ou non sur laquelle effectuer le calcul
	 * @return le service réalisé par l'enseignant, en heures équivalent TD
	 */
	public double getServiceHETDRealise(double serviceDu, NSArray<EOServiceDetail> listeServiceDetail) {

		calculerServiceHETD(serviceDu, listeServiceDetail);

		return arrondirAuMillieme(methodeDeCalcul.serviceHETDRealise());
	}
	
	/**
	 * Retourne le service souhaité par l'enseignant, en heures équivalent TD.
	 * @param serviceDu : le service dû par l'enseignant
	 * @param voeux :  le service souhaité par l'enseignant, en heures présentielles
	 * @return le service souhaité par l'enseignant, en heures équivalent TD
	 */
	public double getServiceHETDSouhaite(double serviceDu, EOFicheVoeux voeux) {
		if (voeux == null) {
			throw new NullPointerException("Le voeux ne peut pas être null");
		}

		calculerServiceHETD(serviceDu, voeux);

		return arrondirAuMillieme(methodeDeCalcul.serviceHETDPrevu());
	}

	/**
	 * Converti des heures présentielles en HETD à l'aide du coefficient "En service".
	 * <p>
	 * Ne sers principalement qu'à convertir des heures à payer présentielles des vacataires qui ne sont jamais au delà du service.
	 * @param ap l'ap
	 * @param heuresPresentielles les heures présentielles à convertir
	 * @return Le heures en HETD
	 */
	public BigDecimal convertirEnHetdEnService(EOAP ap, BigDecimal heuresPresentielles) {
		Coefficients coefficients = null;

		if (typePopulation.equals(TYPE_POPULATION_CONTRAT) && typeContrat != null) {
			coefficients = rechercherCoefficients(ap, typeContrat);
		}		
		if (typePopulation.equals(TYPE_POPULATION_CORPS) && corps != null) {
			coefficients = rechercherCoefficients(ap, corps);
		}
		if (typePopulation.equals(TYPE_POPULATION_POPULATION) && population != null) {
			coefficients = rechercherCoefficients(ap, population);
		}
		if (typePopulation.equals(TYPE_POPULATION_TOUS)) {
			coefficients = rechercherCoefficientsDefault(ap);
		}
		
		if (coefficients == null) {
			coefficients = rechercherCoefficientsDefault(ap);
		}
		
		if (coefficients != null && coefficients.getEnService() != null && heuresPresentielles != null) {
			return coefficients.getEnService().times(heuresPresentielles);
		} 
		
		return heuresPresentielles;
	}
	
	/**
	 * @param nbHeures Un nombre d'heures à arrondir
	 * @return Le nombre d'heures arrondi au centième le plus proche
	 */
	private double arrondirAuCentieme(double nbHeures) {
		return Math.rint(nbHeures * CENT) / CENT;
	}
	
	/**
	 * @param nbHeures Un nombre d'heures à arrondir
	 * @return Le nombre d'heures arrondi au millième le plus proche
	 */
	private double arrondirAuMillieme(double nbHeures) {
		return Math.rint(nbHeures * MILLE) / MILLE;
	}
	



	public IMethodeDeCalcul getMethodeDeCalcul() {
		return methodeDeCalcul;
	}



	public String getLibelleMethodeDeCalcul() {
		return libelleMethodeDeCalcul;
	}
}
