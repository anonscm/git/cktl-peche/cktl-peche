package org.cocktail.peche.metier.hce;

import java.util.Map;


/**
 * Méthode de calcul des heures complémentaires par proratisation par rapport aux nombres d'heures présentielles.
 * <p>
 * C'est la méthode choisie, entre autre, par l'INSA de ROUEN pour certains corps.
 * <p>
 * <ul>
 * <li>On commence par faire le total des heures "présentielles" (sans appliquer de coefficient).</li>
 * <li>On calcul un pourcentage pour chaque type d'AP par rapport à ce total.</li>
 * <li>Avec ce pourcentage, on en déduit le nombre d'heures dans le service (par rapport au service dû) et hors service.</li>
 * <li>Le nombre d'heures dans le service étant plafonnées au nombre d'heures réellement faites.</li>
 * <li>On réintroduit les coefficients "dans le service" et "hors service".</li>
 *</ul>
 * <p>
 * Exemple pour :
 * <pre>
 *  40h CM =  60 HETD
 * 100h TD = 100 HETD
 *  60h TP =  40 HETD
 * 
 *  => 40 + 100 + 60 = 200 heures présentielles.
 *  => 60 + 100 + 40 = 200 HETD de service prévisionnel.
 * </pre>
 * On recalcul la part des heures des différentes AP par rapport au présentiel :
 * <ul>
 * <li>CM =  40 * 100 / 200 = 20%</li>
 * <li>TD = 100 * 100 / 200 = 50%</li>
 * <li>TP =  60 * 100 / 200 = 30%</li>
 * </ul>
 * <p>
 * On applique ces parts sur le service dû (192h) :
 * <ul>
 * <li>CM = 192 * 20% = 38.4</li>
 * <li>TD = 192 * 50% = 96.0</li>
 * <li>TP = 192 * 30% = 57.6</li> 
 * </ul>
 * <p>
 * On réintroduit les coefficients "dans le service", par exemple 1CM = 1.5TD
 * <p>
 * (38.4 * 1.5) + 96 + 57.6 = 211.2 hetd.
 * <p>
 * On réintroduit les coefficients "hors service" sur les parts hors service, par exemple 1TP = 2/3TD
 * <p>
 * ((40 - 38.4) * 1.5) + (100 - 96) + ((60 - 57.6) * 2/3) = 8 hetd.
 * <p>
 * On a donc un total de 211.2 + 8 = 219.2 hetd.
 * Avec service statutaire de 192h : 219.2 - 192 =  27.2 HCE
 * 
 * @author Equipe PECHE.
 */
public class MethodeDeCalculParProratisationSurPresentiel extends MethodeDeCalcul {

	/**
	 * {@inheritDoc}
	 */
	public double hcomp(DonneesDuCalcul donneesDuCalcul) {
		if (donneesDuCalcul.getTotalHetd() <= getServiceDu()) {
			return 0;
		}
		
		double totalHetdHorsReh = donneesDuCalcul.getTotalHetd() - donneesDuCalcul.getTotalPourReh();
		double hcomp = 0.0d;

		// Les REH ne comptent pas dans la proratisation
		if (totalHetdHorsReh < getServiceDu()) {
			hcomp = -(getServiceDu() - totalHetdHorsReh);
		} else {
			// On proratise par rapport au présentiel
			// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			double totalPresentielHorsReh = donneesDuCalcul.getTotalPresentiel() - donneesDuCalcul.getTotalPourReh();
			
			if (totalPresentielHorsReh != 0.0) {
				for (Map.Entry<String, Double> entree : donneesDuCalcul.getMapTotalPresentiel().entrySet()) {
					Fraction coeffService = getMapCoeffService().get(entree.getKey());
					Fraction coeffHorsService = getMapCoeffHorsService().get(entree.getKey());
					Double totalAP = entree.getValue();
					double totalDansService = getServiceDu() * (totalAP / totalPresentielHorsReh);

					// HComp liées au coefficient "dans le service"
					double hcompAP = coeffService.times(totalDansService) - totalDansService;
					// HComp des heures hors service
					hcompAP += coeffHorsService.times(totalAP - totalDansService);

					hcomp += hcompAP;
				}
			}
		}
		
		// Les REH
		hcomp += donneesDuCalcul.getTotalPourReh();
		
		return hcomp;
	}
}
