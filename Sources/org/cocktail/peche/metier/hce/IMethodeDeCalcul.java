package org.cocktail.peche.metier.hce;

import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOServiceDetail;

/**
 * Interface devant être implémentée par toutes les méthodes de calcul.
 * 
 * @author Equipe PECHE.
 */
public interface IMethodeDeCalcul {

	/**
	 * Initialisation du calcul.
	 * <p>
	 * Le {@link MoteurDeCalcul} appelle cette méthode avant chaque calcul.
	 * 
	 * @param serviceDu le service dû par l'enseignant.
	 */
	void init(double serviceDu);

	/**
	 * Traitement à effectuer sur un élément du service (une AP ou un REH).
	 * <p>
	 * Le {@link MoteurDeCalcul} appelle cette méthode une fois par détail de service d'une fiche de service.
	 * Les appels ne sont par ordonnés (il n'y a pas de tri par code AP, par AP ou par REH).
	 * @param detail la ligne de détail du service de l'enseignant.
	 * @param coeffService le coefficient a appliquer dans le service.
	 * @param coeffHorsService le coefficient a appliquer en dehors du service.
	 */
	void traiterDetailService(EOServiceDetail detail, Fraction coeffService, Fraction coeffHorsService);
	
	/**
	 * Traitement à effectuer sur l'AP de la fiche de voeux
	 * <p>
	 * Le {@link MoteurDeCalcul} appelle cette méthode une fois par détail de voeux d'une fiche de voeux.
	 * Les appels ne sont par ordonnés (il n'y a pas de tri par code AP ou par AP).
	 * @param detail la ligne de détail du voeux de l'enseignant.
	 * @param coeffService le coefficient à appliquer dans le calcul des voeux.
	 * @param coeffHorsService le coefficient à appliquer en dehors dans le calcul des voeux.
	 */
	void traiterDetailService(EOFicheVoeuxDetail detail, Fraction coeffService, Fraction coeffHorsService);

	/**
	 * Retourne le nombre d'heures complémentaires prévu.
	 * @return le nombre d'heures complémentaires prévu
	 */
	double hcompPrevu();

	/**
	 * Retourne le nombre d'heures complémentaires réalisé.
	 * @return le nombre d'heures complémentaires réalisé
	 */
	double hcompRealise();

	/**
	 * Retourne le service prévisionnel affecté à l'enseignant, en heures équivalent TD.
	 * @return le service prévisionnel affecté à l'enseignant, en heures équivalent TD
	 */
	double serviceHETDPrevu();

	/**
	 * Retourne le service réalisé par l'enseignant, en heures équivalent TD.
	 * @return le service réalisé par l'enseignant, en heures équivalent TD
	 */
	double serviceHETDRealise();
}
