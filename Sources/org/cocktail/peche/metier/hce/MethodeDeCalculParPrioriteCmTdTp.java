package org.cocktail.peche.metier.hce;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;


/**
 * Méthode de calcul des heures complémentaires par priorité CM/TD/TP.
 * <p>
 * C'est la méthode choisie, entre autre, par La Rochelle.
 * <p>
 * On compte d'abord les heures de CM, puis les heures de TD (et toutes les autres sauf les CM et les TP), puis les heures de TP.
 * Si le total dépasse le service dû, on applique le coefficient sur le dépassement.
 * <p>
 * Exemple pour :
 * <pre>
 *  56h CM =  84 HETD
 *  56h TD =  56 HETD
 * 100h TP = 100 HETD
 * </pre>
 * Les 84 HETD des CM ne suffisent pas à remplir le service.
 * On y ajoute les 56 HETD des TD, on arrive à 140 HETD.
 * Cela ne suffit toujours pas à remplir le service.
 * Celui-ci est complété par 52 HETD provenant des TP.
 * Il reste donc 48 HETD provenant des TP.
 * <p>
 * Avec un coefficient de 1TP=2/3TD en dehors du service, on arrive à
 * 48 * 2/3 = 32 heures complémentaires. 
 * 
 * @author Equipe PECHE.
 *
 */
public class MethodeDeCalculParPrioriteCmTdTp extends MethodeDeCalculParPriorite {

	private static List<String> ordreTypeApLesPremiers;
	private static List<String> ordreTypeApLesDerniers;
	
	static {
		ordreTypeApLesPremiers = new ArrayList<String>(2);
		ordreTypeApLesPremiers.add(EOTypeAP.TYPECOURS_CODE);
		ordreTypeApLesPremiers.add(EOTypeAP.TYPETD_CODE);

		ordreTypeApLesDerniers = new ArrayList<String>(1);
		ordreTypeApLesDerniers.add(EOTypeAP.TYPETP_CODE);
	}
	
	public List<String> getOrdreTypeApLesPremiers() {
		return ordreTypeApLesPremiers;
	}

	public List<String> getOrdreTypeApLesDerniers() {
		return ordreTypeApLesDerniers;
	}
}
