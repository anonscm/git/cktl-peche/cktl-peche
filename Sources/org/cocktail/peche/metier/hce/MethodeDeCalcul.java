package org.cocktail.peche.metier.hce;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAP;
import org.cocktail.peche.entity.EOFicheVoeuxDetail;
import org.cocktail.peche.entity.EOServiceDetail;

/**
 * Cette classe abstraite fourni une implémentation minimale pour les méthode de calcul.
 * <p>
 * On peut la dériver pour créer des méthodes de calcul.
 * 
 * @author Equipe PECHE.
 */
public abstract class MethodeDeCalcul implements IMethodeDeCalcul {

	/** Service dû au delà duquel les heures sont comptées en heures complémentaires. */
	private double serviceDu;

	/** Coefficient "dans le service" pour un type d'AP. */
	private Map<String, Fraction> mapCoeffService;
	/** Coefficient "au delà du service" pour un type d'AP. */
	private Map<String, Fraction> mapCoeffHorsService;

	private DonneesDuCalcul donneesHeuresPrevues = new DonneesDuCalcul();
	private DonneesDuCalcul donneesHeuresRealisees = new DonneesDuCalcul();	
	
	/**
	 * Retourne le service dû par l'enseignant.
	 * @return le service dû par l'enseignant
	 */
	public double getServiceDu() {
		return serviceDu;
	}

	/**
	 * Retourne les coefficients "dans le service" par type d'AP (la clef est le code du type d'AP).
	 * @return les coefficients "dans le service" par type d'AP
	 */
	public Map<String, Fraction> getMapCoeffService() {
		return mapCoeffService;
	}

	/**
	 * Retourne les coefficients "au delà du service" par type d'AP (la clef est le code du type d'AP).
	 * @return les coefficients "au delà du service" par type d'AP
	 */
	public Map<String, Fraction> getMapCoeffHorsService() {
		return mapCoeffHorsService;
	}

	/**
	 * {@inheritDoc} 
	 */
	public void init(double serviceDu) {
		this.serviceDu = serviceDu;
		this.donneesHeuresPrevues.init();
		this.donneesHeuresRealisees.init();
		
		this.mapCoeffService = new HashMap<String, Fraction>();
		this.mapCoeffHorsService = new HashMap<String, Fraction>();
	}

	/**
	 * {@inheritDoc}
	 */
	public void traiterDetailService(EOServiceDetail detail, Fraction coeffService, Fraction coeffHorsService) {
		EOAP ap = detail.composantAP();
		
		if (ap == null) {
			// REH
			donneesHeuresPrevues.addHeuresPourReh(retournerNonNull(detail.heuresPrevues()));
			donneesHeuresRealisees.addHeuresPourReh(retournerNonNull(detail.heuresRealisees()));
		} else {
			// AP
			String codeTypeAp = ap.typeAP().code();
			double heuresPrevues = retournerNonNull(detail.heuresPrevues());
			double heuresRealisees = retournerNonNull(detail.heuresRealisees());
			double hetdHeuresPrevues = coeffService.times(heuresPrevues);
			double hetdHeuresRealisees = coeffService.times(heuresRealisees);
			
			this.mapCoeffService.put(codeTypeAp, coeffService);
			this.mapCoeffHorsService.put(codeTypeAp, coeffHorsService);
			
			donneesHeuresPrevues.addHetdPourAp(codeTypeAp, heuresPrevues, hetdHeuresPrevues);
			donneesHeuresRealisees.addHetdPourAp(codeTypeAp, heuresRealisees, hetdHeuresRealisees);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void traiterDetailService(EOFicheVoeuxDetail detail, Fraction coeffService, Fraction coeffHorsService) {
		EOAP ap = detail.composantAP();
		
		if (ap != null) {
			String codeTypeAp = ap.typeAP().code();
			double heuresSouhaitees = retournerNonNull(detail.heuresSouhaitees());
			double hetdHeuresSouhaitees = coeffService.times(heuresSouhaitees);
			
			this.mapCoeffService.put(codeTypeAp, coeffService);
			this.mapCoeffHorsService.put(codeTypeAp, coeffHorsService);
			
			donneesHeuresPrevues.addHetdPourAp(codeTypeAp, heuresSouhaitees, hetdHeuresSouhaitees);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public double hcompPrevu() {
		return hcomp(donneesHeuresPrevues);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public double hcompRealise() {
		return hcomp(donneesHeuresRealisees);
	}
	
	/**
	 * Retourne le nombre d'heures complémentaires en utilisant les données du calcul passés en paramètre.
	 * @param donneesDuCalcul Les données permettant le calcul des heures complémentaires
	 * @return le nombre d'heures complémentaires
	 */
	public abstract double hcomp(DonneesDuCalcul donneesDuCalcul);

	/**
	 * {@inheritDoc}
	 */
	public double serviceHETDPrevu() {
		return serviceHETD(donneesHeuresPrevues);
	}

	/**
	 * {@inheritDoc}
	 */
	public double serviceHETDRealise() {
		return serviceHETD(donneesHeuresRealisees);
	}
	
	/**
	 * {@inheritDoc}
	 */
	private double serviceHETD(DonneesDuCalcul donneesDuCalcul) {
		double serviceHetd;
		
		if (donneesDuCalcul.getTotalHetd() > getServiceDu()) {
			serviceHetd = getServiceDu() + hcomp(donneesDuCalcul);
		} else {
			serviceHetd = donneesDuCalcul.getTotalHetd();
		}
		
		return serviceHetd;
	}

	/**
	 * @param unDouble Un {@link Double}
	 * @return La valeur du {@link Double} ou 0 si <code>null</code>
	 */
	private double retournerNonNull(Double unDouble) {
		if (unDouble == null) {
			return 0;
		}
		
		return unDouble.doubleValue();
	}
}
