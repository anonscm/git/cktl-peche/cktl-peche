package org.cocktail.peche.metier.hce;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeAP;


/**
 * Méthode de calcul des heures complémentaires par priorité TP/TD/CM.
 * <p>
 * C'est la méthode choisie, entre autre, par l'INSA de TOULOUSE pour les enseignants du second degré.
 * <p>
 * On compte d'abord les heures de TP, puis les heures de TD (et toutes les autres sauf les CM et les TP), puis les heures de CM.
 * Si le total dépasse le service dû, on applique le coefficient sur le dépassement.
 * <p>
 * Exemple pour :
 * <pre>
 * 100h TP = 100 HETD
 *  56h TD =  56 HETD
 *  56h CM =  84 HETD
 * </pre>
 * Les 100 HETD des TP ne suffisent pas à remplir le service.
 * On y ajouter les 56 HETD des TD, on arrive à 156 HETD.
 * Cela ne suffit toujours pas à remplir le service.
 * Celui-ci est complété par 36 HETD provenant des CM.
 * Il reste donc 20 HETD provenant des CM.
 * <p>
 * Avec un coefficient de 1CM=3/2TD en dehors du service, on arrive à
 * 48 * 3/2 = 72 heures complémentaires. 
 * 
 * @author Equipe PECHE.
 *
 */
public class MethodeDeCalculParPrioriteTpTdCm extends MethodeDeCalculParPriorite {

	private static List<String> ordreTypeApLesPremiers;
	private static List<String> ordreTypeApLesDerniers;
	
	static {
		ordreTypeApLesPremiers = new ArrayList<String>(2);
		ordreTypeApLesPremiers.add(EOTypeAP.TYPETP_CODE);
		ordreTypeApLesPremiers.add(EOTypeAP.TYPETD_CODE);

		ordreTypeApLesDerniers = new ArrayList<String>(1);
		ordreTypeApLesDerniers.add(EOTypeAP.TYPECOURS_CODE);
	}
	
	public List<String> getOrdreTypeApLesPremiers() {
		return ordreTypeApLesPremiers;
	}
	
	public List<String> getOrdreTypeApLesDerniers() {
		return ordreTypeApLesDerniers;
	}
}
