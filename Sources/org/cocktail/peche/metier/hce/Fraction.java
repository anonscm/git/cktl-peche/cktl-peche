package org.cocktail.peche.metier.hce;

import java.math.BigDecimal;
import java.math.RoundingMode;


/**
 * Classe utilitaire pour gérer les fractions.
 * 
 * @author Equipe PECHE.
 */
public class Fraction {

	/** Numérateur. */
	private Integer numerateur;
	/** Dénominateur. */
	private Integer denominateur;

	/** Constante représentant le nombre un (1/1). */
	public static final Fraction UN = new Fraction(1, 1);

	/**
	 * Constructeur.
	 * @param numerateur Numérateur
	 * @param denominateur Dénominateur
	 */
	public Fraction(Integer numerateur, Integer denominateur) {
		if (denominateur == 0) {
			throw new ArithmeticException("Le dénominateur ne peut pas être null");
		}
		
		int gcd = gcd(Math.abs(numerateur), Math.abs(denominateur));
		
		this.numerateur = numerateur / gcd;
		this.denominateur = denominateur / gcd;
	}

	/**
	 * Multiplication.
	 * @param nombre Le nombre qui doit être multiplié par cette fraction.
	 * @return Le résultat de la multiplication.
	 */
	public double times(double nombre) {
		return nombre * (double) numerateur / (double) denominateur;
	}
	
	/**
	 * Multiplication.
	 * @param nombre Le nombre qui doit être multiplié par cette fraction.
	 * @return Le résultat de la multiplication.
	 */
	public BigDecimal times(BigDecimal nombre) {
		return nombre.multiply(BigDecimal.valueOf(numerateur)).divide(BigDecimal.valueOf(denominateur), RoundingMode.HALF_UP);
	}

	/**
	 * Multiplication.
	 * @param fraction Une fraction
	 * @return Le résultat de la multiplication des deux fractions.
	 */
	public Fraction times(Fraction fraction) {
		return new Fraction(numerateur * fraction.numerateur, denominateur * fraction.denominateur);
	}
	
	/**
	 * Inversion. 
	 * @return L'inverse de cette fraction.
	 */
	public Fraction inverse() {
		if (this.numerateur == null) {
			throw new ArithmeticException("Impossible d'inverser une fraction dont le numérateur est null");
		}
		return new Fraction(this.denominateur, this.numerateur);
	}

	/**
	 * Retourne le numérateur.
	 * @return Le numérateur
	 */
	public int getNumerateur() {
		return numerateur;
	}
	
	/**
	 * Affecte le numérateur.
	 * @param numerateur Le nouveau numérateur
	 */
	public void setNumerateur(int numerateur) {
		this.numerateur = numerateur;
	}

	/**
	 * Retourne le dénominateur.
	 * @return Le dénominateur
	 */
	public int getDenominateur() {
		return denominateur;
	}
	
	/**
	 * Affecte le dénominateur.
	 * @param denominateur Le nouveau dénominateur
	 */
	public void setDenominateur(int denominateur) {
		this.denominateur = denominateur;
	}
	
	/* Cacul du plug grand diviseur commun */
	private int gcd(int a, int b) {
		if (b == 0) {
			return a;
		} else {
			return gcd(b, a % b);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((denominateur == null) ? 0 : denominateur.hashCode());
		result = prime * result
				+ ((numerateur == null) ? 0 : numerateur.hashCode());
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Fraction other = (Fraction) obj;
		if (denominateur == null) {
			if (other.denominateur != null) {
				return false;
			}
		} else if (!denominateur.equals(other.denominateur)) {
			return false;
		}
		if (numerateur == null) {
			if (other.numerateur != null) {
				return false;
			}
		} else if (!numerateur.equals(other.numerateur)) {
			return false;
		}
		return true;
	}	
}
