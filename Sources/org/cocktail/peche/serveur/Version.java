package org.cocktail.peche.serveur;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWonder;
import org.cocktail.peche.Application;

import er.extensions.appserver.ERXApplication;

/**
 * Gestion du versioning. Cette classe permet de g̩érer les d̩pendances de
 * version. 
 * 
 * {@see VersionMe} pour les changements de version.
 */
public class Version extends A_CktlVersion {

	
	/** Version de la base de donnees requise pour le User GRHUM. */
    private static final String BD_GRH_PECHE_VERSION_MIN = "1.4.0.0";
    private static final String BD_GRH_PECHE_VERSION_MAX = null;
    
    
	/** Version de la base de donnees requise pour le User GRHUM. */
    private static final String BD_GRHUM_VERSION_MIN = "1.9.1.0";
    private static final String BD_GRHUM_VERSION_MAX = null;
    
	/** Version de la base de donnees requise pour le User SCO_SCOLARITE. */
    private static final String BD_SCO_SCOLARITE_VERSION_MIN = "1.4.16.0";
    private static final String BD_SCO_SCOLARITE_VERSION_MAX = null;
    
    /** Version de la base de donnees requise pour le User MANGUE. */
    private static final String BD_MANGUE_VERSION_MIN = "1.7.14.0";
    private static final String BD_MANGUE_VERSION_MAX = null;
    
	/* Version de WebObjects */
	private static final String WO_VERSION_MIN = "5.4.3.0";
	private static final String WO_VERSION_MAX = null;

	/* Version du JRE */
	private static final String JRE_VERSION_MIN = "1.6.0.0";
	private static final String JRE_VERSION_MAX = null;

	/* Version d'ORACLE */
	private static final String ORACLE_VERSION_MIN = "9.0";
	private static final String ORACLE_VERSION_MAX = null;

	/* Version du frmk FwkCktlWebApp */
	private static final String CKTLWEBAPP_VERSION_MIN = "4.0.1";
	private static final String CKTLWEBAPP_VERSION_MAX = null;

	/* Version de WONDER */
	private static final String WONDER_VERSION_MIN = "5.0.0.9000";
	private static final String WONDER_VERSION_MAX = null;

	@Override
	public String name() {
		return VersionMe.APPLICATIONFINALNAME;
	}

	@Override
	public int versionNumMaj() {
		return VersionMe.VERSIONNUMMAJ;
	}

	@Override
	public int versionNumMin() {
		return VersionMe.VERSIONNUMMIN;
	}

	@Override
	public int versionNumPatch() {
		return VersionMe.VERSIONNUMPATCH;
	}

	@Override
	public int versionNumBuild() {
		return VersionMe.VERSIONNUMBUILD;
	}

	@Override
	public String date() {
		return VersionMe.VERSIONDATE;
	}

	@Override
	public String comment() {
		return VersionMe.COMMENT;
	}

	@Override
	public CktlVersionRequirements[] dependencies() {
		boolean isDevMode = ERXApplication.isDevelopmentModeSafe();
		boolean isDbFlyway = ((Application) ERXApplication.application()).isDbFlyway();
		
		// Si on est en mode dev ou si la base est migrée avec flyway,
		// on n'empeche pas le démarrage de l'application si le contrôle des versions BDD échouent
		boolean controlePassant = isDevMode || isDbFlyway;
		
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionWebObjects(), WO_VERSION_MIN, WO_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionJava(), JRE_VERSION_MIN, JRE_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionOracleServer(), ORACLE_VERSION_MIN, ORACLE_VERSION_MAX, false),
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.Version(), CKTLWEBAPP_VERSION_MIN, CKTLWEBAPP_VERSION_MAX, true),
				new CktlVersionRequirements(new CktlVersionWonder(), WONDER_VERSION_MIN, WONDER_VERSION_MAX, true),
	    		new CktlVersionRequirements(new org.cocktail.fwkcktlpersonne.server.VersionDatabase(), BD_GRHUM_VERSION_MIN, BD_GRHUM_VERSION_MAX, !controlePassant),
	    		new CktlVersionRequirements(new org.cocktail.fwkcktlgrh.server.VersionDatabase(), BD_MANGUE_VERSION_MIN, BD_MANGUE_VERSION_MAX, !controlePassant),
	    		new CktlVersionRequirements(new org.cocktail.fwkcktlscolpeda.serveur.VersionDatabase(), BD_SCO_SCOLARITE_VERSION_MIN, BD_SCO_SCOLARITE_VERSION_MAX, 
	    				!controlePassant),
				new CktlVersionRequirements(new org.cocktail.peche.serveur.VersionDatabase(), BD_GRH_PECHE_VERSION_MIN, BD_GRH_PECHE_VERSION_MAX, 
	    				!controlePassant)
		};
	}

}
