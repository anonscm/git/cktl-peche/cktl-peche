package org.cocktail.peche.serveur;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EODatabaseChannel;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOSQLExpression;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

import er.extensions.eof.ERXConstant;
import er.extensions.foundation.ERXThreadStorage;

public class SqlLogger {


    private static final String SQLCOUNT = "sqlcount";
    private static Logger log = Logger.getLogger(SqlLogger.class);

    public static void setupDelegate() {
        NSNotificationCenter.defaultCenter().addObserver(SqlLogger.class,
                new NSSelector("dataBaseChannelNeeded", ERXConstant.NotificationClassArray),
                EODatabaseContext.DatabaseChannelNeededNotification, null);
    }

    public boolean adaptorChannelShouldEvaluateExpression(EOAdaptorChannel channel, EOSQLExpression expression) {
        Integer sqlCount = (Integer) ERXThreadStorage.valueForKey(SQLCOUNT);
        if (sqlCount == null) {
            sqlCount = 0;
        }
        sqlCount++;
        Thread.dumpStack();
        ERXThreadStorage.takeValueForKey(sqlCount, SQLCOUNT);
        return true;
    }

    /**
     * Answers to the EODataBaseChannelNeeded notification. 
     * Creates a new EODatabaseChannel and sets its adaptorChannel delegate 
     * to a new instance of ERXAdaptorChannelDelegate.
     * @param n
     */
    static public void dataBaseChannelNeeded(NSNotification n) {
            EODatabaseContext context = (EODatabaseContext) n.object();
            EODatabaseChannel channel = new EODatabaseChannel(context);
            context.registerChannel(channel);
            channel.adaptorChannel().setDelegate(new SqlLogger());
    }

    public static Integer sqlCount() {
        return (Integer)ERXThreadStorage.valueForKey(SQLCOUNT);
    }
    
}
