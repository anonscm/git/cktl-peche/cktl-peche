package org.cocktail.peche;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.cocktail.fwkcktlpeche.circuitvalidation.EtapePeche;
import org.cocktail.fwkcktlpeche.circuitvalidation.TypeCheminPeche;
import org.cocktail.fwkcktlpeche.droits.Fonction;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;
import org.cocktail.fwkcktlpersonne.common.metier.droits.DroitsHelper;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfilDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdTypeDroitFonction;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique.StructureRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.apipublique.StructureRepositoryEOF;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EODemande;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.peche.commun.IPecheParametres;
import org.cocktail.peche.commun.PecheParametres;
import org.cocktail.peche.components.commun.Constante;
import org.cocktail.peche.components.controlers.IEnseignantsVacatairesCtrl;
import org.cocktail.peche.entity.EODroit;
import org.cocktail.peche.entity.EOPecheParametre;
import org.cocktail.peche.entity.EORepartUefStructure;
import org.cocktail.peche.entity.EOService;
import org.cocktail.peche.entity.EOTypePopulation;
import org.cocktail.peche.entity.interfaces.IEnseignant;
import org.cocktail.peche.metier.finder.DroitsFinder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe implémentant les méthodes de vérification des droits par profil pour l'application Peche "en dur".
 *
 */
public class PecheAutorisationsCache extends AutorisationsCache {

	
	private List<IStructure> listeCibleEnseignants;
	private List<IStructure> listeCibleEnseignantsStatutaires = new ArrayList<IStructure>();
	private List<IStructure> listeCibleEnseignantsVacataire = new ArrayList<IStructure>();
	

	private List<IStructure> listeCibleEnseignement;
	private List<IStructure> listeCibleEnseignementStatutaires  = new ArrayList<IStructure>();
	private List<IStructure> listeCibleEnseignementVacataire = new ArrayList<IStructure>();
	
	private List<IStructure> listeDepartementsEnseignementVacataire = new ArrayList<IStructure>();
	private List<IStructure> listeComposantesEnseignementVacataire = new ArrayList<IStructure>();

	private List<IStructure> listeDepartementsEnseignement;
	private List<IStructure> listeComposantesScolariteEnseignement;
	private List<IStructure> listeDepartementsEnseignant;
	private List<IStructure> listeComposantesScolariteEnseignant;
	
	private List<IStructure> listeStructAdditionnellesUEFlottantes = new ArrayList<IStructure>();
	
	private StructureRepository structureRepository;
	
	private EOEditingContext editingContext;
	private Integer persId;
	private EOIndividu individu;
	private PecheParametres pecheParametres;
	private boolean pecheParametresDejaRecherche = false;

	
	// autorisations spécifiques Peche
	private Map<String, String> autorisationsOnFonctionCache;
	
	private List<Integer> listeIdProfilAGrhum;
	
	/**
	 * Constructeur rendu privé.
	 *
	 * @param appStrId Nom de l'application dans GRHUM.GD_APPLICATION
	 * @param persId persId de la personne connectée
	 */
	private PecheAutorisationsCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}

	/**
	 * Constructeur.
	 *
	 * Instancie un cache d'autorisation pour l'application PECHE.
	 *
	 * @param persId persId de la personne connectée
	 * @param editingContext editing contexte
	 */
	public PecheAutorisationsCache(Integer persId, EOEditingContext editingContext) {
		this(Constante.APP_PECHE, persId);
		this.editingContext = editingContext;
		this.persId = persId;
		
		listeCibleEnseignants = new ArrayList<IStructure>();
		listeCibleEnseignantsStatutaires = new ArrayList<IStructure>();
		listeCibleEnseignantsVacataire = new ArrayList<IStructure>();
		listeCibleEnseignement = new ArrayList<IStructure>();
		listeCibleEnseignementStatutaires = new ArrayList<IStructure>();
		listeCibleEnseignementVacataire = new ArrayList<IStructure>();

		refreshCache();
	}
	
	private void refreshCache() {
		initGestionDroits();
	}
	

	// Méthodes utilitaires pour l'affichage de menu
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	// Menu 'Enseignants'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignants statutaires" ?
	 * @param isEnseignantStatutaire true/false
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuEnseignantsStatutaires(boolean isEnseignantStatutaire, Integer annee) {
		return hasDroitShowMenuGroupEnseignantsPopulationStatutaires()
				|| hasDroitShowMenuGroupEnseignantsRepartitionCroisee()
				|| hasDroitShowMenuGroupEnseignantsFicheVoeux(isEnseignantStatutaire, annee)
				|| isEnseignantStatutaire;
				
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignants vacataires" ?
	 * @param isEnseignantVacataire true/false
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuEnseignantsVacataires(boolean isEnseignantVacataire) {
		return hasDroitShowMenuGroupEnseignantsPopulationVacataires()
				|| isEnseignantVacataire;
				
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignants statutaires" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsPopulationStatutaires() {
		return ((hasDroitShowEnseignants() || hasDroitShowParametrageEnseignantsGeneriques()) && hasListeCibleEnseignantsStatutaires());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignants vacataires" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsPopulationVacataires() {
		return ((hasDroitShowEnseignants() || hasDroitShowParametrageEnseignantsGeneriques()) && hasListeCibleEnseignantsVacataire());
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Répartition croisée" du menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsRepartitionCroisee() {
		return (hasDroitShowRepartitionCroisee() && hasListeCibleEnseignantsStatutaires());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le groupe "Fiche de voeux" du menu "Enseignants" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantsFicheVoeux(boolean isEnseignantStatutaire, Integer annee) {
		return EOPecheParametre.isFicheVoeuxUtilisation(editingContext, annee) && (hasDroitShowListeFichesVoeux() || hasDroitShowEnseignantFicheVoeux(isEnseignantStatutaire));
	}

	// Menu 'Enseignements'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuEnseignements() {
		return hasDroitShowMenuGroupEnseignementsFormation()
				|| hasDroitShowMenuGroupEnseignementsUe();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Formation" du menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignementsFormation() {
		return hasDroitShowOffreFormation();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "UE" du menu "Enseignements" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignementsUe() {
		return hasDroitShowUe();
	}

	// Menu 'Paramétrages'
	/**
	 * Est-ce que j'ai le droit de voir le menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuParametrage() {
		return hasDroitShowMenuGroupParametrageParametrages()
				|| hasDroitShowMenuGroupParametrageHetd()
				|| hasDroitShowMenuGroupParametrageMep()
				|| hasDroitShowParametrageGestionDroits();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "Paramétrages" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageParametrages() {
		return hasDroitShowParametrageCircuitsValidation()
				|| hasDroitShowParametrageActivites();
	}

	/**
	 * Est-ce que j'ai le droit de voir le groupe "MEP" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageMep() {
		return hasDroitShowParametrageTauxHoraires()
				|| hasDroitShowParametrageTypeFluxPaiement();
	}
	/**
	 * Est-ce que j'ai le droit de voir le groupe "HETD" du menu "Paramétrage" ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupParametrageHetd() {
		return hasDroitShowParametrageTypesHoraires()
				|| hasDroitShowParametrageParametresPopulations();
	}

	// Méthodes d'accès au droits de niveau fonction
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Est-ce que j'ai le droit d'accéder à l'application 'peche' ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitAccesPeche() {
		return hasDroitUtilisationPeche() || hasDroitConnaissancePeche();
	}

	
	/**
	 * Est-ce que j'ai le droit d'accéder à l'application 'peche' avec les droits de modifications ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationPeche() {
		return hasDroitUtilisationOnFonction(Fonction.PECHE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur Peche ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissancePeche() {
		return hasDroitConnaissanceOnFonction(Fonction.PECHE);
	}

	/**
	 * Est-ce que j'ai le droit de voir les enseignants ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowEnseignants() {
		return (hasDroitUtilisationEnseignants() || hasDroitConnaissanceEnseignants());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les enseignants ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationEnseignants() {
		return hasDroitUtilisationOnFonction(Fonction.ENSEIGNANTS);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les enseignants ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceEnseignants() {
		return hasDroitConnaissanceOnFonction(Fonction.ENSEIGNANTS);
	}

	/**
	 * Est-ce que j'ai le droit de voir les répartitions croisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowRepartitionCroisee() {
		return (hasDroitUtilisationRepartitionCroisee() || hasDroitConnaissanceRepartitionCroisee());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les répartitions croisées ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationRepartitionCroisee() {
		return hasDroitUtilisationOnFonction(Fonction.REPARTITION_CROISEE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les répartitions croisées ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceRepartitionCroisee() {
		return hasDroitConnaissanceOnFonction(Fonction.REPARTITION_CROISEE);
	}
	
	
	/**
	 * Est-ce que j'ai le droit de voir les répartitions des heures complémentaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowRepartitionHeuresComplementaires() {
		return (hasDroitUtilisationRepartitionHeuresComplementaires() || hasDroitConnaissanceRepartitionHeuresComplementaires());
	}

	/**
	 * Est-ce que j'ai le droit de saisir la répartition des heures complémentaires pour un enseignant statutaire ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationRepartitionHeuresComplementaires() {
		return hasDroitUtilisationOnFonction(Fonction.REPARTITION_HCOMP);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les répartitions des heures complémentaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceRepartitionHeuresComplementaires() {
		return hasDroitConnaissanceOnFonction(Fonction.REPARTITION_HCOMP);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le report des heures complémentaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowReportHeuresComplementaires() {
		return (hasDroitUtilisationReportHeuresComplementaires() || hasDroitConnaissanceReportHeuresComplementaires());
	}

	/**
	 * Est-ce que j'ai le droit de saisir le report des heures complémentaires pour un enseignant statutaire ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationReportHeuresComplementaires() {
		return hasDroitUtilisationOnFonction(Fonction.REPORT_HCOMP);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le report des heures complémentaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceReportHeuresComplementaires() {
		return hasDroitConnaissanceOnFonction(Fonction.REPORT_HCOMP);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir ma fiche en tant qu'enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMenuGroupEnseignantFicheService() {
		return (hasDroitUtilisationValidationParEnseignant());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir ma fiche de voeux en tant qu'enseignant ?
	 * @param isEnseignantStatutaire true si l'enseignant est statutaire
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowEnseignantFicheVoeux(boolean isEnseignantStatutaire) {
		return (hasDroitUtilisationValidationParEnseignant()) && isEnseignantStatutaire;
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la liste des fiches de voeux ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowListeFichesVoeux() {
		return (hasDroitUtilisationValidationParRepartiteur() && hasListeCibleEnseignantsStatutaires());
	}

	/**
	 * Est-ce que j'ai le droit de voir les offres de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowOffreFormation() {
		return (hasDroitUtilisationOffreFormation() || hasDroitConnaissanceOffreFormation());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les offres de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationOffreFormation() {
		return hasDroitUtilisationOnFonction(Fonction.OFFRE_FORMATION);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les offres de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceOffreFormation() {
		return hasDroitConnaissanceOnFonction(Fonction.OFFRE_FORMATION);
	}

	/**
	 * Est-ce que j'ai le droit de voir les UE ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowUe() {
		return (hasDroitUtilisationUe() || hasDroitConnaissanceUe());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les UE ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationUe() {
		return hasDroitUtilisationOnFonction(Fonction.UE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les UE ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceUe() {
		return hasDroitConnaissanceOnFonction(Fonction.UE);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de composante ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurComposante() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_COMPOSANTE);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de département ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurDepartement() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_DEPARTEMENT);
	}
	
	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que directeur de collège ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDirecteurCollege() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DIRECTEUR_COLLEGE);
	}
	
	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que DRH ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParDrh() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_DRH);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant qu'enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParEnseignant() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_ENSEIGNANT);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que président ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParPresident() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_PRESIDENT);
	}

	/**
	 * Est-ce que j'ai le droit de validation des fiches (voeux/service) en tant que répartiteur ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParRepartiteur() {
		return hasDroitUtilisationOnFonction(Fonction.VALID_FICHE_REPARTITEUR);
	}

	/**
	 * renvoie true si la personne connectée a tous les droits de validation
	 * @return vrai/faux
	 */
	public boolean getFonctionToutesValidations() {
		return hasDroitUtilisationValidationParRepartiteur() && hasDroitUtilisationValidationParDirecteurComposante() 
				&& hasDroitUtilisationValidationParDrh() && hasDroitUtilisationValidationParPresident() && hasDroitUtilisationValidationParEnseignant();
	}
	
	
	/**
	 * Est-ce que j'ai le droit de voir les activités ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageActivites() {
		return (hasDroitUtilisationParametrageActivites() || hasDroitConnaissanceParametrageActivites());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les activités ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageActivites() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ACTIVITES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les activités ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageActivites() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ACTIVITES);
	}

	/**
	 * Est-ce que j'ai le droit de voir les circuits de validation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageCircuitsValidation() {
		return (hasDroitUtilisationParametrageCircuitsValidation() || hasDroitConnaissanceParametrageCircuitsValidation());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les circuits de validation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageCircuitsValidation() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_CIRCUITS_VALIDATION);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur les circuits de validation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageCircuitsValidation() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_CIRCUITS_VALIDATION);
	}

	/**
	 * Est-ce que j'ai le droit de voir les enseignants génériques ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageEnseignantsGeneriques() {
		return (hasDroitUtilisationParametrageEnseignantsGeneriques() || hasDroitConnaissanceParametrageEnseignantsGeneriques());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les enseignants génériques ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageEnseignantsGeneriques() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ENSEIGNANTS_GENERIQUES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des enseignants génériques ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageEnseignantsGeneriques() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ENSEIGNANTS_GENERIQUES);
	}

	/**
	 * Est-ce que j'ai le droit de voir l'établissement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageUefChoixComposantes() {
		return (hasDroitUtilisationParametrageUefChoixComposantes() || hasDroitConnaissanceParametrageUefChoixComposantes());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) l'établissement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageUefChoixComposantes() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_UEF_CHOIX_COMPOSANTES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage établissement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageUefChoixComposantes() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_UEF_CHOIX_COMPOSANTES);
	}

	/**
	 * Est-ce que j'ai le droit d'utiliser la duplication ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageDuplication() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_DUPLICATION);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la gestion des droits ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageGestionDroits() {
		return (hasDroitConnaissanceParametrageGestionDroits() || hasDroitUtilisationParametrageGestionDroits());
	}
	
	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des droits ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageGestionDroits() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_GESTION_DROITS);
	}
	
	/**
	 * Est-ce que j'ai le droit d'utiliser la gestion des droits ?
	 * 
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageGestionDroits() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_GESTION_DROITS);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir les paramètres des populations ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageParametresPopulations() {
		return (hasDroitUtilisationParametrageParametresPopulations() || hasDroitConnaissanceParametrageParametresPopulations());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) les paramètres des populations ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageParametresPopulations() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_PARAMETRES_POPULATIONS);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des populations ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageParametresPopulations() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_PARAMETRES_POPULATIONS);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des types d'horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTypesHoraires() {
		return (hasDroitUtilisationParametrageTypesHoraires() || hasDroitConnaissanceParametrageTypesHoraires());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des types d'horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTypesHoraires() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_TYPES_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des types horaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageTypesHoraires() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_TYPES_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre de groupe ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageNbGroupe() {
		return (hasDroitUtilisationParametrageNbGroupe() || hasDroitConnaissanceParametrageNbGroupe());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre de groupe dans l'offre de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageNbGroupe() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_NB_GROUPE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre de groupe dans l'offre de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageNbGroupe() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_NB_GROUPE);
	}

	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre d'heures maquette ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageNbHeureMaquette() {
		return (hasDroitUtilisationParametrageNbHeureMaquette() || hasDroitConnaissanceParametrageNbHeureMaquette());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre d'heures maquette dans l'offre de formation ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageNbHeureMaquette() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_NB_HEURE_MAQUETTE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre d'heures maquette dans l'offre de formation ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageNbHeureMaquette() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_NB_HEURE_MAQUETTE);
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la surcharge du détail de l'offre de formation (nombre de groupe ou nombre 
	 * d'heures maquette des AP) ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageDetailOffreFormation() {
		return (hasDroitShowParametrageNbGroupe() || hasDroitShowParametrageNbHeureMaquette());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir la surcharge du nombre d'heures de l'absence d'un enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageHeuresAbsence() {
		return (hasDroitUtilisationParametrageHeuresAbsence() || hasDroitConnaissanceParametrageHeuresAbsence());
	}

	/**
	 * Est-ce que j'ai le droit de surcharger le nombre d'heures de l'absence d'un enseignant ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageHeuresAbsence() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_ABSENCE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la surcharge du nombre d'heures de l'absence d'un enseignant ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageHeuresAbsence() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_ABSENCE);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des taux horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTauxHoraires() {
		return (hasDroitUtilisationParametrageTauxHoraires() || hasDroitConnaissanceParametrageTauxHoraires());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des taux horaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTauxHoraires() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_TAUX_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des taux horaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceParametrageTauxHoraires() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_TAUX_HORAIRES);
	}

	/**
	 * Est-ce que j'ai le droit de voir le paramétrage des types de flux de paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowParametrageTypeFluxPaiement() {
		return (hasDroitUtilisationParametrageTypeFluxPaiement() || hasDroitConnaissanceParametrageTypeFluxPaiement());
	}

	/**
	 * Est-ce que j'ai le droit de gérer (ajouter/modifier/supprimer) le paramétrage des types de flux de paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationParametrageTypeFluxPaiement() {
		return hasDroitUtilisationOnFonction(Fonction.PARAM_FLUX_PAIEMENT);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur le paramétrage des types de flux de paiement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceParametrageTypeFluxPaiement() {
		return hasDroitConnaissanceOnFonction(Fonction.PARAM_FLUX_PAIEMENT);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir les champs de saisie des heures à payer ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieHeuresAPayer() {
		return (hasDroitUtilisationSaisieHeuresAPayer() || hasDroitConnaissanceSaisieHeuresAPayer());
	}

	/**
	 * Est-ce que j'ai le droit de gérer les heures à payer ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieHeuresAPayer() {
		return hasDroitUtilisationOnFonction(Fonction.HEURES_A_PAYER);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la gestion des heures à payer ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieHeuresAPayer() {
		return hasDroitConnaissanceOnFonction(Fonction.HEURES_A_PAYER);
	}

	/**
	 * Est-ce que j'ai le droit de voir la mise en paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowMiseEnPaiement() {
		return (hasDroitUtilisationMiseEnPaiement() || hasDroitConnaissanceMiseEnPaiement());
	}

	/**
	 * Est-ce que j'ai le droit de gérer la mise en paiement ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationMiseEnPaiement() {
		return hasDroitUtilisationOnFonction(Fonction.MISE_EN_PAIEMENT);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la mise en paiement ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceMiseEnPaiement() {
		return hasDroitConnaissanceOnFonction(Fonction.MISE_EN_PAIEMENT);
	}

	
	/**
	 * Est-ce que j'ai le droit de saisir la répartition REH des vacataires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieRehVacataire() {
		return hasDroitUtilisationOnFonction(Fonction.SAISIE_REH_VACATAIRE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la répartition REH des vacataires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieRehVacataire() {
		return hasDroitConnaissanceOnFonction(Fonction.SAISIE_REH_VACATAIRE);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir la répartition REH des vacataires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieRehVacataire() {
		return (hasDroitUtilisationSaisieRehVacataire() || hasDroitConnaissanceSaisieRehVacataire());
	}
	
	/**
	 * Est-ce que j'ai le droit de saisir la répartition REH des statutaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationSaisieRehStatutaire() {
		return hasDroitUtilisationOnFonction(Fonction.SAISIE_REH_STATUTAIRE);
	}

	/**
	 * Est-ce que j'ai le droit de connaissance sur la répartition REH des statutaires ?
	 * <p>
	 * Attention : C'est juste le droit "Connaissance".
	 * Pour le droit de voir une fonction utiliser plutôt les méthodes {@link #hasDroitShowXxx()}.
	 *
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceSaisieRehStatutaire() {
		return hasDroitConnaissanceOnFonction(Fonction.SAISIE_REH_STATUTAIRE);
	}

	
	/**
	 * Est-ce que j'ai le droit de voir la répartition REH des statutaires ?
	 *
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowSaisieRehStatutaire() {
		return (hasDroitUtilisationSaisieRehStatutaire() || hasDroitConnaissanceSaisieRehStatutaire());
	}
	
	/**
	 * Est-ce que j'ai le droit de voir le bouton "Validation par UE / EC" ?
	 * 
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationValidationParUeEc() {
		return hasDroitUtilisationValidationParRepartiteur() 
				|| hasDroitUtilisationValidationParDirecteurComposante() 
				|| hasDroitUtilisationValidationParDirecteurDepartement(); 
	}
	
	/**
	 * Méthode recherchant les droits soit dans les droits Peche, soit dans ceux définis dans AGrhum pour les enseignants et admin.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationOnFonction(Fonction fonction) {
		if (autorisationsOnFonctionCache != null && autorisationsOnFonctionCache.containsKey(fonction.getIdFonction())) {
			return EOGdTypeDroitFonction.getTypeDroitUtilisation().tdfStrId().equals(autorisationsOnFonctionCache.get(fonction.getIdFonction()));
		}
		return hasDroitUtilisationOnFonction(fonction.getIdFonction());
	}

	/**
	 * Méthode recherchant les droits soit dans les droits Peche, soit dans ceux définis dans AGrhum pour les enseignants et admin.
	 *
	 * @param fonction Le droit fonction
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitConnaissanceOnFonction(Fonction fonction) {
		if (autorisationsOnFonctionCache != null && autorisationsOnFonctionCache.containsKey(fonction.getIdFonction())) {
			return EOGdTypeDroitFonction.getTypeDroitUtilisation().tdfStrId().equals(autorisationsOnFonctionCache.get(fonction.getIdFonction()))
					|| EOGdTypeDroitFonction.getTypeDroitConnaissance().tdfStrId().equals(autorisationsOnFonctionCache.get(fonction.getIdFonction()));
		}
		return hasDroitConnaissanceOnFonction(fonction.getIdFonction());
	}
	
	
	// Méthodes d'accès au droits de prise d'un chemin dans un circuit
	// °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	/**
	 * Est-ce que j'ai le droit d'emprunter (ou de voir) le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param mode "U" pour tester le droit "Utilisable" ou "C" pour le droit "Connaissance"
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitOnChemin(String mode, EODemande demande, TypeCheminPeche typeChemin) {
		EOEtape etapeDemande = demande.toEtape();

		EOChemin chemin = etapeDemande.cheminEmpruntable(typeChemin.getCodeTypeChemin());

		// Si pas de chemin empruntable
		if (chemin == null) {
			return false;
		}

		EtapePeche enumEtape = EtapePeche.getEtape(etapeDemande.toCircuitValidation().codeCircuitValidation(), etapeDemande.codeEtape());
		Fonction[] listeFonctionsAutorisees = enumEtape.getListeFonctionsAutorisees();

		if (listeFonctionsAutorisees == null) {
			// Pas de droits particulier à avoir pour gérer cette étape
			return true;
		}

		// A-t-on un des droits en question ?
		for (int i = 0; i < listeFonctionsAutorisees.length; i++) {
			Fonction fonction = listeFonctionsAutorisees[i];

			if (("U".equals(mode) && hasDroitUtilisationOnFonction(fonction))
					|| ("C".equals(mode) && hasDroitConnaissanceOnFonction(fonction))) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Est-ce que j'ai le droit de voir le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin à prendre
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitShowChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitUtilisationChemin(demande, typeChemin) || hasDroitConnaissanceChemin(demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit d'emprunter le chemin depuis l'étape sur laquelle est la demande ?
	 * <p>
	 * Il faut que la demande soit à l'étape indiquée pour avoir le droit.
	 *
	 * @param demande La demande
	 * @param etape L'étape d'origine
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationChemin(EODemande demande, EtapePeche etape, TypeCheminPeche typeChemin) {
		// Pour avoir le droit d'emprunter ce chemin, l'étape en cours de la demande doit être la même que l'étape de départ
		if (etape != null && !etape.getCodeEtape().equals(demande.toEtape().codeEtape())) {
			return false;
		}

		return hasDroitOnChemin("U", demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit d'emprunter le chemin depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	public boolean hasDroitUtilisationChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitOnChemin("U", demande, typeChemin);
	}

	/**
	 * Est-ce que j'ai le droit de voir le chemin empruntable depuis l'étape sur laquelle est la demande ?
	 *
	 * @param demande La demande
	 * @param typeChemin Le type de chemin
	 * @return <code>true</code> si on a le droit
	 */
	private boolean hasDroitConnaissanceChemin(EODemande demande, TypeCheminPeche typeChemin) {
		return hasDroitOnChemin("C", demande, typeChemin);
	}
	
	private StructureRepository getStructureRepository() {
		if (structureRepository == null) {
			structureRepository = new StructureRepositoryEOF(editingContext);
		}
		
		return structureRepository;
	}
	
	/**
	 * @param service service en cours
	 * @param ctrl controleur
	 * @return composante de scolarité et/ou liste départements selon besoin
	 */
	public List<IStructure> getStructuresPourVacataires(IEnseignantsVacatairesCtrl ctrl, EOService service) {
		List<IStructure> listeComposante = new ArrayList<IStructure>();

		// si le circuit de validation a une étape VISA DEPARTEMENT, ou si l'on est sur un circuit prévisionnel, on regarde également
		// si le définitif contient une étape VISA_DEPARTEMENT
		// alors on ne prend que les départements et pas les composantes de scolarité (règle générale -> cf. spécificités)
		if (ctrl.estServiceAvecCircuitVisaDepartement(this.editingContext, service)) {
			if (!CollectionUtils.isEmpty(getListeDepartementsEnseignementVacataire())) {
				listeComposante.addAll(getListeDepartementsEnseignementVacataire());
			}
			
		} else {
			// cas classique - pas de visa département sur le circuit de validation
			if (!CollectionUtils.isEmpty(getListeComposantesEnseignementVacataire())) {
				listeComposante.addAll(getListeComposantesEnseignementVacataire());
			}
		}
		
		return listeComposante;
	}

	/**
	 * @return structure affectation principale
	 */
	public IStructure getStructurePrincipaleVacataire() {
		if (CollectionUtils.isNotEmpty(getListeCibleEnseignantsVacataire())) {
			return getListeCibleEnseignantsVacataire().get(0);
		}
		return null;
	}

	/**
	 * @param pecheSession : la session
	 * @return Les structures pour la répartition croisée en tenant compte de l'individu connecté
	 */
	public List<IStructure> getComposantesScolaritePourRepartCroisee(Session pecheSession) {
		List<IStructure> listeComposantesScolaritePourRepartCroisee = pecheSession.getComposantesScolarite();
		List<IStructure> listeCibleEnseignant= getListeCibleEnseignantsStatutaires();
		
		if (CollectionUtils.isNotEmpty(listeCibleEnseignant)) {
			// on enlève les structures de l'utilisateur connecté
			for (IStructure struct : listeCibleEnseignant)  {
				listeComposantesScolaritePourRepartCroisee.remove(struct);
			}
		}
		return listeComposantesScolaritePourRepartCroisee;
	}

	/**
	 * Est-ce que la structure passée en paramètre est contenue dans les cibles enseignant de l'utilisateur connecté ?
	 * 
	 * @param structure Une structure
	 * @return <code>true</code> si la structure est contenu dans celles de l'utilisateur connecté
	 */
	public boolean isComposanteContenueDansCibleEnseignant(IStructure structure) {
		if (CollectionUtils.isNotEmpty(getListeCibleEnseignants()) && (structure != null)) {
			return getListeCibleEnseignants().contains(structure);
		}
		return false;
	}
	
	/**
	 * Est-ce que l'enseignant passée en paramètre est contenue dans les cibles enseignant statutaire de l'utilisateur connecté ?
	 * 
	 * @param enseignant un enseignant
	 * @return <code>true</code> si la structure est contenu dans celles de l'utilisateur connecté
	 */
	public boolean isEnseignantContenueDansCibleEnseignantStatutaire(IEnseignant enseignant) {
		boolean isContenue = false;
		
		if ((enseignant != null) && (CollectionUtils.isNotEmpty(getListeCibleEnseignantsStatutaires()))) {
				isContenue = getListeCibleEnseignantsStatutaires().contains(enseignant.getStructure(
						getPecheParametres().getAnneeUniversitaireEnCours(), getPecheParametres().isFormatAnneeExerciceAnneeCivile()));
		}
		
		return isContenue;
	}
	
	/**
	 * Est-ce que la structure passée en paramètre est contenue dans les cibles enseignement vacataire de l'utilisateur connecté ?
	 * 
	 * @param structure une structure
	 * @return <code>true</code> si la structure est contenu dans celles de l'utilisateur connecté
	 */
	public boolean isStructureDansCibleEnseignementVacataire(IStructure structure) {
		boolean isContenue = false;
		
		if (hasListeCibleEnseignementsVacataire()) {
				isContenue = getListeCibleEnseignementVacataire().contains(structure);
		}
		
		return isContenue;
	}
	
	/**
	 * Est-ce que la structure passée en paramètre est contenue dans les cibles enseignement de l'utilisateur connecté ?
	 * 
	 * @param structure Une structure
	 * @return <code>true</code> si la structure est contenu dans celles de l'utilisateur connecté
	 */
	public boolean isComposanteContenueDansCibleEnseignement(IStructure structure) {
		if (CollectionUtils.isNotEmpty(getListeCibleEnseignement()) && (structure != null)) {
			return getListeCibleEnseignement().contains(structure);
		}
		return false;
	}
	
	private void initGestionDroits() {
		List<EODroit> listeDroits = DroitsFinder.sharedInstance().getListeDroitsUtilisateur(editingContext, getIndividu());

		if (isAdmin()) {
			List<IStructure> listeToutesStructures = getListeStructuresAdmin();
			setListeCibleEnseignantsStatutaires(listeToutesStructures);
			setListeCibleEnseignantsVacataire(listeToutesStructures);
			setListeCibleEnseignementStatutaires(listeToutesStructures);
			setListeCibleEnseignementVacataire(listeToutesStructures);	
			setListeStructAdditionnellesUEFlottantes(getListeStructAdditionnellesUEFlottantesAdmin());
		} else {
			for (EODroit unDroit : listeDroits) {
				alimenterListeCible(unDroit);
				alimenterAutorisationsOnFonctionsCache(unDroit);
			}
		}
	}
	
	private List<IStructure> getListeStructAdditionnellesUEFlottantesAdmin() {
		List<IStructure> liste = new ArrayList<IStructure>();
		NSArray<EORepartUefStructure> listeStructAddUEF = EORepartUefStructure.fetchAllEORepartUefStructures(editingContext);
		for (EORepartUefStructure repUEF : listeStructAddUEF) {				
			liste.add(repUEF.toStructure());
		}
		return liste;
	}
	
	
	private void alimenterListeCible(EODroit unDroit) {
		List<IStructure> listeStructuresEnseignement = getListeStructures(unDroit.toStructureEnseignement(), unDroit.isTemoinEnseignementEnfant());
		alimenterListeCiblesSelonPopulation(listeStructuresEnseignement, unDroit.toTypePopulation(), false);
		List<IStructure> listeStructuresEnseignant = getListeStructures(unDroit.toStructureEnseignant(), unDroit.isTemoinEnseignantEnfant());
		alimenterListeCiblesSelonPopulation(listeStructuresEnseignant, unDroit.toTypePopulation(), true);
		setListeStructAdditionnellesUEFlottantes(getListeStructsAdditionnellesUEFlottantes(unDroit.toStructureEnseignement(), unDroit.isTemoinEnseignementEnfant()));
	}

	private void alimenterListeCiblesSelonPopulation(List<IStructure> liste, EOTypePopulation typePopulation, boolean isCibleEnseignant) {
		if (isCibleEnseignant) {
			if (typePopulation.estTypePopulationTous()) {
				getListeCibleEnseignantsStatutaires().addAll(liste);
				getListeCibleEnseignantsVacataire().addAll(liste);
			} else if (typePopulation.estTypePopulationStatutaire()) {
				getListeCibleEnseignantsStatutaires().addAll(liste);
			} else if (typePopulation.estTypePopulationVacataire()) {
				getListeCibleEnseignantsVacataire().addAll(liste);
			}
		} else {
			if (typePopulation.estTypePopulationTous()) {
				getListeCibleEnseignementStatutaires().addAll(liste);
				getListeCibleEnseignementVacataire().addAll(liste);
			} else if (typePopulation.estTypePopulationStatutaire()) {
				getListeCibleEnseignementStatutaires().addAll(liste);
			} else if (typePopulation.estTypePopulationVacataire()) {
				getListeCibleEnseignementVacataire().addAll(liste);
			}
		}
	}
	
	private List<IStructure> getListeStructuresAdmin() {
		return getListeStructures(EOStructure.rechercherEtablissement(editingContext), true);
	}

	private boolean isAdmin() {
		return CollectionUtils.isNotEmpty(listeIdProfilAGrhum) && listeIdProfilAGrhum.contains(EOPecheParametre.getIdProfilAdministrateur(editingContext));
	}
	
	private void alimenterAutorisationsOnFonctionsCache(EODroit unDroit) {
		
		if (autorisationsOnFonctionCache == null) {
			autorisationsOnFonctionCache = new HashMap<String, String>();
			List<EOGdProfilDroitFonction> listeProfilDroitFonction = DroitsHelper.droitsFonctionForProfil(editingContext, Constante.APP_PECHE, Integer.valueOf(unDroit.toProfil().primaryKey()));
			for (EOGdProfilDroitFonction droitFonction : listeProfilDroitFonction) {
			    String key = droitFonction.toGdFonction().fonIdInterne();
			    autorisationsOnFonctionCache.put(key, droitFonction.toGdTypeDroitFonction().tdfStrId());			    
			}
		}
	}
	
	/**
	 * @param structurePere : la structure père
	 * @param isAjoutStructureEnfant : prise en compte des structures enfants (Oui/Non)
	 * @return La liste des structures autorisées ET définies dans ChoixComposante UEF 
	 */
	private List<IStructure> getListeStructsAdditionnellesUEFlottantes(IStructure structurePere, boolean isAjoutStructureEnfant) {
		List<IStructure> liste = new ArrayList<IStructure>();
		List<IStructure> listeaddUEF = getListeStructAdditionnellesUEFlottantesAdmin();

		if (structurePere != null) {
			if (listeaddUEF.contains(structurePere)) {
				liste.add(structurePere);
			}
			if (isAjoutStructureEnfant) {
				EOQualifier qual = null;
				NSArray<EOStructure> enfants = ((EOStructure) structurePere).toutesStructuresFils(qual);
				for (IStructure structUEF : listeaddUEF) {
					if (enfants.contains((EOStructure) structUEF)) {
						liste.add(structUEF);
					}
				}
			}
		}
		return liste;
	}

	/**
	 * @param structurePere : la structure père
	 * @param isAjoutStructureEnfant : prise en compte des structures enfants (Oui/Non)
	 * @return La liste des structures autorisés
	 */
	private List<IStructure> getListeStructures(IStructure structurePere, boolean isAjoutStructureEnfant) {
		List<IStructure> liste = new ArrayList<IStructure>();
		if (structurePere != null) {
			if (isAjoutStructureEnfant) {
				liste.addAll(getListeStructuresDepartements(structurePere));
				liste.addAll(getListeStructuresComposantesScolarite(structurePere));
			} else {
				liste.add(structurePere);
			}
		}
		return liste;
	}
	
	private List<IStructure> getListeStructuresDepartements(IStructure structurePere) {
		if (structurePere != null) {
			return getStructureRepository().getDepartementsEnseignements(structurePere);
		}
		
		return null;
	}
	
	private List<IStructure> getListeStructuresComposantesScolarite(IStructure structurePere) {
		if (structurePere != null) {
			return getStructureRepository().getComposantesScolarites(structurePere);
		}
	
		return null;
	}
	
	/**
	 * @return la liste des structures des enseignants (structures ayant un type de groupe "DE" ou "CS")
	 */
	public List<IStructure> getListeCibleEnseignants() {
		if (CollectionUtils.isEmpty(listeCibleEnseignants)) {
			listeCibleEnseignants.addAll(getListeCibleEnseignantsStatutaires());
			listeCibleEnseignants.addAll(getListeCibleEnseignantsVacataire());
		}
		return listeCibleEnseignants;
	}

	/**
	 * @return la liste des structures des enseignants vacataires (structures ayant un type de groupe "DE" ou "CS")
	 */
	public List<IStructure> getListeCibleEnseignantsVacataire() {
		return listeCibleEnseignantsVacataire;
	}

	/**
	 * @return la liste des structures des enseignants statutaires (structures ayant un type de groupe "DE" ou "CS")
	 */
	public List<IStructure> getListeCibleEnseignantsStatutaires() {
		return listeCibleEnseignantsStatutaires;
	}
	
	/**
	 * @return la liste des structures d'enseignement (structures ayant un type de groupe "DE" ou "CS")
	 */
	public List<IStructure> getListeCibleEnseignement() {
		if (CollectionUtils.isEmpty(listeCibleEnseignement)) {
			listeCibleEnseignement.addAll(getListeCibleEnseignementStatutaires());
			listeCibleEnseignement.addAll(getListeCibleEnseignementVacataire());
			// dédoublonnage
			Set<IStructure> mySet = new HashSet<IStructure>(listeCibleEnseignement);
		    listeCibleEnseignement = new ArrayList<IStructure>(mySet);			 
		}
		return listeCibleEnseignement;
	}

	public List<IStructure> getListeCibleEnseignementStatutaires() {
		return listeCibleEnseignementStatutaires;
	}

	public List<IStructure> getListeCibleEnseignementVacataire() {
		return listeCibleEnseignementVacataire;
	}
	
	public List<IStructure> getListeDepartementsEnseignementVacataire() {
		if (CollectionUtils.isEmpty(listeDepartementsEnseignementVacataire)) {
			listeDepartementsEnseignementVacataire = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignementVacataire(), EOTypeGroupe.TGRP_CODE_DE);
		}
		return listeDepartementsEnseignementVacataire;
	}
	
	public List<IStructure> getListeComposantesEnseignementVacataire() {
		if (CollectionUtils.isEmpty(listeComposantesEnseignementVacataire)) {
			listeComposantesEnseignementVacataire = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignementVacataire(), EOTypeGroupe.TGRP_CODE_DE);
		}
		return listeComposantesEnseignementVacataire;
	}
	
	
	public List<IStructure> getListeDepartementsEnseignement() {
		if (CollectionUtils.isEmpty(listeDepartementsEnseignement)) {
			listeDepartementsEnseignement = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignement(), EOTypeGroupe.TGRP_CODE_DE);
		}
		return listeDepartementsEnseignement;
	}

	public List<IStructure> getListeComposantesScolariteEnseignement() {
		if (CollectionUtils.isEmpty(listeComposantesScolariteEnseignement)) {
			listeComposantesScolariteEnseignement = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignement(), EOTypeGroupe.TGRP_CODE_CS);
		}
		return listeComposantesScolariteEnseignement;
	}

	public List<IStructure> getListeDepartementsEnseignant() {
		if (CollectionUtils.isEmpty(listeDepartementsEnseignant)) {
			listeDepartementsEnseignant = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignants(), EOTypeGroupe.TGRP_CODE_DE);
		}
		return listeDepartementsEnseignant;
	}

	public List<IStructure> getListeComposantesScolariteEnseignant() {
		if (CollectionUtils.isEmpty(listeComposantesScolariteEnseignant)) {
			listeComposantesScolariteEnseignant = getStructureRepository().retournerStructuresDeTypeGroupe(getListeCibleEnseignants(), EOTypeGroupe.TGRP_CODE_CS);
		}
		return listeComposantesScolariteEnseignant;
	}

	public boolean hasListeCibleEnseignantsStatutaires() {
		return CollectionUtils.isNotEmpty(getListeCibleEnseignantsStatutaires());
	}
	
	public boolean hasListeCibleEnseignantsVacataire() {
		return CollectionUtils.isNotEmpty(getListeCibleEnseignantsVacataire());
	}
	
	public boolean hasListeCibleEnseignementsStatutaires() {
		return CollectionUtils.isNotEmpty(getListeCibleEnseignementStatutaires());
	}
	
	public boolean hasListeCibleEnseignementsVacataire() {
		return CollectionUtils.isNotEmpty(getListeCibleEnseignementVacataire());
	}
	
	public void setListeDepartementsEnseignement(List<IStructure> listeDepartementsEnseignement) {
		this.listeDepartementsEnseignement = listeDepartementsEnseignement;
	}

	public void setListeCibleEnseignants(List<IStructure> listeCibleEnseignants) {
		this.listeCibleEnseignants = listeCibleEnseignants;
	}

	public void setListeCibleEnseignement(List<IStructure> listeCibleEnseignement) {
		this.listeCibleEnseignement = listeCibleEnseignement;
	}

	public void setListeDepartementsEnseignant(List<IStructure> listeDepartementsEnseignant) {
		this.listeDepartementsEnseignant = listeDepartementsEnseignant;
	}

	public void setListeComposantesScolariteEnseignement(List<IStructure> listeComposantesScolariteEnseignement) {
		this.listeComposantesScolariteEnseignement = listeComposantesScolariteEnseignement;
	}

	public void setListeComposantesScolariteEnseignant(List<IStructure> listeComposantesScolariteEnseignant) {
		this.listeComposantesScolariteEnseignant = listeComposantesScolariteEnseignant;
	}

	public void setListeCibleEnseignantsStatutaires(
			List<IStructure> listeCibleEnseignantsStatutaires) {
		this.listeCibleEnseignantsStatutaires = listeCibleEnseignantsStatutaires;
	}

	public void setListeCibleEnseignantsVacataire(
			List<IStructure> listeCibleEnseignantsVacataire) {
		this.listeCibleEnseignantsVacataire = listeCibleEnseignantsVacataire;
	}

	public void setListeCibleEnseignementStatutaires(List<IStructure> listeCibleEnseignementStatutaires) {
		this.listeCibleEnseignementStatutaires = listeCibleEnseignementStatutaires;
	}

	public void setListeCibleEnseignementVacataire(List<IStructure> listeCibleEnseignementVacataire) {
		this.listeCibleEnseignementVacataire = listeCibleEnseignementVacataire;
	}
	
	/**
	 * @return L'individu connecté
	 */
	public EOIndividu getIndividu() {
		if (individu == null) {
			individu = EOIndividu.individuWithPersId(this.editingContext, this.persId);
		}
		return individu;
	}

	@Override
	protected NSArray<EOGdProfilDroitFonction> rechercherDroitsFonctionToCache(
			EOEditingContext ec, String appStrId, Integer persId) {
		
		NSArray<EOGdProfil> profils = DroitsHelper.profilsForPersonne(ec, persId);
		
		NSArray<EOGdProfilDroitFonction> droitsFonction = new NSMutableArray<EOGdProfilDroitFonction>();
		
		List<Integer> listeProfilExclus = (List<Integer>) EOPecheParametre.getListeIdProfilExclus(ec);
		
		listeIdProfilAGrhum = new ArrayList<Integer>();
		
		for (EOGdProfil eoGdProfil : profils) {
			if (listeProfilExclus.contains(Integer.valueOf(eoGdProfil.primaryKey()))) {
				listeIdProfilAGrhum.add(Integer.valueOf(eoGdProfil.primaryKey()));
				droitsFonction.addAll(DroitsHelper.droitsFonctionForProfil(ec, Constante.APP_PECHE, Integer.valueOf(eoGdProfil.primaryKey())));
			}
		}
		return droitsFonction;
	}
	
	private IPecheParametres getPecheParametres() {
		if (!pecheParametresDejaRecherche) {
			pecheParametres = new PecheParametres(this.editingContext);
			setPecheParametresDejaRecherche(true);
		}
		
		return pecheParametres;
	}

	public boolean isPecheParametresDejaRecherche() {
		return pecheParametresDejaRecherche;
	}

	public void setPecheParametresDejaRecherche(boolean pecheParametresDejaRecherche) {
		this.pecheParametresDejaRecherche = pecheParametresDejaRecherche;
	}

	public List<IStructure> getListeStructAdditionnellesUEFlottantes() {
		return listeStructAdditionnellesUEFlottantes;
	}

	public void setListeStructAdditionnellesUEFlottantes(
			List<IStructure> listeStructAdditionnellesUEFlottantes) {
		this.listeStructAdditionnellesUEFlottantes = listeStructAdditionnellesUEFlottantes;
	}
}
