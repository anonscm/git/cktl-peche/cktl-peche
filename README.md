Packaging avec Gradle
=====================

Avoir un "working directory clean"
----------------------------------

Message "nothing to commit (working directory clean)" de `git status` sur les projets `frameworks` et `Peche`.

  - Pas de commit non push
  - Pas de fichier staged
  - Pas de fichier tracked ou untracked

Pour le projet cktlframeworks, se positionner sur la version à embarquer dans le livrable (`git checkout x.x.x`)

Modifier le fichier "gradle.properties" de Peche
------------------------------------------------

  - Indiquer la version à fabriquer (`version = 'x.x.x.x'`)
  - Indiquer la version du framework à utiliser (à embarquer dans les livrables) en décommentant et modifiant la ligne "`fwkVersion = '1.0+'`"

Modifier le source "VersionMe"
------------------------------

  - Indiquer la version fabriquée
  - Indiquer la date de fabrication

Fabriquer
---------

Exécuter gradle avec `gradle clean build packagewoapplication`.

Deux livrables ont été fabriqué dans le dossier `build`.

  - Un pour le woa (`aaaammjj_WOA_PECHE_Vx.x.x.x.tar.gz`)
  - Un pour les ressources web (`aaaammjj_WEB_PECHE_Vx.x.x.x.tar.gz`)

Le livrable pour les scripts SQL est à fabriquer manuellement.

Tester le livrable
------------------

	cd build/Peche.woa
	./Peche -DdbConnectURLGLOBAL=jdbc:oracle:thin:@192.168.62.2:1521:pgiref02 -DdbConnectUserGLOBAL=grhum -DdbConnectPasswordGLOBAL.encrypted=true -DdbConnectPasswordGLOBAL=f608cab3a0e9e3b407071e97cae5b0f7

	export NEXT_ROOT=/opt (au besoin, si message "environment variable NEXT_ROOT not set!")

Tagguer la version
------------------

	git tag -a x.x.x.x -m "...."
	git push --tags

Fabriquer les SQL
-----------------

Dans le projet `cocktaildb`

Modifier le commentaire de DB_VERSION du fichier `ScriptAggregation.template` de peche.

Fabriquer les scripts avec `./createAggregateSql.sh peche <version>` avec version = `0.6.0.0` par exemple.

Livraison officielle
--------------------
Avant livraison officielle :
 - Vérifier qu'il n'y a pas de script SCO_SCOLARITE à livrer pour la version du framework qui sera embarqué dans Peche


-----

**Ajustement n°1 : virer les références au répertoire Resources de tes PatternSets

Ligne Resources/**/* dans resources.include.patternset

Ajustement n°2 : vérifier que ton code ne contient pas de pageWithName(<uneStringAuLieuDunComposant.getClass>)

Ajustement n°3 : pour être sur de pas voir de problème, rajouter "compileJava.options.encoding = 'UTF-8'" dans le build.gradle
