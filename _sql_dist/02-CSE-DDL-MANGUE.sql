--
-- Patch DDL de MANGUE du DD/MM/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2
-- Type : DML
-- Schema : MANGUE
-- Numero de version : 1.7.X.X
-- Date de publication : DD/MM/2012
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
-- Pré-requis : user CSE existant
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
-- pré-requis
/*
declare
cpt integer;
version grhum.db_version.dbv_libelle%type;
begin
    select count(*) into cpt from grhum.db_version where dbv_libelle='1.7.X.X';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRHUM n''est pas à jour pour passer ce patch !');
    end if;
    select max(dbv_libelle) into version from grhum.db_version;
    if version = '1.7.X.X+1' then
        raise_application_error(-20000,'Le nouveau patch du user MANGUE est déjà appliqué.');
    end if;    
end;
/
*/




--
-- DECHARGE
--
/*
ALTER TABLE MANGUE.DECHARGE ADD VISA_DRH VARCHAR2(1) DEFAULT 'N' NOT NULL;
ALTER TABLE MANGUE.DECHARGE ADD CONSTRAINT CHK_D_VISA_DRH CHECK (VISA_DRH IN ('O', 'N') );
COMMENT ON COLUMN MANGUE.DECHARGE.VISA_DRH IS 'Témoin précisant si la décharge d''un individu a été validée par la DRH';
*/


--
-- AFFECTATION
--
ALTER TABLE MANGUE.AFFECTATION ADD ID_TYPE_MOTIF NUMBER(2);
ALTER TABLE MANGUE.AFFECTATION ADD CONSTRAINT FK_CV_TYPE_MOTIF FOREIGN KEY (ID_TYPE_MOTIF) REFERENCES GRHUM.TYPE_MOTIF (ID_TYPE_MOTIF);
COMMENT ON COLUMN MANGUE.AFFECTATION.ID_TYPE_MOTIF IS 'référence au motif de recours aux vacataires (cf. GRHUM.TYPE_MOTIF.ID_TYPE_MOTIF)';

CREATE INDEX IDX_TM_ID ON MANGUE.AFFECTATION(ID_TYPE_MOTIF);

COMMIT;
