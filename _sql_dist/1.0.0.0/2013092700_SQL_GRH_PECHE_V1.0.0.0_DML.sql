--
-- Patch DML de PECHE du 27/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.0.0.0
-- Date de publication : 27/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


------------------------V20130927.104149__DML_Modification_Droit_Agrhum.sql-------------------------
--
-- Modification libellé/emplacement des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENSEIGNANTS';
    	    IF (compteur = 0)
  	  THEN
      	 	RAISE_APPLICATION_ERROR(-20000,'La fonction ''ENSEIGNANTS'' n''existe pas');		
 	  ELSE 
		UPDATE GRHUM.GD_FONCTION
         	   SET GRHUM.GD_FONCTION.FON_LC = 'Enseignants - population (hors ens. génériques)', 
                       GRHUM.GD_FONCTION.FON_DESCRIPTION = 'Enseignants - population (hors ens. génériques)'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENSEIGNANTS';
  	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';
    	    IF (compteur = 0)
  	  THEN
       		RAISE_APPLICATION_ERROR(-20000,'La fonction ''PARAM_ENS_GENERIQUES'' n''existe pas');		
  	  ELSE 
		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_CATEGORIE = 'Enseignants'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';

		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';
  	END IF;

END;
/


----------------------V20130927.120655__DML_Modification_libelle_droit_UE.sql-----------------------
--
-- Modification libellé des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'UE';
    	    IF (compteur = 0)
  	  THEN
      	 	RAISE_APPLICATION_ERROR(-20000,'La fonction ''UE'' n''existe pas');		
 	  ELSE 
		UPDATE GRHUM.GD_FONCTION
         	   SET GRHUM.GD_FONCTION.FON_LC = 'Gestion des UE flottantes', 
                       GRHUM.GD_FONCTION.FON_DESCRIPTION = 'Gestion des UE flottantes'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'UE';
  	END IF;
END;
/


------------------V20130927.170834__DML_Correction_erreur_enseignant_generique.sql------------------
--
-- Modification clé enseignants génériques des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES';
    	    IF (compteur = 0)
  	  THEN
       		RAISE_APPLICATION_ERROR(-20000,'La fonction ''ENS_GENERIQUES'' n''existe pas');		
  	  ELSE 
		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES';
  	END IF;

END;
/


-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.0.0.0', TO_DATE('27/09/2013', 'DD/MM/YYYY'), SYSDATE, 'Installation initiale de GRH_PECHE (migration depuis les bêtas)');

COMMIT;
