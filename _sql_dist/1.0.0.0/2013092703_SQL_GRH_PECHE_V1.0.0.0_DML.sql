--
-- Patch DML de PECHE du 27/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.0.0.0
-- Date de publication : 27/09/2013
-- Auteur(s) : Equipe PECHE

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--------------------------V20130718.110228__DML_Ajout_role_repartiteur.sql--------------------------
--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8 (Idem 2013042301_SQL_GRH_PECHE_V0.2.0.0_DML.sql)

--
--
-- Fichier : 1/7
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

DECLARE
    compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.ASSOCIATION WHERE ASS_CODE = 'REPARTITEUR';
	IF (compteur = 0) THEN
		--
		-- ASSOCIATION
		--
		INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, TAS_ID, ASS_CODE, D_OUVERTURE)
		     VALUES (ASSOCIATION_SEQ.NEXTVAL, 'RÉPARTITEUR', 1, 'REPARTITEUR', TO_DATE('21/06/2012', 'DD/MM/YYYY'));
        
		--
		-- ASSOCIATION_RESEAU
		--
		INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, ASR_RANG, ASR_COMMENTAIRE, D_CREATION, D_MODIFICATION)
		     SELECT ASSOCIATION_RESEAU_SEQ.NEXTVAL, A1.ASS_ID, A2.ASS_ID, NULL, NULL, SYSDATE, SYSDATE
		       FROM ASSOCIATION A1, ASSOCIATION A2
		      WHERE A1.ASS_CODE = 'FONCTION ADM' AND A2.ASS_CODE = 'REPARTITEUR';
	END IF;

	COMMIT;
END;
/

----------------------------V20130718.110235__DML_Ajout_droits_peche.sql----------------------------
--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8 (Idem 2013042302_SQL_GRH_PECHE_V0.2.0.0_DML.sql)

--
--
-- Fichier : 2/7
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

CREATE OR REPLACE PROCEDURE insertFunction(appId IN INTEGER, categorie in VARCHAR2, idInterne IN VARCHAR2, libelle IN VARCHAR2, description IN VARCHAR2)
IS
  compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_FONCTION WHERE APP_ID = appId AND FON_ID_INTERNE = idInterne;
	IF (compteur = 0) THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, appId, categorie, idInterne, libelle, description);
	END IF;
END;
/

DECLARE
    compteur NUMBER;
    idDomaine NUMBER;
    idApplication NUMBER;
BEGIN
	-- GD_DOMAINE
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_DOMAINE_SEQ.NEXTVAL INTO idDomaine FROM DUAL;
		INSERT INTO GRHUM.GD_DOMAINE (DOM_ID, DOM_LC) VALUES (idDomaine, 'RH');
	ELSE
		SELECT DOM_ID INTO idDomaine FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	END IF;
	
	-- GD_APPLICATION
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_APPLICATION_SEQ.NEXTVAL INTO idApplication FROM DUAL;
		INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) VALUES (idApplication, idDomaine, 'PECHE', 'PECHE');
	ELSE
		SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	END IF;

	-- GD_FONCTION
	-- -- Catégorie 'Accès Peche'
    --    -- Fonction 'Accès à l'application Peche'
    insertFunction(idApplication, 'Accès Peche', 'PECHE', 'Accès à l''application Peche', 'Accès à l''application PECHE');

    -- -- Catégorie 'Enseignants'
    --    -- Fonction 'Gestion des enseignants'
    --    -- Fonction 'Répartition croisée'
    insertFunction(idApplication, 'Enseignants', 'ENSEIGNANTS', 'Enseignants', 'Enseignants');
    insertFunction(idApplication, 'Enseignants', 'REPARTITION_CROISEE', 'Répartition croisée', 'Répartition croisée');

    -- -- Catégorie 'Enseignements'
    --    -- Fonction 'Offre de formation'
    --    -- Fonction 'Offre de formation - Nombre de groupe'
    --    -- Fonction 'Gestion des UE'
    insertFunction(idApplication, 'Enseignements', 'OFFRE_FORMATION', 'Offre de formation', 'Offre de formation');
    insertFunction(idApplication, 'Enseignements', 'PARAM_NB_GROUPE', 'Offre de formation - Nombre de groupe', 'Offre de formation - Paramétrer le nombre de groupe');
    insertFunction(idApplication, 'Enseignements', 'UE', 'Gestion des UE', 'Gestion des UE');
	
    -- -- Catégorie 'Fiches et validation'
    --    -- Fonction 'Gérer les fiches'
    --    -- Fonction 'Valider une fiche en tant que DRH'
    --    -- Fonction 'Valider une fiche en tant que président'
    --    -- Fonction 'Valider une fiche en tant qu'enseignant'
    --    -- Fonction 'Valider une fiche en tant que directeur de composante'
    --    -- Fonction 'Valider une fiche en tant que répartiteur'
	insertFunction(idApplication, 'Fiches et validation', 'GERER_FICHE', 'Gérer les fiches', 'Gérer les fiches');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_DRH', 'Valider une fiche en tant que DRH', 'Valider une fiche en tant que DRH');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_PRESIDENT', 'Valider une fiche en tant que président', 'Valider une fiche en tant que président');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_ENSEIGNANT', 'Valider une fiche en tant qu''enseignant', 'Valider une fiche en tant qu''enseignant');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_DIRECTEUR', 'Valider une fiche en tant que directeur de comp.', 'Valider une fiche en tant que directeur de composante');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_REPARTITEUR', 'Valider une fiche en tant que répartiteur', 'Valider une fiche en tant que répartiteur');

    -- -- Catégorie 'Paramétrages'
    --    -- Fonction 'Paramétrages - Etablissement'
    --    -- Fonction 'Paramétrages - Enseignants génériques'
    --    -- Fonction 'Paramétrages - Définition des circuits de validation'
    --    -- Fonction 'HETD - Liste des types d'horaire'
    --    -- Fonction 'HETD - Liste des paramètres des populations'
    --    -- Fonction 'REH - Liste des activités'
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ETABLISSEMENT', 'Etablissement', 'Paramétrer l''établissement');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ENS_GENERIQUES', 'Enseignants génériques', 'Gérer les enseignants génériques');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_CIRCUITS_VAL', 'Circuits de validation', 'Paramétrer les circuits de validation');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_TYPES_HORAIRES', 'HETD - Types d''horaires', 'HETD - Types d''horaires');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_P_POPULATIONS', 'HETD - Paramètres des populations', 'HETD - Paramètres des populations');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ACTIVITES', 'REH - Activités', 'REH - Paramétrer les activités');

	COMMIT;
END;
/

DROP PROCEDURE insertFunction;

-------------------------V20130718.110256__DML_Init_circuits_par_defaut.sql-------------------------
--
-- Patch DML de GRH_PECHE du 23/04/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8 (Idem 2013042306_SQL_GRH_PECHE_V0.2.0.0_DML.sql)

--
--
-- Fichier : 6/7
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

--Passez ces drop avant de faire les insertions pour repartir de 1
DROP SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ;
DROP SEQUENCE GRH_PECHE.ETAPE_SEQ;
DROP SEQUENCE GRH_PECHE.CHEMIN_SEQ;
CREATE SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.ETAPE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.CHEMIN_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

DECLARE
	persid_createur integer;
	flag integer;
	id_app number(12,0);
	id_circuit number;
	id_etape_depart number;
	id_etape_arrivee number;
	id_chemin number;
BEGIN
	--
	-- DB_VERSION
	--
	INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.2.0.0', TO_DATE('23/04/2013', 'DD/MM/YYYY'),NULL,'Version Beta 2 du user GRH_PECHE');
	
 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	-- Application
	-- ***********
	SELECT APP_ID INTO id_app FROM GRHUM.GD_APPLICATION WHERE APP_STR_ID = 'PECHE';
	
	-- Circuit 'PECHE_PREV_STATUTAIRE'
	-- ****************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_STATUTAIRE', 1, 'Circuit prévisionnel pour les statutaires : REPARTITEUR, DIRECTEUR COMPOSANTE, ENSEIGNANT, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
  	

	-- Circuit 'PECHE_PREV_VACATAIRE'
	-- ****************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_VACATAIRE', 1, 'Circuit prévisionnel pour les vacataires : REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
   	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');

	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');



	-- Circuit 'PECHE STATUTAIRE'
	-- *************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_STATUTAIRE', 1, 'Circuit pour les statutaires : REPARTITEUR, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
  
  -- Etape non branchée (optionnelle)
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_depart, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'REFUSER', 'Refuser');
    
	-- Circuit 'PECHE_VACATAIRE'
	-- *************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VACATAIRE', 1, 'Circuit où la DRH est obligatoire : REPARTITEUR, DRH, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	-- Circuit 'PECHE_VOEUX'
	-- *********************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VOEUX', 1, 'Circuit de la fiche de vœux : ENSEIGNANT, REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
    
	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	--Validation
	commit;
END;
/

-- METHODES_CALCUL_HCOMP

DECLARE
	persid_createur integer;
	flag integer;
	now timestamp;
BEGIN
	 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	select SYSDATE into now from dual;
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(1, 'Proratisation', 'org.cocktail.peche.metier.hce.MethodeDeCalculParProratisation', now, persid_createur, now, persid_createur);
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(2, 'Priorité CM/TP/TD', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisation', now, persid_createur, now, persid_createur);
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(3, 'Priorité TP/CM/TD', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisationTP', now, persid_createur, now, persid_createur);

	--
	-- DB_VERSION
	--
	UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = now WHERE DBV_LIBELLE = '0.2.0.0';
	
	--Validation
	commit;
END;
/

---------------------------V20130718.110349__DML_Ajout_nouveau_droit.sql----------------------------
--
-- Patch DML de GRH_PECHE du 06/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8 (Idem 2013060601_SQL_GRH_PECHE_V0.3.0.0_DML.sql)

--
--
-- Fichier : 1/3
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.3.0.0
-- Date de publication : 06/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

-- Nouveau droit
INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (SELECT APP_ID FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE') , 'Enseignants', 'PARAM_ABSENCE', 'Enseignants - Saisie des heures d''absence', 'Enseignants - Paramétrer le nombre d''heures des absences');

--Validation
commit;

---------------------------V20130718.110352__DML_Ajout_methode_calcul.sql---------------------------
--
-- Patch DDL de GRH_PECHE du 06/06/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8 (Idem 2013060603_SQL_GRH_PECHE_V0.3.0.0_DML.sql)

--
--
-- Fichier : 3/3
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.3.0.0
-- Date de publication : 06/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

--
-- DB_VERSION
--
INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.3.0.0', TO_DATE('06/06/2013', 'DD/MM/YYYY'),NULL,'Version Beta 3 du user GRH_PECHE');

-- HComp
update GRH_PECHE.METHODES_CALCUL_HCOMP set CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorite' where CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisation';
delete from GRH_PECHE.METHODES_CALCUL_HCOMP where CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisationTP';

DECLARE
	persid_createur integer;
	flag integer;
BEGIN
	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	UPDATE GRH_PECHE.METHODES_CALCUL_HCOMP
	   SET LL_METHODE = 'Priorité CM/TD/TP',
	       CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteCmTdTp',
	       D_MODIFICATION = SYSDATE,
	       PERS_ID_MODIFICATION = persid_createur
	   WHERE METHODE_ID = 2;
	
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP
	           (METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION)
	    VALUES (3, 'Priorité TP/TD/CM', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteTpTdCm', SYSDATE, persid_createur, SYSDATE, persid_createur);
END;
/

-- Année 2013
UPDATE GRH_PECHE.SERVICE SET ANNEE = 2013 WHERE ANNEE = 2012;

--
-- DB_VERSION
--
UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE = '0.3.0.0';

--Validation
commit;

---------------------------V20130718.110400__DML_Ajout_nouveau_droit.sql----------------------------
--
-- Patch DML de GRH_PECHE du 17/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8 (Idem 2013061701_SQL_GRH_PECHE_V0.4.0.0_DML.sql)

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.4.0.0
-- Date de publication : 17/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

CREATE OR REPLACE PROCEDURE insertFunction(appId IN INTEGER, categorie in VARCHAR2, idInterne IN VARCHAR2, libelle IN VARCHAR2, description IN VARCHAR2)
IS
  compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_FONCTION WHERE APP_ID = appId AND FON_ID_INTERNE = idInterne;
	IF (compteur = 0) THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, appId, categorie, idInterne, libelle, description);
	END IF;
END;
/

DECLARE
    idApplication NUMBER;
BEGIN
	
	SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';

	-- GD_FONCTION
    -- -- Catégorie 'Enseignements'
    --    -- Fonction 'Offre de formation - Nombre d'heures'
    insertFunction(idApplication, 'Enseignements', 'PARAM_NB_HEURES', 'Offre de formation - Nombre d''heures', 'Offre de formation - Surcharger le nombre d''heures maquette');

	COMMIT;
END;
/

DROP PROCEDURE insertFunction;

-------------------------V20130718.110434__DML_Renommage_etapes_circuit.sql-------------------------
--
-- Patch DDL de GRH_PECHE du 17/06/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8 (Idem 2013061702_SQL_GRH_PECHE_V0.4.0.0_DML.sql)

--
--
-- Fichier : 2/2
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.4.0.0
-- Date de publication : 17/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

--
-- DB_VERSION
--
INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.4.0.0', TO_DATE('17/06/2013', 'DD/MM/YYYY'),NULL,'Version Beta 4 du user GRH_PECHE');

--
-- Libellé des étapes (raccourcir)
--
UPDATE GRH_PECHE.ETAPE
   SET LL_ETAPE = 'Validation par le dir. comp.'
 WHERE C_ETAPE = 'VALID_DIR_COMPOSANTE';

UPDATE GRH_PECHE.ETAPE
   SET LL_ETAPE = 'Validation par la DRH'
 WHERE C_ETAPE = 'VALID_DRH';

--
-- DB_VERSION
--
UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE = '0.4.0.0';

--Validation
commit;

----------V20130828.141408__DML_Ajout_methode_calcul_hcomp_proratisation_presentielle.sql-----------
-- -----------------------------------------------------------------
-- Ajout de la méthode de calcul par proratisation sur le présentiel
-- -----------------------------------------------------------------

DECLARE
	persid_createur integer;
	flag integer;
BEGIN
	-- Récupération d'un créateur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de récuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP (
		METHODE_ID, LL_METHODE,
		CLASSE_METHODE,
		D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
	VALUES (
		4, 'Proratisation sur présentiel',
		'org.cocktail.peche.metier.hce.MethodeDeCalculParProratisationSurPresentiel',
		SYSDATE, SYSDATE, persid_createur, persid_createur
		);
END;
/
------------------------V20130927.104149__DML_Modification_Droit_Agrhum.sql-------------------------
--
-- Modification libellé/emplacement des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENSEIGNANTS';
    	    IF (compteur = 0)
  	  THEN
      	 	RAISE_APPLICATION_ERROR(-20000,'La fonction ''ENSEIGNANTS'' n''existe pas');		
 	  ELSE 
		UPDATE GRHUM.GD_FONCTION
         	   SET GRHUM.GD_FONCTION.FON_LC = 'Enseignants - population (hors ens. génériques)', 
                       GRHUM.GD_FONCTION.FON_DESCRIPTION = 'Enseignants - population (hors ens. génériques)'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENSEIGNANTS';
  	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';
    	    IF (compteur = 0)
  	  THEN
       		RAISE_APPLICATION_ERROR(-20000,'La fonction ''PARAM_ENS_GENERIQUES'' n''existe pas');		
  	  ELSE 
		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_CATEGORIE = 'Enseignants'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';

		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';
  	END IF;

END;
/


----------------------V20130927.120655__DML_Modification_libelle_droit_UE.sql-----------------------
--
-- Modification libellé des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'UE';
    	    IF (compteur = 0)
  	  THEN
      	 	RAISE_APPLICATION_ERROR(-20000,'La fonction ''UE'' n''existe pas');		
 	  ELSE 
		UPDATE GRHUM.GD_FONCTION
         	   SET GRHUM.GD_FONCTION.FON_LC = 'Gestion des UE flottantes', 
                       GRHUM.GD_FONCTION.FON_DESCRIPTION = 'Gestion des UE flottantes'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'UE';
  	END IF;
END;
/


------------------V20130927.170834__DML_Correction_erreur_enseignant_generique.sql------------------
--
-- Modification clé enseignants génériques des droits dans Agrhum
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES';
    	    IF (compteur = 0)
  	  THEN
       		RAISE_APPLICATION_ERROR(-20000,'La fonction ''ENS_GENERIQUES'' n''existe pas');		
  	  ELSE 
		UPDATE GRHUM.GD_FONCTION
           	   SET GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES'
         	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'ENS_GENERIQUES';
  	END IF;

END;
/


-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.0.0.0', TO_DATE('27/09/2013', 'DD/MM/YYYY'), SYSDATE, 'Initialisation de GRH_PECHE');

COMMIT;
