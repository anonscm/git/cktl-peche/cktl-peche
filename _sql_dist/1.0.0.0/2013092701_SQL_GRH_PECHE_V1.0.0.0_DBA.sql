--
-- Patch DBA de PECHE du 27/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DBA
-- Schema : GRHUM
-- Numero de version : 1.0.0.0
-- Date de publication : 27/09/2013
-- Auteur(s) : Equipe PECHE

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-----------------------------V20130718.110237__DBA_Creation_schema.sql------------------------------
--
-- Patch GRANT de GRH_PECHE du 09/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8 (Idem 2013042303_SQL_GRH_PECHE_V0.2.0.0_DBA.sql)

--
--
-- Fichier : 3/7
-- Type : GRANT
-- Schema : GRHUM
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
-- Pré-requis : user GRH_PECHE inexistant
--

declare
	cpt integer;
begin

	-- Test d'existence du User GRH_PECHE
	select count(*) into cpt from dba_users where username = upper('GRH_PECHE');

	if cpt <> 0 then
		execute immediate 'DROP USER GRH_PECHE CASCADE';
	end if;
end;
/

create user GRH_PECHE identified by A_REMPLACER 
default tablespace GRH 
temporary tablespace TEMP 
quota unlimited on GRH; 

GRANT create session to GRH_PECHE; 
GRANT create sequence to GRH_PECHE; 
GRANT create table to GRH_PECHE; 
GRANT create synonym to GRH_PECHE; 
GRANT create view to GRH_PECHE; 
GRANT unlimited tablespace to GRH_PECHE; 
GRANT connect to GRH_PECHE; 

--
-- Grants pour GRHUM
--
GRANT select, references on GRHUM.PERSONNE to GRH_PECHE;
GRANT select, references on GRHUM.INDIVIDU_ULR to GRH_PECHE;
GRANT select, references on GRHUM.STRUCTURE_ULR to GRH_PECHE;
GRANT select, references on GRHUM.TYPE_VISA to GRH_PECHE;
GRANT select, references on GRHUM.TYPE_ABSENCE to GRH_PECHE;
GRANT select, references on GRHUM.TYPE_POPULATION to GRH_PECHE;
GRANT select, references on GRHUM.COMPTE to GRH_PECHE;
GRANT select, references on GRHUM.CORPS to GRH_PECHE;
GRANT select, references on GRHUM.GRADE to GRH_PECHE;
GRANT select, references on GRHUM.ASSOCIATION to GRH_PECHE;
GRANT select, references on GRHUM.REPART_ASSOCIATION to GRH_PECHE;
GRANT select, references on GRHUM.REPART_STRUCTURE to GRH_PECHE;
GRANT select, references on GRHUM.TYPE_DECHARGE_SERVICE to GRH_PECHE;
GRANT select, references on GRHUM.TYPE_CONTRAT_TRAVAIL to GRH_PECHE;
GRANT select, references on GRHUM.GRHUM_PARAMETRES to GRH_PECHE;
GRANT select, references on GRHUM.REPART_TYPE_GROUPE to GRH_PECHE;
GRANT select, references on GRHUM.VLANS to GRH_PECHE;
GRANT select, references on GRHUM.GD_PROFIL to GRH_PECHE;
GRANT select, references on GRHUM.GROUPE_DYNAMIQUE to GRH_PECHE;
GRANT select, references on GRHUM.GRP_DYNA_PERSONNE to GRH_PECHE;
GRANT select, references on GRHUM.GD_APPLICATION to GRH_PECHE;
GRANT select, references on GRHUM.GD_PROFIL_DROIT_FONCTION to GRH_PECHE;
GRANT select, references on GRHUM.GD_FONCTION to GRH_PECHE;
GRANT select, references on GRHUM.GD_TYPE_DROIT_FONCTION to GRH_PECHE;

--GRANT select, references on GRHUM.TYPE_UNITE_FINANCEMENT to GRH_PECHE;
--GRANT select, references on GRHUM.TYPE_FINANCEMENT to GRH_PECHE;



--
-- Grants pour MANGUE
--
GRANT select, references on MANGUE.AFFECTATION to GRH_PECHE;
GRANT select, references on MANGUE.CONTRAT to GRH_PECHE;
GRANT select, references on MANGUE.CARRIERE to GRH_PECHE;
GRANT select, references on MANGUE.ELEMENT_CARRIERE to GRH_PECHE;
GRANT select, references on MANGUE.CONTRAT_AVENANT to GRH_PECHE;
GRANT select, references on MANGUE.ABSENCES to GRH_PECHE;
GRANT select, references on MANGUE.VISA to GRH_PECHE;
GRANT select, references on MANGUE.CONTRAT_VACATAIRES to GRH_PECHE;
GRANT select, references on MANGUE.DECHARGE to GRH_PECHE;
GRANT select, references on MANGUE.VISA_CGMOD_POP to GRH_PECHE;
GRANT select, references on MANGUE.EMERITAT to GRH_PECHE;


--
-- Grants pour JEFY_ADMIN
--
GRANT select, references on JEFY_ADMIN.DOMAINE to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.EXERCICE to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.FONCTION to GRH_PECHE;

GRANT select, references on JEFY_ADMIN.PARAMETRE to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.TYPE_APPLICATION  to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.TYPE_ETAT to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_GESTION to GRH_PECHE;
GRANT select, references on JEFY_ADMIN.V_UTILISATEUR_INFO to GRH_PECHE;


--
-- Grants pour SCO_SCOLARITE
--
GRANT select, update, references on SCO_SCOLARITE.COMPOSANT to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.LIEN to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.AP to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.AE to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.DIPLOME to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.EC to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.PARCOURS to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.PERIODE to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.REGROUPEMENT to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.UE to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.STRUCTURES_COMPOSANT to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.TYPE_AP to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.VERSION_DIPLOME to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.TYPE_COMPOSANT to GRH_PECHE;
GRANT select, references on SCO_SCOLARITE.TYPE_LIEN to GRH_PECHE;
GRANT select, update, references on SCO_SCOLARITE.LIEN_COMPOSER to GRH_PECHE;


-------------------------------V20130905.172613__DBA_Ajout_Grant.sql--------------------------------
--
-- Grant pour MANGUE
--

GRANT select, references on MANGUE.changement_position to GRH_PECHE;




