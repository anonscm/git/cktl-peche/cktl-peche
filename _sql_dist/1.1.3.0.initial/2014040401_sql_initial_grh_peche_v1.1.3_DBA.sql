-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL

--
-- Patch DBA de GRH_PECHE du 04/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/3
-- Type : DBA
-- Schema : GRH_PECHE
-- Numero de version : 1.1.3
-- Date de publication : 04/04/2014
-- Auteur(s) : Association Cocktail
--
--
-- activation d'affichage
set serveroutput on feedback on

declare

dbn varchar2(255);
ibn varchar2(255);
ord_data varchar2(1024);
ord_indx varchar2(1024);
is_tbs int;
c int;

begin

-- Test d'existence du User GRH_PECHE
select count(*) into c from dba_users where username = upper('GRH_PECHE');
if c <> 0 then
execute immediate 'DROP USER GRH_PECHE CASCADE';
end if;

execute immediate 'create user GRH_PECHE identified by A_REMPLACER default tablespace GRH temporary tablespace TEMP quota unlimited on GRH'; 

execute immediate 'GRANT create session to GRH_PECHE'; 
execute immediate 'GRANT create sequence to GRH_PECHE'; 
execute immediate 'GRANT create table to GRH_PECHE'; 
execute immediate 'GRANT create synonym to GRH_PECHE'; 
execute immediate 'GRANT create view to GRH_PECHE'; 
execute immediate 'GRANT unlimited tablespace to GRH_PECHE'; 
execute immediate 'GRANT connect to GRH_PECHE'; 

--
-- Grants pour GRHUM
--
execute immediate 'GRANT select, references on GRHUM.PERSONNE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.INDIVIDU_ULR to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.STRUCTURE_ULR to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.TYPE_VISA to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.TYPE_ABSENCE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.TYPE_POPULATION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.COMPTE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.CORPS to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GRADE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.ASSOCIATION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.REPART_ASSOCIATION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.REPART_STRUCTURE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.TYPE_DECHARGE_SERVICE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.TYPE_CONTRAT_TRAVAIL to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GRHUM_PARAMETRES to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.REPART_TYPE_GROUPE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.VLANS to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GD_PROFIL to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GROUPE_DYNAMIQUE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GRP_DYNA_PERSONNE to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GD_APPLICATION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GD_PROFIL_DROIT_FONCTION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GD_FONCTION to GRH_PECHE';
execute immediate 'GRANT select, references on GRHUM.GD_TYPE_DROIT_FONCTION to GRH_PECHE';

--
-- Grants pour MANGUE
--
execute immediate 'GRANT select, references on MANGUE.AFFECTATION to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.CONTRAT to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.CARRIERE to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.ELEMENT_CARRIERE to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.CONTRAT_AVENANT to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.ABSENCES to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.VISA to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.CONTRAT_VACATAIRES to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.DECHARGE to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.VISA_CGMOD_POP to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.EMERITAT to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.changement_position to GRH_PECHE';
execute immediate 'GRANT select, references on MANGUE.VACATAIRES to GRH_PECHE';

--
-- Grants pour JEFY_ADMIN
--
execute immediate 'GRANT select, references on JEFY_ADMIN.DOMAINE to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.EXERCICE to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.FONCTION to GRH_PECHE';

execute immediate 'GRANT select, references on JEFY_ADMIN.PARAMETRE to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.TYPE_APPLICATION  to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.TYPE_ETAT to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.UTILISATEUR to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_GESTION to GRH_PECHE';
execute immediate 'GRANT select, references on JEFY_ADMIN.V_UTILISATEUR_INFO to GRH_PECHE';


--
-- Grants pour SCO_SCOLARITE
--
execute immediate 'GRANT select, update, references on SCO_SCOLARITE.COMPOSANT to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.LIEN to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.STRUCTURES_COMPOSANT to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.TYPE_AP to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.TYPE_COMPOSANT to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.TYPE_LIEN to GRH_PECHE';
execute immediate 'GRANT select, references on SCO_SCOLARITE.STRUCTURES_COMPOSANT to GRH_PECHE';

--
-- Grants pour WORKFLOW
--
execute immediate 'GRANT select,update,references on workflow.chemin to GRH_PECHE';
execute immediate 'GRANT select,update,references on workflow.circuit_validation to GRH_PECHE';
execute immediate 'GRANT select,update,references on workflow.demande to GRH_PECHE';
execute immediate 'GRANT select,update,references on workflow.etape to GRH_PECHE';
execute immediate 'GRANT select,update,references on workflow.historique_demande to GRH_PECHE';

COMMIT;

END;

/
