--
-- Patch DML de GRH_PECHE du 04/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 3/3
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 1.1.3
-- Date de publication : 04/04/2014
-- Auteur(s) : Association Cocktail
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


CREATE OR REPLACE PROCEDURE insertFunction(appId IN INTEGER, categorie in VARCHAR2, idInterne IN VARCHAR2, libelle IN VARCHAR2, description IN VARCHAR2)
IS
  compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_FONCTION WHERE APP_ID = appId AND FON_ID_INTERNE = idInterne;
	IF (compteur = 0) THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, appId, categorie, idInterne, libelle, description);
	END IF;
END;
/

declare
	persid_createur integer;
	flag integer;
	id_app number(12,0);
	id_circuit number;
	id_etape_depart number;
	id_etape_arrivee number;
	id_chemin number;
	compteur integer;
 	idDomaine NUMBER;    	
begin

	-- GD_DOMAINE
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_DOMAINE_SEQ.NEXTVAL INTO idDomaine FROM DUAL;
		INSERT INTO GRHUM.GD_DOMAINE (DOM_ID, DOM_LC) VALUES (idDomaine, 'RH');
	ELSE
		SELECT DOM_ID INTO idDomaine FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	END IF;
	
	-- GD_APPLICATION
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_APPLICATION_SEQ.NEXTVAL INTO id_app FROM DUAL;
		INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) VALUES (id_app, idDomaine, 'PECHE', 'PECHE');
	ELSE
		SELECT APP_ID INTO id_app FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	END IF;

 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
    insertFunction(id_app, 'Accès Peche', 'PECHE', 'Accès à l''application Peche', 'Accès à l''application PECHE');
    insertFunction(id_app, 'Enseignants', 'ENSEIGNANTS', 'Enseignants - population (hors ens. génériques)', 'Enseignants - population (hors ens. génériques)');
    insertFunction(id_app, 'Enseignants', 'REPARTITION_CROISEE', 'Répartition croisée', 'Répartition croisée');
	insertFunction(id_app, 'Enseignements', 'OFFRE_FORMATION', 'Offre de formation', 'Offre de formation');
    insertFunction(id_app, 'Enseignements', 'PARAM_NB_GROUPE', 'Offre de formation - Nombre de groupe', 'Offre de formation - Paramétrer le nombre de groupe');
    insertFunction(id_app, 'Enseignements', 'UE', 'Gestion des UE flottantes', 'Gestion des UE flottantes');
    insertFunction(id_app, 'Fiches et validation', 'GERER_FICHE', 'Gérer les fiches', 'Gérer les fiches');
    insertFunction(id_app, 'Fiches et validation', 'VALID_DRH', 'Valider une fiche en tant que DRH', 'Valider une fiche en tant que DRH');
    insertFunction(id_app, 'Fiches et validation', 'VALID_PRESIDENT', 'Valider une fiche en tant que président', 'Valider une fiche en tant que président');
    insertFunction(id_app, 'Fiches et validation', 'VALID_ENSEIGNANT', 'Valider une fiche en tant qu''enseignant', 'Valider une fiche en tant qu''enseignant');
    insertFunction(id_app, 'Fiches et validation', 'VALID_DIRECTEUR', 'Valider une fiche en tant que directeur de comp.', 'Valider une fiche en tant que directeur de composante');
    insertFunction(id_app, 'Fiches et validation', 'VALID_REPARTITEUR', 'Valider une fiche en tant que répartiteur', 'Valider une fiche en tant que répartiteur');
    insertFunction(id_app, 'Paramétrages', 'PARAM_ETABLISSEMENT', 'Etablissement', 'Paramétrer l''établissement');
    insertFunction(id_app, 'Paramétrages', 'PARAM_ENS_GENERIQUES', 'Enseignants génériques', 'Gérer les enseignants génériques');
    insertFunction(id_app, 'Paramétrages', 'PARAM_CIRCUITS_VAL', 'Circuits de validation', 'Paramétrer les circuits de validation');
    insertFunction(id_app, 'Paramétrages', 'PARAM_TYPES_HORAIRES', 'HETD - Types d''horaires', 'HETD - Types d''horaires');
    insertFunction(id_app, 'Paramétrages', 'PARAM_P_POPULATIONS', 'HETD - Paramètres des populations', 'HETD - Paramètres des populations');
    insertFunction(id_app, 'Paramétrages', 'PARAM_ACTIVITES', 'REH - Activités', 'REH - Paramétrer les activités');
    insertFunction(id_app, 'Enseignants', 'PARAM_ABSENCE', 'Enseignants - Saisie des heures d''absence', 'Enseignants - Paramétrer le nombre d''heures des absences');
   insertFunction(id_app, 'Enseignements', 'PARAM_NB_HEURES', 'Offre de formation - Nombre d''heures', 'Offre de formation - Surcharger le nombre d''heures maquette');

   insertFunction(id_app, 'Enseignants','REPARTITION_HCOMP','Ens. statutaires - Répartition des heures comp','Enseignants statutaires - Répartition des heures complémentaires');
   insertFunction(id_app, 'Enseignants','REPORT_HCOMP','Ens. statutaires - Report des heures comp','Enseignants statutaires - Report des heures complémentaires');
   insertFunction(id_app, 'Paramétrages','PARAM_TAUX_HORAIRES','Taux horaires','Gérer les taux horaires');
   insertFunction(id_app, 'Paramétrages','PARAM_FLUX_PAIEMENT','Flux de paiement','Paramétrer les flux de paiement');
   insertFunction(id_app, 'Mise en paiement','HEURES_A_PAYER','Saisie des heures à payer','Saisie des heures à payer');
   insertFunction(id_app, 'Mise en paiement','MISE_EN_PAIEMENT','Gestion des bordereaux de paiement','Gestion des bordereaux de paiement');


	SELECT COUNT(*) INTO compteur FROM GRHUM.ASSOCIATION WHERE ASS_CODE = 'REPARTITEUR';
	IF (compteur = 0) THEN
		--
		-- ASSOCIATION
		--
		INSERT INTO GRHUM.ASSOCIATION (ASS_ID, ASS_LIBELLE, TAS_ID, ASS_CODE, D_OUVERTURE)
		     VALUES (ASSOCIATION_SEQ.NEXTVAL, 'RÉPARTITEUR', 1, 'REPARTITEUR', TO_DATE('21/06/2012', 'DD/MM/YYYY'));
        
		--
		-- ASSOCIATION_RESEAU
		--
		INSERT INTO GRHUM.ASSOCIATION_RESEAU (ASR_ID, ASS_ID_PERE, ASS_ID_FILS, ASR_RANG, ASR_COMMENTAIRE, D_CREATION, D_MODIFICATION)
		     SELECT ASSOCIATION_RESEAU_SEQ.NEXTVAL, A1.ASS_ID, A2.ASS_ID, NULL, NULL, SYSDATE, SYSDATE
		       FROM ASSOCIATION A1, ASSOCIATION A2
		      WHERE A1.ASS_CODE = 'FONCTION ADM' AND A2.ASS_CODE = 'REPARTITEUR';
	END IF;



Insert into GRH_PECHE.METHODES_CALCUL_HCOMP (METHODE_ID,LL_METHODE,CLASSE_METHODE,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('1','Proratisation','org.cocktail.peche.metier.hce.MethodeDeCalculParProratisation',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.METHODES_CALCUL_HCOMP (METHODE_ID,LL_METHODE,CLASSE_METHODE,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('2','Priorité CM/TD/TP','org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteCmTdTp',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.METHODES_CALCUL_HCOMP (METHODE_ID,LL_METHODE,CLASSE_METHODE,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('3','Priorité TP/TD/CM','org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteTpTdCm',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.METHODES_CALCUL_HCOMP (METHODE_ID,LL_METHODE,CLASSE_METHODE,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('4','Proratisation sur présentiel','org.cocktail.peche.metier.hce.MethodeDeCalculParProratisationSurPresentiel',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);

Insert into GRH_PECHE.PECHE_PARAMETRES (PARAM_ID,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('1','hcomp.methode.defaut','4','Méthode de calcul des HCOMP à appliquer par défaut',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.PECHE_PARAMETRES (PARAM_ID,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('2','report.borne.inferieur','0','Nombre d''heures autorisé que l''enseignant peut faire en plus sur l''année N+1 en cas de sous-service',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.PECHE_PARAMETRES (PARAM_ID,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('3','report.borne.superieur','0','Nombre d''heures autorisé que l''enseignant peut faire en moins sur l''année N+1 en cas d''heures complémentaires',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.PECHE_PARAMETRES (PARAM_ID,PARAM_KEY,PARAM_VALUE,PARAM_COMMENTAIRES,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('4','periode.choix.type','M','Choix du type de la période pour la saisie des heures réalisées',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);

Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('1','Septembre','M','1','1','1','9','9');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('2','Octobre','M','2','2','1','10','10');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('3','Novembre','M','3','3','1','11','11');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('4','Décembre','M','4','4','1','12','12');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('5','Semestre 1','S','5','4','1','9','12');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('6','Janvier','M','6','5','2','1','1');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('7','Février','M','7','6','2','2','2');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('8','Mars','M','8','7','2','3','3');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('9','Avril','M','9','8','2','4','4');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('10','Mai','M','10','9','2','5','5');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('11','Juin','M','11','10','2','6','6');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('12','Juillet','M','12','11','2','7','7');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('13','Août','M','13','12','2','8','8');
Insert into GRH_PECHE.PERIODE (ID_PERIODE,LL_PERIODE,TYPE,ORDRE_AFFICHAGE,ORDRE_PERIODE,NO_SEMESTRE,NO_PREMIER_MOIS,NO_DERNIER_MOIS) values ('14','Semestre 2','S','14','12','2','1','8');

Insert into GRH_PECHE.TAUX_HORAIRE (ID_TAUX_HORAIRE,TAUX_BRUT,TAUX_CHARGE_FONCTIONNAIRE,TAUX_CHARGE_NON_FONCTIONNAIRE,D_DEBUT,D_FIN,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('1','36,91','42,96','58,09',to_date('01/01/13','DD/MM/RR'),to_date('15/08/13','DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.TAUX_HORAIRE (ID_TAUX_HORAIRE,TAUX_BRUT,TAUX_CHARGE_FONCTIONNAIRE,TAUX_CHARGE_NON_FONCTIONNAIRE,D_DEBUT,D_FIN,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('2','40,91','42,96','58,09',to_date('16/08/13','DD/MM/RR'),null,to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);

Insert into GRH_PECHE.TYPE_FLUX_PAIEMENT (ID_TYPE_FLUX,LL_TYPE_FLUX,CLASSE_TYPE_FLUX,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('1','Flux Girafe','org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);
Insert into GRH_PECHE.TYPE_FLUX_PAIEMENT (ID_TYPE_FLUX,LL_TYPE_FLUX,CLASSE_TYPE_FLUX,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('2','Flux WinPaie','org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementWinpaie',to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);

Insert into GRH_PECHE.PARAMETRES_FLUX_PAIEMENT (ID_PARAMETRES,ID_TYPE_FLUX,TEMPLATE_NOM_FLUX,ADM,DEPARTEMENT,GIRAFE_CODE_CHAINE_PAYE,GIRAFE_CORR,GIRAFE_MIN,GIRAFE_ORIG,GIRAFE_CODE_IR,GIRAFE_SENS,GIRAFE_CODE_MTT_PRE_CALCULE,GIRAFE_TPL_LIB_COMPLEMENTAIRE,WINPAIE_CODE_DEBUT_LIGNE_CAD,WINPAIE_CODE_CHAINE_PAYE,WINPAIE_TPL_NOM_PRENOM,WINPAIE_OBJET,WINPAIE_CODE_F,WINPAIE_CODE_FIN_LIGNE_CAD,D_DEBUT,D_FIN,D_CREATION,D_MODIFICATION,PERS_ID_CREATION,PERS_ID_MODIFICATION) values ('1','1','paiement_%1$d_%2$04d%3$02d','022','031','PP','DNI','933','1','0331','0','A','%1$1.2f heures à %2$1.2f','CAD','PP01793','%1$s, %2$s','CRS COMPL             ','F10','            01',to_date('01/09/13','DD/MM/RR'),null,to_date(SYSDATE,'DD/MM/RR'),to_date(SYSDATE,'DD/MM/RR'),persid_createur,persid_createur);


	
	-- Circuit 'PECHE_PREV_STATUTAIRE'
	-- ****************************
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM WORKFLOW.CIRCUIT_VALIDATION
 	 WHERE WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION = 'PECHE_PREV_STATUTAIRE';
        IF (compteur = 0)
  	  THEN
	SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into WORKFLOW.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_STATUTAIRE', 1, 'Circuit prévisionnel pour les statutaires : REPARTITEUR, DIRECTEUR COMPOSANTE, ENSEIGNANT, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');

	END IF;

	-- Circuit 'PECHE_PREV_VACATAIRE'
	-- ****************************
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM WORKFLOW.CIRCUIT_VALIDATION
 	 WHERE WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION = 'PECHE_PREV_VACATAIRE';
        IF (compteur = 0)
  	  THEN

	SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into WORKFLOW.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_VACATAIRE', 1, 'Circuit prévisionnel pour les vacataires : REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
   	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');

	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');

	END IF;

	-- Circuit 'PECHE STATUTAIRE'
	-- *************************
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM WORKFLOW.CIRCUIT_VALIDATION
 	 WHERE WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION = 'PECHE_STATUTAIRE';
        IF (compteur = 0)
  	  THEN

	SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into WORKFLOW.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_STATUTAIRE', 1, 'Circuit pour les statutaires : REPARTITEUR, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
  
  -- Etape non branchée (optionnelle)
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_depart, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'REFUSER', 'Refuser');
    
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM WORKFLOW.CIRCUIT_VALIDATION
 	 WHERE WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION = 'PECHE_VACATAIRE';
        IF (compteur = 0)
  	  THEN

	-- Circuit 'PECHE_VACATAIRE'
	-- *************************
	SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into WORKFLOW.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VACATAIRE', 1, 'Circuit où la DRH est obligatoire : REPARTITEUR, DRH, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	END IF;
	
	-- Circuit 'PECHE_VOEUX'
	-- *********************
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM WORKFLOW.CIRCUIT_VALIDATION
 	 WHERE WORKFLOW.CIRCUIT_VALIDATION.C_CIRCUIT_VALIDATION = 'PECHE_VOEUX';
        IF (compteur = 0)
  	  THEN

	SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into WORKFLOW.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VOEUX', 1, 'Circuit de la fiche de vœux : ENSEIGNANT, REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit, 'O');
	SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
    
	id_etape_depart := id_etape_arrivee;
    SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	END IF;	


	INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.1.3', TO_DATE('04/04/2014', 'DD/MM/YYYY'), SYSDATE, 'Version initiale PECHE 1.1.3.0');

COMMIT;

END;
/

DROP PROCEDURE insertFunction;
