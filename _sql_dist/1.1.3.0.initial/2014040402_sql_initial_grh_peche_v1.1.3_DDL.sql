--
-- Patch DDL de GRH_PECHE du 04/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/3
-- Type : DLL
-- Schema : GRH_PECHE
-- Numero de version : 1.1.3
-- Date de publication : 04/04/2014
-- Auteur(s) : Association Cocktail
--
--
WHENEVER SQLERROR EXIT SQL.SQLCODE;

--------------------------------------------------------
--  DDL for Sequence DB_VERSION_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."DB_VERSION_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence ENSEIGNANT_GENERIQUE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."ENSEIGNANT_GENERIQUE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence FICHE_VOEUX_DETAIL_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."FICHE_VOEUX_DETAIL_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence FICHE_VOEUX_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."FICHE_VOEUX_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence FLUX_MISE_EN_PAIEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence METHODES_CALCUL_HCOMP_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."METHODES_CALCUL_HCOMP_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence MISE_EN_PAIEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."MISE_EN_PAIEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PAIEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."PAIEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PARAMETRES_FLUX_PAIEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 2 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PARAM_POP_HETD_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."PARAM_POP_HETD_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PECHE_PARAMETRES_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."PECHE_PARAMETRES_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 5 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence PERIODE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."PERIODE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 15 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REH_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."REH_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REPART_ENSEIGNANT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."REPART_ENSEIGNANT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REPART_METHODE_CORPS_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."REPART_METHODE_CORPS_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence REPART_UEF_ETABLISSEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."REPART_UEF_ETABLISSEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SERVICE_DETAIL_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."SERVICE_DETAIL_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence SERVICE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."SERVICE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TAUX_HORAIRE_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."TAUX_HORAIRE_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 CACHE 20 NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Sequence TYPE_FLUX_PAIEMENT_SEQ
--------------------------------------------------------

   CREATE SEQUENCE  "GRH_PECHE"."TYPE_FLUX_PAIEMENT_SEQ"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 3 NOCACHE  NOORDER  NOCYCLE ;
--------------------------------------------------------
--  DDL for Table DB_VERSION
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."DB_VERSION" 
   (	"DBV_ID" NUMBER(4,0), 
	"DBV_LIBELLE" VARCHAR2(15 BYTE), 
	"DBV_DATE" DATE, 
	"DBV_INSTALL" DATE, 
	"DBV_COMMENT" VARCHAR2(2000 BYTE)
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."DB_VERSION"."DBV_ID" IS 'Identifiant de la version';
 
   COMMENT ON COLUMN "GRH_PECHE"."DB_VERSION"."DBV_LIBELLE" IS 'Libellé de la version';
 
   COMMENT ON COLUMN "GRH_PECHE"."DB_VERSION"."DBV_DATE" IS 'Date de release de la version';
 
   COMMENT ON COLUMN "GRH_PECHE"."DB_VERSION"."DBV_INSTALL" IS 'Date d''installation de la version. Si non renseigné, la version n''est pas complétement installée.';
 
   COMMENT ON COLUMN "GRH_PECHE"."DB_VERSION"."DBV_COMMENT" IS 'Le commentaire : une courte description de cette version de la base de données.';
 
   COMMENT ON TABLE "GRH_PECHE"."DB_VERSION"  IS 'Historique des versions du schema du user GRH_PECHE';
--------------------------------------------------------
--  DDL for Table ENSEIGNANT_GENERIQUE
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" 
   (	"ID_ENSEIGNANT_GENERIQUE" NUMBER, 
	"NOM" VARCHAR2(30 BYTE), 
	"PRENOM" VARCHAR2(30 BYTE), 
	"C_CORPS" VARCHAR2(4 BYTE), 
	"HEURES_PREVUES" NUMBER DEFAULT 0, 
	"TEM_VALIDE" VARCHAR2(1 BYTE) DEFAULT 'O', 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_COMMENTAIRE" DATE DEFAULT SYSDATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."ID_ENSEIGNANT_GENERIQUE" IS 'Clef primaire de la table ENSEIGNANT_GENERIQUE';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."NOM" IS 'Nom de l''intervenant si connu';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."PRENOM" IS 'Prénom de l''intervenant si connu';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."C_CORPS" IS 'Type de population concernée (cf. GRHUM.CORPS.C_CORPS)';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."HEURES_PREVUES" IS 'Heures du service prévu pour l''intervenant si c_corps non renseigné';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."TEM_VALIDE" IS 'Validité de l''enregistrement ? (O:valide, N:invalide) ';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."COMMENTAIRE" IS 'Commentaire sur l''intervenant';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."D_COMMENTAIRE" IS 'Date de création du commentaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."ENSEIGNANT_GENERIQUE"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE"  IS 'Table qui permet de recenser les intervenants pas encore renseignés dans MANGUE';
--------------------------------------------------------
--  DDL for Table FICHE_VOEUX
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."FICHE_VOEUX" 
   (	"ID_FICHE_VOEUX" NUMBER, 
	"ID_DEMANDE" NUMBER, 
	"NO_INDIVIDU" NUMBER(8,0), 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_COMMENTAIRE" DATE DEFAULT SYSDATE, 
	"TEM_VALIDE" VARCHAR2(1 BYTE) DEFAULT 'O', 
	"ANNEE" NUMBER(4,0), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."ID_FICHE_VOEUX" IS 'Clef primaire de la table FICHE_VOEUX';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."ID_DEMANDE" IS 'Référence à la demande sur le circuit de validation (cf. WORKFLOW.DEMANDE.ID_DEMANDE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."NO_INDIVIDU" IS 'Enseignant concerné (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."COMMENTAIRE" IS 'Commentaire sur la fiche de voeux';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."D_COMMENTAIRE" IS 'Date de création du commentaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."TEM_VALIDE" IS 'L''enregistrement est-il valide? (O:oui, N:non) ';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."ANNEE" IS 'Année universitaire considérée';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."FICHE_VOEUX"  IS 'Fiche de voeux d''un enseignant';
--------------------------------------------------------
--  DDL for Table FICHE_VOEUX_DETAIL
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" 
   (	"ID_FV_DETAIL" NUMBER, 
	"ID_FICHE_VOEUX" NUMBER, 
	"ID_AP" NUMBER, 
	"ETABLISSEMENT_EXTERNE" VARCHAR2(1024 BYTE), 
	"UE_ETABLISSEMENT_EXTERNE" VARCHAR2(128 BYTE), 
	"HEURES_SOUHAITEES" NUMBER DEFAULT 0, 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_COMMENTAIRE" DATE DEFAULT SYSDATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."ID_FV_DETAIL" IS 'Clef primaire de la table FICHE_VOEUX_DETAIL';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."ID_FICHE_VOEUX" IS 'Référence à la fiche de voeux de l''enseignant (cf. GRH_PECHE.FICHE_VOEUX.ID_FICHE_VOEUX)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."ID_AP" IS 'Référence à un AP présent dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.AP.ID)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."ETABLISSEMENT_EXTERNE" IS 'Nom de l''établissement si l''enseignement n''est pas dans celui d''origine';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."UE_ETABLISSEMENT_EXTERNE" IS 'UE renseigné uniquement dans le cadre d''un autre établissement si connue';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."HEURES_SOUHAITEES" IS 'Nombre d''heures souhaitées par l''enseignant';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."COMMENTAIRE" IS 'Commentaire sur le détail de la fiche de voeux';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."D_COMMENTAIRE" IS 'Date de création du commentaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."FICHE_VOEUX_DETAIL"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL"  IS 'Détail de la fiche de voeux d''un enseignant';
--------------------------------------------------------
--  DDL for Table FLUX_MISE_EN_PAIEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" 
   (	"ID_FLUX_MISE_EN_PAIEMENT" NUMBER, 
	"ID_PAIEMENT" NUMBER, 
	"ID_SERVICE" NUMBER, 
	"NO_INSEE" CHAR(13 BYTE), 
	"CLE_INSEE" NUMBER(2,0), 
	"NOM" VARCHAR2(80 BYTE), 
	"PRENOM" VARCHAR2(40 BYTE), 
	"NPC" CHAR(2 BYTE), 
	"HEURES_PAYEES" NUMBER, 
	"TAUX_NON_CHARGE" NUMBER, 
	"BRUT_PAYE" NUMBER, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER, 
	"MOIS_SUIVANT_POUR_TG" VARCHAR2(1 BYTE) DEFAULT 'N'
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."ID_FLUX_MISE_EN_PAIEMENT" IS 'Clef primaire de la table';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."ID_PAIEMENT" IS 'Référence vers un paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."ID_SERVICE" IS 'Référence à une fiche de service';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."NO_INSEE" IS 'Numéro INSEE de l''individu';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."CLE_INSEE" IS 'Clef INSEE de l''individu';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."NOM" IS 'Nom de la personne physique (peut contenir des caractères accentués)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."PRENOM" IS 'Prénom de la personne physique (peut contenir des caractères accentués)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."NPC" IS 'Numéro de Prise en Charge du dossier de l''agent pour les indemnités';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."HEURES_PAYEES" IS 'Le nombre d''heures payées (en HETD)';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."TAUX_NON_CHARGE" IS 'Le taux horaire non chargé';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."BRUT_PAYE" IS 'Le montant brut payé';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"."MOIS_SUIVANT_POUR_TG" IS 'Mois suivant pour paiement TG ? (O:mois suivant, N:mois courant)';
 
   COMMENT ON TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT"  IS 'Table contenant l''historique des heures mise en paiement (à payer ou payées)';
--------------------------------------------------------
--  DDL for Table METHODES_CALCUL_HCOMP
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" 
   (	"METHODE_ID" NUMBER, 
	"LL_METHODE" VARCHAR2(100 BYTE), 
	"CLASSE_METHODE" VARCHAR2(100 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."METHODE_ID" IS 'Clef primaire de la table METHODES_CALCUL_HCOMP.';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."LL_METHODE" IS 'Le libellé de la méthode de calcul';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."CLASSE_METHODE" IS 'Le nom de la classe Java qui permet de faire le calcul';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."D_MODIFICATION" IS 'Date de la dernière modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."PERS_ID_CREATION" IS 'Pers_id de la personne à l''origine de la création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."METHODES_CALCUL_HCOMP"."PERS_ID_MODIFICATION" IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement.';
 
   COMMENT ON TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP"  IS 'Les méthodes de calculs des heures complémentaires';
--------------------------------------------------------
--  DDL for Table MISE_EN_PAIEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" 
   (	"ID_MISE_EN_PAIEMENT" NUMBER, 
	"ID_SERVICE" NUMBER, 
	"ID_SERVICE_DETAIL" NUMBER, 
	"ID_PAIEMENT" NUMBER, 
	"HEURES_A_PAYER" NUMBER, 
	"HEURES_PAYEES" NUMBER, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER, 
	"C_STRUCTURE" VARCHAR2(10 BYTE), 
	"HEURES_A_PAYER_HETD" NUMBER, 
	"HEURES_PAYEES_HETD" NUMBER, 
	"TAUX_BRUT" NUMBER, 
	"TAUX_CHARGE" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."ID_MISE_EN_PAIEMENT" IS 'Clef primaire de la table';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."ID_SERVICE" IS 'Référence à une fiche de service';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."ID_SERVICE_DETAIL" IS 'Référence à un détail fiche de service (doit être un détail de la fiche de service référencée par ID_SERVICE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."ID_PAIEMENT" IS 'Référence vers un paiement (null si les heures ne sont pas encore payées)';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."HEURES_A_PAYER" IS 'Heures à payer';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."HEURES_PAYEES" IS 'Heures payées';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."C_STRUCTURE" IS 'Référence à la structure qui paye';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."HEURES_A_PAYER_HETD" IS 'Heures à payer (en hetd)';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."HEURES_PAYEES_HETD" IS 'Heures payées (en hetd)';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."TAUX_BRUT" IS 'Taux horaire brut appliqué';
 
   COMMENT ON COLUMN "GRH_PECHE"."MISE_EN_PAIEMENT"."TAUX_CHARGE" IS 'Taux horaire chargé appliqué';
 
   COMMENT ON TABLE "GRH_PECHE"."MISE_EN_PAIEMENT"  IS 'Table contenant l''historique des heures mise en paiement (à payer ou payées)';
--------------------------------------------------------
--  DDL for Table PAIEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."PAIEMENT" 
   (	"ID_PAIEMENT" NUMBER, 
	"NUMERO_PAIEMENT" NUMBER, 
	"MOIS_PAIEMENT" NUMBER(2,0), 
	"ANNEE_PAIEMENT" NUMBER(4,0), 
	"PAYE" CHAR(1 BYTE) DEFAULT 'N', 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER, 
	"LIBELLE" VARCHAR2(100 BYTE), 
	"D_PAIEMENT" DATE DEFAULT SYSDATE, 
	"D_BORDEREAU" DATE, 
	"D_ARRETE" DATE, 
	"D_FLUX" DATE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."ID_PAIEMENT" IS 'Clef primaire de la table';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."NUMERO_PAIEMENT" IS 'Numéro de paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."MOIS_PAIEMENT" IS 'Mois de paiement de ce paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."ANNEE_PAIEMENT" IS 'Année de paiement de ce paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."PAYE" IS 'Est-ce que ce paiement est payé (O/N) ? Si oui, il n''est plus du tout modifiable (ni les mises en paiement associées).';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."LIBELLE" IS 'Libellé';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_PAIEMENT" IS 'Date de la mise en paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_BORDEREAU" IS 'Date de dernière génération du bordereau';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_ARRETE" IS 'Date de dernière génération de l''arrêté';
 
   COMMENT ON COLUMN "GRH_PECHE"."PAIEMENT"."D_FLUX" IS 'Date de dernière génération du flux';
 
   COMMENT ON TABLE "GRH_PECHE"."PAIEMENT"  IS 'Table contenant les paiements de mise en paiement';
--------------------------------------------------------
--  DDL for Table PARAMETRES_FLUX_PAIEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" 
   (	"ID_PARAMETRES" NUMBER, 
	"ID_TYPE_FLUX" NUMBER, 
	"TEMPLATE_NOM_FLUX" VARCHAR2(100 BYTE), 
	"ADM" CHAR(3 BYTE), 
	"DEPARTEMENT" CHAR(3 BYTE), 
	"GIRAFE_CODE_CHAINE_PAYE" CHAR(2 BYTE), 
	"GIRAFE_CORR" CHAR(3 BYTE), 
	"GIRAFE_MIN" CHAR(3 BYTE), 
	"GIRAFE_ORIG" CHAR(1 BYTE), 
	"GIRAFE_CODE_IR" CHAR(4 BYTE), 
	"GIRAFE_SENS" CHAR(1 BYTE), 
	"GIRAFE_CODE_MTT_PRE_CALCULE" CHAR(1 BYTE), 
	"GIRAFE_TPL_LIB_COMPLEMENTAIRE" VARCHAR2(100 BYTE), 
	"WINPAIE_CODE_DEBUT_LIGNE_CAD" CHAR(3 BYTE), 
	"WINPAIE_CODE_CHAINE_PAYE" CHAR(7 BYTE), 
	"WINPAIE_TPL_NOM_PRENOM" VARCHAR2(100 BYTE), 
	"WINPAIE_OBJET" CHAR(22 BYTE), 
	"WINPAIE_CODE_F" CHAR(3 BYTE), 
	"WINPAIE_CODE_FIN_LIGNE_CAD" CHAR(14 BYTE), 
	"D_DEBUT" DATE DEFAULT SYSDATE, 
	"D_FIN" DATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."ID_PARAMETRES" IS 'Clef primaire de la table';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."ID_TYPE_FLUX" IS 'Clef étrangère vers la table des types de flux de paiement (TYPE_FLUX_PAIEMENT)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."TEMPLATE_NOM_FLUX" IS 'Template du nom du flux (%1$ : le numéro de paiement, %2$ : l''année de paiement, %3$ : le mois de paiement)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."ADM" IS 'Adm';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."DEPARTEMENT" IS 'Département de l''établissement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_CODE_CHAINE_PAYE" IS 'Flux Girafe - Code de la chaîne de paye';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_CORR" IS 'Flux Girafe - Corr (Code attribué par la Trésorerie Générale)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_MIN" IS 'Flux Girafe - Min';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_ORIG" IS 'Flux Girafe - Orig';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_CODE_IR" IS 'Flux Girafe - Code Ir';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_SENS" IS 'Flux Girafe - Sens du mouvement (0 = positif par défaut)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_CODE_MTT_PRE_CALCULE" IS 'Flux Girafe - Code indiquant que le montant à payer est pré-calculé (A = montant pré-calculé)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."GIRAFE_TPL_LIB_COMPLEMENTAIRE" IS 'Flux Girafe - Template du libellé commentaire (%1$ : le nombre d''heures payées, %2$ : le taux brut, %3$ : le montant brut)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_CODE_DEBUT_LIGNE_CAD" IS 'Flux WinPaie - Code marquant le début de ligne CAD (CAD)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_CODE_CHAINE_PAYE" IS 'Flux WinPaie - Code de la chaîne de paye';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_TPL_NOM_PRENOM" IS 'Flux WinPaie - Template pour nom/prénom (%1$ : le nom, %2$ : le prénom)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_OBJET" IS 'Flux WinPaie - Objet (du paiement ?)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_CODE_F" IS 'Flux WinPaie - Code F (F10)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."WINPAIE_CODE_FIN_LIGNE_CAD" IS 'Flux WinPaie - Code marquant la fin de ligne CAD (            01)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."D_DEBUT" IS 'Date du début d''application de ces paramètres';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."D_FIN" IS 'Date de fin d''application de ces paramètres';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT"  IS 'Table contenant les paramètres nécessaires à la génération des flux de mise en paiement';
--------------------------------------------------------
--  DDL for Table PARAM_POP_HETD
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."PARAM_POP_HETD" 
   (	"ID_PARAM_POP_HETD" NUMBER, 
	"C_TYPE_CONTRAT_TRAV" VARCHAR2(2 BYTE), 
	"C_CORPS" VARCHAR2(4 BYTE), 
	"ID_TYPE_AP" NUMBER(2,0), 
	"NUMERATEUR_SERVICE" NUMBER, 
	"DENOMINATEUR_SERVICE" NUMBER DEFAULT 1, 
	"NUMERATEUR_HCOMP" NUMBER, 
	"DENOMINATEUR_HCOMP" NUMBER, 
	"D_DEBUT" DATE DEFAULT SYSDATE, 
	"D_FIN" DATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."ID_PARAM_POP_HETD" IS 'Clef primaire de la table PARAM_POP_HETD';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."C_TYPE_CONTRAT_TRAV" IS 'Type de population concernée parmi les contractuels(cf. GRHUM.TYPE_CONTRAT_TRAVAIL.C_TYPE_CONTRAT_TRAV)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."C_CORPS" IS 'Type de population concernée parmi les titulaires (cf. GRHUM.CORPS.C_CORPS)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."ID_TYPE_AP" IS 'Type du code de l''AP (cf. SCO_SCOLARITE.TYPE_AP.ID)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."NUMERATEUR_SERVICE" IS 'Numérateur du coefficient pour le calcul de l''AP dans le service';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."DENOMINATEUR_SERVICE" IS 'Dénominateur du coefficient pour le calcul de l''AP dans le service';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."NUMERATEUR_HCOMP" IS 'Numérateur du coefficient pour le calcul de l''AP en dehors du service dû';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."DENOMINATEUR_HCOMP" IS 'Dénominateur du coefficient pour le calcul de l''AP en dehors du service dû';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."D_DEBUT" IS 'Date du début d''application de l''équivalence';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."D_FIN" IS 'Date de fin d''application de l''équivalence';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."PARAM_POP_HETD"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."PARAM_POP_HETD"  IS 'Table qui définit les heures en équivalent TD des AP(CM, TD,...) en fonction de la population';
--------------------------------------------------------
--  DDL for Table PECHE_PARAMETRES
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."PECHE_PARAMETRES" 
   (	"PARAM_ID" NUMBER, 
	"PARAM_KEY" VARCHAR2(2000 BYTE), 
	"PARAM_VALUE" VARCHAR2(500 BYTE), 
	"PARAM_COMMENTAIRES" VARCHAR2(500 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PARAM_ID" IS 'Clef primaire de la table PECHE_PARAMETRES.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PARAM_KEY" IS 'Code du paramètre ';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PARAM_VALUE" IS 'Valeur du paramètre';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PARAM_COMMENTAIRES" IS 'Commentaire du paramètre';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."D_MODIFICATION" IS 'Date de la dernière modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PERS_ID_CREATION" IS 'Pers_id de la personne à l''origine de la création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."PECHE_PARAMETRES"."PERS_ID_MODIFICATION" IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement.';
 
   COMMENT ON TABLE "GRH_PECHE"."PECHE_PARAMETRES"  IS 'Les paramètres de l''application PECHE';
--------------------------------------------------------
--  DDL for Table PERIODE
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."PERIODE" 
   (	"ID_PERIODE" NUMBER, 
	"LL_PERIODE" VARCHAR2(15 BYTE), 
	"TYPE" VARCHAR2(1 BYTE) DEFAULT 'M', 
	"ORDRE_AFFICHAGE" NUMBER(*,0), 
	"ORDRE_PERIODE" NUMBER(*,0), 
	"NO_SEMESTRE" NUMBER(*,0), 
	"NO_PREMIER_MOIS" NUMBER(*,0) DEFAULT 0, 
	"NO_DERNIER_MOIS" NUMBER(*,0) DEFAULT 0
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."ID_PERIODE" IS 'Clef primaire de la table PERIODE';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."LL_PERIODE" IS 'Libellé de la période';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."TYPE" IS 'Type de la période (M pour le mois, S pour le semestre)';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."ORDRE_AFFICHAGE" IS 'Ordre d''affichage de la période';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."ORDRE_PERIODE" IS 'Ordre de la période';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."NO_SEMESTRE" IS 'Numéro du semestre lié à la période';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."NO_PREMIER_MOIS" IS 'Numéro du premier mois de la période';
 
   COMMENT ON COLUMN "GRH_PECHE"."PERIODE"."NO_DERNIER_MOIS" IS 'Numéro du dernier mois de la période';
 
   COMMENT ON TABLE "GRH_PECHE"."PERIODE"  IS 'Table des périodes utilisées dans PECHE';
--------------------------------------------------------
--  DDL for Table REH
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."REH" 
   (	"ID_REH" NUMBER, 
	"LC_REH" VARCHAR2(128 BYTE), 
	"LL_REH" VARCHAR2(256 BYTE), 
	"DESCRIPTION" VARCHAR2(2048 BYTE), 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_DEBUT" DATE DEFAULT SYSDATE, 
	"D_FIN" DATE DEFAULT SYSDATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."REH"."ID_REH" IS 'Clef primaire de la table REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."LC_REH" IS 'Libellé court de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."LL_REH" IS 'Libellé long de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."DESCRIPTION" IS 'Description de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."COMMENTAIRE" IS 'Commentaire avec les méthodes de calcul de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."D_DEBUT" IS 'Date du début de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."D_FIN" IS 'Date du fin de l''activité REH';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REH"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."REH"  IS 'Table des activités du Réferentiel des Equivalences Horaires';
--------------------------------------------------------
--  DDL for Table REPART_ENSEIGNANT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."REPART_ENSEIGNANT" 
   (	"ID_REPART_ENSEIGNANT" NUMBER, 
	"ID_SERVICE_DETAIL" NUMBER, 
	"COMPOSANTE_REFERENTE" VARCHAR2(10 BYTE), 
	"REPARTITEUR_DEMANDEUR" NUMBER, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."ID_REPART_ENSEIGNANT" IS 'Clef primaire de la table REPART_ENSEIGNANT';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."ID_SERVICE_DETAIL" IS 'Référence au détail du service de l''enseignant concerné (cf. GRH_PECHE.SERVICE_DETAIL.ID_SERVICE_DETAIL)';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."COMPOSANTE_REFERENTE" IS 'Référence à la composante référente de l''enseignant (cf. GRHUM.STRUCTURE_ULR.C_STRUCTURE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."REPARTITEUR_DEMANDEUR" IS 'Référence au répartiteur demandeur d''une autre composante (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_ENSEIGNANT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."REPART_ENSEIGNANT"  IS 'Table où sont listés le répartiteur référent d''un enseignant et un autre répartiteur qui veut affecter des heures à cet enseignant';
--------------------------------------------------------
--  DDL for Table REPART_METHODE_CORPS
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."REPART_METHODE_CORPS" 
   (	"METHODE_ID" NUMBER, 
	"C_CORPS" VARCHAR2(4 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"ID_REPART_METHODE_CORPS" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."REPART_METHODE_CORPS"."METHODE_ID" IS 'Référence la clef primaire de la table GRHUM.CORPS.';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_METHODE_CORPS"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_METHODE_CORPS"."D_MODIFICATION" IS 'Date de la dernière modification de l''enregistrement.';
 
   COMMENT ON TABLE "GRH_PECHE"."REPART_METHODE_CORPS"  IS 'Répartition des méthodes de calculs pour les corps des enseignants.';
--------------------------------------------------------
--  DDL for Table REPART_UEF_ETABLISSEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" 
   (	"ID_REPART_UEF_ETAB" NUMBER, 
	"ID_UE" NUMBER, 
	"ETABLISSEMENT_EXTERNE" VARCHAR2(1024 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."ID_REPART_UEF_ETAB" IS 'Clef primaire de la table REPART_UEF_ETABLISSEMENT';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."ID_UE" IS 'Référence à une UE flottante présente dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.UE.ID)';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."ETABLISSEMENT_EXTERNE" IS 'Etablissement externe où se déroulera l''enseignement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT"  IS 'Table de répartition des UE flottantes (taguées PECHE) et d''un établissement d''enseignement externe';
--------------------------------------------------------
--  DDL for Table SERVICE
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."SERVICE" 
   (	"ID_SERVICE" NUMBER, 
	"ID_DEMANDE" NUMBER, 
	"NO_INDIVIDU" NUMBER(8,0), 
	"ID_ENSEIGNANT_GENERIQUE" NUMBER, 
	"TEM_ENS_GENERIQUE" VARCHAR2(1 BYTE) DEFAULT 'N', 
	"TEM_VALIDE" VARCHAR2(1 BYTE) DEFAULT 'O', 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_COMMENTAIRE" DATE, 
	"ANNEE" NUMBER(4,0), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER, 
	"HEURES_A_PAYER" NUMBER, 
	"HEURES_A_REPORTER_SOUHAITEES" NUMBER, 
	"HEURES_A_REPORTER_ATTRIBUEES" NUMBER, 
	"HEURES_GRATUITES" NUMBER, 
	"HEURES_PAYEES" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."ID_SERVICE" IS 'Clef primaire de la table SERVICE';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."ID_DEMANDE" IS 'Référence à la demande sur le circuit de validation (cf. WORKFLOW.DEMANDE.ID_DEMANDE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."NO_INDIVIDU" IS 'Intervenant concerné (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."ID_ENSEIGNANT_GENERIQUE" IS 'Intervenant générique concerné (cf. GRH_PECHE.ENSEIGNANT_GENERIQUE.ID_ENSEIGNANT_GENERIQUE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."TEM_ENS_GENERIQUE" IS 'L''affectation concerne un intervenant générique? (O:oui, N:non) ';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."TEM_VALIDE" IS 'L''enregistrement est-il valide? (O:oui, N:non) ';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."COMMENTAIRE" IS 'Commentaire sur la fiche de service';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."D_COMMENTAIRE" IS 'Date de création du commentaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."ANNEE" IS 'Année universitaire considérée';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."HEURES_A_PAYER" IS 'Heures à payer (parmi les HComp et pour les statutaires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."HEURES_A_REPORTER_SOUHAITEES" IS 'Heures que l''enseignant souhaite reporter pour l''année N+1 (parmi les HComp et pour les statutaires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."HEURES_A_REPORTER_ATTRIBUEES" IS 'Heures effectivement reportées pour l''année N+1 (parmi les HComp et pour les statutaires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."HEURES_GRATUITES" IS 'Heures qui ne seront pas payées (parmi les HComp et pour les statutaires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE"."HEURES_PAYEES" IS 'Heures qui ont été payées (parmi les HComp et pour les statutaires)';
 
   COMMENT ON TABLE "GRH_PECHE"."SERVICE"  IS 'Fiche de service d''un intervenant';
--------------------------------------------------------
--  DDL for Table SERVICE_DETAIL
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."SERVICE_DETAIL" 
   (	"ID_SERVICE_DETAIL" NUMBER, 
	"ID_AP" NUMBER, 
	"ID_REH" NUMBER, 
	"ID_SERVICE" NUMBER, 
	"HEURES_PREVUES" NUMBER DEFAULT 0, 
	"HEURES_REALISEES" NUMBER DEFAULT 0, 
	"COMMENTAIRE" VARCHAR2(1024 BYTE), 
	"D_COMMENTAIRE" DATE, 
	"TEM_PAYE" VARCHAR2(1 BYTE) DEFAULT 'N', 
	"TEM_REPART_CROISEE" VARCHAR2(1 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER, 
	"HEURES_A_PAYER" NUMBER, 
	"HEURES_PAYEES" NUMBER, 
	"ID_PERIODE" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."ID_SERVICE_DETAIL" IS 'Clef primaire de la table SERVICE_DETAIL';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."ID_AP" IS 'Référence à un AP présent dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.AP.ID)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."ID_REH" IS 'Référence à une activité REH dans laquelle intervient l''enseignant (cf. GRH_PECHE.REH.ID_REH)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."ID_SERVICE" IS 'Référence à la fiche de service de l''intervenant (cf. GRH_PECHE.SERVICE.ID_SERVICE)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."HEURES_PREVUES" IS 'Heures prévues dans le service de l''intervenant';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."HEURES_REALISEES" IS 'Heures réalisées dans le service de l''intervenant';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."COMMENTAIRE" IS 'Commentaire sur le détail de la fiche de service';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."D_COMMENTAIRE" IS 'Date de création du commentaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."TEM_PAYE" IS 'Intervenant payé (O:oui, N:non) ';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."TEM_REPART_CROISEE" IS 'Répartition croisée: (A:accepté, R:refusé, E:en attente, null:pas de répartition croisée)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."D_CREATION" IS 'Date de création de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."D_MODIFICATION" IS 'Date de modification de l''enregistrement.';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."HEURES_A_PAYER" IS 'Heures à payer (parmi les heures réalisées et pour les vacataires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."HEURES_PAYEES" IS 'Heures qui ont été payées (parmi les heures réalisées et pour les vacataires)';
 
   COMMENT ON COLUMN "GRH_PECHE"."SERVICE_DETAIL"."ID_PERIODE" IS 'Période concernée (mois ou semestre) (cf. GRH_PECHE.PERIODE.ID_PERIODE)';
 
   COMMENT ON TABLE "GRH_PECHE"."SERVICE_DETAIL"  IS 'Détail de la fiche de service d''un intervenant';
--------------------------------------------------------
--  DDL for Table TAUX_HORAIRE
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."TAUX_HORAIRE" 
   (	"ID_TAUX_HORAIRE" NUMBER, 
	"TAUX_BRUT" NUMBER, 
	"TAUX_CHARGE_FONCTIONNAIRE" NUMBER, 
	"TAUX_CHARGE_NON_FONCTIONNAIRE" NUMBER, 
	"D_DEBUT" DATE DEFAULT SYSDATE, 
	"D_FIN" DATE, 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."ID_TAUX_HORAIRE" IS 'Clef primaire de la table';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."TAUX_BRUT" IS 'Taux horaire brut appliqué à toutes populations confondus';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."TAUX_CHARGE_FONCTIONNAIRE" IS 'Taux horaire chargé pour les fonctionnaires uniquement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."TAUX_CHARGE_NON_FONCTIONNAIRE" IS 'Taux horaire chargé pour les non fonctionnaires uniquement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."D_DEBUT" IS 'Date du début d''application du taux horaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."D_FIN" IS 'Date de fin d''application du taux horaire';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."D_MODIFICATION" IS 'Date de modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."PERS_ID_CREATION" IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TAUX_HORAIRE"."PERS_ID_MODIFICATION" IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."TAUX_HORAIRE"  IS 'Table des taux horaires appliqués pour les paiements des intervenants';
--------------------------------------------------------
--  DDL for Table TYPE_FLUX_PAIEMENT
--------------------------------------------------------

  CREATE TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" 
   (	"ID_TYPE_FLUX" NUMBER, 
	"LL_TYPE_FLUX" VARCHAR2(100 BYTE), 
	"CLASSE_TYPE_FLUX" VARCHAR2(100 BYTE), 
	"D_CREATION" DATE DEFAULT SYSDATE, 
	"D_MODIFICATION" DATE DEFAULT SYSDATE, 
	"PERS_ID_CREATION" NUMBER, 
	"PERS_ID_MODIFICATION" NUMBER
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
 

   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."ID_TYPE_FLUX" IS 'Clef primaire de la table TYPE_FLUX_PAIEMENT';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."LL_TYPE_FLUX" IS 'Libellé du type de flux';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."CLASSE_TYPE_FLUX" IS 'nom de la classe Java qui permet de formater le flux de mise en paiement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."D_MODIFICATION" IS 'Date de la dernière modification de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."PERS_ID_CREATION" IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
 
   COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."PERS_ID_MODIFICATION" IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';
 
   COMMENT ON TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT"  IS 'Table des types de flux de paiement';
--------------------------------------------------------
--  DDL for Index IDX_EG_C_CORPS
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_EG_C_CORPS" ON "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ("C_CORPS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FMEP_NO_INSEE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FMEP_NO_INSEE" ON "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ("NO_INSEE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FMEP_NOM_PRENOM
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FMEP_NOM_PRENOM" ON "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ("NOM", "PRENOM") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FVD_ID_AP
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FVD_ID_AP" ON "GRH_PECHE"."FICHE_VOEUX_DETAIL" ("ID_AP") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FVD_ID_FICHE_VOEUX
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FVD_ID_FICHE_VOEUX" ON "GRH_PECHE"."FICHE_VOEUX_DETAIL" ("ID_FICHE_VOEUX") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FV_ID_DEMANDE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FV_ID_DEMANDE" ON "GRH_PECHE"."FICHE_VOEUX" ("ID_DEMANDE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_FV_NO_INDIVIDU
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_FV_NO_INDIVIDU" ON "GRH_PECHE"."FICHE_VOEUX" ("NO_INDIVIDU") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_PARAM_PARAM_KEY
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_PARAM_PARAM_KEY" ON "GRH_PECHE"."PECHE_PARAMETRES" ("PARAM_KEY") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_PPH_C_CORPS
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_PPH_C_CORPS" ON "GRH_PECHE"."PARAM_POP_HETD" ("C_CORPS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_PPH_C_TYPE_CONTRAT_TRAV
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_PPH_C_TYPE_CONTRAT_TRAV" ON "GRH_PECHE"."PARAM_POP_HETD" ("C_TYPE_CONTRAT_TRAV") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_PPH_TYPE_AP
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_PPH_TYPE_AP" ON "GRH_PECHE"."PARAM_POP_HETD" ("ID_TYPE_AP") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_RE_COMPOSANTE_REFERENTE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_RE_COMPOSANTE_REFERENTE" ON "GRH_PECHE"."REPART_ENSEIGNANT" ("COMPOSANTE_REFERENTE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_RE_ID_SERVICE_DETAIL
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_RE_ID_SERVICE_DETAIL" ON "GRH_PECHE"."REPART_ENSEIGNANT" ("ID_SERVICE_DETAIL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_RE_REPARTITEUR_DEMANDEUR
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_RE_REPARTITEUR_DEMANDEUR" ON "GRH_PECHE"."REPART_ENSEIGNANT" ("REPARTITEUR_DEMANDEUR") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_RUE_ID_UE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_RUE_ID_UE" ON "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ("ID_UE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SD_ID_AP
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SD_ID_AP" ON "GRH_PECHE"."SERVICE_DETAIL" ("ID_AP") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SD_ID_PERIODE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SD_ID_PERIODE" ON "GRH_PECHE"."SERVICE_DETAIL" ("ID_PERIODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SD_ID_REH
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SD_ID_REH" ON "GRH_PECHE"."SERVICE_DETAIL" ("ID_REH") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SD_ID_SERVICE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SD_ID_SERVICE" ON "GRH_PECHE"."SERVICE_DETAIL" ("ID_SERVICE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SER_ANNEE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SER_ANNEE" ON "GRH_PECHE"."SERVICE" ("ANNEE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SER_ID_DEMANDE
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SER_ID_DEMANDE" ON "GRH_PECHE"."SERVICE" ("ID_DEMANDE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SER_ID_EG
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SER_ID_EG" ON "GRH_PECHE"."SERVICE" ("ID_ENSEIGNANT_GENERIQUE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_SER_NO_INDIVIDU
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_SER_NO_INDIVIDU" ON "GRH_PECHE"."SERVICE" ("NO_INDIVIDU") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index IDX_TH_D_FIN_D_DEBUT
--------------------------------------------------------

  CREATE INDEX "GRH_PECHE"."IDX_TH_D_FIN_D_DEBUT" ON "GRH_PECHE"."TAUX_HORAIRE" ("D_FIN", "D_DEBUT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_ARRETE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_ARRETE" ON "GRH_PECHE"."PAIEMENT" ("ID_PAIEMENT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_DB_VERSION
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_DB_VERSION" ON "GRH_PECHE"."DB_VERSION" ("DBV_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_ENSEIGNANT_GENERIQUE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_ENSEIGNANT_GENERIQUE" ON "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ("ID_ENSEIGNANT_GENERIQUE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_FICHE_VOEUX
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_FICHE_VOEUX" ON "GRH_PECHE"."FICHE_VOEUX" ("ID_FICHE_VOEUX") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_FICHE_VOEUX_DETAIL
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_FICHE_VOEUX_DETAIL" ON "GRH_PECHE"."FICHE_VOEUX_DETAIL" ("ID_FV_DETAIL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_FLUX_MISE_EN_PAIEMENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_FLUX_MISE_EN_PAIEMENT" ON "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ("ID_FLUX_MISE_EN_PAIEMENT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_ID_TYPE_FLUX
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_ID_TYPE_FLUX" ON "GRH_PECHE"."TYPE_FLUX_PAIEMENT" ("ID_TYPE_FLUX") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_METHODES_CALCUL_HCOMP
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_METHODES_CALCUL_HCOMP" ON "GRH_PECHE"."METHODES_CALCUL_HCOMP" ("METHODE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_MISE_EN_PAIEMENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_MISE_EN_PAIEMENT" ON "GRH_PECHE"."MISE_EN_PAIEMENT" ("ID_MISE_EN_PAIEMENT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_PARAMETRES_FLUX_PAIEMENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_PARAMETRES_FLUX_PAIEMENT" ON "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" ("ID_PARAMETRES") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_PARAM_POP_HETD
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_PARAM_POP_HETD" ON "GRH_PECHE"."PARAM_POP_HETD" ("ID_PARAM_POP_HETD") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_PECHE_PARAMETRES
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_PECHE_PARAMETRES" ON "GRH_PECHE"."PECHE_PARAMETRES" ("PARAM_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_PERIODE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_PERIODE" ON "GRH_PECHE"."PERIODE" ("ID_PERIODE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_REH
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_REH" ON "GRH_PECHE"."REH" ("ID_REH") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_REPART_ENSEIGNANT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_REPART_ENSEIGNANT" ON "GRH_PECHE"."REPART_ENSEIGNANT" ("ID_REPART_ENSEIGNANT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_REPART_METHODE_CORPS
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_REPART_METHODE_CORPS" ON "GRH_PECHE"."REPART_METHODE_CORPS" ("ID_REPART_METHODE_CORPS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_REPART_UEF_ETABLISSEMENT
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_REPART_UEF_ETABLISSEMENT" ON "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ("ID_REPART_UEF_ETAB") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_SERVICE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_SERVICE" ON "GRH_PECHE"."SERVICE" ("ID_SERVICE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_SERVICE_DETAIL
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_SERVICE_DETAIL" ON "GRH_PECHE"."SERVICE_DETAIL" ("ID_SERVICE_DETAIL") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PK_TAUX_HORAIRE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PK_TAUX_HORAIRE" ON "GRH_PECHE"."TAUX_HORAIRE" ("ID_TAUX_HORAIRE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index PMA
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."PMA" ON "GRH_PECHE"."SERVICE" ("ANNEE", "NO_INDIVIDU", "ID_ENSEIGNANT_GENERIQUE") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH" ;
--------------------------------------------------------
--  DDL for Index UK_ARRETE
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."UK_ARRETE" ON "GRH_PECHE"."PAIEMENT" ("NUMERO_PAIEMENT") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  DDL for Index UK_REPART_METHODE_CORPS_CORPS
--------------------------------------------------------

  CREATE UNIQUE INDEX "GRH_PECHE"."UK_REPART_METHODE_CORPS_CORPS" ON "GRH_PECHE"."REPART_METHODE_CORPS" ("C_CORPS") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX" ;
--------------------------------------------------------
--  Constraints for Table DB_VERSION
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."DB_VERSION" ADD CONSTRAINT "PK_DB_VERSION" PRIMARY KEY ("DBV_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."DB_VERSION" MODIFY ("DBV_ID" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."DB_VERSION" MODIFY ("DBV_LIBELLE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."DB_VERSION" MODIFY ("DBV_DATE" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table ENSEIGNANT_GENERIQUE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ADD CONSTRAINT "CHK_EG_TEM_VALIDE" CHECK (TEM_VALIDE IN ('O', 'N') ) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ADD CONSTRAINT "PK_ENSEIGNANT_GENERIQUE" PRIMARY KEY ("ID_ENSEIGNANT_GENERIQUE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("ID_ENSEIGNANT_GENERIQUE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("HEURES_PREVUES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("TEM_VALIDE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table FICHE_VOEUX
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "CHK_FV_TEM_VALIDE" CHECK (TEM_VALIDE IN ('O', 'N')) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "PK_FICHE_VOEUX" PRIMARY KEY ("ID_FICHE_VOEUX")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("ID_FICHE_VOEUX" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("ID_DEMANDE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("NO_INDIVIDU" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("TEM_VALIDE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("ANNEE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table FICHE_VOEUX_DETAIL
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" ADD CONSTRAINT "PK_FICHE_VOEUX_DETAIL" PRIMARY KEY ("ID_FV_DETAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("ID_FV_DETAIL" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("ID_FICHE_VOEUX" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("HEURES_SOUHAITEES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table FLUX_MISE_EN_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "CHK_MOIS_SUIVANT_POUR_TG" CHECK (MOIS_SUIVANT_POUR_TG IN ('O', 'N')) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "PK_FLUX_MISE_EN_PAIEMENT" PRIMARY KEY ("ID_FLUX_MISE_EN_PAIEMENT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("ID_FLUX_MISE_EN_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("ID_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("ID_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("NOM" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("PRENOM" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("HEURES_PAYEES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("TAUX_NON_CHARGE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("BRUT_PAYE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("PERS_ID_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" MODIFY ("MOIS_SUIVANT_POUR_TG" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table METHODES_CALCUL_HCOMP
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" ADD CONSTRAINT "PK_METHODES_CALCUL_HCOMP" PRIMARY KEY ("METHODE_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" MODIFY ("METHODE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table MISE_EN_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "PK_MISE_EN_PAIEMENT" PRIMARY KEY ("ID_MISE_EN_PAIEMENT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("ID_MISE_EN_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("ID_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("HEURES_A_PAYER" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("PERS_ID_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("HEURES_A_PAYER_HETD" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" MODIFY ("TAUX_BRUT" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PAIEMENT" ADD CONSTRAINT "CHK_PAIEMENT_PAYE" CHECK (PAYE IN ('O', 'N')) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" ADD CONSTRAINT "PK_PAIEMENT" PRIMARY KEY ("ID_PAIEMENT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("ID_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("NUMERO_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("MOIS_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("ANNEE_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("PAYE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("PERS_ID_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" MODIFY ("D_PAIEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" ADD CONSTRAINT "UK_PAIEMENT" UNIQUE ("NUMERO_PAIEMENT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table PARAMETRES_FLUX_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" ADD CONSTRAINT "PK_PARAMETRES_FLUX_PAIEMENT" PRIMARY KEY ("ID_PARAMETRES")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("ID_PARAMETRES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("ID_TYPE_FLUX" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("TEMPLATE_NOM_FLUX" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("ADM" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("DEPARTEMENT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_CODE_CHAINE_PAYE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_CORR" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_MIN" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_ORIG" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_CODE_IR" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_SENS" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_CODE_MTT_PRE_CALCULE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("GIRAFE_TPL_LIB_COMPLEMENTAIRE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_CODE_DEBUT_LIGNE_CAD" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_CODE_CHAINE_PAYE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_TPL_NOM_PRENOM" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_OBJET" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_CODE_F" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("WINPAIE_CODE_FIN_LIGNE_CAD" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("D_DEBUT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" MODIFY ("PERS_ID_MODIFICATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PARAM_POP_HETD
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "PK_PARAM_POP_HETD" PRIMARY KEY ("ID_PARAM_POP_HETD")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "PPH_TCT_CORPS_NOT_NULLS" CHECK (C_TYPE_CONTRAT_TRAV IS NULL OR C_CORPS IS NULL) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("ID_PARAM_POP_HETD" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("ID_TYPE_AP" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("NUMERATEUR_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("DENOMINATEUR_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("D_DEBUT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PECHE_PARAMETRES
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" ADD CONSTRAINT "PK_PECHE_PARAMETRES" PRIMARY KEY ("PARAM_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" MODIFY ("PARAM_ID" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table PERIODE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PERIODE" ADD CONSTRAINT "CHK_PERIODE_TYPE" CHECK (TYPE IN ('M', 'S')) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PERIODE" ADD CONSTRAINT "PK_PERIODE" PRIMARY KEY ("ID_PERIODE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("ID_PERIODE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("TYPE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("ORDRE_AFFICHAGE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("ORDRE_PERIODE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("NO_SEMESTRE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("NO_PREMIER_MOIS" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."PERIODE" MODIFY ("NO_DERNIER_MOIS" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REH
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REH" ADD CONSTRAINT "PK_REH" PRIMARY KEY ("ID_REH")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REH" MODIFY ("ID_REH" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REH" MODIFY ("D_DEBUT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REH" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REH" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REH" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REPART_ENSEIGNANT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "PK_REPART_ENSEIGNANT" PRIMARY KEY ("ID_REPART_ENSEIGNANT")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("ID_REPART_ENSEIGNANT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("ID_SERVICE_DETAIL" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("COMPOSANTE_REFERENTE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("REPARTITEUR_DEMANDEUR" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table REPART_METHODE_CORPS
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" ADD CONSTRAINT "PK_REPART_METHODE_CORPS" PRIMARY KEY ("ID_REPART_METHODE_CORPS")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" MODIFY ("METHODE_ID" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" MODIFY ("C_CORPS" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" MODIFY ("ID_REPART_METHODE_CORPS" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" ADD CONSTRAINT "UK_REPART_METHODE_CORPS_CORPS" UNIQUE ("C_CORPS")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
--------------------------------------------------------
--  Constraints for Table REPART_UEF_ETABLISSEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ADD CONSTRAINT "PK_REPART_UEF_ETABLISSEMENT" PRIMARY KEY ("ID_REPART_UEF_ETAB")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("ID_REPART_UEF_ETAB" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("ID_UE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("ETABLISSEMENT_EXTERNE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SERVICE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "CHK_SER_TEM_EG" CHECK (TEM_ENS_GENERIQUE IN ('O', 'N') ) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "CHK_SER_TEM_VALIDE" CHECK (TEM_VALIDE IN ('O', 'N') ) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "INDIVIDU_EG_NOT_NULLS" CHECK (NO_INDIVIDU IS NULL OR ID_ENSEIGNANT_GENERIQUE IS NULL) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "INDIVIDU_EG_NULLS" CHECK (NO_INDIVIDU IS NOT NULL OR ID_ENSEIGNANT_GENERIQUE IS NOT NULL) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "PK_SERVICE" PRIMARY KEY ("ID_SERVICE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("ID_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("TEM_ENS_GENERIQUE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("TEM_VALIDE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("ANNEE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table SERVICE_DETAIL
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "AP_REH_NOT_NULLS" CHECK (ID_AP IS NULL OR ID_REH IS NULL) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "AP_REH_NULLS" CHECK (ID_AP IS NOT NULL OR ID_REH IS NOT NULL) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "CHK_SD_TEM_PAYE" CHECK (TEM_PAYE IN ('O', 'N') ) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "CHK_SD_TEM_REPART_CROISEE" CHECK (TEM_REPART_CROISEE IN ('A', 'R', 'E') ) ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "PK_SERVICE_DETAIL" PRIMARY KEY ("ID_SERVICE_DETAIL")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("ID_SERVICE_DETAIL" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("ID_SERVICE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("HEURES_PREVUES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("HEURES_REALISEES" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("TEM_PAYE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TAUX_HORAIRE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" ADD CONSTRAINT "PK_TAUX_HORAIRE" PRIMARY KEY ("ID_TAUX_HORAIRE")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("ID_TAUX_HORAIRE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("TAUX_BRUT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("TAUX_CHARGE_FONCTIONNAIRE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("TAUX_CHARGE_NON_FONCTIONNAIRE" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("D_DEBUT" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" MODIFY ("PERS_ID_MODIFICATION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table TYPE_FLUX_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" ADD CONSTRAINT "PK_ID_TYPE_FLUX" PRIMARY KEY ("ID_TYPE_FLUX")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "GRH_INDX"  ENABLE;
 
  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" MODIFY ("ID_TYPE_FLUX" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" MODIFY ("D_CREATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" MODIFY ("D_MODIFICATION" NOT NULL ENABLE);
 
  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" MODIFY ("PERS_ID_CREATION" NOT NULL ENABLE);

--------------------------------------------------------
--  Ref Constraints for Table ENSEIGNANT_GENERIQUE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ADD CONSTRAINT "FK_EG_CORPS" FOREIGN KEY ("C_CORPS")
	  REFERENCES "GRHUM"."CORPS" ("C_CORPS") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ADD CONSTRAINT "FK_EG_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ADD CONSTRAINT "FK_EG_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table FICHE_VOEUX
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "FK_FV_DEMANDE" FOREIGN KEY ("ID_DEMANDE")
	  REFERENCES "WORKFLOW"."DEMANDE" ("ID_DEMANDE") DEFERRABLE INITIALLY DEFERRED ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "FK_FV_INDIVIDU" FOREIGN KEY ("NO_INDIVIDU")
	  REFERENCES "GRHUM"."INDIVIDU_ULR" ("NO_INDIVIDU") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "FK_FV_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX" ADD CONSTRAINT "FK_FV_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table FICHE_VOEUX_DETAIL
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" ADD CONSTRAINT "FK_FVD_AP" FOREIGN KEY ("ID_AP")
	  REFERENCES "SCO_SCOLARITE"."COMPOSANT" ("ID_COMPOSANT") DEFERRABLE INITIALLY DEFERRED ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" ADD CONSTRAINT "FK_FVD_FICHE_VOEUX" FOREIGN KEY ("ID_FICHE_VOEUX")
	  REFERENCES "GRH_PECHE"."FICHE_VOEUX" ("ID_FICHE_VOEUX") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" ADD CONSTRAINT "FK_FVD_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FICHE_VOEUX_DETAIL" ADD CONSTRAINT "FK_FVD_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table FLUX_MISE_EN_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_FMEP_PAIEMENT" FOREIGN KEY ("ID_PAIEMENT")
	  REFERENCES "GRH_PECHE"."PAIEMENT" ("ID_PAIEMENT") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_FMEP_PERS_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_FMEP_PERS_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."FLUX_MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_FMEP_SERVICE" FOREIGN KEY ("ID_SERVICE")
	  REFERENCES "GRH_PECHE"."SERVICE" ("ID_SERVICE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table METHODES_CALCUL_HCOMP
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" ADD CONSTRAINT "FK_METHODES_PERS_ID_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."METHODES_CALCUL_HCOMP" ADD CONSTRAINT "FK_METH_PERS_ID_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table MISE_EN_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_PAIEMENT" FOREIGN KEY ("ID_PAIEMENT")
	  REFERENCES "GRH_PECHE"."PAIEMENT" ("ID_PAIEMENT") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_PERS_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_PERS_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_SERVICE" FOREIGN KEY ("ID_SERVICE")
	  REFERENCES "GRH_PECHE"."SERVICE" ("ID_SERVICE") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_SERVICE_DETAIL" FOREIGN KEY ("ID_SERVICE_DETAIL")
	  REFERENCES "GRH_PECHE"."SERVICE_DETAIL" ("ID_SERVICE_DETAIL") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."MISE_EN_PAIEMENT" ADD CONSTRAINT "FK_MEP_STRUCTURE" FOREIGN KEY ("C_STRUCTURE")
	  REFERENCES "GRHUM"."STRUCTURE_ULR" ("C_STRUCTURE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PAIEMENT" ADD CONSTRAINT "FK_PAIEMENT_PERS_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PAIEMENT" ADD CONSTRAINT "FK_PAIEMENT_PERS_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PARAMETRES_FLUX_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" ADD CONSTRAINT "FK_PFP_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" ADD CONSTRAINT "FK_PFP_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAMETRES_FLUX_PAIEMENT" ADD CONSTRAINT "FK_PFP_TYPE_FLUX_PAIEMENT" FOREIGN KEY ("ID_TYPE_FLUX")
	  REFERENCES "GRH_PECHE"."TYPE_FLUX_PAIEMENT" ("ID_TYPE_FLUX") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PARAM_POP_HETD
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "FK_PPH_CORPS" FOREIGN KEY ("C_CORPS")
	  REFERENCES "GRHUM"."CORPS" ("C_CORPS") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "FK_PPH_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "FK_PPH_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PARAM_POP_HETD" ADD CONSTRAINT "FK_PPH_TYPE_CONTRAT_TRAV" FOREIGN KEY ("C_TYPE_CONTRAT_TRAV")
	  REFERENCES "GRHUM"."TYPE_CONTRAT_TRAVAIL" ("C_TYPE_CONTRAT_TRAV") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table PECHE_PARAMETRES
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" ADD CONSTRAINT "FK_PARAM_PERS_ID_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."PECHE_PARAMETRES" ADD CONSTRAINT "FK_PARAM_PERS_ID_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;

--------------------------------------------------------
--  Ref Constraints for Table REH
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REH" ADD CONSTRAINT "FK_REH_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REH" ADD CONSTRAINT "FK_REH_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REPART_ENSEIGNANT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "FK_RE_COMPOSANTE_REFERENTE" FOREIGN KEY ("COMPOSANTE_REFERENTE")
	  REFERENCES "GRHUM"."STRUCTURE_ULR" ("C_STRUCTURE") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "FK_RE_PERS_ID_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "FK_RE_PERS_ID_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "FK_RE_REPARTITEUR_DEMANDEUR" FOREIGN KEY ("REPARTITEUR_DEMANDEUR")
	  REFERENCES "GRHUM"."INDIVIDU_ULR" ("NO_INDIVIDU") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_ENSEIGNANT" ADD CONSTRAINT "FK_RE_SERVICE_DETAIL" FOREIGN KEY ("ID_SERVICE_DETAIL")
	  REFERENCES "GRH_PECHE"."SERVICE_DETAIL" ("ID_SERVICE_DETAIL") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REPART_METHODE_CORPS
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" ADD CONSTRAINT "FK_REPART_METHODE_CORPS_CORPS" FOREIGN KEY ("C_CORPS")
	  REFERENCES "GRHUM"."CORPS" ("C_CORPS") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_METHODE_CORPS" ADD CONSTRAINT "FK_REPART_METHODE_CORPS_METHO" FOREIGN KEY ("METHODE_ID")
	  REFERENCES "GRH_PECHE"."METHODES_CALCUL_HCOMP" ("METHODE_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table REPART_UEF_ETABLISSEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ADD CONSTRAINT "FK_RUE_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ADD CONSTRAINT "FK_RUE_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."REPART_UEF_ETABLISSEMENT" ADD CONSTRAINT "FK_RUE_UE" FOREIGN KEY ("ID_UE")
	  REFERENCES "SCO_SCOLARITE"."COMPOSANT" ("ID_COMPOSANT") DEFERRABLE INITIALLY DEFERRED ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SERVICE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "FK_SER_DEMANDE" FOREIGN KEY ("ID_DEMANDE")
	  REFERENCES "WORKFLOW"."DEMANDE" ("ID_DEMANDE") DEFERRABLE INITIALLY DEFERRED ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "FK_SER_ENSEIGNANT_GENERIQUE" FOREIGN KEY ("ID_ENSEIGNANT_GENERIQUE")
	  REFERENCES "GRH_PECHE"."ENSEIGNANT_GENERIQUE" ("ID_ENSEIGNANT_GENERIQUE") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "FK_SER_INDIVIDU_ULR" FOREIGN KEY ("NO_INDIVIDU")
	  REFERENCES "GRHUM"."INDIVIDU_ULR" ("NO_INDIVIDU") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "FK_SER_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE" ADD CONSTRAINT "FK_SER_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table SERVICE_DETAIL
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_AP" FOREIGN KEY ("ID_AP")
	  REFERENCES "SCO_SCOLARITE"."COMPOSANT" ("ID_COMPOSANT") DEFERRABLE INITIALLY DEFERRED ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_PERIODE" FOREIGN KEY ("ID_PERIODE")
	  REFERENCES "GRH_PECHE"."PERIODE" ("ID_PERIODE") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_REH" FOREIGN KEY ("ID_REH")
	  REFERENCES "GRH_PECHE"."REH" ("ID_REH") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."SERVICE_DETAIL" ADD CONSTRAINT "FK_SD_SERVICE" FOREIGN KEY ("ID_SERVICE")
	  REFERENCES "GRH_PECHE"."SERVICE" ("ID_SERVICE") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table TAUX_HORAIRE
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" ADD CONSTRAINT "FK_TH_PERSONNE_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."TAUX_HORAIRE" ADD CONSTRAINT "FK_TH_PERSONNE_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table TYPE_FLUX_PAIEMENT
--------------------------------------------------------

  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" ADD CONSTRAINT "FK_FLUX_PAIEM_ID_CREATION" FOREIGN KEY ("PERS_ID_CREATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;
 
  ALTER TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" ADD CONSTRAINT "FK_FLUX_PAIEM_ID_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION")
	  REFERENCES "GRHUM"."PERSONNE" ("PERS_ID") ENABLE;


--------------------------------------------------------
--  DDL for View V_PERSONNEL_ACTUEL_ENS
--------------------------------------------------------

------------------------------V20140324.165444__DDL_Vue_Vacataires.sql------------------------------
-- ---------------------------------------------------------------
-- Ajout des vacataires (suite modification Mangue)
-- ---------------------------------------------------------------


CREATE OR REPLACE FORCE VIEW GRH_PECHE.V_PERSONNEL_ACTUEL_ENS (
  NO_DOSSIER_PERS, TEM_STATUTAIRE, TEM_TITULAIRE)
AS
  SELECT
     c.no_dossier_pers,
    'O',
    'O'
  FROM
    MANGUE.carriere c,
    MANGUE.element_carriere ec,
    MANGUE.changement_position cp,
    GRHUM.corps co,
    GRHUM.type_population tp
  WHERE
    (
      ec.d_effet_element <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        ec.d_fin_element  IS NULL
      OR ec.d_fin_element >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
        'dd/mm/YYYY')
      )
    )
  AND ec.tem_valide = 'O'
  AND
    (
      tp.tem_enseignant = 'O'
    OR tp.tem_2degre    = 'O'
    OR tp.tem_ens_sup   = 'O'
    )    
  AND cp.c_position = 'ACTI'
  AND cp.tem_valide = 'O'
  AND c.no_seq_carriere = cp.carriere_accueil
  AND c.no_dossier_pers = cp.no_dossier_pers
  AND c.no_seq_carriere = ec.no_seq_carriere
  AND c.no_dossier_pers = ec.no_dossier_pers  
  AND ec.c_corps       = co.c_corps
  AND co.c_type_corps = tp.c_type_population
  UNION
  -- inclusion des contrats enseignant au niveau réglementaire
  SELECT
    c.no_dossier_pers,
    'O',
    'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct
  WHERE
  (
    c.d_deb_contrat_trav <= SYSDATE
    AND
    (
      (
        c.d_fin_anticipee   IS NOT NULL
        AND c.d_fin_anticipee >= SYSDATE
      )
      OR
      (
        c.d_fin_anticipee IS NULL
        AND
        (
          c.d_fin_contrat_trav IS NULL
          OR c.d_fin_contrat_trav >= SYSDATE
        )
      )
    )
  )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND tct.tem_invite_associe          != 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- inclusion des contrats doctorant enseignant
  SELECT
    c.no_dossier_pers, 
    'O',
    'N'
  FROM
    MANGUE.CONTRAT_AVENANT ca,
    MANGUE.CONTRAT c
  WHERE 
  (
    c.d_deb_contrat_trav <= SYSDATE
    AND
    (
      (
        c.d_fin_anticipee   IS NOT NULL
        AND c.d_fin_anticipee >= SYSDATE
      )
      OR
      (
        c.d_fin_anticipee IS NULL
        AND
        (
          c.d_fin_contrat_trav IS NULL
          OR c.d_fin_contrat_trav >= SYSDATE
        )
      )
    )
  ) 
  AND c.tem_annulation = 'N'
  AND c.c_type_contrat_trav = 'DO'
  AND ca.c_grade = '6904'
  AND c.no_seq_contrat   = ca.no_seq_contrat
  UNION
  -- inclusion des contrats vacataires typés enseignant
  SELECT
    v.no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.VACATAIRES v
  WHERE
    v.tem_enseignant = 'O'
    AND v.tem_valide  = 'O'
  AND
  (
    v.d_deb_vacation <= SYSDATE
    AND
    (
       v.d_fin_vacation IS NULL
       OR v.d_fin_vacation >= SYSDATE
    )
  )
  UNION
  -- Prise en compte des Professeurs émérites
  SELECT
    no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.emeritat E
  WHERE
    E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      E.D_FIN_EMERITAT  IS NULL
    OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    );

