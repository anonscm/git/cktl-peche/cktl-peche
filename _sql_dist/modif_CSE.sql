-- 11/04 : Modification des CV : ajout du circuit pour le prévisionnel des vacataires + renommer certains circuits
delete from GRH_PECHE.REPART_ENSEIGNANT;
delete from GRH_PECHE.FICHE_VOEUX_DETAIL;
delete from GRH_PECHE.FICHE_VOEUX;
delete from GRH_PECHE.service_detail;
delete from GRH_PECHE.service;
delete from GRH_PECHE.historique_demande;
delete from GRH_PECHE.DEMANDE;
delete from GRH_PECHE.CHEMIN;
delete from GRH_PECHE.ETAPE;
delete from GRH_PECHE.CIRCUIT_VALIDATION;

ALTER TABLE GRH_PECHE.CIRCUIT_VALIDATION MODIFY (C_CIRCUIT_VALIDATION	VARCHAR2(30));

Passer le script "/_sql_dist/0.2.0.0/2013042302_SQL_GRH_PECHE_V0.2.0.0_DML.sql"
Passer le script "/_sql_dist/0.2.0.0/2013042306_SQL_GRH_PECHE_V0.2.0.0_DML.sql"

--DELETE FROM GRHUM.GD_FONCTION WHERE FON_ID_INTERNE = 'PARAM_ENS_GENERIQUES';
