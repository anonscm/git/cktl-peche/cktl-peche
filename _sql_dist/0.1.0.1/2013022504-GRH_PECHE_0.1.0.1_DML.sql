--
-- Patch DML de GRH_PECHE du 22/02/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 4/4
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 0.1.0.0
-- Date de publication : 22/02/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

DECLARE
    compteur NUMBER;
    idDomaine NUMBER;
    idApplication NUMBER;
BEGIN
	--
	-- DB_VERSION
	--
	INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.1.0.1', TO_DATE('27/03/2013', 'DD/MM/YYYY'),NULL,'Version Beta du user GRH_PECHE');

	-- GD_DOMAINE
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	IF (compteur = 0) THEN
		idDomaine := GRHUM.GD_DOMAINE_SEQ.NEXTVAL;
		INSERT INTO GRHUM.GD_DOMAINE (DOM_ID, DOM_LC) VALUES (idDomaine, 'RH');
	ELSE
		SELECT DOM_ID INTO idDomaine FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	END IF;
	
	-- GD_APPLICATION
	idApplication := GRHUM.GD_APPLICATION_SEQ.NEXTVAL;
	INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID)
	     VALUES (idApplication, idDomaine, 'PECHE', 'PECHE');

	-- GD_FONCTION
	-- -- Catégorie 'Accès Peche'
    --    -- Fonction 'Accès à l'application Peche'
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Accès Peche', 'PECHE', 'Accès à l''application Peche', 'Accès à l''application PECHE');

    -- -- Catégorie 'Enseignants'
    --    -- Fonction 'Gestion des enseignants'
    --    -- Fonction 'Répartition croisée'
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignants', 'ENSEIGNANTS', 'Enseignants', 'Enseignants');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignants', 'REPARTITION_CROISEE', 'Répartition croisée', 'Répartition croisée');

    -- -- Catégorie 'Enseignements'
    --    -- Fonction 'Offre de formation'
    --    -- Fonction 'Gestion des UE'
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignements', 'OFFRE_FORMATION', 'Offre de formation', 'Offre de formation');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignements', 'UE', 'Gestion des UE', 'Gestion des UE');
	
    -- -- Catégorie 'Fiches et validation'
    --    -- Fonction 'Gérer les fiches'
    --    -- Fonction 'Valider une fiche en tant que DRH'
    --    -- Fonction 'Valider une fiche en tant que président'
    --    -- Fonction 'Valider une fiche en tant qu'enseignant'
    --    -- Fonction 'Valider une fiche en tant que directeur de composante'
    --    -- Fonction 'Valider une fiche en tant que répartiteur'
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
	     VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'GERER_FICHE', 'Gérer les fiches', 'Gérer les fiches');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'VALID_DRH', 'Valider une fiche en tant que DRH', 'Valider une fiche en tant que DRH');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'VALID_PRESIDENT', 'Valider une fiche en tant que président', 'Valider une fiche en tant que président');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'VALID_ENSEIGNANT', 'Valider une fiche en tant qu''enseignant', 'Valider une fiche en tant qu''enseignant');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'VALID_DIRECTEUR', 'Valider une fiche en tant que directeur de comp.', 'Valider une fiche en tant que directeur de composante');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Fiches et validation', 'VALID_REPARTITEUR', 'Valider une fiche en tant que répartiteur', 'Valider une fiche en tant que répartiteur');

    -- -- Catégorie 'Paramétrages'
    --    -- Fonction 'Paramétrages - Etablissement'
    --    -- Fonction 'Paramétrages - Enseignants génériques'
    --    -- Fonction 'Paramétrages - Définition des circuits de validation'
    --    -- Fonction 'HETD - Liste des types d'horaire'
    --    -- Fonction 'HETD - Liste des paramètres des populations'
    --    -- Fonction 'REH - Liste des activités'
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_ETABLISSEMENT', 'Etablissement', 'Paramétrer l''établissement');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_ENS_GENERIQUES', 'Enseignants génériques', 'Gérer les enseignants génériques');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_CIRCUITS_VAL', 'Circuits de validation', 'Paramétrer les circuits de validation');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_TYPES_HORAIRES', 'HETD - Types d''horaires', 'HETD - Types d''horaires');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_P_POPULATIONS', 'HETD - Paramètres des populations', 'HETD - Paramètres des populations');
	INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_ACTIVITES', 'REH - Activités', 'REH - Paramétrer les activités');

	--
	-- DB_VERSION
	--
	UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='0.1.0.1';
	
	COMMIT;
END;
/
