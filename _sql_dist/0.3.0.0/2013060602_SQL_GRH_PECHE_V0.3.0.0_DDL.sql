--
-- Patch DDL de GRH_PECHE du 06/06/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/3
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.3.0.0
-- Date de publication : 06/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
--  V_PERSONNEL_ACTUEL_ENS
--
CREATE OR REPLACE VIEW GRH_PECHE.V_PERSONNEL_ACTUEL_ENS (NO_DOSSIER_PERS, TEM_STATUTAIRE, TEM_TITULAIRE)
AS
  --Enseignant statutaire titulaire 
  SELECT
    c.no_dossier_pers, 'O', 'O'
  FROM
    MANGUE.element_carriere c,
    GRHUM.corps co,
    GRHUM.type_population tp
  WHERE
    (
      c.d_effet_element <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND
      (
        c.d_fin_element  IS NULL
      OR c.d_fin_element >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND c.tem_valide    = 'O'
  AND
    (
      tp.tem_enseignant = 'O'
    OR tp.tem_2degre    = 'O'
    OR tp.tem_ens_sup   = 'O'
    )
  AND c.c_corps       = co.c_corps
  AND co.c_type_corps = tp.c_type_population
  UNION
  -- inclusion des contrats enseignant au niveau réglementaire
  SELECT
    c.no_dossier_pers, 'O', 'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct
  WHERE
    (
      c.d_deb_contrat_trav <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        c.d_fin_contrat_trav                  IS NULL
      OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >= TO_DATE (TO_CHAR (SYSDATE,
        'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND tct.tem_invite_associe          != 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- inclusion des contrats vacataires typés enseignant
  SELECT
    c.no_dossier_pers, 'N', 'N'
  FROM
    MANGUE.CONTRAT_VACATAIRES v,
    MANGUE.CONTRAT c ,
    GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE
     v.tem_enseignant='O'
  AND
    (
      c.d_deb_contrat_trav   <= SYSDATE
    AND ( c.d_fin_contrat_trav IS NULL
          OR c.d_fin_contrat_trav  >= SYSDATE
        )
    )
  AND c.tem_annulation                 = 'N'
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O'
  AND c.no_seq_contrat  = v.no_seq_contrat
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- Prise en compte des Professeurs émérites
  SELECT
    no_dossier_pers, 'N', 'N'
  FROM
    MANGUE.emeritat E
  WHERE
    E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      E.D_FIN_EMERITAT  IS NULL
      OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    );

--
-- Calcul HCOMP
--
    
alter table GRH_PECHE.PARAM_POP_HETD drop constraint PPH_TCT_CORPS_NULLS;

alter table GRH_PECHE.REPART_METHODE_CORPS drop constraint PK_REPART_METHODE_CORPS;
alter table GRH_PECHE.REPART_METHODE_CORPS add (ID_REPART_METHODE_CORPS number not null);
alter table GRH_PECHE.REPART_METHODE_CORPS add constraint PK_REPART_METHODE_CORPS primary key (ID_REPART_METHODE_CORPS) using index tablespace GRH_INDX;
alter table GRH_PECHE.REPART_METHODE_CORPS add constraint UK_REPART_METHODE_CORPS_CORPS unique (C_CORPS);
create sequence GRH_PECHE.REPART_METHODE_CORPS_SEQ start with 1 increment by 1 minvalue 1 nocycle noorder;

-- UE FLOTTANTES
-- REPART_UEF_ETABLISSEMENT
--
CREATE TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT
(
  ID_REPART_UEF_ETAB	NUMBER NOT NULL ,
  ID_UE			NUMBER 	NOT NULL ,
  ETABLISSEMENT_EXTERNE	VARCHAR2(1024) NOT NULL ,
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER 
);

ALTER TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT ADD CONSTRAINT PK_REPART_UEF_ETABLISSEMENT PRIMARY KEY(ID_REPART_UEF_ETAB) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT ADD CONSTRAINT FK_RUE_UE FOREIGN KEY (ID_UE) 
REFERENCES SCO_SCOLARITE.UE (ID_UE) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT ADD CONSTRAINT FK_RUE_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT ADD CONSTRAINT FK_RUE_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);

CREATE INDEX GRH_PECHE.IDX_RUE_ID_UE ON GRH_PECHE.REPART_UEF_ETABLISSEMENT(ID_UE) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.REPART_UEF_ETABLISSEMENT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.REPART_UEF_ETABLISSEMENT IS 'Table de répartition des UE flottantes (taguées PECHE) et d''un établissement d''enseignement externe';    
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.ID_REPART_UEF_ETAB IS 'Clef primaire de la table REPART_UEF_ETABLISSEMENT';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.ID_UE IS 'Référence à une UE flottante présente dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.UE.ID)';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.ETABLISSEMENT_EXTERNE IS 'Etablissement externe où se déroulera l''enseignement';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_UEF_ETABLISSEMENT.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

