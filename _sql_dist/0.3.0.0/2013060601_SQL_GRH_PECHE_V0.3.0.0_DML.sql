--
-- Patch DML de GRH_PECHE du 06/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/3
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.3.0.0
-- Date de publication : 06/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Nouveau droit
INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
         VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (SELECT APP_ID FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE') , 'Enseignants', 'PARAM_ABSENCE', 'Enseignants - Saisie des heures d''absence', 'Enseignants - Paramétrer le nombre d''heures des absences');

--Validation
commit;
