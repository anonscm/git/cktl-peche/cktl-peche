--
-- Patch DDL de GRH_PECHE du 06/06/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 3/3
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.3.0.0
-- Date de publication : 06/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.3.0.0', TO_DATE('06/06/2013', 'DD/MM/YYYY'),NULL,'Version Beta 3 du user GRH_PECHE');

-- HComp
update GRH_PECHE.METHODES_CALCUL_HCOMP set CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorite' where CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisation';
delete from GRH_PECHE.METHODES_CALCUL_HCOMP where CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisationTP';

DECLARE
	persid_createur integer;
	flag integer;
BEGIN
	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	UPDATE GRH_PECHE.METHODES_CALCUL_HCOMP
	   SET LL_METHODE = 'Priorité CM/TD/TP',
	       CLASSE_METHODE = 'org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteCmTdTp',
	       D_MODIFICATION = SYSDATE,
	       PERS_ID_MODIFICATION = persid_createur
	   WHERE METHODE_ID = 2;
	
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP
	           (METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION)
	    VALUES (3, 'Priorité TP/TD/CM', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPrioriteTpTdCm', SYSDATE, persid_createur, SYSDATE, persid_createur);
END;
/

-- Année 2013
UPDATE GRH_PECHE.SERVICE SET ANNEE = 2013 WHERE ANNEE = 2012;

--
-- DB_VERSION
--
UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE = '0.3.0.0';

--Validation
commit;
