--
-- Patch DML de GRH_PECHE du 23/04/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 6/7
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--Passez ces drop avant de faire les insertions pour repartir de 1
DROP SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ;
DROP SEQUENCE GRH_PECHE.ETAPE_SEQ;
DROP SEQUENCE GRH_PECHE.CHEMIN_SEQ;
CREATE SEQUENCE GRH_PECHE.CIRCUIT_VALIDATION_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.ETAPE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;
CREATE SEQUENCE GRH_PECHE.CHEMIN_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

DECLARE
	persid_createur integer;
	flag integer;
	id_app number(12,0);
	id_circuit number;
	id_etape_depart number;
	id_etape_arrivee number;
	id_chemin number;
BEGIN
	--
	-- DB_VERSION
	--
	INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.2.0.0', TO_DATE('23/04/2013', 'DD/MM/YYYY'),NULL,'Version Beta 2 du user GRH_PECHE');
	
 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	-- Application
	-- ***********
	SELECT APP_ID INTO id_app FROM GRHUM.GD_APPLICATION WHERE APP_STR_ID = 'PECHE';
	
	-- Circuit 'PECHE_PREV_STATUTAIRE'
	-- ****************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_STATUTAIRE', 1, 'Circuit prévisionnel pour les statutaires : REPARTITEUR, DIRECTEUR COMPOSANTE, ENSEIGNANT, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
  	

	-- Circuit 'PECHE_PREV_VACATAIRE'
	-- ****************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_PREV_VACATAIRE', 1, 'Circuit prévisionnel pour les vacataires : REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
   	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');

	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');



	-- Circuit 'PECHE STATUTAIRE'
	-- *************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_STATUTAIRE', 1, 'Circuit pour les statutaires : REPARTITEUR, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
  
  -- Etape non branchée (optionnelle)
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_depart, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, null, 'REFUSER', 'Refuser');
    
	-- Circuit 'PECHE_VACATAIRE'
	-- *************************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VACATAIRE', 1, 'Circuit où la DRH est obligatoire : REPARTITEUR, DRH, ENSEIGNANT, DIRECTEUR COMPOSANTE, PRESIDENT', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DRH', 'Validation par la direction des ressoures humaines', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_DIR_COMPOSANTE', 'Validation par le directeur de la composante', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_PRESIDENT', 'Validation par le président', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin,  id_etape_arrivee, id_etape_depart, 'REFUSER', 'Refuser');

	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	-- Circuit 'PECHE_VOEUX'
	-- *********************
	SELECT GRH_PECHE.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit FROM DUAL;
	Insert into GRH_PECHE.CIRCUIT_VALIDATION(ID_CIRCUIT_VALIDATION, C_CIRCUIT_VALIDATION, NUMERO_VERSION, LL_CIRCUIT_VALIDATION, ID_APP, TEM_UTILISABLE, PERS_ID_MODIFICATION)
    values (id_circuit, 'PECHE_VOEUX', 1, 'Circuit de la fiche de vœux : ENSEIGNANT, REPARTITEUR', id_app, 'O', persid_createur);

    -- Etapes
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_depart FROM DUAL;
  	Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION, TEM_ETAPE_INITIALE) values (id_etape_depart, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', id_circuit, 'O');
	SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALID_REPARTITEUR', 'Validation par le répartiteur', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
    
	id_etape_depart := id_etape_arrivee;
    SELECT GRH_PECHE.ETAPE_SEQ.NEXTVAL INTO id_etape_arrivee FROM DUAL;
    Insert into GRH_PECHE.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_arrivee, 'VALIDEE', 'Fiche validée', id_circuit);
	SELECT GRH_PECHE.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
	Insert into GRH_PECHE.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_depart, id_etape_arrivee, 'VALIDER', 'Valider');
	
	--Validation
	commit;
END;
/

-- METHODES_CALCUL_HCOMP

DECLARE
	persid_createur integer;
	flag integer;
	now timestamp;
BEGIN
	 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	select SYSDATE into now from dual;
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(1, 'Proratisation', 'org.cocktail.peche.metier.hce.MethodeDeCalculParProratisation', now, persid_createur, now, persid_createur);
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(2, 'Priorité CM/TP/TD', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisation', now, persid_createur, now, persid_createur);
	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP(METHODE_ID, LL_METHODE, CLASSE_METHODE, D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) VALUES(3, 'Priorité TP/CM/TD', 'org.cocktail.peche.metier.hce.MethodeDeCalculParPriorisationTP', now, persid_createur, now, persid_createur);

	--
	-- DB_VERSION
	--
	UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = now WHERE DBV_LIBELLE = '0.2.0.0';
	
	--Validation
	commit;
END;
/
