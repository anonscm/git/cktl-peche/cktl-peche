--
-- Patch DML de GRHUM du 12/03/2013 à exécuter depuis le user GRHUM
-- Affectation des enseignants à une structure et un rôle dans cette structure
--
-- Rem : fichier encodé UTF-8
--

--
--
-- Fichier : 7/7
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--


SET DEFINE OFF;
-- Instructions :
--    Avant d'exécuter le script, vous devez indiquez sur quelle
--    structure seront affectés les enseignants.
--    Pour cela, merci de modifier ce script en alimentant
--    la variable 'codeStructure' avec un code de structure (ne pas
--    confondre avec le persId).
--    
--    Par exemple, pour affecter les enseignants sur la structure ayant
--    le code '54', modifiez la ligne 36 comme ceci :
--      codeStructure VARCHAR2(10) := '54';

WHENEVER SQLERROR EXIT SQL.SQLCODE;

DECLARE
 codeStructure VARCHAR2(10) := NULL;
 assId NUMBER;
 persIdCreateur INTEGER;
 flag INTEGER;
BEGIN

-- Préparations
-- ------------

-- Recherche du rôle 'Enseignant(e)'
-- Récupérer le ASS_ID
SELECT COUNT(*) INTO flag FROM GRHUM.ASSOCIATION
 INNER JOIN GRHUM.TYPE_ASSOCIATION ON (TYPE_ASSOCIATION.TAS_ID = ASSOCIATION.TAS_ID)
 WHERE TAS_CODE = 'ROLE'
   AND ASS_CODE = 'ENSEIGNANT(E';
   
IF (flag <> 1) THEN
    RAISE_APPLICATION_ERROR(-20000, 'Impossible de déterminer le rôle enseignant(e) (' || flag || ' trouvé)');
END IF;

SELECT ASS_ID INTO assId FROM GRHUM.ASSOCIATION
 INNER JOIN GRHUM.TYPE_ASSOCIATION ON (TYPE_ASSOCIATION.TAS_ID = ASSOCIATION.TAS_ID)
 WHERE TAS_CODE = 'ROLE'
   AND ASS_CODE = 'ENSEIGNANT(E';

-- Message si un code structure n'est pas fourni
IF (codeStructure IS NULL) THEN
	RAISE_APPLICATION_ERROR(-20000, 'Vous devez indiquer un code structure. Voir le paragraphe ''Instructions'' en début de script.');
ELSE
	-- Vérifier si le code structure fourni existe
	SELECT COUNT(*) INTO flag FROM GRHUM.STRUCTURE_ULR
	 INNER JOIN GRHUM.TYPE_STRUCTURE ON (TYPE_STRUCTURE.C_TYPE_STRUCTURE = STRUCTURE_ULR.C_TYPE_STRUCTURE)
	   WHERE STRUCTURE_ULR.C_STRUCTURE = codeStructure;

	IF (flag <> 1) THEN
		RAISE_APPLICATION_ERROR(-20000, 'La structure ayant le code ''' || codeStructure || ''' n''existe pas.');
	END IF;
END IF;

-- Récupération d'un créateur
SELECT COUNT(*) INTO flag FROM (SELECT PARAM_VALUE FROM (
SELECT * FROM GRHUM_PARAMETRES WHERE PARAM_KEY = 'GRHUM_CREATEUR' ORDER BY PARAM_ORDRE DESC)
WHERE ROWNUM = 1) X, GRHUM.COMPTE C WHERE X.PARAM_VALUE = C.CPT_LOGIN;

IF (flag = 0) THEN
    RAISE_APPLICATION_ERROR(-20000, 'Impossible de récupérer le pers_id d''un GRHUM_CREATEUR');
END IF;

SELECT C.PERS_ID INTO persIdCreateur FROM (SELECT PARAM_VALUE FROM (
SELECT * FROM GRHUM_PARAMETRES WHERE PARAM_KEY = 'GRHUM_CREATEUR' ORDER BY PARAM_ORDRE DESC)
WHERE ROWNUM = 1) X, GRHUM.COMPTE C WHERE X.PARAM_VALUE = C.CPT_LOGIN;

-----------------------------
-- Log
SYS.dbms_output.put_line('assId : ' || assId);
SYS.dbms_output.put_line('codeStructure : ' || codeStructure);
SYS.dbms_output.put_line('persIdCreateur : ' || persIdCreateur);
-----------------------------

-- Insertions
-- ----------

-- Création du lien Enseignant <-> Structure
-- On ajoute tous les enseignants qui ne sont pas déjà dans la structures
INSERT INTO GRHUM.REPART_STRUCTURE
    (C_STRUCTURE, PERS_ID, D_CREATION, PERS_ID_CREATION, PERS_ID_MODIFICATION, D_MODIFICATION)
    (SELECT codeStructure, PERS_ID, SYSDATE, persIdCreateur, persIdCreateur, SYSDATE
      FROM (SELECT DISTINCT(PERS_ID)
       	    FROM GRH_PECHE.V_PERSONNEL_ACTUEL_ENS
      	    INNER JOIN GRHUM.INDIVIDU_ULR ON (INDIVIDU_ULR.NO_INDIVIDU = V_PERSONNEL_ACTUEL_ENS.NO_DOSSIER_PERS)
      	    WHERE INDIVIDU_ULR.PERS_ID NOT IN (SELECT PERS_ID FROM GRHUM.REPART_STRUCTURE WHERE C_STRUCTURE = codeStructure)
      	   )
    );

-- Création du lien Enseignant <-> Structure <-> Association
-- On ajoute tous les enseignants qui n'ont pas déjà dans le rôle dans la structure à la date d'aujourd'hui
INSERT INTO GRHUM.REPART_ASSOCIATION 
    (D_MODIFICATION, RAS_QUOTITE, D_CREATION, PERS_ID_CREATION, RAS_D_OUVERTURE, RAS_RANG, PERS_ID_MODIFICATION, RAS_D_FERMETURE, C_STRUCTURE, PERS_ID, RAS_COMMENTAIRE, ASS_ID, RAS_ID)
    (SELECT SYSDATE, NULL, SYSDATE, persIdCreateur, NULL, 1, persIdCreateur, NULL, codeStructure, PERS_ID, NULL, assId, GRHUM.REPART_ASSOCIATION_SEQ.NEXTVAL
       FROM (SELECT DISTINCT(PERS_ID)
       	     FROM GRH_PECHE.V_PERSONNEL_ACTUEL_ENS
     	     INNER JOIN GRHUM.INDIVIDU_ULR ON (INDIVIDU_ULR.NO_INDIVIDU = V_PERSONNEL_ACTUEL_ENS.NO_DOSSIER_PERS)
     	     WHERE INDIVIDU_ULR.PERS_ID NOT IN (SELECT PERS_ID FROM GRHUM.REPART_ASSOCIATION
                                         	 WHERE C_STRUCTURE = codeStructure AND ASS_ID = assId
                                          	  AND (RAS_D_OUVERTURE IS NULL OR RAS_D_OUVERTURE < SYSDATE) 
						  AND (RAS_D_FERMETURE IS NULL OR RAS_D_FERMETURE > SYSDATE))
      	     )
    );

COMMIT;
END;
/