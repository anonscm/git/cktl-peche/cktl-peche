--
-- Patch DML de GRHUM du 23/04/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/7
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 0.2.0.0
-- Date de publication : 23/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

CREATE OR REPLACE PROCEDURE insertFunction(appId IN INTEGER, categorie in VARCHAR2, idInterne IN VARCHAR2, libelle IN VARCHAR2, description IN VARCHAR2)
IS
  compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_FONCTION WHERE APP_ID = appId AND FON_ID_INTERNE = idInterne;
	IF (compteur = 0) THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, appId, categorie, idInterne, libelle, description);
	END IF;
END;
/

DECLARE
    compteur NUMBER;
    idDomaine NUMBER;
    idApplication NUMBER;
BEGIN
	-- GD_DOMAINE
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_DOMAINE_SEQ.NEXTVAL INTO idDomaine FROM DUAL;
		INSERT INTO GRHUM.GD_DOMAINE (DOM_ID, DOM_LC) VALUES (idDomaine, 'RH');
	ELSE
		SELECT DOM_ID INTO idDomaine FROM GRHUM.GD_DOMAINE WHERE DOM_LC = 'RH';
	END IF;
	
	-- GD_APPLICATION
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	IF (compteur = 0) THEN
		SELECT GRHUM.GD_APPLICATION_SEQ.NEXTVAL INTO idApplication FROM DUAL;
		INSERT INTO GRHUM.GD_APPLICATION (APP_ID, DOM_ID, APP_LC, APP_STR_ID) VALUES (idApplication, idDomaine, 'PECHE', 'PECHE');
	ELSE
		SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';
	END IF;

	-- GD_FONCTION
	-- -- Catégorie 'Accès Peche'
    --    -- Fonction 'Accès à l'application Peche'
    insertFunction(idApplication, 'Accès Peche', 'PECHE', 'Accès à l''application Peche', 'Accès à l''application PECHE');

    -- -- Catégorie 'Enseignants'
    --    -- Fonction 'Gestion des enseignants'
    --    -- Fonction 'Répartition croisée'
    insertFunction(idApplication, 'Enseignants', 'ENSEIGNANTS', 'Enseignants', 'Enseignants');
    insertFunction(idApplication, 'Enseignants', 'REPARTITION_CROISEE', 'Répartition croisée', 'Répartition croisée');

    -- -- Catégorie 'Enseignements'
    --    -- Fonction 'Offre de formation'
    --    -- Fonction 'Offre de formation - Nombre de groupe'
    --    -- Fonction 'Gestion des UE'
    insertFunction(idApplication, 'Enseignements', 'OFFRE_FORMATION', 'Offre de formation', 'Offre de formation');
    insertFunction(idApplication, 'Enseignements', 'PARAM_NB_GROUPE', 'Offre de formation - Nombre de groupe', 'Offre de formation - Paramétrer le nombre de groupe');
    insertFunction(idApplication, 'Enseignements', 'UE', 'Gestion des UE', 'Gestion des UE');
	
    -- -- Catégorie 'Fiches et validation'
    --    -- Fonction 'Gérer les fiches'
    --    -- Fonction 'Valider une fiche en tant que DRH'
    --    -- Fonction 'Valider une fiche en tant que président'
    --    -- Fonction 'Valider une fiche en tant qu'enseignant'
    --    -- Fonction 'Valider une fiche en tant que directeur de composante'
    --    -- Fonction 'Valider une fiche en tant que répartiteur'
	insertFunction(idApplication, 'Fiches et validation', 'GERER_FICHE', 'Gérer les fiches', 'Gérer les fiches');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_DRH', 'Valider une fiche en tant que DRH', 'Valider une fiche en tant que DRH');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_PRESIDENT', 'Valider une fiche en tant que président', 'Valider une fiche en tant que président');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_ENSEIGNANT', 'Valider une fiche en tant qu''enseignant', 'Valider une fiche en tant qu''enseignant');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_DIRECTEUR', 'Valider une fiche en tant que directeur de comp.', 'Valider une fiche en tant que directeur de composante');
    insertFunction(idApplication, 'Fiches et validation', 'VALID_REPARTITEUR', 'Valider une fiche en tant que répartiteur', 'Valider une fiche en tant que répartiteur');

    -- -- Catégorie 'Paramétrages'
    --    -- Fonction 'Paramétrages - Etablissement'
    --    -- Fonction 'Paramétrages - Enseignants génériques'
    --    -- Fonction 'Paramétrages - Définition des circuits de validation'
    --    -- Fonction 'HETD - Liste des types d'horaire'
    --    -- Fonction 'HETD - Liste des paramètres des populations'
    --    -- Fonction 'REH - Liste des activités'
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ETABLISSEMENT', 'Etablissement', 'Paramétrer l''établissement');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ENS_GENERIQUES', 'Enseignants génériques', 'Gérer les enseignants génériques');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_CIRCUITS_VAL', 'Circuits de validation', 'Paramétrer les circuits de validation');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_TYPES_HORAIRES', 'HETD - Types d''horaires', 'HETD - Types d''horaires');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_P_POPULATIONS', 'HETD - Paramètres des populations', 'HETD - Paramètres des populations');
    insertFunction(idApplication, 'Paramétrages', 'PARAM_ACTIVITES', 'REH - Activités', 'REH - Paramétrer les activités');

	COMMIT;
END;
/

DROP PROCEDURE insertFunction;
