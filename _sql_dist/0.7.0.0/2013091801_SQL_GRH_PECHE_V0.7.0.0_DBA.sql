--
-- Patch DBA de PECHE du 18/09/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DBA
-- Schema : GRHUM
-- Numero de version : 0.7.0.0
-- Date de publication : 18/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-------------------------------V20130905.172613__DBA_Ajout_Grant.sql--------------------------------
--
-- Grant pour MANGUE
--

GRANT select, references on MANGUE.changement_position to GRH_PECHE;




