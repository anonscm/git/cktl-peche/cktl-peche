--
-- Patch DDL de PECHE du 18/09/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 0.7.0.0
-- Date de publication : 18/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-----------------------V20130905.172659__DDL_Modification_vue_enseignant.sql------------------------
-- -------------------------------------------------------------------
-- Ajout filtre pour remonter que les enseignants statutaires actifs
-- -------------------------------------------------------------------


CREATE OR REPLACE FORCE VIEW GRH_PECHE.V_PERSONNEL_ACTUEL_ENS (
  NO_DOSSIER_PERS, TEM_STATUTAIRE, TEM_TITULAIRE)
AS
  SELECT
     c.no_dossier_pers,
    'O',
    'O'
  FROM
    MANGUE.carriere c,
    MANGUE.element_carriere ec,
    MANGUE.changement_position cp,
    GRHUM.corps co,
    GRHUM.type_population tp
  WHERE
    (
      ec.d_effet_element <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        ec.d_fin_element  IS NULL
      OR ec.d_fin_element >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
        'dd/mm/YYYY')
      )
    )
  AND ec.tem_valide = 'O'
  AND
    (
      tp.tem_enseignant = 'O'
    OR tp.tem_2degre    = 'O'
    OR tp.tem_ens_sup   = 'O'
    )
    
  AND cp.c_position = 'ACTI'
  AND cp.tem_valide = 'O'
  AND c.no_seq_carriere = cp.carriere_accueil
  AND c.no_dossier_pers = cp.no_dossier_pers
  AND c.no_seq_carriere = ec.no_seq_carriere
  AND c.no_dossier_pers = ec.no_dossier_pers  
  AND ec.c_corps       = co.c_corps
  AND co.c_type_corps = tp.c_type_population
  UNION
  -- inclusion des contrats enseignant au niveau réglementaire
  SELECT
    c.no_dossier_pers,
    'O',
    'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct
  WHERE
    (
      c.d_deb_contrat_trav <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        c.d_fin_contrat_trav                  IS NULL
      OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >= TO_DATE (TO_CHAR (SYSDATE,
        'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND tct.tem_invite_associe          != 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- inclusion des contrats vacataires typés enseignant
  SELECT
    c.no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.CONTRAT_VACATAIRES v,
    MANGUE.CONTRAT c ,
    GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE
    v.tem_enseignant='O'
  AND
    (
      c.d_deb_contrat_trav <= SYSDATE
    AND
      (
        c.d_fin_anticipee   IS NOT NULL
      AND c.d_fin_anticipee >= SYSDATE
      )
    OR
      (
        c.d_fin_anticipee IS NULL
      AND
        (
          c.d_fin_contrat_trav  IS NULL
        OR c.d_fin_contrat_trav >= SYSDATE
        )
      )
    )
  AND c.tem_annulation                 = 'N'
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O'
  AND c.no_seq_contrat                 = v.no_seq_contrat
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- Prise en compte des Professeurs émérites
  SELECT
    no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.emeritat E
  WHERE
    E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      E.D_FIN_EMERITAT  IS NULL
    OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    );



