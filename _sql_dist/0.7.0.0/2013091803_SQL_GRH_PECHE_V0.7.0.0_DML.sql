--
-- Patch DML de PECHE du 18/09/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 0.7.0.0
-- Date de publication : 18/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.7.0.0', TO_DATE('18/09/2013', 'DD/MM/YYYY'), SYSDATE, 'Version Beta 7 du User GRH_PECHE');

COMMIT;
