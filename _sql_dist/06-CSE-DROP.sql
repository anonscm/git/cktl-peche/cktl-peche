drop table cse.param_pop_hetd;
drop table cse.service_detail;
drop table cse.service;
drop table cse.enseignant_generique;
drop table cse.reh;
drop table cse.element_maquette;
drop table cse.db_version;

drop SEQUENCE CSE.DB_VERSION_SEQ;
drop SEQUENCE CSE.REH_SEQ;
drop SEQUENCE CSE.SERVICE_SEQ;
drop SEQUENCE CSE.SERVICE_DETAIL_SEQ;
drop SEQUENCE CSE.ENSEIGNANT_GENERIQUE_SEQ;
drop SEQUENCE CSE.PARAM_POP_HETD_SEQ;
