--
-- Patch DBA de PECHE du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DBA
-- Schema : GRHUM
-- Numero de version : 1.1.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Equipe PECHE
-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


----------------------------V20131008.120505__DBA_Migration_Workflow.sql----------------------------
--
-- Grant pour utilisation WORKFLOW dans PECHE
--

GRANT select, references on WORKFLOW.DEMANDE to GRH_PECHE;




