--
-- Patch DDL de PECHE du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.1.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Equipe PECHE
-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


----------------------------V20131008.120517__DDL_Migration_Workflow.sql----------------------------


-- modification WORKFLOW 0.0.0.2 au cas où
ALTER TABLE WORKFLOW.CIRCUIT_VALIDATION modify (C_CIRCUIT_VALIDATION VARCHAR2(30));

-- création des tables temporaires de correspondance

CREATE TABLE GRH_PECHE.CHEMIN_MIGRATION(ID_CHEMIN_OLD NUMBER,ID_CHEMIN_NEW NUMBER);
CREATE TABLE GRH_PECHE.CIRCUIT_VALIDATION_MIGRATION(ID_CIRCUIT_VALIDATION_OLD NUMBER,ID_CIRCUIT_VALIDATION_NEW NUMBER);
CREATE TABLE GRH_PECHE.DEMANDE_MIGRATION(ID_DEMANDE_OLD NUMBER,ID_DEMANDE_NEW NUMBER);
CREATE TABLE GRH_PECHE.ETAPE_MIGRATION(ID_ETAPE_OLD NUMBER,ID_ETAPE_NEW NUMBER);
CREATE TABLE GRH_PECHE.HISTORIQUE_DEMANDE_MIGRATION(ID_HISTORIQUE_DEMANDE_OLD NUMBER,ID_HISTORIQUE_DEMANDE_NEW NUMBER);

create or replace PROCEDURE  GRHUM.MIGRATION_WORKFLOW IS

  id_circuit_new number;  
  id_etape_new number;  
  id_chemin_new number;  
  id_demande_new number;  
  id_historique_new number;
  id_etape_depart_new number;
  id_etape_arrivee_new number;
  id_circuit_validation_new number;
  
  CURSOR c_circuits IS SELECT * from GRH_PECHE.CIRCUIT_VALIDATION ;  
  le_circuit GRH_PECHE.CIRCUIT_VALIDATION%ROWTYPE;

  CURSOR c_etapes IS SELECT * from GRH_PECHE.ETAPE ;  
  l_etape GRH_PECHE.ETAPE%ROWTYPE;

  CURSOR c_chemins IS SELECT * from GRH_PECHE.CHEMIN ;  
  le_chemin GRH_PECHE.CHEMIN%ROWTYPE;

  CURSOR c_demandes IS SELECT * from GRH_PECHE.DEMANDE ;  
  la_demande GRH_PECHE.DEMANDE%ROWTYPE;

  CURSOR c_historiques IS SELECT * from GRH_PECHE.HISTORIQUE_DEMANDE ;  
  l_historique GRH_PECHE.HISTORIQUE_DEMANDE%ROWTYPE;

  CURSOR c_voeux IS SELECT * from GRH_PECHE.FICHE_VOEUX ;  
  le_voeux GRH_PECHE.FICHE_VOEUX%ROWTYPE;

  CURSOR c_services IS SELECT * from GRH_PECHE.SERVICE ;  
  le_service GRH_PECHE.SERVICE%ROWTYPE;
    
BEGIN

-- alimentation des tables 

FOR le_circuit IN c_circuits
LOOP
 SELECT WORKFLOW.CIRCUIT_VALIDATION_SEQ.NEXTVAL INTO id_circuit_new FROM DUAL;
 INSERT INTO GRH_PECHE.CIRCUIT_VALIDATION_MIGRATION(id_circuit_validation_old,id_circuit_validation_new) VALUES(le_circuit.id_circuit_validation,id_circuit_new);  
END LOOP;

FOR l_etape IN c_etapes
LOOP
 SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_new FROM DUAL;    
 INSERT INTO GRH_PECHE.etape_migration(id_etape_old,id_etape_new) VALUES(l_etape.id_etape,id_etape_new);  
END LOOP;

FOR le_chemin IN c_chemins
LOOP
 SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin_new FROM DUAL;    
 INSERT INTO GRH_PECHE.chemin_migration(id_chemin_old,id_chemin_new) VALUES(le_chemin.id_chemin,id_chemin_new);  
END LOOP;

FOR la_demande IN c_demandes
LOOP
 SELECT WORKFLOW.DEMANDE_SEQ.NEXTVAL INTO id_demande_new FROM DUAL;    
 INSERT INTO GRH_PECHE.demande_migration(id_demande_old,id_demande_new) VALUES(la_demande.id_demande,id_demande_new);  
END LOOP;

FOR l_historique IN c_historiques
LOOP
 SELECT WORKFLOW.HISTORIQUE_DEMANDE_SEQ.NEXTVAL INTO id_historique_new FROM DUAL;
 INSERT INTO GRH_PECHE.historique_demande_migration(id_historique_demande_old,id_historique_demande_new) VALUES(l_historique.id_historique_demande,id_historique_new);  
END LOOP;

-- copie des données 

FOR le_chemin IN c_chemins
LOOP
SELECT id_chemin_new INTO id_chemin_new FROM GRH_PECHE.chemin_migration WHERE id_chemin_old=le_chemin.id_chemin;
SELECT id_etape_new INTO id_etape_depart_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=le_chemin.id_etape_depart;

if le_chemin.id_etape_arrivee is not null then 
  SELECT id_etape_new INTO id_etape_arrivee_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=le_chemin.id_etape_arrivee;
else
 id_etape_arrivee_new := null;
end if;

INSERT INTO WORKFLOW.CHEMIN(id_chemin,id_etape_arrivee,id_etape_depart,c_type_chemin,lc_type_chemin) VALUES (id_chemin_new,id_etape_arrivee_new,id_etape_depart_new,le_chemin.c_type_chemin,le_chemin.lc_type_chemin);  
END LOOP;

FOR le_circuit IN c_circuits
LOOP
SELECT id_circuit_validation_new INTO id_circuit_validation_new FROM GRH_PECHE.circuit_validation_migration WHERE id_circuit_validation_old=le_circuit.id_circuit_validation;

INSERT INTO WORKFLOW.CIRCUIT_VALIDATION(id_circuit_validation , d_modification , id_app , c_circuit_validation , ll_circuit_validation , numero_version , pers_id_modification , tem_utilisable) VALUES
(id_circuit_validation_new , le_circuit.d_modification , le_circuit.id_app , le_circuit.c_circuit_validation , le_circuit.ll_circuit_validation , le_circuit.numero_version , le_circuit.pers_id_modification , le_circuit.tem_utilisable);
END LOOP;

FOR la_demande IN c_demandes
LOOP

SELECT id_demande_new INTO id_demande_new FROM GRH_PECHE.demande_migration WHERE id_demande_old=la_demande.id_demande;
SELECT id_etape_new INTO id_etape_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=la_demande.id_etape;

INSERT INTO WORKFLOW.DEMANDE(ID_DEMANDE,ID_ETAPE) VALUES (id_demande_new,id_etape_new);

END LOOP;

FOR l_etape IN c_etapes
LOOP

SELECT id_etape_new INTO id_etape_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=l_etape.id_etape;
SELECT id_circuit_validation_new INTO id_circuit_validation_new FROM GRH_PECHE.circuit_validation_migration WHERE id_circuit_validation_old=l_etape.id_circuit_validation;

INSERT INTO WORKFLOW.ETAPE(ID_ETAPE,C_ETAPE,LL_ETAPE,ID_CIRCUIT_VALIDATION,TEM_ETAPE_INITIALE) VALUES (id_etape_new,l_etape.C_ETAPE,l_etape.LL_ETAPE,id_circuit_validation_new,l_etape.TEM_ETAPE_INITIALE);

END LOOP;

FOR l_historique IN c_historiques
LOOP

SELECT id_historique_demande_new INTO id_historique_new FROM GRH_PECHE.historique_demande_migration WHERE id_historique_demande_old=l_historique.id_historique_demande;
SELECT id_demande_new INTO id_demande_new FROM GRH_PECHE.demande_migration WHERE id_demande_old=l_historique.id_demande;

if l_historique.id_etape_depart is not null then 
  SELECT id_etape_new INTO id_etape_depart_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=l_historique.id_etape_depart;
else
 id_etape_depart_new := null;
end if;

SELECT id_etape_new INTO id_etape_arrivee_new FROM GRH_PECHE.etape_migration WHERE id_etape_old=l_historique.id_etape_arrivee;

INSERT INTO WORKFLOW.HISTORIQUE_DEMANDE(ID_HISTORIQUE_DEMANDE,ID_DEMANDE,ID_ETAPE_DEPART,ID_ETAPE_ARRIVEE,PERS_ID_MODIFICATION,D_MODIFICATION) VALUES
(id_historique_new,id_demande_new,id_etape_depart_new,id_etape_arrivee_new,l_historique.pers_id_modification,l_historique.d_modification);

END LOOP;


-- modifications des tables PECHE

FOR le_voeux IN c_voeux
LOOP

SELECT id_demande_new INTO id_demande_new FROM GRH_PECHE.demande_migration WHERE id_demande_old=le_voeux.id_demande;
UPDATE GRH_PECHE.FICHE_VOEUX SET id_demande = id_demande_new WHERE id_demande = le_voeux.id_demande ;

END LOOP;

FOR le_service IN c_services
LOOP

if le_service.id_demande is not null then
  SELECT id_demande_new INTO id_demande_new FROM GRH_PECHE.demande_migration WHERE id_demande_old=le_service.id_demande;
  UPDATE GRH_PECHE.SERVICE SET id_demande = id_demande_new WHERE id_demande = le_service.id_demande ;
end if;

END LOOP;

commit;

END;
/

-- désactivation des contraintes le temps de la copie

alter table WORKFLOW.CHEMIN disable constraint FK_CHEMIN_ETAPE_ARRIVEE;
alter table WORKFLOW.CHEMIN disable constraint FK_CHEMIN_ETAPE_DEPART;
alter table WORKFLOW.CIRCUIT_VALIDATION disable constraint FK_CIRCUIT_VALID_GD_APPLI;
alter table WORKFLOW.DEMANDE disable constraint FK_DEMANDE_ID_ETAPE;
alter table WORKFLOW.ETAPE disable constraint FK_ETA_CIRCUIT_VALID;
alter table WORKFLOW.HISTORIQUE_DEMANDE disable constraint FK_HD_ID_DEMANDE;
alter table WORKFLOW.HISTORIQUE_DEMANDE disable constraint FK_HD_ID_ETAPE_ARRIVEE;
alter table WORKFLOW.HISTORIQUE_DEMANDE disable constraint FK_HD_ID_ETAPE_DEPART;
alter table WORKFLOW.HISTORIQUE_DEMANDE disable constraint FK_HD_PERS_ID_MODIFICATION;

-- suppression des contraintes qui seront refaites à la fin de la copie

alter table GRH_PECHE.FICHE_VOEUX drop constraint FK_FV_DEMANDE;
alter table GRH_PECHE.SERVICE drop constraint FK_SER_DEMANDE;


BEGIN
	GRHUM.MIGRATION_WORKFLOW();
END;
/

-- suppression des tables temporaires

drop table GRH_PECHE.CHEMIN_MIGRATION;
drop table GRH_PECHE.CIRCUIT_VALIDATION_MIGRATION;
drop table GRH_PECHE.ETAPE_MIGRATION;
drop table GRH_PECHE.DEMANDE_MIGRATION;
drop table GRH_PECHE.HISTORIQUE_DEMANDE_MIGRATION;

-- suppression de la procedure

drop procedure GRHUM.MIGRATION_WORKFLOW;

-- réactivation des contraintes 

alter table WORKFLOW.CHEMIN enable constraint FK_CHEMIN_ETAPE_ARRIVEE;
alter table WORKFLOW.CHEMIN enable constraint FK_CHEMIN_ETAPE_DEPART;
alter table WORKFLOW.CIRCUIT_VALIDATION enable constraint FK_CIRCUIT_VALID_GD_APPLI;
alter table WORKFLOW.DEMANDE enable constraint FK_DEMANDE_ID_ETAPE;
alter table WORKFLOW.ETAPE enable constraint FK_ETA_CIRCUIT_VALID;
alter table WORKFLOW.HISTORIQUE_DEMANDE enable constraint FK_HD_ID_DEMANDE;
alter table WORKFLOW.HISTORIQUE_DEMANDE enable constraint FK_HD_ID_ETAPE_ARRIVEE;
alter table WORKFLOW.HISTORIQUE_DEMANDE enable constraint FK_HD_ID_ETAPE_DEPART;
alter table WORKFLOW.HISTORIQUE_DEMANDE enable constraint FK_HD_PERS_ID_MODIFICATION;

-- création des nouvelles contraintes 
GRANT select, references on WORKFLOW.DEMANDE to GRH_PECHE;
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT FK_FV_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES WORKFLOW.DEMANDE (ID_DEMANDE) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES WORKFLOW.DEMANDE (ID_DEMANDE) DEFERRABLE INITIALLY DEFERRED;

COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.ID_DEMANDE IS 'Référence à la demande sur le circuit de validation (cf. WORKFLOW.DEMANDE.ID_DEMANDE)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.ID_DEMANDE IS 'Référence à la demande sur le circuit de validation (cf. WORKFLOW.DEMANDE.ID_DEMANDE)';

-- suppression des anciennes tables dans peche

alter table GRH_PECHE.CHEMIN drop constraint FK_CHEMIN_ETAPE_ARRIVEE;
alter table GRH_PECHE.CHEMIN drop constraint FK_CHEMIN_ETAPE_DEPART;
alter table GRH_PECHE.CIRCUIT_VALIDATION drop constraint FK_CIRCUIT_VALID_GD_APPLI;
alter table GRH_PECHE.DEMANDE drop constraint FK_DEMANDE_ID_ETAPE;
alter table GRH_PECHE.ETAPE drop constraint FK_ETA_CIRCUIT_VALID;
alter table GRH_PECHE.HISTORIQUE_DEMANDE drop constraint FK_HD_ID_DEMANDE;
alter table GRH_PECHE.HISTORIQUE_DEMANDE drop constraint FK_HD_ID_ETAPE_ARRIVEE;
alter table GRH_PECHE.HISTORIQUE_DEMANDE drop constraint FK_HD_ID_ETAPE_DEPART;
alter table GRH_PECHE.HISTORIQUE_DEMANDE drop constraint FK_HD_PERS_ID_MODIFICATION;

drop table GRH_PECHE.CHEMIN;
drop table GRH_PECHE.CIRCUIT_VALIDATION;
drop table GRH_PECHE.DEMANDE;
drop table GRH_PECHE.ETAPE;
drop table GRH_PECHE.HISTORIQUE_DEMANDE;


----------------------------V20131023.151239__DDL_Type_flux_paiement.sql----------------------------


-- création de la table des types de flux de paiement

CREATE TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT"
  (
    "ID_TYPE_FLUX"     NUMBER NOT NULL,
    "LL_TYPE_FLUX"     VARCHAR2(100 BYTE),
    "CLASSE_TYPE_FLUX" VARCHAR2(100 BYTE),
    "D_CREATION" DATE DEFAULT SYSDATE NOT NULL,
    "D_MODIFICATION" DATE DEFAULT SYSDATE NOT NULL,
    "PERS_ID_CREATION"     NUMBER NOT NULL,
    "PERS_ID_MODIFICATION" NUMBER,
    CONSTRAINT "PK_ID_TYPE_FLUX" PRIMARY KEY ("ID_TYPE_FLUX") USING INDEX TABLESPACE "GRH_INDX",
    CONSTRAINT "FK_FLUX_PAIEM_ID_CREATION" FOREIGN KEY ("PERS_ID_CREATION") REFERENCES "GRHUM"."PERSONNE" ("PERS_ID"),
    CONSTRAINT "FK_FLUX_PAIEM_ID_MODIFICATION" FOREIGN KEY ("PERS_ID_MODIFICATION") REFERENCES "GRHUM"."PERSONNE" ("PERS_ID")
  );

CREATE SEQUENCE "GRH_PECHE"."TYPE_FLUX_PAIEMENT_SEQ" START WITH 1 NOCACHE;

COMMENT ON TABLE "GRH_PECHE"."TYPE_FLUX_PAIEMENT" IS 'Table des types de flux de paiement';

COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."ID_TYPE_FLUX" IS 'Clef primaire de la table TYPE_FLUX_PAIEMENT';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."LL_TYPE_FLUX" IS 'Libellé du type de flux';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."CLASSE_TYPE_FLUX" IS 'nom de la classe Java qui permet de formater le flux de mise en paiement';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."D_CREATION" IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."D_MODIFICATION" IS 'Date de la dernière modification de l''enregistrement';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."PERS_ID_CREATION" IS 'Pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN "GRH_PECHE"."TYPE_FLUX_PAIEMENT"."PERS_ID_MODIFICATION" IS 'Pers_id de la personne à l''origine de la modification de l''enregistrement';

---------------------V20131023.152226__DDL_Modification_commentaire_colonne.sql---------------------

-- Ajout de la précision "en HETD" pour la colonne "HEURES_PAYEES"
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.HEURES_PAYEES IS 'Le nombre d''heures payées (en HETD)';

----------------------V20131024.154613__DDL_Creation_table_parametres_flux.sql----------------------
-- ---------------------------------------------
-- Paramètres de génération des flux de paiement
-- ---------------------------------------------

--DROP TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT;
--DROP SEQUENCE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT_SEQ;

-- Table "Paramètres des flux de paiement"

CREATE TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT (
	ID_PARAMETRES NUMBER NOT NULL,
	ID_TYPE_FLUX NUMBER NOT NULL,
	TEMPLATE_NOM_FLUX VARCHAR2(100) NOT NULL,
	ADM CHAR(3) NOT NULL,
	DEPARTEMENT CHAR(3) NOT NULL,
	GIRAFE_CODE_CHAINE_PAYE CHAR(2) NOT NULL,
	GIRAFE_CORR CHAR(3) NOT NULL,
	GIRAFE_MIN CHAR(3) NOT NULL,
	GIRAFE_ORIG CHAR(1) NOT NULL,
	GIRAFE_CODE_IR CHAR(4) NOT NULL,
	GIRAFE_SENS CHAR(1) NOT NULL,
	GIRAFE_CODE_MTT_PRE_CALCULE CHAR(1) NOT NULL,
	GIRAFE_TPL_LIB_COMPLEMENTAIRE VARCHAR2(100) NOT NULL,
	WINPAIE_CODE_DEBUT_LIGNE_CAD CHAR(3) NOT NULL,
	WINPAIE_CODE_CHAINE_PAYE CHAR(7) NOT NULL,
	WINPAIE_TPL_NOM_PRENOM VARCHAR2(100) NOT NULL,
	WINPAIE_OBJET CHAR(22) NOT NULL,
	WINPAIE_CODE_F CHAR(3) NOT NULL,
	WINPAIE_CODE_FIN_LIGNE_CAD CHAR(14) NOT NULL,
	D_DEBUT DATE DEFAULT SYSDATE NOT NULL,
	D_FIN DATE NULL,
	D_CREATION DATE DEFAULT SYSDATE NOT NULL,
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
	PERS_ID_CREATION NUMBER NOT NULL,
	PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT ADD CONSTRAINT PK_PARAMETRES_FLUX_PAIEMENT PRIMARY KEY(ID_PARAMETRES) USING INDEX TABLESPACE GRH_INDX;

ALTER TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT ADD CONSTRAINT FK_PFP_TYPE_FLUX_PAIEMENT FOREIGN KEY (ID_TYPE_FLUX) REFERENCES GRH_PECHE.TYPE_FLUX_PAIEMENT(ID_TYPE_FLUX);
ALTER TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT ADD CONSTRAINT FK_PFP_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT ADD CONSTRAINT FK_PFP_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE(PERS_ID);

CREATE SEQUENCE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.PARAMETRES_FLUX_PAIEMENT IS 'Table contenant les paramètres nécessaires à la génération des flux de mise en paiement';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.ID_PARAMETRES IS 'Clef primaire de la table';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.ID_TYPE_FLUX IS 'Clef étrangère vers la table des types de flux de paiement (TYPE_FLUX_PAIEMENT)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.TEMPLATE_NOM_FLUX IS 'Template du nom du flux (%1$ : le numéro de paiement, %2$ : l''année de paiement, %3$ : le mois de paiement)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.ADM IS 'Adm';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.DEPARTEMENT IS 'Département de l''établissement';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_CODE_CHAINE_PAYE IS 'Flux Girafe - Code de la chaîne de paye';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_CORR IS 'Flux Girafe - Corr (Code attribué par la Trésorerie Générale)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_MIN IS 'Flux Girafe - Min';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_ORIG IS 'Flux Girafe - Orig';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_CODE_IR IS 'Flux Girafe - Code Ir';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_SENS IS 'Flux Girafe - Sens du mouvement (0 = positif par défaut)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_CODE_MTT_PRE_CALCULE IS 'Flux Girafe - Code indiquant que le montant à payer est pré-calculé (A = montant pré-calculé)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.GIRAFE_TPL_LIB_COMPLEMENTAIRE IS 'Flux Girafe - Template du libellé commentaire (%1$ : le nombre d''heures payées, %2$ : le taux brut, %3$ : le montant brut)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_CODE_DEBUT_LIGNE_CAD IS 'Flux WinPaie - Code marquant le début de ligne CAD (CAD)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_CODE_CHAINE_PAYE IS 'Flux WinPaie - Code de la chaîne de paye';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_TPL_NOM_PRENOM IS 'Flux WinPaie - Template pour nom/prénom (%1$ : le nom, %2$ : le prénom)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_OBJET IS 'Flux WinPaie - Objet (du paiement ?)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_CODE_F IS 'Flux WinPaie - Code F (F10)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.WINPAIE_CODE_FIN_LIGNE_CAD IS 'Flux WinPaie - Code marquant la fin de ligne CAD (            01)';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.D_DEBUT IS 'Date du début d''application de ces paramètres';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.D_FIN IS 'Date de fin d''application de ces paramètres';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAMETRES_FLUX_PAIEMENT.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

------------------------V20131029.183029__DDL_Modification_Table_Arrete.sql-------------------------
-- Table "Arrêté" devient Table "paiement"

ALTER TABLE GRH_PECHE.ARRETE RENAME TO PAIEMENT;
COMMENT ON TABLE GRH_PECHE.PAIEMENT IS 'Table contenant les paiements de mise en paiement';

alter table GRH_PECHE.PAIEMENT rename column ID_ARRETE to ID_PAIEMENT;

alter table GRH_PECHE.PAIEMENT rename column NUMERO_ARRETE to NUMERO_PAIEMENT;
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.NUMERO_PAIEMENT IS 'Numéro de paiement';

COMMENT ON COLUMN GRH_PECHE.PAIEMENT.MOIS_PAIEMENT IS 'Mois de paiement de ce paiement';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.ANNEE_PAIEMENT IS 'Année de paiement de ce paiement';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.PAYE IS 'Est-ce que ce paiement est payé (O/N) ? Si oui, il n''est plus du tout modifiable (ni les mises en paiement associées).';

 
ALTER TABLE GRH_PECHE.PAIEMENT RENAME CONSTRAINT PK_ARRETE TO PK_PAIEMENT;
ALTER TABLE GRH_PECHE.PAIEMENT RENAME CONSTRAINT UK_ARRETE TO UK_PAIEMENT;

ALTER TABLE GRH_PECHE.PAIEMENT RENAME CONSTRAINT FK_ARRETE_PERS_CREATION TO FK_PAIEMENT_PERS_CREATION;
ALTER TABLE GRH_PECHE.PAIEMENT RENAME CONSTRAINT FK_ARRETE_PERS_MODIFICATION TO FK_PAIEMENT_PERS_MODIFICATION;
ALTER TABLE GRH_PECHE.PAIEMENT RENAME CONSTRAINT CHK_ARRETE_PAYE TO  CHK_PAIEMENT_PAYE;
 
alter table GRH_PECHE.FLUX_MISE_EN_PAIEMENT rename column ID_ARRETE to ID_PAIEMENT;
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.ID_PAIEMENT IS 'Référence vers un paiement';

alter table GRH_PECHE.FLUX_MISE_EN_PAIEMENT rename CONSTRAINT FK_FMEP_ARRETE TO FK_FMEP_PAIEMENT;

alter table GRH_PECHE.MISE_EN_PAIEMENT rename column ID_ARRETE to ID_PAIEMENT;
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.ID_PAIEMENT IS 'Référence vers un paiement (null si les heures ne sont pas encore payées)';

alter table GRH_PECHE.MISE_EN_PAIEMENT rename CONSTRAINT FK_MEP_ARRETE TO FK_MEP_PAIEMENT;










---------------------------V20131030.143548__DDL_Ajout_table_Periode.sql----------------------------
-- ---------------------------------------------------------------------------
-- Création de la table PERIODE
-- ---------------------------------------------------------------------------

CREATE TABLE GRH_PECHE.PERIODE (
	ID_PERIODE 	NUMBER NOT NULL,
	LL_PERIODE 	VARCHAR2(15),
	TYPE		VARCHAR2(1) DEFAULT 'M' NOT NULL,
	ORDRE_AFFICHAGE	INTEGER NOT NULL,
	ORDRE_PERIODE	INTEGER NOT NULL,
	NO_SEMESTRE	INTEGER NOT NULL
);

ALTER TABLE GRH_PECHE.PERIODE ADD CONSTRAINT PK_PERIODE PRIMARY KEY(ID_PERIODE) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.PERIODE ADD CONSTRAINT CHK_PERIODE_TYPE CHECK (TYPE IN ('M', 'S'));

CREATE SEQUENCE GRH_PECHE.PERIODE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.PERIODE 			IS 'Table des périodes utilisées dans PECHE';
COMMENT ON COLUMN GRH_PECHE.PERIODE.ID_PERIODE 		IS 'Clef primaire de la table PERIODE';
COMMENT ON COLUMN GRH_PECHE.PERIODE.LL_PERIODE 		IS 'Libellé de la période';
COMMENT ON COLUMN GRH_PECHE.PERIODE.TYPE 		IS 'Type de la période (M pour le mois, S pour le semestre)';
COMMENT ON COLUMN GRH_PECHE.PERIODE.ORDRE_AFFICHAGE 	IS 'Ordre d''affichage de la période';
COMMENT ON COLUMN GRH_PECHE.PERIODE.ORDRE_PERIODE 	IS 'Ordre de la période';
COMMENT ON COLUMN GRH_PECHE.PERIODE.NO_SEMESTRE 	IS 'Numéro du semestre lié à la période';

---------------------V20131031.122207__DDL_Ajout_Periode_Au_Service_Detail.sql----------------------
-- ---------------------------------------------------------------------------
-- Ajout de la colonne PERIODE à la table SERVICE_DETAIL
-- ---------------------------------------------------------------------------

DECLARE
  is_column_exists int;
BEGIN 
	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'ID_PERIODE ' 
	   AND table_name = 'SERVICE_DETAIL';
  	    IF (is_column_exists  = 0) 
	  THEN
    		EXECUTE IMMEDIATE 'ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD (ID_PERIODE NUMBER)';
		EXECUTE IMMEDIATE 'COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.ID_PERIODE IS ''Période concernée (mois ou semestre) (cf. GRH_PECHE.PERIODE.ID_PERIODE)'' ';
		EXECUTE IMMEDIATE 'ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_PERIODE FOREIGN KEY (ID_PERIODE) REFERENCES GRH_PECHE.PERIODE (ID_PERIODE)';
		EXECUTE IMMEDIATE 'CREATE INDEX GRH_PECHE.IDX_SD_ID_PERIODE ON GRH_PECHE.SERVICE_DETAIL(ID_PERIODE) TABLESPACE GRH_INDX';
        END IF;
END;
/

-------------------V20131104.100515__DDL_Modification_table_mise_en_paiement.sql--------------------
-- -----------------------------------------
-- Modification de la table MISE_EN_PAIEMENT
-- -----------------------------------------

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT RENAME COLUMN HEURES_A_PAYEES TO HEURES_A_PAYER;
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD (C_STRUCTURE VARCHAR2(10));

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_STRUCTURE FOREIGN KEY (C_STRUCTURE) REFERENCES GRHUM.STRUCTURE_ULR(C_STRUCTURE);

COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.C_STRUCTURE IS 'Référence à la structure qui paye';
----------------V20131105.150442__DDL_Ajout_colonnes_hetd_dans_mise_en_paiement.sql-----------------
-- --------------------------------------------------------------------
-- Ajout colonne nombre d'heures en hetd dans la table MISE_EN_PAIEMENT
-- --------------------------------------------------------------------

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD (HEURES_A_PAYER_HETD NUMBER NOT NULL);
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD (HEURES_PAYEES_HETD NUMBER NULL);

COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.HEURES_A_PAYER_HETD IS 'Heures à payer (en hetd)';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.HEURES_PAYEES_HETD IS 'Heures payées (en hetd)';
----------------------V20131106.095421__DDL_Ajout_colonnes_table_paiement.sql-----------------------
-- -------------------------------------
-- Ajout colonnes dans la table PAIEMENT
-- -------------------------------------

ALTER TABLE GRH_PECHE.PAIEMENT ADD (LIBELLE VARCHAR2(100) NULL);
ALTER TABLE GRH_PECHE.PAIEMENT ADD (D_PAIEMENT DATE DEFAULT SYSDATE NOT NULL);
ALTER TABLE GRH_PECHE.PAIEMENT ADD (D_BORDEREAU DATE NULL);
ALTER TABLE GRH_PECHE.PAIEMENT ADD (D_ARRETE DATE NULL);
ALTER TABLE GRH_PECHE.PAIEMENT ADD (D_FLUX DATE NULL);

COMMENT ON COLUMN GRH_PECHE.PAIEMENT.LIBELLE IS 'Libellé';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.D_PAIEMENT IS 'Date de la mise en paiement';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.D_BORDEREAU IS 'Date de dernière génération du bordereau';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.D_ARRETE IS 'Date de dernière génération de l''arrêté';
COMMENT ON COLUMN GRH_PECHE.PAIEMENT.D_FLUX IS 'Date de dernière génération du flux';
------------------------V20131107.112010__DDL_Creation_Sequence_Paiement.sql------------------------
-- ---------------------------------------------------------------------------
-- Création de la séquence GRH_PECHE.PAIEMENT_SEQ si non créée avant
-- 	==> Ce cas peut arriver si on n'a pas de données dans la table GRH_PECHE.PAIEMENT
-- ---------------------------------------------------------------------------


DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur
	  FROM ALL_SEQUENCES
         WHERE SEQUENCE_OWNER = 'GRH_PECHE'
           AND SEQUENCE_NAME = 'PAIEMENT_SEQ';  

  	    IF (compteur = 0)
  	  THEN              
    		EXECUTE IMMEDIATE 'CREATE SEQUENCE GRH_PECHE.PAIEMENT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
  	END IF;

COMMIT;
END;
/

--------------V20131108.102841__DDL_Ajout_colonne_taux_brut_dans_mise_en_paiement.sql---------------
-- ------------------------------------------------------
-- Ajout colonne TAUX_BRUT dans la table MISE_EN_PAIEMENT
-- ------------------------------------------------------

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD (TAUX_BRUT NUMBER NOT NULL);

COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.TAUX_BRUT IS 'Taux horaire brut appliqué';
-----------------------V20131108.105656__DDL_Ajout_colonne_table_periode.sql------------------------
-- ---------------------------------------------------
-- Ajout colonne NO_DERNIER_MOIS dans la table PERIODE
-- ---------------------------------------------------

ALTER TABLE GRH_PECHE.PERIODE ADD (NO_PREMIER_MOIS INTEGER DEFAULT 0 NOT NULL);
ALTER TABLE GRH_PECHE.PERIODE ADD (NO_DERNIER_MOIS INTEGER DEFAULT 0 NOT NULL);

COMMENT ON COLUMN GRH_PECHE.PERIODE.NO_PREMIER_MOIS IS 'Numéro du premier mois de la période';
COMMENT ON COLUMN GRH_PECHE.PERIODE.NO_DERNIER_MOIS IS 'Numéro du dernier mois de la période';


