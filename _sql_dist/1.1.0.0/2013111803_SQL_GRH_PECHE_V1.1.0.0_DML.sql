--
-- Patch DML de PECHE du 18/11/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.1.0.0
-- Date de publication : 18/11/2013
-- Auteur(s) : Equipe PECHE
-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


----------------------------V20131023.151250__DML_Type_flux_paiement.sql----------------------------

-- Creation des 2 premiers types de flux de mise en paiement

DECLARE
	persid_createur integer;
	flag integer;
	now timestamp;
BEGIN
	 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	select SYSDATE into now from dual;
	INSERT INTO GRH_PECHE.TYPE_FLUX_PAIEMENT(ID_TYPE_FLUX,LL_TYPE_FLUX,CLASSE_TYPE_FLUX,D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) 
  	VALUES(GRH_PECHE.TYPE_FLUX_PAIEMENT_SEQ.NEXTVAL, 'Flux Girafe', 'org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe', now, persid_createur, now, persid_createur);
  	INSERT INTO GRH_PECHE.TYPE_FLUX_PAIEMENT(ID_TYPE_FLUX,LL_TYPE_FLUX,CLASSE_TYPE_FLUX,D_CREATION, PERS_ID_CREATION, D_MODIFICATION, PERS_ID_MODIFICATION) 
  	VALUES(GRH_PECHE.TYPE_FLUX_PAIEMENT_SEQ.NEXTVAL, 'Flux WinPaie', 'org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementWinpaie', now, persid_createur, now, persid_createur);
  
  commit;
END;
/
-------------------------------V20131024.095807__DML_Ajout_Droits.sql-------------------------------
--
-- Ajout droits dans Agrhum
--

DECLARE
   compteur NUMBER;
   idDomaine NUMBER;
   idApplication NUMBER;
BEGIN
	-- On recherche le domaine RH
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.GD_DOMAINE 
	 WHERE DOM_LC = 'RH';
	    IF (compteur = 0) 
	  THEN
		RAISE_APPLICATION_ERROR(-20000,'Le domaine RH n''existe pas');
	  ELSE
		SELECT DOM_ID 
		  INTO idDomaine 
		  FROM GRHUM.GD_DOMAINE 
		 WHERE DOM_LC = 'RH';
	END IF;

	-- On recherche l'application PECHE
	SELECT COUNT(*) 
	  INTO compteur 
	  FROM GRHUM.GD_APPLICATION 
	 WHERE APP_LC = 'PECHE';
	    IF (compteur = 0) 
	  THEN
		RAISE_APPLICATION_ERROR(-20000,'L''application PECHE n''existe pas');
	ELSE
		SELECT APP_ID 
		  INTO idApplication 
		  FROM GRHUM.GD_APPLICATION 
		 WHERE APP_LC = 'PECHE';
	END IF;

	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_TAUX_HORAIRES'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_TAUX_HORAIRES', 'Taux horaires', 'Gérer les taux horaires');
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'PARAM_FLUX_PAIEMENT'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Paramétrages', 'PARAM_FLUX_PAIEMENT', 'Flux de paiement', 'Paramétrer les flux de paiement');
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'HEURES_A_PAYER'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Mise en paiement', 'HEURES_A_PAYER', 'Saisie des heures à payer', 'Saisie des heures à payer');
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'FIC_SERVICE_PAIEMENT'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Mise en paiement', 'FIC_SERVICE_PAIEMENT', 'Mise en paiement de la fiche de service', 'Mise en paiement de la fiche de service');
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'MISE_EN_PAIEMENT'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Mise en paiement', 'MISE_EN_PAIEMENT', 'Gestion des arrêtés', 'Gestion des arrêtés');
	END IF;

	commit;
END;
/
------------------------V20131024.171050__DML_Init_table_parametres_flux.sql------------------------
-- ---------------------------------------------------------------------------
-- Init des valeurs par défaut de la table des paramètres des flux de paiement
-- ---------------------------------------------------------------------------

DECLARE
	persid_createur integer;
	idTypeFlux integer;
	flag integer;
	now timestamp;
BEGIN
	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	select SYSDATE into now from dual;
	
	-- Récup du type de flux Girafe (utilise par INSA Toulouse)
	SELECT ID_TYPE_FLUX
	  INTO idTypeFlux
	  FROM GRH_PECHE.TYPE_FLUX_PAIEMENT
	 WHERE CLASSE_TYPE_FLUX = 'org.cocktail.peche.metier.miseenpaiement.FormatFluxPaiementGirafe';

	INSERT INTO GRH_PECHE.PARAMETRES_FLUX_PAIEMENT (
		ID_PARAMETRES, ID_TYPE_FLUX,
		TEMPLATE_NOM_FLUX,
		ADM, DEPARTEMENT,
		GIRAFE_CODE_CHAINE_PAYE, GIRAFE_CORR, GIRAFE_MIN, GIRAFE_ORIG, GIRAFE_CODE_IR, GIRAFE_SENS, GIRAFE_CODE_MTT_PRE_CALCULE, GIRAFE_TPL_LIB_COMPLEMENTAIRE,
		WINPAIE_CODE_DEBUT_LIGNE_CAD, WINPAIE_CODE_CHAINE_PAYE, WINPAIE_TPL_NOM_PRENOM, WINPAIE_OBJET, WINPAIE_CODE_F, WINPAIE_CODE_FIN_LIGNE_CAD,
		D_DEBUT, D_FIN,
		D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION
	) VALUES (
		GRH_PECHE.PARAMETRES_FLUX_PAIEMENT_SEQ.NEXTVAL, idTypeFlux,
		'paiement_%1$d_%2$04d%3$02d',
		' ', ' ',
		'PP', ' ', ' ', '1', '0204', '0', 'A', '%1$1.2f heures à %2$1.2f',
		'CAD', ' ', '%1$s, %2$s', 'CRS COMPL', 'F10', LPAD('01', 14),
		TO_DATE('01/09/2013', 'dd/MM/yyyy'), null,
		now, now, persid_createur, persid_createur
	);	 
	 
	COMMIT;
END;
/

---------------------------V20131029.090801__DML_Modification_Droits.sql----------------------------
--
-- Modifications droits dans Agrhum
--

DECLARE
   compteur NUMBER;
   idDomaine NUMBER;
   idApplication NUMBER;
BEGIN
	-- On recherche le domaine RH
	SELECT COUNT(*) 
	  INTO compteur 
          FROM GRHUM.GD_DOMAINE 
	 WHERE DOM_LC = 'RH';
	    IF (compteur = 0) 
	  THEN
		RAISE_APPLICATION_ERROR(-20000,'Le domaine RH n''existe pas');
	  ELSE
		SELECT DOM_ID 
		  INTO idDomaine 
		  FROM GRHUM.GD_DOMAINE 
		 WHERE DOM_LC = 'RH';
	END IF;

	-- On recherche l'application PECHE
	SELECT COUNT(*) 
	  INTO compteur 
	  FROM GRHUM.GD_APPLICATION 
	 WHERE APP_LC = 'PECHE';
	    IF (compteur = 0) 
	  THEN
		RAISE_APPLICATION_ERROR(-20000,'L''application PECHE n''existe pas');
	ELSE
		SELECT APP_ID 
		  INTO idApplication 
		  FROM GRHUM.GD_APPLICATION 
		 WHERE APP_LC = 'PECHE';
	END IF;

	
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'FIC_SERVICE_PAIEMENT'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 1)
  	  THEN
		DELETE FROM GRHUM.GD_FONCTION WHERE APP_ID = idApplication and FON_ID_INTERNE = 'FIC_SERVICE_PAIEMENT';
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'MISE_EN_PAIEMENT'
	   AND GRHUM.GD_FONCTION.APP_ID = idApplication;
        IF (compteur = 1)
  	  THEN
		UPDATE GRHUM.GD_FONCTION set FON_LC='Gestion des bordereaux de paiement',FON_DESCRIPTION='Gestion des bordereaux de paiement' WHERE APP_ID = idApplication and FON_ID_INTERNE = 'MISE_EN_PAIEMENT';        
	END IF;

	commit;
END;
/
------------------------V20131029.192233__DML_Modification_Table_Arrete.sql-------------------------
-- ---------------------------------------------------------------------------
-- Création d'une nouvelle séquence avec valeurs de l'ancienne séquence
-- ---------------------------------------------------------------------------


declare
 ex number;
begin
  select MAX(ID_PAIEMENT)+1 into ex from GRH_PECHE.PAIEMENT;
  if (ex > 0) then               
    execute immediate 'CREATE SEQUENCE GRH_PECHE.PAIEMENT_SEQ START WITH ' || ex || ' INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER';
  end if;
   execute immediate 'DROP SEQUENCE GRH_PECHE.ARRETE_SEQ';
  commit;
END;
/
--------------------------V20131030.143652__DML_Ajout_Donnees_Periode.sql---------------------------
-- ---------------------------------------------------------------------------
-- Insertion dans la table PERIODE
-- ---------------------------------------------------------------------------

INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Septembre', 1, 1, 1);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Octobre', 2, 2, 1);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Novembre', 3, 3, 1);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Décembre', 4, 4, 1);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, TYPE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Semestre 1', 'S', 5, 4, 1);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Janvier', 6, 5, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Février', 7, 6, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Mars', 8, 7, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Avril', 9, 8, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Mai', 10, 9, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Juin', 11, 10, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Juillet', 12, 11, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Août', 13, 12, 2);
INSERT INTO GRH_PECHE.PERIODE (ID_PERIODE, LL_PERIODE, TYPE, ORDRE_AFFICHAGE, ORDRE_PERIODE, NO_SEMESTRE)
     VALUES (GRH_PECHE.PERIODE_SEQ.NEXTVAL, 'Semestre 2', 'S', 14, 13, 2);


--Ajout du paramétrage de l'établissement : affichage de la période en mois ou semestre
DECLARE
	persid_createur integer;
	flag integer;
BEGIN
	-- Récupération d'un créateur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de récuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	INSERT INTO GRH_PECHE.PECHE_PARAMETRES (PARAM_ID, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
		VALUES (GRH_PECHE.PECHE_PARAMETRES_SEQ.NEXTVAL, 'periode.choix.type', 'M', 'Choix du type de la période pour la saisie des heures réalisées', SYSDATE, SYSDATE, persid_createur, persid_createur);
END;
/


------------------------V20131104.152153__DML_Modification_Ordre_periode.sql------------------------
--
-- Modification de l'ordre de la période pour le semestre 2
--

DECLARE
   compteur integer;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRH_PECHE.PERIODE
 	 WHERE GRH_PECHE.PERIODE.LL_PERIODE = 'Semestre 2';
    	    IF (compteur = 0)
  	  THEN
      	 	RAISE_APPLICATION_ERROR(-20000,'La période Semestre 2 n''existe pas');		
 	  ELSE 
		UPDATE GRH_PECHE.PERIODE
         	   SET GRH_PECHE.PERIODE.ORDRE_PERIODE = 12
         	 WHERE GRH_PECHE.PERIODE.LL_PERIODE = 'Semestre 2';
  	END IF;
END;
/


--------------------------V20131104.154711__DML_Ajout_Etapes_Circuits.sql---------------------------
--
-- Ajout etapes optionnelles aux circuits PECHE_PREV_STATUTAIRE et PECHE_PREV_VACATAIRE
--


DECLARE
	persid_createur integer;
	flag integer;
	id_app number(12,0);
	id_circuit number;
	id_etape_d number;
	id_chemin number;
  compteur NUMBER;
  
  CURSOR c_circuits IS SELECT * from WORKFLOW.CIRCUIT_VALIDATION where C_CIRCUIT_VALIDATION='PECHE_PREV_STATUTAIRE' OR C_CIRCUIT_VALIDATION='PECHE_PREV_VACATAIRE'
  and ID_APP=id_app;  
  le_circuit WORKFLOW.CIRCUIT_VALIDATION%ROWTYPE;
  
BEGIN
 	-- recup d'un createur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de recuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	-- Application
	-- ***********
	SELECT APP_ID INTO id_app FROM GRHUM.GD_APPLICATION WHERE APP_STR_ID = 'PECHE';
	
  FOR le_circuit IN c_circuits
LOOP
  
SELECT COUNT(*) INTO compteur FROM WORKFLOW.ETAPE 
 	 WHERE WORKFLOW.ETAPE.ID_CIRCUIT_VALIDATION = le_circuit.id_circuit_validation
	   AND WORKFLOW.ETAPE.C_ETAPE='VALID_DRH';
        IF (compteur = 0) THEN
          SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_d FROM DUAL;
          Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_d, 'VALID_DRH', 'Validation par la DRH', le_circuit.id_circuit_validation);
  
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'VALIDER', 'Valider');
	
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'REFUSER', 'Refuser');
	END IF;
  
SELECT COUNT(*) INTO compteur FROM WORKFLOW.ETAPE 
 	 WHERE WORKFLOW.ETAPE.ID_CIRCUIT_VALIDATION = le_circuit.id_circuit_validation
	   AND WORKFLOW.ETAPE.C_ETAPE='VALID_DIR_COMPOSANTE';
        IF (compteur = 0) THEN
          SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_d FROM DUAL;
          Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_d, 'VALID_DIR_COMPOSANTE', 'Validation par le dir. comp.', le_circuit.id_circuit_validation);

          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'VALIDER', 'Valider');
	
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'REFUSER', 'Refuser');
	END IF;

SELECT COUNT(*) INTO compteur FROM WORKFLOW.ETAPE 
 	 WHERE WORKFLOW.ETAPE.ID_CIRCUIT_VALIDATION = le_circuit.id_circuit_validation
	   AND WORKFLOW.ETAPE.C_ETAPE='VALID_ENSEIGNANT';
        IF (compteur = 0) THEN
          SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_d FROM DUAL;
          Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_d, 'VALID_ENSEIGNANT', 'Validation par l''enseignant', le_circuit.id_circuit_validation);
  
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'VALIDER', 'Valider');
	
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'REFUSER', 'Refuser');
	END IF;

SELECT COUNT(*) INTO compteur FROM WORKFLOW.ETAPE 
 	 WHERE WORKFLOW.ETAPE.ID_CIRCUIT_VALIDATION = le_circuit.id_circuit_validation
	   AND WORKFLOW.ETAPE.C_ETAPE='VALID_PRESIDENT';
        IF (compteur = 0) THEN
          SELECT WORKFLOW.ETAPE_SEQ.NEXTVAL INTO id_etape_d FROM DUAL;
          Insert into WORKFLOW.ETAPE(ID_ETAPE, C_ETAPE, LL_ETAPE, ID_CIRCUIT_VALIDATION) values (id_etape_d, 'VALID_PRESIDENT', 'Validation par le président', le_circuit.id_circuit_validation);
	
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'VALIDER', 'Valider');
	
          SELECT WORKFLOW.CHEMIN_SEQ.NEXTVAL INTO id_chemin FROM DUAL;
          Insert into WORKFLOW.CHEMIN(ID_CHEMIN, ID_ETAPE_DEPART, ID_ETAPE_ARRIVEE, C_TYPE_CHEMIN, LC_TYPE_CHEMIN) values (id_chemin, id_etape_d, null, 'REFUSER', 'Refuser');
	END IF;
  
END LOOP;
    
	commit;
END;
/

----------------V20131108.110006__DML_Init_colonne_no_dernier_mois_table_periode.sql----------------

UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 9, NO_DERNIER_MOIS = 9
 WHERE LL_PERIODE = 'Septembre';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 10, NO_DERNIER_MOIS = 10
 WHERE LL_PERIODE = 'Octobre';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 11, NO_DERNIER_MOIS = 11
 WHERE LL_PERIODE = 'Novembre';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 12, NO_DERNIER_MOIS = 12
 WHERE LL_PERIODE = 'Décembre';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 9, NO_DERNIER_MOIS = 12
 WHERE LL_PERIODE = 'Semestre 1';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 1, NO_DERNIER_MOIS = 1
 WHERE LL_PERIODE = 'Janvier';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 2, NO_DERNIER_MOIS = 2
 WHERE LL_PERIODE = 'Février';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 3, NO_DERNIER_MOIS = 3
 WHERE LL_PERIODE = 'Mars';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 4, NO_DERNIER_MOIS = 4
 WHERE LL_PERIODE = 'Avril';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 5, NO_DERNIER_MOIS = 5
 WHERE LL_PERIODE = 'Mai';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 6, NO_DERNIER_MOIS = 6
 WHERE LL_PERIODE = 'Juin';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 7, NO_DERNIER_MOIS = 7
 WHERE LL_PERIODE = 'Juillet';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 8, NO_DERNIER_MOIS = 8
 WHERE LL_PERIODE = 'Août';
UPDATE GRH_PECHE.PERIODE
   SET NO_PREMIER_MOIS = 1, NO_DERNIER_MOIS = 8
 WHERE LL_PERIODE = 'Semestre 2';

-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.1.0.0', TO_DATE('18/11/2013', 'DD/MM/YYYY'), SYSDATE, 'Installation PECHE version 1.1');

COMMIT;
