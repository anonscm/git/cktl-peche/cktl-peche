--
-- Patch DML de PECHE du 04/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.1.3
-- Date de publication : 04/04/2014
-- Auteur(s) : Equipe PECHE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;



-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.1.3', TO_DATE('04/04/2014', 'DD/MM/YYYY'), SYSDATE, 'Maj PECHE version 1.1.3.0');

COMMIT;
