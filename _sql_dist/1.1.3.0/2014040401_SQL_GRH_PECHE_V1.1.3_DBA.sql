--
-- Patch DBA de PECHE du 04/04/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DBA
-- Schema : GRHUM
-- Numero de version : 1.1.3
-- Date de publication : 04/04/2014
-- Auteur(s) : Equipe PECHE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


-------------------------V20140324.165423__DBA_Grant_Mangue_Vacataires.sql--------------------------
-- -----------------------------------
-- Grant pour MANGUE.Vacataires
-- -----------------------------------

GRANT select, references on MANGUE.VACATAIRES to GRH_PECHE;



