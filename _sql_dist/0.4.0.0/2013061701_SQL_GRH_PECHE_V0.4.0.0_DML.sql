--
-- Patch DML de GRH_PECHE du 17/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1/2
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.4.0.0
-- Date de publication : 17/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

CREATE OR REPLACE PROCEDURE insertFunction(appId IN INTEGER, categorie in VARCHAR2, idInterne IN VARCHAR2, libelle IN VARCHAR2, description IN VARCHAR2)
IS
  compteur NUMBER;
BEGIN
	SELECT COUNT(*) INTO compteur FROM GRHUM.GD_FONCTION WHERE APP_ID = appId AND FON_ID_INTERNE = idInterne;
	IF (compteur = 0) THEN
		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	 VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, appId, categorie, idInterne, libelle, description);
	END IF;
END;
/

DECLARE
    idApplication NUMBER;
BEGIN
	
	SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';

	-- GD_FONCTION
    -- -- Catégorie 'Enseignements'
    --    -- Fonction 'Offre de formation - Nombre d'heures'
    insertFunction(idApplication, 'Enseignements', 'PARAM_NB_HEURES', 'Offre de formation - Nombre d''heures', 'Offre de formation - Surcharger le nombre d''heures maquette');

	COMMIT;
END;
/

DROP PROCEDURE insertFunction;
