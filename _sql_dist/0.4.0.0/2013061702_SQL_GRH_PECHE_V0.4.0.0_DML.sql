--
-- Patch DDL de GRH_PECHE du 17/06/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 2/2
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.4.0.0
-- Date de publication : 17/06/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
INSERT INTO GRH_PECHE.DB_VERSION VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.4.0.0', TO_DATE('17/06/2013', 'DD/MM/YYYY'),NULL,'Version Beta 4 du user GRH_PECHE');

--
-- Libellé des étapes (raccourcir)
--
UPDATE GRH_PECHE.ETAPE
   SET LL_ETAPE = 'Validation par le dir. comp.'
 WHERE C_ETAPE = 'VALID_DIR_COMPOSANTE';

UPDATE GRH_PECHE.ETAPE
   SET LL_ETAPE = 'Validation par la DRH'
 WHERE C_ETAPE = 'VALID_DRH';

--
-- DB_VERSION
--
UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL = SYSDATE WHERE DBV_LIBELLE = '0.4.0.0';

--Validation
commit;
