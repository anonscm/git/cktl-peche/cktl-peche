--
-- Patch DML de PECHE du 05/09/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 0.6.0.0
-- Date de publication : 05/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


----------V20130828.141408__DML_Ajout_methode_calcul_hcomp_proratisation_presentielle.sql-----------
-- -----------------------------------------------------------------
-- Ajout de la méthode de calcul par proratisation sur le présentiel
-- -----------------------------------------------------------------

DECLARE
	persid_createur integer;
	flag integer;
BEGIN
	-- Récupération d'un créateur
	persid_createur := null;
	select count(*) into flag from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;
	
	if (flag = 0) then
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de récuperer le pers_id d''un GRHUM_CREATEUR');
	end if;
	
	select c.pers_id into persid_createur from (select param_value from (
	select * from GRHUM.grhum_parametres where param_key='GRHUM_CREATEUR' order by param_ordre desc)
	where rownum=1) x, grhum.compte c where x.param_value=c.cpt_login;

	INSERT INTO GRH_PECHE.METHODES_CALCUL_HCOMP (
		METHODE_ID, LL_METHODE,
		CLASSE_METHODE,
		D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
	VALUES (
		4, 'Proratisation sur présentiel',
		'org.cocktail.peche.metier.hce.MethodeDeCalculParProratisationSurPresentiel',
		SYSDATE, SYSDATE, persid_createur, persid_createur
		);
END;
/

-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.6.0.0', TO_DATE('05/09/2013', 'DD/MM/YYYY'), SYSDATE, 'Version Beta 6 du user GRH_PECHE');

COMMIT;
