--
-- Patch DDL de PECHE du 05/09/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 0.6.0.0
-- Date de publication : 05/09/2013
-- Auteur(s) : Pascal MACOUIN

-- Licence : CeCILL version 2
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


---------------------V20130718.110624__DDL_Ajout_colonnes_mise_en_paiement.sql----------------------
-- ----------------
-- Mise en paiement
-- ----------------

-- Ajout de colonnes dans SERVICE
ALTER TABLE GRH_PECHE.SERVICE
  ADD (
    HEURES_A_PAYER NUMBER,
    HEURES_A_REPORTER_SOUHAITEES NUMBER,
    HEURES_A_REPORTER_ATTRIBUEES NUMBER,
    HEURES_GRATUITES NUMBER,
    HEURES_PAYEES NUMBER
  );

COMMENT ON COLUMN GRH_PECHE.SERVICE.HEURES_A_PAYER IS 'Heures à payer (parmi les HComp et pour les statutaires)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.HEURES_A_REPORTER_SOUHAITEES IS 'Heures que l''enseignant souhaite reporter pour l''année N+1 (parmi les HComp et pour les statutaires)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.HEURES_A_REPORTER_ATTRIBUEES IS 'Heures effectivement reportées pour l''année N+1 (parmi les HComp et pour les statutaires)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.HEURES_GRATUITES IS 'Heures qui ne seront pas payées (parmi les HComp et pour les statutaires)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.HEURES_PAYEES IS 'Heures qui ont été payées (parmi les HComp et pour les statutaires)';

-- Ajout de colonnes dans SERVICE_DETAIL
ALTER TABLE GRH_PECHE.SERVICE_DETAIL
  ADD (
    HEURES_A_PAYER NUMBER,
    HEURES_PAYEES NUMBER
  );

COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.HEURES_A_PAYER IS 'Heures à payer (parmi les heures réalisées et pour les vacataires)';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.HEURES_PAYEES IS 'Heures qui ont été payées (parmi les heures réalisées et pour les vacataires)';


----------------------V20130718.164812__DDL_Ajout_tables_mise_en_paiement.sql-----------------------
-- ----------------
-- Mise en paiement
-- ----------------

--DROP TABLE GRH_PECHE.MISE_EN_PAIEMENT;
--DROP SEQUENCE GRH_PECHE.MISE_EN_PAIEMENT_SEQ;
--DROP TABLE GRH_PECHE.ARRETE;
--DROP SEQUENCE GRH_PECHE.ARRETE_SEQ;

-- Table "Arrêté"

CREATE TABLE GRH_PECHE.ARRETE (
	ID_ARRETE NUMBER NOT NULL,
	NUMERO_ARRETE NUMBER NOT NULL,
	MOIS_PAIEMENT NUMBER(2) NOT NULL,
	ANNEE_PAIEMENT NUMBER(4) NOT NULL,
	PAYE CHAR(1) DEFAULT 'N' NOT NULL,
	D_CREATION DATE DEFAULT SYSDATE NOT NULL,
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
	PERS_ID_CREATION NUMBER NOT NULL,
	PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE GRH_PECHE.ARRETE ADD CONSTRAINT PK_ARRETE PRIMARY KEY(ID_ARRETE) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.ARRETE ADD CONSTRAINT UK_ARRETE UNIQUE (NUMERO_ARRETE) USING INDEX TABLESPACE GRH_INDX;

ALTER TABLE GRH_PECHE.ARRETE ADD CONSTRAINT FK_ARRETE_PERS_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.ARRETE ADD CONSTRAINT FK_ARRETE_PERS_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.ARRETE ADD CONSTRAINT CHK_ARRETE_PAYE CHECK (PAYE IN ('O', 'N'));

CREATE SEQUENCE GRH_PECHE.ARRETE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.ARRETE IS 'Table contenant les arrêtés de mise en paiement';
COMMENT ON COLUMN GRH_PECHE.ARRETE.ID_ARRETE IS 'Clef primaire de la table';
COMMENT ON COLUMN GRH_PECHE.ARRETE.NUMERO_ARRETE IS 'Numéro d''arrêté';
COMMENT ON COLUMN GRH_PECHE.ARRETE.MOIS_PAIEMENT IS 'Mois de paiement de cet arrêté';
COMMENT ON COLUMN GRH_PECHE.ARRETE.ANNEE_PAIEMENT IS 'Année de paiement de cet arrêté';
COMMENT ON COLUMN GRH_PECHE.ARRETE.PAYE IS 'Est-ce qu ecet arrêté est payé (O/N) ? Si oui, il n''est plus du tout modifiable (ni les mises en paiement associées).';
COMMENT ON COLUMN GRH_PECHE.ARRETE.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.ARRETE.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.ARRETE.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.ARRETE.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

-- Table "Mise en paiement"

CREATE TABLE GRH_PECHE.MISE_EN_PAIEMENT (
	ID_MISE_EN_PAIEMENT NUMBER NOT NULL,
	ID_SERVICE NUMBER NOT NULL,
	ID_SERVICE_DETAIL NUMBER NULL,
	ID_ARRETE NUMBER NULL,
	HEURES_A_PAYEES NUMBER NOT NULL,
	HEURES_PAYEES NUMBER NULL,
	D_CREATION DATE DEFAULT SYSDATE NOT NULL,
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
	PERS_ID_CREATION NUMBER NOT NULL,
	PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT PK_MISE_EN_PAIEMENT PRIMARY KEY(ID_MISE_EN_PAIEMENT) USING INDEX TABLESPACE GRH_INDX;

ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_SERVICE FOREIGN KEY (ID_SERVICE) REFERENCES GRH_PECHE.SERVICE(ID_SERVICE);
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_SERVICE_DETAIL FOREIGN KEY (ID_SERVICE_DETAIL) REFERENCES GRH_PECHE.SERVICE_DETAIL(ID_SERVICE_DETAIL);
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_ARRETE FOREIGN KEY (ID_ARRETE) REFERENCES GRH_PECHE.ARRETE(ID_ARRETE);
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_PERS_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD CONSTRAINT FK_MEP_PERS_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE(PERS_ID);

CREATE SEQUENCE GRH_PECHE.MISE_EN_PAIEMENT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.MISE_EN_PAIEMENT IS 'Table contenant l''historique des heures mise en paiement (à payer ou payées)';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.ID_MISE_EN_PAIEMENT IS 'Clef primaire de la table';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.ID_SERVICE IS 'Référence à une fiche de service';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.ID_SERVICE_DETAIL IS 'Référence à un détail fiche de service (doit être un détail de la fiche de service référencée par ID_SERVICE)';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.ID_ARRETE IS 'Référence vers un arrêté (null si les heures ne sont pas encore payées)';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.HEURES_A_PAYEES IS 'Heures à payer';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.HEURES_PAYEES IS 'Heures payées';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

--------------------V20130729.143342__DDL_Ajout_table_flux_mise_en_paiement.sql---------------------
-- ------------------------
-- Flux de mise en paiement
-- ------------------------

--DROP TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT;
--DROP SEQUENCE GRH_PECHE.FLUX_MISE_EN_PAIEMENT_SEQ;

-- Table "Flux de mise en paiement"

CREATE TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT (
	ID_FLUX_MISE_EN_PAIEMENT NUMBER NOT NULL,
	ID_ARRETE NUMBER NOT NULL,
	ID_SERVICE NUMBER NOT NULL,
	NO_INSEE CHAR(13) NULL,
	CLE_INSEE NUMBER(2) NULL,
	NOM VARCHAR2(80) NOT NULL,
	PRENOM VARCHAR2(40) NOT NULL,
	NPC CHAR(2) NULL,
	HEURES_PAYEES NUMBER NOT NULL,
	TAUX_NON_CHARGE NUMBER NOT NULL,
	BRUT_PAYE NUMBER NOT NULL,
	D_CREATION DATE DEFAULT SYSDATE NOT NULL,
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
	PERS_ID_CREATION NUMBER NOT NULL,
	PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT PK_FLUX_MISE_EN_PAIEMENT PRIMARY KEY(ID_FLUX_MISE_EN_PAIEMENT) USING INDEX TABLESPACE GRH_INDX;

ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT FK_FMEP_ARRETE FOREIGN KEY (ID_ARRETE) REFERENCES GRH_PECHE.ARRETE(ID_ARRETE);
ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT FK_FMEP_SERVICE FOREIGN KEY (ID_SERVICE) REFERENCES GRH_PECHE.SERVICE(ID_SERVICE);
ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT FK_FMEP_PERS_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT FK_FMEP_PERS_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE(PERS_ID);

CREATE INDEX GRH_PECHE.IDX_FMEP_NO_INSEE ON GRH_PECHE.FLUX_MISE_EN_PAIEMENT(NO_INSEE) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_FMEP_NOM_PRENOM ON GRH_PECHE.FLUX_MISE_EN_PAIEMENT(NOM, PRENOM) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.FLUX_MISE_EN_PAIEMENT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT IS 'Table contenant l''historique des heures mise en paiement (à payer ou payées)';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.ID_FLUX_MISE_EN_PAIEMENT IS 'Clef primaire de la table';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.ID_ARRETE IS 'Référence vers un arrêté';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.ID_SERVICE IS 'Référence à une fiche de service';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.NO_INSEE IS 'Numéro INSEE de l''individu';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.CLE_INSEE IS 'Clef INSEE de l''individu';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.NOM IS 'Nom de la personne physique (peut contenir des caractères accentués)';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.PRENOM IS 'Prénom de la personne physique (peut contenir des caractères accentués)';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.NPC IS 'Numéro de Prise en Charge du dossier de l''agent pour les indemnités';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.HEURES_PAYEES IS 'Le nombre d''heures payées';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.TAUX_NON_CHARGE IS 'Le taux horaire non chargé';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.BRUT_PAYE IS 'Le montant brut payé';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

-------------------------V20130731.170451__DDL_Ajout_table_taux_horaire.sql-------------------------
-- -------------------------------
-- Mise en paiement - Taux horaire
-- -------------------------------

--DROP TABLE GRH_PECHE.TAUX_HORAIRE;
--DROP SEQUENCE GRH_PECHE.TAUX_HORAIRE_SEQ;
--INSERT INTO GRH_PECHE.TAUX_HORAIRE VALUES (GRH_PECHE.TAUX_HORAIRE_SEQ.NEXTVAL, 40.91, 42.96, 58.09, '01/01/2013', NULL, '01/08/2013', '01/08/2013', 72, 72);

CREATE TABLE GRH_PECHE.TAUX_HORAIRE (
	ID_TAUX_HORAIRE NUMBER NOT NULL,
	TAUX_BRUT NUMBER NOT NULL,
	TAUX_CHARGE_FONCTIONNAIRE NUMBER NOT NULL,
	TAUX_CHARGE_NON_FONCTIONNAIRE NUMBER NOT NULL,
	D_DEBUT DATE DEFAULT SYSDATE NOT NULL,
	D_FIN DATE NULL,
	D_CREATION DATE DEFAULT SYSDATE NOT NULL,
	D_MODIFICATION DATE DEFAULT SYSDATE NOT NULL,
	PERS_ID_CREATION NUMBER NOT NULL,
	PERS_ID_MODIFICATION NUMBER NOT NULL
);

ALTER TABLE GRH_PECHE.TAUX_HORAIRE ADD CONSTRAINT PK_TAUX_HORAIRE PRIMARY KEY(ID_TAUX_HORAIRE) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.TAUX_HORAIRE ADD CONSTRAINT FK_TH_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE(PERS_ID);
ALTER TABLE GRH_PECHE.TAUX_HORAIRE ADD CONSTRAINT FK_TH_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE(PERS_ID);

CREATE INDEX GRH_PECHE.IDX_TH_D_FIN_D_DEBUT ON GRH_PECHE.TAUX_HORAIRE(D_FIN, D_DEBUT) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.TAUX_HORAIRE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.TAUX_HORAIRE IS 'Table des taux horaires appliqués pour les paiements des intervenants';    
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.ID_TAUX_HORAIRE IS 'Clef primaire de la table';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.TAUX_BRUT IS 'Taux horaire brut appliqué à toutes populations confondus';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.TAUX_CHARGE_FONCTIONNAIRE IS 'Taux horaire chargé pour les fonctionnaires uniquement';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.TAUX_CHARGE_NON_FONCTIONNAIRE IS 'Taux horaire chargé pour les non fonctionnaires uniquement';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.D_DEBUT IS 'Date du début d''application du taux horaire';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.D_FIN IS 'Date de fin d''application du taux horaire';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.TAUX_HORAIRE.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

----------------------V20130823.113132__DDL_Index_dans_tablespace_grh_indx.sql----------------------
-- Recréation de l'index sur le tablespace INDX

ALTER TABLE GRH_PECHE.REPART_METHODE_CORPS DROP CONSTRAINT UK_REPART_METHODE_CORPS_CORPS;
ALTER TABLE GRH_PECHE.REPART_METHODE_CORPS  ADD CONSTRAINT UK_REPART_METHODE_CORPS_CORPS UNIQUE (C_CORPS) USING INDEX TABLESPACE GRH_INDX;

-------------------V20130828.143444__DDL_Maj_sequence_methodes_calculs_hcomp.sql--------------------
-- --------------------------------------------------------------------------------------
-- Mise à jour de la séquence de la table "Méthodes de calcul des heures complémentaires"
-- --------------------------------------------------------------------------------------

DROP SEQUENCE GRH_PECHE.METHODES_CALCUL_HCOMP_SEQ;
CREATE SEQUENCE GRH_PECHE.METHODES_CALCUL_HCOMP_SEQ START WITH 5;
-----------------------V20130904.174850__DDL_Modification_vue_enseignant.sql------------------------
-- -----------------------------------------------------------------
-- Prise en compte de la date de fin anticipée pour les vacataires
-- -----------------------------------------------------------------


CREATE OR REPLACE FORCE VIEW GRH_PECHE.V_PERSONNEL_ACTUEL_ENS (
  NO_DOSSIER_PERS, TEM_STATUTAIRE, TEM_TITULAIRE)
AS
  SELECT
    c.no_dossier_pers,
    'O',
    'O'
  FROM
    MANGUE.element_carriere c,
    GRHUM.corps co,
    GRHUM.type_population tp
  WHERE
    (
      c.d_effet_element <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        c.d_fin_element  IS NULL
      OR c.d_fin_element >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
        'dd/mm/YYYY')
      )
    )
  AND c.tem_valide = 'O'
  AND
    (
      tp.tem_enseignant = 'O'
    OR tp.tem_2degre    = 'O'
    OR tp.tem_ens_sup   = 'O'
    )
  AND c.c_corps       = co.c_corps
  AND co.c_type_corps = tp.c_type_population
  UNION
  -- inclusion des contrats enseignant au niveau réglementaire
  SELECT
    c.no_dossier_pers,
    'O',
    'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct
  WHERE
    (
      c.d_deb_contrat_trav <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        c.d_fin_contrat_trav                  IS NULL
      OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >= TO_DATE (TO_CHAR (SYSDATE,
        'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND tct.tem_invite_associe          != 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- inclusion des contrats vacataires typés enseignant
  SELECT
    c.no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.CONTRAT_VACATAIRES v,
    MANGUE.CONTRAT c ,
    GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE
    v.tem_enseignant='O'
  AND
    (
      c.d_deb_contrat_trav <= SYSDATE
    AND
      (
        c.d_fin_anticipee   IS NOT NULL
      AND c.d_fin_anticipee >= SYSDATE
      )
    OR
      (
        c.d_fin_anticipee IS NULL
      AND
        (
          c.d_fin_contrat_trav  IS NULL
        OR c.d_fin_contrat_trav >= SYSDATE
        )
      )
    )
  AND c.tem_annulation                 = 'N'
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O'
  AND c.no_seq_contrat                 = v.no_seq_contrat
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  UNION
  -- Prise en compte des Professeurs émérites
  SELECT
    no_dossier_pers,
    'N',
    'N'
  FROM
    MANGUE.emeritat E
  WHERE
    E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      E.D_FIN_EMERITAT  IS NULL
    OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    );



