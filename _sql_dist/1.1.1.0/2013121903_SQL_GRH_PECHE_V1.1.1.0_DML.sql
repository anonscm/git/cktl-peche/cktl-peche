--
-- Patch DML de PECHE du 19/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.1.1.0
-- Date de publication : 19/12/2013
-- Auteur(s) : Equipe PECHE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


------------------------V20131218.121931__DML_Ajout_taux_horaire_charge.sql-------------------------

-- ajout taux chargés par type d'agent fonctionnaire ou non

create or replace PROCEDURE  GRH_PECHE.COPIE_TAUX_CHARGE IS

  contrat_travail varchar2(2);
  
  CURSOR c_mise_en_paiement IS select * from grh_peche.mise_en_paiement where taux_charge is null;

  la_mise_en_paiement GRH_PECHE.mise_en_paiement%ROWTYPE;
  le_taux_horaire GRH_PECHE.taux_horaire%ROWTYPE;
  flag integer;
  
BEGIN

select count(*) into flag from grh_peche.taux_horaire where d_fin is null;

if (flag = 0) then
	    return;
end if;

select count(*) into flag from grh_peche.mise_en_paiement where taux_charge is null;
if (flag = 0) then
	    return;
end if;

select * into le_taux_horaire from grh_peche.taux_horaire where d_fin is null;

FOR la_mise_en_paiement IN c_mise_en_paiement
LOOP


 select mangue.contrat.C_TYPE_CONTRAT_TRAV into contrat_travail from mangue.contrat,GRH_PECHE.MISE_EN_PAIEMENT,GRH_PECHE.SERVICE where mise_en_paiement.id_mise_en_paiement=la_mise_en_paiement.id_mise_en_paiement 
 and service.id_service=mise_en_paiement.id_service and contrat.no_dossier_pers=service.no_individu; 
 
 if (contrat_travail='VF') then
  update grh_peche.mise_en_paiement set taux_charge=le_taux_horaire.taux_charge_fonctionnaire where mise_en_paiement.id_mise_en_paiement=la_mise_en_paiement.id_mise_en_paiement;
 else
  update grh_peche.mise_en_paiement set taux_charge=le_taux_horaire.taux_charge_non_fonctionnaire where mise_en_paiement.id_mise_en_paiement=la_mise_en_paiement.id_mise_en_paiement;
 end if;

end loop;

commit;

END;
/


BEGIN
  GRH_PECHE.COPIE_TAUX_CHARGE();
END;
/

-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.1.1.0', TO_DATE('19/12/2013', 'DD/MM/YYYY'), SYSDATE, 'Maj PECHE version 1.1.1.0');

COMMIT;
