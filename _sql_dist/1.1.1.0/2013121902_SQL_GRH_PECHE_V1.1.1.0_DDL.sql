--
-- Patch DDL de PECHE du 19/12/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DDL
-- Schema : GRHUM
-- Numero de version : 1.1.1.0
-- Date de publication : 19/12/2013
-- Auteur(s) : Equipe PECHE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


------------------------V20131218.121919__DDL_Ajout_taux_horaire_charge.sql-------------------------
-- ---------------------------------------------------
-- Ajout colonne TAUX_CHARGE dans la table MISE_EN_PAIEMENT
-- ---------------------------------------------------


-- le taux n'est pas non null pour gérer les paiements déjà faits

DECLARE
  is_column_exists int;
BEGIN 
	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'TAUX_CHARGE' 
	   AND table_name = 'MISE_EN_PAIEMENT';
  	    IF (is_column_exists  = 0) 
	  THEN
    	EXECUTE IMMEDIATE 'ALTER TABLE GRH_PECHE.MISE_EN_PAIEMENT ADD (TAUX_CHARGE NUMBER)';
		EXECUTE IMMEDIATE 'COMMENT ON COLUMN GRH_PECHE.MISE_EN_PAIEMENT.TAUX_CHARGE IS ''Taux horaire chargé appliqué''';
	  END IF;
END;
/

--------------------------V20131218.121955__DDL_Ajout_mois_paiement_tg.sql--------------------------


-- ----------------------------------------------------------------------
-- Ajout colonne MOIS_SUIVANT_POUR_TG dans la table FLUX_MISE_EN_PAIEMENT
-- ----------------------------------------------------------------------


DECLARE
  is_column_exists int;
BEGIN 
	SELECT count(*) 
	  INTO is_column_exists 
	  FROM all_tab_cols 
	 WHERE column_name = 'MOIS_SUIVANT_POUR_TG' 
	   AND table_name = 'FLUX_MISE_EN_PAIEMENT';
  	    IF (is_column_exists  = 0) 
	  THEN
		EXECUTE IMMEDIATE 'ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD (MOIS_SUIVANT_POUR_TG VARCHAR2(1 BYTE) DEFAULT ''N'' NOT NULL)';
    	EXECUTE IMMEDIATE 'COMMENT ON COLUMN GRH_PECHE.FLUX_MISE_EN_PAIEMENT.MOIS_SUIVANT_POUR_TG IS ''Mois suivant pour paiement TG ? (O:mois suivant, N:mois courant)''';
    	EXECUTE IMMEDIATE 'ALTER TABLE GRH_PECHE.FLUX_MISE_EN_PAIEMENT ADD CONSTRAINT CHK_MOIS_SUIVANT_POUR_TG CHECK (MOIS_SUIVANT_POUR_TG IN (''O'', ''N''))';
	  END IF;
END;
/




