--
-- Patch DDL de GRH_PECHE du 09/04/2013 à exécuter depuis le user GRH_PECHE
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 3/4
-- Type : DDL
-- Schema : GRH_PECHE
-- Numero de version : 0.1.0.2
-- Date de publication : 09/04/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

--
-- DB_VERSION
--
CREATE TABLE GRH_PECHE.DB_VERSION 
(
  DBV_ID	NUMBER(4) NOT NULL, 
  DBV_LIBELLE	VARCHAR2(15) NOT NULL, 
  DBV_DATE	DATE NOT NULL, 
  DBV_INSTALL	DATE, 
  DBV_COMMENT	VARCHAR2(2000)
);

ALTER TABLE GRH_PECHE.DB_VERSION ADD CONSTRAINT PK_DB_VERSION PRIMARY KEY (DBV_ID) USING INDEX TABLESPACE GRH_INDX;
CREATE SEQUENCE GRH_PECHE.DB_VERSION_SEQ START WITH 1 NOCACHE;

COMMENT ON TABLE GRH_PECHE.DB_VERSION IS 'Historique des versions du schéma du user GRH_PECHE'; 
COMMENT ON COLUMN GRH_PECHE.DB_VERSION.DBV_ID IS 'Identifiant de la version'; 
COMMENT ON COLUMN GRH_PECHE.DB_VERSION.DBV_LIBELLE IS 'Libellé de la version'; 
COMMENT ON COLUMN GRH_PECHE.DB_VERSION.DBV_DATE IS 'Date de release de la version'; 
COMMENT ON COLUMN GRH_PECHE.DB_VERSION.DBV_INSTALL IS 'Date d''installation de la version. Si non renseigné, la version n''est pas complétement installée.';
COMMENT ON COLUMN GRH_PECHE.DB_VERSION.DBV_COMMENT IS 'Le commentaire : une courte description de cette version de la base de données.';


--
--  REH
--
CREATE TABLE GRH_PECHE.REH
(
  ID_REH		NUMBER NOT NULL ,
  LC_REH   		VARCHAR2(128) ,
  LL_REH   		VARCHAR2(256) ,
  DESCRIPTION 		VARCHAR2(2048) ,
  COMMENTAIRE 		VARCHAR2(1024) ,
  D_DEBUT 		DATE DEFAULT SYSDATE NOT NULL,
  D_FIN  		DATE DEFAULT SYSDATE ,
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER  
);

ALTER TABLE GRH_PECHE.REH ADD CONSTRAINT PK_REH PRIMARY KEY(ID_REH) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.REH ADD CONSTRAINT FK_REH_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.REH ADD CONSTRAINT FK_REH_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);

CREATE SEQUENCE GRH_PECHE.REH_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.REH IS 'Table des activités du Réferentiel des Equivalences Horaires';    
COMMENT ON COLUMN GRH_PECHE.REH.ID_REH IS 'Clef primaire de la table REH';
COMMENT ON COLUMN GRH_PECHE.REH.LC_REH IS 'Libellé court de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.LL_REH IS 'Libellé long de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.DESCRIPTION IS 'Description de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.COMMENTAIRE IS 'Commentaire avec les méthodes de calcul de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.D_DEBUT IS 'Date du début de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.D_FIN IS 'Date du fin de l''activité REH';
COMMENT ON COLUMN GRH_PECHE.REH.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REH.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REH.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REH.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- ENSEIGNANT_GENERIQUE
--
CREATE TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE
(
  ID_ENSEIGNANT_GENERIQUE	NUMBER	NOT NULL ,
  NOM		 		VARCHAR2(30) ,
  PRENOM		 	VARCHAR2(30) ,
  C_CORPS 			VARCHAR2(4) ,
  HEURES_PREVUES	 	NUMBER	DEFAULT 0 NOT NULL ,
  TEM_VALIDE 			VARCHAR2(1) DEFAULT 'O' NOT NULL , 
  COMMENTAIRE 			VARCHAR2(1024) ,
  D_COMMENTAIRE  		DATE DEFAULT SYSDATE ,
  D_CREATION 			DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 		DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  		NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 		NUMBER 
);


ALTER TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE ADD CONSTRAINT PK_ENSEIGNANT_GENERIQUE PRIMARY KEY(ID_ENSEIGNANT_GENERIQUE) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE ADD CONSTRAINT FK_EG_CORPS FOREIGN KEY (C_CORPS) REFERENCES GRHUM.CORPS (C_CORPS);
ALTER TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE ADD CONSTRAINT FK_EG_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE ADD CONSTRAINT FK_EG_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE ADD CONSTRAINT CHK_EG_TEM_VALIDE CHECK (TEM_VALIDE IN ('O', 'N') );

CREATE INDEX GRH_PECHE.IDX_EG_C_CORPS ON GRH_PECHE.ENSEIGNANT_GENERIQUE(C_CORPS) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.ENSEIGNANT_GENERIQUE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.ENSEIGNANT_GENERIQUE IS 'Table qui permet de recenser les intervenants pas encore renseignés dans MANGUE';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.ID_ENSEIGNANT_GENERIQUE IS 'Clef primaire de la table ENSEIGNANT_GENERIQUE';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.NOM IS 'Nom de l''intervenant si connu';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.PRENOM IS 'Prénom de l''intervenant si connu';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.C_CORPS IS 'Type de population concernée (cf. GRHUM.CORPS.C_CORPS)';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.HEURES_PREVUES IS 'Heures du service prévu pour l''intervenant si c_corps non renseigné';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.TEM_VALIDE IS 'Validité de l''enregistrement ? (O:valide, N:invalide) ';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.COMMENTAIRE IS 'Commentaire sur l''intervenant';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.D_COMMENTAIRE IS 'Date de création du commentaire';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.ENSEIGNANT_GENERIQUE.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- SERVICE
--
CREATE TABLE GRH_PECHE.SERVICE
(
  ID_SERVICE			NUMBER	NOT NULL ,
  ID_DEMANDE 			NUMBER ,
  NO_INDIVIDU    		NUMBER(8, 0),
  ID_ENSEIGNANT_GENERIQUE	NUMBER ,
  TEM_ENS_GENERIQUE		VARCHAR2(1) DEFAULT 'N' NOT NULL ,
  TEM_VALIDE			VARCHAR2(1) DEFAULT 'O' NOT NULL ,
  COMMENTAIRE 			VARCHAR2(1024) ,
  D_COMMENTAIRE  		DATE ,
  ANNEE 			NUMBER(4) NOT NULL ,
  D_CREATION 			DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 		DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  		NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 		NUMBER
);

ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT PK_SERVICE PRIMARY KEY(ID_SERVICE) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES GRH_PECHE.DEMANDE (ID_DEMANDE) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_INDIVIDU_ULR FOREIGN KEY (NO_INDIVIDU) REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU);
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_ENSEIGNANT_GENERIQUE FOREIGN KEY (ID_ENSEIGNANT_GENERIQUE) REFERENCES GRH_PECHE.ENSEIGNANT_GENERIQUE (ID_ENSEIGNANT_GENERIQUE);
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT FK_SER_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT CHK_SER_TEM_EG CHECK (TEM_ENS_GENERIQUE IN ('O', 'N') );
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT CHK_SER_TEM_VALIDE CHECK (TEM_VALIDE IN ('O', 'N') );
-- Un des 2 identifiants (NO_INDIVIDU, ID_ENSEIGNANT_GENERIQUE) doit être renseigné au plus, mais également un des deux doit être renseigné au moins.
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT INDIVIDU_EG_NULLS CHECK (NO_INDIVIDU IS NOT NULL OR ID_ENSEIGNANT_GENERIQUE IS NOT NULL);
ALTER TABLE GRH_PECHE.SERVICE ADD CONSTRAINT INDIVIDU_EG_NOT_NULLS CHECK (NO_INDIVIDU IS NULL OR ID_ENSEIGNANT_GENERIQUE IS NULL);

CREATE INDEX GRH_PECHE.IDX_SER_ID_DEMANDE ON GRH_PECHE.SERVICE(ID_DEMANDE) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_SER_NO_INDIVIDU ON GRH_PECHE.SERVICE(NO_INDIVIDU) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_SER_ID_EG ON GRH_PECHE.SERVICE(ID_ENSEIGNANT_GENERIQUE) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_SER_ANNEE ON GRH_PECHE.SERVICE(ANNEE) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.SERVICE_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.SERVICE IS 'Fiche de service d''un intervenant';
COMMENT ON COLUMN GRH_PECHE.SERVICE.ID_SERVICE IS 'Clef primaire de la table SERVICE';
COMMENT ON COLUMN GRH_PECHE.SERVICE.ID_DEMANDE IS 'Référence à la demande sur le circuit de validation (cf. GRH_PECHE.DEMANDE.ID_DEMANDE)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.NO_INDIVIDU IS 'Intervenant concerné (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.ID_ENSEIGNANT_GENERIQUE IS 'Intervenant générique concerné (cf. GRH_PECHE.ENSEIGNANT_GENERIQUE.ID_ENSEIGNANT_GENERIQUE)';
COMMENT ON COLUMN GRH_PECHE.SERVICE.TEM_ENS_GENERIQUE IS 'L''affectation concerne un intervenant générique? (O:oui, N:non) ';
COMMENT ON COLUMN GRH_PECHE.SERVICE.TEM_VALIDE IS 'L''enregistrement est-il valide? (O:oui, N:non) ';
COMMENT ON COLUMN GRH_PECHE.SERVICE.COMMENTAIRE IS 'Commentaire sur la fiche de service';
COMMENT ON COLUMN GRH_PECHE.SERVICE.D_COMMENTAIRE IS 'Date de création du commentaire';
COMMENT ON COLUMN GRH_PECHE.SERVICE.ANNEE IS 'Année universitaire considérée';
COMMENT ON COLUMN GRH_PECHE.SERVICE.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.SERVICE.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.SERVICE.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.SERVICE.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- SERVICE_DETAIL
--
CREATE TABLE GRH_PECHE.SERVICE_DETAIL
(
  ID_SERVICE_DETAIL	NUMBER	NOT NULL ,
  ID_AP			NUMBER ,
  ID_REH  		NUMBER ,
  ID_SERVICE		NUMBER	NOT NULL,	
  HEURES_PREVUES 	NUMBER	DEFAULT 0 NOT NULL ,
  HEURES_REALISEES	NUMBER	DEFAULT 0 NOT NULL ,
  COMMENTAIRE 		VARCHAR2(1024) ,
  D_COMMENTAIRE  	DATE ,
  TEM_PAYE 		VARCHAR2(1) DEFAULT 'N' NOT NULL ,
  TEM_REPART_CROISEE 	VARCHAR2(1),
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER
);

ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT PK_SERVICE_DETAIL PRIMARY KEY(ID_SERVICE_DETAIL) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_AP FOREIGN KEY (ID_AP) REFERENCES SCO_SCOLARITE.AP (ID_AP);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_REH FOREIGN KEY (ID_REH) REFERENCES GRH_PECHE.REH (ID_REH);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_SERVICE FOREIGN KEY (ID_SERVICE) REFERENCES GRH_PECHE.SERVICE (ID_SERVICE);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT FK_SD_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT CHK_SD_TEM_PAYE CHECK (TEM_PAYE IN ('O', 'N') );
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT CHK_SD_TEM_REPART_CROISEE CHECK (TEM_REPART_CROISEE IN ('A', 'R', 'E') );
-- Un des 2 identifiants (ID_AP, ID_REH) doit être renseigné au plus, mais également un des deux doit être renseigné au moins.
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT AP_REH_NULLS CHECK (ID_AP IS NOT NULL OR ID_REH IS NOT NULL);
ALTER TABLE GRH_PECHE.SERVICE_DETAIL ADD CONSTRAINT AP_REH_NOT_NULLS CHECK (ID_AP IS NULL OR ID_REH IS NULL);

CREATE INDEX GRH_PECHE.IDX_SD_ID_AP ON GRH_PECHE.SERVICE_DETAIL(ID_AP) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_SD_ID_REH ON GRH_PECHE.SERVICE_DETAIL(ID_REH) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_SD_ID_SERVICE ON GRH_PECHE.SERVICE_DETAIL(ID_SERVICE) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.SERVICE_DETAIL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.SERVICE_DETAIL IS 'Détail de la fiche de service d''un intervenant';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.ID_SERVICE_DETAIL IS 'Clef primaire de la table SERVICE_DETAIL';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.ID_AP IS 'Référence à un AP présent dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.AP.ID)';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.ID_REH IS 'Référence à une activité REH dans laquelle intervient l''enseignant (cf. GRH_PECHE.REH.ID_REH)';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.ID_SERVICE IS 'Référence à la fiche de service de l''intervenant (cf. GRH_PECHE.SERVICE.ID_SERVICE)';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.HEURES_PREVUES IS 'Heures prévues dans le service de l''intervenant';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.HEURES_REALISEES IS 'Heures réalisées dans le service de l''intervenant';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.COMMENTAIRE IS 'Commentaire sur le détail de la fiche de service';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.D_COMMENTAIRE IS 'Date de création du commentaire';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.TEM_PAYE IS 'Intervenant payé (O:oui, N:non) ';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.TEM_REPART_CROISEE IS 'Répartition croisée: (A:accepté, R:refusé, E:en attente, null:pas de répartition croisée)';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.SERVICE_DETAIL.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- PARAM_POP_HETD
--
CREATE TABLE GRH_PECHE.PARAM_POP_HETD
(
  ID_PARAM_POP_HETD	NUMBER NOT NULL ,
  C_TYPE_CONTRAT_TRAV 	VARCHAR2(2) ,
  C_CORPS 		VARCHAR2(4) ,
  ID_TYPE_AP  	NUMBER(2) NOT NULL ,
  NUMERATEUR_SERVICE	NUMBER NOT NULL,
  DENOMINATEUR_SERVICE	NUMBER DEFAULT 1 NOT NULL, 
  NUMERATEUR_HCOMP	NUMBER ,
  DENOMINATEUR_HCOMP	NUMBER , 
  D_DEBUT 		DATE DEFAULT SYSDATE NOT NULL,
  D_FIN  		DATE ,
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER  
);

ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT PK_PARAM_POP_HETD PRIMARY KEY(ID_PARAM_POP_HETD) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT FK_PPH_TYPE_CONTRAT_TRAV FOREIGN KEY (C_TYPE_CONTRAT_TRAV) REFERENCES GRHUM.TYPE_CONTRAT_TRAVAIL (C_TYPE_CONTRAT_TRAV);
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT FK_PPH_CORPS FOREIGN KEY (C_CORPS) REFERENCES GRHUM.CORPS (C_CORPS);
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT FK_PPH_TYPE_AP FOREIGN KEY (ID_TYPE_AP) REFERENCES SCO_SCOLARITE.TYPE_AP (ID_TYPE_AP);
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT FK_PPH_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT FK_PPH_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
-- Un des 2 identifiants (C_TYPE_CONTRAT_TRAV, C_CORPS) doit être renseigné au plus, mais également un des deux doit être renseigné au moins.
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT PPH_TCT_CORPS_NULLS CHECK (C_TYPE_CONTRAT_TRAV IS NOT NULL OR C_CORPS IS NOT NULL);
ALTER TABLE GRH_PECHE.PARAM_POP_HETD ADD CONSTRAINT PPH_TCT_CORPS_NOT_NULLS CHECK (C_TYPE_CONTRAT_TRAV IS NULL OR C_CORPS IS NULL);

CREATE INDEX GRH_PECHE.IDX_PPH_C_TYPE_CONTRAT_TRAV ON GRH_PECHE.PARAM_POP_HETD(C_TYPE_CONTRAT_TRAV) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_PPH_C_CORPS ON GRH_PECHE.PARAM_POP_HETD(C_CORPS) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_PPH_TYPE_AP ON GRH_PECHE.PARAM_POP_HETD(ID_TYPE_AP) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.PARAM_POP_HETD_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.PARAM_POP_HETD IS 'Table qui définit les heures en équivalent TD des AP(CM, TD,...) en fonction de la population';    
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.ID_PARAM_POP_HETD IS 'Clef primaire de la table PARAM_POP_HETD';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.C_TYPE_CONTRAT_TRAV IS 'Type de population concernée parmi les contractuels(cf. GRHUM.TYPE_CONTRAT_TRAVAIL.C_TYPE_CONTRAT_TRAV)';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.C_CORPS IS 'Type de population concernée parmi les titulaires (cf. GRHUM.CORPS.C_CORPS)';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.ID_TYPE_AP IS 'Type du code de l''AP (cf. SCO_SCOLARITE.TYPE_AP.ID)';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.NUMERATEUR_SERVICE IS 'Numérateur du coefficient pour le calcul de l''AP dans le service';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.DENOMINATEUR_SERVICE IS 'Dénominateur du coefficient pour le calcul de l''AP dans le service';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.NUMERATEUR_HCOMP IS 'Numérateur du coefficient pour le calcul de l''AP en dehors du service dû';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.DENOMINATEUR_HCOMP IS 'Dénominateur du coefficient pour le calcul de l''AP en dehors du service dû';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.D_DEBUT IS 'Date du début d''application de l''équivalence';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.D_FIN IS 'Date de fin d''application de l''équivalence';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.PARAM_POP_HETD.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';

--
-- REPART_ENSEIGNANT
--
CREATE TABLE GRH_PECHE.REPART_ENSEIGNANT
(
  ID_REPART_ENSEIGNANT	NUMBER NOT NULL ,
  ID_SERVICE_DETAIL	NUMBER	NOT NULL ,
  COMPOSANTE_REFERENTE  VARCHAR2(10) NOT NULL ,
  REPARTITEUR_DEMANDEUR	NUMBER NOT NULL ,
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER 
);

ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT PK_REPART_ENSEIGNANT PRIMARY KEY(ID_REPART_ENSEIGNANT) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT FK_RE_SERVICE_DETAIL FOREIGN KEY (ID_SERVICE_DETAIL) REFERENCES GRH_PECHE.SERVICE_DETAIL (ID_SERVICE_DETAIL);
ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT FK_RE_COMPOSANTE_REFERENTE FOREIGN KEY (COMPOSANTE_REFERENTE) REFERENCES GRHUM.STRUCTURE_ULR (C_STRUCTURE);
ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT FK_RE_REPARTITEUR_DEMANDEUR FOREIGN KEY (REPARTITEUR_DEMANDEUR) REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU);
ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT FK_RE_PERS_ID_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.REPART_ENSEIGNANT ADD CONSTRAINT FK_RE_PERS_ID_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);

CREATE INDEX GRH_PECHE.IDX_RE_ID_SERVICE_DETAIL ON GRH_PECHE.REPART_ENSEIGNANT(ID_SERVICE_DETAIL) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_RE_COMPOSANTE_REFERENTE ON GRH_PECHE.REPART_ENSEIGNANT(COMPOSANTE_REFERENTE) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_RE_REPARTITEUR_DEMANDEUR ON GRH_PECHE.REPART_ENSEIGNANT(REPARTITEUR_DEMANDEUR) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.REPART_ENSEIGNANT_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.REPART_ENSEIGNANT IS 'Table où sont listés le répartiteur référent d''un enseignant et un autre répartiteur qui veut affecter des heures à cet enseignant';    
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.ID_REPART_ENSEIGNANT IS 'Clef primaire de la table REPART_ENSEIGNANT';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.ID_SERVICE_DETAIL IS 'Référence au détail du service de l''enseignant concerné (cf. GRH_PECHE.SERVICE_DETAIL.ID_SERVICE_DETAIL)';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.COMPOSANTE_REFERENTE IS 'Référence à la composante référente de l''enseignant (cf. GRHUM.STRUCTURE_ULR.C_STRUCTURE)';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.REPARTITEUR_DEMANDEUR IS 'Référence au répartiteur demandeur d''une autre composante (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.D_CREATION IS 'Date de création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.D_MODIFICATION IS 'Date de modification de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.REPART_ENSEIGNANT.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- FICHE_VOEUX
--
CREATE TABLE GRH_PECHE.FICHE_VOEUX
(
  ID_FICHE_VOEUX	NUMBER	NOT NULL ,
  ID_DEMANDE		NUMBER NOT NULL,
  NO_INDIVIDU    	NUMBER(8, 0) NOT NULL ,
  COMMENTAIRE 		VARCHAR2(1024) ,
  D_COMMENTAIRE  	DATE DEFAULT SYSDATE ,
  TEM_VALIDE		VARCHAR2(1) DEFAULT 'O' NOT NULL ,
  ANNEE 		NUMBER(4) NOT NULL,
  D_CREATION 		DATE DEFAULT SYSDATE NOT NULL ,
  D_MODIFICATION 	DATE DEFAULT SYSDATE NOT NULL ,
  PERS_ID_CREATION  	NUMBER NOT NULL ,
  PERS_ID_MODIFICATION 	NUMBER 
);

ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT PK_FICHE_VOEUX PRIMARY KEY(ID_FICHE_VOEUX)USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT FK_FV_DEMANDE FOREIGN KEY (ID_DEMANDE) REFERENCES GRH_PECHE.DEMANDE (ID_DEMANDE) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT FK_FV_INDIVIDU FOREIGN KEY (NO_INDIVIDU) REFERENCES GRHUM.INDIVIDU_ULR (NO_INDIVIDU);
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT FK_FV_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT FK_FV_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.FICHE_VOEUX ADD CONSTRAINT CHK_FV_TEM_VALIDE CHECK (TEM_VALIDE IN ('O', 'N'));

CREATE INDEX GRH_PECHE.IDX_FV_ID_DEMANDE ON GRH_PECHE.FICHE_VOEUX(ID_DEMANDE) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_FV_NO_INDIVIDU ON GRH_PECHE.FICHE_VOEUX(NO_INDIVIDU) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.FICHE_VOEUX_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.FICHE_VOEUX IS 'Fiche de voeux d''un enseignant';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.ID_FICHE_VOEUX IS 'Clef primaire de la table FICHE_VOEUX';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.ID_DEMANDE IS 'Référence à la demande sur le circuit de validation (cf. GRH_PECHE.DEMANDE.ID_DEMANDE)';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.NO_INDIVIDU IS 'Enseignant concerné (cf. GRHUM.INDIVIDU_ULR.NO_INDIVIDU)';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.COMMENTAIRE IS 'Commentaire sur la fiche de voeux';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.D_COMMENTAIRE IS 'Date de création du commentaire';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.TEM_VALIDE IS 'L''enregistrement est-il valide? (O:oui, N:non) ';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.ANNEE IS 'Année universitaire considérée';  
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';


--
-- FICHE_VOEUX_DETAIL
--
CREATE TABLE GRH_PECHE.FICHE_VOEUX_DETAIL
(
  ID_FV_DETAIL			NUMBER	NOT NULL,
  ID_FICHE_VOEUX		NUMBER	NOT NULL,
  ID_AP				NUMBER,
  ETABLISSEMENT_EXTERNE		VARCHAR2(1024),
  UE_ETABLISSEMENT_EXTERNE	VARCHAR2(128),
  HEURES_SOUHAITEES 		NUMBER	DEFAULT 0 NOT NULL,
  COMMENTAIRE 			VARCHAR2(1024),
  D_COMMENTAIRE  		DATE DEFAULT SYSDATE ,
  D_CREATION 			DATE DEFAULT SYSDATE NOT NULL,
  D_MODIFICATION 		DATE DEFAULT SYSDATE NOT NULL,
  PERS_ID_CREATION  		NUMBER NOT NULL,
  PERS_ID_MODIFICATION 		NUMBER 
);

ALTER TABLE GRH_PECHE.FICHE_VOEUX_DETAIL ADD CONSTRAINT PK_FICHE_VOEUX_DETAIL PRIMARY KEY(ID_FV_DETAIL) USING INDEX TABLESPACE GRH_INDX;
ALTER TABLE GRH_PECHE.FICHE_VOEUX_DETAIL ADD CONSTRAINT FK_FVD_FICHE_VOEUX FOREIGN KEY (ID_FICHE_VOEUX) REFERENCES GRH_PECHE.FICHE_VOEUX (ID_FICHE_VOEUX);
ALTER TABLE GRH_PECHE.FICHE_VOEUX_DETAIL ADD CONSTRAINT FK_FVD_AP FOREIGN KEY (ID_AP) REFERENCES SCO_SCOLARITE.AP (ID_AP);
ALTER TABLE GRH_PECHE.FICHE_VOEUX_DETAIL ADD CONSTRAINT FK_FVD_PERSONNE_CREATION FOREIGN KEY (PERS_ID_CREATION) REFERENCES GRHUM.PERSONNE (PERS_ID);
ALTER TABLE GRH_PECHE.FICHE_VOEUX_DETAIL ADD CONSTRAINT FK_FVD_PERSONNE_MODIFICATION FOREIGN KEY (PERS_ID_MODIFICATION) REFERENCES GRHUM.PERSONNE (PERS_ID);


CREATE INDEX GRH_PECHE.IDX_FVD_ID_FICHE_VOEUX ON GRH_PECHE.FICHE_VOEUX_DETAIL(ID_FICHE_VOEUX) TABLESPACE GRH_INDX;
CREATE INDEX GRH_PECHE.IDX_FVD_ID_AP ON GRH_PECHE.FICHE_VOEUX_DETAIL(ID_AP) TABLESPACE GRH_INDX;

CREATE SEQUENCE GRH_PECHE.FICHE_VOEUX_DETAIL_SEQ START WITH 1 INCREMENT BY 1 MINVALUE 1 NOCYCLE NOORDER;

COMMENT ON TABLE GRH_PECHE.FICHE_VOEUX_DETAIL IS 'Détail de la fiche de voeux d''un enseignant';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.ID_FV_DETAIL IS 'Clef primaire de la table FICHE_VOEUX_DETAIL';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.ID_FICHE_VOEUX IS 'Référence à la fiche de voeux de l''enseignant (cf. GRH_PECHE.FICHE_VOEUX.ID_FICHE_VOEUX)';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.ID_AP IS 'Référence à un AP présent dans l''offre de formation où intervient l''enseignant (cf. SCO_SCOLARITE.AP.ID)';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.ETABLISSEMENT_EXTERNE IS 'Nom de l''établissement si l''enseignement n''est pas dans celui d''origine';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.UE_ETABLISSEMENT_EXTERNE IS 'UE renseigné uniquement dans le cadre d''un autre établissement si connue';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.HEURES_SOUHAITEES IS 'Nombre d''heures souhaitées par l''enseignant';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.COMMENTAIRE IS 'Commentaire sur le détail de la fiche de voeux';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.D_COMMENTAIRE IS 'Date de création du commentaire';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.D_CREATION IS 'Date de création de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.D_MODIFICATION IS 'Date de modification de l''enregistrement.';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.PERS_ID_CREATION IS 'pers_id de la personne à l''origine de la création de l''enregistrement';
COMMENT ON COLUMN GRH_PECHE.FICHE_VOEUX_DETAIL.PERS_ID_MODIFICATION IS 'pers_id de la personne à l''origine de la modification de l''enregistrement';






























--
--  V_PERSONNEL_ACTUEL_ENS
--
CREATE OR REPLACE VIEW GRH_PECHE.V_PERSONNEL_ACTUEL_ENS (NO_DOSSIER_PERS, TEM_TITULAIRE)
AS
  --Enseignant titulaire 
  SELECT
    c.no_dossier_pers, 'O'
  FROM
    MANGUE.element_carriere c,
    GRHUM.corps co,
    GRHUM.type_population tp
  WHERE
    (
      c.d_effet_element <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND
      (
        c.d_fin_element  IS NULL
      OR c.d_fin_element >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND c.tem_valide    = 'O'
  AND c.c_corps       = co.c_corps
  AND co.c_type_corps = tp.c_type_population
  AND
    (
      tp.tem_enseignant = 'O'
    OR tp.tem_2degre    = 'O'
    OR tp.tem_ens_sup   = 'O'
    )
  UNION
  -- inclusion des contrats enseignant au niveau réglementaire
  SELECT
    c.no_dossier_pers, 'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct
  WHERE
    (
      c.d_deb_contrat_trav <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    AND
      (
        c.d_fin_contrat_trav                  IS NULL
      OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >= TO_DATE (TO_CHAR (SYSDATE,
        'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND tct.tem_invite_associe          != 'O'
  UNION
  -- inclusion des contrats non enseignant typés enseignant
  SELECT
    c.no_dossier_pers, 'N'
  FROM
    MANGUE.CONTRAT_VACATAIRES v,
    MANGUE.CONTRAT c ,
    GRHUM.TYPE_CONTRAT_TRAVAIL tct
  WHERE
    c.no_seq_contrat  = v.no_seq_contrat
  AND v.tem_enseignant='O'
  AND
    (
      c.d_deb_contrat_trav   <= SYSDATE
    AND ( c.d_fin_contrat_trav IS NULL
          OR c.d_fin_contrat_trav  >= SYSDATE
        )
    )
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  AND c.tem_annulation                 = 'N'
  AND tct.TEM_REMUNERATION_ACCESSOIRE != 'O'
  AND tct.TEM_TITULAIRE               != 'O'
  UNION
  -- inclusion des contrats non enseignant typés par le
  -- typepopulation enseignant pour des fonctionnaires
  SELECT
    no_dossier_pers, 'N'
  FROM
    MANGUE.contrat c,
    GRHUM.type_contrat_travail tct,
    MANGUE.contrat_avenant ca,
    GRHUM.grade g,
    GRHUM.corps c,
    GRHUM.type_population tp
  WHERE
    (
      c.d_deb_contrat_trav <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
    AND
      (
        c.d_fin_contrat_trav IS NULL
      OR ADD_MONTHS (c.d_fin_contrat_trav, 1) >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
      )
    )
  AND tct.droit_conges_contrat         = 'O'
  AND c.tem_annulation                 = 'N'
  AND tct.tem_enseignant               = 'O'
  AND c.c_type_contrat_trav            = tct.c_type_contrat_trav
  AND tct.tem_remuneration_accessoire != 'O'
  AND tct.tem_titulaire               != 'O'
  AND c.no_seq_contrat                 = ca.no_seq_contrat
  AND ca.c_grade                       = g.c_grade
  AND g.c_corps                        = c.c_corps
  AND c.c_type_corps                   = tp.c_type_population
  AND tp.tem_enseignant                = 'O'
  AND ca.tem_annulation                = 'N'
  AND ca.d_deb_contrat_av             <= TO_DATE (TO_CHAR (SYSDATE,
    'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      ca.d_fin_contrat_av IS NULL
    OR ADD_MONTHS (ca.d_fin_contrat_av, 1) >= TO_DATE (TO_CHAR (SYSDATE,
      'dd/mm/YYYY'), 'dd/mm/YYYY')
    )
  UNION
  -- Prise en compte des Professeurs émérites
  SELECT
    no_dossier_pers, 'N'
  FROM
    MANGUE.emeritat E
  WHERE
    E.D_DEB_EMERITAT <= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'), 'dd/mm/YYYY')
  AND
    (
      E.D_FIN_EMERITAT  IS NULL
      OR E.D_FIN_EMERITAT >= TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/YYYY'),
      'dd/mm/YYYY')
    );
