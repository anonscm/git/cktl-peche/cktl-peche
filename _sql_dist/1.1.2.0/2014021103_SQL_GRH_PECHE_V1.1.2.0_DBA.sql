--
-- Patch DBA de PECHE du 11/02/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DBA
-- Schema : GRHUM
-- Numero de version : 1.1.2.0
-- Date de publication : 11/02/2014
-- Auteur(s) : Equipe PECHE
--
--


WHENEVER SQLERROR EXIT SQL.SQLCODE;


declare
    cpt integer;
begin
	select count(*) into cpt from GRH_PECHE.DB_VERSION where dbv_libelle='1.1.1.0';
    if cpt = 0 then
        raise_application_error(-20000,'Le user GRH_PECHE n''est pas a jour pour passer ce patch !');
    end if;
       
	select count(*) into cpt from GRH_PECHE.DB_VERSION where dbv_libelle='1.1.2.0';
    if cpt > 0 then
        raise_application_error(-20000,'Le patch 1.1.2.0 a deja ete passe !');
    end if;

end;
/




