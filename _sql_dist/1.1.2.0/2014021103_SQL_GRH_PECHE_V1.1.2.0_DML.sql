--
-- Patch DML de PECHE du 11/02/2014 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
-- Fichier : 1
-- Type : DML
-- Schema : GRHUM
-- Numero de version : 1.1.2.0
-- Date de publication : 11/02/2014
-- Auteur(s) : Equipe PECHE
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--------------------------V20140121.151741__DML_Ajout_parametre_report.sql--------------------------

--Ajout de paramétrage : valeur pour le report positif et négatif des heures autorisé aux enseignants
DECLARE
	persid_createur integer;
	compteur integer;
	flag integer;
BEGIN
	-- Récupération d'un créateur
	persid_createur := null;
	SELECT count(*) INTO flag FROM (SELECT param_value FROM (
	SELECT * FROM GRHUM.grhum_parametres WHERE param_key='GRHUM_CREATEUR' ORDER BY param_ordre desc)
	WHERE ROWNUM=1) x, grhum.compte c WHERE x.param_value=c.cpt_login;
	
	IF (flag = 0) 
	THEN
	    RAISE_APPLICATION_ERROR(-20000,'GD_UTILS.init_constantes : impossible de récuperer le pers_id d''un GRHUM_CREATEUR');
	END IF;
	
	SELECT c.pers_id INTO persid_createur FROM (SELECT param_value FROM (
	SELECT * FROM GRHUM.grhum_parametres WHERE param_key='GRHUM_CREATEUR' ORDER BY param_ordre desc)
	WHERE ROWNUM=1) x, grhum.compte c WHERE x.param_value=c.cpt_login;

	--Tester existence paramètres
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRH_PECHE.PECHE_PARAMETRES 
 	 WHERE GRH_PECHE.PECHE_PARAMETRES.PARAM_KEY = 'report.borne.inferieur';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRH_PECHE.PECHE_PARAMETRES (PARAM_ID, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
		VALUES (GRH_PECHE.PECHE_PARAMETRES_SEQ.NEXTVAL, 'report.borne.inferieur', '0', 'Nombre d''heures autorisé que l''enseignant peut faire en plus sur l''année N+1 en cas de sous-service', SYSDATE, SYSDATE, persid_createur, persid_createur);
	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRH_PECHE.PECHE_PARAMETRES 
 	 WHERE GRH_PECHE.PECHE_PARAMETRES.PARAM_KEY = 'report.borne.superieur';
        IF (compteur = 0)
  	  THEN
		INSERT INTO GRH_PECHE.PECHE_PARAMETRES (PARAM_ID, PARAM_KEY, PARAM_VALUE, PARAM_COMMENTAIRES, D_CREATION, D_MODIFICATION, PERS_ID_CREATION, PERS_ID_MODIFICATION)
		VALUES (GRH_PECHE.PECHE_PARAMETRES_SEQ.NEXTVAL, 'report.borne.superieur', '0', 'Nombre d''heures autorisé que l''enseignant peut faire en moins sur l''année N+1 en cas d''heures complémentaires', SYSDATE, SYSDATE, persid_createur, persid_createur);
	END IF;
	
END;
/


-------------------------------V20140121.152306__DML_Ajout_droit.sql--------------------------------
--
-- Ajout droit pour la répartition des heures complémentaires
--

DECLARE
   compteur integer;
   idApplication NUMBER;
BEGIN
	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION, GRHUM.GD_APPLICATION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'REPARTITION_HCOMP'
	   AND GRHUM.GD_APPLICATION.APP_LC = 'PECHE'
	   AND GRHUM.GD_FONCTION.APP_ID = GRHUM.GD_APPLICATION.APP_ID;

    	    IF (compteur = 0)
  	  THEN
		SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';

		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	     VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignants', 'REPARTITION_HCOMP', 'Ens. statutaires - Répartition des heures comp', 'Enseignants statutaires - Répartition des heures complémentaires');
  	END IF;

	SELECT COUNT(*) 
  	  INTO compteur 
  	  FROM GRHUM.GD_FONCTION, GRHUM.GD_APPLICATION 
 	 WHERE GRHUM.GD_FONCTION.FON_ID_INTERNE = 'REPORT_HCOMP'
	   AND GRHUM.GD_APPLICATION.APP_LC = 'PECHE'
	   AND GRHUM.GD_FONCTION.APP_ID = GRHUM.GD_APPLICATION.APP_ID;

    	    IF (compteur = 0)
  	  THEN
		SELECT APP_ID INTO idApplication FROM GRHUM.GD_APPLICATION WHERE APP_LC = 'PECHE';

		INSERT INTO GRHUM.GD_FONCTION (FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION)
        	     VALUES (GRHUM.GD_FONCTION_SEQ.NEXTVAL, idApplication, 'Enseignants', 'REPORT_HCOMP', 'Ens. statutaires - Report des heures comp', 'Enseignants statutaires - Report des heures complémentaires');
  	END IF;
END;
/


-- DB_VERSION
INSERT INTO GRH_PECHE.DB_VERSION (DBV_ID, DBV_LIBELLE, DBV_DATE, DBV_INSTALL, DBV_COMMENT) VALUES (GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '1.1.2.0', TO_DATE('11/02/2014', 'DD/MM/YYYY'), SYSDATE, 'Maj PECHE version 1.1.2.0');

COMMIT;
