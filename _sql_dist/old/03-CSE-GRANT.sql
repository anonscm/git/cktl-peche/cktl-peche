--
-- Patch GRANT de CSE du 18/10/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : GRANT
-- Schema : CSE
-- Numero de version : 1.0.0.0
-- Date de publication : 19/09/2012 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


create user CSE identified by A_REMPLACER 
default tablespace GRH 
temporary tablespace TEMP 
quota unlimited on GRH; 

grant create session to CSE; 
grant create sequence to CSE; 
grant create table to CSE; 
grant create synonym to CSE; 
grant create view to CSE; 
grant unlimited tablespace to CSE; 
grant connect to CSE; 


--
-- Grants pour GRHUM
--
GRANT select, references on GRHUM.PERSONNE to CSE;
GRANT select, references on GRHUM.INDIVIDU_ULR to CSE;
GRANT select, references on GRHUM.STRUCTURE_ULR to CSE;
GRANT select, references on GRHUM.TYPE_VISA to CSE;
GRANT select, references on GRHUM.TYPE_ABSENCE to CSE;
GRANT select, references on GRHUM.TYPE_POPULATION to CSE;
GRANT select, references on GRHUM.COMPTE to CSE;
GRANT select, references on GRHUM.CORPS to CSE;
GRANT select, references on GRHUM.GRADE to CSE;
GRANT select, references on GRHUM.ASSOCIATION to CSE;
GRANT select, references on GRHUM.REPART_ASSOCIATION to CSE;
GRANT select, references on GRHUM.REPART_STRUCTURE to CSE;
GRANT select, references on GRHUM.TYPE_DECHARGE_SERVICE to CSE;
GRANT select, references on GRHUM.TYPE_CONTRAT_TRAVAIL to CSE;
GRANT select, references on GRHUM.GRHUM_PARAMETRES to CSE;
GRANT select, references on GRHUM.REPART_TYPE_GROUPE to CSE;
GRANT select, references on GRHUM.V_PERSONNEL_ACTUEL_ENS to CSE;
GRANT select, references on GRHUM.V_PERSONNEL_ACTUEL_VAC_ENS to CSE;
--GRANT select, references on GRHUM.TYPE_UNITE_FINANCEMENT to CSE;
--GRANT select, references on GRHUM.TYPE_FINANCEMENT to CSE;

GRANT SELECT,REFERENCES ON GRHUM.TYPE_MOTIF TO MANGUE;

GRANT execute on GRHUM.Trouve_Composante to CSE;


--
-- Grants pour MANGUE
--
GRANT select, references on MANGUE.AFFECTATION to CSE;
GRANT select, references on MANGUE.CONTRAT to CSE;
GRANT select, references on MANGUE.CARRIERE to CSE;
GRANT select, references on MANGUE.ELEMENT_CARRIERE to CSE;
GRANT select, references on MANGUE.CONTRAT_AVENANT to CSE;
GRANT select, references on MANGUE.ABSENCES to CSE;
GRANT select, references on MANGUE.VISA to CSE;
GRANT select, references on MANGUE.CONTRAT_VACATAIRES to CSE;
GRANT select, references on MANGUE.DECHARGE to CSE;
GRANT select, references on MANGUE.VISA_CGMOD_POP to CSE;
GRANT select, references on MANGUE.EMERITAT to CSE;


--
-- Grants pour JEFY_ADMIN
--
GRANT select, references on JEFY_ADMIN.DOMAINE to CSE;
GRANT select, references on JEFY_ADMIN.EXERCICE to CSE;
GRANT select, references on JEFY_ADMIN.FONCTION to CSE;

GRANT select, references on JEFY_ADMIN.PARAMETRE to CSE;
GRANT select, references on JEFY_ADMIN.TYPE_APPLICATION  to CSE;
GRANT select, references on JEFY_ADMIN.TYPE_ETAT to CSE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR to CSE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT to CSE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_EXERCICE to CSE;
GRANT select, references on JEFY_ADMIN.UTILISATEUR_FONCT_GESTION to CSE;
GRANT select, references on JEFY_ADMIN.V_UTILISATEUR_INFO to CSE;


--
-- Grants pour SCO_SCOLARITE
--
grant select, references on SCO_SCOLARITE.COMPOSANT to CSE;
grant select, references on SCO_SCOLARITE.LIEN to CSE;
grant select, references on SCO_SCOLARITE.AP to CSE;
grant select, references on SCO_SCOLARITE.AE to CSE;
grant select, references on SCO_SCOLARITE.DIPLOME to CSE;
grant select, references on SCO_SCOLARITE.EC to CSE;
grant select, references on SCO_SCOLARITE.PARCOURS to CSE;
grant select, references on SCO_SCOLARITE.PERIODE to CSE;
grant select, references on SCO_SCOLARITE.REGROUPEMENT to CSE;
grant select, references on SCO_SCOLARITE.UE to CSE;
grant select, references on SCO_SCOLARITE.STRUCTURES_COMPOSANT to CSE;
grant select, references on SCO_SCOLARITE.TYPE_AP to CSE;
grant select, references on SCO_SCOLARITE.VERSION_DIPLOME to cse;
grant select, references on SCO_SCOLARITE.TYPE_COMPOSANT to cse;
grant select, references on SCO_SCOLARITE.TYPE_LIEN to cse;

COMMIT;
