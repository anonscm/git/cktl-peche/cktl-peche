--
-- Patch DML de GRH_PECHE du 22/02/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 4/4
-- Type : DML
-- Schema : GRH_PECHE
-- Numero de version : 0.1.0.0
-- Date de publication : 22/02/2013 
-- Auteur(s) : Cocktail
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;


--
-- DB_VERSION
--
INSERT INTO GRH_PECHE.DB_VERSION VALUES(GRH_PECHE.DB_VERSION_SEQ.NEXTVAL, '0.1.0.0', TO_DATE('22/02/2013', 'DD/MM/YYYY'),NULL,'Version Beta du user GRH_PECHE');

--GD_DOMAINE
Insert into GRHUM.GD_DOMAINE(DOM_ID, DOM_LC) values (GRHUM.GD_DOMAINE_SEQ.NEXTVAL, 'RH');

--GD_APPLICATION
Insert into GRHUM.GD_APPLICATION(APP_ID, DOM_ID, APP_LC, APP_STR_ID) values (GRHUM.GD_APPLICATION_SEQ.NEXTVAL, (select max(dom_id) from grhum.GD_DOMAINE), 'PECHE', 'PECHE');


--GD_FONCTION
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 'Accès Peche', 'PECHE', 'Accès à l''application Peche', 'Accès à l''application PECHE');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 'Gestion Paramètres', 'SERVICE', 'Service des intervenants', 'Service des intervenants');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 'Gestion Paramètres', 'REH', 'Référentiel des Equivalences Horaires', 'Référentiel des Equivalences Horaires');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 'Gestion Paramètres', 'HETD', 'Gestion des Heures Equivalences TD', 'Gestion des Heures Equivalences TD');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) 
values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 
'Fiche de service', 'VALID_REPARTITEUR', 'Modifier la fiche de service', 'Modifier la fiche de service');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) 
values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 
'Fiche de service', 'VALID_DIRECTEUR', 'Valider la fiche par le directeur de composante', 'Valider la fiche par le directeur de composante');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) 
values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 
'Fiche de service', 'VALID_ENSEIGNANT', 'Valider la fiche de service par l''enseignant', 'Valider la fiche de service par l''enseignant');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) 
values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 
'Fiche de service', 'VALID_PRESIDENT', 'Valider la fiche de service par le président', 'Valider la fiche de service par le président');
Insert into GRHUM.GD_FONCTION(FON_ID, APP_ID, FON_CATEGORIE, FON_ID_INTERNE, FON_LC, FON_DESCRIPTION) 
values (GRHUM.GD_FONCTION_SEQ.NEXTVAL, (select max(APP_ID) from grhum.GD_APPLICATION), 
'Fiche de service', 'VALID_DRH', 'Valider la fiche de service par la DRH', 'Valider la fiche de service par la DRH');


--
-- DB_VERSION
--
UPDATE GRH_PECHE.DB_VERSION SET DBV_INSTALL=SYSDATE WHERE DBV_LIBELLE='0.1.0.0';


