# Génération des javadocs publiques PECHE
# ***************************************

# Méthodes de calcul des heures complémentaires
javadoc -d dochce -charset utf8 -windowtitle "PECHE - Méthodes de calcul des heures complémentaires" -sourcepath Sources org.cocktail.peche.metier.hce

# Flux de mise en paiement
javadoc -d docmep -charset utf8 -windowtitle "PECHE - Flux de mise en paiement" -sourcepath Sources org.cocktail.peche.metier.miseenpaiement
